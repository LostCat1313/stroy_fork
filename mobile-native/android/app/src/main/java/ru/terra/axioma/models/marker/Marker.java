package ru.terra.axioma.models.marker;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

import ru.terra.axioma.GsonTypeAdapters;
import ru.terra.axioma.R;
import ru.terra.axioma.models.Document;
import ru.terra.axioma.models.ProjectFile;

/**
 * Created by alexei on 7/23/17.
 */

@JsonAdapter(GsonTypeAdapters.MarkerAdapter.class)
public class Marker implements Parcelable {
    // Permanent marker type
    public final static int EDIT = 0;
    public final static int LINK = 1;
    public final static int NOTE = 2;
    // Temporary marker type
    public final static int TEXT = 3;
    public final static int RECT = 4;
    public final static int LINE = 5;

    @Expose
    public int id;
    @Expose
    public int type;
    @Expose
    public String title;
    @Expose
    public String text;
    @Expose
    public BaseParams params;
    @Expose
    public int docId;
    @SerializedName("holderId")
    public int fileId;
    @Expose
    public Date created;
    @Expose
    public int createdBy;

    @Expose(serialize = false)
    private boolean saved;
    /*@Expose(serialize = false)
    private Rect area;*/

    public Marker() {
    }

    public Marker(int type) {
        this.type = type;
    }

    public Marker(int id, int type, String title, String text, BaseParams params, int docId, int fileId, Date created, int createdBy) {
        this.id = id;
        this.type = type;
        this.title = title;
        this.text = text;
        this.params = params;
        this.docId = docId;
        this.fileId = fileId;
        this.created = created;
        this.createdBy = createdBy;
    }

    public Marker copy() {
        return new Marker(id, type, title, text, params, docId, fileId, created, createdBy);
    }

//    public Rect getArea() {
//        return area;
//    }
//
//    public void setArea(Rect area) {
//        this.area = area;
//    }

    public boolean isDrawable() {
        return type == RECT || type == LINE || type == TEXT;
    }

//    public void setParams(BaseParams params) {
//        this.params = params;
//    }
//
//    public BaseParams getParams() {
//        return params;
//    }

    //    public void setLink(Document document) {
//        if (params == null)
//            params = new LinkParams();
//        params.refDocId = document.id;
//        text = document.title;
//    }

    public void setLink(Document document, ProjectFile file) {
        if (params == null)
            return;

        LinkParams p = (LinkParams) params;
        p.refDocId = document.id;
        p.refFileId = file != null ? file.id : document.file.id;

        text = document.title + (file != null ? file.pageNumber : "");
        title = document.title + (file != null ? " Страница " + file.pageNumber : "");
    }

    public String getTypeName(Context c) {
        int stringRes = 0;
        switch (type) {
            case EDIT:
                stringRes = R.string.common_edit;
                break;

            case LINK:
                stringRes = R.string.common_link;
                break;

            case NOTE:
                stringRes = R.string.common_note;
                break;

            case TEXT:
                stringRes = R.string.editor_text;
                break;
        }

        return c.getString(stringRes);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeInt(this.type);
        dest.writeString(this.title);
        dest.writeString(this.text);
        dest.writeParcelable(this.params, flags);
        dest.writeInt(this.docId);
        dest.writeInt(this.fileId);
        dest.writeLong(this.created != null ? this.created.getTime() : -1);
        dest.writeInt(this.createdBy);
        dest.writeByte(this.saved ? (byte) 1 : (byte) 0);
    }

    protected Marker(Parcel in) {
        this.id = in.readInt();
        this.type = in.readInt();
        this.title = in.readString();
        this.text = in.readString();
        this.params = in.readParcelable(BaseParams.class.getClassLoader());
        this.docId = in.readInt();
        this.fileId = in.readInt();
        long tmpCreated = in.readLong();
        this.created = tmpCreated == -1 ? null : new Date(tmpCreated);
        this.createdBy = in.readInt();
        this.saved = in.readByte() != 0;
    }

    public static final Creator<Marker> CREATOR = new Creator<Marker>() {
        @Override
        public Marker createFromParcel(Parcel source) {
            return new Marker(source);
        }

        @Override
        public Marker[] newArray(int size) {
            return new Marker[size];
        }
    };
}
