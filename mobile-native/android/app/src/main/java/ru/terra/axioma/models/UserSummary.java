package ru.terra.axioma.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by alexei on 10/13/17.
 */

public class UserSummary {
    public int documents;
    @SerializedName("newMessages")
    public int messages;
    public int projects;
    public int remarks;
}
