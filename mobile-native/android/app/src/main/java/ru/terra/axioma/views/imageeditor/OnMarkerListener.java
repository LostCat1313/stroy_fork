package ru.terra.axioma.views.imageeditor;

import ru.terra.axioma.models.marker.Marker;

public interface OnMarkerListener {
    void markerAdd(Marker marker);
    void onMarkerAdded();
    //        void onStartEdit();
//        void onStopEdit();
    void onClick(Marker m);
}
