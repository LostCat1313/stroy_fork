package ru.terra.axioma.chat_v2


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.WhichButton
import com.afollestad.materialdialogs.actions.setActionButtonEnabled
import com.afollestad.materialdialogs.callbacks.onShow
import com.afollestad.materialdialogs.input.getInputField
import com.afollestad.materialdialogs.input.input
import com.afollestad.materialdialogs.list.listItems
import com.afollestad.materialdialogs.list.listItemsMultiChoice
import kotlinx.android.synthetic.main.fragment_chat_list.*
import org.koin.android.ext.android.getKoin
import org.koin.android.viewmodel.ext.android.sharedViewModel
import ru.terra.axioma.R
import ru.terra.axioma.chat_v2.adapters.ChatsAdapter
import ru.terra.axioma.models.chat.Room
import ru.terra.axioma.store.ChatStore
import ru.terra.axioma.store.CurrentSessionStore
import ru.terra.axioma.store.UserStore


class ListFragment : Fragment() {

    private val viewModel: ChatViewModel by sharedViewModel()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_chat_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        toolbar.setNavigationOnClickListener { activity?.onBackPressed() }
        toolbar.contentInsetStartWithNavigation = 0

        val chatStore: ChatStore = getKoin().get()
        chatStore.attachment?.let {
            toolbar.title = getString(R.string.chat_choose_chat)
        }

        dialogList.adapter = ChatsAdapter { viewModel.openChat(it) }

        viewModel.chats.observe(this, Observer {
            (dialogList.adapter as ChatsAdapter).items = it
        })

        viewModel.loading.observe(this, Observer {
            progress.visibility = if (it) View.VISIBLE else View.GONE
        })

        createChat.setOnClickListener { createChat() }
        createGroup.setOnClickListener { createGroup() }
    }

    private fun createChat() {
        menu.close(true)
        context?.let { context ->
            val currentSessionStore: CurrentSessionStore = getKoin().get()

            val filtered = viewModel.contacts.filter { currentSessionStore.user.id != it.id }
            val items = filtered.map { it.fullName }

            MaterialDialog(context)
                    .title(R.string.common_contacts)
                    .listItems(items = items, waitForPositiveButton = false) { d, i, text ->
                        viewModel.createChat(text, arrayListOf(filtered[i].id))
                        d.dismiss()
                    }
                    .negativeButton(R.string.common_cancel)
                    .show()
        }
    }

    private fun createGroup() {
        menu.close(true)
        context?.let { context ->
            val selectUsers = { name: String ->
                val filtered = viewModel.contacts.filter { UserStore.user.id != it.id }
                val items = filtered.map { it.fullName }

                var selected: IntArray? = null

                MaterialDialog(context)
                        .title(R.string.chat_choose_users)
                        .listItemsMultiChoice(items = items, waitForPositiveButton = false) { dialog, indices, _ ->
                            dialog.setActionButtonEnabled(WhichButton.POSITIVE, indices.size > 1)
                            selected = indices
                        }
                        .negativeButton(R.string.common_cancel)
                        .positiveButton(R.string.editor_ok) {
                            val ids = selected?.map { i -> filtered[i].id }
                            viewModel.createChat(name, ids as ArrayList<Int>)
                        }
                        .cancelOnTouchOutside(false)
                        .cancelable(false)
                        .onShow { it.setActionButtonEnabled(WhichButton.POSITIVE, false) }
                        .show()
            }

            MaterialDialog(context)
                    .title(R.string.chat_input_name)
                    .input(hintRes = R.string.chat_input_name, waitForPositiveButton = false) { dialog, text ->
                        dialog.setActionButtonEnabled(WhichButton.POSITIVE, text.isNotEmpty())
                    }
                    .negativeButton(R.string.common_cancel)
                    .positiveButton(R.string.editor_ok) { dialog ->
                        val inputField = dialog.getInputField()
                        selectUsers(inputField.text.toString())
                    }
                    .onShow { it.setActionButtonEnabled(WhichButton.POSITIVE, false) }
                    .show()
        }
    }
}
