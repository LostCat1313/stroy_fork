package ru.terra.axioma.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ru.terra.axioma.R
import ru.terra.axioma.models.PersonalNote


internal class PersonalNotesAdapter : RecyclerView.Adapter<PersonalNotesAdapter.ViewHolder>() {
    private var items: ArrayList<PersonalNote> = ArrayList()
    private var onClickListener: PersonalNotesAdapter.OnClickListener? = null
    private var isSelection: Boolean = false;
    private var selectedItems: HashMap<PersonalNote, Boolean> = HashMap()

    fun setData(data: ArrayList<PersonalNote>) {
        items = data
        notifyDataSetChanged()
    }

    fun setSelectionMode(mode: Boolean) {
        isSelection = mode
        notifyDataSetChanged()
    }

    fun getSelected(): List<PersonalNote> {
        return selectedItems.keys.toList()
    }

    fun setOnClickListener(onClickListener: PersonalNotesAdapter.OnClickListener) {
        this.onClickListener = onClickListener
    }

    internal inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var content: TextView = view.findViewById(R.id.content)
        var checkBox: CheckBox = view.findViewById(R.id.checkBox)
    }

    override fun onCreateViewHolder(parent: ViewGroup, type: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.personal_note_item, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        holder.content.text = item.content
        holder.itemView.setOnClickListener { v ->
            if (onClickListener != null) {
                onClickListener!!.onItemClick(item)
            }
        }

        if (isSelection) {
            holder.checkBox.visibility = View.VISIBLE
            holder.checkBox.isChecked = selectedItems[item] != null && selectedItems[item]!!
            holder.checkBox.setOnCheckedChangeListener { _, b -> selectedItems[item] = b }
        } else {
            holder.checkBox.visibility = View.GONE
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    internal interface OnClickListener {
        fun onItemClick(note: PersonalNote)
    }
}
