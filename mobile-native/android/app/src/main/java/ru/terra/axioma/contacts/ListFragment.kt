package ru.terra.axioma.contacts


import android.app.SearchManager
import android.content.Context
import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.fragment_contacts_list.*
import org.koin.android.viewmodel.ext.android.sharedViewModel
import ru.terra.axioma.BaseOnClickListener
import ru.terra.axioma.R
import ru.terra.axioma.adapters.ContactsAdapter
import ru.terra.axioma.views.LineDividerItemDecorator

class ListFragment : Fragment() {

    private val vm: ContactsViewModel by sharedViewModel()
    private var adapter: ContactsAdapter? = null
    private var searchView: SearchView? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_contacts_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupActionBar()
        setupList()
        setupSubscribers()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_search)
            return true

        return super.onOptionsItemSelected(item)
    }

    private fun setupList() {
        adapter = adapter ?: ContactsAdapter(BaseOnClickListener {
            vm.selectedItem.value = it
        })

        recyclerView.addItemDecoration(LineDividerItemDecorator.withLeftMargin(context))
        recyclerView.adapter = adapter
    }

    private fun setupSubscribers() {
        vm.contacts.observe(this, Observer { users ->
            adapter?.setData(users)
        })
        vm.loading.observe(this, Observer { aBoolean ->
            progress.visibility = if (aBoolean == true) View.VISIBLE else View.GONE
            recyclerView.visibility = if (aBoolean == true) View.GONE else View.VISIBLE
        })
    }

    private fun setupActionBar() {
        toolbar.apply {
            setNavigationOnClickListener { activity?.onBackPressed() }
            inflateMenu(R.menu.menu_contacts)
        }

        searchView = (toolbar.menu.findItem(R.id.action_search)?.actionView as SearchView?)?.apply {
            val searchManager: SearchManager = context?.getSystemService(Context.SEARCH_SERVICE) as SearchManager
            setSearchableInfo(searchManager.getSearchableInfo(activity?.componentName))
            maxWidth = Int.MAX_VALUE

            setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    adapter?.filter?.filter(query)
                    return false
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    adapter?.filter?.filter(newText)
                    return false
                }
            })
        }
    }

    companion object {
        fun newInstance(): ListFragment {
            return ListFragment()
        }
    }
}
