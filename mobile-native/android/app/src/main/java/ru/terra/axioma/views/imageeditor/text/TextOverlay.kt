package ru.terra.axioma.views.imageeditor.text


import android.app.Activity
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.Rect
import android.view.MotionEvent
import android.view.View
import android.view.View.OnClickListener
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.core.content.ContextCompat
import ru.terra.axioma.BuildConfig
import ru.terra.axioma.DEFAULT_COLOR
import ru.terra.axioma.R
import ru.terra.axioma.Utils
import ru.terra.axioma.models.marker.Marker
import ru.terra.axioma.models.marker.TextParams
import ru.terra.axioma.views.imageeditor.PanelView
import timber.log.Timber

class TextOverlay constructor(
        context: Context,
        private val scale: Float,
        private val initScale: Float
) : FrameLayout(context) {

    private lateinit var editText: DraggableTextView
    private lateinit var dummyView: View
    private var onDone: (Marker) -> Unit = { Timber.d("TextOverlay -> onDone not initialized") }
    private var onCancel: () -> Unit = { Timber.d("TextOverlay -> onCancel not initialized") }
    private var lastTouchAction: Int = 0

    private var curTextSizeValue: Float = 8f

    private var textSize: Float = 8f

    init {
        descendantFocusability = ViewGroup.FOCUS_AFTER_DESCENDANTS
        layoutParams = LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)

        if (BuildConfig.DEBUG) {
            setBackgroundColor(0x4800ffff)
        }

        addDummyView()
        addPanel()


        addEditText()
    }

    private fun addDummyView() {
        dummyView = View(context)
        dummyView.apply {
            isFocusable = true
            isFocusableInTouchMode = true
            setOnFocusChangeListener { _, _ -> Utils.hideKeyboard(context as Activity) }
        }
        addView(dummyView)
    }

    private fun addEditText() {
        editText = DraggableTextView(context).apply {
            isFocusable = true
            isFocusableInTouchMode = true

            backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context, android.R.color.transparent))

            hint = "Введите текст"
            textSize = this@TextOverlay.textSize
            setTextColor(DEFAULT_COLOR)

            onFocusChangeListener = OnFocusChangeListener { _, hasFocus ->
                if (hasFocus) {
                    this@TextOverlay.setBackgroundResource(R.color.transparent_white)
                    Utils.showSoftInput(context, true)
                    editText.center()
                } else {
                    this@TextOverlay.setBackgroundColor(Color.TRANSPARENT)
                    Utils.hideKeyboard(context as Activity)
                    editText.restore()
                }
            }
        }

        addView(editText)
        editText.requestFocus()
    }

    private fun addPanel() {
        val panelView = PanelView(context)
        addView(panelView)

        panelView.setOnDoneClick(OnClickListener {
            dummyView.requestFocus()

            onDone(Marker().apply {
                type = Marker.TEXT
                text = editText.text.toString()
                params = TextParams(editText.x, editText.y, curTextSizeValue.toInt(), editText.currentTextColor)
            })
        })

        panelView.setOnDeleteClick(OnClickListener {
            dummyView.requestFocus()
            onCancel()
        })

        panelView.onSizeChanged = {
            curTextSizeValue = (it + 12f)
            editText.textSize = curTextSizeValue * (1 / initScale) * scale
        }

        panelView.onColorChanged = {
            editText.setTextColor(it)
        }

        curTextSizeValue = panelView.sizeValue + 12f
        textSize = curTextSizeValue * (1 / initScale) * scale
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        when (event.action) {
            MotionEvent.ACTION_DOWN -> lastTouchAction = event.action

            MotionEvent.ACTION_UP -> {
                if (lastTouchAction == MotionEvent.ACTION_DOWN && editText.isFocused && !isInEditTextView(event))
                    return dummyView.requestFocus()
                return false
            }
        }

        return true
    }

    fun setOnDone(onDone: (Marker) -> Unit) {
        this.onDone = onDone
    }

    fun setOnCancel(onCancel: () -> Unit) {
        this.onCancel = onCancel
    }

    private fun isInEditTextView(ev: MotionEvent): Boolean {
        val rect = Rect()
        editText.getHitRect(rect)
        return rect.contains(ev.rawX.toInt(), ev.rawY.toInt())
    }
}
