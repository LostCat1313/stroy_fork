package ru.terra.axioma.models

class ProjectEntry(var image: ProjectFile, var title: String) {
    var entry: ProjectFields = ProjectFields(image, title)

    inner class ProjectFields(var image: ProjectFile, var title: String)
}