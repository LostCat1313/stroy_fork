package ru.terra.axioma.fcm

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.firebase.jobdispatcher.FirebaseJobDispatcher
import com.firebase.jobdispatcher.GooglePlayDriver
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import com.google.gson.JsonObject
import nl.komponents.kovenant.task
import org.koin.android.ext.android.getKoin
import org.koin.android.ext.android.inject
import ru.terra.axioma.App
import ru.terra.axioma.Constants
import ru.terra.axioma.R
import ru.terra.axioma.chat_v2.ChatActivity
import ru.terra.axioma.client.ClientApi
import ru.terra.axioma.documentstructure.DocumentListActivity
import ru.terra.axioma.login.LoginActivity
import ru.terra.axioma.main.MainActivity
import ru.terra.axioma.models.Notification
import ru.terra.axioma.store.ChatStore
import ru.terra.axioma.store.CurrentSessionStore
import ru.terra.axioma.store.UserStore
import timber.log.Timber

class CustomFirebaseMessagingService : FirebaseMessagingService() {

    private val currentSessionStore by inject<CurrentSessionStore>()

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        remoteMessage?.data?.let{
            if (it.isNotEmpty()) {
                Timber.tag(TAG).d("Message data payload: %s", it)

                val json = it.values.toTypedArray()[0]
                val notification = Gson().fromJson(json, Notification::class.java)
                handleNotification(notification)
            }
        }

    }

    override fun onNewToken(t: String?) {
        super.onNewToken(t)
        t?.let {
            currentSessionStore.regId = it
            sendRegistrationToServer(it)
        }
    }

    private fun scheduleJob() {
        val dispatcher = FirebaseJobDispatcher(GooglePlayDriver(this))
        val myJob = dispatcher.newJobBuilder()
                .setService(PushJobService::class.java)
                .setTag("axioma-job")
                .build()
        dispatcher.schedule(myJob)
    }

    private fun handleNow() {
        Timber.tag(TAG).d("Short lived task is done.")
    }

    private fun handleNotification(notification: Notification) {
        val intent = getIntent(notification)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent, PendingIntent.FLAG_ONE_SHOT)

        val channelId = getString(R.string.default_notification_channel_id)
        val notificationBuilder = NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.mipmap.icon)
                .setContentTitle(notification.title)
                .setContentText(notification.content)
                .setAutoCancel(true)
                .setDefaults(NotificationCompat.DEFAULT_ALL)
                .setContentIntent(pendingIntent)


        (getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).apply {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val channel = NotificationChannel(channelId, "Axioma channel", NotificationManager.IMPORTANCE_DEFAULT)
                createNotificationChannel(channel)
            }

            notify(0, notificationBuilder.build())
        }


        if (notification.type == Constants.NotificationType.ACTIVATION_CODE) {
            Intent(Constants.FILTER_ACTIVATION_CODE).apply {
                this.putExtra(LoginActivity.ACTIVATION_CODE, notification.content)
                LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(this)
            }
        } else if (MainActivity.isRunning) {
            val i = Intent(Constants.UPDATE_NOTIFICATION_LIST)
            LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(i)
        }
    }

    private fun getIntent(notification: Notification): Intent {
        val intent: Intent
        when (notification.type) {
            Constants.NotificationType.ACTIVATION_CODE -> return Intent(this, LoginActivity::class.java)

            Constants.NotificationType.CHAT_MESSAGE -> {
                val store: ChatStore by inject()
                store.targetMessage = notification.message
                startActivity(Intent(this, ChatActivity::class.java))
            }

            Constants.NotificationType.CHAT_ROOM_ADDED -> return Intent(this, ChatActivity::class.java)

            Constants.NotificationType.DOCUMENT_ADDED -> {
                intent = Intent(this, DocumentListActivity::class.java)
                intent.putExtra(Constants.NOTIFICATION_PROJECT_ID, notification.projectId)
                return intent
            }

            Constants.NotificationType.DOCUMENT_DESCRIPTOR_ADDED -> {
                intent = Intent(this, DocumentListActivity::class.java)
                intent.putExtra(Constants.NOTIFICATION_PROJECT_ID, notification.projectId)
                return intent
            }

            Constants.NotificationType.DOCUMENT_PAGES_ADDED -> {
                intent = Intent(this, DocumentListActivity::class.java)
                intent.putExtra(Constants.NOTIFICATION_PROJECT_ID, notification.projectId)
                return intent
            }

            Constants.NotificationType.DOCUMENT_REMOVED -> {
            }

            Constants.NotificationType.DOCUMENT_UPDATED -> {
                intent = Intent(this, DocumentListActivity::class.java)
                intent.putExtra(Constants.NOTIFICATION_PROJECT_ID, notification.projectId)
                return intent
            }

            Constants.NotificationType.DOCUMENT_VERSION_ADDED -> {
                intent = Intent(this, DocumentListActivity::class.java)
                intent.putExtra(Constants.NOTIFICATION_PROJECT_ID, notification.projectId)
                return intent
            }
        }

        return Intent(this, MainActivity::class.java)
    }

    private fun sendRegistrationToServer(token: String) {
        val client = getKoin().get<ClientApi>()
        task { client.setFirebaseId(getPingBody(token)).execute() }
    }

    private fun getPingBody(token: String): JsonObject {
        val entry = JsonObject()
        entry.addProperty("regId", token)
        entry.addProperty("deviceType", 0)
        return entry
    }

    companion object {

        private const val TAG = "FirebaseMsgService"
    }
}
