package ru.terra.axioma.models.chat;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ru.terra.axioma.models.User;

/**
 * Created by alexei on 12/17/17.
 */

public class Room implements Parcelable {
    public int id;
    public String name;
    public boolean isPrivate;

    public Date createdAt;
    public int createdBy;
    public User createdByUser;

    public int lastMessageId;
    public Message lastMessage;

    public int targetUserId;
    public User targetUser;

    public Date updatedAt;
    public List<User> users;

    public User getLastMessageSender() {
        if (users == null || users.isEmpty() || lastMessage == null)
            return null;

        for (User user : users) {
            if (user.id == lastMessage.senderId)
                return user;
        }

        return null;
    }

    public String getLastMessagePreview() {
        if (lastMessage == null)
            return "";

        User user = getLastMessageSender();
        if (user == null)
            return lastMessage.content;

        return "<b>" + user.getFullName() + ":</b> " + lastMessage.content;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeByte(this.isPrivate ? (byte) 1 : (byte) 0);
        dest.writeLong(this.createdAt != null ? this.createdAt.getTime() : -1);
        dest.writeInt(this.createdBy);
        dest.writeParcelable(this.createdByUser, flags);
        dest.writeInt(this.lastMessageId);
        dest.writeParcelable(this.lastMessage, flags);
        dest.writeInt(this.targetUserId);
        dest.writeParcelable(this.targetUser, flags);
        dest.writeLong(this.updatedAt != null ? this.updatedAt.getTime() : -1);
        dest.writeList(this.users);
    }

    public Room() {
    }

    protected Room(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.isPrivate = in.readByte() != 0;
        long tmpCreatedAt = in.readLong();
        this.createdAt = tmpCreatedAt == -1 ? null : new Date(tmpCreatedAt);
        this.createdBy = in.readInt();
        this.createdByUser = in.readParcelable(User.class.getClassLoader());
        this.lastMessageId = in.readInt();
        this.lastMessage = in.readParcelable(Message.class.getClassLoader());
        this.targetUserId = in.readInt();
        this.targetUser = in.readParcelable(User.class.getClassLoader());
        long tmpUpdatedAt = in.readLong();
        this.updatedAt = tmpUpdatedAt == -1 ? null : new Date(tmpUpdatedAt);
        this.users = new ArrayList<User>();
        in.readList(this.users, User.class.getClassLoader());
    }

    public static final Parcelable.Creator<Room> CREATOR = new Parcelable.Creator<Room>() {
        @Override
        public Room createFromParcel(Parcel source) {
            return new Room(source);
        }

        @Override
        public Room[] newArray(int size) {
            return new Room[size];
        }
    };
}
