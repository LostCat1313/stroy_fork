package ru.terra.axioma.views

import android.content.Context

import ru.terra.axioma.R

/**
 * Created by alexei on 9/27/17.
 */

class NotificationLink(context: Context) : NotificationCard(context) {

    override val iconRes: Int
        get() = R.drawable.ic_link

    override val colorRes: Int
        get() = R.color.teal

    override val titleRes: Int
        get() = R.string.card_link
}
