package ru.terra.axioma.repository

import androidx.lifecycle.MutableLiveData
import nl.komponents.kovenant.Promise
import nl.komponents.kovenant.task
import nl.komponents.kovenant.then
import nl.komponents.kovenant.ui.alwaysUi
import ru.terra.axioma.client.ClientApi
import ru.terra.axioma.models.Position
import timber.log.Timber

class PositionsRepository(private val client: ClientApi) {

    var positions: MutableLiveData<List<Position>> = MutableLiveData()
    var isLoading: MutableLiveData<Boolean> = MutableLiveData()

    val positionsList: List<Position>
        get() = positions.value!!

    fun fetch(): Promise<Unit, Exception> {
        isLoading.value = true

        return task {
            client.positions.execute()
        }.alwaysUi {
            isLoading.value = false
        }.then {
            if (it.isSuccessful) {
                val list = it.body()?.get("list")?.asJsonArray
                positions.postValue(
                        Array(list?.size() ?: 0) { v ->
                            Position(list?.get(v)!!)
                        }.asList()
                )
            }
        }.fail {
            Timber.d(it)
        }
    }

    fun getIdByName(name: String): Int? {
        return positionsList.find { it.name.equals(name, true) }?.id
    }
}
