package ru.terra.axioma.interfaces;

import android.view.View;

import ru.terra.axioma.models.Document;

/**
 * Created by alexei on 01.02.18.
 */

public interface OnDocumentStructureListener {
    void openPages(Document document);
    void openDocument(Document document);
    void setSelectableMode(boolean selectable, boolean isFile);
    void openAddFileScreen(Document document);
    void updateList(Document document, boolean remove);
    View getVersionSelectorButton(Document document);
}
