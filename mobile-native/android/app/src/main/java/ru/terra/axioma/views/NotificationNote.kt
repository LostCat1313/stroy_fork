package ru.terra.axioma.views

import android.content.Context

import ru.terra.axioma.R


class NotificationNote(context: Context) : NotificationCard(context) {

    override val iconRes: Int
        get() = R.drawable.ic_note_black_24dp

    override val colorRes: Int
        get() = R.color.lime

    override val titleRes: Int
        get() = R.string.card_note
}
