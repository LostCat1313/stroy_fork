package ru.terra.axioma.repository

import nl.komponents.kovenant.Promise
import nl.komponents.kovenant.all
import nl.komponents.kovenant.task
import nl.komponents.kovenant.then
import ru.terra.axioma.client.ClientApi
import ru.terra.axioma.models.marker.Marker
import ru.terra.axioma.models.marker.MarkerEntry


class MarkersRepository(private val client: ClientApi) {

    fun fetch(docId: Int, fileId: Int): Promise<ArrayList<Marker>, Exception> {
        return task {
            client.getMarkers(docId, fileId).execute()
        } then {
            it.body()?.list as ArrayList<Marker>
        }
    }

    fun update(marker: Marker): Promise<Marker?, Exception> {
        return task {
            client.updateMarker(marker.id, MarkerEntry(marker)).execute()
        } then {
            it.body()
        }
    }

    fun save(markers: ArrayList<Marker>): Promise<List<Marker>, Exception> {
        val promises = markers.map {
            task {
                client.saveMarker(MarkerEntry(it)).execute()
            }
        }

        return all(promises) then {
            it.map { response ->
                response.body()
            }.requireNoNulls()
        }
    }

    fun save(marker: Marker): Promise<Marker?, Exception> {
        return task {
            client.saveMarker(MarkerEntry(marker)).execute()
        } then {
            it.body()
        }
    }
}
