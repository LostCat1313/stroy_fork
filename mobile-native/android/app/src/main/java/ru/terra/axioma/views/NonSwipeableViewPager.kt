package ru.terra.axioma.views

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import androidx.viewpager.widget.ViewPager

class NonSwipeableViewPager @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null) : ViewPager(context, attrs) {

    override fun onInterceptTouchEvent(event: MotionEvent) : Boolean {
        return false
    }

    override fun onTouchEvent(event: MotionEvent) : Boolean {
        return false
    }
}