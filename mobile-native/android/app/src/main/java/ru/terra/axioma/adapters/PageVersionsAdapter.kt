package ru.terra.axioma.adapters

import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.TextView

import com.google.common.collect.Lists

import ru.terra.axioma.R
import ru.terra.axioma.StringUtils
import ru.terra.axioma.models.Version

class PageVersionsAdapter : RecyclerView.Adapter<PageVersionsAdapter.ViewHolder>() {
    private val HEADER_TYPE = 0
    private val ITEM_TYPE = 1

    private var data: List<Version> = ArrayList()
    var selectedPosition = -1

//    private val selectedPositionValue: Int
//        get() = if (selectedPosition.value != null) selectedPosition.value!! else -1

    val selectedVersion: MutableLiveData<Version?> = MutableLiveData()
//        get() = if (selectedPositionValue <= data.fontSize) data[selectedPositionValue] else null

    init {
        data = Lists.newArrayList()
    }

    fun setData(data: List<Version>) {
        this.data = data
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var text: TextView? = itemView.findViewById(R.id.text)
//        var radio: RadioButton? = itemView.findViewById(R.id.radio)

        init {
            text?.apply {
                setOnClickListener {
                    selectedVersion.value = data[adapterPosition]
                    selectedPosition = adapterPosition
                    notifyDataSetChanged()
                }
            }
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view: View = if (viewType == ITEM_TYPE)
            inflater.inflate(R.layout.version_list_item, parent, false)
        else
            inflater.inflate(R.layout.version_list_header, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = data[position]
        val viewType = getItemViewType(position)
        if (viewType == HEADER_TYPE) {
            holder.text?.text = item.title
        } else {
            holder.text?.apply {
                text = StringUtils.dateToDateTimeString(item.createdAt)
//                isChecked = holder.adapterPosition == position
            }

            if (selectedPosition == position)
                holder.itemView.setBackgroundResource(R.color.grey_300)
            else
                holder.itemView.setBackgroundResource(R.color.transparent_white)
        }
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (data[position].id == -1) HEADER_TYPE else ITEM_TYPE
    }
}
