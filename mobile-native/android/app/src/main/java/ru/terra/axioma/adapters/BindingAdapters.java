package ru.terra.axioma.adapters;

import android.net.Uri;
import android.text.TextWatcher;
import android.widget.EditText;

import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import ru.terra.axioma.chat_v2.adapters.AttachmentsAdapter;

/**
 * Created by alexei on 20.03.18.
 */

public class BindingAdapters {
    @BindingAdapter("textChangedListener")
    public static void bindTextListener(EditText editText, TextWatcher textWatcher) {
        editText.addTextChangedListener(textWatcher);
    }

    @BindingAdapter("attachments")
    public static void setAttachments(RecyclerView view, List<Uri> data) {
        AttachmentsAdapter adapter = (AttachmentsAdapter) view.getAdapter();
        if (data != null && adapter != null) {
            adapter.setData(data);
        }
    }

    @BindingAdapter("pages")
    public static void setPages(RecyclerView view, List<AttachPhotoAdapter.AttachPhoto> data) {
        AttachPhotoAdapter adapter = (AttachPhotoAdapter) view.getAdapter();
        if (data != null && adapter != null) {
            adapter.setData(data);
        }
    }
}
