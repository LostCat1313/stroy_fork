package ru.terra.axioma.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.JsonAdapter;

import java.util.Date;
import java.util.List;

import javax.annotation.Nullable;

import ru.terra.axioma.GsonTypeAdapters;

/**
 * Created by alexei on 18.02.18.
 */

@JsonAdapter(GsonTypeAdapters.VersionAdapter.class)
public class Version extends BaseObject implements Parcelable {
    public int documentId;
    public String title;
    public ProjectFile file;
    public int fileId;
    public List<ProjectFile> pages;

    public Version() {
    }

    public Version(String title) {
        id = -1;
        this.title = title;
    }

    public Version(int id, Date createdAt, Date updatedAt, int createdBy, int modifiedBy, int documentId, String title, ProjectFile file, int fileId, List<ProjectFile> pages) {
        this.id = id;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.createdBy = createdBy;
        this.modifiedBy = modifiedBy;
        this.documentId = documentId;
        this.title = title;
        this.file = file;
        this.fileId = fileId;
        this.pages = pages;
    }

    public boolean isMultiPage() {
        return pages != null && pages.size() > 1;
    }

    public int getPagesCount() {
        return pages == null ? 0 : pages.size();
    }

    @Nullable
    public ProjectFile getPageVersion(ProjectFile page) {
        for (ProjectFile p : pages)
            if (/*p.actual &&*/ p.pageNumber == page.pageNumber)
                return p;
        return null;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.documentId);
        dest.writeString(this.title);
        dest.writeParcelable(this.file, flags);
        dest.writeInt(this.fileId);
        dest.writeTypedList(this.pages);
        dest.writeInt(this.id);
        dest.writeLong(this.createdAt != null ? this.createdAt.getTime() : -1);
        dest.writeLong(this.updatedAt != null ? this.updatedAt.getTime() : -1);
        dest.writeInt(this.createdBy);
        dest.writeInt(this.modifiedBy);
    }

    protected Version(Parcel in) {
        this.documentId = in.readInt();
        this.title = in.readString();
        this.file = in.readParcelable(ProjectFile.class.getClassLoader());
        this.fileId = in.readInt();
        this.pages = in.createTypedArrayList(ProjectFile.CREATOR);
        this.id = in.readInt();
        long tmpCreatedAt = in.readLong();
        this.createdAt = tmpCreatedAt == -1 ? null : new Date(tmpCreatedAt);
        long tmpUpdatedAt = in.readLong();
        this.updatedAt = tmpUpdatedAt == -1 ? null : new Date(tmpUpdatedAt);
        this.createdBy = in.readInt();
        this.modifiedBy = in.readInt();
    }

    public static final Parcelable.Creator<Version> CREATOR = new Parcelable.Creator<Version>() {
        @Override
        public Version createFromParcel(Parcel source) {
            return new Version(source);
        }

        @Override
        public Version[] newArray(int size) {
            return new Version[size];
        }
    };
}
