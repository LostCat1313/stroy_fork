package ru.terra.axioma.models.marker;

public class LabelBaseParams extends BaseParams {
    public int x;
    public int y;

    public void setXY(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
