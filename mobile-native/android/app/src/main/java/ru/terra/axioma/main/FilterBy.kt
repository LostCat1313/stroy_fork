package ru.terra.axioma.main

enum class FilterBy {
    Docs, Notes, Messages, Project
}