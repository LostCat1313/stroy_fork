package ru.terra.axioma.pages;

import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.PopupWindow;

import com.google.common.collect.Lists;

import java.util.Arrays;
import java.util.List;

import ru.terra.axioma.App;
import ru.terra.axioma.Constants;
import ru.terra.axioma.R;
import ru.terra.axioma.adapters.PagesAdapter;
import ru.terra.axioma.adapters.SimpleTextAdapter;
import ru.terra.axioma.client.ClientApi;
import ru.terra.axioma.databinding.ActivityPagesBinding;
import ru.terra.axioma.documentviewer.DocumentActivity;
import ru.terra.axioma.models.Document;
import ru.terra.axioma.models.ProjectFile;
import ru.terra.axioma.views.GridSpacingItemDecorator;

public class PagesActivity extends AppCompatActivity {
    private ActivityPagesBinding binding;

    private Document document;
    private PagesAdapter adapter;
    private Button versionSelectorButton;
    private int versionIndex = -1;
    private PopupWindow versionMenu;

    public PagesActivity() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_pages);

        document = getIntent().getParcelableExtra(Constants.DOCUMENT);

        setupActionBar();

        GridLayoutManager layoutManager = new GridLayoutManager(this, 2);
        binding.recyclerView.setLayoutManager(layoutManager);
        binding.recyclerView.addItemDecoration(new GridSpacingItemDecorator(2, 50, true));

        adapter = new PagesAdapter(document, document.getAllPages(), (file, position) -> {
            Intent intent = new Intent(this, DocumentActivity.class);
            intent.putExtra(DocumentActivity.DOCUMENT, document);
            intent.putExtra(DocumentActivity.FILE, file);
            intent.putExtra(DocumentActivity.VERSION, versionIndex);
            intent.putExtra(DocumentActivity.DOCUMENT_ID, document.id);
            intent.putExtra(DocumentActivity.FILE_ID, file.id);
            startActivity(intent);
        });
        binding.recyclerView.setAdapter(adapter);
    }

    private void setupActionBar() {
        binding.toolbar.setTitleTextAppearance(this, R.style.ToolbarTitle);
        setSupportActionBar(binding.toolbar);
        setTitle(document.title);
        ActionBar actionBar = getSupportActionBar();

        if (actionBar == null)
            return;

        if (document.isMultiVersion()) {
            setupVersionMenu();

            View view = getLayoutInflater().inflate(R.layout.toolbar_version_selector, null);

            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setCustomView(view, new ActionBar.LayoutParams(
                    ActionBar.LayoutParams.WRAP_CONTENT,
                    ActionBar.LayoutParams.WRAP_CONTENT,
                    Gravity.END | Gravity.CENTER_VERTICAL
            ));

//            actionBar.getCustomView().findViewById(R.id.version_selector);
            versionSelectorButton = actionBar.getCustomView().findViewById(R.id.version_selector);
            versionSelectorButton.setText("Все");
            versionSelectorButton.setOnClickListener(v -> showVersionList());
        }

        actionBar.setDisplayHomeAsUpEnabled(true);
        binding.toolbar.setContentInsetStartWithNavigation(0);

        binding.toolbar.setNavigationOnClickListener(view -> {
            onBackPressed();
        });
    }

    private void setupVersionMenu() {
        versionMenu = new PopupWindow(this);

        LayoutInflater inflater = LayoutInflater.from(this);
        View view = inflater.inflate(R.layout.pages_list, null);

        versionMenu.setFocusable(true);
        versionMenu.setOutsideTouchable(true);
        versionMenu.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        versionMenu.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        versionMenu.setContentView(view);
        versionMenu.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        versionMenu.setElevation(0);

        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);

        RecyclerView list = view.findViewById(R.id.recyclerView);

        String[] items = new String[] {
                "Все",
                "Оригинал",
                "Свежайшее"
        };

        List<List<ProjectFile>> datas = Lists.newArrayList();
        datas.add(document.getAllPages());
        datas.add(document.getOriginalPages());
        datas.add(document.getLatestPages());

        SimpleTextAdapter adapter = new SimpleTextAdapter(Arrays.asList(items), R.layout.string_list_item, position -> {
            versionSelectorButton.setText(items[position]);
            PagesActivity.this.adapter.setData(datas.get(position));
            versionMenu.dismiss();
        });
        list.setLayoutManager(new LinearLayoutManager(this));
        list.setAdapter(adapter);
    }

    private void showVersionList() {
        Rect r = new Rect();
        binding.recyclerView.getGlobalVisibleRect(r);
        versionMenu.showAtLocation(binding.recyclerView, Gravity.TOP | Gravity.END, 0, r.top);
    }
}
