package ru.terra.axioma.projects

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.RecyclerView
import ru.terra.axioma.R
import ru.terra.axioma.models.UserRights


internal class RightsAdapter(var value: Int) : RecyclerView.Adapter<RightsAdapter.ViewHolder>() {

    private val items = mutableListOf<Int>().apply {
        add(UserRights.READ)
        add(UserRights.WRIGHT)
        add(UserRights.ADMIN)
    }
    private var selected = items.indexOf(value)

    val selectedValue: Int
        get() {
            return items[selected]
        }

//    var onClickListener: RightsAdapter.OnClickListener? = null

    internal inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var text: TextView = view.findViewById(R.id.text)
        var check: AppCompatImageView = view.findViewById(R.id.check)

        init {
            view.setOnClickListener {
                selected = adapterPosition
                notifyDataSetChanged()
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, type: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.right_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.text.text = UserRights.getString(items[position])
        holder.check.visibility = if (selected == position) View.VISIBLE else View.INVISIBLE
    }

    override fun getItemCount(): Int {
        return items.size
    }
}
