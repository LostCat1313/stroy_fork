package ru.terra.axioma.views

import android.Manifest
import android.app.Activity
import android.content.Context
import android.view.View
import android.widget.TextView
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.customview.customView
import com.afollestad.materialdialogs.customview.getCustomView
import com.afollestad.materialdialogs.list.listItems
import com.facebook.drawee.view.SimpleDraweeView
import pub.devrel.easypermissions.EasyPermissions
import pub.devrel.easypermissions.PermissionRequest
import ru.terra.axioma.Constants
import ru.terra.axioma.R

object CustomDialog {
    operator fun get(context: Context): MaterialDialog {
        return MaterialDialog(context)
//                .negativeColorRes(R.color.grey_600)
//                .positiveColorRes(R.color.grey_600)
    }

    fun getLoadingDialog(context: Context): MaterialDialog {
        return MaterialDialog(context)
                .message(R.string.common_loading)
                .cancelOnTouchOutside(false)
                .cancelable(false)
//                .progress(true, 0)
    }

    fun error(context: Context, messageId: Int): MaterialDialog {
        return error(context, context.getString(messageId))
    }

    fun error(context: Context, message: String): MaterialDialog {
        return MaterialDialog(context)
//                .negativeColorRes(R.color.grey_600)
//                .positiveColorRes(R.color.grey_600)
//                .neutralColorRes(R.color.grey_600)
                .message(text = message)
                .title(R.string.common_error)
    }

    fun getImageDialog(context: Context, uriString: String): MaterialDialog {
        val dialog = CustomDialog[context].customView(R.layout.dialog_with_image_view)

        val content = dialog.getCustomView()
        val preview = content.findViewById<SimpleDraweeView>(R.id.preview)
        preview.setImageURI(uriString)

        return dialog
    }

    fun getImageWithTextDialog(context: Context, uriString: String, text: String): MaterialDialog {
        val dialog = getImageDialog(context, uriString)

        val content = dialog.getCustomView()
        val textView = content.findViewById<TextView>(R.id.text)
        textView.visibility = View.VISIBLE
        textView.text = text

        return dialog
    }

    fun getImageWithInputDialog(context: Context, uriString: String): MaterialDialog {
        val dialog = CustomDialog[context].customView(R.layout.dialog_with_image_and_input_view)

//        val dialog = builder.build()
        val content = dialog.getCustomView()
        val preview = content.findViewById<SimpleDraweeView>(R.id.preview)
        preview.setImageURI(uriString)

        return dialog
    }

    fun getImageSelectorDialog(context: Context, listener: OnImageDialogSelect): MaterialDialog {
        val items = listOf(
                context.getString(R.string.common_camera),
                context.getString(R.string.common_gallery)
        )

        return MaterialDialog(context)
                .title(R.string.common_attach)
                .listItems(items = items, waitForPositiveButton = false) { dialog, index, text ->
                    if (index == Constants.CAMERA_REQUEST) {
                        val cameraPerm = arrayOf(Manifest.permission.CAMERA)
                        if (EasyPermissions.hasPermissions(context, *cameraPerm)) {
                            listener.onCamera()
                        } else {
                            listener.onNoPermission(Constants.CAMERA_REQUEST, cameraPerm)
                        }
                    } else if (index == Constants.GALLERY_REQUEST) {
                        val storagePerm = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE)
                        if (EasyPermissions.hasPermissions(context, *storagePerm)) {
                            listener.onGallery()
                        } else {
                            listener.onNoPermission(Constants.GALLERY_REQUEST, storagePerm)
                        }
                    }

                    dialog.dismiss()
                }
    }

    interface OnImageDialogSelect {
        fun onCamera()
        fun onGallery()
        fun onNoPermission(requestCode: Int, perm: Array<String>)
    }
}
