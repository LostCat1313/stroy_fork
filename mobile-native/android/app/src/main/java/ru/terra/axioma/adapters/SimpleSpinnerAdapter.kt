package ru.terra.axioma.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import ru.terra.axioma.models.SimpleType
import android.content.Context


class SimpleSpinnerAdapter(context: Context, val objects: List<SimpleType>, strings: MutableList<String>) :
        ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, 0, strings) {

    var selectedId: Int? = null
    private var inflater: LayoutInflater = LayoutInflater.from(context)

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        return getView(position, convertView, parent)
    }

    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false)

        view.findViewById<TextView>(android.R.id.text1)?.apply{
            text = objects[position].name
        }

        return view
    }

    override fun getCount(): Int {
        return objects.size
    }

    override fun getItemId(position: Int): Long {
        selectedId = objects[position].id
        return selectedId?.toLong() ?: 0
    }
}
