package ru.terra.axioma.views.imageeditor.text;

import android.graphics.Bitmap;
import android.graphics.PointF;

public class TextDrawingObject {
    public String text;
    public PointF pointRaw;
    public PointF point;
    public Bitmap bitmap;

    TextDrawingObject(String text, PointF pointRaw, Bitmap bitmap) {
        this.text = text;
        this.pointRaw = pointRaw;
        this.bitmap = bitmap;
    }
}
