package ru.terra.axioma.store

import ru.terra.axioma.models.Project
import ru.terra.axioma.models.User

class CurrentSessionStore {
    var user = User()
    var token = ""
    var regId = ""
    var project: Project? = null

    var isActive: Boolean = false
        get() { return user.isActive && !user.firstName.isNullOrEmpty() && !user.lastName.isNullOrEmpty() }
        private set
}