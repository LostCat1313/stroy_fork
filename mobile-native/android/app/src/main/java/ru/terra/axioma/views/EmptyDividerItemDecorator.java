package ru.terra.axioma.views;

import android.content.Context;
import android.graphics.Rect;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import ru.terra.axioma.R;

/**
 * Created by alexei on 20.01.17.
 */

public class EmptyDividerItemDecorator extends RecyclerView.ItemDecoration {
    private int height;

    public EmptyDividerItemDecorator(Context context) {
        this.height = context.getResources().getDimensionPixelSize(R.dimen.activity_horizontal_margin);
    }

//    @Override
//    public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
//        int childCount = parent.getChildCount();
//        for (int i = 0; i < childCount; i++) {
//            View child = parent.getChildAt(i);
//            int top = i == 0 ? height : height / 2;
//            int bottom = i == childCount - 1 ? height : height / 2;
////            child.setPaddingRelative(0, top, 0, bottom);
//
//            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) child.getLayoutParams();
//            marginLayoutParams.topMargin = top;
//            marginLayoutParams.bottomMargin = bottom;
//        }
//    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++) {
            int top = i == 0 ? height : height / 2;
            int bottom = i == childCount - 1 ? height : height / 2;
            outRect.set(0, top, 0, bottom);
        }
    }
}
