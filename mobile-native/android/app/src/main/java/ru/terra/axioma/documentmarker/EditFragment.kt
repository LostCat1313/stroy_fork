package ru.terra.axioma.documentmarker

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.google.common.collect.Lists
import kotlinx.android.synthetic.main.editor_button_block.view.*
import kotlinx.android.synthetic.main.fragment_edit.*
import org.koin.android.ext.android.getKoin
import ru.terra.axioma.DocumentUtils
import ru.terra.axioma.adapters.PageVersionsAdapter
import ru.terra.axioma.interfaces.OnEditorInteraction
import ru.terra.axioma.models.Document
import ru.terra.axioma.models.ProjectFile
import ru.terra.axioma.models.Version
import ru.terra.axioma.models.marker.Marker
import ru.terra.axioma.repository.DocumentsRepository
import ru.terra.axioma.R

class EditFragment : EditorBaseFragment() {
    private var mListener: OnEditorInteraction? = null

    private lateinit var currentDocument: Document
    private lateinit var currentFile: ProjectFile
    private lateinit var adapter: PageVersionsAdapter
    private var fileToLink: ProjectFile? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_edit, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        makeIcon(markerImage, R.color.red, R.drawable.ic_content_cut)
        buttons.ok.apply {
            isEnabled = false
            setOnClickListener { onOkClicked() }
        }
        buttons.cancel.setOnClickListener { onCancelClicked() }

        setupList()
    }

    override fun onSaveSuccess(marker: Marker) {
        mListener?.onOk(marker)
    }

    override fun onSaveError(marker: Marker) {
        mListener?.onOk(marker)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnEditorInteraction) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement OnEditorInteraction")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    private fun onOkClicked() {
        marker.text = comment.text.toString()
        marker.setLink(currentDocument, fileToLink)
        super.save()
    }

    private fun onCancelClicked() {
        mListener?.onCancel()
    }

    private fun setupList() {
        val repository: DocumentsRepository = getKoin().get()

        currentDocument = DocumentUtils.findDocument(repository.documents, marker.docId) ?: return
        currentFile = DocumentUtils.findFile(currentDocument, marker.fileId) ?: return
        val version = DocumentUtils.findVersion(currentDocument, currentFile) ?: return

        val last = Lists.newArrayList<Version>()
        val old = Lists.newArrayList<Version>()
        for (v in currentDocument.versions) {
            val ver = v.getPageVersion(currentFile)
            if (ver != null && v.id != version.id)
                if (v.createdAt.before(version.createdAt)) {
                    old.add(0, v)
                } else {
                    last.add(0, v)
                }
        }

        val result = Lists.newArrayList(last)
        if (!result.isEmpty())
            result.add(0, Version(getString(R.string.editor_newer)))

        if (!old.isEmpty()) {
            result.add(Version(getString(R.string.editor_older)))
            result.addAll(old)
        }

        adapter = PageVersionsAdapter().apply {
            setData(result)
            recyclerView.adapter = this

            selectedVersion.observe(this@EditFragment, Observer {
                fileToLink = it?.getPageVersion(currentFile)
                buttons.ok.isEnabled = it != null
            })
        }
    }

    companion object {

        fun newInstance(): EditFragment {
            return EditFragment()
        }
    }
}
