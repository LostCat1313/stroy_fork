package ru.terra.axioma.di

import io.socket.client.IO
import io.socket.client.Socket
import org.koin.dsl.module
import ru.terra.axioma.Configuration
import ru.terra.axioma.store.CurrentSessionStore
import java.net.URISyntaxException

val socketModule = module {
    single { provideSocket(get()) }
}

fun provideSocket(currentSessionStore: CurrentSessionStore): Socket? {
    return try {
        val opts = IO.Options()
        opts.query = "token=" + currentSessionStore.token
        IO.socket(Configuration.baseUrl, opts)
    } catch (e: URISyntaxException) {
        e.printStackTrace()
        null
    }
}