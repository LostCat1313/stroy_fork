package ru.terra.axioma.main

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.ImageButton

import com.google.android.material.navigation.NavigationView
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.lifecycle.Observer
import androidx.viewpager.widget.ViewPager
import com.afollestad.materialdialogs.MaterialDialog
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.nav_header_main.view.*
import nl.komponents.kovenant.task
import org.koin.android.ext.android.getKoin
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import ru.terra.axioma.App
import ru.terra.axioma.BuildConfig
import ru.terra.axioma.login.LoginActivity
import ru.terra.axioma.R
import ru.terra.axioma.adapters.MainViewPagerAdapter
import ru.terra.axioma.chat_v2.ChatActivity
import ru.terra.axioma.client.ClientApi
import ru.terra.axioma.contacts.ContactsActivity
import ru.terra.axioma.documentstructure.DocumentListActivity
import ru.terra.axioma.models.Document
import ru.terra.axioma.models.Notification
import ru.terra.axioma.notes.NotesActivity
import ru.terra.axioma.personalnotes.PersonalNotesActivity
import ru.terra.axioma.projects.ProjectsActivity
import ru.terra.axioma.repository.ProjectNotesRepository
import ru.terra.axioma.store.CurrentSessionStore
import ru.terra.axioma.store.UserStore
import ru.terra.axioma.views.CounterView
import ru.terra.axioma.views.CustomDialog
import timber.log.Timber

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, MainNavigator {

    private var tab1: Button? = null
    private var tab2: Button? = null
    private var filter: ImageButton? = null
    private var counterDocuments: CounterView? = null
    private var counterNotes: CounterView? = null
    private var counterChats: CounterView? = null
    private var counterProjects: CounterView? = null
    private var navDocuments: MenuItem? = null
    private var navNotes: MenuItem? = null

    private var activityToShow: Class<*>? = null
    private val broadcastReceiver: BroadcastReceiver? = null

    private lateinit var loadingDialog: MaterialDialog

    private val currentSessionStore: CurrentSessionStore by inject()
    private val notesRepository: ProjectNotesRepository by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val vm: MainViewModel by viewModel { parametersOf(this) }

        setSupportActionBar(toolbar)

        ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close).let {
            drawerLayout.addDrawerListener(it)
            it.syncState()
        }

        navView.setNavigationItemSelectedListener(this)

        setupDrawerComponents()
        setupDrawerValues()
        setupViewPager()
        setupActionBar()

        intent.extras?.run {
            for (key in keySet())
                Timber.tag("MAIN ACTIVITY").d("Key: $key Value: %s", get(key))
        }

        notesRepository.data.observe(this, Observer { markers ->
            if (markers != null && counterNotes != null)
                counterNotes!!.setTextAndBackground(markers.size.toString(), R.drawable.rounded_rect_lime)
        })

        version.text = getString(R.string.version, BuildConfig.VERSION_NAME)

        loadingDialog = CustomDialog.getLoadingDialog(this)
                .message(R.string.common_loading)

        vm.showLoading.observe(this, Observer {
            if (it) loadingDialog.show()
            else loadingDialog.dismiss()
        })
    }

    override fun onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onResume() {
        super.onResume()
        setupDrawerValues()

        /*broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Toast.makeText(getApplicationContext(), "Edtljvktybt", Toast.LENGTH_LONG).show();
            }
        };

        try {
            LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, new IntentFilter(Constants.UPDATE_NOTIFICATION_LIST));
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }

    //    @Override
    //    protected void onPause() {
    //        super.onPause();
    //        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
    //    }

    override fun onStart() {
        super.onStart()
        isRunning = true
    }

    override fun onStop() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START))
            drawerLayout.closeDrawer(GravityCompat.START, false)

        super.onStop()
        isRunning = false
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == R.id.nav_projects) {
            activityToShow = ProjectsActivity::class.java
        } else if (id == R.id.nav_contacts) {
            activityToShow = ContactsActivity::class.java
        } else if (id == R.id.nav_documents) {
            activityToShow = DocumentListActivity::class.java
        } else if (id == R.id.nav_notes) {
            activityToShow = NotesActivity::class.java
        } else if (id == R.id.nav_personal_notes) {
            activityToShow = PersonalNotesActivity::class.java
        } else if (id == R.id.nav_chats) {
            activityToShow = ChatActivity::class.java
        } else if (id == R.id.nav_exit) {
            val editor = getSharedPreferences(App.PREFS_NAME, Context.MODE_PRIVATE).edit()
            editor.remove("token")
            editor.apply()
            currentSessionStore.project = null
            logout()
            activityToShow = LoginActivity::class.java
        } else {
            drawerLayout.closeDrawer(GravityCompat.START)
        }

        if (activityToShow != null) {
            val intent = Intent(this@MainActivity, activityToShow)
            startActivity(intent)
            activityToShow = null
        }

        return true
    }

    private fun setupDrawerComponents() {
        val navProjects = navView.menu.findItem(R.id.nav_projects)
        counterProjects = navProjects.actionView as CounterView

        navDocuments = navView.menu.findItem(R.id.nav_documents)
        counterDocuments = navDocuments!!.actionView as CounterView

        navNotes = navView.menu.findItem(R.id.nav_notes)
        counterNotes = navNotes!!.actionView as CounterView

        val navChats = navView.menu.findItem(R.id.nav_chats)
        counterChats = navChats.actionView as CounterView
    }

    private fun setupDrawerValues() {
        UserStore.user.let {
            navView.getHeaderView(0).username.text = it.fullName
        }

        currentSessionStore.project?.let {
            navDocuments?.isEnabled = true
            counterDocuments?.apply {
                visibility = View.VISIBLE
                setTextAndBackground(it.documentsCount.toString(), R.drawable.rounded_rect_orange)
            }
            navNotes?.isEnabled = true
            counterNotes?.visibility = View.VISIBLE

            navView.getHeaderView(0).projectName.text = it.title
        } ?: run {
            navDocuments?.isEnabled = false
            counterDocuments?.visibility = View.INVISIBLE

            navNotes?.isEnabled = false
            counterNotes?.visibility = View.INVISIBLE
        }
    }

    private fun setupActionBar() {
        val actionBar = supportActionBar ?: return

        actionBar.setDisplayShowCustomEnabled(true)
        actionBar.setDisplayHomeAsUpEnabled(false)
        actionBar.setCustomView(R.layout.toolbar_tabs)
        actionBar.displayOptions = ActionBar.DISPLAY_SHOW_HOME or ActionBar.DISPLAY_SHOW_CUSTOM
        (actionBar.customView.parent as Toolbar).contentInsetStartWithNavigation = 0

        tab1 = actionBar.customView.findViewById(R.id.tab1)
        tab2 = actionBar.customView.findViewById(R.id.tab2)
        setupTabs()

        filter = actionBar.customView.findViewById<ImageButton>(R.id.filter).apply {
            setOnClickListener {
                val f = supportFragmentManager.fragments[viewPager.currentItem]
                if (viewPager.currentItem == 0)
                    (f as NotificationsFragment).showFilterMenu()
            }
        }

    }

    private fun setupViewPager() {
        val viewPagerAdapter = MainViewPagerAdapter(supportFragmentManager)
        viewPager.adapter = viewPagerAdapter

        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                tab1!!.isSelected = position == 0
                tab2!!.isSelected = position == 1
                filter!!.visibility = if (position == 0) View.VISIBLE else View.INVISIBLE
            }

            override fun onPageScrollStateChanged(state: Int) {

            }
        })
    }

    private fun setupTabs() {
        tab1?.apply {
            isSelected = viewPager.currentItem == 0
            setOnClickListener { viewPager.currentItem = 0 }
        }

        tab2?.apply {
            isSelected = viewPager.currentItem == 1
            setOnClickListener { viewPager.currentItem = 1 }
        }
    }

    private fun logout() {
        val client: ClientApi = getKoin().get()
        task {
            client.logout()
        }
    }


    override fun openDocument(documents: List<Document>, docId: Int) {
        MainActivityHelper.openDocument(this, documents, docId)
    }

    override fun openDocument(documents: List<Document>, notification: Notification) {
        MainActivityHelper.openDocument(this, documents, notification)
    }


    companion object {
        var isRunning: Boolean = false
    }
}
