package ru.terra.axioma.views

import android.content.Context
import android.net.Uri
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.facebook.drawee.view.SimpleDraweeView
import ru.terra.axioma.Configuration
import ru.terra.axioma.R
import ru.terra.axioma.models.ProjectFile
import ru.terra.axioma.models.chat.Message

abstract class MessageBaseView(context: Context) : ConstraintLayout(context) {

    abstract fun setMessage(msg: Message, onAttachmentClick: (ProjectFile) -> Unit = {})

    internal fun getAttachmentView(file: ProjectFile, onClick: (ProjectFile) -> Unit = {}): SimpleDraweeView {
        val progress = CircleProgressBarDrawable().apply {
            backgroundColor = ContextCompat.getColor(context, R.color.grey)
            color = ContextCompat.getColor(context, R.color.grey_600)
        }

        return SimpleDraweeView(context).apply {
            aspectRatio = 0.7f
            val thumbUri = Uri.parse(Configuration.baseUrl + file.thumbUrl)
            setImageURI(thumbUri, context)
            hierarchy.setProgressBarImage(progress)
            setOnClickListener {
                onClick(file)
            }
        }
    }
}