package ru.terra.axioma.main

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import nl.komponents.kovenant.then
import nl.komponents.kovenant.ui.alwaysUi

import ru.terra.axioma.client.ClientApi
import ru.terra.axioma.models.Document
import ru.terra.axioma.models.Notification
import ru.terra.axioma.repository.DocumentsRepository
import ru.terra.axioma.repository.NotificationsRepository
import ru.terra.axioma.repository.RecentDocumentsRepository

class MainViewModel(
        application: Application,
        val client: ClientApi,
        private val navigator: MainNavigator,
        private val recentDocumentsRepository: RecentDocumentsRepository,
        private val notificationsRepository: NotificationsRepository,
        private val documentsRepository: DocumentsRepository
) : AndroidViewModel(application) {

    val showLoading = MutableLiveData<Boolean>()
    val notificationFilterBy: MutableLiveData<List<FilterBy>> = MutableLiveData()

    val notifications: MutableLiveData<MutableList<Notification>>
        get() { return notificationsRepository.notifications }

    val docs: MutableLiveData<List<Document>>
        get() = recentDocumentsRepository.docs

    init {
        notificationFilterBy.value = arrayListOf(FilterBy.Docs, FilterBy.Messages, FilterBy.Notes)

        recentDocumentsRepository.fetch()
        notificationsRepository.fetch(0)
    }

    fun loadPage(page: Int = 0) {
        notificationsRepository.fetch(page)
    }

    fun loadNotifications() {
        notificationsRepository.fetch()
    }

    fun onSameFunction(doc: Document) {
        navigator.openDocument(documentsRepository.documents, doc.id)
    }

    fun onSameFunction(notification: Notification) {
        navigator.openDocument(documentsRepository.documents, notification)
    }

    fun onDifferentFunction(doc: Document) {
        showLoading.value = true
        documentsRepository.fetch(doc.projectId) alwaysUi {
            showLoading.value = false
        } then { docs ->
            navigator.openDocument(docs, doc.id)
        }
    }

    fun onDifferentFunction(notification: Notification) {
        showLoading.value = true
        documentsRepository.fetch(notification.projectId) alwaysUi {
            showLoading.value = false
        } then { docs ->
            navigator.openDocument(docs, notification)
        }
    }

    fun updateFilter(filter: FilterBy, value: Boolean) {
        val new = notificationFilterBy.value?.run {
            if (indexOf(filter) == -1)
                plus(filter)
            else
                minus(filter)
        }
        notificationFilterBy.value = new
    }

    fun getFilterState(filter: FilterBy) : Boolean {
        return notificationFilterBy.value?.contains(filter) ?: false
    }

    fun refreshNotifications() {
        notificationsRepository.fetch(0)
    }
}
