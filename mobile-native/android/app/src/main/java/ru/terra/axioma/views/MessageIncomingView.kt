package ru.terra.axioma.views

import android.content.Context
import android.view.LayoutInflater
import com.amulyakhare.textdrawable.TextDrawable
import com.amulyakhare.textdrawable.util.ColorGenerator
import kotlinx.android.synthetic.main.message_incoming_view.view.*
import ru.terra.axioma.R
import ru.terra.axioma.StringUtils
import ru.terra.axioma.models.ProjectFile
import ru.terra.axioma.models.chat.Message

class MessageIncomingView(context: Context) : MessageBaseView(context) {

    init {
        LayoutInflater.from(context).inflate(R.layout.message_incoming_view, this, true)
    }

    override fun setMessage(msg: Message, onAttachmentClick: (ProjectFile) -> Unit) {
        message.text = msg.content
        time.text = msg.messageTime

        msg.attachments?.let {
            if (it.isEmpty())
                attachments.removeAllViews()

            msg.attachments?.forEach {
                attachments.addView(getAttachmentView(it.file, onAttachmentClick))
            }
        } ?: run {
            attachments.removeAllViews()
        }

        (msg.user?.fullName ?: "Неизвестный").let {
            sender.text = it
            val drawable = TextDrawable.builder()
                    .buildRound(StringUtils.getFirstLetters(it),  ColorGenerator.MATERIAL.getColor(it))
            icon.setImageDrawable(drawable)
        }
    }
}