package ru.terra.axioma.chat_v2.adapters

import androidx.recyclerview.widget.RecyclerView
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView

import com.amulyakhare.textdrawable.TextDrawable
import com.amulyakhare.textdrawable.util.ColorGenerator
import com.google.common.collect.Lists

import ru.terra.axioma.R
import ru.terra.axioma.StringUtils
import ru.terra.axioma.autoNotify
import ru.terra.axioma.models.chat.Message
import ru.terra.axioma.models.chat.Room
import kotlin.properties.Delegates

class ChatsAdapter(val onClick: (Room) -> Unit) : RecyclerView.Adapter<ChatsAdapter.ViewHolder>() {

    var items: ArrayList<Room> by Delegates.observable(arrayListOf()) { _, oldValue, newValue ->
        autoNotify(oldValue, newValue) { o, n -> o.id == n.id }
    }

    inner class ViewHolder internal constructor(view: View) : RecyclerView.ViewHolder(view) {
        val avatar: ImageView = view.findViewById(R.id.avatar)
        val groupIcon: ImageView = view.findViewById(R.id.groupIcon)
        val title: TextView = view.findViewById(R.id.title)
        val subtitle: TextView = view.findViewById(R.id.subtitle)
        val time: TextView = view.findViewById(R.id.time)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.rooms_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        items[position].let { obj ->
            holder.groupIcon.visibility = if (obj.isPrivate) View.GONE else View.VISIBLE
            holder.title.text = obj.name

            obj.lastMessage?.let {
                holder.subtitle.text = if (obj.isPrivate) obj.lastMessage.content else Html.fromHtml(obj.lastMessagePreview)
                holder.time.text = StringUtils.dateToTimeString(obj.lastMessage.updatedAt)
            } ?: run {
                holder.subtitle.text = holder.itemView.context.getString(R.string.chat_no_messages)
                holder.time.text = ""
            }

            val name = if (obj.name == null) "Без названия" else obj.name

            val drawable = TextDrawable.builder()
                    .buildRound(StringUtils.getFirstLetters(name).toUpperCase(), ColorGenerator.MATERIAL.getColor(name))
            holder.avatar.setImageDrawable(drawable)

            holder.itemView.setOnClickListener { onClick(obj) }
        }
    }

    override fun getItemCount(): Int {
        return items.size ?: 0
    }
}
