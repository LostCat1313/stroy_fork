package ru.terra.axioma.models;

import java.util.Date;

/**
 * Created by alexei on 19.02.18.
 */

public class BaseObject {
    public int id;
    public Date createdAt;
    public Date updatedAt;
    public int createdBy;
    public int modifiedBy;
}
