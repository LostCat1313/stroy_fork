package ru.terra.axioma

class LoginException(private val code: Int? = 0): Exception() {
    override fun toString(): String {
        return  when (code) {
            404 -> "Ошибка авторизации"
            else -> "Ошибка сервера"
        }
    }
}