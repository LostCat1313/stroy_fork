package ru.terra.axioma.models.marker;

import java.util.List;

public class NoteParams extends LabelBaseParams {
    public boolean isDraft;
    public List<Integer> attachments;

    public NoteParams() {
        isDraft = true;
    }
}
