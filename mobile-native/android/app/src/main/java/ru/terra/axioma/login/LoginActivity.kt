package ru.terra.axioma.login

import android.content.BroadcastReceiver
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import kotlinx.android.synthetic.main.activity_login.*
import org.koin.android.viewmodel.ext.android.viewModel
import ru.terra.axioma.Constants
import ru.terra.axioma.R
import ru.terra.axioma.main.MainActivity
import ru.terra.axioma.registration.RegistrationActivity

class LoginActivity : AppCompatActivity(), OnFragmentInteractionListener {

    private lateinit var broadcastReceiver: BroadcastReceiver
    val viewModel: LoginViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        broadcastReceiver = viewModel.getBroadcastReceiver()

        viewModel.showNextScreen.observe(this, Observer {
            if (it) goNext()
        })
        viewModel.showRegistrationScreen.observe(this, Observer {
            if (it) showRegistration()
        })

        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, IntentFilter(Constants.FILTER_ACTIVATION_CODE))

        showPlainLogin()
    }

    override fun onDestroy() {
        super.onDestroy()
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver)
    }

    override fun onBackPressed() {
        if (viewModel.showActivateScreen.value != false)
            viewModel.showActivateScreen.value = false
        else
            super.onBackPressed()
    }

    override fun goNext() {
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }

    override fun showRegistration() {
        startActivity(Intent(this, RegistrationActivity::class.java))
        finish()
    }

    override fun showPlainLogin() {
        supportFragmentManager.beginTransaction().replace(container.id, PlainLoginFragment.newInstance()).commit()
    }

    override fun showSmsLogin() {
        supportFragmentManager.beginTransaction().replace(container.id, SmsLoginFragment()).commit()
    }

    companion object {
        const val ACTIVATION_CODE = "code"
    }
}