package ru.terra.axioma.documentmarker

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.core.content.FileProvider
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.list.listItems
import com.google.common.collect.Collections2
import kotlinx.android.synthetic.main.editor_button_block.view.*
import kotlinx.android.synthetic.main.fragment_note.*
import okhttp3.MultipartBody
import okhttp3.RequestBody
import pub.devrel.easypermissions.AppSettingsDialog
import pub.devrel.easypermissions.EasyPermissions
import pub.devrel.easypermissions.PermissionRequest
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import ru.terra.axioma.Constants
import ru.terra.axioma.FileUtils
import ru.terra.axioma.ImageViewerActivity
import ru.terra.axioma.R
import ru.terra.axioma.adapters.AttachPhotoAdapter
import ru.terra.axioma.interfaces.OnEditorInteraction
import ru.terra.axioma.models.FileEntry
import ru.terra.axioma.models.UploadResponse
import ru.terra.axioma.models.marker.Marker
import ru.terra.axioma.models.marker.NoteParams
import ru.terra.axioma.views.CustomDialog
import ru.terra.axioma.views.GridAutoFitLayoutManager
import ru.terra.axioma.views.GridSpacingItemDecorator
import timber.log.Timber
import java.io.File
import java.io.IOException
import java.util.*

/**
 * Created by alexei on 04.01.18.
 */

class NoteFragment : EditorBaseFragment(), EasyPermissions.PermissionCallbacks {
    private var mListener: OnEditorInteraction? = null

    private lateinit var adapter: AttachPhotoAdapter
    private var mCurrentImageUri: Uri? = null

    private var params: NoteParams? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view = inflater.inflate(R.layout.fragment_note, container, false)
        params = marker.params as NoteParams
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        makeIcon(markerImage, R.color.purple, R.drawable.ic_star)

        buttons.ok.setOnClickListener { onOk() }
        buttons.cancel.setOnClickListener { mListener!!.onCancel() }

        val columns = 3
        val layoutManager = GridAutoFitLayoutManager(context, columns)
        recyclerView.layoutManager = layoutManager
        recyclerView.addItemDecoration(GridSpacingItemDecorator(columns, 40, false))

        adapter = AttachPhotoAdapter(object : AttachPhotoAdapter.OnClickListener {
            override fun onAddFileClick() {
                selectImage()
            }

            override fun onAttachmentClick(uri: Uri) {
                Timber.d("Image")
                val intent = Intent(context, ImageViewerActivity::class.java)
                intent.putExtra(ImageViewerActivity.URI, uri)
                startActivity(intent)
            }
        })
        recyclerView.adapter = adapter



        if (marker.id != 0) {
            if (params!!.attachments == null || params!!.attachments.isEmpty())
                attachmentLabel.visibility = View.INVISIBLE
            else
                loadAttachments()

            text.setText(marker.text)
            text.isClickable = params!!.isDraft
            text.isCursorVisible = params!!.isDraft
            text.isFocusable = params!!.isDraft
            adapter.setEditable(params!!.isDraft)
        }
    }

    override fun onSaveSuccess(marker: Marker) {
        mListener?.onOk(marker)
    }

    override fun onSaveError(marker: Marker) {
        mListener?.onOk(marker)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnEditorInteraction) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement OnEditorInteraction")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    /**
     * Загрузка картинки
     */

    private fun cameraIntent() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)

        // Ensure that there's a camera activity to handle the intent
        if (intent.resolveActivity(requireContext().packageManager) != null) {
            // Create the ProjectFile where the photo should go
            var photoFile: File? = null
            try {
                photoFile = FileUtils.createTempImageFile(context, false)
            } catch (ex: IOException) {
                // Error occurred while creating the ProjectFile
            }

            // Continue only if the ProjectFile was successfully createdAt
            if (photoFile != null) {
                mCurrentImageUri = Uri.fromFile(photoFile)

                val photoURI = FileProvider.getUriForFile(requireContext(),
                        "ru.terra.axioma.fileprovider",
                        photoFile
                )

                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                requireActivity().startActivityForResult(intent, Constants.CAMERA_REQUEST)
            }
        }
    }

    private fun galleryIntent() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        requireActivity().startActivityForResult(Intent.createChooser(intent, getString(R.string.common_select_file)), Constants.GALLERY_REQUEST)
    }

    private fun selectImage() {
        CustomDialog.getImageSelectorDialog(requireContext(), object : CustomDialog.OnImageDialogSelect {
            override fun onNoPermission(requestCode: Int, perm: Array<String>) {
                EasyPermissions.requestPermissions(
                        PermissionRequest.Builder(this@NoteFragment, requestCode, *perm)
                                .setRationale(R.string.card_message)
                                .setPositiveButtonText(R.string.editor_ok)
                                .setNegativeButtonText(R.string.common_cancel)
                                .build())
            }

            override fun onCamera() {
                cameraIntent()
            }

            override fun onGallery() {
                galleryIntent()
            }
        }).show()
    }

    private fun onCaptureImageResult() {
        if (mCurrentImageUri != null)
            adapter.addValue(mCurrentImageUri)

        mCurrentImageUri = null
    }

    private fun onSelectFromGalleryResult(data: Intent) {
        val path = FileUtils.getPath(context, data.data)
        val f = File(path)
        adapter.addValue(Uri.fromFile(f))
    }

    override fun onPermissionsGranted(requestCode: Int, @NonNull perms: List<String>) {
        if (requestCode == Constants.CAMERA_REQUEST)
            cameraIntent()
        else if (requestCode == Constants.GALLERY_REQUEST)
            galleryIntent()
    }

    override fun onPermissionsDenied(requestCode: Int, @NonNull perms: List<String>) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms))
            AppSettingsDialog.Builder(this).build().show()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == Constants.GALLERY_REQUEST)
                onSelectFromGalleryResult(data!!)
            else if (requestCode == Constants.CAMERA_REQUEST)
                onCaptureImageResult()
        }
    }


    /**
     * Сохранение данных на сервер
     */

    private fun loadAttachments() {
        for (id in params!!.attachments) {
            client.getFile(id).enqueue(object : Callback<FileEntry> {
                override fun onResponse(@NonNull call: Call<FileEntry>, @NonNull response: Response<FileEntry>) {
                    val result = response.body()
                    if (result != null)
                        adapter.addValue(Uri.parse(result.entry.url))
                    // TODO обработать ошибку
                }

                override fun onFailure(@NonNull call: Call<FileEntry>, @NonNull t: Throwable) {
                    Timber.d("Error")
                    // TODO обработать ошибку
                }
            })
        }
    }

    // Загрузка файла на сервер
    private fun uploadFiles(attachments: List<Uri>?) {
        if (attachments == null || attachments.isEmpty()) {
            super.save()
            return
        }

        val description = RequestBody.create(MultipartBody.FORM, "description")

        val attachIds = ArrayList<Int>()
        val count = intArrayOf(attachments.size)

        for (uri in attachments) {
            val body = FileUtils.getFilePart("attachment", uri)

            client.uploadFile(description, body).enqueue(object : Callback<UploadResponse> {
                override fun onResponse(call: Call<UploadResponse>, response: Response<UploadResponse>) {
                    Timber.v("success")

                    val result = response.body()

                    if (result != null)
                        attachIds.add(result.files[0].id)

                    count[0]--
                    if (count[0] == 0) {
                        if (params!!.attachments == null)
                            params!!.attachments = ArrayList()

                        params!!.attachments.addAll(attachIds)
                        super@NoteFragment.save()
                    }
                }

                override fun onFailure(call: Call<UploadResponse>, t: Throwable) {
                    Timber.e(t)
                    // TODO обработать

                    count[0]--
                    if (count[0] == 0) {
                        if (params!!.attachments == null)
                            params!!.attachments = ArrayList()

                        params!!.attachments.addAll(attachIds)
                        super@NoteFragment.save()
                    }
                }
            })
        }
    }


    /**
     * Кнопки
     */

    private fun onOk() {
        val params = marker.params as NoteParams
        if (marker.id == 0 || params.isDraft) {
            val items = kotlin.collections.listOf(
                    getString(R.string.editor_save_as_draft),
                    getString(R.string.editor_save)
            )

            MaterialDialog(context!!)
                    .title(R.string.editor_save_as)
                    .listItems(items = items, waitForPositiveButton = false) { dialog, index, _ ->
                        params.isDraft = index == 0
                        marker.text = text.text.toString()
                        val attachments = adapter.data
                        if (attachments.isEmpty())
                            super.save()
                        else {
                            val newAttach = ArrayList(Collections2.filter(attachments) { it!!.scheme != null })

                            uploadFiles(newAttach)
                        }
                        dialog.dismiss()
                    }
                    .negativeButton(R.string.editor_cancel)
                    .show()
        } else {
            mListener?.onOk()
        }
    }

    companion object {

        fun newInstance(): NoteFragment {
            return NoteFragment()
        }
    }
}
