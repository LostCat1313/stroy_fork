package ru.terra.axioma.views.imageeditor

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.os.Build
import android.text.Layout
import android.text.StaticLayout
import android.text.TextPaint
import android.util.AttributeSet
import android.util.SparseArray
import android.view.MotionEvent
import android.view.View
import android.view.ViewConfiguration
import androidx.annotation.NonNull
import androidx.core.content.ContextCompat
import androidx.core.util.forEach
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView
import nl.komponents.kovenant.task
import nl.komponents.kovenant.ui.alwaysUi
import ru.terra.axioma.MarkerIcons
import ru.terra.axioma.R
import ru.terra.axioma.models.marker.*
import ru.terra.axioma.models.marker.Marker.*
import java.util.*


internal class ScaleImageView @JvmOverloads constructor(
        context: Context,
        attr: AttributeSet? = null
) : SubsamplingScaleImageView(context, attr), View.OnTouchListener {

    private var onMarkerInteraction: OnMarkerListener? = null
    private var sharing: Boolean = false

    private var isLabelMode: Boolean = false
    private var type: Int = 0

    private var clickRects: SparseArray<Rect> = SparseArray()
    private var markers: SparseArray<Marker> = SparseArray()


    private var devPaint: Paint = Paint().apply {
        isAntiAlias = true
        color = Color.BLACK
    }

    private var labelPaint: Paint = Paint().apply {
        isAntiAlias = true
    }

    private var labelTextPaint: TextPaint = TextPaint().apply {
        textSize = resources.getDimensionPixelSize(R.dimen.marker_link_text_size).toFloat()
        color = Color.WHITE
    }

    private var drawingPaint: Paint = Paint().apply {
        isAntiAlias = true
        style = Paint.Style.STROKE
        strokeCap = Paint.Cap.ROUND
    }

    private var textPaint: TextPaint = TextPaint()


    private var clickRect = Rect()

    private var clickStartTime: Long = 0

    private var labelParams: LabelBaseParams? = null

    private var markerToHighlight = 0

    var initScale = -1f


    val sharePicture: Bitmap
        get() {
            sharing = true
            invalidate()

            val result = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565)
            val c = Canvas(result)
            c.drawColor(ContextCompat.getColor(context, android.R.color.white))
            draw(c)

            sharing = false
            invalidate()

            return result
        }

    init {
        setOnTouchListener(this)
    }

    override fun onTouch(view: View, motionEvent: MotionEvent): Boolean {
        return false
    }


    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(@NonNull event: MotionEvent): Boolean {
        when (event.action) {
            MotionEvent.ACTION_DOWN -> onActionDown(event)
            MotionEvent.ACTION_UP -> onActionUp(event)
        }

        // Use parent to handle pinch and two-finger pan.
        return super.onTouchEvent(event)
    }

    private fun onActionDown(@NonNull event: MotionEvent) {
        clickStartTime = Calendar.getInstance().timeInMillis
    }

    private fun onActionUp(event: MotionEvent) {
        if (Calendar.getInstance().timeInMillis - clickStartTime < ViewConfiguration.getTapTimeout())
            click(event)
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        // Don't draw addPin before image is ready so it doesn't move around during setup.
        if (!isReady) {
            return
        }

        if (initScale < 0)
            initScale = scale

        markers.forEach { _, m ->
            m.params?.let {
                when (m.type) {
                    LINE -> drawLine(canvas, m.params as DrawParams)
                    RECT -> drawRect(canvas, m.params as DrawParams)
                    TEXT -> drawText(canvas, m.text, m.params as TextParams)
                    else -> if (!sharing) drawLabel(canvas, m)
                }
            }
        }
    }

    private fun drawLabel(canvas: Canvas, m: Marker) {
        val hide = isLabelMode || (markerToHighlight != 0 && markerToHighlight != m.id)

        labelPaint.alpha = if (hide) 128 else 255

        val bitmap = MarkerIcons.get(MarkerIcons.getType(m))

        val p = m.params as LabelBaseParams
        val point = sourceToViewCoord(p.x.toFloat(), p.y.toFloat()) ?: return
        val textOffset = 10

        val basePointX = point.x - bitmap.width / 2
        val basePointY = point.y - bitmap.height
        canvas.drawBitmap(bitmap, basePointX, basePointY, labelPaint)

        clickRect.set(basePointX.toInt(), basePointY.toInt(), basePointX.toInt() + bitmap.width, basePointY.toInt() + bitmap.height)

        if (m.type == LINK) {
            val text = if (m.text == null) "ССЫЛКА" else m.text

            val widths = FloatArray(text.length)
            labelTextPaint.getTextWidths(text, widths)

            val x = basePointX + bitmap.width.toFloat() + 5f
            val h = bitmap.height * 0.7f + 5
            val w = widths.sum() + textOffset * 2
            val ty = h / 2 + labelTextPaint.textSize / 3

            labelPaint.color = ContextCompat.getColor(context, R.color.teal)
            labelPaint.alpha = if (hide) 128 else 255
            canvas.drawRoundRect(x, basePointY, x + w, basePointY + h, 7f, 7f, labelPaint)

            labelTextPaint.alpha = if (hide) 128 else 255
            canvas.drawText(text, x + textOffset, basePointY + ty, labelTextPaint)

            clickRect.right += w.toInt()
        }

        clickRects.put(m.id, Rect.unflattenFromString(clickRect.flattenToString()))
    }

    private fun drawLine(canvas: Canvas, params: DrawParams) {
        if (params.points.isNotEmpty()) {
            val vPath = Path() // TODO
            var vPrev = sourceToViewCoord(params.points[0].x, params.points[0].y) ?: return
            vPath.moveTo(vPrev.x, vPrev.y)

            for (k in 1 until params.points.size) {
                val vPoint = sourceToViewCoord(params.points[k].x, params.points[k].y) ?: return
                vPath.quadTo(vPrev.x, vPrev.y, (vPoint.x + vPrev.x) / 2, (vPoint.y + vPrev.y) / 2)
                vPrev = vPoint
            }

            drawingPaint.color = params.color
            drawingPaint.strokeWidth = params.width.toFloat() * scale

            canvas.drawPath(vPath, drawingPaint)
        }
    }

    private fun drawRect(canvas: Canvas, params: DrawParams) {
        val start = sourceToViewCoord(params.points[0])
        val end = sourceToViewCoord(params.points[1])

        drawingPaint.color = params.color
        drawingPaint.strokeWidth = params.width.toFloat() * scale

        if (start != null && end != null)
            canvas.drawRect(start.x, start.y, end.x, end.y, drawingPaint)
    }

    private fun drawText(canvas: Canvas, text: String, params: TextParams) {
        params.point.let {
            sourceToViewCoord(it.x, it.y)?.run {
                textPaint.color = params.color
                textPaint.textSize = params.fontSize * scale

                val w = textPaint.measureText(text).toInt()
                val textLayout = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    StaticLayout.Builder.obtain(text, 0, text.length, textPaint, w)
                            .setHyphenationFrequency(Layout.HYPHENATION_FREQUENCY_NONE).build()
                } else {
                    StaticLayout(text, textPaint, w, Layout.Alignment.ALIGN_NORMAL, 1f, 0f, false)
                }

                // draw text to the Canvas center
                canvas.save()
                canvas.translate(x, y)
                textLayout.draw(canvas)
                canvas.restore()
            }
        }
    }

    private fun click(event: MotionEvent) {
        val rawP = PointF(event.x, event.y)
        if (isLabelMode) {
            isLabelMode = false

            val p = viewToSourceCoord(rawP) ?: return
            if (isInPicture(p)) {
                val m = Marker()
                m.type = type

                if (type == EDIT || type == LINK) {
                    labelParams = LinkParams()
                }

                if (type == NOTE) {
                    labelParams = NoteParams()
                }

                labelParams?.setXY(p.x.toInt(), p.y.toInt())

                m.params = labelParams
                onMarkerInteraction!!.markerAdd(m)
            }
        } else {
            val m = checkPoint(rawP)
            if (m != null)
                onMarkerInteraction!!.onClick(m)
        }
    }

    private fun isInPicture(p: PointF): Boolean {
        return p.x >= 0 && p.y >= 0 && p.x < sWidth && p.y < sHeight
    }

    fun stopEdit() {
        isLabelMode = false

        labelPaint.alpha = 255
        invalidate()
    }

    fun addMarkers(list: ArrayList<Marker>, highlight: Int = 0) {
        if (initScale < 0)
            initScale = scale

        markerToHighlight = highlight
        if (markerToHighlight > 0) {
            runTimer()
        }

        list.forEach {
            if (markers.get(it.id) == null) {
                if (it.type == LINE || it.type == RECT)
                    (it.params as DrawParams).width = ((it.params as DrawParams).width * (1 / initScale)).toInt()
                else if (it.type == TEXT)
                    (it.params as TextParams).fontSize =
                            ((it.params as TextParams).fontSize * context.resources.displayMetrics.density * (1 / initScale)).toInt()
            }

            markers.put(it.id, it)
        }

        invalidate()
    }

    private fun runTimer() {
        task {
            Thread.sleep(3000)
        } alwaysUi {
            markerToHighlight = 0
            invalidate()
        }
    }

    fun updateMarker(updated: Marker) {
        markers.get(updated.id).apply {
            params = updated.params
            title = updated.title
            text = updated.text
        }
    }

    fun notifyMarkerListChanged() {
        invalidate()
        stopEdit()
    }

    private fun checkPoint(p: PointF): Marker? {
        for (i in 0 until clickRects.size()) {
            val r = clickRects.valueAt(i)
            if (r != null && r.contains(p.x.toInt(), p.y.toInt())) {
                val id = clickRects.keyAt(i)
                return markers.get(id)
            }
        }
        return null
    }

    fun setOnMarkerInteraction(l: OnMarkerListener) {
        onMarkerInteraction = l
    }

    fun addLabel(type: Int) {
        isLabelMode = true
        this.type = type
        invalidate()
    }
}
