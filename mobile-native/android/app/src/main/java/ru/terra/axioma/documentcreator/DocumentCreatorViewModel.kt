package ru.terra.axioma.documentcreator

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.databinding.Observable
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.databinding.ObservableList
import android.net.Uri
import androidx.annotation.NonNull

import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOException
import java.nio.channels.FileChannel
import java.util.ArrayList

import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import ru.terra.axioma.DocumentUtils
import ru.terra.axioma.FileUtils
import ru.terra.axioma.LoadPdfTask
import ru.terra.axioma.R
import ru.terra.axioma.RealPathUtil
import ru.terra.axioma.adapters.AttachPhotoAdapter
import ru.terra.axioma.client.ClientApi
import ru.terra.axioma.models.Document
import ru.terra.axioma.repository.DocumentsRepository
import timber.log.Timber

class DocumentCreatorViewModel internal constructor(
        application: Application,
        private val client: ClientApi,
        private val repository: DocumentsRepository,
        private val parent: Document
) : AndroidViewModel(application), DocumentCreatorNavigator.Action {

    var pages = ObservableArrayList<AttachPhotoAdapter.AttachPhoto>()
    var enabledSave = ObservableBoolean(false)
    var title = ObservableField("")
    var shortTitle = ObservableField("")
    var path = ObservableField<String>()


    private var view: DocumentCreatorNavigator.View? = null
    private var pdfLoadingTask: LoadPdfTask? = null
    private var tempDir: File? = null


    init {
        pages.addOnListChangedCallback(object : ObservableList.OnListChangedCallback<ObservableList<AttachPhotoAdapter.AttachPhoto>>() {
            override fun onChanged(sender: ObservableList<AttachPhotoAdapter.AttachPhoto>) {}

            override fun onItemRangeChanged(sender: ObservableList<AttachPhotoAdapter.AttachPhoto>, positionStart: Int, itemCount: Int) {}

            override fun onItemRangeInserted(sender: ObservableList<AttachPhotoAdapter.AttachPhoto>, positionStart: Int, itemCount: Int) {
                enabledSave.set(!title.get()?.isEmpty()!!)
            }

            override fun onItemRangeMoved(sender: ObservableList<AttachPhotoAdapter.AttachPhoto>, fromPosition: Int, toPosition: Int, itemCount: Int) {}

            override fun onItemRangeRemoved(sender: ObservableList<AttachPhotoAdapter.AttachPhoto>, positionStart: Int, itemCount: Int) {
                enabledSave.set(!sender.isEmpty() && !title.get()?.isEmpty()!!)
            }
        })

        title.addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable, propertyId: Int) {
                enabledSave.set(!pages.isEmpty() && !title.get()?.isEmpty()!!)
            }
        })

        path.set(DocumentUtils.getDocPath(repository.documents, parent))
    }

    fun setViewListener(view: DocumentCreatorNavigator.View) {
        this.view = view
    }

    fun save() {
        view!!.showLoadingToServerProgress()
        createDocument()
    }

    fun cancel() {
        view!!.cancelAndClose()
    }

    /**
     * Сохранение данных на сервер
     */


    private fun createDocument() {
        repository.createDocument(
                parent, title.get()!!, shortTitle.get(),
                { document -> uploadPagesToVersion(document, document.defaultVersionId) },
                { view?.showError(R.string.common_error_create_document) }
        )
    }

    private fun uploadPagesToVersion(document: Document, versionId: Int) {
        val description = RequestBody.create(MultipartBody.FORM, "description")

        val parts = ArrayList<MultipartBody.Part>()
        for (page in pages) {
            val uri = page.uri
            if (File(uri.toString()).exists()) {

            }
            parts.add(FileUtils.getFilePart("page" + pages.indexOf(page), uri))
        }

        client.uploadFilesToVersion(document.id, versionId, description, parts).enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                Timber.v("uploadPagesToVersion - success")
                onFinish()
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Timber.e(t)
                view!!.showError(R.string.common_finished_with_errors)
            }
        })
    }

    private fun onFinish() {
        view!!.dismissProgress()
        if (tempDir != null)
            tempDir!!.delete()
        view!!.close()
    }

    override fun onCleared() {
        super.onCleared()
        if (pdfLoadingTask != null)
            pdfLoadingTask!!.setListener(null)
    }

    override fun cancelPdfRenderTask() {
        pdfLoadingTask!!.cancel(true)
    }

    private fun renderPdfPages(path: String) {
        try {
            tempDir = FileUtils.createTempDir(getApplication<Application>().applicationContext)
            if (tempDir != null) {
                pdfLoadingTask = LoadPdfTask(getApplication<Application>().applicationContext, tempDir)
                pdfLoadingTask!!.setListener(object : LoadPdfTask.LoadPdfListener {
                    override fun onStart(max: Int) {
                        view!!.showRenderPdfProgress(max)
                    }

                    override fun onProgress(value: Int, max: Int) {
                        view!!.updateProgress(value, max)
                    }

                    override fun onDone(result: List<Uri>) {
                        pages.addAll(result.map { AttachPhotoAdapter.AttachPhoto(it) })
                    }
                })

                pdfLoadingTask!!.execute(path)
            }
        } catch (e: IOException) {
            Timber.e(e)
        }

    }

    override fun renderPage(uri: Uri) {
        val path = RealPathUtil.getRealPath(getApplication(), uri)

        path ?: return

        val f = File(path)
        if (!f.exists()) {
            view!!.showError("Файл не найден")
            return
        }

        if (path.contains(".pdf")) {
            renderPdfPages(path)
        } else {
            val tmp = copyFile(f)
            pages.add(AttachPhotoAdapter.AttachPhoto(Uri.fromFile(tmp)))
        }
    }

    private fun copyFile(sourceFile: File): File? {
        if (!sourceFile.exists()) {
            return null
        }

        try {
            val path = sourceFile.path
            val extension = path.substring(path.lastIndexOf("."))
            tempDir = FileUtils.createTempDir(getApplication<Application>().applicationContext)

            val destFile = FileUtils.createTempFile(tempDir, extension)

            val source: FileChannel? = FileInputStream(sourceFile).channel
            val destination: FileChannel
            destination = FileOutputStream(destFile).channel
            if (source != null) {
                destination.transferFrom(source, 0, source.size())
                source.close()
            }
            destination.close()

            return destFile
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return null
    }
}