package ru.terra.axioma.models.marker;

/**
 * Created by alexei on 11/2/17.
 */

public class LinkParams extends LabelBaseParams {
    public String label;
    public int refDocId;
    public int refFileId;
}
