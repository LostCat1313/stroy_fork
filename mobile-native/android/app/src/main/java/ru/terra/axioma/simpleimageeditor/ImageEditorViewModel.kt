package ru.terra.axioma.simpleimageeditor

import android.app.Application
import android.util.Pair
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.AndroidViewModel
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import ru.terra.axioma.Configuration
import ru.terra.axioma.DocumentUtils
import ru.terra.axioma.client.ClientApi
import ru.terra.axioma.models.ProjectFile

class ImageEditorViewModel(application: Application) : AndroidViewModel(application) {
    var loading = ObservableBoolean()
    var error = ObservableBoolean()
    var isLight = ObservableBoolean(true)

    private var navigator: ImageEditorNavigator? = null
    private var client: ClientApi? = null

    fun onActivityCreate(client: ClientApi, file: ProjectFile) {
        this.client = client
        loadFile(file.url)
    }

    fun setNavigator(navigator: ImageEditorNavigator) {
        this.navigator = navigator
    }

    private fun loadFile(url: String) {
        loading.set(true)
        client!!.downloadDocument(Configuration.baseUrl + url).enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    val result = DocumentUtils.writeToTemp(getApplication<Application>().applicationContext, response.body()!!)
                    if (result != null && result.first) {
                        //                        filename = result.second;
                        navigator!!.setImage(result.second)
                        loading.set(false)
                        return
                    }
                }

                loading.set(false)
                error.set(true)
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                loading.set(false)
                error.set(true)
            }
        })
    }
}
