package ru.terra.axioma;

import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import ru.terra.axioma.databinding.ActivityImageViewerBinding;
import ru.terra.axioma.views.CircleProgressBarDrawable;

public class ImageViewerActivity extends AppCompatActivity {
    public final static String URI = "newPageUri";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityImageViewerBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_image_viewer);

        binding.toolbar.setContentInsetStartWithNavigation(0);
        binding.toolbar.setTitleTextAppearance(this, R.style.ToolbarTitle);
        setSupportActionBar(binding.toolbar);
        binding.toolbar.setNavigationOnClickListener(view -> onBackPressed());

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        CircleProgressBarDrawable progress = new CircleProgressBarDrawable();
        progress.setBackgroundColor(this.getResources().getColor(R.color.grey));
        progress.setColor(this.getResources().getColor(R.color.grey_600));
        binding.image.getHierarchy().setProgressBarImage(progress);

        Uri uri = getIntent().getParcelableExtra(URI);
        binding.image.setPhotoUri(uri.getScheme() != null ? uri : Uri.parse(Configuration.baseUrl + uri.toString()));
    }
}
