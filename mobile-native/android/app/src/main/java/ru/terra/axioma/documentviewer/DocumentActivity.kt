package ru.terra.axioma.documentviewer

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.*
import android.widget.Button
import android.widget.PopupWindow
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_document.*
import kotlinx.android.synthetic.main.toolbar_document.*
import ru.terra.axioma.R
import ru.terra.axioma.Constants.VersionType.ALL
import ru.terra.axioma.DocumentUtils
import ru.terra.axioma.ListUtils
import ru.terra.axioma.adapters.SimpleTextAdapter
import ru.terra.axioma.documentmarker.DocumentMarkerActivity
import ru.terra.axioma.models.Document
import ru.terra.axioma.models.ProjectFile
import ru.terra.axioma.models.marker.Marker
import ru.terra.axioma.documentmarker.EditableMarker
import kotlin.collections.ArrayList

class DocumentActivity : AppCompatActivity(), OnDocumentFragmentListener {

    private var actionBar: ActionBar? = null
    private lateinit var actionMenuPopup: PopupWindow
    private lateinit var pageListPopup: PopupWindow

    private var document: Document? = null
    private var pages: List<ProjectFile> = ArrayList()
    private lateinit var file: ProjectFile

    private var fragment: DocumentFragment? = null

    private var currentPage: MutableLiveData<Int> = MutableLiveData()

    private var isPageLoaded = MutableLiveData<Boolean>()
    private var isEdit = MutableLiveData<Boolean>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_document)

        toolbar.setTitleTextAppearance(this, R.style.ToolbarTitle)
        setSupportActionBar(toolbar)
        actionBar = supportActionBar

        setupVariables()
        setupActionBar()
        setupEditorMenu()
        setupPageListPopup()

        currentPage.observe(this, Observer {
            openDocument(document, it)
            page.text = getString(R.string.viewer_pages, it + 1, pages.size)
        })

        openDocument(document, file)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        isEdit.value = false
        fragment?.onActivityResult(requestCode, resultCode, data)
    }

    private fun setupActionBar() {
        val actionBar = supportActionBar ?: return

        actionBar.setDisplayHomeAsUpEnabled(false)
        actionBar.setDisplayShowTitleEnabled(false)

        layoutInflater.inflate(R.layout.toolbar_document, toolbar)

        if (pages.size > 1) {
            pagesBlock.visibility = View.VISIBLE
            titleView.visibility = View.GONE

            page.setOnClickListener { showPageList() }
            previous.setOnClickListener {
                currentPage.value?.let {
                    currentPage.value = if (it - 1 < 0) pages.size - 1 else it - 1
                }
            }

            next.setOnClickListener {
                currentPage.value?.let {
                    currentPage.value = if (it + 1 == pages.size) 0 else it + 1
                }
            }
        } else {
            titleView.text = document!!.title
        }

        shareButton.setOnClickListener { fragment?.share() }
        menuButton.setOnClickListener { showEditorMenu() }

        isEdit.observe(this, Observer { v ->
            if (v) {
                menuButton.setImageResource(R.drawable.ic_close_white)
                menuButton.setOnClickListener {
                    fragment?.stopEdit()
                    isEdit.value = false
                }
            } else {
                menuButton.setImageResource(R.drawable.ic_mode_edit_24dp)
                menuButton.setOnClickListener { showEditorMenu() }
            }
        })

        isPageLoaded.observe(this, Observer { v ->
            shareButton.isEnabled = v
            menuButton.isEnabled = v
        })

        back.setOnClickListener { onBackPressed() }
        toolbar.contentInsetStartWithNavigation = 0
    }

    private fun setupVariables() {
        file = intent.getParcelableExtra(FILE)

        intent.getParcelableExtra<Document>(DOCUMENT)?.let {
            document = it
            val versionType = intent.getIntExtra(VERSION, ALL)
            pages = it.getPagesList(versionType)
            currentPage.value = ListUtils.indexOf(pages, file)
        }
    }

    private fun setupEditorMenu() {
        val inflater = LayoutInflater.from(this)

        actionMenuPopup = PopupWindow(this).apply {
            contentView = inflater.inflate(R.layout.editor_popup_menu, null)
            isFocusable = true
            isOutsideTouchable = true
            width = WindowManager.LayoutParams.WRAP_CONTENT
            height = WindowManager.LayoutParams.WRAP_CONTENT
            setBackgroundDrawable(ColorDrawable(Color.WHITE))
            elevation = 0f
        }

        val editBtn = actionMenuPopup.contentView.findViewById<Button>(R.id.editMarkButton)
        editBtn.visibility = if (DocumentUtils.isFileHasVersions(document!!, file)) View.VISIBLE else View.GONE
        editBtn.setOnClickListener { onEditorClick(Marker.EDIT) }

        val linkBtn = actionMenuPopup.contentView.findViewById<Button>(R.id.linkMarkButton)
        linkBtn.setOnClickListener { onEditorClick(Marker.LINK) }

        val noteBtn = actionMenuPopup.contentView.findViewById<Button>(R.id.noteMarkButton)
        noteBtn.setOnClickListener { onEditorClick(Marker.NOTE) }

        val textBtn = actionMenuPopup.contentView.findViewById<Button>(R.id.textMarkButton)
        textBtn.setOnClickListener { onEditorClick(Marker.TEXT) }

        val rectBtn = actionMenuPopup.contentView.findViewById<Button>(R.id.rectMarkButton)
        rectBtn.setOnClickListener { onEditorClick(Marker.RECT) }

        val pathBtn = actionMenuPopup.contentView.findViewById<Button>(R.id.pathMarkButton)
        pathBtn.setOnClickListener { onEditorClick(Marker.LINE) }
    }

    private fun setupPageListPopup() {
        val inflater = LayoutInflater.from(this)
        val view = inflater.inflate(R.layout.pages_list, null)

        pageListPopup = PopupWindow(this).apply {
            isFocusable = true
            isOutsideTouchable = true
            width = WindowManager.LayoutParams.WRAP_CONTENT
            height = WindowManager.LayoutParams.WRAP_CONTENT
            contentView = view
            setBackgroundDrawable(ColorDrawable(Color.WHITE))
            elevation = 0f
        }

        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED)

        val list = view.findViewById<RecyclerView>(R.id.recyclerView)

        val count = pages.size
        val items = ArrayList<String>()
        for (i in 0 until count)
            items.add(getString(R.string.editor_page, pages[i].pageNumber))

        val adapter = SimpleTextAdapter(items, R.layout.string_list_item) { position ->
            currentPage.value = position
        }
        list.layoutManager = LinearLayoutManager(this)
        list.adapter = adapter
    }

    private fun showPageList() {
        pageListPopup.showAtLocation(root, Gravity.TOP or Gravity.CENTER, 0, appBar.bottom)
    }

    private fun showEditorMenu() {
        actionMenuPopup.showAtLocation(root, Gravity.TOP or Gravity.END, 0, appBar.bottom)

        actionMenuPopup.contentView.findViewById<Button>(R.id.editMarkButton).visibility =
                if (DocumentUtils.isFileHasVersions(document!!, file)) View.VISIBLE else View.GONE
    }

    private fun onEditorClick(type: Int) {
        isEdit.value = true

        fragment!!.startEdit(type)
        actionMenuPopup.dismiss()
    }

    private fun openDocument(d: Document?, page: Int) {
        isEdit.value = false

        file = pages[page]
        openDocument(d, file)
    }

    private fun openDocument(d: Document?, f: ProjectFile?) {
        fragment = DocumentFragment.newInstance(d!!, f!!, intent.getIntExtra(MARKER_ID, 0))
        supportFragmentManager.beginTransaction().replace(R.id.root, fragment!!).commit()

//        fragment?.isEdit?.removeObservers(this)
//        fragment?.isEdit?.observe(this, androidx.lifecycle.Observer {
//            isEdit.value = it
//        })
    }

    private fun setEditableMarker(m: Marker) {
        EditableMarker.set(m)
    }

    override fun onMarkerAdd(m: Marker) {
        setEditableMarker(m)
        val intent = Intent(this, DocumentMarkerActivity::class.java)
        startActivityForResult(intent, DocumentMarkerActivity.ACTION_CREATE)
    }

    override fun onMarkerEdit(m: Marker) {
        setEditableMarker(m)
        val intent = Intent(this, DocumentMarkerActivity::class.java)
        startActivityForResult(intent, DocumentMarkerActivity.ACTION_EDIT)
    }

    override fun onMarkerView(m: Marker) {
        setEditableMarker(m)
        val intent = Intent(this, DocumentMarkerActivity::class.java)
        startActivityForResult(intent, DocumentMarkerActivity.ACTION_VIEW)
    }

    override fun onPageChange(d: Document, f: ProjectFile?) {
        openDocument(d, f)
    }

    override fun setPageLoaded(loaded: Boolean) {
        isPageLoaded.value = loaded
    }

    companion object {
        const val FILE = "file"
        const val DOCUMENT = "document"
        const val VERSION = "version"
        const val FILE_ID = "FILE_ID"
        const val DOCUMENT_ID = "DOCUMENT_ID"
        const val MARKER_ID = "MARKER_ID"
    }
}
