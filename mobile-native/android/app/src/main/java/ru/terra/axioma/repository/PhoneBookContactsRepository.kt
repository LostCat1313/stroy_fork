package ru.terra.axioma.repository

import android.content.ContentUris
import android.content.Context
import android.net.Uri
import android.provider.ContactsContract
import android.util.Patterns
import nl.komponents.kovenant.Promise
import nl.komponents.kovenant.task
import nl.komponents.kovenant.then
import ru.terra.axioma.StringUtils
import ru.terra.axioma.models.PhoneBookContact
import timber.log.Timber
import java.util.*

class PhoneBookContactsRepository(private val context: Context) {
    var contacts: HashMap<String, PhoneBookContact>? = null

    fun fetch(): Promise<HashMap<String, PhoneBookContact>?, Exception> {
        return task {
            load()
        } then {
            it
        } fail {
            Timber.d(it)
        }
    }

    private fun load(): HashMap<String, PhoneBookContact>? {
        val DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME_PRIMARY
        val FILTER = "$DISPLAY_NAME NOT LIKE '%@%'"
        val ORDER = "$DISPLAY_NAME COLLATE NOCASE"
        val PROJECTION = arrayOf(ContactsContract.Contacts._ID, DISPLAY_NAME, ContactsContract.Contacts.HAS_PHONE_NUMBER)

        val result = HashMap<String, PhoneBookContact>()

        val contentResolver = context.contentResolver
        val cursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, PROJECTION, FILTER, null, ORDER)

        if (cursor != null && cursor.moveToFirst()) {
            while (cursor.moveToNext()) {
                val id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID))
                if (cursor.getInt(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {
                    val cursorInfo = contentResolver.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            arrayOf(id), null
                    ) ?: return null

                    val person = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, java.lang.Long.valueOf(id))
                    val pURI = Uri.withAppendedPath(person, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY)

                    while (cursorInfo.moveToNext()) {
                        var phone = cursorInfo.getString(cursorInfo.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
                        phone = StringUtils.formatPhoneNumber(phone)

                        if (result[phone] == null && phone.length > 7 && !phone.contains("*") && Patterns.PHONE.matcher(phone).matches()) {
                            val contact = PhoneBookContact()
                            contact.id = id
                            contact.firstName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME))
                            contact.lastName = ""
                            contact.phoneNumber = phone
                            contact.photoURI = pURI
                            result[phone] = contact
                        }
                    }

                    cursorInfo.close()
                }
            }

            cursor.close()
        }

        return result
    }
}
