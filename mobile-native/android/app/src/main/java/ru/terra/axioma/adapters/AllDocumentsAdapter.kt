package ru.terra.axioma.adapters

import android.graphics.drawable.Drawable
import android.util.Pair
import android.util.SparseBooleanArray
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.RadioButton
import android.widget.TextView

import com.google.common.collect.Lists
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import ru.terra.axioma.R
import ru.terra.axioma.models.Document


class AllDocumentsAdapter @JvmOverloads constructor(private var isSelectable: Boolean = false) : RecyclerView.Adapter<AllDocumentsAdapter.ViewHolder>/*RecursiveRecyclerAdapter<AllDocumentsAdapter.ViewHolder>*/() {
    private val viewItems: MutableList<Document> = mutableListOf()
    private var data: List<Document>? = null
    private var clickListener: OnClickListener? = null
    private var checkedItem: Pair<Boolean, Document>? = null
    private val stateTree: SparseBooleanArray
    private var mode: Int = 0

    init {
        this.data = Lists.newArrayList()
        this.stateTree = SparseBooleanArray()
    }

    fun setData(documents: List<Document>) {
        isSelectable = false

        if (stateTree.size() == 0) {
            buildStateTree(documents)
        }

        /*if (data == null || data.isEmpty()) {
            setItems(documentsCount);
        } else {

        }*/


        clearItems()
        setItems(documents)
        this.data = documents
    }

    private fun buildStateTree(documents: List<Document>) {
        for (d in documents) {
            stateTree.append(d.id, false)
            if (d.children != null && !d.children.isEmpty())
                buildStateTree(d.children)
        }
    }

    private fun toggleState(document: Document, value: Boolean) {
        stateTree.put(document.id, value)
    }

    private fun setItems(documents: List<Document>) {
        for (document in documents) {
            document.depth = 0
            viewItems.add(document)
            if (isExpanded(document)) {
                if (document.children == null || document.children.isEmpty()) {
                    toggleState(document, false)
                } else {
                    addItems(document, document.getChildren())
                }
            }
        }
    }

    fun removeRoot() {
        val item = getItem(0)
        if (item.id == -1) {
            viewItems.removeAt(0)
            notifyItemRemoved(0)
        }
    }

    fun setSelectable(selectable: Boolean, mode: Int) {
        isSelectable = selectable
        this.mode = mode
    }

    fun addRoot(rootDoc: Document) {
        viewItems.add(0, rootDoc)
        notifyItemInserted(0)
    }

    inner class ViewHolder(itemView: View, type: Int) : RecyclerView.ViewHolder(itemView) {
        var padding1: View = itemView.findViewById(R.id.padding1)
        var separateWhite: View? = null
        var separateBlack: View
        var title: TextView = itemView.findViewById(R.id.title)
        var expand: ImageButton? = null

        init {
            separateWhite = itemView.findViewById(R.id.separate_white)
            separateBlack = itemView.findViewById(R.id.separate_black)
            expand = itemView.findViewById(R.id.expand)

            when (type) {
                0 -> {
                    itemView.setBackgroundResource(R.color.grey)
                    separateWhite?.visibility = View.VISIBLE
                }

                else -> {
                    separateBlack.visibility = View.VISIBLE
                    padding1.visibility = View.VISIBLE
                    padding1.layoutParams.width *= getDepth(type)
                }
            }
        }

        fun toggleIcon(expanded: Boolean) {
            expand?.rotation = (if (expanded) 180 else 0).toFloat()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, type: Int): ViewHolder {
        val layoutRes: Int
        if (type % 2 == 0)
            layoutRes = R.layout.folder_list_item
        else
            layoutRes = R.layout.document_list_item

        val view = LayoutInflater.from(parent.context).inflate(layoutRes, parent, false)
        return ViewHolder(view, type)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.title.text = item.title

        if (!item.isFolder) {
            val icon = ContextCompat.getDrawable(
                    holder.itemView.context,
                    if (item.isMultiPage) R.drawable.ic_file_multiple else R.drawable.zzz_file_orange
            )
            holder.title.setCompoundDrawablesRelativeWithIntrinsicBounds(icon, null, null, null)
        }

        holder.expand?.takeIf { item.isFolder }?.apply {
            if (item.getChildren().isEmpty())
                visibility = View.GONE
            else {
                visibility = View.VISIBLE
                setOnClickListener { holder.toggleIcon(toggle(item)) }
            }
        }

        holder.toggleIcon(isExpanded(item))


        holder.itemView.setOnClickListener {
            if (isSelectable && (item.isFolder || mode == 2)) {
                checkedItem = Pair(item.id == -1, item)
                clickListener?.onSelect(item)
            } else if (!item.isFolder) {
                clickListener?.onDocumentClick(item)
            }
        }
        holder.itemView.setOnLongClickListener { v ->
            clickListener?.onLongClick(item)
            false
        }
    }

    override fun getItemViewType(position: Int): Int {
        val depth = viewItems[position].getDepth()
        val item = getItem(position)
        return 2 * depth + if (item.isFolder) 0 else 1
    }

    override fun getItemCount(): Int {
        return viewItems.size
    }

    private fun getDepth(value: Int): Int {
        val s = (value.toDouble() / 2).toString().split("\\.".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        return Integer.parseInt(s[0])
    }

    private fun getItem(position: Int): Document {
        return viewItems[position]
    }

    private fun isExpanded(item: Document): Boolean {
        return stateTree.get(item.id, false)
    }

    private fun addItems(parent: Document, items: List<Document>?): Int {
        return items?.let {
            var position = viewItems.indexOf(parent)
            val parentDepth = viewItems[position].getDepth()

            val count = it.indices.fold(0, { acc: Int, i: Int ->
                val item = it[i]
                item.depth = parentDepth + 1
                viewItems.add(position + 1 + i, item)
                if (isExpanded(item)) {
                    position += addItems(item, item.children)
                }
                acc + 1
            })

            toggleState(viewItems[position], true)
            notifyItemRangeInserted(position + 1, items.size)
            notifyItemChanged(position)

            count
        } ?: let {
            0
        }
    }

    private fun removeItems(parent: Document, children: List<Document>) {
        val position = viewItems.indexOf(parent)
        val item = viewItems[position + 1]

        for (i in children.indices) {
            if (isExpanded(item))
                removeItems(item, item.getChildren())
            viewItems.removeAt(position + 1)
        }
        toggleState(viewItems[position], false)
        notifyItemRangeRemoved(position + 1, children.size)
        notifyItemChanged(position)
    }

    private fun clearItems() {
        viewItems.clear()
    }

    private fun toggle(_item: Document): Boolean {
        return if (isExpanded(_item)) {
            removeItems(_item, _item.getChildren())
            false
        } else {
            addItems(_item, _item.getChildren())
            true
        }
    }

    fun setOnClickListener(clickListener: OnClickListener) {
        this.clickListener = clickListener
    }

    interface OnClickListener {
        fun onDocumentClick(document: Document)
        fun onSelect(document: Document)
        fun onLongClick(document: Document)
    }
}
