package ru.terra.axioma.views.imageeditor.text

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.PointF
import android.graphics.Rect
import android.view.MotionEvent
import android.view.ViewGroup

import androidx.appcompat.widget.AppCompatEditText
import androidx.core.content.ContextCompat
import ru.terra.axioma.*
import ru.terra.axioma.Utils

internal class InnerEditText(context: Context) : AppCompatEditText(context) {

    val textForDrawing: String
        get() = text.toString()

    val textPaintForDrawing: Paint
        get() = paint

    val textPointForDrawing: PointF
        get() {
            val rect = Rect()
            getGlobalVisibleRect(rect)
            return PointF(rect.left.toFloat(), rect.top.toFloat())
        }

    init {
        init()
    }

    private fun init() {
        isFocusable = true
        isFocusableInTouchMode = true
        isDrawingCacheEnabled = true

        layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)

        backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context, android.R.color.transparent))
        setTextColor(DEFAULT_COLOR)

        hint = "Введите текст"
        textSize = 16f
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        return isFocused && super.onTouchEvent(event)
    }

    override fun onFocusChanged(focused: Boolean, direction: Int, previouslyFocusedRect: Rect?) {
        if (focused)
            Utils.showSoftInput(context)

        super.onFocusChanged(focused, direction, previouslyFocusedRect)
    }
}
