package ru.terra.axioma.projects


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.fragment_projects_list.*
import nl.komponents.kovenant.ui.failUi
import org.koin.android.ext.android.getKoin
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.sharedViewModel

import ru.terra.axioma.R
import ru.terra.axioma.adapters.ProjectsAdapter
import ru.terra.axioma.models.Project
import ru.terra.axioma.repository.DocumentsRepository
import ru.terra.axioma.repository.ProjectNotesRepository
import ru.terra.axioma.repository.ProjectsRepository
import ru.terra.axioma.store.CurrentSessionStore
import ru.terra.axioma.views.CustomDialog

class ListFragment : Fragment(), ProjectsAdapter.OnClickListener  {

    private val vm: ProjectsViewModel by sharedViewModel()
    private val documentsRepository: DocumentsRepository by inject()
    private val currentSessionStore: CurrentSessionStore by inject()
    private val notesRepository: ProjectNotesRepository by inject()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_projects_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupActionBar()

        val adapter = ProjectsAdapter(this)
        list.adapter = adapter

        val projectsRepository: ProjectsRepository = getKoin().get()
        projectsRepository.run {
            projects.observe(this@ListFragment, Observer {
                adapter.setData(it ?: ArrayList())
            })

            isLoading.observe(this@ListFragment, Observer {
                progress.visibility = if (it) View.VISIBLE else View.GONE
            })

            fetch().failUi {
                CustomDialog.error(context!!, it.message ?: "Ошибка")
            }
        }

        fab.setOnClickListener {
            vm.selectedProject.value = Project()
        }
    }

    private fun setupActionBar() {
        toolbar.setNavigationOnClickListener { activity?.onBackPressed() }
        toolbar.contentInsetStartWithNavigation = 0
    }

    override fun onSelect(project: Project) {
        currentSessionStore.project = project

        val d = CustomDialog.getLoadingDialog(context!!)
                .message(R.string.common_loading)

        val loading = MutableLiveData<Boolean>()
        loading.observe(this, Observer { value ->
            if (value) {
                d.show()
            } else {
                d.dismiss()
                activity?.onBackPressed()
            }
        })

        val docLoading = documentsRepository.isLoading

        val markerLoading = notesRepository.isLoading

        docLoading.observe(this, Observer { value ->
            loading.value = value && (markerLoading.value ?: false)
        })

        markerLoading.observe(this, Observer { value ->
            loading.value = value && (docLoading.value ?: false)
        })

        notesRepository.fetchNotesByProject(project.id)

        documentsRepository.clear2()
        documentsRepository.fetch()
    }

    override fun onEdit(project: Project) {
        vm.selectedProject.value = project
    }
}
