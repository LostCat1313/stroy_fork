package ru.terra.axioma.personalnotes

import android.app.Application
import androidx.annotation.NonNull
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import ru.terra.axioma.client.ClientApi
import ru.terra.axioma.models.PersonalNote
import ru.terra.axioma.models.PersonalNotes
import java.util.*


class PersonalNotesViewModel(application: Application, private val client: ClientApi) : AndroidViewModel(application) {
    val notes = MutableLiveData<ArrayList<PersonalNote>>()
    val selectedItem = MutableLiveData<PersonalNote>()
    val isSelectionMode = MutableLiveData<Boolean>()
    var isEditMode: Boolean = false

    var showProgress = ObservableBoolean(false)

    init {
        load()
        isSelectionMode.value = false
    }

    private fun load() {
        //        showProgress.set(true);

        client.notes.enqueue(object : Callback<PersonalNotes> {
            override fun onResponse(@NonNull call: Call<PersonalNotes>, @NonNull response: Response<PersonalNotes>) {
                if (response.body() != null) {
                    val personalNotes = response.body()
                    notes.value = personalNotes!!.list
                }
                //                showProgress.set(false);
            }

            override fun onFailure(@NonNull call: Call<PersonalNotes>, @NonNull t: Throwable) {

            }
        })
    }

    fun setSelectedItemValue(selectedItem: PersonalNote?) {
        isEditMode = selectedItem != null && selectedItem.content == null
        this.selectedItem.value = selectedItem
    }

    fun toggleSelectionMode() {
        isSelectionMode.value = !isSelectionMode.value!!
    }

    fun save(text: String) {
        val entry = JsonObject()
        entry.addProperty("content", text)
        val obj = JsonObject()
        obj.add("entry", entry)

        val callback = object : Callback<JsonObject> {
            override fun onResponse(@NonNull call: Call<JsonObject>, @NonNull response: Response<JsonObject>) {
                isEditMode = false
                setSelectedItemValue(null)
                load()
            }

            override fun onFailure(@NonNull call: Call<JsonObject>, @NonNull t: Throwable) {

            }
        }

        val id = Objects.requireNonNull<PersonalNote>(selectedItem.value).id

        if (id > 0)
            client.updateNote(id, obj).enqueue(callback)
        else
            client.createNote(obj).enqueue(callback)
    }

    fun delete(values: List<PersonalNote>) {
//        Toast.makeText(getApplication(), "Удаление будет", Toast.LENGTH_LONG).show()
//        for (note in notes) {
        client.deleteNotes(values.map { it.id }).enqueue(object : Callback<JsonObject> {
            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                val updated = notes.value?.filter { !values.contains(it) }
                notes.postValue(updated as ArrayList<PersonalNote>?)
            }

            override fun onFailure(call: Call<JsonObject>, t: Throwable) {

            }
        })
//        }
    }
}
