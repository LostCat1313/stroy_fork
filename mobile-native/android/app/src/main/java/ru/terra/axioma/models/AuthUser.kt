package ru.terra.axioma.models

import com.google.gson.annotations.SerializedName

/**
 * Created by alexei on 9/18/17.
 */

class AuthUser {
    var token: String = ""
    var user: User = User()
    @SerializedName("countSummary")
    var summary: UserSummary? = null
}
