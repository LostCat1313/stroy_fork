package ru.terra.axioma.store

import android.net.Uri
import ru.terra.axioma.models.chat.Message


class ChatStore {
    var targetMessage: Message? = null
    var attachment: Uri? = null
}