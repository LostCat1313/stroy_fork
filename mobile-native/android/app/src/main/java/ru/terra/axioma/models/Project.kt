package ru.terra.axioma.models

import android.net.Uri
import android.util.SparseIntArray
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.annotations.JsonAdapter
import ru.terra.axioma.Configuration
import ru.terra.axioma.GsonTypeAdapters
import java.util.ArrayList

/**
 * Created by alexei on 9/29/17.
 */

class Project() : BaseObject() {
    var title: String = ""
    var image: ProjectFile? = null
    var usersCount: Int = 0
    var documentsCount: Int = 0

    @JsonAdapter(GsonTypeAdapters.UsersRightsAdapter::class)
    var users: SparseIntArray = SparseIntArray()

    constructor(json: JsonElement) : this() {
        val v = Gson().fromJson(json, Project::class.java)

        id = v.id
        createdAt = v.createdAt
        createdBy = v.createdBy
        updatedAt = v.updatedAt
        modifiedBy = v.modifiedBy
        title = v.title
        image = v.image
    }

    /*constructor(id: Int, title: String, created: Date, usersCount: Int, documentsCount: Int) {
        this.id = id
        this.title = title
        this.createdAt = created
        this.usersCount = usersCount
        this.documentsCount = documentsCount
    }*/
}
