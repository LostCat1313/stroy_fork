package ru.terra.axioma.chat_v2


import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.FileProvider
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_chat_dialog.*
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.sharedViewModel
import pub.devrel.easypermissions.AppSettingsDialog
import pub.devrel.easypermissions.EasyPermissions
import pub.devrel.easypermissions.PermissionRequest
import ru.terra.axioma.Constants
import ru.terra.axioma.FileUtils
import ru.terra.axioma.ImageViewerActivity

import ru.terra.axioma.R
import ru.terra.axioma.chat_v2.adapters.AttachmentsAdapter
import ru.terra.axioma.chat_v2.adapters.MessagesAdapter
import ru.terra.axioma.simpleimageeditor.ImageEditorActivity
import ru.terra.axioma.store.CurrentSessionStore
import ru.terra.axioma.views.CustomDialog
import ru.terra.axioma.views.GridSpacingItemDecorator
import timber.log.Timber
import java.io.File
import java.io.IOException


class ChatDialogFragment : Fragment(), EasyPermissions.PermissionCallbacks {

    val viewModel: ChatViewModel by sharedViewModel()

    var capturedImageUri: Uri? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_chat_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val currentSessionStore: CurrentSessionStore by inject()

        val messagesAdapter = MessagesAdapter(currentSessionStore.user.id,
                onAttachmentClick = {
                    val intent = Intent(context, ImageEditorActivity::class.java).putExtra(ImageEditorActivity.FILE, it)
                    startActivityForResult(intent, Constants.IMAGE_EDITOR_REQUEST)
                })

        messagesList.apply {
            addItemDecoration(GridSpacingItemDecorator(1, 20, true))
            adapter = messagesAdapter
        }

        viewModel.messages.observe(this, Observer {
            messagesAdapter.items = it
            messagesList.scrollToPosition(it.size - 1)
        })


        val attachmentAdapter = AttachmentsAdapter { uri ->
            val intent = Intent(context, ImageViewerActivity::class.java)
            intent.putExtra(ImageViewerActivity.URI, uri)
            startActivity(intent)
        }.apply {
            registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
                override fun onItemRangeRemoved(positionStart: Int, itemCount: Int) {
                    attachmentsList.visibility = if (this@apply.itemCount > 0) View.VISIBLE else View.GONE
                }
            })
        }

        attachmentsList.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            addItemDecoration(GridSpacingItemDecorator(1, 20, true))
            adapter = attachmentAdapter
        }

        viewModel.attachments.observe(this, Observer {
            attachmentsList.visibility = if (it.isNotEmpty()) View.VISIBLE else View.GONE
            attachmentAdapter.data = it
        })


        messageBlock.setEndIconOnClickListener {
            viewModel.sendMessage(messageText.editableText.toString())
            messageText.editableText.clear()
        }

        messageBlock.setStartIconOnClickListener {
            showAttachmentDialog()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                Constants.GALLERY_REQUEST -> data?.data?.let {
                    val f = File(FileUtils.getPath(context, it))
                    viewModel.addAttachment(Uri.fromFile(f))
                }
                Constants.CAMERA_REQUEST -> capturedImageUri?.let {
                    viewModel.addAttachment(it)
                }
                Constants.IMAGE_EDITOR_REQUEST -> data?.let {
                    viewModel.addAttachment(it.getParcelableExtra(Constants.ATTACHMENT_URI))
                }
            }
        }
    }

    private fun showAttachmentDialog() {
        context?.let {
            CustomDialog.getImageSelectorDialog(it, object : CustomDialog.OnImageDialogSelect {
                override fun onNoPermission(requestCode: Int, perm: Array<String>) {
                    EasyPermissions.requestPermissions(
                            PermissionRequest.Builder(this@ChatDialogFragment, requestCode, *perm)
                                    .setRationale(R.string.card_message)
                                    .setPositiveButtonText(R.string.editor_ok)
                                    .setNegativeButtonText(R.string.common_cancel)
                                    .build())
                }

                override fun onCamera() {
                    openCamera()
                }

                override fun onGallery() {
                    openGallery()
                }
            }).show()
        }
    }

    fun openGallery() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, context?.getString(R.string.common_select_file)), Constants.GALLERY_REQUEST)
    }

    fun openCamera() {
        capturedImageUri = null
        context?.let { context ->
            try {
                FileUtils.createTempImageFile(context, false)?.let {
                    capturedImageUri = Uri.fromFile(it)

                    val photoURI = FileProvider.getUriForFile(context, "ru.terra.axioma.fileprovider", it)

                    val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                    if (intent.resolveActivity(context.packageManager) != null) {
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                        startActivityForResult(intent, Constants.CAMERA_REQUEST)
                    }
                }
            } catch (ex: IOException) {
                CustomDialog.error(context, "Файл не создан")
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    override fun onPermissionsGranted(requestCode: Int, perms: List<String>) {
        if (requestCode == Constants.CAMERA_REQUEST)
            openCamera()
        else if (requestCode == Constants.GALLERY_REQUEST)
            openGallery()
    }

    override fun onPermissionsDenied(requestCode: Int, perms: List<String>) {
        Timber.d("%s%s", "onPermissionsDenied:$requestCode:", perms.size)

        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            AppSettingsDialog.Builder(this).build().show()
        }
    }
}
