package ru.terra.axioma.views.imageeditor;

public interface OnOverlayListener {
    void showColorPicker();
}
