package ru.terra.axioma.documentupdater

import android.app.Application
import android.net.Uri
import androidx.annotation.NonNull
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.google.common.collect.Lists
import com.google.common.io.Files
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import ru.terra.axioma.DocumentUtils
import ru.terra.axioma.FileUtils
import ru.terra.axioma.LoadPdfTask
import ru.terra.axioma.RealPathUtil
import ru.terra.axioma.client.ClientApi
import ru.terra.axioma.models.Document
import ru.terra.axioma.models.ProjectFile
import ru.terra.axioma.models.UploadResponse
import ru.terra.axioma.models.Version
import ru.terra.axioma.repository.DocumentsRepository
import timber.log.Timber
import java.io.File
import java.io.IOException

class DocumentUpdaterViewModel(
        application: Application,
        private val client: ClientApi,
        private val documentsRepository: DocumentsRepository,
        private val parent: Document,
        private val navigator: DocumentUpdaterNavigator.View
) : AndroidViewModel(application), DocumentUpdaterNavigator.Action {

    var enabledSave = ObservableBoolean(false)
    var path = ObservableField<String>()

    val pagesLiveData: MutableLiveData<List<LinkerItem>>
    private val pages: MutableList<LinkerItem>
    private var filtered: Collection<LinkerItem>? = null

    private var pdfLoadingTask: LoadPdfTask? = null
    private var tempDir: File? = null

    private val ADDITIONAL_OPERATIONS = 2

    private val pagesObserver = Observer<List<LinkerItem>> { linkerItems ->
        if (linkerItems == null) {
            enabledSave.set(false)
            return@Observer
        }

        filtered = linkerItems.filter { it.oldPage != null }
        enabledSave.set(!filtered!!.isEmpty())
    }


    init {
        pages = Lists.newArrayList()
        pagesLiveData = MutableLiveData()
        pagesLiveData.value = pages
        pagesLiveData.observeForever(pagesObserver)
        path.set(DocumentUtils.getDocPath(documentsRepository.documents, parent))
    }

    fun save() {
        navigator.showRenderPdfProgress(filtered!!.size + ADDITIONAL_OPERATIONS, false)
        createVersion()
    }

    fun addNewPage() {
        navigator.addFile()
    }

    /**
     * Сохранение данных на сервер
     */
    //TODO Move to repository
    private fun createVersion() {
        val entry = JsonObject()
        entry.addProperty("title", "Version from android")
        entry.addProperty("documentId", parent.id)

        val obj = JsonObject()
        obj.add("entry", entry)

        client.createVersion(obj).enqueue(object : Callback<JsonObject> {
            override fun onResponse(@NonNull call: Call<JsonObject>, @NonNull response: Response<JsonObject>) {
                if (response.body() != null) {
                    val ver = Gson().fromJson(response.body(), Version::class.java)

                    navigator.updateProgress(1)

                    uploadPagesToVersion(parent, ver.id)
                }
            }

            override fun onFailure(@NonNull call: Call<JsonObject>, @NonNull t: Throwable) {
                t.printStackTrace()
            }
        })
    }

    private fun uploadPagesToVersion(document: Document?, versionId: Int) {
        val description = RequestBody.create(MultipartBody.FORM, "description")

        val count = intArrayOf(filtered!!.size)

        for (page in filtered!!) {
            val uri = page.newPageUri
            val body = FileUtils.getFilePart("page", uri)

            client.uploadFileToVersion(document!!.id, versionId, description, body).enqueue(object : Callback<UploadResponse> {
                override fun onResponse(call: Call<UploadResponse>, response: Response<UploadResponse>) {
                    Timber.v("uploadPagesToVersion - success")

                    val uploadResponse = response.body()
                    if (uploadResponse != null) {
                        page.uploadedPage = uploadResponse.files[0]
                    }

                    navigator.updateProgress(filtered!!.size - count[0] + ADDITIONAL_OPERATIONS)
                    count[0]--
                    if (count[0] == 0) {
                        setPageNumbers()
                    }
                }

                override fun onFailure(@NonNull call: Call<UploadResponse>, @NonNull t: Throwable) {
                    Timber.e(t)

                    navigator.updateProgress(filtered!!.size - count[0] + ADDITIONAL_OPERATIONS)
                    count[0]--
                    if (count[0] == 0) {
                        setPageNumbers()
                    }
                }
            })
        }
    }

    private fun setPageNumbers() {
        var showError = false
        val arr = JsonArray()

        for (item in filtered!!) {
            if (item.uploadedPage == null) {
                showError = true
                return
            }

            val entry = JsonObject()
            entry.addProperty("id", item.uploadedPage?.id)
            entry.addProperty("pageNumber", item.oldPage?.pageNumber)
            entry.addProperty("actual", true)
            arr.add(entry)
        }

        val obj = JsonObject()
        obj.add("pageList", arr)

        client.updatePage(obj).enqueue(object : Callback<JsonObject> {
            override fun onResponse(@NonNull call: Call<JsonObject>, @NonNull response: Response<JsonObject>) {
                navigator.updateProgress(filtered!!.size + ADDITIONAL_OPERATIONS)
                onFinish()
            }

            override fun onFailure(@NonNull call: Call<JsonObject>, @NonNull t: Throwable) {
                navigator.updateProgress(filtered!!.size + ADDITIONAL_OPERATIONS)

                if (showError)
                    navigator.showError("TODO добавить текст ошибки")
            }
        })
    }

    private fun onFinish() {
        if (tempDir != null)
            tempDir!!.delete()
        navigator.close()
    }

    override fun onCleared() {
        super.onCleared()
        pagesLiveData.removeObserver(pagesObserver)
        if (pdfLoadingTask != null)
            pdfLoadingTask!!.setListener(null)
    }

    override fun cancelPdfRenderTask() {
        pdfLoadingTask!!.cancel(true)
    }

    private fun renderPdfPages(path: String) {
        try {
            tempDir = FileUtils.createTempDir(getApplication<Application>().applicationContext)
            if (tempDir != null) {
                pdfLoadingTask = LoadPdfTask(getApplication<Application>().applicationContext, tempDir)
                pdfLoadingTask!!.setListener(object : LoadPdfTask.LoadPdfListener {
                    override fun onStart(max: Int) {
                        navigator.showRenderPdfProgress(max, true)
                    }

                    override fun onProgress(value: Int, max: Int) {
                        navigator.updateProgress(value)
                    }

                    override fun onDone(result: List<Uri>) {
                        pages.addAll(result.map { LinkerItem(it) })
                        pagesLiveData.setValue(pages)
                    }
                })

                pdfLoadingTask!!.execute(path)
            }
        } catch (e: IOException) {
            Timber.e(e)
        }

    }

    override fun renderPage(uri: Uri) {
        val path = RealPathUtil.getRealPath(getApplication(), uri)

        val f = File(path)
        if (!f.exists()) {
            navigator.showError("Файл не найден")
            return
        }

        if (path!!.contains(".pdf")) {
            renderPdfPages(path)
        } else {
            val tmp = copyFile(f)
            pages.add(LinkerItem(Uri.fromFile(tmp)))
            pagesLiveData.setValue(pages)
        }
    }

    private fun copyFile(sourceFile: File): File? {
        if (!sourceFile.exists()) {
            return null
        }

        try {
            val path = sourceFile.path
            val extension = path.substring(path.lastIndexOf("."))
            tempDir = FileUtils.createTempDir(getApplication<Application>().applicationContext)
            val destFile = FileUtils.createTempFile(tempDir, extension)
            Files.copy(sourceFile, destFile)
            return destFile
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return null
    }

    override fun addLinkToPage(newPage: Int, oldPage: ProjectFile) {
        pages[newPage].oldPage = oldPage
        pagesLiveData.setValue(pages)
    }

    fun removePage(position: Int) {
        pages.removeAt(position)
        pagesLiveData.setValue(pages)
    }
}
