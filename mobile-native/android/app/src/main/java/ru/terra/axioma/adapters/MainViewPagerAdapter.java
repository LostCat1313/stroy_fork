package ru.terra.axioma.adapters;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import ru.terra.axioma.main.NotificationsFragment;
import ru.terra.axioma.main.RecentDocumentsFragment;

/**
 * Created by alexei on 6/11/17.
 */

public class MainViewPagerAdapter extends FragmentPagerAdapter {
    public MainViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        switch (i) {
            case 0:
                return NotificationsFragment.Companion.newInstance();
            case 1:
                return RecentDocumentsFragment.Companion.newInstance();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }
}
