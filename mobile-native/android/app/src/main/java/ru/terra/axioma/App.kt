package ru.terra.axioma

import android.app.Application
import com.crashlytics.android.Crashlytics
import com.facebook.cache.disk.DiskCacheConfig
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.imagepipeline.backends.okhttp3.OkHttpImagePipelineConfigFactory
import io.fabric.sdk.android.Fabric
import nl.komponents.kovenant.android.startKovenant
import nl.komponents.kovenant.android.stopKovenant
import okhttp3.OkHttpClient
import org.koin.android.ext.android.getKoin
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import ru.terra.axioma.di.appModule
import ru.terra.axioma.di.restModule
import ru.terra.axioma.di.socketModule
import timber.log.Timber

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
        Fabric.with(this, Crashlytics())

        startKoin {
            androidLogger()
            androidContext(this@App)
            modules(appModule, restModule, socketModule)
        }
        startKovenant()

        val okHttpClient: OkHttpClient = getKoin().get()
        val diskCacheConfig = DiskCacheConfig.newBuilder(this)
                .setBaseDirectoryName("image_cache")
                .build()

        val imagePipelineConfig = OkHttpImagePipelineConfigFactory.newBuilder(this, okHttpClient)
                .setDownsampleEnabled(true)
                .setMainDiskCacheConfig(diskCacheConfig)
                .build()
        Fresco.initialize(this, imagePipelineConfig)

        MarkerIcons.init(applicationContext)
    }

    override fun onTerminate() {
        super.onTerminate()
        stopKovenant()
    }

    companion object {
        const val PREFS_NAME = "StroyProjectPrefs"
    }
}
