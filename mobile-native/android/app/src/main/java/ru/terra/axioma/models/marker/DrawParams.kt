package ru.terra.axioma.models.marker

import android.graphics.PointF
import com.google.gson.annotations.JsonAdapter

import ru.terra.axioma.GsonTypeAdapters

open class DrawParams : BaseParams() {
    @JsonAdapter(GsonTypeAdapters.ColorAdapter::class)
    var color: Int = 0
    var width: Int = 0
    var points: ArrayList<PointF> = ArrayList()
}
