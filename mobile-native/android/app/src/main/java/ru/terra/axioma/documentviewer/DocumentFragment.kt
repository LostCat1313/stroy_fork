package ru.terra.axioma.documentviewer

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import com.afollestad.materialdialogs.MaterialDialog
import com.davemorrissey.labs.subscaleview.ImageSource
import com.davemorrissey.labs.subscaleview.ImageViewState
import kotlinx.android.synthetic.main.fragment_document.*
import nl.komponents.kovenant.Kovenant
import nl.komponents.kovenant.Promise
import nl.komponents.kovenant.task
import nl.komponents.kovenant.then
import nl.komponents.kovenant.ui.alwaysUi
import nl.komponents.kovenant.ui.failUi
import nl.komponents.kovenant.ui.successUi
import org.koin.android.ext.android.getKoin
import org.koin.android.ext.android.inject
import ru.terra.axioma.*
import ru.terra.axioma.chat_v2.ChatActivity
import ru.terra.axioma.documentmarker.DocumentMarkerActivity
import ru.terra.axioma.documentmarker.EditableMarker
import ru.terra.axioma.models.*
import ru.terra.axioma.models.marker.*
import ru.terra.axioma.repository.DocumentsRepository
import ru.terra.axioma.repository.FilesRepository
import ru.terra.axioma.repository.MarkersRepository
import ru.terra.axioma.store.ChatStore
import ru.terra.axioma.views.CustomDialog
import ru.terra.axioma.views.imageeditor.OnMarkerListener
import timber.log.Timber
import java.io.FileOutputStream

class DocumentFragment : Fragment(), OnMarkerListener {

    private var document: Document? = null
    private var file: ProjectFile? = null
    private var filename: String? = null
    private var mListener: OnDocumentFragmentListener? = null
    private var isEdit = MutableLiveData<Boolean>()
    private var loadPromise: Promise<Unit?, Exception>? = null
    private var markerId = 0

    private val markersRepository: MarkersRepository by inject()
    private val filesRepository: FilesRepository by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            document = it.getParcelable(ARG_DOCUMENT)
            file = it.getParcelable(ARG_FILE)
            markerId = it.getInt(ARG_MARKER_ID)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_document, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setPageLoaded(false)
        documentView.setOnMarkerInteraction(this)

        if (file == null) {
            showError()
        } else {
            savedInstanceState?.let { bundle ->
                var imageViewState: ImageViewState? = null
                if (bundle.containsKey(BUNDLE_STATE) && bundle.containsKey(BUNDLE_FILENAME)) {
                    imageViewState = bundle.getSerializable(BUNDLE_STATE) as ImageViewState
                    filename = bundle.getString(BUNDLE_FILENAME)
                }

                filename?.takeIf { it.isNotEmpty() }?.let {
                    progress.visibility = View.GONE
                    documentView.setImage(ImageSource.uri(it), imageViewState)
                    documentView.setMarkers(file!!.descriptors)
                    setPageLoaded(true)
                }
            } ?: run {
                loadFile()
            }
        }

        documentView.saveFn = {
            markersRepository.save(it) then { res ->
                if (res.size < it.size)
                    CustomDialog.error(context!!, "Some markers no saved").show()

                documentView.setMarkers(it)
            } fail {
                CustomDialog.error(context!!, "Marker no saved").show()
            }
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnDocumentFragmentListener) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        documentView.state?.let {
            outState.putSerializable(BUNDLE_STATE, it)
            outState.putString(BUNDLE_FILENAME, filename)
            outState.putParcelable(BUNDLE_DOCUMENT, document)
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    override fun onDestroy() {
        super.onDestroy()
        loadPromise?.let {
            Kovenant.cancel(it, Exception("Fragment detached"))
        }
    }

    private fun showError() {
        setPageLoaded(false)
        progress?.visibility = View.GONE
        errorText?.setText(R.string.viewer_error)
    }

    private fun loadFile() {
        val onSuccess = { path: String ->
            documentView?.setImage(ImageSource.uri(path)) {
                loadMarkers()
                setPageLoaded(true)
            }
        }

        file?.let { f ->
            val cache = DocumentUtils.getFromCache(context, f.fileId)
            if (cache.first) {
                onSuccess(cache.second)
            } else {
                progress.visibility = View.VISIBLE

                filesRepository.downloadFile(f) successUi {
                    if (it != null && it.first) {
                        filename = it.second
                        onSuccess(it.second)
                    } else {
                        setPageLoaded(false)
                    }
                } alwaysUi {
                    progress?.visibility = View.GONE
                } failUi {
                    showError()
                }
            }
        }
    }

    private fun loadMarkers() {
        file?.let { f ->
            loadPromise = markersRepository.fetch(f.docId, f.id) then {
                f.descriptors = it
                documentView?.run {
                    setMarkers(f.descriptors, markerId)
                    notifyMarkerListChanged()
                    fileId = f.id
                    docId = f.docId
                }
            } fail {
                Timber.d(it)
            }
        }
    }

    fun startEdit(type: Int) {
        documentView.startEdit(Marker(type))
        isEdit.value = true
    }

    fun stopEdit() {
        documentView.stopEdit()
        isEdit.value = false
    }

    fun share() {
        task {
            FileUtils.createTempImageFile(context, true)
        } then {
            FileOutputStream(it).apply {
                documentView.sharePicture.compress(Bitmap.CompressFormat.PNG, 100, this)
                close()
            }
            it
        } successUi {
            val store: ChatStore by inject()
            store.attachment = Uri.fromFile(it)
            startActivity(Intent(context, ChatActivity::class.java))
        } fail {
            Timber.e(it)
        }
    }

    private fun setPageLoaded(loaded: Boolean) {
        mListener?.setPageLoaded(loaded)
    }

    override fun markerAdd(marker: Marker) {
        file?.run {
            marker.docId = docId
            marker.fileId = id
            mListener?.onMarkerAdd(marker)
        }
    }

    override fun onMarkerAdded() {

    }

    override fun onClick(m: Marker) {
        var builder: MaterialDialog = CustomDialog[context!!].message(text = m.text)
        var title = m.getTypeName(context)

        val documentsRepository: DocumentsRepository = getKoin().get() // TODO

        if (m.type == Marker.LINK || m.type == Marker.EDIT) {
            val params = m.params as LinkParams
            val document = documentsRepository.find(params.refDocId)

            document?.let { doc ->
                val file = DocumentUtils.findFile(doc, params.refFileId)
                val url = Configuration.baseUrl + if (file == null) doc.file.thumbUrl else file.thumbUrl
                builder = CustomDialog
                        .getImageWithTextDialog(context!!, url, if (m.type == Marker.EDIT) m.text else m.title)
                        .negativeButton(R.string.common_goto) { d ->
                            mListener!!.onPageChange(doc, file)
                        }
            }
        }

        if (m.type == Marker.NOTE) {
            val params = m.params as NoteParams
            if (params.attachments != null && !params.attachments.isEmpty()) {
                val attach = getString(R.string.editor_attach_count, params.attachments.size)
                title = "$title $attach"
            }

            if (params.isDraft)
                builder.negativeButton(R.string.editor_edit) { mListener!!.onMarkerEdit(m) }
            else
                builder.negativeButton(R.string.editor_view) { mListener!!.onMarkerView(m) }
        }

        builder.title(text = title)
                .positiveButton(R.string.editor_ok)
                .show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == DocumentMarkerActivity.ACTION_CREATE) {
            when (resultCode) {
                DocumentMarkerActivity.EDITOR_OK -> {
                    file?.descriptors?.add(EditableMarker.get())
                    documentView.setMarkers(file!!.descriptors)
                    documentView.notifyMarkerListChanged()
                }

                DocumentMarkerActivity.EDITOR_ERROR -> {
                    // TODO обработать ошибку сохранения
                    file?.descriptors?.add(EditableMarker.get())
                    documentView.setMarkers(file!!.descriptors)
                    documentView.notifyMarkerListChanged()
                }

                DocumentMarkerActivity.EDITOR_CANCEL -> documentView.stopEdit()
            }
        } else if (requestCode == DocumentMarkerActivity.ACTION_EDIT) {
            when (resultCode) {
                DocumentMarkerActivity.EDITOR_OK -> {
                    documentView.updateMarker(EditableMarker.get())
                    documentView.notifyMarkerListChanged()
                }

                DocumentMarkerActivity.EDITOR_ERROR ->
                    // TODO обработать ошибку сохранения
                    documentView.stopEdit()

                DocumentMarkerActivity.EDITOR_CANCEL -> documentView.stopEdit()
            }
        }

        EditableMarker.destroy()
    }


    companion object {
        const val ARG_DOCUMENT = "document"
        const val ARG_FILE = "file"
        const val ARG_MARKER_ID = "marker_id"
        const val BUNDLE_FILENAME = "filename"
        const val BUNDLE_STATE = "imageviewstate"
        const val BUNDLE_DOCUMENT = "document"

        fun newInstance(document: Document, file: ProjectFile, markerId: Int = 0): DocumentFragment {
            return DocumentFragment().apply {
                arguments = bundleOf(
                        ARG_DOCUMENT to document,
                        ARG_FILE to file,
                        ARG_MARKER_ID to markerId
                )
            }
        }
    }
}
