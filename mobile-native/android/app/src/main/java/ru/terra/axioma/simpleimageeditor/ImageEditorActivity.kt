package ru.terra.axioma.simpleimageeditor

import androidx.lifecycle.ViewModelProvider
import android.content.Intent
import androidx.databinding.DataBindingUtil
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import android.view.Gravity
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.PopupWindow

import com.davemorrissey.labs.subscaleview.ImageSource
import com.google.common.collect.Lists
import org.koin.android.ext.android.getKoin

import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException

import ru.terra.axioma.App
import ru.terra.axioma.BR
import ru.terra.axioma.Constants
import ru.terra.axioma.FileUtils
import ru.terra.axioma.R
import ru.terra.axioma.client.ClientApi
import ru.terra.axioma.databinding.ActivityImageEditorBinding
import ru.terra.axioma.databinding.EditorPopupMenuBinding
import ru.terra.axioma.documentmarker.DocumentMarkerActivity
import ru.terra.axioma.models.ProjectFile
import ru.terra.axioma.models.marker.Marker
import ru.terra.axioma.views.imageeditor.OnMarkerListener

class ImageEditorActivity : AppCompatActivity(), OnMarkerListener, ImageEditorNavigator {

    private var binding: ActivityImageEditorBinding? = null

    private var actionBar: ActionBar? = null
    private var popupWindow: PopupWindow? = null
    private var file: ProjectFile? = null
    private val menu: Menu? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_image_editor)

        binding!!.toolbar.setTitleTextAppearance(this, R.style.ToolbarTitle)
        setSupportActionBar(binding!!.toolbar)
        actionBar = supportActionBar

        setupVariables()
        setupActionBar()
        setupEditorMenu()

        val client = getKoin().get<ClientApi>()

        val viewModel = ViewModelProvider.AndroidViewModelFactory.getInstance(application).create(ImageEditorViewModel::class.java)
        binding!!.setVariable(BR.viewModel, viewModel)
        viewModel.setNavigator(this)
        viewModel.onActivityCreate(client, file!!)

        binding!!.documentView.setOnMarkerInteraction(this)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        //        getMenuInflater().inflate(R.menu.menu_viewer, menu);
        //        menu.getItem(0).setEnabled(false);
        //        this.menu = menu;
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.share -> onShare()
            R.id.editor -> showEditorMenu()
        }
        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu): Boolean {
        return super.onPrepareOptionsMenu(menu)
    }

    private fun setupActionBar() {
        binding!!.toolbar.setNavigationOnClickListener { view -> onBackPressed() }

        if (actionBar == null)
            return

        actionBar!!.setDisplayHomeAsUpEnabled(true)
        binding!!.toolbar.contentInsetStartWithNavigation = 0
    }

    private fun setupVariables() {
        file = intent.getParcelableExtra(FILE)
        if (file!!.descriptors == null)
            file!!.descriptors = Lists.newArrayList()
    }

    private fun setupEditorMenu() {
        popupWindow = PopupWindow(this)

        val inflater = LayoutInflater.from(this)
        val binding = DataBindingUtil.inflate<EditorPopupMenuBinding>(inflater, R.layout.editor_popup_menu, null, true)
        binding.isLite = true

        popupWindow!!.isFocusable = true
        popupWindow!!.isOutsideTouchable = true
        popupWindow!!.width = WindowManager.LayoutParams.WRAP_CONTENT
        popupWindow!!.height = WindowManager.LayoutParams.WRAP_CONTENT
        popupWindow!!.contentView = binding.root
        popupWindow!!.setBackgroundDrawable(ColorDrawable(Color.WHITE))
        popupWindow!!.elevation = 0f

        binding.root.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED)
    }

    private fun showEditorMenu() {
        popupWindow!!.showAtLocation(binding!!.root, Gravity.TOP or Gravity.END, 0, binding!!.appBar.bottom)
        val rootView = popupWindow!!.contentView

        val textBtn = rootView.findViewById<Button>(R.id.textMarkButton)
        textBtn.setOnClickListener { v -> onEditorClick(Marker.TEXT) }

        val rectBtn = rootView.findViewById<Button>(R.id.rectMarkButton)
        rectBtn.setOnClickListener { v -> onEditorClick(Marker.RECT) }

        val pathBtn = rootView.findViewById<Button>(R.id.pathMarkButton)
        pathBtn.setOnClickListener { v -> onEditorClick(Marker.LINE) }
    }

    private fun onShare() {
        val share = binding!!.documentView.sharePicture
        //        documentView.setImage(ImageSource.bitmap(share));

        var tmp: File? = null
        try {
            val tempDir = FileUtils.createTempDir(this)
            tmp = FileUtils.createTempPNG(this, tempDir)
        } catch (e: IOException) {
            e.printStackTrace()
        }

        if (tmp != null) {
            var stream: FileOutputStream? = null
            try {
                stream = FileOutputStream(tmp)
                share.compress(Bitmap.CompressFormat.PNG, 100, stream)
            } catch (e: FileNotFoundException) {
                e.printStackTrace()
            } finally {
                if (stream != null)
                    try {
                        stream.close()
                    } catch (e: IOException) {
                        e.printStackTrace()
                    } finally {
                        val data = Intent()
                        data.putExtra(Constants.ATTACHMENT_URI, Uri.fromFile(tmp))
                        setResult(RESULT_OK, data)
                        finishActivity(Constants.IMAGE_EDITOR_REQUEST)
                        finish()
                    }
            }
        }
    }

    private fun onEditorClick(type: Int) {
        binding!!.documentView.startEdit(Marker(type))
        popupWindow!!.dismiss()
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (data == null) {
            binding!!.documentView.stopEdit()
            return
        }

        if (requestCode == DocumentMarkerActivity.ACTION_CREATE) {
            val m: Marker
            when (resultCode) {
                DocumentMarkerActivity.EDITOR_OK -> {
                    m = data.getParcelableExtra(DocumentMarkerActivity.MARKER)
                    file!!.descriptors.add(m)
                    onMarkerAdded()
                    binding!!.documentView.notifyMarkerListChanged()
                }

                DocumentMarkerActivity.EDITOR_ERROR -> {
                    // TODO обработать ошибку сохранения
                    m = data.getParcelableExtra(DocumentMarkerActivity.MARKER)
                    file!!.descriptors.add(m)
                    onMarkerAdded()
                    binding!!.documentView.notifyMarkerListChanged()
                }

                DocumentMarkerActivity.EDITOR_CANCEL -> binding!!.documentView.stopEdit()
            }
        } else if (requestCode == DocumentMarkerActivity.ACTION_EDIT) {
            when (resultCode) {
                DocumentMarkerActivity.EDITOR_OK -> {
                    val m = data.getParcelableExtra<Marker>(DocumentMarkerActivity.MARKER)
                    binding!!.documentView.updateMarker(m)
                    binding!!.documentView.notifyMarkerListChanged()
                }

                DocumentMarkerActivity.EDITOR_ERROR ->
                    // TODO обработать ошибку сохранения
                    binding!!.documentView.stopEdit()

                DocumentMarkerActivity.EDITOR_CANCEL -> binding!!.documentView.stopEdit()
            }
        }
    }

    override fun setImage(uri: String) {
        binding!!.documentView.setImage(ImageSource.uri(uri))
        binding!!.documentView.setMarkers(file!!.descriptors)
    }

    override fun markerAdd(marker: Marker) {
        marker.docId = file!!.docId
        marker.fileId = file!!.id

        // TODO переделать тексты
        val mode = DocumentMarkerActivity.ACTION_CREATE
        val intent = Intent(this, DocumentMarkerActivity::class.java)
        intent.putExtra(DocumentMarkerActivity.MARKER, marker)
        intent.putExtra(DocumentMarkerActivity.CREATE_MODE, mode)
        startActivityForResult(intent, mode)
    }

    override fun onMarkerAdded() {
        menu!!.getItem(0).isEnabled = true
    }

    override fun onClick(m: Marker) {

    }

    companion object {
        val FILE = "file"
    }
}
