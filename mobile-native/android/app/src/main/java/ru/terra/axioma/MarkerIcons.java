package ru.terra.axioma;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import androidx.core.content.ContextCompat;
import android.util.SparseArray;

import ru.terra.axioma.models.marker.Marker;
import ru.terra.axioma.models.marker.NoteParams;

public class MarkerIcons {
    private final static int EDIT = 0;
    private final static int LINK = 1;
    private final static int NOTE = 2;
    private final static int DRAFT = 3;

    private SparseArray<Bitmap> icons;

    private static MarkerIcons INSTANCE;

    public static void init(Context context) {
        INSTANCE = new MarkerIcons();
        INSTANCE.generateIcons(context);
    }

    public static Bitmap get(int type) {
        return INSTANCE.icons.get(type);
    }

    public static int getType(Marker m) {
        if (m.type == Marker.NOTE && ((NoteParams) m.params).isDraft)
            return DRAFT;

        return m.type;
    }

    private void generateIcons(Context c) {
        icons = new SparseArray<>();
        icons.put(EDIT, makeIcon(c, R.drawable.ic_content_cut, R.color.red));
        icons.put(LINK, makeIcon(c, R.drawable.ic_link, R.color.teal));
        icons.put(NOTE, makeIcon(c, R.drawable.ic_star, R.color.purple));
        icons.put(DRAFT, makeIcon(c, R.drawable.ic_star, R.color.grey_600));
    }

    private Bitmap makeIcon(Context context, int iconRes, int colorRes) {
        Drawable background = ContextCompat.getDrawable(context, R.drawable.ic_label);
        assert background != null;
        background.mutate().setTint(ContextCompat.getColor(context, colorRes));

        Drawable image = ContextCompat.getDrawable(context, iconRes);

        Bitmap icon = Bitmap.createBitmap((int) (background.getIntrinsicWidth() / 1.5), (int) (background.getIntrinsicHeight() / 1.5), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(icon);
        background.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        background.draw(canvas);

        int x = (int) (canvas.getHeight() * 0.2);
        int y = x / 2;

        assert image != null;
        image.setBounds(x, y, canvas.getWidth() - x, canvas.getHeight() - (x + y));
        image.draw(canvas);

        return icon;
    }
}
