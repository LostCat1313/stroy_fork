package ru.terra.axioma.models.chat;

import java.util.List;

/**
 * Created by alexei on 23.01.18.
 */

public class RoomData {
    public Room room;
    public List<Message> messages;

    public void addMessage(Message message) {
        messages.add(message);
    }
}
