package ru.terra.axioma.documentcreator

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.list.listItems
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import pub.devrel.easypermissions.AppSettingsDialog
import pub.devrel.easypermissions.EasyPermissions
import pub.devrel.easypermissions.PermissionRequest
import ru.terra.axioma.*
import ru.terra.axioma.adapters.AttachPhotoAdapter
import ru.terra.axioma.databinding.ActivityDocumentCreatorBinding
import ru.terra.axioma.models.Document
import ru.terra.axioma.views.CustomDialog
import ru.terra.axioma.views.GridAutoFitLayoutManager
import ru.terra.axioma.views.GridSpacingItemDecorator
import timber.log.Timber

class DocumentCreatorActivity : AppCompatActivity(), AttachPhotoAdapter.OnClickListener, EasyPermissions.PermissionCallbacks, DocumentCreatorNavigator.View {

    private var binding: ActivityDocumentCreatorBinding? = null
    private var actions: DocumentCreatorNavigator.Action? = null

    private var progressDialog: MaterialDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_document_creator)

        setupActionBar()
        setupList()

        val parent = intent.getParcelableExtra<Document>(Constants.DOCUMENT)
        val viewModel: DocumentCreatorViewModel by viewModel { parametersOf(parent) }
        viewModel.setViewListener(this)

        actions = viewModel

        binding!!.viewModel = viewModel
    }

    private fun setupActionBar() {
        binding!!.toolbar.setTitleTextAppearance(this, R.style.ToolbarTitle)
        setSupportActionBar(binding!!.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        binding!!.toolbar.contentInsetStartWithNavigation = 0
        binding!!.toolbar.setNavigationOnClickListener { onBackPressed() }
    }

    private fun setupList() {
        val columns = 3
        val layoutManager = GridAutoFitLayoutManager(this, columns)
        binding!!.recyclerView.layoutManager = layoutManager
        binding!!.recyclerView.addItemDecoration(GridSpacingItemDecorator(columns, 40, false))
        binding!!.recyclerView.adapter = AttachPhotoAdapter(this)
    }

    private fun showFileDialog() {
        val items = listOf("Изображение", "PDF")

        MaterialDialog(this)
                .title(text = "Выберите тип файла")
                .listItems(items = items) { _, index, _ ->
                    if (index == 0)
                        galleryIntent()
                    else
                        pdfIntent()
                }.show()
    }

    private fun galleryIntent() {
        val intent = Intent()
        intent.type = "*/*"
        intent.action = Intent.ACTION_OPEN_DOCUMENT
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true)
        val mimeTypes = arrayOf("image/*")
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)
        startActivityForResult(intent/*Intent.createChooser(intent, getString(R.string.common_select_file))*/, Constants.GALLERY_REQUEST)
    }

    private fun pdfIntent() {
        val intent = Intent()
        intent.type = "*/*"
        intent.action = Intent.ACTION_OPEN_DOCUMENT
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true)
        val mimeTypes = arrayOf("application/pdf")
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)
        startActivityForResult(intent/*Intent.createChooser(intent, getString(R.string.common_select_file))*/, Constants.GALLERY_REQUEST)
    }

    override fun onAddFileClick() {
        val storagePerm = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE)
        if (EasyPermissions.hasPermissions(this, *storagePerm)) {
            showFileDialog()
        } else {
            EasyPermissions.requestPermissions(
                    PermissionRequest.Builder(this, Constants.GALLERY_REQUEST, *storagePerm)
                            .setRationale(R.string.card_message)
                            .setPositiveButtonText(R.string.editor_ok)
                            .setNegativeButtonText(R.string.common_cancel)
                            // .setTheme(R.style.my_fancy_style)
                            .build())
        }
    }

    override fun onAttachmentClick(uri: Uri) {
        val intent = Intent(this, ImageViewerActivity::class.java)
        intent.putExtra(ImageViewerActivity.URI, uri)
        startActivity(intent)
    }

    override fun onPermissionsGranted(requestCode: Int, perms: List<String>) {
        showFileDialog()
    }

    override fun onPermissionsDenied(requestCode: Int, perms: List<String>) {
        Timber.d("onPermissionsDenied:%s:%s", requestCode, perms.size)

        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            AppSettingsDialog.Builder(this).build().show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            actions!!.renderPage(data?.data!!)
        }
    }

    override fun cancelAndClose() {
        setResult(Activity.RESULT_CANCELED)
        finishActivity(Constants.CREATE_DOCUMENT_REQUEST)
        finish()
    }

    override fun close() {
        setResult(Activity.RESULT_OK)
        finishActivity(Constants.CREATE_DOCUMENT_REQUEST)
        finish()
    }

    override fun showRenderPdfProgress(max: Int) {
        CustomDialog.getLoadingDialog(this).show()

        //TODO("Диалог с прогрессом")
        /*runOnUiThread {
            progressDialog = CustomDialog.get(this@DocumentCreatorActivity)
                    .content(R.string.please_wait)
                    .progress(false, max, true)
                    .canceledOnTouchOutside(false)
                    .positiveText(R.string.editor_ok)
                    .negativeText(R.string.common_cancel)
                    .onNegative({ dialog, which ->
                        actions!!.cancelPdfRenderTask()
                        dialog.cancel()
                    })
                    .show()

            progressDialog!!.getActionButton(DialogAction.POSITIVE).setVisibility(View.GONE)
        }*/

        progressDialog = MaterialDialog(this)
                .message(R.string.please_wait)
                .cancelOnTouchOutside(false)
                .cancelable(false)

        progressDialog?.show()
    }

    override fun updateProgress(value: Int, max: Int) {
        //TODO("Диалог с прогрессом")
        runOnUiThread {
            if (progressDialog == null || !progressDialog!!.isShowing)
                return@runOnUiThread

//            progressDialog!!.setProgress(value)

            if (value == max) {
                progressDialog?.dismiss()
            }
        }
    }

    override fun showError(error: Int) {
        dismissProgress()
        CustomDialog.error(this, error).show()
    }

    override fun showError(error: String) {
        dismissProgress()
        CustomDialog.error(this, error).show()
    }

    override fun showLoadingToServerProgress() {
        progressDialog = MaterialDialog(this)
                .message(R.string.common_saving)
                .cancelOnTouchOutside(false)
                .cancelable(false)

        progressDialog?.show()
    }

    override fun dismissProgress() {
        progressDialog?.dismiss()
    }
}
