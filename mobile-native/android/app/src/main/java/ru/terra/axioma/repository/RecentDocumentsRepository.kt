package ru.terra.axioma.repository

import androidx.lifecycle.MutableLiveData
import org.koin.core.KoinComponent

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import ru.terra.axioma.client.ClientApi
import ru.terra.axioma.models.Document
import ru.terra.axioma.models.DocumentsList

class RecentDocumentsRepository constructor(private val client: ClientApi) : KoinComponent {
    var docs: MutableLiveData<List<Document>> = MutableLiveData()
        private set
    val loading = MutableLiveData<Boolean>()

    fun fetch() {
        client.recentDocuments.enqueue(object : Callback<DocumentsList> {
            override fun onResponse(call: Call<DocumentsList>, response: Response<DocumentsList>) {
                val data = response.body()
                if (data != null)
                    docs.postValue(data.list)

                loading.value = true
            }

            override fun onFailure(call: Call<DocumentsList>, t: Throwable) {
                //                show(); // TODO показывать ошибку
                loading.value = true
            }
        })
    }

    fun update() {
        fetch()
    }
}
