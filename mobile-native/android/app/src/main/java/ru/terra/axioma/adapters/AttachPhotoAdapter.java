package ru.terra.axioma.adapters;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import ru.terra.axioma.Configuration;
import ru.terra.axioma.R;

/**
 * Created by alexei on 06.01.18.
 */

public class AttachPhotoAdapter extends RecyclerView.Adapter<AttachPhotoAdapter.ViewHolder> {
    private List<AttachPhoto> objects;
    private OnClickListener clickListener;
    private boolean isEditable;

    public AttachPhotoAdapter(OnClickListener clickListener) {
        objects = new ArrayList<>();
        this.clickListener = clickListener;
        isEditable = true;
    }

    public void setEditable(boolean editable) {
        isEditable = editable;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        SimpleDraweeView image;
        FrameLayout frame;
        ImageButton remove;

        ViewHolder(View view) {
            super(view);

            image = view.findViewById(R.id.photo);
            frame = view.findViewById(R.id.frame);
            remove = view.findViewById(R.id.remove);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int type) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(
                type == 0 ? R.layout.attach_photo_action_item : R.layout.attach_photo_item,
                parent,
                false
        );

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (getItemViewType(position) == 0 && holder.frame != null) {
            holder.frame.setOnClickListener(v -> clickListener.onAddFileClick());
        } else {
            AttachPhoto data = objects.get(position);

            if (holder.image != null) {
                Uri uri = data.uri.toString().startsWith("file") ? data.uri : Uri.parse(Configuration.baseUrl + data.uri.toString());
                holder.image.setImageURI(uri);
                holder.image.setOnClickListener(v -> clickListener.onAttachmentClick(uri));
            }

            if (holder.remove != null) {
                holder.remove.setVisibility(isEditable ? View.VISIBLE : View.GONE);
                holder.remove.setOnClickListener(v -> {
                    int pos = objects.indexOf(data);
                    objects.remove(data);
                    notifyItemRemoved(pos);
                });
            }
        }
    }

    @Override
    public int getItemCount() {
        return (objects == null || objects.isEmpty() ? 0 : objects.size()) + (isEditable ? 1 : 0);
    }

    @Override
    public int getItemViewType(int position) {
        return (isEditable && position == getItemCount() - 1) ? 0 : 1;
    }

    public void setData(List<AttachPhoto> data) {
        this.objects = data;
        notifyDataSetChanged();
    }

    public void addData(List<AttachPhoto> data) {
        this.objects.addAll(data);
        notifyDataSetChanged();
    }

    public List<Uri> getData() {
        return Lists.transform(this.objects, input -> input.uri);
    }


    public void addValue(Uri value) {
        this.objects.add(new AttachPhoto(value));
        notifyDataSetChanged();
    }

    public interface OnClickListener {
        void onAddFileClick();
        void onAttachmentClick(Uri uri);
    }

    public static class AttachPhoto {
//        public boolean isRemovable;
        public Uri uri;

        public AttachPhoto(Uri uri) {
//            this.isRemovable = isRemovable;
            this.uri = uri;
        }
    }
}