package ru.terra.axioma.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import ru.terra.axioma.R;
import ru.terra.axioma.models.Document;

/**
 * Created by alexei on 9/27/17.
 */

public class RecentDocsAdapter extends RecyclerView.Adapter<RecentDocsAdapter.ViewHolder> {
    private List<Document> objects;
    private OnClickListener onClickListener;

    public RecentDocsAdapter() {
    }

    public RecentDocsAdapter(OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView title;

        public ViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.title);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int type) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.recent_list_item, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Document doc = objects.get(position);

        if (doc != null)
            holder.title.setText(doc.title);

        if (onClickListener != null)
            holder.itemView.setOnClickListener(view -> onClickListener.onClick(doc));
    }

    @Override
    public int getItemCount() {
        return objects == null ? 0 : objects.size();
    }

    public void setData(List<Document> data) {
        objects = data;
        notifyDataSetChanged();
    }

    public interface OnClickListener {
        void onClick(Document document);
    }
}
