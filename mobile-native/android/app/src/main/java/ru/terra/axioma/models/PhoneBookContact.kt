package ru.terra.axioma.models

import android.net.Uri

import ru.terra.axioma.StringUtils

class PhoneBookContact() {
    var id: String = ""
    var user: User? = null

    var firstName: String = ""
    var lastName: String = ""
    var phoneNumber: String = ""
//    var position: Position? = null

//    var photo: Bitmap = null
    var photoURI: Uri? = null
    var isServerContact: Boolean = false

    val fullName: String
        get() = StringUtils.concatStrings(firstName, lastName)

    /*constructor(id: String, username: String, firstName: String, lastName: String, phoneNumber: String, position: Position, isServerContact: Boolean) {
        this.id = id
        this.username = username
        this.firstName = firstName
        this.lastName = lastName
        this.phoneNumber = phoneNumber
        this.position = position
        this.isServerContact = isServerContact
    }*/

//    fun setPhoto(photo: Bitmap?) {
//        if (photo == null) {
//            val generator = ColorGenerator.MATERIAL
//            val drawable = ColorDrawable(generator.getColor(fullName))
//            this.photo = Bitmap.createBitmap(10, 10, Bitmap.Config.ARGB_8888)
//            drawable.draw(Canvas(this.photo))
//        } else
//            this.photo = photo
//    }
}
