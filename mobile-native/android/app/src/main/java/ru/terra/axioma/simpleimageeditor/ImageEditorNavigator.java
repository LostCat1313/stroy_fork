package ru.terra.axioma.simpleimageeditor;

public interface ImageEditorNavigator {
    void setImage(String uri);
}
