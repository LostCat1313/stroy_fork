package ru.terra.axioma.models;

import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.Locale;

import ru.terra.axioma.models.chat.Message;
import ru.terra.axioma.models.marker.Marker;

import static ru.terra.axioma.Constants.NotificationType.CHAT_MESSAGE;
import static ru.terra.axioma.Constants.NotificationType.CHAT_ROOM_ADDED;
import static ru.terra.axioma.Constants.NotificationType.DOCUMENT_ADDED;
import static ru.terra.axioma.Constants.NotificationType.DOCUMENT_DESCRIPTOR_ADDED;
import static ru.terra.axioma.Constants.NotificationType.DOCUMENT_REMOVED;

/**
 * Created by alexei on 9/27/17.
 */

public class Notification extends BaseObject {
    public int type;
    public String title;
    public int projectId;
    public int targetId;
    public String organization;
    public String content;
    public Message message;
    public Document document;
    @SerializedName("descriptor")
    public Marker marker;

    public String getCreatedDate() {
        if (createdAt == null)
            return "";

        SimpleDateFormat format = new SimpleDateFormat("hh:mm:ss", new Locale("ru"));
        return format.format(createdAt);
    }

    public boolean isChat() {
        return type == CHAT_MESSAGE || type == CHAT_ROOM_ADDED;
    }

    public boolean isDocument() {
        return type == DOCUMENT_ADDED || type == DOCUMENT_REMOVED;
    }

    public boolean isMarker() {
        return type == DOCUMENT_DESCRIPTOR_ADDED;
    }
}
