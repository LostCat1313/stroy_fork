package ru.terra.axioma.views

import android.content.Context
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.annotation.RequiresApi

import java.text.SimpleDateFormat
import java.util.Locale

import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import ru.terra.axioma.Configuration
import ru.terra.axioma.R
import ru.terra.axioma.databinding.ProjectCardBinding
import ru.terra.axioma.models.Project

class ProjectCard(context: Context) : LinearLayout(context) {
    private var binding: ProjectCardBinding? = null

    init {
        orientation = VERTICAL
        binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.project_card, this, true)

        val color = ContextCompat.getColor(context, R.color.grey_600)
        val drawable = ContextCompat.getDrawable(context, R.drawable.zzz_box_shadow)
        drawable!!.mutate().setTint(color)

        binding!!.icon.setImageDrawable(drawable)

        val progress = CircleProgressBarDrawable()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            progress.backgroundColor = context.resources.getColor(R.color.grey, null)
            progress.color = context.resources.getColor(R.color.grey_600, null)
        } else {
            progress.backgroundColor = context.resources.getColor(R.color.grey)
            progress.color = context.resources.getColor(R.color.grey_600)
        }
    }

    fun setData(project: Project, isOwner: Boolean) {
        binding?.apply {
            title.text = project.title

            val f = SimpleDateFormat("dd.MM.yyyy", Locale.ROOT)
            created.text = f.format(project.createdAt)

            users.text = context.getString(R.string.project_users, project.usersCount)
            documents.text = context.getString(R.string.project_documents, project.documentsCount)

            project.image?.let {
                cover.setImageURI(Uri.parse(Configuration.baseUrl + it.thumbUrl), this)
            } ?: let {
                cover.setActualImageResource(R.drawable.login_bg)
            }

            btnInfo.visibility = if (isOwner) View.VISIBLE else View.GONE
        }
    }

    fun setMargin() {
        (binding?.cardView?.layoutParams as MarginLayoutParams).topMargin = context.resources.getDimensionPixelSize(R.dimen.card_out_margin)
    }

    fun setOnButtonClickListeners(onSelect: View.OnClickListener, onInfo: View.OnClickListener) {
        binding?.btnSelect?.setOnClickListener(onSelect)
        binding?.btnInfo?.setOnClickListener(onInfo)
    }
}
