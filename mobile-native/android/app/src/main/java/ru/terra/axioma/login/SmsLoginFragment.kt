package ru.terra.axioma.login

import android.content.Context
import android.graphics.PorterDuff
import android.os.Bundle
import android.telephony.PhoneNumberFormattingTextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.viewpager.widget.ViewPager
import com.afollestad.materialdialogs.callbacks.onDismiss
import com.google.firebase.FirebaseException
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider
import kotlinx.android.synthetic.main.fragment_sms_login.*
import nl.komponents.kovenant.then
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import ru.terra.axioma.R

import ru.terra.axioma.views.CustomDialog
import java.util.concurrent.TimeUnit


class SmsLoginFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_sms_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val viewModel: LoginViewModel by sharedViewModel()

        viewPager.apply {
            adapter = ViewPagerAdapter()
            addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
                override fun onPageScrollStateChanged(state: Int) {}

                override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}

                override fun onPageSelected(position: Int) {
                    page1.isSelected = position == 0
                    page2.isSelected = position == 1
                }
            })

            page1.isSelected = this.currentItem == 0
            page2.isSelected = this.currentItem == 1
        }

        progress.indeterminateDrawable.setColorFilter(ContextCompat.getColor(context!!, android.R.color.white), PorterDuff.Mode.SRC_IN)

        viewModel.showLoading.observe(this, Observer {
            viewPager.visibility = if (it) View.GONE else View.VISIBLE
            progress.visibility = if (it) View.VISIBLE else View.GONE
        })

        viewModel.showError.observe(this, Observer {
            it?.let { message ->
                CustomDialog[context!!]
                        .title(R.string.common_error)
                        .message(text = message)
                        .onDismiss { viewModel.showLoading.postValue(false) }
                        .show()
            }
        })

        viewModel.showActivateScreen.observe(this, Observer { viewPager.currentItem = if (it) 1 else 0 })

        viewModel.code.observe(this, Observer {
            code.setText(it)
        })

        phone.addTextChangedListener(PhoneNumberFormattingTextWatcher("RU"))

        send.setOnClickListener {
            val p = phone.text.toString().replace("-", "").replace(" ", "")
            viewModel.signUp(p) then {
                try {
                    PhoneAuthProvider.getInstance().verifyPhoneNumber(p, 120, TimeUnit.SECONDS, activity!!, viewModel.verificationCallback)
                    viewModel.showActivateScreen.postValue(true)
                } catch (e: Exception) {
                    viewModel.showError.value = getString(R.string.login_auth_code_error)
                }
            }
        }
        activate.setOnClickListener { viewModel.activate(code.text.toString()) }
    }
}
