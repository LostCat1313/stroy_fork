package ru.terra.axioma.main


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.afollestad.materialdialogs.MaterialDialog
import kotlinx.android.synthetic.main.fragment_recent_document.*
import nl.komponents.kovenant.ui.successUi
import org.koin.android.ext.android.getKoin
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.sharedViewModel
import ru.terra.axioma.R
import ru.terra.axioma.adapters.RecentDocsAdapter
import ru.terra.axioma.models.Document
import ru.terra.axioma.repository.DocumentsRepository
import ru.terra.axioma.repository.ProjectsRepository
import ru.terra.axioma.store.CurrentSessionStore
import ru.terra.axioma.views.LineDividerItemDecorator

class RecentDocumentsFragment : Fragment() {

    private val documentsRepository: DocumentsRepository by inject()
    private val currentSessionStore: CurrentSessionStore by inject()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_recent_document, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val vm: MainViewModel by sharedViewModel()

        val adapter = RecentDocsAdapter { document ->
            checkProject(document, { d -> vm.onSameFunction(d) }, { d -> vm.onDifferentFunction(doc = d) })
        }
        val layoutManager = LinearLayoutManager(recyclerView.context)
        recyclerView.addItemDecoration(LineDividerItemDecorator(recyclerView.context, false))
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter

        vm.docs.observe(this, Observer { adapter.setData(it) })
    }

    private fun checkProject(document: Document, onSame: (Document) -> Unit, onDifferent: (Document) -> Unit) {
        val pr: ProjectsRepository = getKoin().get()

        val fn = {
            if (currentSessionStore.project == null || currentSessionStore.project!!.id != document.projectId) {
                pr.getProject(document.projectId)?.let { p ->
                    context?.let {
                        MaterialDialog(it)
                                .message(text = getString(R.string.project_choose_another_project, p.title))
                                .negativeButton(R.string.common_cancel)
                                .positiveButton(R.string.editor_ok) {
                                    currentSessionStore.project = p
                                    documentsRepository.clear2()
                                    onDifferent(document)
                                }
                                .show()
                    }
                }
            } else
                onSame(document)
        }

        if (pr.projects.value?.isEmpty() != false) {
            pr.fetch() successUi {
                fn()
            }
        } else
            fn()
    }

    companion object {
        fun newInstance(): RecentDocumentsFragment {
            return RecentDocumentsFragment()
        }
    }
}
