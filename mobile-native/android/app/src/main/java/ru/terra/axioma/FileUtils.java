package ru.terra.axioma;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import androidx.annotation.NonNull;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by alexei on 30.01.18.
 */

public class FileUtils {
    public static final String JPEG = ".jpg";
    public static final String PDF = ".pdf";
    public static final String PNG = ".png";

    public static String getPath(Context context, Uri input) {
        String filePath = "";
        Uri uri = input;

        if (uri.toString().startsWith("text://com.google.android.apps.photos.text")) {
            String str = uri.getPath();
            String newPath = str.substring(str.indexOf("text"), str.lastIndexOf("/ACTUAL"));
            uri = Uri.parse(newPath);
        }

        List<String> paths = uri.getPathSegments();
        int pathsSize = paths.size();
        String wholeID = pathsSize == 0 ? "" : paths.get(pathsSize - 1);

        String[] ids = wholeID.split(":");
        String id = ids.length > 1 ? ids[1] : wholeID;

        String[] column = { MediaStore.Images.Media.DATA };

        String selection = MediaStore.Images.Media._ID + "=?";

        Cursor cursor = context.getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                column,
                selection,
                new String[]{id},
                null
        );

        if (cursor != null) {
            int columnIndex = cursor.getColumnIndex(column[0]);

            if (cursor.moveToFirst()) {
                filePath = cursor.getString(columnIndex);
            }
            cursor.close();
        }

        return filePath;
    }

//    public static String getFilePath(Context context, Uri newPageUri) {
//        String selection = null;
//        String[] selectionArgs = null;
//        if (DocumentsContract.isDocumentUri(context, newPageUri)) {
//            if (context.isExternalStorageDocument(newPageUri)) {
//                final String docId = DocumentsContract.getDocumentId(newPageUri);
//                final String[] split = docId.split(":");
//                return Environment.getExternalStorageDirectory() + "/" + split[1];
//            } else if (isDownloadsDocument(newPageUri)) {
//                final String id = DocumentsContract.getDocumentId(newPageUri);
//                newPageUri = ContentUris.withAppendedId(
//                        Uri.parse("text://downloads/public_downloads"), Long.valueOf(id));
//            } else if (isMediaDocument(newPageUri)) {
//                final String docId = DocumentsContract.getDocumentId(newPageUri);
//                final String[] split = docId.split(":");
//                final String type = split[0];
//                if ("image".equals(type)) {
//                    newPageUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
//                } else if ("video".equals(type)) {
//                    newPageUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
//                } else if ("audio".equals(type)) {
//                    newPageUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
//                }
//                selection = "_id=?";
//                selectionArgs = new String[]{
//                        split[1]
//                };
//            }
//        }
//        if ("text".equalsIgnoreCase(newPageUri.getScheme())) {
//            String[] projection = {
//                    MediaStore.Images.Media.DATA
//            };
//            Cursor cursor = null;
//            try {
//                cursor = context.getContentResolver()
//                        .query(newPageUri, projection, selection, selectionArgs, null);
//                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
//                if (cursor.moveToFirst()) {
//                    return cursor.getString(column_index);
//                }
//            } catch (Exception e) {
//            }
//        } else if ("file".equalsIgnoreCase(newPageUri.getScheme())) {
//            return newPageUri.getPath();
//        }
//        return null;
//    }

    public static File createTempFile(File parent, String type) throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = timeStamp + "_";
        return File.createTempFile(imageFileName, type, parent);
    }

    public static File createTempDir(Context context) throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        File file = new File(context.getCacheDir() + File.separator + timeStamp);

        if (file.mkdir())
            return file;
        else
            return null;
    }

    public static File createTempImageFile(Context context, boolean internal) throws IOException {
        File storageDir;
        if (internal)
            storageDir = context.getFilesDir();
        else
            storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        return createTempFile(storageDir, JPEG);
    }

    public static File createTempPdfFile(Context context, File parent) throws IOException {
        return createTempFile(parent, PDF);
    }

    public static File createTempJPEG(File parent) throws IOException {
        return createTempFile(parent, JPEG);
    }

    public static File createTempPNG(Context context, File parent) throws IOException {
        return createTempFile(parent, PNG);
    }

    public static void writeToFile(Context context, String from, File to) throws IOException {
        File pdf = new File(from);
        InputStream inputStream = context.getContentResolver().openInputStream(Uri.fromFile(pdf));
        FileOutputStream output = new FileOutputStream(to);
        final byte[] buffer = new byte[1024];
        int size;
        while ((size = inputStream.read(buffer)) != -1) {
            output.write(buffer, 0, size);
        }
        inputStream.close();
        output.close();
    }

    @NonNull
    public static MultipartBody.Part getFilePart(String name, Uri uri) {
        File file = new File(uri.getPath());
        RequestBody requestFile = RequestBody.create(MediaType.parse("image"), file);

        return MultipartBody.Part.createFormData(name, file.getName(), requestFile);
    }
}
