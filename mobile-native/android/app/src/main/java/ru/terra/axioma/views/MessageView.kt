package ru.terra.axioma.views

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import ru.terra.axioma.R
import ru.terra.axioma.models.ProjectFile
import ru.terra.axioma.models.chat.Message


class MessageView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyle: Int = 0
) : FrameLayout(context, attrs, defStyle) {

    init {
        LayoutInflater.from(context).inflate(R.layout.message_view, this, true)
    }

    fun setMessage(
            message: Message,
            userId: Int,
            onAttachmentClick: (ProjectFile) -> Unit = {}
    ) {
        if (childCount > 0) {
            val child = getChildAt(0)
            if (message.senderId == userId && child is MessageOutgoingView) {
                child.setMessage(message, onAttachmentClick)
                return
            } else if (message.senderId != userId && child is MessageIncomingView) {
                child.setMessage(message, onAttachmentClick)
                return
            } else {
                removeAllViews()
            }
        }

        val v: MessageBaseView = if (message.senderId == userId)
            MessageOutgoingView(context)
        else
            MessageIncomingView(context)

        v.setMessage(message, onAttachmentClick)
        addView(v)
    }
}