package ru.terra.axioma.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import ru.terra.axioma.R;

/**
 * Created by alexei on 20.01.17.
 */

public class LineDividerItemDecorator extends RecyclerView.ItemDecoration {
    private RectF divider;
    private Paint paint;
    private boolean drawDividerFirst;
    private int leftMargin = 0;

    public static LineDividerItemDecorator withLeftMargin(Context context) {
        return new LineDividerItemDecorator(context, android.R.color.black, false, true);
    }

    public LineDividerItemDecorator(Context context) {
        this(context, android.R.color.black, true, false);
    }

    public LineDividerItemDecorator(Context context, boolean drawDividerFirst) {
        this(context, android.R.color.black, drawDividerFirst, false);
    }

    private LineDividerItemDecorator(Context context, int colorId, boolean drawDividerFirst, boolean withLeftMargin) {
        divider = new RectF();
        paint = new Paint();
        paint.setColor(ContextCompat.getColor(context, colorId));
        this.drawDividerFirst = drawDividerFirst;
        if (withLeftMargin)
            leftMargin = 2 * context.getResources().getDimensionPixelSize(R.dimen.base_margin)
                    + context.getResources().getDimensionPixelSize(R.dimen.contact_icon_size);
    }

    @Override
    public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
        int right = parent.getWidth() - parent.getPaddingRight();

        int childCount = parent.getChildCount();
        for (int i = 0; i < childCount - 1; i++) {
            View child = parent.getChildAt(i);

            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

            int top = child.getBottom() + params.bottomMargin;
            float bottom = top + 1.5f;

            if (i == 0 && drawDividerFirst) {
                divider.set(leftMargin, top - child.getHeight(), right, bottom - child.getHeight());
                c.drawRect(divider, paint);
            }

            divider.set(leftMargin, top, right, bottom);
            c.drawRect(divider, paint);
        }
    }
}
