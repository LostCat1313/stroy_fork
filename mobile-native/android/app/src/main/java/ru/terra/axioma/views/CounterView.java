package ru.terra.axioma.views;

import android.content.Context;
import android.graphics.Color;
import androidx.core.content.ContextCompat;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import ru.terra.axioma.R;


public class CounterView extends RelativeLayout {
    TextView textView;

    public CounterView(Context context) {
        super(context);
        init();
    }

    private void init() {
        textView = new TextView(getContext());
        int padding = getResources().getDimensionPixelSize(R.dimen.drawer_counter_padding);
        textView.setPadding(padding, 0, padding, 0);
        textView.setTextColor(Color.BLACK);

        LayoutParams lp = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.addRule(CENTER_IN_PARENT);
        textView.setLayoutParams(lp);

        addView(textView);
    }

    public void setText(String text) {
        textView.setText(text);
    }

    public void setTextAndBackground(String text, int backgroundRes) {
        textView.setText(text);
        textView.setBackground(ContextCompat.getDrawable(getContext(), backgroundRes));
    }

    public void setTextAndBackground(int text, int backgroundRes) {
        setTextAndBackground(String.valueOf(text), backgroundRes);
    }
}
