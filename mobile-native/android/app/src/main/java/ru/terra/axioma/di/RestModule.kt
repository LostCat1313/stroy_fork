package ru.terra.axioma.di

import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidApplication
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import ru.terra.axioma.Configuration
import ru.terra.axioma.client.ClientApi
import ru.terra.axioma.login.LoginViewModel
import ru.terra.axioma.store.CurrentSessionStore
import ru.terra.axioma.store.UserStore

val restModule = module {
    single { provideDefaultOkHttpClient(get()) }
    single { provideRetrofit(get()) }
    single { provideService(get()) }
}

fun provideDefaultOkHttpClient(currentSessionStore: CurrentSessionStore): OkHttpClient {
    return OkHttpClient.Builder()
            .addInterceptor { chain ->
                val original = chain.request()
                val request = original.newBuilder()
                        .addHeader("Content-Type", "application/json")
                        .addHeader("Authorization", "Bearer " + currentSessionStore.token)
                        .method(original.method(), original.body())
                        .build()

                chain.proceed(request)
            }
            .build()
}

fun provideRetrofit(client: OkHttpClient): Retrofit {
    return Retrofit.Builder()
            .baseUrl(Configuration.baseUrl)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
}

fun provideService(retrofit: Retrofit): ClientApi = retrofit.create(ClientApi::class.java)