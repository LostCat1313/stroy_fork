package ru.terra.axioma.documentviewer

import ru.terra.axioma.models.Document
import ru.terra.axioma.models.ProjectFile
import ru.terra.axioma.models.marker.Marker

interface OnDocumentFragmentListener {
    fun onMarkerAdd(m: Marker)
    fun onMarkerEdit(m: Marker)
    fun onMarkerView(m: Marker)
    fun onPageChange(d: Document, f: ProjectFile?)
    fun setPageLoaded(loaded: Boolean)
}