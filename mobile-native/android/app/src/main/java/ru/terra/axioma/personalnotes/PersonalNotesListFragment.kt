package ru.terra.axioma.personalnotes


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.fragment_personal_notes_list.*
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.sharedViewModel
import ru.terra.axioma.R
import ru.terra.axioma.adapters.PersonalNotesAdapter
import ru.terra.axioma.models.PersonalNote
import ru.terra.axioma.views.ItemDecoratorForFab

class PersonalNotesListFragment : Fragment() {
    private val viewModel: PersonalNotesViewModel by sharedViewModel()
    private lateinit var adapter: PersonalNotesAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_personal_notes_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupActionBar()
        setupList()
        setupSubscribers()
        setupFab()
    }

    private fun setupFab() {
        fab.setOnClickListener {
            viewModel.setSelectedItemValue(PersonalNote())
        }
    }

    private fun setupList() {
        adapter = PersonalNotesAdapter()
        adapter.setOnClickListener(object : PersonalNotesAdapter.OnClickListener {
            override fun onItemClick(note: PersonalNote) {
                viewModel.setSelectedItemValue(note)
            }
        })
        recyclerView.addItemDecoration(ItemDecoratorForFab())
        recyclerView.adapter = adapter
    }

    private fun setupSubscribers() {
        viewModel.notes.observe(this, Observer { personalNotes ->
            if (personalNotes != null)
                adapter.setData(personalNotes)
        })
    }

    private fun setupActionBar() {
        (activity as AppCompatActivity).setSupportActionBar(toolbar)
        val actionBar = (activity as AppCompatActivity).supportActionBar ?: return

        val toolbarView = layoutInflater.inflate(R.layout.toolbar_personal_notes, toolbar)

        actionBar.setDisplayShowCustomEnabled(true)
        actionBar.setDisplayShowTitleEnabled(true)

        val openSelectionBtn = toolbarView.findViewById<ImageButton>(R.id.openSelectionBtn)
        openSelectionBtn.setOnClickListener {
            viewModel.toggleSelectionMode()
        }

        val deleteBtn = toolbarView.findViewById<ImageButton>(R.id.deleteBtn)
        deleteBtn.setOnClickListener {
            viewModel.delete(adapter.getSelected())
        }

        viewModel.isSelectionMode.observe(this, Observer { value ->
            adapter.setSelectionMode(value)
            if (value) {
                toolbar.setNavigationIcon(R.drawable.ic_close_white)
                toolbar.contentInsetStartWithNavigation = 0
                toolbar.setNavigationOnClickListener { viewModel.toggleSelectionMode() }

                openSelectionBtn.visibility = View.GONE
                deleteBtn.visibility = View.VISIBLE
                fab.hide()
            } else {
                toolbar.setNavigationIcon(R.drawable.ic_arrow_back_24dp)
                toolbar.contentInsetStartWithNavigation = 0
                toolbar.setNavigationOnClickListener { requireActivity().onBackPressed() }

                openSelectionBtn.visibility = View.VISIBLE
                deleteBtn.visibility = View.GONE
                fab.show()
            }
        })
    }

    companion object {
        fun newInstance(): PersonalNotesListFragment {
            return PersonalNotesListFragment()
        }
    }
}
