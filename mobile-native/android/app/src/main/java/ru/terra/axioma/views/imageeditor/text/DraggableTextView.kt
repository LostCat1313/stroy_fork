package ru.terra.axioma.views.imageeditor.text

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import androidx.annotation.NonNull
import android.text.TextWatcher
import android.view.Gravity
import android.view.MotionEvent
import android.view.MotionEvent.*
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.widget.FrameLayout
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.content.ContextCompat
import androidx.core.view.setPadding
import ru.terra.axioma.DEFAULT_COLOR

import ru.terra.axioma.R
import ru.terra.axioma.Utils

internal class DraggableTextView(context: Context) : AppCompatEditText(context) {

    private val editorCenterX = context.resources.displayMetrics.widthPixels / 2f
    private val editorCenterY = context.resources.displayMetrics.heightPixels / 4f

    private var lastAction: Int = 0
    private var startTime: Long = 0

    private var curX = editorCenterX
    private var curY = editorCenterY

    private var onGlobalLayoutCalled = false

    init {
        layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        setPadding(0)

        viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                onGlobalLayoutCalled = true

                x = editorCenterX - width / 2
                y = editorCenterY - height / 2
                viewTreeObserver.removeOnGlobalLayoutListener(this)
            }
        })
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(ev: MotionEvent): Boolean {
        val action = ev.actionMasked

        if (!isFocused) {
            when (action) {
                ACTION_DOWN -> {
                    lastAction = action
                    startTime = System.currentTimeMillis()

                    curX = x - ev.rawX
                    curY = y - ev.rawY
                }

                ACTION_MOVE -> {
                    val rawX = ev.rawX
                    val rawY = ev.rawY

                    if (lastAction == ACTION_MOVE || lastAction == ACTION_DOWN && distance(curX, curY, x - rawX, y - rawY) > 10) {
                        lastAction = action

                        x = rawX + curX
                        y = rawY + curY
                    }
                }

                ACTION_UP -> if (lastAction == ACTION_DOWN && System.currentTimeMillis() - startTime < 200)
                    requestFocus()

                else -> return false
            }
        }

        return true
    }

    private fun distance(x1: Float, y1: Float, x2: Float, y2: Float): Float {
        val dx = x1 - x2
        val dy = y1 - y2
        val distanceInPx = Math.sqrt((dx * dx + dy * dy).toDouble()).toFloat()
        return pxToDp(distanceInPx)
    }

    private fun pxToDp(px: Float): Float {
        return px / context.resources.displayMetrics.density
    }

    fun center() {
        if (onGlobalLayoutCalled) {
            curX = x
            curY = y
        }

        x = editorCenterX - width / 2
        y = editorCenterY - height / 2
    }

    fun restore() {
        x = curX
        y = curY
    }
}
