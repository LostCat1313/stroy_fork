package ru.terra.axioma;

public interface BaseOnClickListener<T> {
    void onClick(T item);
}
