package ru.terra.axioma.contacts

import android.Manifest
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import org.koin.androidx.viewmodel.ext.android.viewModel
import pub.devrel.easypermissions.AppSettingsDialog
import pub.devrel.easypermissions.EasyPermissions
import pub.devrel.easypermissions.PermissionRequest
import ru.terra.axioma.Constants
import ru.terra.axioma.R
import timber.log.Timber

class ContactsActivity : AppCompatActivity(), EasyPermissions.PermissionCallbacks {

    private val viewModel: ContactsViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contacts)

        supportFragmentManager
                .beginTransaction()
                .replace(R.id.container, ListFragment.newInstance())
                .commit()

        viewModel.selectedItem.observe(this, Observer { contact ->
            if (contact != null) {
                supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.container, ItemFragment.newInstance())
                        .addToBackStack(null)
                        .commit()
            } else {
                supportFragmentManager.popBackStack()
            }
        })

        val storagePerm = arrayOf(Manifest.permission.READ_CONTACTS)
        if (EasyPermissions.hasPermissions(this, *storagePerm)) {
            viewModel.readContacts()
        } else {
            EasyPermissions.requestPermissions(
                    PermissionRequest.Builder(this, Constants.GALLERY_REQUEST, *storagePerm)
                            .setRationale(R.string.card_message)
                            .setPositiveButtonText(R.string.editor_ok)
                            .setNegativeButtonText(R.string.common_cancel)
                            .build()
            )
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    override fun onPermissionsGranted(requestCode: Int, perms: List<String>) {
        viewModel.readContacts()
    }

    override fun onPermissionsDenied(requestCode: Int, perms: List<String>) {
        Timber.d("onPermissionsDenied: %s:", perms.size)
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            AppSettingsDialog.Builder(this).build().show()
        }
    }
}
