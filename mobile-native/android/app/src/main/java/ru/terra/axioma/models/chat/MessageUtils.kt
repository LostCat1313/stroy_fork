package ru.terra.axioma.models.chat

import java.util.ArrayList


object MessageUtils {
    private fun separateMessage(msg: Message, out: MutableList<Message>) {
        if (msg.attachments == null || msg.attachments.isEmpty()) {
            msg.viewType = 0
            out.add(msg)
        } else if (msg.attachments.isNotEmpty()) {
            if (msg.content.isNotEmpty()) {
                out.add(msg.textMessage)
            }

            out.add(msg.pictureMessage)
        }
    }

    fun separate(input: ArrayList<Message>): ArrayList<Message> {
        val messages = ArrayList<Message>()
        for (msg in input) {
            separateMessage(msg, messages)
        }
        return messages
    }
}
