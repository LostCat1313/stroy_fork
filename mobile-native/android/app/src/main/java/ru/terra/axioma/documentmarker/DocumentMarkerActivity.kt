package ru.terra.axioma.documentmarker


import android.content.Intent
import android.os.Bundle
import android.view.MenuItem

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import kotlinx.android.synthetic.main.activity_marker_editor.*
import ru.terra.axioma.R
import ru.terra.axioma.interfaces.OnEditorInteraction
import ru.terra.axioma.models.marker.Marker

class DocumentMarkerActivity : AppCompatActivity(), OnEditorInteraction {
    private var fm: FragmentManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_marker_editor)

        setSupportActionBar(toolbar)

        fm = supportFragmentManager

        val marker = EditableMarker.get()

        when (marker.type) {
            Marker.NOTE -> {
                setTitle(R.string.common_note)
                showFragment(NoteFragment.newInstance())
            }

            Marker.LINK -> {
                setTitle(R.string.common_link)
                showFragment(LinkFragment.newInstance())
            }

            Marker.EDIT -> {
                setTitle(R.string.common_edit)
                showFragment(EditFragment.newInstance())
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val fragment = fm!!.fragments[0]
        fragment.onActivityResult(requestCode, resultCode, data)
    }

    private fun showFragment(f: Fragment) {
        fm!!.beginTransaction().replace(R.id.fragment_container, f).commit()
    }

    override fun onBackPressed() {
        onCancel()
    }

    override fun onOk() {
        finishActivity(ACTION_VIEW)
        super.onBackPressed()
    }

    override fun onOk(m: Marker) {
        EditableMarker.set(m)
        setResult(EDITOR_OK)
        finishActivity(ACTION_CREATE)
        super.onBackPressed()
    }

    override fun onCancel() {
        EditableMarker.destroy()
        setResult(EDITOR_CANCEL)
        finishActivity(ACTION_CREATE)
        super.onBackPressed()
    }

    companion object {
        const val MARKER = "marker"
        const val CREATE_MODE = "mode"

        const val EDITOR_OK = 0
        const val EDITOR_CANCEL = 1
        const val EDITOR_ERROR = 2
        const val ACTION_CREATE = 10
        const val ACTION_VIEW = 11
        const val ACTION_EDIT = 12
    }
}
