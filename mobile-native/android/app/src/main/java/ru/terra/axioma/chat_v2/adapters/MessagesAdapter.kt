package ru.terra.axioma.chat_v2.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ru.terra.axioma.R
import ru.terra.axioma.autoNotify
import ru.terra.axioma.models.ProjectFile
import ru.terra.axioma.models.chat.Message
import ru.terra.axioma.views.MessageView
import kotlin.properties.Delegates


class MessagesAdapter(
        private val userId: Int,
        private val onClick: (msg: Message) -> Unit = {},
        private val onLongClick: (msg: Message) -> Unit = {},
        private val onAttachmentClick: (file: ProjectFile) -> Unit = {}
) : RecyclerView.Adapter<MessagesAdapter.ViewHolder>() {

    var items: ArrayList<Message> by Delegates.observable(arrayListOf()) { _, oldValue, newValue ->
        autoNotify(oldValue, newValue) { o, n -> o.id == n.id }
    }

    inner class ViewHolder internal constructor(view: View) : RecyclerView.ViewHolder(view) {
        val messageView: MessageView = view.findViewById(R.id.message)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.messages_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val obj = items[position]
        holder.messageView.setMessage(obj, userId, onAttachmentClick)
    }

    override fun getItemCount(): Int {
        return items.size
    }
}
