package ru.terra.axioma.adapters;

import android.annotation.SuppressLint;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import ru.terra.axioma.Configuration;
import ru.terra.axioma.R;
import ru.terra.axioma.models.Document;
import ru.terra.axioma.models.ProjectFile;


public class PagesAdapter extends RecyclerView.Adapter<PagesAdapter.ViewHolder> {
    private Document document;
    private List<ProjectFile> mData;
    private OnClickListener clickListener;

    public PagesAdapter() {
    }

    public PagesAdapter(OnClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public PagesAdapter(Document document, List<ProjectFile> data) {
        this.document = document;
        mData = data;
    }

    public PagesAdapter(Document document, List<ProjectFile> data, OnClickListener clickListener) {
        this.clickListener = clickListener;
        this.document = document;
        mData = data;
    }

    public void setClickListener(OnClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public void setData(List<ProjectFile> data) {
        mData = data;
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        SimpleDraweeView image;
        TextView page;

        ViewHolder(View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.image);
            page = itemView.findViewById(R.id.page);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int type) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.page_list_item, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ProjectFile item = mData.get(position);

        String page;
        if (item.versionNumber != 0)
            page = String.format("%1$s-%2$d (%3$d)", document.getShortTitle(), item.pageNumber, item.versionNumber);
        else
            page = String.format("%1$s-%2$d", document.getShortTitle(), item.pageNumber);

        holder.page.setText(page);
        holder.image.setImageURI(Uri.parse(Configuration.baseUrl + item.thumbUrl));

        holder.itemView.setOnClickListener(v -> {
            if (clickListener != null)
                clickListener.onClick(item, position);
        });
    }

    @Override
    public int getItemCount() {
        return mData == null ? 0 : mData.size();
    }

    public interface OnClickListener {
        void onClick(ProjectFile file, int position);
    }
}
