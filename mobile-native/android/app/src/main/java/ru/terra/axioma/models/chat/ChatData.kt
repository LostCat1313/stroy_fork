package ru.terra.axioma.models.chat

import java.util.ArrayList

import ru.terra.axioma.models.User

/**
 * Created by alexei on 22.03.18.
 */

class ChatData {
    var contacts: ArrayList<User> = arrayListOf()
    var rooms: ArrayList<Room> = arrayListOf()

    fun getRoomById(id: Int): Room? {
        for (r in rooms) {
            if (r.id == id)
                return r
        }
        return null
    }
}
