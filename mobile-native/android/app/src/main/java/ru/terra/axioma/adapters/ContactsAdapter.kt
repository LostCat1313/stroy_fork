package ru.terra.axioma.adapters

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import android.widget.TextView

import com.amulyakhare.textdrawable.TextDrawable
import com.amulyakhare.textdrawable.util.ColorGenerator

import ru.terra.axioma.BaseOnClickListener
import ru.terra.axioma.R
import ru.terra.axioma.StringUtils
import ru.terra.axioma.models.PhoneBookContact

class ContactsAdapter(onClickListener: BaseOnClickListener<PhoneBookContact>) : RecyclerView.Adapter<ContactsAdapter.ViewHolder>(),
        Filterable {

    private var objects: List<PhoneBookContact> = ArrayList()
    private var objectsFiltered: List<PhoneBookContact> = ArrayList()
    private var onClickListener: BaseOnClickListener<PhoneBookContact>? = onClickListener

    fun setData(data: List<PhoneBookContact>) {
        objects = data
        objectsFiltered = data
        notifyDataSetChanged()
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var name: TextView = view.findViewById(R.id.name)
        var job: TextView = view.findViewById(R.id.job)
        var avatar: ImageView = view.findViewById(R.id.avatar)
        var serverIcon: ImageView = view.findViewById(R.id.server)
        var localIcon: ImageView = view.findViewById(R.id.local)
    }

    override fun onCreateViewHolder(parent: ViewGroup, type: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.contact_item, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val obj = objectsFiltered[position]

        val full = obj.fullName
        holder.name.text = full
        obj.user?.position?.let {
            holder.job.text = it.name
        }

        val generator = ColorGenerator.MATERIAL
        val drawable = TextDrawable.builder()
                .beginConfig()
                .endConfig()
                .buildRound(StringUtils.getFirstLetters(full), generator.getColor(full))
        holder.avatar.setImageDrawable(drawable)
        holder.serverIcon.visibility = if (obj.user != null) View.VISIBLE else View.GONE
        holder.itemView.setOnClickListener { onClickListener?.onClick(obj) }
    }

    override fun getItemCount(): Int {
        return objectsFiltered.size
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charString = constraint.toString()
                if (charString.isEmpty()) {
                    objectsFiltered = objects
                } else {
                    val filteredList: ArrayList<PhoneBookContact> = ArrayList()
                    for (row in objects) {
                        if (row.fullName.toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row)
                        }
                    }

                    objectsFiltered = filteredList
                }

                val filterResults = FilterResults()
                filterResults.values = objectsFiltered
                return filterResults
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                results?.run {
                    objectsFiltered = values as ArrayList<PhoneBookContact>
                    notifyDataSetChanged()
                }
            }
        }
    }

}
