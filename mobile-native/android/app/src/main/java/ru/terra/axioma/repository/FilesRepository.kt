package ru.terra.axioma.repository

import android.content.Context
import nl.komponents.kovenant.Promise
import nl.komponents.kovenant.task
import nl.komponents.kovenant.then
import okhttp3.ResponseBody
import ru.terra.axioma.Configuration
import ru.terra.axioma.DocumentUtils
import ru.terra.axioma.client.ClientApi
import ru.terra.axioma.models.ProjectFile

class FilesRepository(private val context: Context, private val client: ClientApi) {

    fun downloadFile(file: ProjectFile): Promise<Pair<Boolean, String>?, Exception> {
        return task {
            client.downloadDocument(Configuration.baseUrl + file.url).execute()
        } then {
            it.body()?.run {
                DocumentUtils.writeToCache(context, this, file.fileId)
            }
        }
    }

    fun downloadFile(url: String): Promise<ResponseBody?, Exception> {
        return task {
            client.downloadDocument(url).execute()
        } then {
            it.body()
        }
    }
}