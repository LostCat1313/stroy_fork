package ru.terra.axioma.contact

import android.app.Application
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.AndroidViewModel
import androidx.databinding.ObservableField
import com.google.android.material.appbar.CollapsingToolbarLayout

import ru.terra.axioma.models.PhoneBookContact

/**
 * Created by alexei on 14.03.18.
 */

class ContactViewModel(application: Application) : AndroidViewModel(application) {
    internal val contact = ObservableField<PhoneBookContact>()
    internal val collapseToolbar: CollapsingToolbarLayout? = null
    internal val toolbar: Toolbar? = null
    internal var call: AppCompatButton? = null
}
