package ru.terra.axioma.models;

import android.graphics.Bitmap;
import android.net.Uri;

/**
 * Created by alexei on 10/1/17.
 */

public class Contact {
    public String id;
    public String name;
    public String mobileNumber;
    public Bitmap photo;
    public Uri photoURI;
}
