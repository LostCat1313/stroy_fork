package ru.terra.axioma.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import ru.terra.axioma.R;

/**
 * Created by alexei on 9/27/17.
 */

public class SimpleTextAdapter extends RecyclerView.Adapter<SimpleTextAdapter.ViewHolder> {
    private List<String> objects;
    private int layoutRes;
    private OnClickListener onClickListener;

    public SimpleTextAdapter() {
//        this.objects = objects;
        this.layoutRes = R.layout.document_list_item;
    }

    public SimpleTextAdapter(List<String> objects, int layoutRes, OnClickListener onClickListener) {
        this.objects = objects;
        this.layoutRes = layoutRes;
        this.onClickListener = onClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textView;

        public ViewHolder(View view) {
            super(view);
            textView = view.findViewById(R.id.title);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int type) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(layoutRes, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
//        com.stroyproject.models.Notification data = objects.get(position);
//        Notification notification_card = holder.notification_card;
//        notification_card.setTitle(data.title);
        if (objects != null)
            holder.textView.setText(objects.get(position));

        if (onClickListener != null)
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onClickListener.onClick(holder.getAdapterPosition());
                }
            });
    }

    @Override
    public int getItemCount() {
        return objects == null ? 10 : objects.size();
    }
//
//    @Override
//    public int getItemViewType(int position) {
//        return objects.get(position).type;
//    }

    //    private void showPopupMenu(View view) {
//        // inflate menu
//        PopupMenu popup = new PopupMenu(mContext, view);
//        MenuInflater inflater = popup.getMenuInflater();
//        inflater.inflate(R.menu.menu_album, popup.getMenu());
//        popup.setOnMenuItemClickListener(new MyMenuItemClickListener());
//        popup.show();
//    }
//
//    /**
//     * Click listener for popup menu items
//     */
//    class MyMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {
//
//        public MyMenuItemClickListener() {
//        }
//
//        @Override
//        public boolean onMenuItemClick(MenuItem menuItem) {
//            switch (menuItem.getItemId()) {
//                case R.id.action_add_favourite:
//                    Toast.makeText(mContext, "Add to favourite", Toast.LENGTH_SHORT).show();
//                    return true;
//                case R.id.action_play_next:
//                    Toast.makeText(mContext, "Play next", Toast.LENGTH_SHORT).show();
//                    return true;
//                default:
//            }
//            return false;
//        }
//    }

    public interface OnClickListener {
        void onClick(int position);
    }
}
