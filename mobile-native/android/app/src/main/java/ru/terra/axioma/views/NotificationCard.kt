package ru.terra.axioma.views

import android.content.Context
import androidx.databinding.DataBindingUtil
import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.notification_card.view.*
import org.koin.core.KoinComponent

import ru.terra.axioma.R
import ru.terra.axioma.models.Notification
import ru.terra.axioma.repository.ProjectsRepository

abstract class NotificationCard(context: Context) : LinearLayout(context), KoinComponent {

    internal abstract val iconRes: Int
    internal abstract val colorRes: Int
    internal abstract val titleRes: Int

    init {
        init()
    }

    private fun init() {
        LayoutInflater.from(context).inflate(R.layout.notification_card, this, true)

        orientation = VERTICAL

        val color = ContextCompat.getColor(context, colorRes)
        val drawable = ContextCompat.getDrawable(context, iconRes)!!
        drawable.mutate().setTint(color)

        icon.setImageDrawable(drawable)
        colorIndicator.setBackgroundColor(color)

        val menuDrawable = ContextCompat.getDrawable(context, R.drawable.zzz_dots_vertical)!!
        menuDrawable.mutate().setTint(ContextCompat.getColor(context, R.color.grey_600))
    }

    fun setNotification(notification: Notification) {
        val pr: ProjectsRepository = getKoin().get()
        title.text = context.getString(titleRes, notification.title)
        date.text = notification.createdDate
        project.text = pr.getProjectTitle(notification.projectId)
        organization.text = notification.organization
        content.text = notification.content
    }

    fun setMargin() {
        (cardView.layoutParams as MarginLayoutParams).topMargin = context.resources.getDimensionPixelSize(R.dimen.card_out_margin)
    }
}
