package ru.terra.axioma.repository

import androidx.lifecycle.MutableLiveData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import ru.terra.axioma.client.ClientApi
import ru.terra.axioma.models.marker.Marker
import ru.terra.axioma.models.marker.MarkerEntry
import timber.log.Timber

class ProjectNotesRepository(private val client: ClientApi) {

    val data: MutableLiveData<List<Marker>>
    val isLoading: MutableLiveData<Boolean>

    init {
        this.data = MutableLiveData()
        this.isLoading = MutableLiveData()
    }

    fun fetchByDoc(docId: Int, fileId: Int) {
        isLoading.value = true
        client.getMarkers(docId, fileId).enqueue(object : Callback<MarkerEntry> {
            override fun onResponse(call: Call<MarkerEntry>, response: Response<MarkerEntry>) {
                if (response.body() != null) {
                    data.value = response.body()!!.list
                }
                isLoading.value = false
            }

            override fun onFailure(call: Call<MarkerEntry>, t: Throwable) {
                Timber.e(t)
                isLoading.value = false
            }
        })
    }

    fun fetchNotesByProject(projectId: Int) {
        isLoading.value = true
        client.getMarkersByProjectAndType(projectId, Marker.NOTE).enqueue(object : Callback<MarkerEntry> {
            override fun onResponse(call: Call<MarkerEntry>, response: Response<MarkerEntry>) {
                if (response.body() != null) {
                    data.value = response.body()!!.list
                }
                isLoading.value = false
            }

            override fun onFailure(call: Call<MarkerEntry>, t: Throwable) {
                Timber.e(t)
                isLoading.value = false
            }
        })
    }
}
