package ru.terra.axioma.models.chat;

import android.os.Parcel;
import android.os.Parcelable;

import ru.terra.axioma.models.ProjectFile;

import java.util.Date;

/**
 * Created by alexei on 31.01.18.
 */

public class Attachment implements Parcelable {
    public int id;
    public int messageId;
    public int type;
    public int targetId;
    public Date createdAt;
    public Date updatedAt;
    public ProjectFile file;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeInt(this.messageId);
        dest.writeInt(this.type);
        dest.writeInt(this.targetId);
        dest.writeLong(this.createdAt != null ? this.createdAt.getTime() : -1);
        dest.writeLong(this.updatedAt != null ? this.updatedAt.getTime() : -1);
        dest.writeParcelable(this.file, flags);
    }

    public Attachment() {
    }

    protected Attachment(Parcel in) {
        this.id = in.readInt();
        this.messageId = in.readInt();
        this.type = in.readInt();
        this.targetId = in.readInt();
        long tmpCreatedAt = in.readLong();
        this.createdAt = tmpCreatedAt == -1 ? null : new Date(tmpCreatedAt);
        long tmpUpdatedAt = in.readLong();
        this.updatedAt = tmpUpdatedAt == -1 ? null : new Date(tmpUpdatedAt);
        this.file = in.readParcelable(ProjectFile.class.getClassLoader());
    }

    public static final Parcelable.Creator<Attachment> CREATOR = new Parcelable.Creator<Attachment>() {
        @Override
        public Attachment createFromParcel(Parcel source) {
            return new Attachment(source);
        }

        @Override
        public Attachment[] newArray(int size) {
            return new Attachment[size];
        }
    };
}
