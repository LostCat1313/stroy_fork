package ru.terra.axioma.views.imageeditor

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.PointF
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.ViewGroup
import android.widget.FrameLayout
import com.davemorrissey.labs.subscaleview.ImageSource
import com.davemorrissey.labs.subscaleview.ImageViewState
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView
import nl.komponents.kovenant.Promise
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import ru.terra.axioma.client.ClientApi
import ru.terra.axioma.models.marker.*
import ru.terra.axioma.models.marker.Marker.*
import ru.terra.axioma.views.imageeditor.draw.DrawOverlay
import ru.terra.axioma.views.imageeditor.text.TextOverlay
import timber.log.Timber
import java.lang.Exception

class ImageEditorView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    var saveFn: (ArrayList<Marker>) -> Unit = { Timber.d("ImageEditorView -> onSave not initialized") }

    private var imageView: ScaleImageView = ScaleImageView(context)
    private var textOverlay: TextOverlay? = null
    private var drawOverlay: DrawOverlay? = null
    private var editable: Boolean = false

    val sharePicture: Bitmap
        get() = imageView.sharePicture

    val state: ImageViewState?
        get() = imageView.state!!

    var fileId: Int = 0
    var docId: Int = 0

    init {
        init()
    }

    private fun init() {
        isFocusableInTouchMode = true
        isClickable = true

        val layoutParams = LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)

        imageView.layoutParams = layoutParams

        addView(imageView)
    }



    private fun addTextOverlay() {
        val lp = LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)

        textOverlay = TextOverlay(context, imageView.scale, imageView.initScale).apply {
            layoutParams = lp

            setOnDone { marker ->
                marker.docId = docId
                marker.fileId = fileId

                (marker.params as? TextParams)?.run {
                    point.set(imageView.viewToSourceCoord(point))
                }

                if (marker.text.isNotEmpty())
                    saveFn(arrayListOf(marker))
                stopEdit()
            }

            setOnCancel {
                stopEdit()
            }

        }

        addView(textOverlay)
    }

    private fun addDrawOverlay(type: Int) {
        val lp = LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)

        drawOverlay = DrawOverlay(context, type, imageView.scale, imageView.initScale).apply {
            layoutParams = lp

            onDone = { v ->
                v.map { marker ->
                    marker.docId = docId
                    marker.fileId = fileId

                    when (marker.type) {
                        LINE -> {
                            (marker.params as? DrawParams)?.let {
                                it.points = it.points.map { p ->
                                    imageView.viewToSourceCoord(p.x, p.y) ?: PointF()
                                } as ArrayList<PointF>
                            }
                        }

                        RECT -> {
                            (marker.params as? DrawParams)?.let {
                                it.points[0] = imageView.viewToSourceCoord(it.points[0]) ?: PointF()
                                it.points[1] = imageView.viewToSourceCoord(it.points[1]) ?: PointF()
                            }
                        }

                        else -> {
                        }
                    }
                }

                if (v.isNotEmpty())
                    saveFn(v)
                stopEdit()
            }

            onCancel = {
                stopEdit()
            }
        }

        addView(drawOverlay)
    }

    // Делегаты
    fun setMarkers(markers: ArrayList<Marker>, highlight: Int = 0) {
        imageView.addMarkers(markers, highlight)
    }

    fun setImage(uri: ImageSource, onReady: () -> Unit) {
        imageView.apply {
            setOnImageEventListener(object: SubsamplingScaleImageView.OnImageEventListener {
                override fun onImageLoaded() {

                }

                override fun onTileLoadError(e: Exception?) {

                }

                override fun onPreviewReleased() {

                }

                override fun onImageLoadError(e: Exception?) {

                }

                override fun onPreviewLoadError(e: Exception?) {

                }

                override fun onReady() {
                    onReady()
                }
            })
            setImage(uri)
        }
    }

    fun setImage(uri: ImageSource) {
        imageView.setImage(uri)
    }

    fun setImage(drawable: Drawable) {
        if (drawable is BitmapDrawable) {
            imageView.setImage(ImageSource.bitmap(drawable.bitmap))
        } else {
            val bitmap = Bitmap.createBitmap(drawable.intrinsicWidth, drawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
            val canvas = Canvas(bitmap)
            drawable.setBounds(0, 0, canvas.width, canvas.height)
            drawable.draw(canvas)

            imageView.setImage(ImageSource.bitmap(bitmap))
        }
    }

    fun setImage(uri: ImageSource, imageViewState: ImageViewState?) {
        imageView.setImage(uri, imageViewState)
    }

    fun setOnMarkerInteraction(listener: OnMarkerListener) {
        imageView.setOnMarkerInteraction(listener)
    }

    fun startEdit(marker: Marker) {
        if (drawOverlay != null || textOverlay != null) {
            stopEdit()
//            confirmDialog()
        } else {
            if (marker.type == TEXT)
                addTextOverlay()
            else if (marker.type == RECT || marker.type == LINE)
                addDrawOverlay(marker.type)
            else
                imageView.addLabel(marker.type)
        }
    }

    fun stopEdit() {
        if (drawOverlay != null) {
            removeView(drawOverlay)
            drawOverlay = null
        }

        if (textOverlay != null) {
            removeView(textOverlay)
            textOverlay = null
        }

        editable = false
        imageView.stopEdit()
    }

    fun notifyMarkerListChanged() {
        imageView.notifyMarkerListChanged()
    }

    fun updateMarker(m: Marker) {
        imageView.updateMarker(m)
    }



    /*private fun confirmDialog() {
        MaterialDialog(context)
                .message(R.string.common_save_confirm)
                .negativeButton(R.string.no) { stopEdit() }
                .positiveButton(R.string.yes) {
                    if (drawOverlay != null)
                        drawOverlay!!.onDone()
                    if (textOverlay != null)
                        textOverlay!!.destroy()
                }
                .show()
    }*/
}
