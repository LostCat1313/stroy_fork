package ru.terra.axioma.documentstructure

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.WhichButton
import com.afollestad.materialdialogs.actions.setActionButtonEnabled
import com.afollestad.materialdialogs.callbacks.onShow
import com.afollestad.materialdialogs.input.getInputField
import com.afollestad.materialdialogs.input.input
import com.afollestad.materialdialogs.list.listItems
import kotlinx.android.synthetic.main.activity_document_list.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import ru.terra.axioma.Constants
import ru.terra.axioma.R
import ru.terra.axioma.adapters.AllDocumentsAdapter
import ru.terra.axioma.documentcreator.DocumentCreatorActivity
import ru.terra.axioma.documentupdater.DocumentUpdaterActivity
import ru.terra.axioma.models.Document
import ru.terra.axioma.pages.PagesActivity

class DocumentListActivity : AppCompatActivity(), AllDocumentsAdapter.OnClickListener {

    private val viewModel: DocumentListViewModel by viewModel()

    private var adapter: AllDocumentsAdapter = AllDocumentsAdapter()

    private var toolbarProgress: ProgressBar? = null
    private var toolbarTitle: TextView? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_document_list)

        initToolbar()
        setupList()
        setupRefreshLayout()
        setupMenu()

        viewModel.selectionMode.observe(this, Observer {
            if (it == DEFAULT_MODE && adapter.itemCount > 0) {
                adapter.removeRoot()
            }

            if (it == CREATE_FOLDER_MODE) {
                adapter.addRoot(viewModel.rootDoc)
            }

            adapter.setSelectable(it != DEFAULT_MODE, it)
            adapter.notifyDataSetChanged()

            if (it == DEFAULT_MODE) menu.showMenu(false)
            else menu.hideMenu(false)

            setToolbarValues(it)
        })


        viewModel.documents.observe(this, Observer {
            show(it)

            /*if (viewModel.selectionMode.value != DEFAULT_MODE) {
                viewModel.selectionMode.value = DEFAULT_MODE
            }*/
        })
        /*repository.isLoading.observe(this, Observer { loading ->
            toolbarProgress?.let {
                it.visibility = if (loading != null && loading) View.VISIBLE else View.GONE
            }
        })*/

        //        Notification notification = getIntent().getParcelableExtra(Constants.NOTIFICATION);
        //        if (notification != null) {
        //            Project newProject = Projects.getProject(notification.projectId);
        //            if (newProject == null)
        //                return;
        //
        //            Document document = notification.document;
        //            if (App.session.project == null || App.session.project.id != document.projectId) {
        //                CustomDialog.get(this)
        //                        .content(getString(R.string.project_choose_another_project, newProject.title))
        //                        .negativeText(R.string.common_cancel)
        //                        .onNegative((dialog, which) -> super.onBackPressed())
        //                        .positiveText(R.string.editor_ok)
        //                        .onPositive((dialog, which) -> {
        //                            App.session.project = newProject;
        //                            fm.beginTransaction().replace(R.id.fragment_container, listFragment).commit();
        ////                            openPages(document);
        //                        }).show();
        //            } else {
        //                openPages(document);
        //            }
        //        } else {
        //        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Constants.CREATE_DOCUMENT_REQUEST) {
            if (resultCode == Activity.RESULT_OK)
                viewModel.update()
            viewModel.setMode(DEFAULT_MODE)
        } else if (requestCode == Constants.UPDATE_DOCUMENT_REQUEST) {
            if (resultCode == Activity.RESULT_OK)
                viewModel.update()
            viewModel.setMode(DEFAULT_MODE)
        }
    }

    override fun onBackPressed() {
        val mode: Int? = viewModel.selectionMode.value
        if (mode == DEFAULT_MODE)
            super.onBackPressed()
        else
            viewModel.setMode(DEFAULT_MODE)
    }

    private fun initToolbar() {
        val toolbarView = layoutInflater.inflate(R.layout.toolbar_title_and_progress, toolbar)

        toolbarProgress = toolbarView.findViewById(R.id.progress)
        toolbarTitle = toolbarView.findViewById(R.id.title)

        toolbar.contentInsetStartWithNavigation = 0
    }

    private fun setToolbarValues(mode: Int) {
        if (mode == DEFAULT_MODE) {
            toolbarTitle?.setText(R.string.common_documents)
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back_24dp)
            toolbar.setNavigationOnClickListener { onBackPressed() }
        } else {
            val res = if (mode == UPDATE_DOCUMENT_MODE) R.string.common_select_document else R.string.common_select_folder
            toolbarTitle?.setText(res)
            toolbar.setNavigationIcon(R.drawable.ic_clear)
            toolbar.setNavigationOnClickListener { viewModel.setMode(DEFAULT_MODE) }
        }
    }

    private fun setupList() {
        adapter = AllDocumentsAdapter()
        adapter.setOnClickListener(this)

        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter
    }


    private fun setupRefreshLayout() {
        refreshLayout.setOnRefreshListener { viewModel.update() }
    }


    private fun setupMenu() {
        menu.setClosedOnTouchOutside(true)
        createFolder.setOnClickListener {
            viewModel.setMode(CREATE_FOLDER_MODE)
            menu.close(true)
        }
        createDocument.setOnClickListener {
            viewModel.setMode(CREATE_DOCUMENT_MODE)
            menu.close(true)
        }
        updateDocument.setOnClickListener {
            viewModel.setMode(UPDATE_DOCUMENT_MODE)
            menu.close(true)
        }
    }

    private fun show(docs: List<Document>?) {
        if (refreshLayout != null && refreshLayout.isRefreshing)
            refreshLayout.isRefreshing = false

        adapter.setData(docs!!)
        adapter.notifyDataSetChanged()
    }

    private fun showNameInput(document: Document) {
        MaterialDialog(this).show {
            input(hintRes = R.string.common_name, waitForPositiveButton = false) { dialog, text ->
                dialog.setActionButtonEnabled(WhichButton.POSITIVE, text.isNotEmpty())
            }
            negativeButton(R.string.editor_cancel) {
                viewModel.setMode(DEFAULT_MODE)
            }
            positiveButton(R.string.editor_ok) { dialog ->
                val inputField = dialog.getInputField()
                viewModel.createFolder(document, inputField.text.toString())
                dialog.dismiss()
            }
            onShow {
                it.setActionButtonEnabled(WhichButton.POSITIVE, false)
            }
        }
    }

    override fun onDocumentClick(document: Document) {
        val intent = Intent(this, PagesActivity::class.java)
        intent.putExtra(Constants.DOCUMENT, document)
        startActivity(intent)
    }

    override fun onSelect(document: Document) {
        var mode: Int? = viewModel.selectionMode.value
        if (mode == null)
            mode = DEFAULT_MODE

        if (mode == CREATE_FOLDER_MODE)
            showNameInput(document)
        else if (mode == CREATE_DOCUMENT_MODE && document.isFolder) {
            val intent = Intent(this, DocumentCreatorActivity::class.java)
            intent.putExtra(Constants.DOCUMENT, document)
            startActivityForResult(intent, Constants.CREATE_DOCUMENT_REQUEST)
        } else if (mode == UPDATE_DOCUMENT_MODE && !document.isFolder) {
            val intent = Intent(this, DocumentUpdaterActivity::class.java)
            intent.putExtra(Constants.DOCUMENT, document)
            startActivityForResult(intent, Constants.UPDATE_DOCUMENT_REQUEST)
        }
    }

    override fun onLongClick(document: Document) {
        val rename = {
            MaterialDialog(this)
                    .input(prefill = document.title, waitForPositiveButton = false) { dialog, text ->
                        dialog.setActionButtonEnabled(WhichButton.POSITIVE, text.isNotEmpty())
                    }
                    .negativeButton(R.string.common_cancel)
                    .positiveButton(R.string.editor_ok) { dialog ->
                        dialog.getInputField().text?.toString()?.takeIf { document.title != it }?.let {
                            viewModel.rename(document, it)
                        }
                    }
                    .onShow { it.setActionButtonEnabled(WhichButton.POSITIVE, false) }
                    .show()
        }

        val items = listOf(
                getString(R.string.common_rename),
                getString(R.string.common_remove)
        )

        MaterialDialog(this)
                .listItems(items = items, waitForPositiveButton = false) { dialog, index, _ ->
                    when(index) {
                        0 -> rename()
                        1 -> viewModel.delete(document)
                    }
                    dialog.dismiss()
                }
                .show()
    }
}
