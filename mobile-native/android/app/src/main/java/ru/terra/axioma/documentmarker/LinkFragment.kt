package ru.terra.axioma.documentmarker

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import androidx.annotation.NonNull
import androidx.annotation.Nullable
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.afollestad.materialdialogs.list.listItems
import kotlinx.android.synthetic.main.editor_button_block.view.*
import kotlinx.android.synthetic.main.fragment_link.*
import org.koin.android.ext.android.getKoin
import ru.terra.axioma.Configuration
import ru.terra.axioma.R
import ru.terra.axioma.adapters.AllDocumentsAdapter
import ru.terra.axioma.interfaces.OnEditorInteraction
import ru.terra.axioma.models.Document
import ru.terra.axioma.models.ProjectFile
import ru.terra.axioma.models.marker.Marker
import ru.terra.axioma.repository.DocumentsRepository
import ru.terra.axioma.views.CustomDialog

/**
 * Created by alexei on 04.01.18.
 */

class LinkFragment : EditorBaseFragment() {
    private var mListener: OnEditorInteraction? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view = inflater.inflate(R.layout.fragment_link, container, false)
        return view
    }

    override fun onViewCreated(view: View, @Nullable savedInstanceState: Bundle?) {
        makeIcon(markerImage, R.color.green, R.drawable.ic_link)
        buttons.ok.visibility = View.GONE
        buttons.cancel.setOnClickListener { mListener!!.onCancel() }

        val adapter = AllDocumentsAdapter(true)
        adapter.setOnClickListener(object : AllDocumentsAdapter.OnClickListener {
            override fun onDocumentClick(document: Document) {
                loadPageList(document)
            }

            override fun onSelect(document: Document) {

            }

            override fun onLongClick(document: Document) {

            }
        })

        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = adapter

        // TODO проверить
        val repository: DocumentsRepository = getKoin().get()
//        repository.documents.observe(this, Observer { documents -> adapter.setData(documents) })
        adapter.setData(repository.documents)
    }

    override fun onSaveSuccess(marker: Marker) {
        mListener!!.onOk(marker)
    }

    override fun onSaveError(marker: Marker) {
        mListener!!.onOk(marker)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnEditorInteraction) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement OnEditorInteraction")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    private fun loadPageList(document: Document) {
        val pages = document.allPages
        if (pages != null) {
            val count = pages.size
            val items = mutableListOf<String>()
            for (i in 0 until count)
                items.add(getString(R.string.editor_page, pages[i].pageNumber))

            CustomDialog[context!!]
                    .title(text = document.title)
                    .listItems(items = items, waitForPositiveButton = false) { _, index, _ -> linkConfirmDialog(document, pages[index]) }
                    .negativeButton(R.string.editor_cancel)
                    .show()
        }
    }

    private fun linkConfirmDialog(document: Document, file: ProjectFile?) {
        val title = document.title + if (file != null) " " + getString(R.string.editor_page, file.pageNumber) else ""

        val url = Configuration.baseUrl + if (file == null) document.file.thumbUrl else file.thumbUrl
        CustomDialog.getImageDialog(context!!, url)
                .title(null, title)
                .negativeButton(R.string.editor_cancel)
                .positiveButton(R.string.editor_ok) {
                    marker.setLink(document, file)
                    super.save()
                }
                .show()
    }

    companion object {

        fun newInstance(): LinkFragment {
            return LinkFragment()
        }
    }
}
