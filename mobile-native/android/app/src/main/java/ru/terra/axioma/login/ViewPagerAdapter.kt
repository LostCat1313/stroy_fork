package ru.terra.axioma.login

import android.view.ViewGroup
import android.view.View
import androidx.viewpager.widget.PagerAdapter
import ru.terra.axioma.R

class ViewPagerAdapter : PagerAdapter() {

    override fun instantiateItem(collection: ViewGroup, position: Int): Any {
        var resId = 0
        when (position) {
            0 -> resId = R.id.panel1
            1 -> resId = R.id.panel2
        }

        return collection.findViewById(resId)
    }

    override fun destroyItem(collection: ViewGroup, position: Int, view: Any) {
        collection.removeView(view as View)
    }

    override fun getCount(): Int {
        return 2
    }

    override fun isViewFromObject(view: View, obj: Any): Boolean {
        return view === obj
    }
}