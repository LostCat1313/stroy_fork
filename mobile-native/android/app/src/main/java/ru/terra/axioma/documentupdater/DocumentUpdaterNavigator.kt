package ru.terra.axioma.documentupdater

import android.net.Uri

import ru.terra.axioma.models.ProjectFile

interface DocumentUpdaterNavigator {

    interface View {
        fun close()

        fun showRenderPdfProgress(max: Int, showMinMax: Boolean)

        fun updateProgress(value: Int)

        fun showError(error: Int)

        fun showError(error: String)

        fun showLoadingToServerProgress()

        fun dismissProgress()

        fun addFile()
    }

    interface Action {
        fun cancelPdfRenderTask()

        //        void renderPdfPages(String path);
        //
        //        void addPage(Uri newPageUri);

        fun renderPage(uri: Uri)

        fun addLinkToPage(newPage: Int, oldPage: ProjectFile)
    }
}
