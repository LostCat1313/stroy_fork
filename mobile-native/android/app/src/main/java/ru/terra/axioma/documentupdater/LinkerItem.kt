package ru.terra.axioma.documentupdater

import android.net.Uri
import ru.terra.axioma.models.ProjectFile
import java.util.*

class LinkerItem(var newPageUri: Uri) {
    var id: UUID = UUID.randomUUID()
    var oldPage: ProjectFile? = null
    var uploadedPage: ProjectFile? = null
}
