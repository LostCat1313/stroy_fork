package ru.terra.axioma.adapters

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup

import com.google.common.collect.Collections2
import com.google.common.collect.Lists

import ru.terra.axioma.Constants
import ru.terra.axioma.autoNotify
import ru.terra.axioma.main.FilterBy
import ru.terra.axioma.models.Notification
import ru.terra.axioma.store.CurrentSessionStore
import ru.terra.axioma.views.NotificationCard
import ru.terra.axioma.views.NotificationDocument
import ru.terra.axioma.views.NotificationEdit
import ru.terra.axioma.views.NotificationLink
import ru.terra.axioma.views.NotificationMessage
import ru.terra.axioma.views.NotificationNote
import timber.log.Timber
import kotlin.properties.Delegates


class NotificationsAdapter(
        private val currentSessionStore: CurrentSessionStore
) : RecyclerView.Adapter<NotificationsAdapter.ViewHolder>() {

    private var objects: MutableList<Notification> = mutableListOf()

    var items: ArrayList<Notification> by Delegates.observable(arrayListOf()) { _, _, _ ->
        filter()
    }

    private var filtered: ArrayList<Notification> by Delegates.observable(arrayListOf()) { _, oldValue, newValue ->
        autoNotify(oldValue, newValue) { o, n -> o.id == n.id }
    }

    private var onClick: (Notification) -> Unit = { Timber.d("Not init") }
    private var filterBy: List<FilterBy> = arrayListOf(FilterBy.Docs, FilterBy.Messages, FilterBy.Notes)

    fun setClickListener(clickListener: (Notification) -> Unit) {
        this.onClick = clickListener
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var notification: NotificationCard = view as NotificationCard
    }

    override fun onCreateViewHolder(parent: ViewGroup, type: Int): ViewHolder {
        return when (type) {
            NEW_EDIT -> ViewHolder(NotificationEdit(parent.context))
            NEW_LINK -> ViewHolder(NotificationLink(parent.context))
            NEW_NOTE -> ViewHolder(NotificationNote(parent.context))
            Constants.NotificationType.CHAT_MESSAGE -> ViewHolder(NotificationMessage(parent.context))
            Constants.NotificationType.CHAT_ROOM_ADDED -> ViewHolder(NotificationMessage(parent.context))
            else -> ViewHolder(NotificationDocument(parent.context))
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = filtered[position]
        val notification = holder.notification
        notification.setNotification(data)
        if (position == 0)
            notification.setMargin()

        holder.itemView.setOnClickListener { v ->
            onClick(data)
        }
    }

    override fun getItemCount(): Int {
        return filtered.size
    }

    override fun getItemViewType(position: Int): Int {
        val n = filtered[position]
        return if (n.type == Constants.NotificationType.DOCUMENT_DESCRIPTOR_ADDED && n.marker != null) {
            1000 + n.marker.type
        } else filtered[position].type

    }

    fun filter(filterBy: List<FilterBy>) {
        this.filterBy = filterBy
        filter()
    }

    private fun filter() {
        items.let {
            val showDocs = filterBy.contains(FilterBy.Docs)
            val showMessages = filterBy.contains(FilterBy.Messages)
            val showNotes = filterBy.contains(FilterBy.Notes)
            val filterByProject = filterBy.contains(FilterBy.Project)

            val predicate = { input: Notification? ->
                input?.run {
                    ((showMessages && isChat) || (showNotes && isMarker) || (showDocs && isDocument)) &&
                            (!filterByProject || currentSessionStore.project?.id == projectId)
                } ?: false
            }

            filtered = Lists.newArrayList(Collections2.filter(it, predicate))
        }
    }

    companion object {
        private val NEW_EDIT = 1000
        private val NEW_LINK = 1001
        private val NEW_NOTE = 1002
    }
}
