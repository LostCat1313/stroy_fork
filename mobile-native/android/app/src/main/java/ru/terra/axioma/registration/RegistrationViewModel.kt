package ru.terra.axioma.registration

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.google.gson.JsonObject
import nl.komponents.kovenant.task
import nl.komponents.kovenant.then
import ru.terra.axioma.client.ClientApi
import ru.terra.axioma.models.Organization
import ru.terra.axioma.models.Position
import ru.terra.axioma.repository.OrganizationsRepository
import ru.terra.axioma.repository.PositionsRepository
import ru.terra.axioma.store.CurrentSessionStore
import ru.terra.axioma.store.UserStore
import timber.log.Timber

class RegistrationViewModel constructor(
        application: Application,
        private val client: ClientApi,
        private val positionsRepository: PositionsRepository,
        private val organizationsRepository: OrganizationsRepository,
        private val currentSessionStore: CurrentSessionStore
) : AndroidViewModel(application) {

    val showError = MutableLiveData<Boolean>()
    val showNextScreen = MutableLiveData<Boolean>()

    var organizations = MutableLiveData<List<Organization>>()

    var positions = MutableLiveData<List<Position>>()

    init {
        organizationsRepository.run {
            this@RegistrationViewModel.organizations = organizations
            fetch()
        }

        positionsRepository.run {
            this@RegistrationViewModel.positions = positions
            fetch()
        }
    }

    fun register(name: String, password: String, firstName: String, secondName: String, thirdName: String = "",
                 email: String = "", organizationName: String, positionName: String) {
        val entryContent = JsonObject().apply {
            addProperty("name", name)
            addProperty("password", password)
            addProperty("firstName", firstName)
            addProperty("lastName", secondName)
            addProperty("secondName", thirdName)
            addProperty("email", email)

            if (organizationName.isNotEmpty()) {
                organizationsRepository.getIdByName(organizationName)?.let {
                    addProperty("organizationId", it)
                }
                addProperty("organizationName", organizationName)
            }

            if (positionName.isNotEmpty()) {
                organizationsRepository.getIdByName(organizationName)?.let {
                    addProperty("positionId", it)
                }
                addProperty("positionName", positionName)
            }
        }

        val entry = JsonObject().apply {
            add("entry", entryContent)
        }

        task {
            client.updateUser(entry).execute()
        }.then {
            Timber.d("%s", it)

            it.body()?.let { v ->
                UserStore.user = v
                currentSessionStore.user = v
                showNextScreen.postValue(true)
            }
        }.fail {
            Timber.e(it)
        }
    }
}
