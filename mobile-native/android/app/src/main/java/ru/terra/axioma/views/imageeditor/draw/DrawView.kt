package ru.terra.axioma.views.imageeditor.draw

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Path
import android.graphics.PointF
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import ru.terra.axioma.DEFAULT_COLOR
import ru.terra.axioma.models.marker.DrawParams
import ru.terra.axioma.models.marker.Marker
import timber.log.Timber

class DrawView(context: Context, private val type: Int, private val scale: Float, private val initScale: Float) : View(context) {
    private val defaultColor = DEFAULT_COLOR
    private val defaultStrokeWidth = 8f

    private val drawingPaint: Paint = Paint()

    private var lineParams: DrawParams? = null
    private var rectParams: DrawParams? = null

    var markers = arrayListOf<Marker>()

    private var selectedColor = defaultColor
    private var selectedStrokeWidth = defaultStrokeWidth

    init {
        layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)

        drawingPaint.apply {
            color = defaultColor
            style = Paint.Style.STROKE
            strokeJoin = Paint.Join.ROUND
            strokeCap = Paint.Cap.ROUND
            strokeWidth = 8f
            isAntiAlias = true
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent): Boolean {
        val touchCount = event.pointerCount

        when (event.action) {
            MotionEvent.ACTION_DOWN -> onActionDown(event)

            MotionEvent.ACTION_MOVE -> onActionMove(event, touchCount)

            MotionEvent.ACTION_UP -> onActionUp()
        }

        // Use parent to handle pinch and two-finger pan.
        return true
    }

    private fun onActionDown(event: MotionEvent) {
        when (type) {
            Marker.LINE -> {
                lineParams = DrawParams().apply {
                    color = selectedColor
                    width = selectedStrokeWidth.toInt()

                    val p = PointF(event.x, event.y)
                    points.add(p)
                }
            }

            Marker.RECT -> {
                rectParams = DrawParams().apply {
                    color = selectedColor
                    width = selectedStrokeWidth.toInt()
                    points.add(PointF(event.x, event.y))
                    points.add(PointF(event.x, event.y))
                }
            }
        }
    }

    private fun onActionMove(event: MotionEvent, touchCount: Int) {
        if (touchCount == 1) {
            val sCurrentF = PointF(event.x, event.y)
            val sCurrent = PointF(sCurrentF.x, sCurrentF.y)

            lineParams?.let {
                val prev = it.points.last()

                val vDX = Math.abs(event.x - prev.x)
                val vDY = Math.abs(event.y - prev.y)

                if (vDX >= it.width || vDY >= it.width) {
                    it.points.add(sCurrent)
                }

                invalidate()
            }

            rectParams?.let {
                it.points.last().set(sCurrent)
                invalidate()
            }
        }
    }

    private fun onActionUp() {
        if (lineParams != null && rectParams != null) {
            Timber.e("ScaleImageView have more then 1 initialized labelParams")
            return
        }

        val m = Marker()
        m.type = type

        if (lineParams != null) {
            m.params = lineParams

            lineParams = null
        }

        if (rectParams != null) {
            m.params = rectParams

            rectParams = null
        }

        markers.add(m)
    }


    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        lineParams?.let {
            drawLine(canvas, it.points, it.color, it.width)
        }

        rectParams?.let {
            drawRect(canvas, it.points[0], it.points[1], it.color, it.width)
        }

        for (m in markers) {
            if (m.type == Marker.LINE) {
                val p = m.params as DrawParams
                drawLine(canvas, p.points, p.color, p.width)
            }

            if (m.type == Marker.RECT) {
                val p = m.params as DrawParams
                drawRect(canvas, p.points[0], p.points[1], p.color, p.width)
            }
        }
    }

    private fun drawLine(canvas: Canvas, linePoints: List<PointF>?, color: Int, thickness: Int) {
        linePoints?.takeIf { it.size >= 2 }?.let {
            val vPath = Path() // TODO
            var vPrev = PointF(it[0].x, it[0].y)
            vPath.moveTo(vPrev.x, vPrev.y)
            for (k in 1 until it.size) {
                val vPoint = PointF(it[k].x, it[k].y)
                vPath.quadTo(
                        vPrev.x,
                        vPrev.y,
                        (vPoint.x + vPrev.x) / 2,
                        (vPoint.y + vPrev.y) / 2
                )
                vPrev = vPoint
            }

            drawingPaint.color = color
            drawingPaint.strokeWidth = thickness.toFloat() * (1 / initScale) * scale
            canvas.drawPath(vPath, drawingPaint)
        }
    }

    private fun drawRect(canvas: Canvas, start: PointF, end: PointF, color: Int, thickness: Int) {
        drawingPaint.color = color
        drawingPaint.strokeWidth = thickness.toFloat() * (1 / initScale) * scale
        canvas.drawRect(start.x, start.y, end.x, end.y, drawingPaint)
    }

    fun setColor(color: Int) {
        selectedColor = color
    }

    fun setStrokeWidth(width: Float) {
        selectedStrokeWidth = width
    }
}