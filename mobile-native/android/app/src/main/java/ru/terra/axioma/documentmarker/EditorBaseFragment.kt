package ru.terra.axioma.documentmarker

import android.graphics.Bitmap
import android.graphics.Canvas
import android.os.Bundle
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import nl.komponents.kovenant.ui.failUi
import nl.komponents.kovenant.ui.successUi
import org.koin.android.ext.android.inject
import ru.terra.axioma.R
import ru.terra.axioma.client.ClientApi
import ru.terra.axioma.models.marker.Marker
import ru.terra.axioma.repository.MarkersRepository


abstract class EditorBaseFragment : Fragment() {

    internal lateinit var marker: Marker
    internal val client: ClientApi by inject()
    private val markersRepository: MarkersRepository by inject()

    internal abstract fun onSaveSuccess(marker: Marker)
    internal abstract fun onSaveError(marker: Marker)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        marker = EditableMarker.get() ?: Marker()
    }

    protected fun makeIcon(target: ImageView, colorRes: Int, iconRes: Int) {
        val background = ContextCompat.getDrawable(requireActivity(), R.drawable.ic_label) ?: return

        background.mutate().setTint(ContextCompat.getColor(requireActivity(), colorRes))

        val image = ContextCompat.getDrawable(requireActivity(), iconRes) ?: return

        val size = resources.getDimensionPixelSize(R.dimen.editor_image_size)
        val bitmap = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888)

        val canvas = Canvas(bitmap)
        background.setBounds(0, 0, canvas.width, canvas.height)
        background.draw(canvas)

        val x = (canvas.height * 0.2).toInt()
        val y = x / 2
        image.setBounds(x, y, canvas.width - x, canvas.height - (x + y))
        image.draw(canvas)

        target.setImageBitmap(bitmap)
    }

    protected fun save() {
        if (marker.id != 0) {
            updateMarker()
        } else {
            saveMarker()
        }
    }

    private fun saveMarker() {
        markersRepository.save(marker) successUi {
            if (it == null)
                onSaveError(marker)
            else
                onSaveSuccess(it)
        } failUi {
            onSaveError(marker)
        }
    }

    private fun updateMarker() {
        markersRepository.update(marker) successUi {
            if (it == null)
                onSaveError(marker)
            else
                onSaveSuccess(it)
        } failUi {
            onSaveError(marker)
        }
    }
}
