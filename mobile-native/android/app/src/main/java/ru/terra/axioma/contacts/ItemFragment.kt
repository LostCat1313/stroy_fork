package ru.terra.axioma.contacts

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.res.ColorStateList
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.Nullable
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.amulyakhare.textdrawable.util.ColorGenerator
import kotlinx.android.synthetic.main.fragment_contacts_item.*
import org.koin.android.viewmodel.ext.android.sharedViewModel
import pub.devrel.easypermissions.EasyPermissions
import ru.terra.axioma.R
import ru.terra.axioma.Utils
import ru.terra.axioma.databinding.FragmentPersonalNotesItemBinding
import ru.terra.axioma.models.PersonalNote
import ru.terra.axioma.personalnotes.PersonalNotesViewModel
import ru.terra.axioma.views.ContactContentItemView

class ItemFragment : Fragment() {
    private val vm: ContactsViewModel by sharedViewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_contacts_item, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupActionBar()
        setupSubscribers()
    }

    private fun setupActionBar() {
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_24dp)
        toolbar.contentInsetStartWithNavigation = 0
        toolbar.setNavigationOnClickListener { vm.selectedItem.value = null }
    }

    private fun setupSubscribers() {
        vm.selectedItem.observe(this, Observer {
            it?.let { contact ->
//                header.apply {
//                    val color = ColorGenerator.MATERIAL.getColor(contact.fullName)
//                    setBackgroundColor(color)
//                }

                name.text = contact.fullName

                phone.setData(contact.phoneNumber, ContactContentItemView.ValueType.PERSONAL, ContactContentItemView.Type.PHONE)
                phone.setOnClickListener { call(contact.phoneNumber) }

                /*email.setData(contact.lastName, ContactContentItemView.ValueType.WORK, ContactContentItemView.Type.EMAIL)
                email.setOnClickListener { call(contact.phoneNumber) }*/

                contact.user?.position?.run {
                    job.text = name
                    job.visibility = if (contact.isServerContact) View.VISIBLE else View.GONE
                }

                action.apply {
                    if (contact.user != null) {
                        backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context!!, R.color.green))
                        setImageDrawable(ContextCompat.getDrawable(context!!, R.drawable.message_white))
                        setOnClickListener {
                            Toast.makeText(context!!, "Сообщение", Toast.LENGTH_SHORT).show()
                        }
                    } else {
                        backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context!!, R.color.light_blue))
                        setImageDrawable(ContextCompat.getDrawable(context!!, R.drawable.account_plus))
                        setOnClickListener {
                            Toast.makeText(context!!, "Приглашение", Toast.LENGTH_SHORT).show()
                        }
                    }
                }


            }
        })
    }

    private fun call(phone: String) {
        val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone.replace("[^0-9|+]".toRegex(), "")))
        if (!EasyPermissions.hasPermissions(context!!, Manifest.permission.CALL_PHONE)) {
            EasyPermissions.requestPermissions(this, "", CALL_PERMISSION, Manifest.permission.CALL_PHONE)
            return
        }
        startActivity(intent)
    }

    companion object {

        fun newInstance(): ItemFragment {
            return ItemFragment()
        }

        private const val CALL_PERMISSION = 1
    }
}
