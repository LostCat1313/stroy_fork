package ru.terra.axioma.models.marker

import android.graphics.PointF
import com.google.gson.annotations.JsonAdapter
import ru.terra.axioma.GsonTypeAdapters

class TextParams(
        x: Float,
        y: Float,
        var fontSize: Int,
        @JsonAdapter(GsonTypeAdapters.ColorAdapter::class) var color: Int
) : BaseParams() {

    var point: PointF = PointF(x, y)
//    var textBold: Boolean = false

}
