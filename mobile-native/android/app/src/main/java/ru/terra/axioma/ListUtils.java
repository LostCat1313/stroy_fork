package ru.terra.axioma;

import androidx.annotation.NonNull;

import com.google.common.base.Predicate;

import java.util.List;

import ru.terra.axioma.documentupdater.LinkerItem;
import ru.terra.axioma.models.BaseObject;

public class ListUtils {
    public static int indexOf(@NonNull List<? extends BaseObject> list, BaseObject obj) {
        return indexOf(list, obj.id);
    }

    public static int indexOf(@NonNull List<? extends BaseObject> list, int id) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).id == id)
                return i;
        }

        return -1;
    }

    public static int find(List<LinkerItem> list, Predicate<LinkerItem> condition) {
        for (int i = 0; i < list.size(); i++) {
            if (condition.apply(list.get(i))) {
                return i;
            }
        }
        return -1;
    }
}
