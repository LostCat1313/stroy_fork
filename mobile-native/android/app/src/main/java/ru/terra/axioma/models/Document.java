package ru.terra.axioma.models;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import com.google.common.collect.Lists;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.JsonAdapter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ru.terra.axioma.GsonTypeAdapters;

import static ru.terra.axioma.Constants.VersionType.LATEST;
import static ru.terra.axioma.Constants.VersionType.ORIGINAL;

/**
 * Created by alexei on 10/5/17.
 */

public class Document extends BaseObject implements Parcelable {
    @Expose
    public String title;
    @Expose
    public String shortTitle;
    @Expose
    public Integer parentId;
    @Expose
    public int projectId;
    @Expose
    public boolean isFolder;
    @Expose
    public ProjectFile file;
    @Expose
    public List<Document> children;
    @Expose
    @JsonAdapter(GsonTypeAdapters.VersionListAdapter.class)
    public List<Version> versions;
    @Expose(serialize = false)
    public int depth;

    private List<ProjectFile> combinedPages;

    public Document() {
    }

    public Document(String title, List<Document> children) {
        this.title = title;
        this.children = children;
        isFolder = children != null;
    }

    public List<Document> getChildren() {
        if (children == null)
            children = new ArrayList<>();

        return children;
    }

    public boolean hasVersion() {
        return versions != null && versions.size() > 0;
    }

    public int getPagesCount() {
        return hasVersion() ? versions.get(0).getPagesCount() : 0;
    }

    public boolean isMultiPage() {
        return hasVersion() && versions.get(0).isMultiPage();
    }

    public boolean isMultiVersion() {
        return versions != null && versions.size() > 1;
    }

    @NonNull
    public List<ProjectFile> getPagesList(int versionType) {
        switch (versionType) {
            case ORIGINAL:
                return getOriginalPages();
            case LATEST:
                return getLatestPages();
            default:
                return getAllPages();
        }
    }

    // TODO optimize
    public List<ProjectFile> getAllPages() {
        if (combinedPages != null)
            return combinedPages;

        if (isMultiVersion()) {
            combinedPages = Lists.newArrayList();
            for (ProjectFile file : getOriginalPages()) {
                int fileVersionNumber = 0;
                file.versionNumber = fileVersionNumber;
                combinedPages.add(file);

                for (int i = 1; i < versions.size(); i++) {
                    ProjectFile version = versions.get(i).getPageVersion(file);
                    if (version != null) {
                        version.versionNumber = ++fileVersionNumber;
                        combinedPages.add(version);
                    }
                }
            }

            return combinedPages;
        }

        return getOriginalPages();
    }

    public List<ProjectFile> getLatestPages() {
        List<ProjectFile> latest = Lists.newArrayList();

        if (isMultiVersion()) {
            for (ProjectFile file : getOriginalPages()) {
                if (file.actual)
                    latest.add(file);
                else
                    for (int i = 1; i < versions.size(); i++) {
                        ProjectFile version = versions.get(i).getPageVersion(file);
                        if (version != null) {
                            latest.add(version);
                        }
                    }
            }
        }

        return latest;
    }

    public int getDefaultVersionId() {
        return hasVersion() ? versions.get(0).id : -1;
    }

    public List<ProjectFile> getOriginalPages() {
        return versions == null || versions.isEmpty() ? Lists.newArrayList() : versions.get(0).pages;
    }

    public String getShortTitle() {
        if (shortTitle == null || shortTitle.isEmpty()) {
            if (title.length() == 1)
                return title;

            String[] titleArr = title.split(" ");
            if (titleArr.length == 1)
                return title.substring(0, 2).toUpperCase();

            StringBuilder result = new StringBuilder();
            for (String t : titleArr)
                result.append(t.substring(0, 1));

            return result.toString().toUpperCase();
        }

        return shortTitle;
    }

//    public boolean isExpanded() {
//        return expanded;
//    }

    public int getDepth() {
        return depth;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.shortTitle);
        dest.writeValue(this.parentId == null ? -1 : this.parentId);
        dest.writeInt(this.projectId);
        dest.writeByte(this.isFolder ? (byte) 1 : (byte) 0);
        dest.writeParcelable(this.file, flags);
        dest.writeTypedList(this.children);
        dest.writeTypedList(this.versions);
        dest.writeInt(this.depth);
        dest.writeTypedList(this.combinedPages);
        dest.writeInt(this.id);
        dest.writeLong(this.createdAt != null ? this.createdAt.getTime() : -1);
        dest.writeLong(this.updatedAt != null ? this.updatedAt.getTime() : -1);
        dest.writeInt(this.createdBy);
        dest.writeInt(this.modifiedBy);
    }

    protected Document(Parcel in) {
        this.title = in.readString();
        this.shortTitle = in.readString();
        this.parentId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.projectId = in.readInt();
        this.isFolder = in.readByte() != 0;
        this.file = in.readParcelable(ProjectFile.class.getClassLoader());
        this.children = in.createTypedArrayList(Document.CREATOR);
        this.versions = in.createTypedArrayList(Version.CREATOR);
        this.depth = in.readInt();
        this.combinedPages = in.createTypedArrayList(ProjectFile.CREATOR);
        this.id = in.readInt();
        long tmpCreatedAt = in.readLong();
        this.createdAt = tmpCreatedAt == -1 ? null : new Date(tmpCreatedAt);
        long tmpUpdatedAt = in.readLong();
        this.updatedAt = tmpUpdatedAt == -1 ? null : new Date(tmpUpdatedAt);
        this.createdBy = in.readInt();
        this.modifiedBy = in.readInt();
    }

    public static final Creator<Document> CREATOR = new Creator<Document>() {
        @Override
        public Document createFromParcel(Parcel source) {
            return new Document(source);
        }

        @Override
        public Document[] newArray(int size) {
            return new Document[size];
        }
    };
}
