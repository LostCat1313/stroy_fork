package ru.terra.axioma.projects

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import org.koin.android.ext.android.getKoin
import org.koin.androidx.viewmodel.ext.android.viewModel
import ru.terra.axioma.*
import ru.terra.axioma.client.ClientApi

class ProjectsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_projects)

        val vm: ProjectsViewModel by viewModel()

        supportFragmentManager
                .beginTransaction()
                .replace(R.id.container, ListFragment())
                .commit()

        vm.selectedProject.observe(this, Observer {
            if (it != null) {
                supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.container, ItemFragment())
                        .addToBackStack(null)
                        .commit()
            } else {
                supportFragmentManager.popBackStack()
            }
        })
    }
}