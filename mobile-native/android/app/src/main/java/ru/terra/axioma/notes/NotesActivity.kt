package ru.terra.axioma.notes

import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.activity_notes.*
import org.koin.android.ext.android.inject
import ru.terra.axioma.App
import ru.terra.axioma.R
import ru.terra.axioma.adapters.ProjectNotesAdapter
import ru.terra.axioma.main.MainActivityHelper
import ru.terra.axioma.models.marker.Marker
import ru.terra.axioma.repository.DocumentsRepository
import ru.terra.axioma.repository.ProjectNotesRepository
import ru.terra.axioma.store.CurrentSessionStore

class NotesActivity : AppCompatActivity(), ProjectNotesAdapter.OnClickListener {
    private val notesRepository: ProjectNotesRepository by inject()
    private val currentSessionStore: CurrentSessionStore by inject()
    private val documentsRepository: DocumentsRepository by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notes)

        val adapter = ProjectNotesAdapter(this)
        list.adapter = adapter
        notesRepository.data.observe(this, Observer { value ->
            adapter.setItems(value)
        })

        setupActionBar()

        notesRepository.fetchNotesByProject(currentSessionStore.project!!.id)
    }

    private fun setupActionBar() {
        val toolbarView = layoutInflater.inflate(R.layout.toolbar_title_and_progress, toolbar)
        toolbarView.findViewById<TextView>(R.id.title).setText(R.string.common_notes)

        val progressBar = toolbarView.findViewById<ProgressBar>(R.id.progress)
        notesRepository.isLoading.observe(this, Observer { value ->
            progressBar.let{
                it.visibility = if (value) View.VISIBLE else View.GONE
            }
        })

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_24dp)
        toolbar.contentInsetStartWithNavigation = 0
        toolbar.setNavigationOnClickListener { view -> onBackPressed() }
    }

    override fun onItemClick(note: Marker) {
        val documents = documentsRepository.documents
        MainActivityHelper.openDocument(this, documents, note)
    }
}
