package ru.terra.axioma

import java.text.SimpleDateFormat
import java.util.*

object StringUtils {
    fun dateToTimeString(date: Date): String {
        val format = SimpleDateFormat("HH:mm", Locale.ROOT)
        return format.format(date)
    }

    fun dateToDateString(date: Date): String {
        val format = SimpleDateFormat("dd.MM.yyyy", Locale.ROOT)
        return format.format(date)
    }

    fun dateToDateTimeString(date: Date): String {
        val format = SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.ROOT)
        return format.format(date)
    }

    fun concatStrings(f: String?, s: String?): String {
        if (f == null && s != null)
            return String.format("%1\$s", s)
        else if (f != null && s == null)
            return String.format("%1\$s", f)
        else if (f != null && s != null)
            return String.format("%1\$s %2\$s", f, s)
        else
            return "Имя и фамилия не указаны"
    }

    fun getFirstLetters(name: String?): String {
        var res = ""

        name?.let {
            if (it.isBlank())
                return@let

            val arr = it.trim().split(" ").filter { v -> v.isNotBlank() }
            res = if (arr.size > 1) {
                val first = arr[0].substring(0, 1)
                val second = arr[1].substring(0, 1)
                first + second
            } else {
                it.substring(0, 1)
            }
        }

        return res
    }

    fun formatPhoneNumber(phone: String): String {
        var res = phone

        if (res.startsWith("8"))
            res = res.replaceFirst("8".toRegex(), "+7")

        res = res.replace("-", "")
        res = res.replace(" ", "")
        res = res.replace("(", "")
        res = res.replace(")", "")
        return res
    }
}
