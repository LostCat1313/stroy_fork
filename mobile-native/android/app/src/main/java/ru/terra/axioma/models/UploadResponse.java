package ru.terra.axioma.models;

import java.util.List;

/**
 * Created by alexei on 11.01.18.
 */

public class UploadResponse {
    public List<ProjectFile> files;
}
