package ru.terra.axioma.repository

import androidx.lifecycle.MutableLiveData
import nl.komponents.kovenant.Promise
import nl.komponents.kovenant.task
import nl.komponents.kovenant.then
import nl.komponents.kovenant.ui.alwaysUi
import nl.komponents.kovenant.ui.successUi
import org.koin.core.KoinComponent
import ru.terra.axioma.client.ClientApi
import ru.terra.axioma.models.Notification
import timber.log.Timber
import java.lang.Exception

class NotificationsRepository constructor(private val client: ClientApi): KoinComponent {

    var notifications: MutableLiveData<MutableList<Notification>> = MutableLiveData()
    var isLoading: MutableLiveData<Boolean> = MutableLiveData()

    val notificationsList: List<Notification>
        get() = notifications.value ?: ArrayList()

    fun fetch() {
        isLoading.value = true

        task {
            client.notifications.execute()
        }.alwaysUi {
            isLoading.value = false
        } then {
            it.body()?.list ?: notificationsList
        } successUi {
            notifications.value = it as MutableList<Notification>
        } fail {
            Timber.d(it)
        }
    }

    fun fetch(page: Int, limit: Int = 15) {
        isLoading.postValue(true)

        task {
            client.getNotifications(page * limit, limit).execute()
        } alwaysUi {
            isLoading.value = false
        } then {
            it.body()?.list ?: notificationsList
        } successUi {
            notifications.value = it as MutableList<Notification>
        } fail {
            Timber.d(it)
        }
    }
}
