package ru.terra.axioma.personalnotes

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import org.koin.androidx.viewmodel.ext.android.viewModel
import ru.terra.axioma.R

class PersonalNotesActivity : AppCompatActivity() {
    private val viewModel: PersonalNotesViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_personal_notes)

        supportFragmentManager
                .beginTransaction()
                .replace(R.id.fragment_container, PersonalNotesListFragment.newInstance())
                .commit()

        viewModel.selectedItem.observe(this, Observer { personalNote ->
            if (personalNote != null) {
                supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.fragment_container, PersonalNotesItemFragment.newInstance())
                        .addToBackStack(null)
                        .commit()
            } else {
                supportFragmentManager.popBackStack()
            }
        })
    }

    override fun onBackPressed() {
        if (viewModel.isSelectionMode.value == true)
            viewModel.toggleSelectionMode()
        else
            super.onBackPressed()
    }
}
