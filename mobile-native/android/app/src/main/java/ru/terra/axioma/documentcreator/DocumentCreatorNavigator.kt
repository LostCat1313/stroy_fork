package ru.terra.axioma.documentcreator

import android.net.Uri

interface DocumentCreatorNavigator {

    interface View {
        fun close()

        fun cancelAndClose()

        fun showRenderPdfProgress(max: Int)

        fun updateProgress(value: Int, max: Int)

        fun showError(error: Int)

        fun showError(error: String)

        fun showLoadingToServerProgress()

        fun dismissProgress()
    }

    interface Action {
        fun cancelPdfRenderTask()

        fun renderPage(uri: Uri)
    }
}
