package ru.terra.axioma.chat_v2


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.fragment_chat_item.*
import org.koin.android.viewmodel.ext.android.sharedViewModel
import ru.terra.axioma.R
import ru.terra.axioma.chat_v2.adapters.ViewPagerAdapter


class ItemFragment : Fragment() {

    val viewModel: ChatViewModel by sharedViewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_chat_item, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        toolbar.setNavigationOnClickListener { activity?.onBackPressed() }
        toolbar.setOnClickListener {
            viewPager.currentItem = 1 - viewPager.currentItem
        }
        toolbar.contentInsetStartWithNavigation = 0

        viewModel.selectedChat.observe(this, Observer {
            it?.run {
                toolbar.title = name
                if (!isPrivate)
                    toolbar.subtitle = getString(R.string.common_users_count, users.size)
            }
        })

        viewPager.adapter = ViewPagerAdapter(this)
    }
}
