package ru.terra.axioma.chat_v2.adapters

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import ru.terra.axioma.chat_v2.ChatDialogFragment
import ru.terra.axioma.chat_v2.ChatInfoFragment

class ViewPagerAdapter(fragment: Fragment) : FragmentStateAdapter(fragment) {

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> ChatDialogFragment()
            else -> ChatInfoFragment()
        }
    }

    override fun getItemCount(): Int = 2
}
