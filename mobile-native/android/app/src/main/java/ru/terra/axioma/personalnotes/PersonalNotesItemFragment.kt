package ru.terra.axioma.personalnotes

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.Nullable
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import org.koin.android.viewmodel.ext.android.sharedViewModel
import ru.terra.axioma.R
import ru.terra.axioma.Utils
import ru.terra.axioma.databinding.FragmentPersonalNotesItemBinding
import ru.terra.axioma.models.PersonalNote

class PersonalNotesItemFragment : Fragment() {
    private var binding: FragmentPersonalNotesItemBinding? = null
    private val viewModel: PersonalNotesViewModel by sharedViewModel()
    private val item: PersonalNote? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_personal_notes_item, container, false)

        binding!!.dummy.setOnFocusChangeListener { _, _ -> Utils.hideKeyboard(requireActivity()) }

        viewModel.selectedItem.observe(this, Observer { personalNote ->
            if (personalNote != null)
                binding!!.text.setText(personalNote.content)
        })

        setupActionBar()
        setupInput()

        return binding!!.root
    }

    override fun onViewCreated(view: View, @Nullable savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (viewModel.isEditMode) {
            binding!!.text.requestFocus()
            Utils.showSoftInput(requireContext(), true)
        }
    }

    override fun onDetach() {
        super.onDetach()
        viewModel.isEditMode = false
    }

    private fun setupInput() {
        binding!!.text.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                binding?.toolbar?.inflateMenu(R.menu.menu_notes)
                binding?.toolbar?.setOnMenuItemClickListener {
                    viewModel.save(binding!!.text.text.toString())
                    false
                }
                viewModel.isEditMode = true
            }
        }
    }

    private fun setupActionBar() {
        binding!!.toolbar.setNavigationIcon(R.drawable.ic_arrow_back_24dp)
        binding!!.toolbar.contentInsetStartWithNavigation = 0
        binding!!.toolbar.setNavigationOnClickListener { view ->
            Utils.hideKeyboard(requireActivity())
            viewModel.setSelectedItemValue(null)
        }
    }

    companion object {

        fun newInstance(): PersonalNotesItemFragment {
            return PersonalNotesItemFragment()
        }
    }
}
