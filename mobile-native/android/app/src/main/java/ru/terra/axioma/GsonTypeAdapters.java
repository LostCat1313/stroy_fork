package ru.terra.axioma;

import android.graphics.Color;
import android.util.SparseIntArray;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import ru.terra.axioma.models.ProjectFile;
import ru.terra.axioma.models.Version;
import ru.terra.axioma.models.marker.BaseParams;
import ru.terra.axioma.models.marker.DrawParams;
import ru.terra.axioma.models.marker.LinkParams;
import ru.terra.axioma.models.marker.Marker;
import ru.terra.axioma.models.marker.NoteParams;
import ru.terra.axioma.models.marker.TextParams;
import timber.log.Timber;

/**
 * Created by alexei on 26.02.18.
 */

public class GsonTypeAdapters {

    public class VersionListAdapter implements JsonDeserializer<List<Version>> {
        @Override
        public List<Version> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            List<Version> result = new Gson().fromJson(json, typeOfT);
            if (!result.isEmpty()) {
                Collections.sort(result, (o1, o2) -> o1.createdAt.compareTo(o2.createdAt));
                result.get(0).title = "Исходный";
            }
            return result;
        }
    }

    public class VersionAdapter implements JsonDeserializer<Version> {
        @Override
        public Version deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            JsonObject jsonObject = json.getAsJsonObject();

            JsonElement pagesJson = jsonObject.get("pages");
            ProjectFile[] pagesArray = new Gson().fromJson(pagesJson, ProjectFile[].class);
            List<ProjectFile> pages;
            if (pagesArray != null) {
                pages = Lists.newArrayList(pagesArray);
                Collections.sort(pages, (o1, o2) -> Integer.compare(o1.pageNumber, o2.pageNumber));
            } else
                pages = Lists.newArrayList();

            JsonElement dateJson = jsonObject.get("createdAt");
            Date createdAt = Utils.jsonToDate(dateJson);
            String title = StringUtils.INSTANCE.dateToDateString(createdAt);

            JsonElement fileJson = jsonObject.get("file");
            ProjectFile file = new Gson().fromJson(fileJson, ProjectFile.class);

            return new Version(
                    jsonObject.get("id").getAsInt(),
                    createdAt,
                    Utils.jsonToDate(jsonObject.get("updatedAt")),
                    -1, //jsonObject.get("createdBy").getAsInt(),
                    -1, //jsonObject.get("modifiedBy").getAsInt(),
                    jsonObject.get("documentId").getAsInt(),
                    title,
                    file,
                    0, //jsonObject.get("fileId").getAsInt(),
                    pages
            );
        }
    }

    public class MarkerAdapter implements JsonDeserializer<Marker> {
        @Override
        public Marker deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            JsonObject jsonObject = json.getAsJsonObject();

            int id = jsonObject.get("id").getAsInt();
            int type = jsonObject.get("type").getAsInt();
            String title = stringOrNull(jsonObject.get("title"));
            String text = stringOrNull(jsonObject.get("text"));
            int docId = jsonObject.get("docId").getAsInt();
            int holderId = jsonObject.get("holderId").getAsInt();
            int createdBy = jsonObject.get("createdBy").getAsInt();
            Date createdAt = Utils.jsonToDate(jsonObject.get("createdAt"));

            JsonElement paramsJson = jsonObject.get("params");
            BaseParams params = null;
            switch (type) {
                case Marker.EDIT: params = new Gson().fromJson(paramsJson, LinkParams.class); break;
                case Marker.LINK: params = new Gson().fromJson(paramsJson, LinkParams.class); break;
                case Marker.NOTE: params = new Gson().fromJson(paramsJson, NoteParams.class); break;
                case Marker.TEXT: params = new Gson().fromJson(paramsJson, TextParams.class); break;
                case Marker.RECT: params = new Gson().fromJson(paramsJson, DrawParams.class); break;
                case Marker.LINE: params = new Gson().fromJson(paramsJson, DrawParams.class); break;
            }

            return new Marker(id, type, title, text, params, docId, holderId, createdAt, createdBy);
        }

        private String stringOrNull(JsonElement json) {
            return json.isJsonNull() ? null : json.getAsString();
        }
    }

    public class ColorAdapter implements JsonDeserializer<Integer>, JsonSerializer<Integer> {
        @Override
        public Integer deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            try {
                String color = json.getAsString();
                return Color.parseColor(color);
            } catch (Exception e) {
                Timber.e(e, json.toString());
                return Color.BLACK;
            }
        }

        @Override
        public JsonElement serialize(Integer obj, Type typeOfSrc, JsonSerializationContext context) {
            //            jsonObj.add("color", "#" + Integer.toHexString(obj).toUpperCase());
            return new JsonPrimitive("#" + Integer.toHexString(obj).toUpperCase());
        }
    }

    public class UsersRightsAdapter implements JsonDeserializer<SparseIntArray> {
        @Override
        public SparseIntArray deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            SparseIntArray result = new SparseIntArray();

            JsonArray array = json.getAsJsonArray();
            for (JsonElement el : array) {
                try {
                    JsonElement mask = el.getAsJsonObject().get("rightMask");
                    result.put(
                            el.getAsJsonObject().get("id").getAsInt(),
                            mask.isJsonNull() ? 1 : mask.getAsInt()
                    );
                } catch (Exception e) {
                    Timber.e(e, json.toString());
                }
            }

            return result;
        }
    }
}
