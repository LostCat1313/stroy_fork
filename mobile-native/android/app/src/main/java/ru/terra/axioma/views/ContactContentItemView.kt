package ru.terra.axioma.views

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout

import kotlinx.android.synthetic.main.contact_content_item_view.view.*
import ru.terra.axioma.R

class ContactContentItemView : ConstraintLayout {
    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    init {
        inflate(context, R.layout.contact_content_item_view, this)
    }

    fun setData(value: String, valueType: ValueType, type: Type, showDivider: Boolean = true) {
        valueView.text = value

        valueTypeView.text = valueType.getText()

        typeView.setImageResource(type.getResId())

        divider.visibility = if (showDivider) View.VISIBLE else View.GONE
    }

    enum class ValueType {
        PERSONAL {
            override fun getText() = "Личный"
        },
        WORK {
            override fun getText() = "Рабочий"
        };

        abstract fun getText(): String
    }

    enum class Type {
        PHONE {
            override fun getResId() = R.drawable.phone
        },
        EMAIL {
            override fun getResId() = R.drawable.phone
        },
        ADDRESS {
            override fun getResId() = R.drawable.phone
        };

        abstract fun getResId(): Int
    }
}
