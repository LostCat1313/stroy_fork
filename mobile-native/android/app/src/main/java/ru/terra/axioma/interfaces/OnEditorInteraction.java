package ru.terra.axioma.interfaces;

import ru.terra.axioma.models.marker.Marker;

/**
 * Created by alexei on 04.01.18.
 */

public interface OnEditorInteraction {
    void onOk();
    void onOk(Marker marker);
    void onCancel();
//    ClientApi getClient();
}