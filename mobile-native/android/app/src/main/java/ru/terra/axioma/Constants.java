package ru.terra.axioma;

/**
 * Created by alexei on 23.03.18.
 */

public class Constants {
    public final static int CAMERA_REQUEST = 0;
    public final static int GALLERY_REQUEST = 1;
    public final static int IMAGE_EDITOR_REQUEST = 2;
    public final static int CREATE_DOCUMENT_REQUEST = 3;
    public final static int UPDATE_DOCUMENT_REQUEST = 4;

    public final static String DOCUMENT = "document";
    public final static String CONTACT = "contact";

    public final static String UPDATE_NOTIFICATION_LIST = "UPDATE_NOTIFICATION_LIST";
    public final static String FILTER_ACTIVATION_CODE = "FILTER_ACTIVATION_CODE";

    public class NotificationType {
        public final static int ACTIVATION_CODE = 1;
        public final static int CHAT_MESSAGE = 201;
        public final static int CHAT_ROOM_ADDED = 202;
        public final static int DOCUMENT_ADDED = 101;
        public final static int DOCUMENT_REMOVED = 102;
        public final static int DOCUMENT_PAGES_ADDED = 103;
        public final static int DOCUMENT_UPDATED = 104;
        public final static int DOCUMENT_DESCRIPTOR_ADDED = 105;
        public final static int DOCUMENT_VERSION_ADDED = 106;
    }

    public final static String NOTIFICATION_PROJECT_ID = "project_id";
    public final static String NOTIFICATION = "notification";
    public final static String ATTACHMENT_URI = "newPageUri";

    public class VersionType {
        public final static int ALL = 0;
        public final static int ORIGINAL = 1;
        public final static int LATEST = 2;
    }

    public final static int REQUEST_CREATE = 0;
    public final static int REQUEST_EDIT = 1;

    public final static String ACTION_CREATE = "create";
    public final static String ACTION_EDIT = "edit";
}
