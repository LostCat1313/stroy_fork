package ru.terra.axioma.chat_v2.adapters

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton

import com.facebook.drawee.view.SimpleDraweeView

import java.util.ArrayList

import androidx.recyclerview.widget.RecyclerView
import ru.terra.axioma.Configuration
import ru.terra.axioma.R
import ru.terra.axioma.views.CircleProgressBarDrawable


class AttachmentsAdapter(private val onClick: (uri: Uri) -> Unit = {}) : RecyclerView.Adapter<AttachmentsAdapter.ViewHolder>() {

    private var objects: MutableList<Uri>? = null

    var data: MutableList<Uri>?
        get() = objects
        set(data) {
            this.objects = data
            notifyDataSetChanged()
        }

    init {
        objects = ArrayList()
    }

    inner class ViewHolder internal constructor(view: View) : RecyclerView.ViewHolder(view) {
        internal var image: SimpleDraweeView = view.findViewById(R.id.photo)
        internal var remove: ImageButton = view.findViewById(R.id.remove)

        init {
            val progress = CircleProgressBarDrawable()
            progress.backgroundColor = view.context.resources.getColor(R.color.grey)
            progress.color = view.context.resources.getColor(R.color.grey_600)
            image.hierarchy.setProgressBarImage(progress)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, type: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.attachment_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        objects?.get(position)?.let { uri ->
            holder.image.setImageURI(if (uri.scheme != null) uri else Uri.parse(Configuration.baseUrl + uri.toString()), holder.itemView.context)
            holder.image.setOnClickListener { onClick(uri) }
            holder.remove.setOnClickListener {
                objects?.indexOf(uri)?.let { i ->
                    objects?.remove(uri)
                    notifyItemRemoved(i)
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return objects?.size ?: 0
    }
}