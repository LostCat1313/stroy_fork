package ru.terra.axioma.di

import org.koin.android.ext.koin.androidApplication
import org.koin.android.ext.koin.androidContext
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.core.scope.ScopeID
import org.koin.dsl.module
import ru.terra.axioma.chat_v2.ChatViewModel
import ru.terra.axioma.contacts.ContactsViewModel
import ru.terra.axioma.documentcreator.DocumentCreatorActivity
import ru.terra.axioma.documentcreator.DocumentCreatorNavigator
import ru.terra.axioma.documentcreator.DocumentCreatorViewModel
import ru.terra.axioma.documentstructure.DocumentListViewModel
import ru.terra.axioma.documentupdater.DocumentUpdaterActivity
import ru.terra.axioma.documentupdater.DocumentUpdaterNavigator
import ru.terra.axioma.documentupdater.DocumentUpdaterViewModel
import ru.terra.axioma.login.LoginViewModel
import ru.terra.axioma.main.MainNavigator
import ru.terra.axioma.main.MainViewModel
import ru.terra.axioma.models.Document
import ru.terra.axioma.models.chat.Message
import ru.terra.axioma.personalnotes.PersonalNotesViewModel
import ru.terra.axioma.projects.ProjectsViewModel
import ru.terra.axioma.registration.RegistrationViewModel
import ru.terra.axioma.repository.*
import ru.terra.axioma.store.ChatStore
import ru.terra.axioma.store.CurrentSessionStore

val appModule = module {
     single { CurrentSessionStore() }

     viewModel { LoginViewModel(androidApplication(), get(), get()) }
     viewModel { ProjectsViewModel(androidApplication(), get(), get()) }
     viewModel { ChatViewModel(androidApplication(), get(), get(), get(), get()) }
     viewModel { DocumentListViewModel(androidApplication(), get(), get(), get()) }
     viewModel { RegistrationViewModel(androidApplication(), get(), get(), get(), get()) }
     viewModel { ContactsViewModel(androidApplication(), get(), get()) }
     viewModel { PersonalNotesViewModel(androidApplication(), get()) }
     viewModel { (navigator : MainNavigator) -> MainViewModel(androidApplication(), get(), navigator, get(), get(), get()) }
     viewModel { (parent : Document) -> DocumentCreatorViewModel(androidApplication(), get(), get(), parent) }
     viewModel { (parent : Document, navigator: DocumentUpdaterNavigator.View) -> DocumentUpdaterViewModel(androidApplication(), get(), get(), parent, navigator) }

     single { RecentDocumentsRepository(get()) }
     single { NotificationsRepository(get()) }
     single { ProjectsRepository(get()) }
     single { DocumentsRepository(get(), get()) }
     single { MarkersRepository(get()) }
     single { FilesRepository(androidContext(), get()) }
     single { PositionsRepository(get()) }
     single { OrganizationsRepository(get()) }
     single { PhoneBookContactsRepository(androidContext()) }
     single { UsersRepository(get()) }
     single { ProjectNotesRepository(get()) }

     single { ChatStore() }

     scope(named("message")) {
          scoped { Message() }
     }
}