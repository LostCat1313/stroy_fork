package ru.terra.axioma.login

import android.content.Context
import android.graphics.PorterDuff
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.afollestad.materialdialogs.callbacks.onDismiss
import kotlinx.android.synthetic.main.fragment_plain_login.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

import ru.terra.axioma.R
import ru.terra.axioma.views.CustomDialog


class PlainLoginFragment : Fragment() {
    private var listener: OnFragmentInteractionListener? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_plain_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val viewModel: LoginViewModel by sharedViewModel()

        enter.setOnClickListener { viewModel.login(login.text.toString(), password.text.toString()) }

        progress.indeterminateDrawable.setColorFilter(ContextCompat.getColor(context!!, android.R.color.white), PorterDuff.Mode.SRC_IN)

        viewModel.showLoading.observe(this, Observer {
            form.visibility = if (it) View.GONE else View.VISIBLE
            progress.visibility = if (it) View.VISIBLE else View.GONE
        })

        viewModel.showError.observe(this, Observer {
            it?.let { message ->
                CustomDialog[context!!]
                        .title(R.string.common_error)
                        .message(text = message)
                        .onDismiss { viewModel.showLoading.postValue(false) }
                        .show()
            }
        })

        enterBySms.setOnClickListener {
            listener?.showSmsLogin()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    companion object {
        @JvmStatic
        fun newInstance() = PlainLoginFragment()
    }
}
