package ru.terra.axioma.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Date;

import ru.terra.axioma.models.marker.Marker;

/**
 * Created by alexei on 10/16/17.
 */

public class ProjectFile extends BaseObject implements Parcelable {
    public int docId;
    public int docVersionId;
    @SerializedName("fid")
    public String fileId;
    public String original;
    public int pageNumber;
    public int type;
    public boolean actual;
    public int versionNumber;

    public String url;
    public String thumbUrl;
    public ArrayList<Marker> descriptors;

    public ProjectFile() {
    }

    public boolean containsMarker(int id) {
        for (Marker m : descriptors) {
            if (m.id == id)
                return true;
        }

        return false;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.docId);
        dest.writeInt(this.docVersionId);
        dest.writeString(this.fileId);
        dest.writeString(this.original);
        dest.writeInt(this.pageNumber);
        dest.writeInt(this.type);
        dest.writeByte(this.actual ? (byte) 1 : (byte) 0);
        dest.writeInt(this.versionNumber);
        dest.writeString(this.url);
        dest.writeString(this.thumbUrl);
        dest.writeTypedList(this.descriptors);
        dest.writeInt(this.id);
        dest.writeLong(this.createdAt != null ? this.createdAt.getTime() : -1);
        dest.writeLong(this.updatedAt != null ? this.updatedAt.getTime() : -1);
        dest.writeInt(this.createdBy);
        dest.writeInt(this.modifiedBy);
    }

    protected ProjectFile(Parcel in) {
        this.docId = in.readInt();
        this.docVersionId = in.readInt();
        this.fileId = in.readString();
        this.original = in.readString();
        this.pageNumber = in.readInt();
        this.type = in.readInt();
        this.actual = in.readByte() != 0;
        this.versionNumber = in.readInt();
        this.url = in.readString();
        this.thumbUrl = in.readString();
        this.descriptors = in.createTypedArrayList(Marker.CREATOR);
        this.id = in.readInt();
        long tmpCreatedAt = in.readLong();
        this.createdAt = tmpCreatedAt == -1 ? null : new Date(tmpCreatedAt);
        long tmpUpdatedAt = in.readLong();
        this.updatedAt = tmpUpdatedAt == -1 ? null : new Date(tmpUpdatedAt);
        this.createdBy = in.readInt();
        this.modifiedBy = in.readInt();
    }

    public static final Creator<ProjectFile> CREATOR = new Creator<ProjectFile>() {
        @Override
        public ProjectFile createFromParcel(Parcel source) {
            return new ProjectFile(source);
        }

        @Override
        public ProjectFile[] newArray(int size) {
            return new ProjectFile[size];
        }
    };
}
