package ru.terra.axioma.login

interface OnFragmentInteractionListener {
    fun goNext()
    fun showRegistration()
    fun showPlainLogin()
    fun showSmsLogin()
}