package ru.terra.axioma.models

import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.JsonObject

class Organization(): SimpleType() {
    constructor(json: JsonElement) : this() {
        val v = Gson().fromJson(json, Organization::class.java)

        id = v.id
        name = v.name
    }
}