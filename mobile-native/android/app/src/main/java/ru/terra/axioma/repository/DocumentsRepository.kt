package ru.terra.axioma.repository

import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.google.gson.JsonObject
import nl.komponents.kovenant.Promise
import nl.komponents.kovenant.task
import nl.komponents.kovenant.then
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import ru.terra.axioma.DocumentUtils
import ru.terra.axioma.client.ClientApi
import ru.terra.axioma.models.Document
import ru.terra.axioma.models.DocumentEntry
import ru.terra.axioma.store.CurrentSessionStore
import timber.log.Timber

class DocumentsRepository(private val client: ClientApi, private var currentSessionStore: CurrentSessionStore) {

    var isLoading: MutableLiveData<Boolean> = MutableLiveData()

    var documents: ArrayList<Document> = arrayListOf()

    fun fetch(): Promise<ArrayList<Document>, Exception> {
        return fetch(currentSessionStore.project?.id ?: 0)
    }

    fun fetch(projectId: Int): Promise<ArrayList<Document>, Exception> {
        return task {
            client.getDocuments(projectId).execute()
        } then { res ->
            documents = res.body()?.list ?: documents
            documents
        } fail {
            Timber.e(it)
        }
    }

    fun delete(document: Document): Promise<Response<ResponseBody>, Exception> {
        return task {
            client.deleteDocument(document.id).execute()
        }
    }

    fun update(document: Document): Promise<Response<Document>, Exception> {
        return task {
            client.updateDocument(document.id, DocumentEntry(document)).execute()
        }
    }

    fun createFolder(parent: Document, title: String, onDone: (Document) -> Unit) {
        create(parent, true, title, onDone = onDone)
    }

    fun createDocument(parent: Document, title: String, shortTitle: String?, onDone: (Document) -> Unit = {}, onError: () -> Unit = {}) {
        create(parent, false, title, shortTitle, onDone, onError)
    }

    private fun create(
            parent: Document,
            isFolder: Boolean,
            title: String,
            shortTitle: String? = null,
            onDone: (Document) -> Unit = {},
            onError: () -> Unit = {}
    ) {

        val entry = JsonObject()
        entry.addProperty("title", title)
        entry.addProperty("shortTitle", shortTitle)
        if (parent.id != -1)
            entry.addProperty("parentId", parent.id)
        entry.addProperty("projectId", parent.projectId)
        entry.addProperty("isFolder", isFolder)

        val obj = JsonObject()
        obj.add("entry", entry)

        task {
            client.createDocument(obj).execute()
        } then { response ->
            response.body()?.let {
                val document = Gson().fromJson(it, Document::class.java)
                if (document.title != null || isFolder) // TODO Проверять на success
                    onDone(document)
                else
                    onError()
            }
        }
    }

    fun find(id: Int): Document? {
        return DocumentUtils.findDocument(documents, id)
    }

    fun clear2() {
        documents = ArrayList<Document>()
    }
}
