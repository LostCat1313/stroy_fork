package ru.terra.axioma.models

import android.os.Parcel
import android.os.Parcelable

import com.google.gson.annotations.SerializedName

import java.util.Date

import ru.terra.axioma.StringUtils

class User() : BaseObject(), Parcelable {
    var name: String? = null
    var email: String? = null
    var isActive: Boolean = false
    var isAdmin: Boolean = false
    var activationCode: String = ""

    var firstName: String? = null
    var lastName: String? = null
    var secondName: String? = null

    var phoneNumber: String = ""
    var positionId: Int? = null
    var position: Position? = null

    var rightMask: Int = 0

    val fullName: String
        get() = StringUtils.concatStrings(firstName, lastName)

    constructor(parcel: Parcel) : this() {
        name = parcel.readString()
        email = parcel.readString()
        isActive = parcel.readByte() != 0.toByte()
        isAdmin = parcel.readByte() != 0.toByte()
        activationCode = parcel.readString()
        firstName = parcel.readString()
        lastName = parcel.readString()
        secondName = parcel.readString()
        phoneNumber = parcel.readString()
        positionId = parcel.readValue(Int::class.java.classLoader) as? Int
        position = parcel.readParcelable(Position::class.java.classLoader)
        rightMask = parcel.readInt()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(email)
        parcel.writeByte(if (isActive) 1 else 0)
        parcel.writeByte(if (isAdmin) 1 else 0)
        parcel.writeString(activationCode)
        parcel.writeString(firstName)
        parcel.writeString(lastName)
        parcel.writeString(secondName)
        parcel.writeString(phoneNumber)
        parcel.writeValue(positionId)
        parcel.writeParcelable(position, flags)
        parcel.writeInt(rightMask)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<User> {
        override fun createFromParcel(parcel: Parcel): User {
            return User(parcel)
        }

        override fun newArray(size: Int): Array<User?> {
            return arrayOfNulls(size)
        }
    }


}
