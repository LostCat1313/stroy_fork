package ru.terra.axioma.store

import ru.terra.axioma.models.User

class UserStore {
    private var user = User()

    private object Holder {
        val INSTANCE = UserStore()
    }

    companion object {
        val instance: UserStore by lazy { Holder.INSTANCE }

        var user: User
            get() { return instance.user }
            set(value) { instance.user = value }
    }
}