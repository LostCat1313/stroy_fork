package ru.terra.axioma.models;

import android.net.Uri;

public class PdfPage {
    public int number;
    public Uri uri;

    public PdfPage(int number, Uri uri) {
        this.number = number;
        this.uri = uri;
    }
}
