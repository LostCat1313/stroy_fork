package ru.terra.axioma.documentmarker;

import ru.terra.axioma.models.marker.Marker;

public class EditableMarker {
    private Marker marker;

    private static EditableMarker INSTANCE;

    public static Marker get() {
        if (INSTANCE == null)
            INSTANCE = new EditableMarker();
        return INSTANCE.marker;
    }

    public static void set(Marker marker) {
        if (INSTANCE == null)
            INSTANCE = new EditableMarker();
        INSTANCE.marker = marker;
    }

    public static void destroy() {
        INSTANCE.marker = null;
    }
}
