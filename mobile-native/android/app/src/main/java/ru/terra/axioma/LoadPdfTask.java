package ru.terra.axioma;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.pdf.PdfRenderer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.ParcelFileDescriptor;

import com.google.common.collect.Lists;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

public class LoadPdfTask extends AsyncTask<String, Integer, List<Uri>> {
    public interface LoadPdfListener {
        void onStart(int max);
        void onProgress(int value, int max);
        void onDone(List<Uri> result);
    }

    private LoadPdfTask.LoadPdfListener listener;
    private final WeakReference<Context> context;
    private File dir;
    private int pageCount;

    public LoadPdfTask(Context context, File dir) {
        this.context = new WeakReference<>(context);
        this.dir = dir;
    }

    public void setListener(LoadPdfTask.LoadPdfListener listener) {
        this.listener = listener;
    }

    @Override
    protected List<Uri> doInBackground(String... strings) {
        try {
            ArrayList<Uri> result = Lists.newArrayList();

            File tmpPdf = FileUtils.createTempPdfFile(context.get(), dir);
            FileUtils.writeToFile(context.get(), strings[0], tmpPdf);

            ParcelFileDescriptor mFileDescriptor = ParcelFileDescriptor.open(tmpPdf, ParcelFileDescriptor.MODE_READ_ONLY);
            PdfRenderer pdfRenderer = new PdfRenderer(mFileDescriptor);
            pageCount = pdfRenderer.getPageCount();

            if (listener != null)
                listener.onStart(pageCount);

            for (int i = 0; i < pageCount; i++) {
                PdfRenderer.Page page = pdfRenderer.openPage(i);
                Bitmap bitmap = Bitmap.createBitmap(
                        page.getWidth() * 2,
                        page.getHeight() * 2,
                        Bitmap.Config.ARGB_8888
                );

                Canvas canvas = new Canvas(bitmap);
                canvas.drawColor(Color.WHITE);


                page.render(bitmap, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);
                page.close();

                File tmp = FileUtils.createTempJPEG(dir);
                if (bitmap.compress(Bitmap.CompressFormat.JPEG, 90, new FileOutputStream(tmp))) {
                    bitmap.recycle();
                    result.add(Uri.fromFile(tmp));
                }

                publishProgress(i + 1);
            }

            return result;
        } catch (IOException e) {
            Timber.e(e);
            return null;
        }
    }

    @Override
    protected void onPostExecute(List<Uri> result) {
        if (result != null && listener != null)
            listener.onDone(result);
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        if (listener != null)
            listener.onProgress(values[0], pageCount);
    }
}
