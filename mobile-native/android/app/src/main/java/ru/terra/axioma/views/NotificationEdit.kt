package ru.terra.axioma.views

import android.content.Context

import ru.terra.axioma.R


class NotificationEdit(context: Context) : NotificationCard(context) {

    override val iconRes: Int
        get() = R.drawable.ic_content_cut

    override val colorRes: Int
        get() = R.color.red

    override val titleRes: Int
        get() = R.string.card_edit
}
