package ru.terra.axioma.documentupdater

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.list.customListAdapter
import com.afollestad.materialdialogs.list.getRecyclerView
import com.afollestad.materialdialogs.list.listItems
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf
import pub.devrel.easypermissions.AppSettingsDialog
import pub.devrel.easypermissions.EasyPermissions
import pub.devrel.easypermissions.PermissionRequest
import ru.terra.axioma.App
import ru.terra.axioma.Constants
import ru.terra.axioma.ImageViewerActivity
import ru.terra.axioma.R
import ru.terra.axioma.adapters.DocumentLinkerAdapter
import ru.terra.axioma.adapters.PagesAdapter
import ru.terra.axioma.databinding.ActivityDocumentUpdaterBinding
import ru.terra.axioma.models.Document
import ru.terra.axioma.views.CustomDialog
import ru.terra.axioma.views.EmptyDividerItemDecorator
import ru.terra.axioma.views.GridSpacingItemDecorator

class DocumentUpdaterActivity : AppCompatActivity(), DocumentLinkerAdapter.OnClickListener, EasyPermissions.PermissionCallbacks, DocumentUpdaterNavigator.View {

    private lateinit var binding: ActivityDocumentUpdaterBinding
    private lateinit var actions: DocumentUpdaterNavigator.Action
    private lateinit var viewModel: DocumentUpdaterViewModel

    private var progressDialog: MaterialDialog? = null
    private lateinit var parent: Document

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_document_updater)

        parent = intent.getParcelableExtra(DOCUMENT)
        val vm: DocumentUpdaterViewModel by inject { parametersOf(parent, this) }
        viewModel = vm

        setupActionBar()
        setupList()

        actions = viewModel

        binding.viewModel = viewModel
    }

    private fun setupActionBar() {
        binding.toolbar.setTitleTextAppearance(this, R.style.ToolbarTitle)
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        binding.toolbar.contentInsetStartWithNavigation = 0
        binding.toolbar.setNavigationOnClickListener { view -> onBackPressed() }
    }

    private fun setupList() {
        val adapter = DocumentLinkerAdapter()
        adapter.setOnClickListener(this)
        binding.recyclerView.adapter = adapter
        binding.recyclerView.addItemDecoration(EmptyDividerItemDecorator(this))

        viewModel.pagesLiveData.observe(this, Observer<List<LinkerItem>> { adapter.setData(it) })
    }

    private fun showFileDialog() {
        val items = listOf("Изображение", "PDF")

        MaterialDialog(this)
                .title(text = "Выберите тип файла")
                .listItems(items = items) { _, index, _ ->
                    if (index == 0)
                        galleryIntent()
                    else
                        pdfIntent()
                }.show()
    }

    private fun galleryIntent() {
        val intent = Intent()
        intent.type = "*/*"
        intent.action = Intent.ACTION_OPEN_DOCUMENT
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true)
        val mimeTypes = arrayOf("image/*")
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)
        startActivityForResult(intent/*Intent.createChooser(intent, getString(R.string.common_select_file))*/, Constants.GALLERY_REQUEST)
    }

    private fun pdfIntent() {
        val intent = Intent()
        intent.type = "*/*"
        intent.action = Intent.ACTION_OPEN_DOCUMENT
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true)
        val mimeTypes = arrayOf("application/pdf")
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)
        startActivityForResult(intent/*Intent.createChooser(intent, getString(R.string.common_select_file))*/, Constants.GALLERY_REQUEST)
    }

    override fun onPermissionsGranted(requestCode: Int, perms: List<String>) {
        showFileDialog()
    }

    override fun onPermissionsDenied(requestCode: Int, perms: List<String>) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            AppSettingsDialog.Builder(this).build().show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            actions.renderPage(data?.data!!)
        }
    }

    override fun close() {
        setResult(Activity.RESULT_OK)
        finishActivity(Constants.UPDATE_DOCUMENT_REQUEST)
        finish()
    }

    override fun addFile() {
        val storagePerm = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE)
        if (EasyPermissions.hasPermissions(this, *storagePerm)) {
            showFileDialog()
        } else {
            EasyPermissions.requestPermissions(
                    PermissionRequest.Builder(this, Constants.GALLERY_REQUEST, *storagePerm)
                            .setRationale(R.string.card_message)
                            .setPositiveButtonText(R.string.editor_ok)
                            .setNegativeButtonText(R.string.common_cancel)
                            .build())
        }
    }

    override fun onOldPageClick(newPagePosition: Int) {
        val adapter = PagesAdapter(parent, parent.allPages)

        val oldPageDialog = MaterialDialog(this)
                .title(text = "Select page")
                .customListAdapter(adapter)
                .negativeButton(R.string.common_cancel)

        adapter.setClickListener { file, _ ->
            actions.addLinkToPage(newPagePosition, file)
            oldPageDialog.dismiss()
        }

        val list = oldPageDialog.getRecyclerView()
        list.layoutManager = GridLayoutManager(this, 2)
        list.addItemDecoration(GridSpacingItemDecorator(2, 50, true))

        oldPageDialog.show()
    }

    override fun onNewPageClick(uri: Uri) {
        val intent = Intent(this, ImageViewerActivity::class.java)
        intent.putExtra(ImageViewerActivity.URI, uri)
        startActivity(intent)
    }

    override fun onRemovePageClick(position: Int) {
//        viewModel.removePage(position) // TODO
    }

    override fun showRenderPdfProgress(max: Int, showMinMax: Boolean) {
        CustomDialog.getLoadingDialog(this).show()

        //TODO("Диалог с прогрессом")
        /*runOnUiThread {
            progressDialog = CustomDialog.get(this@DocumentUpdaterActivity)
                    .message(R.string.please_wait)
                    .progress(false, max, showMinMax)
                    .canceledOnTouchOutside(false)
                    .positiveText(R.string.editor_ok)
                    .negativeText(R.string.common_cancel)
                    .onNegative({ dialog, which ->
                        actions.cancelPdfRenderTask()
                        dialog.cancel()
                    })
                    .show()

            progressDialog!!.getActionButton(DialogAction.POSITIVE).setVisibility(View.GONE)
        }*/
    }

    override fun updateProgress(value: Int) {
        //TODO("Диалог с прогрессом")
        /*runOnUiThread {
            if (progressDialog == null || progressDialog!!.isCancelled())
                return@runOnUiThread

            progressDialog!!.setProgress(value)

            if (value == progressDialog!!.getMaxProgress()) {
                progressDialog!!.dismiss()
                //                    progressDialog.setContent(getString(R.string.done));
                //                    progressDialog.getActionButton(DialogAction.NEGATIVE).setVisibility(View.GONE);
                //                    progressDialog.getActionButton(DialogAction.POSITIVE).setVisibility(View.VISIBLE);
            }
        }*/
    }

    override fun showError(error: Int) {
        dismissProgress()
        CustomDialog.error(this, error).show()
    }

    override fun showError(error: String) {
        dismissProgress()
        CustomDialog.error(this, error).show()
    }

    override fun showLoadingToServerProgress() {
        TODO("Переделать")
//        progressDialog = MaterialDialog.Builder(this)
//                .content(R.string.common_saving)
//                .canceledOnTouchOutside(false)
//                .cancelable(false)
//                .progress(true, 0)
//                .show()
    }

    override fun dismissProgress() {
        if (progressDialog != null)
            progressDialog!!.dismiss()
    }

    companion object {

        val DOCUMENT = "document"
    }
}
