package ru.terra.axioma.login

import android.app.Application
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.google.firebase.FirebaseException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import nl.komponents.kovenant.*
import nl.komponents.kovenant.combine.and
import okhttp3.FormBody
import ru.terra.axioma.App
import ru.terra.axioma.LoginException
import ru.terra.axioma.client.ClientApi
import ru.terra.axioma.models.AuthUser
import ru.terra.axioma.models.User
import ru.terra.axioma.repository.ProjectsRepository
import ru.terra.axioma.repository.UsersRepository
import ru.terra.axioma.store.CurrentSessionStore
import ru.terra.axioma.store.UserStore
import timber.log.Timber

class LoginViewModel internal constructor(
        application: Application,
        private val client: ClientApi,
        private val currentSessionStore: CurrentSessionStore
) : AndroidViewModel(application) {

    val showLoading = MutableLiveData<Boolean>()
    val showError = MutableLiveData<String?>()
    val showNextScreen = MutableLiveData<Boolean>()
    val showRegistrationScreen = MutableLiveData<Boolean>()
    val showActivateScreen = MutableLiveData<Boolean>()
    val code = MutableLiveData<String>()
    private var verificationId = ""

    private var app: Application = application

    init {
        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener {
            currentSessionStore.regId = it.token
            checkSession()
        }
    }

    private fun checkSession() {
        currentSessionStore.token = restoreToken()

        if (currentSessionStore.token.isNotBlank()) {
            showLoading.postValue(true)

            task {
                client.checkSession().execute()
            }.then {
                it.body()?.let { res ->
                    val user = res.get("user") ?: throw Exception("Token expired")

                    UserStore.user = Gson().fromJson(user, User::class.java)
                    currentSessionStore.user = Gson().fromJson(user, User::class.java)
                    if (res.get("pong").asBoolean)
                        loadData()
                }
            }.fail {
                Timber.e(it)
                showLoading.postValue(false)
            }
        } else {
            showLoading.postValue(false)
        }
    }

    // Старый логин по паролю
    fun login(username: String, password: String) {
        if (password.isEmpty())
            return

        showLoading.postValue(true)
        val requestBody = FormBody.Builder()
                .add("username", username)
                .add("password", password)
                .build()

        task {
            client.authenticate(requestBody).execute()
        }.then {
            it.body()?.let { json ->
                if (json.get("success")?.asBoolean == false) {
                    throw LoginException(json.get("error")?.asJsonObject?.get("status")?.asInt)
                } else {
                    Gson().fromJson(json, AuthUser::class.java)?.let { user ->
                        UserStore.user = user.user

                        currentSessionStore.token = user.token
                        currentSessionStore.user = user.user

                        saveToken()
                    } ?: run {
                        throw LoginException(2)
                    }
                }
            }
        }.success {
            loadData()
        }.fail {
            Timber.d(it)
            showError.postValue(it.toString())
            showLoading.postValue(false)
        }
    }

    fun signUp(phone: String): Promise<Unit?, Exception> {
        return task {
            client.signup(phone, currentSessionStore.regId).execute()
        }.then {
            it.body()?.let { v ->
                UserStore.user = v
                currentSessionStore.user = v
            }
        }.fail {
            Timber.d(it)
        }
    }

    fun activate(code: String) {
        if (code.isBlank()) {
            showError.postValue("Введите код активации")
            return
        }

        showLoading.postValue(true)

        val credential = PhoneAuthProvider.getCredential(verificationId, code)
        FirebaseAuth.getInstance().signInWithCredential(credential)
                .addOnCompleteListener {
                    if (it.isSuccessful) {
                        task {
                            client.activate(currentSessionStore.user.phoneNumber, currentSessionStore.user.activationCode).execute()
                        }.then { response ->
                            response.body()?.let { v ->
                                currentSessionStore.token = v.token
                                saveToken()
                                loadData()
                            }
                        }.fail { e ->
                            Timber.d(e)
                            showLoading.postValue(false)
                            showError.postValue("Ошибка активации")
                        }
                    } else {
                        Timber.w("signInWithCredential:failure")
                        showLoading.postValue(false)
                        showError.postValue("signInWithCredential failure")
                    }
                }
                .addOnFailureListener {
                    Timber.e(it)
                    showLoading.postValue(false)
                    showError.postValue(it.message)
                }
    }

    private fun loadData() {
        if (!currentSessionStore.isActive)
            showRegistrationScreen.postValue(true)
        else
            showNextScreen.postValue(true)
    }

    private fun saveToken() {
        val editor = app.getSharedPreferences(App.PREFS_NAME, Context.MODE_PRIVATE).edit()
        editor.putString("token", Gson().toJson(currentSessionStore.token))
        editor.apply()
    }

    private fun saveUser() {
        val editor = app.getSharedPreferences(App.PREFS_NAME, Context.MODE_PRIVATE).edit()
        editor.putString("user", Gson().toJson(UserStore.user))
        editor.apply()
    }

    private fun restoreToken(): String {
        val prefs = app.getSharedPreferences(App.PREFS_NAME, Context.MODE_PRIVATE)
        return (prefs.getString("token", "") ?: "").trim('\"')
    }

    private fun restoreUser(): User? {
        val prefs = app.getSharedPreferences(App.PREFS_NAME, Context.MODE_PRIVATE)
        val value = prefs.getString("user", "") ?: ""
        return try {
            Gson().fromJson<User>(value, User::class.java)
        } catch (e: JsonSyntaxException) {
            null
        }
    }

    fun getBroadcastReceiver(): BroadcastReceiver {
        return object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                code.value = intent.getStringExtra(LoginActivity.ACTIVATION_CODE)
            }
        }
    }

    val verificationCallback = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        override fun onVerificationCompleted(p0: PhoneAuthCredential?) {
            showActivateScreen.value = true
        }

        override fun onVerificationFailed(p0: FirebaseException?) {
            Timber.e(p0)
            showError.postValue(p0!!.message)
        }

        override fun onCodeSent(p0: String?, p1: PhoneAuthProvider.ForceResendingToken?) {
            verificationId = p0 ?: ""
        }

        override fun onCodeAutoRetrievalTimeOut(p0: String?) {
            Timber.e(p0)
            super.onCodeAutoRetrievalTimeOut(p0)
        }
    }
}
