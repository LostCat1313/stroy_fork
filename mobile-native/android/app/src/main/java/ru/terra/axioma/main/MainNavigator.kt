package ru.terra.axioma.main

import ru.terra.axioma.models.Document
import ru.terra.axioma.models.Notification

interface MainNavigator {
    fun openDocument(documents: List<Document>, docId: Int)
    fun openDocument(documents: List<Document>, notification: Notification)
}