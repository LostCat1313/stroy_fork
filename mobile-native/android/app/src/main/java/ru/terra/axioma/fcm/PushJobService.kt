package ru.terra.axioma.fcm

import com.firebase.jobdispatcher.JobParameters
import com.firebase.jobdispatcher.JobService

import timber.log.Timber

class PushJobService : JobService() {

    override fun onStartJob(jobParameters: JobParameters): Boolean {
        Timber.tag(TAG).d("Performing long running task in scheduled job")
        // TODO(developer): add long running task here.


        return false
    }

    override fun onStopJob(jobParameters: JobParameters): Boolean {
        return false
    }

    companion object {

        private val TAG = "PushJobService"
    }

}