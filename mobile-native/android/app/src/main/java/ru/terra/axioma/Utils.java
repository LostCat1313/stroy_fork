package ru.terra.axioma;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import com.google.gson.Gson;
import com.google.gson.JsonElement;

import java.util.Date;

/**
 * Created by alexei on 14.09.16.
 */

public class Utils {
    public static void addPadding(View view) {
        if (view != null) {
            view.setPadding(
                    view.getPaddingLeft(),
                    view.getPaddingTop() + view.getResources().getDimensionPixelSize(R.dimen.status_bar_height),
                    view.getPaddingRight(),
                    view.getPaddingBottom()
            );
        }
    }

    public static void addMargin(View view) {
        if (view != null)
            ((ViewGroup.MarginLayoutParams) view.getLayoutParams()).topMargin += view.getResources().getDimensionPixelSize(R.dimen.status_bar_height);
    }

    public static void hideKeyboard(Activity activity) {
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            assert imm != null;
            imm.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
        }
    }

    public static void showSoftInput(Context context, boolean forced) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.toggleSoftInput(forced ? 2 : 0, 0);
        }
    }

    public static void showSoftInput(Context context) {
        showSoftInput(context, false);
    }

    public static float dpToPx(Context context, float valueInDp) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, valueInDp, metrics);
    }

    public static Date jsonToDate(JsonElement json) {
        return new Gson().fromJson(json, Date.class);
    }
}
