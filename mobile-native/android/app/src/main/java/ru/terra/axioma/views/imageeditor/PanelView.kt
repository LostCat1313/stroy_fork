package ru.terra.axioma.views.imageeditor

import android.content.Context
import android.view.Gravity
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageButton
import android.widget.LinearLayout
import android.widget.SeekBar
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.color.colorChooser
import kotlinx.android.synthetic.main.panel_view.view.*
import ru.terra.axioma.DEFAULT_COLOR
import ru.terra.axioma.PRIMARY_COLORS
import ru.terra.axioma.R
import timber.log.Timber

internal class PanelView(context: Context, mode: Int = 0) : LinearLayout(context) {
    private var selectedColor = DEFAULT_COLOR
    private var panelGravity = Gravity.BOTTOM

    var onSizeChanged: ((Int) -> Unit) = { Timber.d("onSizeChanged did not set!!!") }
    var onColorChanged: ((Int) -> Unit) = { Timber.d("onColorChanged did not set!!!") }

    val sizeValue: Int
        get() { return sizeControl?.progress ?: 10 }

    init {
        View.inflate(context, R.layout.panel_view, this)

        orientation = VERTICAL

        val lp = FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        lp.gravity = panelGravity
        layoutParams = lp
        setBackgroundResource(R.color.grey_600)

        // Если линия или прямоугольник
        if (mode == 1) {
            size.setImageResource(R.drawable.format_line_weight)
        }


        sizeControl.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                onSizeChanged(progress)
                sizeText.text = progress.toString()
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {

            }
        })

        drag.setOnClickListener { v ->
            val lp1 = layoutParams as FrameLayout.LayoutParams
            panelGravity = if (lp1.gravity == Gravity.BOTTOM) Gravity.TOP else Gravity.BOTTOM
            lp1.gravity = panelGravity
            layoutParams = lp1

            setupSizeControlPosition()

            (v as ImageButton).setImageResource(if (panelGravity == Gravity.BOTTOM) R.drawable.chevron_up else R.drawable.chevron_down)
        }

        setColorButtonColor()
        color.setOnClickListener {
            MaterialDialog(context)
                    .title(R.string.editor_text)
                    .colorChooser(PRIMARY_COLORS, initialSelection = selectedColor, waitForPositiveButton = false) { d, color->
                        selectedColor = color
                        onColorChanged(selectedColor)
                        setColorButtonColor()
                        d.dismiss()
                    }
                    .show()
        }

        setupSizeControlPosition()

        size!!.setOnClickListener { v ->
            val opened = v.isSelected
            v.isSelected = !opened
            toggleSizeControl(!opened)
        }
    }

    private fun setColorButtonColor() {
        val mDrawable = color.drawable
        mDrawable.setTint(selectedColor)
    }

    fun setOnDoneClick(onClick: OnClickListener) {
        findViewById<View>(R.id.done).setOnClickListener(onClick)
    }

    fun setOnDeleteClick(onClick: OnClickListener) {
        findViewById<View>(R.id.delete).setOnClickListener(onClick)
    }

    private fun toggleSizeControl(show: Boolean) {
        sizeControl.visibility = if (show) View.VISIBLE else View.GONE
    }

    private fun setupSizeControlPosition() {
        removeView(sizeControl)

        if (panelGravity == Gravity.BOTTOM) {
            addView(sizeControl, 0)
        } else {
            addView(sizeControl, 1)
        }
    }
}
