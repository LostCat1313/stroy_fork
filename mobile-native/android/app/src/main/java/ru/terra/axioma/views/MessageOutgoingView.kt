package ru.terra.axioma.views

import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.ViewGroup
import com.facebook.drawee.view.SimpleDraweeView
import kotlinx.android.synthetic.main.message_outgoing_view.view.*
import ru.terra.axioma.Configuration
import ru.terra.axioma.R
import ru.terra.axioma.models.ProjectFile
import ru.terra.axioma.models.chat.Message

class MessageOutgoingView(context: Context) : MessageBaseView(context) {

    init {
        LayoutInflater.from(context).inflate(R.layout.message_outgoing_view, this, true)
    }

    override fun setMessage(msg: Message, onAttachmentClick: (ProjectFile) -> Unit) {
        message.text = msg.content
        time.text = msg.messageTime

        msg.attachments?.let {
            if (it.isEmpty())
                attachments.removeAllViews()

            msg.attachments?.forEach {
                attachments.addView(getAttachmentView(it.file, onAttachmentClick))
            }
        } ?: run {
            attachments.removeAllViews()
        }
    }
}