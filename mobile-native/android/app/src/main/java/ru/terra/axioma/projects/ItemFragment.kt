package ru.terra.axioma.projects

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.FileProvider
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.list.customListAdapter
import kotlinx.android.synthetic.main.fragment_projects_item.*
import nl.komponents.kovenant.ui.successUi
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.sharedViewModel
import pub.devrel.easypermissions.AppSettingsDialog
import pub.devrel.easypermissions.EasyPermissions
import pub.devrel.easypermissions.PermissionRequest
import ru.terra.axioma.*

import ru.terra.axioma.models.*
import ru.terra.axioma.repository.UsersRepository
import ru.terra.axioma.store.CurrentSessionStore
import ru.terra.axioma.store.UserStore
import ru.terra.axioma.views.CustomDialog
import java.io.File
import java.io.IOException


class ItemFragment : Fragment(), EasyPermissions.PermissionCallbacks {

    private val vm: ProjectsViewModel by sharedViewModel()
    private val currentSessionStore: CurrentSessionStore by inject()

    private lateinit var progressDialog : MaterialDialog
    private lateinit var errorDialog : MaterialDialog
    private var currentImageUri = MutableLiveData<Uri>()
    private var enableSave = MutableLiveData<Boolean>()
    private var tempUri = Uri.EMPTY

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_projects_item, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        vm.loading.value = false
        vm.error.value = null

        setupActionBar()
        setupButtonBar()
        setupUI()

        vm.error.observe(this, Observer {
            it?.let { s ->
                progressDialog.dismiss()
                errorDialog.message(text = s).show()
            }
        })

        vm.loading.observe(this, Observer {
            if (it)
                progressDialog.show()
            else
                progressDialog.dismiss()
        })
    }

    override fun onPermissionsGranted(requestCode: Int, perms: List<String>) {
        addCover()
    }

    override fun onPermissionsDenied(requestCode: Int, perms: List<String>) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            AppSettingsDialog.Builder(this).build().show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == Constants.GALLERY_REQUEST) {
                val path = FileUtils.getPath(context!!, data?.data)
                val f = File(path)
                currentImageUri.value = Uri.fromFile(f)
            } else if (requestCode == Constants.CAMERA_REQUEST) {
                currentImageUri.value = tempUri
            }
        }
    }

    private fun setupActionBar() {
        toolbar.setNavigationOnClickListener { vm.selectedProject.value = null }
        toolbar.contentInsetStartWithNavigation = 0
    }

    private fun setupButtonBar() {
        ok.setOnClickListener { vm.save(name.text.toString(), currentImageUri.value) }
        cancel.setOnClickListener { vm.selectedProject.value = null }
    }

    private fun setupUI() {
        progressDialog = CustomDialog.getLoadingDialog(context!!)
        errorDialog = MaterialDialog(context!!).title(R.string.common_error)

        enableSave.observe(this, Observer {
            ok.isEnabled = it
        })

        currentImageUri.observe(this, Observer {
            enableSave.value = it.isNotNull() && name.text?.isNotEmpty()!!
            cover.apply {
                setImageURI(it, this)
                background = null
            }
        })

        cover.setOnClickListener {
            addCover()
        }

        name.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                enableSave.value = /*capturedImageUri.isNotNull() &&*/ count > 0
            }

            override fun afterTextChanged(s: Editable?) {

            }

        })

        vm.project.run {
            if (id == 0) {
                toolbar.title = getString(R.string.common_project_new)
                createdBy = currentSessionStore.user.id
                users.put(createdBy, UserRights.ADMIN)
            } else {
                toolbar.title = title
                name.setText(title)
                currentImageUri.value = Uri.parse(Configuration.baseUrl + image?.url)
            }

            participants.apply {
                text = getString(R.string.project_users, users.size())
                setOnClickListener {
                    val usersRepository: UsersRepository by inject()
                    usersRepository.fetch().successUi {
                        showSelectUsersDialog(it)
                    }
                }
            }
        }

    }

    private fun addCover() {
        CustomDialog.getImageSelectorDialog(context!!, object : CustomDialog.OnImageDialogSelect {
            override fun onNoPermission(requestCode: Int, perm: Array<String>) {
                EasyPermissions.requestPermissions(
                        PermissionRequest.Builder(this@ItemFragment, requestCode, *perm)
                                .setRationale(R.string.card_message)
                                .setPositiveButtonText(R.string.editor_ok)
                                .setNegativeButtonText(R.string.common_cancel)
                                .build())
            }

            override fun onCamera() {
                cameraIntent()
            }

            override fun onGallery() {
                galleryIntent()
            }
        }).show()
    }

    private fun cameraIntent() {
        context?.run {
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)

            if (intent.resolveActivity(packageManager) != null) {
                var photoFile: File? = null
                try {
                    photoFile = FileUtils.createTempImageFile(this, false)
                } catch (ex: IOException) {
                    // Error occurred while creating the ProjectFile
                }

                photoFile?.let {
                    tempUri = Uri.fromFile(photoFile)

                    val photoURI = FileProvider.getUriForFile(this, "ru.terra.axioma.fileprovider", it)
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    startActivityForResult(intent, Constants.CAMERA_REQUEST)
                }
            }
        }
    }

    private fun galleryIntent() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, getString(R.string.common_select_file)), Constants.GALLERY_REQUEST)
    }

    private fun showSelectUsersDialog(users: ArrayList<User>?) {
        users ?: return

        val projectUsers = vm.project.users.clone()

        val adapter = UsersAdapter(users, projectUsers, vm.project.createdBy)
        adapter.setOnClickListener(object : UsersAdapter.OnClickListener {
            override fun onButtonClick(id: Int, position: Int) {
                val curValue = projectUsers[id]
                val rightsAdapter = RightsAdapter(curValue)

                MaterialDialog(context!!)
                        .negativeButton(R.string.common_cancel)
                        .positiveButton(R.string.editor_ok) {
                            projectUsers.put(id, rightsAdapter.selectedValue)
                            adapter.notifyItemChanged(position)
                        }
                        .cancelOnTouchOutside(false)
                        .cancelable(false)
                        .show { customListAdapter(rightsAdapter) }
            }
        })

        MaterialDialog(context!!)
                .title(R.string.chat_choose_users)
                .negativeButton(R.string.common_cancel)
                .positiveButton(R.string.editor_ok) {
                    vm.project.users = projectUsers
                    participants.text = getString(R.string.project_users, vm.project.users.size())
                }
                .cancelOnTouchOutside(false)
                .cancelable(false)
                .show { customListAdapter(adapter) }
    }
}
