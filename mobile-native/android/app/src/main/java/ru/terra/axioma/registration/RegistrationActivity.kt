package ru.terra.axioma.registration

import android.content.Intent
import android.os.Bundle
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.activity_registration.*
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import ru.terra.axioma.R
import ru.terra.axioma.main.MainActivity
import timber.log.Timber


class RegistrationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)

        val viewModel: RegistrationViewModel by viewModel()


        viewModel.showNextScreen.observe(this, Observer {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        })


        setupOrganizationField()

        viewModel.organizations.observe(this@RegistrationActivity, Observer {
            Timber.d("%s", it)

            val strings = MutableList(it.size) { i -> it[i].name }
            ArrayAdapter<String>(this@RegistrationActivity, android.R.layout.simple_list_item_1, strings).also { adapter ->
                organization.setAdapter(adapter)
            }
        })

        viewModel.positions.observe(this@RegistrationActivity, Observer {
            Timber.d("%s", it)

            val strings = MutableList(it.size) { i -> it[i].name }
            ArrayAdapter<String>(this@RegistrationActivity, android.R.layout.simple_list_item_1, strings).also { adapter ->
                position.setAdapter(adapter)
            }
        })


        register.setOnClickListener {
            if (validate()) {
                viewModel.register(
                        name = login.text.toString(),
                        password = password.text.toString(),
                        firstName = firstName.text.toString(),
                        secondName = secondName.text.toString(),
                        thirdName = thirdName.text.toString(),
                        email = email.text.toString(),
                        organizationName = organization.text.toString(),
                        positionName = position.text.toString()
                )
            }
        }

    }

    private fun validate(): Boolean {
        if (!(firstName.text.isNullOrBlank()
                        || secondName.text.isNullOrBlank()
                        || login.text.isNullOrBlank()
                        || password.text.isNullOrBlank())) {

            if (password.text.toString().length < 6) {
                password.error = getString(R.string.registration_error_password_too_short)
                return false
            }

            if (password.text.toString() != passwordRepeat.text.toString()) {
                passwordRepeat.error = getString(R.string.registration_error_passwords_repeat)
                return false
            }

            return true
        }

        if (login.text.isNullOrBlank())
            loginLayout.error = getString(R.string.registration_error_empty)

        if (password.text.isNullOrBlank())
            passwordLayout.error = getString(R.string.registration_error_empty)

        if (firstName.text.isNullOrBlank())
            firstNameLayout.error = getString(R.string.registration_error_empty)

        if (secondName.text.isNullOrBlank())
            secondNameLayout.error = getString(R.string.registration_error_empty)

        return false
    }

    private fun setupOrganizationField() {
        organizationLayout.setEndIconOnClickListener {
            organization.showDropDown()
        }
    }
}
