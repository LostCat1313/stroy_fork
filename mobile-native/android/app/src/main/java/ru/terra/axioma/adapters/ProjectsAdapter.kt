package ru.terra.axioma.adapters

import android.view.View
import android.view.ViewGroup

import java.util.ArrayList
import androidx.recyclerview.widget.RecyclerView
import ru.terra.axioma.models.Project
import ru.terra.axioma.store.UserStore
import ru.terra.axioma.views.ProjectCard

class ProjectsAdapter(private var clickListener: OnClickListener?) : RecyclerView.Adapter<ProjectsAdapter.ViewHolder>() {
    private var objects: List<Project>? = null

    init {
        objects = ArrayList()
    }

    inner class ViewHolder internal constructor(view: View) : RecyclerView.ViewHolder(view) {
        var view: ProjectCard = view as ProjectCard
    }

    override fun onCreateViewHolder(parent: ViewGroup, type: Int): ViewHolder {
        return ViewHolder(ProjectCard(parent.context))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = objects!![position]
        holder.view.setData(data, data.createdBy == UserStore.user.id)
        if (position == 0)
            holder.view.setMargin()

        holder.view.setOnButtonClickListeners(
                View.OnClickListener {
                    if (clickListener != null)
                        clickListener!!.onSelect(data)
                },
                View.OnClickListener {
                    if (clickListener != null)
                        clickListener!!.onEdit(data)
                }
        )
    }

    override fun getItemCount(): Int {
        return objects!!.size
    }

    fun setData(data: List<Project>) {
        this.objects = data
        notifyDataSetChanged()
    }

    fun setOnClickListener(clickListener: OnClickListener) {
        this.clickListener = clickListener
    }

    interface OnClickListener {
        fun onSelect(project: Project)
        fun onEdit(project: Project)
    }
}
