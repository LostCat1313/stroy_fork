package ru.terra.axioma.projects

import android.util.SparseIntArray
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ru.terra.axioma.R
import ru.terra.axioma.models.User
import ru.terra.axioma.models.UserRights
import ru.terra.axioma.store.UserStore


internal class UsersAdapter(var items: ArrayList<User>, var selected: SparseIntArray, val creatorId: Int) : RecyclerView.Adapter<UsersAdapter.ViewHolder>() {

    private var onClickListener: OnClickListener? = null

    fun setOnClickListener(onClickListener: OnClickListener) {
        this.onClickListener = onClickListener
    }

    internal inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var text: TextView = view.findViewById(R.id.text)
        var checkBox: CheckBox = view.findViewById(R.id.checkBox)
        var button: Button = view.findViewById(R.id.button)

        init {
            checkBox.setOnCheckedChangeListener { _, isChecked ->
                val item = items[adapterPosition]
                val right = selected[item.id]

                if (isChecked && right == 0)
                    selected.put(item.id, UserRights.READ)
                else if (!isChecked)
                    selected.delete(item.id)

                view.post {
                    notifyItemChanged(adapterPosition)
                }
            }

            button.setOnClickListener {
                val item = items[adapterPosition]
                onClickListener?.onButtonClick(item.id, adapterPosition)
            }
        }

        fun disable() {
            text.isEnabled = false
            checkBox.isEnabled = false
            button.isEnabled = false
        }

        fun enabled() {
            text.isEnabled = true
            checkBox.isEnabled = true
            button.isEnabled = true
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, type: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.user_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]

        val isCurrentUser = item.id == UserStore.user.id
        val isCurrentUserOwner = item.id == creatorId && creatorId == UserStore.user.id

        if (isCurrentUser)
            holder.text.text = item.fullName + " (Вы)"
        else if (isCurrentUserOwner)
            holder.text.text = item.fullName + " (Создатель)"
        else
            holder.text.text = item.fullName

        val right = selected[item.id]
        if (right > 0) {
            if (!isCurrentUser && !isCurrentUserOwner) {
                holder.button.visibility = View.VISIBLE
                holder.button.text = UserRights.getString(right)
                holder.enabled()
            } else {
                holder.disable()
            }

            holder.checkBox.isChecked = true
        } else {
            holder.button.visibility = View.GONE
            holder.checkBox.isChecked = false
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    internal interface OnClickListener {
        fun onButtonClick(id: Int, position: Int)
    }
}
