package ru.terra.axioma.documentstructure

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import nl.komponents.kovenant.then
import ru.terra.axioma.R
import ru.terra.axioma.client.ClientApi
import ru.terra.axioma.models.Document
import ru.terra.axioma.repository.DocumentsRepository
import ru.terra.axioma.store.CurrentSessionStore

const val DEFAULT_MODE = -1
const val CREATE_FOLDER_MODE = 0
const val CREATE_DOCUMENT_MODE = 1
const val UPDATE_DOCUMENT_MODE = 2

class DocumentListViewModel(
        application: Application,
        private val client: ClientApi,
        private val documentsRepository: DocumentsRepository,
        private val currentSessionStore: CurrentSessionStore
) : AndroidViewModel(application) {

    val documents = MutableLiveData<ArrayList<Document>>()
    val selectionMode = MutableLiveData<Int>()

    val rootDoc: Document = Document(getApplication<Application>().getString(R.string.common_root_folder), null).apply {
        id = -1
        parentId = null
        projectId = currentSessionStore.project?.id ?: 0
        isFolder = true
    }

    init {
        selectionMode.value = DEFAULT_MODE
        update()
    }

    fun update() {
        documentsRepository.fetch() then {
            documents.postValue(it)
            setMode(DEFAULT_MODE)
        }
    }

    fun rename(doc: Document, name: String) {
        doc.title = name
        documentsRepository.update(doc) then {
            update()
        }
    }

    fun createFolder(parent: Document, name: String) {
        documentsRepository.createFolder(parent, name) {
            selectionMode.postValue(DEFAULT_MODE)
            update()
        }
    }

    fun delete(doc: Document) {
        documentsRepository.delete(doc) then {
            update()
        }
    }

    fun setMode(mode: Int) {
        selectionMode.postValue(mode)
    }

}