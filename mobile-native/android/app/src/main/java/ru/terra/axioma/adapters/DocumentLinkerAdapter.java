package ru.terra.axioma.adapters;

import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.common.base.Predicate;
import com.google.common.collect.Lists;

import java.util.List;

import ru.terra.axioma.Configuration;
import ru.terra.axioma.ListUtils;
import ru.terra.axioma.R;
import ru.terra.axioma.documentupdater.LinkerItem;
import ru.terra.axioma.models.ProjectFile;

public class DocumentLinkerAdapter extends RecyclerView.Adapter<DocumentLinkerAdapter.ViewHolder> {
    private List<LinkerItem> objects;
    private OnClickListener onClickListener;

    public DocumentLinkerAdapter() {
        objects = Lists.newArrayList();
    }

    public void setOnClickListener(OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public void setData(List<LinkerItem> data) {
        if (objects.isEmpty()) {
            objects.addAll(data);
            notifyDataSetChanged();
        } else {
            if (data.size() < objects.size()) {
                for (int i = 0; i < objects.size(); i++) {
                    LinkerItem item = objects.get(i);
                    Predicate<LinkerItem> predicate = input -> input.getId() == item.getId();
                    int pos = ListUtils.find(data, predicate);
                    if (pos != -1) {
                        objects.set(pos, data.get(pos));
                        notifyItemChanged(i);
                    } else {
                        objects.remove(item);
                        i--;
                        notifyItemRemoved(i);
                    }
                }
            } else {
                for (int i = 0; i < data.size(); i++) {
                    LinkerItem item = data.get(i);
                    Predicate<LinkerItem> predicate = input -> input.getId() == item.getId();
                    int pos = ListUtils.find(objects, predicate);
                    if (pos != -1)
                        objects.set(pos, item);
                    else {
                        objects.add(item);
                    }
                }
                notifyDataSetChanged();
            }
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView page;
        SimpleDraweeView oldPage, newPage;
        ImageButton remove;
        View underline;

        ViewHolder(View view) {
            super(view);
            this.page = view.findViewById(R.id.page);
            this.newPage = view.findViewById(R.id.new_page);
            this.oldPage = view.findViewById(R.id.old_page);
            this.remove = view.findViewById(R.id.remove);
            this.underline = view.findViewById(R.id.underline);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.document_linker_item, parent, false);
        return new DocumentLinkerAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        LinkerItem item = objects.get(position);

        if (item.getOldPage() != null) {
            holder.page.setVisibility(View.VISIBLE);
            holder.underline.setVisibility(View.VISIBLE);
            holder.page.setText(String.valueOf(item.getOldPage().pageNumber));
        } else {
            holder.page.setVisibility(View.INVISIBLE);
            holder.underline.setVisibility(View.INVISIBLE);
        }

        holder.newPage.setImageURI(item.getNewPageUri());

        ProjectFile file = item.getOldPage();
        if (file == null) {
            holder.oldPage.setImageURI("");
        } else {
            holder.oldPage.setImageURI(Configuration.baseUrl + file.thumbUrl);
        }

        holder.newPage.setOnClickListener(v -> onClickListener.onNewPageClick(item.getNewPageUri()));
        holder.oldPage.setOnClickListener(v -> onClickListener.onOldPageClick(holder.getAdapterPosition()));
        holder.remove.setOnClickListener(v -> {
            int pos = objects.indexOf(item);
            onClickListener.onRemovePageClick(pos);
        });
    }

    @Override
    public int getItemCount() {
        return objects.size();
    }

    /*private View getFirstListView(Context context) {
        TextView title = new TextView(context);
        title.setText("Add text here");
        int padding = context.getResources().getDimensionPixelSize(R.dimen.base_margin);
        title.setPadding(padding, padding, padding, padding);
        return title;
    }*/

    public interface OnClickListener {
        void onOldPageClick(int position);

        void onNewPageClick(Uri uri);

        void onRemovePageClick(int position);
    }
}
