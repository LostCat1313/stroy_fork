package ru.terra.axioma.repository

import nl.komponents.kovenant.Promise
import nl.komponents.kovenant.task
import nl.komponents.kovenant.then
import ru.terra.axioma.StringUtils
import ru.terra.axioma.client.ClientApi
import ru.terra.axioma.models.User
import timber.log.Timber
import java.util.*
import kotlin.collections.ArrayList

class UsersRepository (private val client: ClientApi) {
    private var users: ArrayList<User> = ArrayList()

    val hash: HashMap<String, User>
        get() {
            val hash = HashMap<String, User>()
            for (u in users) {
                u.phoneNumber = StringUtils.formatPhoneNumber(u.phoneNumber)
                hash[u.phoneNumber] = u
            }
            return hash
        }

    fun getUsers(): ArrayList<User> {
        return users
    }

    fun fetch(): Promise<ArrayList<User>, Exception> {
        users = ArrayList()
        return task {
            client.users.execute()
        }.then {
            it.body()?.let { v ->
                users.addAll(v.list?.filter {user ->
                    user.firstName != null && user.lastName != null
                } ?: ArrayList())
            }
            users
        }.fail {
            Timber.d(it)
        }
    }

    fun update() {
        fetch()
    }
}
