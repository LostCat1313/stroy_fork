package ru.terra.axioma.contacts

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData

import com.google.common.collect.Lists
import nl.komponents.kovenant.ui.alwaysUi
import ru.terra.axioma.models.Contact

import java.util.Collections
import java.util.HashMap

import ru.terra.axioma.models.PhoneBookContact
import ru.terra.axioma.models.User
import ru.terra.axioma.repository.PhoneBookContactsRepository
import ru.terra.axioma.repository.UsersRepository


class ContactsViewModel(
        application: Application,
        private val contactsRepository: PhoneBookContactsRepository,
        private val usersRepository: UsersRepository
) : AndroidViewModel(application) {

    val contacts = MutableLiveData<List<PhoneBookContact>>()
    val loading = MutableLiveData<Boolean>()
    val selectedItem = MutableLiveData<PhoneBookContact>()

    init {
        loading.value = false
    }

    private fun mergeContacts(local: HashMap<String, PhoneBookContact>?, users: HashMap<String, User>) {
        if (local == null)
            return

        val merged: List<PhoneBookContact>
        val result = HashMap(local)
        for ((key, value) in result) {
            users[key]?.run { value.user = this }
        }
        merged = Lists.newArrayList(result.values)

        Collections.sort(merged) {
            o1, o2 -> o1.fullName.compareTo(o2.fullName)
        }
        contacts.postValue(merged)
        loading.postValue(false)
    }

    fun readContacts() {
        loading.value = true

        contactsRepository.contacts?.let {
            mergeContacts(it, usersRepository.hash)
        } ?: let {
            contactsRepository.fetch() success {
                mergeContacts(it, usersRepository.hash)
            }
        }
    }
}
