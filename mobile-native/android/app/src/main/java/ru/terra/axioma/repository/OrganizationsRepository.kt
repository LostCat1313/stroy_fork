package ru.terra.axioma.repository

import androidx.lifecycle.MutableLiveData
import nl.komponents.kovenant.Promise
import nl.komponents.kovenant.task
import nl.komponents.kovenant.then
import nl.komponents.kovenant.ui.alwaysUi
import ru.terra.axioma.client.ClientApi
import ru.terra.axioma.models.Organization
import timber.log.Timber

class OrganizationsRepository(private val client: ClientApi) {

    var organizations: MutableLiveData<List<Organization>> = MutableLiveData()
    var isLoading: MutableLiveData<Boolean> = MutableLiveData()

    val organizationsList: List<Organization>
        get() = organizations.value!!

    fun fetch(): Promise<Unit, Exception> {
        isLoading.value = true

        return task {
            client.organizations.execute()
        }.alwaysUi {
            isLoading.value = false
        }.then {
            if (it.isSuccessful) {
                val list = it.body()?.get("list")?.asJsonArray
                organizations.postValue(
                        Array(list?.size() ?: 0) { v ->
                            Organization(list?.get(v)!!)
                        }.asList()
                )
            }
        }.fail {
            Timber.d(it)
        }
    }

    fun getIdByName(name: String): Int? {
        return organizationsList.find { it.name.equals(name, true) }?.id
    }
}
