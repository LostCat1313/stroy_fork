package ru.terra.axioma.chat_v2


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.fragment_chat_info.*
import org.koin.android.viewmodel.ext.android.sharedViewModel
import ru.terra.axioma.R
import ru.terra.axioma.adapters.SimpleTextAdapter


class ChatInfoFragment : Fragment() {

    val viewModel: ChatViewModel by sharedViewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_chat_info, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel.selectedChat.observe(this, Observer { room ->
            val list = room?.users?.map { it.fullName }
            usersList.adapter = SimpleTextAdapter(list, R.layout.string_list_item2) {}

            title.text = room?.name
        })
    }
}
