package ru.terra.axioma.views

import android.content.Context

import ru.terra.axioma.R

/**
 * Created by alexei on 9/27/17.
 */

class NotificationMessage(context: Context) : NotificationCard(context) {

    override val iconRes: Int
        get() = R.drawable.zzz_message

    override val colorRes: Int
        get() = R.color.green

    override val titleRes: Int
        get() = R.string.card_message
}
