package ru.terra.axioma.repository

import android.util.SparseIntArray
import androidx.core.util.forEach
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import nl.komponents.kovenant.Promise
import nl.komponents.kovenant.task
import nl.komponents.kovenant.then
import nl.komponents.kovenant.ui.alwaysUi
import ru.terra.axioma.client.ClientApi
import ru.terra.axioma.models.Project
import ru.terra.axioma.models.ProjectFile
import timber.log.Timber

class ProjectsRepository(private val client: ClientApi) {

    var projects: MutableLiveData<List<Project>?> = MutableLiveData()
    var isLoading: MutableLiveData<Boolean> = MutableLiveData()

    fun fetch(): Promise<Unit, Exception> {
        isLoading.postValue(projects.value.isNullOrEmpty())

        return task {
            client.projects.execute()
        } then {
            if (it.isSuccessful) {
                it.body()?.get("list")?.asJsonArray?.let { array ->
                    projects.postValue(
                            Array(array.size()) { v ->
                                Gson().fromJson(array.get(v), Project::class.java)
                            }.asList()
                    )
                } ?: throw Exception(ProjectsRepository::class.simpleName + ": fetch error")
            }
        } alwaysUi {
            isLoading.value = false
        } fail {
            Timber.d(it)
        }
    }

    fun create(image: ProjectFile?, name: String): Promise<Project?, Exception> {
        return task {
            client.createProject(getJson(name, image)).execute()
        } then {
            it.body()
        } fail {
            Timber.d(it)
        }
    }

    fun update(projectId: Int, image: ProjectFile?, name: String): Promise<Project?, Exception> {
        return task {
            client.updateProject(projectId, getJson(name, image)).execute()
        } then {
            it.body()
        } fail {
            Timber.d(it)
        }
    }

    fun saveUsers(projectId: Int, users: SparseIntArray): Promise<Boolean, Exception> {
        return task {
            client.saveProjectUsers(projectId, getUsersJson(projectId, users)).execute()
        } then {
            it.isSuccessful
        } fail {
            Timber.d(it)
        }
    }

    private fun getJson(name: String, image: ProjectFile?): JsonObject {
        val data = JsonObject()
        data.add("entry", Gson().toJsonTree(Entry(name, image)))
        return data
    }

    private fun getUsersJson(projectId: Int, users: SparseIntArray): JsonObject {
        val obj = JsonObject()
        obj.addProperty("projectId", projectId.toString())

        val arr = JsonArray()
        users.forEach { key, value ->
            val element = JsonObject()
            element.addProperty("UserId", key)
            element.addProperty("rightMask", value)
            arr.add(element)
        }

        obj.add("rights", arr)
        return obj
    }

    fun getProjectTitle(id: Int): String? {
        return projects.value?.run {
            this.firstOrNull { v -> v.id == id }?.title
        }
    }

    fun getProject(id: Int): Project? {
        return projects.value?.let {
            it.firstOrNull { v -> v.id == id }
        }
    }

    class Entry(var title: String, var image: ProjectFile?)

}
