package ru.terra.axioma.views

import android.content.Context

import ru.terra.axioma.R

class NotificationDocument(context: Context) : NotificationCard(context) {

    override val iconRes: Int
        get() = R.drawable.zzz_file

    override val colorRes: Int
        get() = R.color.orange

    override val titleRes: Int
        get() = R.string.card_document
}
