package ru.terra.axioma.contact

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import androidx.databinding.DataBindingUtil
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders

import android.view.View
import android.widget.Toolbar

import com.amulyakhare.textdrawable.util.ColorGenerator
import kotlinx.android.synthetic.main.activity_contact.view.*
import kotlinx.android.synthetic.main.page_list_item.*

import pub.devrel.easypermissions.EasyPermissions
import ru.terra.axioma.Constants
import ru.terra.axioma.R
import ru.terra.axioma.databinding.ActivityContactBinding
import ru.terra.axioma.models.PhoneBookContact
import timber.log.Timber

class ContactActivity : AppCompatActivity(), EasyPermissions.PermissionCallbacks {
    private val CALL_PERMISSION = 1

    private var binding: ActivityContactBinding? = null
    private var viewModel: ContactViewModel? = null
    private var contact: PhoneBookContact? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView<ActivityContactBinding>(this, R.layout.activity_contact)

        val factory = ViewModelProvider.AndroidViewModelFactory.getInstance(application)
        viewModel = ViewModelProviders.of(this, factory).get(ContactViewModel::class.java)

        contact = intent.getParcelableExtra(Constants.CONTACT)
        binding!!.setContact(contact)

        viewModel!!.contact.set(contact)
        setSupportActionBar(viewModel!!.toolbar)

        val generator = ColorGenerator.MATERIAL

        // work
        val color = generator.getColor(binding!!.contact!!.fullName)

        // don't work
        // val color = generator.getColor(contact!!.fullName/*viewModel!!.contact!!.get()!!.fullName*/)

        Timber.d("test $color")

        viewModel!!.collapseToolbar?.setBackgroundColor(color)
        viewModel!!.collapseToolbar?.setContentScrimColor(color)
        viewModel!!.collapseToolbar?.setStatusBarScrimColor(color)

        if (viewModel!!.contact.get()!!.photo != null)
            viewModel!!.contact.get()!!.setPhoto(contact!!.photo)

        viewModel!!.call?.setOnClickListener({ v -> call(contact!!.phoneNumber)})
        viewModel!!.call?.setOnClickListener(View.OnClickListener {  })



        // setSupportActionBar(binding!!.toolbar)
        // setSupportActionBar(viewModel!!.toolbar)
        // Timber.d(viewModel!!.contact.get()!!.fullName)
        // val color = generator.getColor(contact!!.fullName)
        // val color = generator.getColor(binding!!.contact!!.fullName)


        /*binding!!.collapseToolbar.setBackgroundColor(color)
        binding!!.collapseToolbar.setContentScrimColor(color)
        binding!!.collapseToolbar.setStatusBarScrimColor(color)
        if (contact!!.photo != null)
            binding!!.photo.setImageBitmap(contact!!.photo)

        binding!!.call.setOnClickListener({ v -> call(contact!!.phoneNumber) })
        binding!!.chat.setOnClickListener(View.OnClickListener { })*/

    }

    private fun chat() {

    }

    @SuppressLint("MissingPermission")
    private fun call(phone: String) {
        val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone.replace("[^0-9|+]".toRegex(), "")))
        if (!EasyPermissions.hasPermissions(this, Manifest.permission.CALL_PHONE)) {
            EasyPermissions.requestPermissions(this, "", CALL_PERMISSION, Manifest.permission.CALL_PHONE)
            return
        }
        startActivity(intent)
    }

    override fun onPermissionsGranted(requestCode: Int, perms: List<String>) {
        if (requestCode == CALL_PERMISSION)
            call(contact!!.phoneNumber)
    }

    override fun onPermissionsDenied(requestCode: Int, perms: List<String>) {

    }
}