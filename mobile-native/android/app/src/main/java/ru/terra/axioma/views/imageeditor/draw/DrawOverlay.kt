package ru.terra.axioma.views.imageeditor.draw


import android.content.Context
import android.view.View.OnClickListener
import android.view.ViewGroup
import android.widget.FrameLayout
import ru.terra.axioma.BuildConfig
import ru.terra.axioma.models.marker.Marker
import ru.terra.axioma.views.imageeditor.PanelView
import timber.log.Timber

class DrawOverlay(
        context: Context,
        private val type: Int,
        private val scale: Float,
        private val initScale: Float
) : FrameLayout(context) {

    var onDone: (ArrayList<Marker>) -> Unit = { Timber.d("DrawOverlay -> onDone not initialized") }
    var onCancel: () -> Unit = { Timber.d("DrawOverlay -> onCancel not initialized") }

    private lateinit var drawView: DrawView

    init {
        layoutParams = LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)

        if (BuildConfig.DEBUG) {
            setBackgroundColor(0x4800ffff)
        }

        addSurface()
        addPanel()
    }

    private fun addSurface() {
        drawView = DrawView(context, type, scale, initScale)
        addView(drawView)
    }

    private fun addPanel() {
        val panelView = PanelView(context, 1)
        addView(panelView)

        panelView.setOnDoneClick(OnClickListener {
            onDone(drawView.markers)
        })

        panelView.setOnDeleteClick(OnClickListener {
            onCancel()
        })

        panelView.onSizeChanged = {
            drawView.setStrokeWidth(it.toFloat())
        }

        panelView.onColorChanged = {
            drawView.setColor(it)
        }
    }

    fun getMarkers(): ArrayList<Marker> {
        return drawView.markers
    }
}
