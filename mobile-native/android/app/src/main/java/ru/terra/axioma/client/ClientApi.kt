package ru.terra.axioma.client

import com.google.gson.JsonObject
import io.reactivex.Observable

import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Part
import retrofit2.http.Path
import retrofit2.http.Query
import retrofit2.http.Url
import ru.terra.axioma.models.*
import ru.terra.axioma.models.marker.Marker
import ru.terra.axioma.models.marker.MarkerEntry


interface ClientApi {

    /* Users */
    @get:GET("/api/users/?")
    val users: Call<Users>

    @get:GET("/api/users/projectscount")
    val projectsCount: Call<ResponseBody>

    @POST("/api/users/signUp/update")
    fun updateUser(@Body entry: JsonObject): Call<User>



    /* Authenticate */
    @POST("/api/authenticate")
    fun authenticate(@Body body: RequestBody): Call<JsonObject>

    @GET("/api/authenticate/logout")
    fun logout(): Call<JsonObject>

    @GET("/api/authenticate/signUp")
    fun signup(@Query("phone") phone: String, @Query("regId") regId: String, @Query("deviceType") deviceType: Int = 0): Call<User>

    @GET("/api/authenticate/activate")
    fun activate(@Query("phone") phone: String, @Query("code") code: String): Call<Token>



    /* Ping */
    @GET("/api/ping")
    fun checkSession(): Call<JsonObject>

    @POST("/api/ping")
    fun setFirebaseId(@Body entry: JsonObject): Call<ResponseBody>



    /* Organizations */
    @get:GET("/api/organizations")
    val organizations: Call<JsonObject>



    /* Positions */
    @get:GET("/api/positions")
    val positions: Call<JsonObject>



    /* Projects */
    @get:GET("/api/projects")
    val projects: Call<JsonObject>

    @POST("/api/projects")
    fun createProject(@Body entry: JsonObject): Call<Project>

    @PUT("/api/projects/{id}")
    fun updateProject(@Path("id") id: Int, @Body entry: JsonObject): Call<Project>

    @PUT("/api/projects/{id}/users")
    fun saveProjectUsers(@Path("id") id: Int, @Body entry: JsonObject): Call<ResponseBody>



    /* Notifications */
    @get:GET("/api/notifications/")
    val notifications: Call<Notifications>

    @get:GET("/api/notifications/")
    val notificationsRaw: Call<ResponseBody>

    @GET("/api/notifications/")
    fun getNotifications(@Query("offset") offset: Int, @Query("limit") limit: Int): Call<Notifications>



    /* Deep Links */
//    @POST("/api/deeplinks/new")



    /* Markers */
    @get:GET("/api/descriptors/?")
    val allMarkers: Call<MarkerEntry>

    @POST("/api/descriptors")
    fun saveMarker(@Body entry: MarkerEntry): Call<Marker>

    @PUT("/api/descriptors/{id}")
    fun updateMarker(@Path("id") id: Int, @Body entry: MarkerEntry): Call<Marker>

    @GET("/api/descriptors")
    fun getMarkers(@Query("docId") docId: Int, @Query("holderId") fileId: Int): Call<MarkerEntry>

    @GET("/api/descriptors")
    fun getMarkersByProjectAndType(@Query("projectId") projectId: Int, @Query("type") type: Int): Call<MarkerEntry>



    /* Files */
    @GET("/api/files/{fileId}")
    fun getFile(@Path("fileId") fileId: Int): Call<FileEntry>

    @GET
    fun downloadDocument(@Url url: String): Call<ResponseBody>

    @Multipart
    @POST("/api/files/upload")
    fun uploadFile(@Part("description") description: RequestBody, @Part file: MultipartBody.Part): Call<UploadResponse>

    @Multipart
    @POST("/api/files/upload")
    fun uploadFiles(@Part("description") description: RequestBody, @Part files: List<MultipartBody.Part>): Call<UploadResponse>



    @get:GET("/api/notes/")
    val notes: Call<PersonalNotes>

    @get:GET("api/documents/last")
    val recentDocuments: Call<DocumentsList>

    @POST("/api/notes/")
    fun createNote(@Body entry: JsonObject): Call<JsonObject>

    @PUT("/api/notes/{id}")
    fun updateNote(@Path("id") id: Int, @Body entry: JsonObject): Call<JsonObject>

    @DELETE("/api/notes/")
    fun deleteNotes(@Query("noteIds") id: List<Int>): Call<JsonObject>

    @GET("/api/projects/{projectId}/documents")
    fun getDocuments(@Path("projectId") projectId: Int): Call<DocumentsList>

    @GET("/api/projects/{projectId}/documents")
    fun getDocumentsRaw(@Path("projectId") projectId: Int): Call<ResponseBody>

    @POST("/api/document/versions")
    fun createVersion(@Body entry: JsonObject): Call<JsonObject>

    @GET("/api/documents/{id}")
    fun getDocument(@Path("id") id: Int): Call<DocumentEntry>

    //    @GET("/api/documents/{documentId}/pages")
    //    Call<Pages> getPages(@Path("documentId") int documentId);

    @POST("/api/documents/")
    fun createDocument(@Body entry: JsonObject): Call<JsonObject>

    @POST("/api/pages/update")
    fun updatePage(@Body entry: JsonObject): Call<JsonObject>

    @DELETE("/api/documents/{id}")
    fun deleteDocument(@Path("id") id: Int): Call<ResponseBody>

    @PUT("/api/documents/{id}")
    fun updateDocument(@Path("id") id: Int, @Body entry: DocumentEntry): Call<Document>

    @GET("/api/projects/{projectId}/users/?")
    fun getProjectContacts(@Path("projectId") projectId: Int): Call<Users>

    @Multipart
    @POST("/api/documents/{id}/{versionId}/pages/upload")
    fun uploadFileToVersion(@Path("id") docId: Int, @Path("versionId") versionId: Int, @Part("description") description: RequestBody, @Part file: MultipartBody.Part): Call<UploadResponse>

    @Multipart
    @POST("/api/documents/{id}/{versionId}/pages/upload")
    fun uploadFilesToVersion(@Path("id") docId: Int, @Path("versionId") versionId: Int, @Part("description") description: RequestBody, @Part files: List<MultipartBody.Part>): Call<ResponseBody>
}