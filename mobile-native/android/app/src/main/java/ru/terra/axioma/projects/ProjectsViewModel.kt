package ru.terra.axioma.projects

import android.app.Application
import android.net.Uri
import android.util.SparseIntArray
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import nl.komponents.kovenant.Promise
import nl.komponents.kovenant.task
import nl.komponents.kovenant.then
import nl.komponents.kovenant.ui.failUi
import nl.komponents.kovenant.ui.successUi
import okhttp3.MultipartBody
import okhttp3.RequestBody
import ru.terra.axioma.Configuration
import ru.terra.axioma.FileUtils
import ru.terra.axioma.client.ClientApi
import ru.terra.axioma.models.Project
import ru.terra.axioma.models.ProjectFile
import ru.terra.axioma.models.UploadResponse
import ru.terra.axioma.repository.ProjectsRepository
import timber.log.Timber
import java.lang.Exception

class ProjectsViewModel(
        application: Application,
        val client: ClientApi,
        private val projectsRepository: ProjectsRepository
) : AndroidViewModel(application) {

    val selectedProject = MutableLiveData<Project>()
    val project: Project
        get() {
            return selectedProject.value ?: Project()
        }

    val error = MutableLiveData<String?>()
    val loading = MutableLiveData<Boolean>()

    fun save(name: String, imageUri: Uri?) {
        loading.value = true
        uploadCover(imageUri) success {
            if (project.id != 0) {
                updateProject(it, name)
            } else {
                createProject(it, name)
            } success { p ->
                p?.run {
                    saveUsers(this.id, project.users) successUi { result ->
                        loading.value = false
                        if (result)
                            selectedProject.value = null
                    }
                }
            }
        } failUi {
            Timber.e(it)
            error.value = it.message
        }
    }

    private fun uploadCover(imageUri: Uri?): Promise<ProjectFile?, Exception> {
        val initialCoverUri = Uri.parse(Configuration.baseUrl + selectedProject.value?.image?.url)
        return if (imageUri != null && initialCoverUri != imageUri) {
            val description = RequestBody.create(MultipartBody.FORM, "description")
            val body = FileUtils.getFilePart("attachment", imageUri)

            task {
                client.uploadFile(description, body).execute()
            } then {
                it.body()?.let { upload: UploadResponse ->
                    upload.files[0]
                }
            }
        } else {
            Promise.ofSuccess(null)
        }
    }

    private fun createProject(image: ProjectFile?, name: String): Promise<Project?, Exception> {
        return projectsRepository.create(image, name) then { it }
    }

    private fun updateProject(image: ProjectFile?, name: String): Promise<Project?, Exception> {
        return projectsRepository.update(project.id, image, name) then { it }
    }

    private fun saveUsers(projectId: Int, users: SparseIntArray): Promise<Boolean, Exception> {
        return projectsRepository.saveUsers(projectId, users) failUi { error.value = it.message }
    }
}
