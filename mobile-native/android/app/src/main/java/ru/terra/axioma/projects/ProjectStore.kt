package ru.terra.axioma.projects

import ru.terra.axioma.models.Project

class ProjectStore {
    var value: Project? = null

    private object Holder {
        val INSTANCE = ProjectStore()
    }

    companion object {
        val instance: ProjectStore by lazy { Holder.INSTANCE }
    }
}