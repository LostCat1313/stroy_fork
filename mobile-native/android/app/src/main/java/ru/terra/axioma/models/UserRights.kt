package ru.terra.axioma.models

class UserRights {
    companion object {
        const val READ = 1
        const val WRIGHT = 2
        const val ADMIN = 7

        fun getString(r: Int = READ): String {
            return when (r) {
                READ -> "Чтение"
                WRIGHT -> "Запись"
                ADMIN -> "Управление"
                else -> "Не задано"
            }

        }

        fun getInt(s: String): Int {
            return when (s) {
                "Запись" -> WRIGHT
                "Управление" -> ADMIN
                else -> READ
            }
        }
    }
}