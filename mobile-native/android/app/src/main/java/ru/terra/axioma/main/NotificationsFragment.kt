package ru.terra.axioma.main

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.graphics.Rect
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.*
import android.widget.CheckBox
import android.widget.PopupWindow

import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.afollestad.materialdialogs.MaterialDialog
import kotlinx.android.synthetic.main.fragment_notifications.*
import org.koin.android.viewmodel.ext.android.sharedViewModel
import nl.komponents.kovenant.task
import nl.komponents.kovenant.ui.alwaysUi
import nl.komponents.kovenant.ui.successUi
import org.koin.android.ext.android.getKoin
import org.koin.android.ext.android.inject
import ru.terra.axioma.*
import ru.terra.axioma.adapters.NotificationsAdapter
import ru.terra.axioma.chat_v2.ChatActivity
import ru.terra.axioma.models.Notification
import ru.terra.axioma.repository.DocumentsRepository
import ru.terra.axioma.repository.ProjectsRepository
import ru.terra.axioma.store.ChatStore
import ru.terra.axioma.store.CurrentSessionStore

class NotificationsFragment : Fragment() {

    private var broadcastReceiver: BroadcastReceiver? = null

    private var filterMenu: PopupWindow? = null
    private lateinit var adapter: NotificationsAdapter
    private var loadingDialog: MaterialDialog? = null

    private val vm: MainViewModel by sharedViewModel()
    private val currentSessionStore: CurrentSessionStore by inject()
    private val documentsRepository: DocumentsRepository by inject()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_notifications, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setupList()


        filterMenu = getFilterMenu()

        if (loadingDialog != null)
            loadingDialog?.dismiss()

//        fab.setOnClickListener { onFabClicked() }
        refresh.setOnRefreshListener {
            task {
                vm.refreshNotifications()
            } alwaysUi {
                refresh.isRefreshing = false
            }
        }

        super.onViewCreated(view, savedInstanceState)
    }

    private fun setupList() {
        adapter = NotificationsAdapter(currentSessionStore)
        adapter.setClickListener { notification: Notification ->
            when (notification.type) {
                Constants.NotificationType.CHAT_MESSAGE -> onChatNotificationClick(notification)
                Constants.NotificationType.CHAT_ROOM_ADDED -> onChatNotificationClick(notification)
                else -> onDocumentNotificationClick(notification)
            }
        }

        /*recyclerView.addOnScrollListener(object : EndlessRecyclerViewScrollListener(recyclerView.layoutManager as LinearLayoutManager) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView) {
                vm.loadPage(page)
            }
        })*/
        recyclerView.adapter = adapter

        vm.notifications.observe(this, Observer {
            adapter.items = it as ArrayList<Notification>
        })

        vm.notificationFilterBy.observe(this, Observer {
            adapter.filter(it)
        })
    }

    private fun onChatNotificationClick(notification: Notification) {
        notification.message?.let {
            val store: ChatStore by inject()
            store.targetMessage = notification.message
            startActivity(Intent(context, ChatActivity::class.java))
        }
    }

    private fun onDocumentNotificationClick(notification: Notification) {
        checkProject(notification, { n -> vm.onSameFunction(n) }, { n -> vm.onDifferentFunction(notification = n) })
    }

    override fun onResume() {
        super.onResume()

        broadcastReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                vm.loadNotifications()
            }
        }

        try {
            LocalBroadcastManager.getInstance(requireContext()).registerReceiver(broadcastReceiver as BroadcastReceiver, IntentFilter(Constants.UPDATE_NOTIFICATION_LIST))
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    override fun onPause() {
        super.onPause()
        broadcastReceiver?.let { LocalBroadcastManager.getInstance(context!!).unregisterReceiver(it) }
    }

    /*fun onFabClicked() {
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.fab_popup_notification, null)

        PopupWindow(context).apply {
            isFocusable = true
            isOutsideTouchable = true
            width = WindowManager.LayoutParams.WRAP_CONTENT
            height = WindowManager.LayoutParams.WRAP_CONTENT
            contentView = view
            setBackgroundDrawable(ColorDrawable(Color.WHITE))
            elevation = 10f

            view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED)
            val yoff = root.height - fab.top + fab.paddingEnd
            showAtLocation(root, Gravity.BOTTOM or Gravity.END, fab.paddingEnd, yoff)
        }
    }*/

    private fun getFilterMenu(): PopupWindow {
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.filter_popup_menu, null)

        val popupWindow = PopupWindow(context).apply {
            isFocusable = true
            isOutsideTouchable = true
            width = WindowManager.LayoutParams.WRAP_CONTENT
            height = WindowManager.LayoutParams.WRAP_CONTENT
            contentView = view
            setBackgroundDrawable(ColorDrawable(Color.WHITE))
            elevation = 0f

            contentView?.findViewById<CheckBox>(R.id.filter_docs)?.apply {
                isChecked = vm.getFilterState(FilterBy.Docs)
                setOnCheckedChangeListener { _: View, isChecked: Boolean -> vm.updateFilter(FilterBy.Docs, isChecked) }
            }
            contentView?.findViewById<CheckBox>(R.id.filter_notes)?.apply {
                isChecked = vm.getFilterState(FilterBy.Notes)
                setOnCheckedChangeListener { _: View, isChecked: Boolean -> vm.updateFilter(FilterBy.Notes, isChecked) }
            }
            contentView?.findViewById<CheckBox>(R.id.filter_messages)?.apply {
                isChecked = vm.getFilterState(FilterBy.Messages)
                setOnCheckedChangeListener { _: View, isChecked: Boolean -> vm.updateFilter(FilterBy.Messages, isChecked) }
            }
            contentView?.findViewById<CheckBox>(R.id.filter_project)?.apply {
                isEnabled = currentSessionStore.project != null
                isChecked = !vm.getFilterState(FilterBy.Messages)
                setOnCheckedChangeListener { _: View, isChecked: Boolean -> vm.updateFilter(FilterBy.Project, isChecked) }
            }
        }

        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED)

        return popupWindow
    }

    fun showFilterMenu() {
        val r = Rect()
        recyclerView.getGlobalVisibleRect(r)
        filterMenu?.run {
            contentView?.findViewById<CheckBox>(R.id.filter_project)?.apply {
                isEnabled = currentSessionStore.project != null
            }
            showAtLocation(root, Gravity.TOP or Gravity.END, 0, r.top)
        }
    }

    private fun checkProject(notification: Notification, onSame: (Notification) -> Unit, onDifferent: (Notification) -> Unit) {
        val pr: ProjectsRepository = getKoin().get()

        val fn = {
            if (currentSessionStore.project == null || currentSessionStore.project!!.id != notification.projectId) {
                pr.getProject(notification.projectId)?.let { p ->
                    context?.let {
                        MaterialDialog(it)
                                .message(text = getString(R.string.project_choose_another_project, p.title))
                                .negativeButton(R.string.common_cancel)
                                .positiveButton(R.string.editor_ok) {
                                    currentSessionStore.project = p
                                    documentsRepository.clear2()
                                    onDifferent(notification)
                                }
                                .show()
                    }
                }
            } else
                onSame(notification)
        }

        if (pr.projects.value?.isEmpty() != false) {
            pr.fetch() successUi {
                fn()
            }
        } else
            fn()
    }

    companion object {

        fun newInstance(): NotificationsFragment {
            return NotificationsFragment()
        }
    }
}
