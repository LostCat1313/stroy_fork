package ru.terra.axioma.chat_v2

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.activity_chat_2.*
import org.koin.android.viewmodel.ext.android.viewModel
import ru.terra.axioma.R

class ChatActivity : AppCompatActivity() {

    private val viewModel: ChatViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat_2)

        supportFragmentManager.beginTransaction().replace(container.id, ListFragment()).commit()

        viewModel.selectedChat.observe(this, Observer {
            it?.let { r ->
                supportFragmentManager.beginTransaction().addToBackStack(r.name).add(container.id, ItemFragment()).commit()
            }
        })
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 0)
            supportFragmentManager.popBackStack()
        else
            super.onBackPressed()
    }
}
