package ru.terra.axioma.views;

import android.graphics.Rect;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import ru.terra.axioma.R;

public class ItemDecoratorForFab extends RecyclerView.ItemDecoration {
    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        final int itemPosition = parent.getChildAdapterPosition(view);
        final int itemCount = state.getItemCount();

        if (itemCount > 0 && itemPosition == itemCount - 1) {
            outRect.bottom = parent.getContext().getResources().getDimensionPixelOffset(R.dimen.fab_height)
                    + parent.getContext().getResources().getDimensionPixelOffset(R.dimen.fab_margin);
        }
    }
}
