package ru.terra.axioma.chat_v2

import android.app.Application
import android.net.Uri
import androidx.annotation.NonNull
import androidx.databinding.ObservableInt
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import io.socket.client.Ack
import io.socket.client.Socket
import io.socket.emitter.Emitter
import nl.komponents.kovenant.Kovenant
import nl.komponents.kovenant.Promise
import nl.komponents.kovenant.task
import nl.komponents.kovenant.then
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import ru.terra.axioma.FileUtils
import ru.terra.axioma.R
import ru.terra.axioma.client.ClientApi
import ru.terra.axioma.models.UploadResponse
import ru.terra.axioma.models.User
import ru.terra.axioma.models.chat.Message
import ru.terra.axioma.models.chat.Room
import ru.terra.axioma.store.ChatStore
import ru.terra.axioma.store.CurrentSessionStore
import timber.log.Timber
import java.io.File
import java.lang.Exception
import java.util.*
import kotlin.collections.ArrayList


class ChatViewModel(
        application: Application,
        private val client: ClientApi,
        private val socket: Socket?,
        private val currentSessionStore: CurrentSessionStore,
        private val chatStore: ChatStore
) : AndroidViewModel(application) {

    val chats = MutableLiveData<ArrayList<Room>>()
    var contacts = ArrayList<User>()
    val selectedChat = MutableLiveData<Room?>()

    val messages = MutableLiveData<ArrayList<Message>>()
    val attachments = MutableLiveData<ArrayList<Uri>>()

    val loading = MutableLiveData<Boolean>()
    val title = ObservableInt()


    init {
        connect()
    }


    private fun connect() {
        socket?.run {
            if (connected()) {// TODO иправить путем кэширования всего и вся
                disconnect()
                close()
            } else {
                loading.value = true
            }


            val onInit = Emitter.Listener { args: Array<out Any>? ->
                Timber.d(Arrays.toString(args))
                args?.let {
                    val data = Gson().fromJson(it[0].toString(), ChatData::class.java)
//                    val r = chatData?.rooms?.filter { v -> v.users.isNotEmpty() }

                    chatStore.targetMessage?.let { message ->
                        chatStore.targetMessage = null
                        data.rooms.find { r -> r.id == message.roomId }?.let { room ->
                            openChat(room)
                        }
                    }

                    chats.postValue(data.rooms)
                    contacts = data.contacts

                    loading.postValue(false)
                }
            }
            on("init", onInit)


            val onError = Emitter.Listener {
                args: Array<out Any>? -> Timber.d(Arrays.toString(args))
            }
            on(Socket.EVENT_ERROR, onError)


            val onMessage = Emitter.Listener { args: Array<out Any>? ->
                Timber.d(Arrays.toString(args))
                attachments.value?.let {
                    attachments.postValue(arrayListOf())
                }

                args?.let {
                    val msg = Gson().fromJson(it[0].toString(), Message::class.java)

                    selectedChat.value?.let { room ->
                        if (msg.room.id == room.id) {
                            val msgs = messages.value ?: arrayListOf()
                            msgs.add(msg)
                            messages.postValue(msgs)
                        }
                    }

                    chats.postValue(chats.value?.let { list ->
                        val e = list.first { r -> r.id == msg.room.id }
                        list.remove(e)
                        e.lastMessage = msg
                        list.add(0, e)
                        list
                    })
                }
            }
            on(Socket.EVENT_MESSAGE, onMessage)


            connect()
        }
    }

    private fun join(chat: Room) {
        attachments.value?.clear()
        chatStore.attachment?.let { uri ->
            addAttachment(uri)
        }

        val onJoin = Ack { args ->
            Timber.d(Arrays.toString(args))
            args?.let {
                val data = Gson().fromJson(it[0].toString(), RoomData::class.java)

                data.messages.map { m ->
                    m.user = contacts.find { user -> user.id == m.senderId }
                }

                messages.postValue(data.messages)
            }
        }

        val obj = JSONObject().put("roomId", chat.id)
        socket?.emit("join", obj, onJoin)
    }


    fun sendMessage(message: String?) {
        chatStore.attachment?.let {
            chatStore.attachment = null
        }

        selectedChat.value?.let { room ->
            val outgoingMessage = OutgoingMessage().apply {
                roomId = room.id
                content = message ?: ""
            }

            attachments.value?.let {
                uploadFiles(it) then { list ->
                    list?.let {
                        outgoingMessage.attachments = it
                    }
                } always {
                    socket?.emit("message", JSONObject(Gson().toJson(outgoingMessage)))
                }
            } ?: run {
                socket?.emit("message", JSONObject(Gson().toJson(outgoingMessage)))
            }
        }
    }

    private fun uploadFiles(attachments: ArrayList<Uri>?): Promise<ArrayList<OutgoingAttachment>?, Exception> {
        val description = RequestBody.create(MultipartBody.FORM, "description")

        return attachments?.run {
            val parts: List<MultipartBody.Part> = mapIndexed { index, uri -> FileUtils.getFilePart("attachment$index", uri) }

            task {
                client.uploadFiles(description, parts).execute()
            } then {
                it.body()?.files?.map { v -> OutgoingAttachment(v.id) } as ArrayList
            } fail {
                Timber.e(it)
            }
        } ?: Promise.ofSuccess(null)
    }


    fun createChat(name: String, userIds: ArrayList<Int>) {
        val newChat = NewChat().apply {
            this.name = name
            this.userIds = userIds
        }

        socket?.emit("join", JSONObject(Gson().toJson(newChat)), Ack { args ->
            Gson().fromJson(args[0].toString(), RoomData::class.java).room?.let {
                val l = chats.value ?: arrayListOf()
                l.add(0, it)
                chats.postValue(l)
            }
        })
    }


    fun openChat(chat: Room) {
        messages.postValue(arrayListOf())
        selectedChat.postValue(chat)
        join(chat)
    }

    fun addAttachment(uri: Uri) {
        val l = attachments.value ?: arrayListOf()
        l.add(uri)
        attachments.postValue(l)
    }




    class ChatData {
        var contacts: ArrayList<User> = arrayListOf()
        var rooms: ArrayList<Room> = arrayListOf()
    }

    class RoomData {
        var room: Room? = null
        var count: Int = 0
        var messages: ArrayList<Message> = arrayListOf()
    }

    class OutgoingMessage {
        var roomId: Int = 0
        var content: String = ""
        var attachments: ArrayList<OutgoingAttachment> = arrayListOf()
    }

    class OutgoingAttachment(id: Int) {
        var file: OutgoingAttachmentFile = OutgoingAttachmentFile(id)
    }

    class OutgoingAttachmentFile(var id: Int)

    class NewChat {
        var name: String = ""
        var userIds: ArrayList<Int> = arrayListOf()
    }
}