package ru.terra.axioma.models.chat;

import android.os.Parcel;
import android.os.Parcelable;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ru.terra.axioma.models.User;

/**
 * Created by alexei on 12/17/17.
 */

public class Message implements Parcelable {
    public int id;
    public int viewType;
    public int roomId;
    public Room room;
    public int senderId;
    public User user;
    public String content;

    public Date createdAt;
    public Date updatedAt;

    public List<Attachment> attachments;

    public Message() {
    }

    public Message(int id, int roomId, Room room, int senderId, User user, String content, Date createdAt, Date updatedAt, List<Attachment> attachments) {
        this.id = id;
        this.roomId = roomId;
        this.room = room;
        this.senderId = senderId;
        this.user = user;
        this.content = content;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.attachments = attachments;
    }

    public String getMessageTime() {
        SimpleDateFormat format = new SimpleDateFormat("hh:mm");
        return format.format(createdAt);
    }

    public int getSenderId() {
        return user == null ? senderId : user.id;
    }

    public int getRoomId() {
        return room == null ? roomId : room.id;
    }

    public Message getTextMessage() {
        Message m = this.copy();
        m.viewType = 0;
        m.attachments = null;
        return m;
    }

    public Message getPictureMessage() {
        Message m = this.copy();
        m.viewType = 1;
        return m;
    }

    public void setMessage(Message msg) {
        this.id = msg.id;
        this.roomId = msg.roomId;
        this.room = msg.room;
        this.senderId = msg.senderId;
        this.user = msg.user;
        this.content = msg.content;
        this.createdAt = msg.createdAt;
        this.updatedAt = msg.updatedAt;
        this.attachments = msg.attachments;
    }

    private Message copy() {
        return new Message(
                id,
                roomId,
                room,
                senderId,
                user,
                content,
                createdAt,
                updatedAt,
                attachments
        );
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeInt(this.viewType);
        dest.writeInt(this.roomId);
        dest.writeParcelable(this.room, flags);
        dest.writeInt(this.senderId);
        dest.writeParcelable(this.user, flags);
        dest.writeString(this.content);
        dest.writeLong(this.createdAt != null ? this.createdAt.getTime() : -1);
        dest.writeLong(this.updatedAt != null ? this.updatedAt.getTime() : -1);
        dest.writeList(this.attachments);
    }

    protected Message(Parcel in) {
        this.id = in.readInt();
        this.viewType = in.readInt();
        this.roomId = in.readInt();
        this.room = in.readParcelable(Room.class.getClassLoader());
        this.senderId = in.readInt();
        this.user = in.readParcelable(User.class.getClassLoader());
        this.content = in.readString();
        long tmpCreatedAt = in.readLong();
        this.createdAt = tmpCreatedAt == -1 ? null : new Date(tmpCreatedAt);
        long tmpUpdatedAt = in.readLong();
        this.updatedAt = tmpUpdatedAt == -1 ? null : new Date(tmpUpdatedAt);
        this.attachments = new ArrayList<Attachment>();
        in.readList(this.attachments, Attachment.class.getClassLoader());
    }

    public static final Creator<Message> CREATOR = new Creator<Message>() {
        @Override
        public Message createFromParcel(Parcel source) {
            return new Message(source);
        }

        @Override
        public Message[] newArray(int size) {
            return new Message[size];
        }
    };
}
