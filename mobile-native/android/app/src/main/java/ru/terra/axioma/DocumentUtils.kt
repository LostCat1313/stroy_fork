package ru.terra.axioma

import android.content.Context
import android.util.SparseArray

import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream
import okhttp3.ResponseBody
import ru.terra.axioma.models.Document
import ru.terra.axioma.models.ProjectFile
import ru.terra.axioma.models.Version

object DocumentUtils {
    fun findDocument(where: List<Document>?, what: Int): Document? {
        if (where == null)
            return null

        for (v in where) {
            if (v.id == what)
                return v

            if (v.children != null && !v.children.isEmpty()) {
                val d = findDocument(v.children, what)

                if (d != null)
                    return d
            }
        }

        return null
    }

    fun findFileByMarker(where: Document, markerId: Int, verType: Int): ProjectFile? {
        val pages = where.getPagesList(verType)
        for (p in pages) {
            if (p.containsMarker(markerId))
                return p
        }

        return null
    }

    fun findFile(where: Document, what: Int): ProjectFile? {
        return findFile(where, what, Constants.VersionType.ALL)
    }

    private fun findFile(where: Document, what: Int, verType: Int): ProjectFile? {
        val pages = where.getPagesList(verType)
        for (v in pages) {
            if (v.id == what)
                return v
        }

        return null
    }

    fun findVersion(where: Document, file: ProjectFile): Version? {
        for (v in where.versions) {
            if (v.pages.contains(file))
                return v
        }

        return null
    }

    fun isFileHasVersions(where: Document, file: ProjectFile): Boolean {
        for (v in where.versions) {
            for (f in v.pages) {
                if (f.id != file.id && f.pageNumber == file.pageNumber)
                    return true
            }
        }

        return false
    }

    fun getFromCache(context: Context?, name: String): Pair<Boolean, String> {
        if (context != null) {
            val dest = File(context.applicationInfo.dataDir + "/cache/docs/" + name)
            if (dest.exists())
                return Pair(true, dest.absolutePath)
        }
        return Pair(false, "")
    }

    fun writeToCache(context: Context, body: ResponseBody, name: String): Pair<Boolean, String> {
        return writeDocument(context, body, "docs", name)
    }

    fun writeToTemp(context: Context, body: ResponseBody): Pair<Boolean, String>? {
        return writeDocument(context, body, "temp", "temp")
    }

    private fun writeDocument(context: Context, body: ResponseBody, folder: String, name: String): Pair<Boolean, String> {
        val cacheDir = File(context.applicationInfo.dataDir + "/cache/" + folder)
        if (!cacheDir.exists())
            cacheDir.mkdirs()

        val filename = File(cacheDir, name)

        try {
            body.byteStream().use { inputStream ->
                FileOutputStream(filename).use { outputStream ->
                    inputStream.copyTo(outputStream)

                    outputStream.flush()
                    outputStream.close()
                    inputStream.close()

                    return Pair(true, filename.absolutePath)
                }
            }
        } catch (e: IOException) {
            return Pair(false, "")
        }
    }

    fun insert(target: MutableList<Document>?, document: Document): List<Document>? {
        if (target == null)
            return null

        if (document.parentId == null) {
            target.add(document)
            return target
        } else {
            for (doc in target) {
                if (doc.id == document.parentId) {
                    doc.getChildren().add(document)
                    return target
                }

                if (insert(doc.getChildren(), document) != null) {
                    return target
                }
            }
        }

        return null
    }

    fun getDocPath(target: List<Document>, document: Document): String {
        var doc = document
        val flat = flatten(target, null)

        var res = document.title
        var parent: Document? = Document()
        // TODO repair
        /*while (parent != null) {
            parent = if (document.parentId == null) null else flat.get(document.parentId)
            if (parent != null)
                res = parent.title + " / " + res
            doc = parent
        }*/

        return res
    }

    private fun flatten(list: List<Document>?, res: SparseArray<Document>?): SparseArray<Document> {
        var res = res
        if (res == null)
            res = SparseArray()

        if (list == null)
            return res

        for (doc in list) {
            res.put(doc.id, doc)
            if (!doc.getChildren().isEmpty()) {
                flatten(doc.getChildren(), res)
            }
        }

        return res
    }
}
