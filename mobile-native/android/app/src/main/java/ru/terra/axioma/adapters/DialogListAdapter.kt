package ru.terra.axioma.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ru.terra.axioma.R
import ru.terra.axioma.models.marker.Marker

internal class DialogListAdapter(private var onClickListener: OnClickListener) : RecyclerView.Adapter<DialogListAdapter.ViewHolder>() {
    private var items: List<Marker> = ArrayList()

    fun setItems(items: List<Marker>) {
        this.items = items
        notifyDataSetChanged();
    }

    internal inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var content: TextView = view.findViewById(R.id.content)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.project_note_item, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val obj = items[position]
        holder.content.text = obj.text
        holder.itemView.setOnClickListener {
            onClickListener.onItemClick(obj)
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    interface OnClickListener {
        fun onItemClick(note: Marker)
    }


}