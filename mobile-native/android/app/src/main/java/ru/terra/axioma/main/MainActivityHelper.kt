package ru.terra.axioma.main

import android.content.Context
import android.content.Intent
import ru.terra.axioma.Constants
import ru.terra.axioma.DocumentUtils
import ru.terra.axioma.R
import ru.terra.axioma.documentviewer.DocumentActivity
import ru.terra.axioma.models.Document
import ru.terra.axioma.models.Notification
import ru.terra.axioma.models.marker.Marker
import ru.terra.axioma.pages.PagesActivity
import ru.terra.axioma.views.CustomDialog

object MainActivityHelper {
    fun openDocument(context: Context, documents: List<Document>, notification: Notification) {
        if (notification.isDocument)
            openDocument(context, documents, notification.targetId)
        else
            openDocument(context, documents, notification.marker)
    }

    fun openDocument(context: Context, documents: List<Document>, docId: Int) {
        val document = DocumentUtils.findDocument(documents, docId)
// TODO убрать эту проверку чтобы не делать поиск дважды
        if (document == null) {
            CustomDialog.error(context, R.string.error_document_not_found)
        } else {
            val intent = Intent(context, PagesActivity::class.java)
            intent.putExtra(DocumentActivity.DOCUMENT_ID, docId)
            intent.putExtra(DocumentActivity.DOCUMENT, document)
            context.startActivity(intent)
        }
    }

    fun openDocument(context: Context, documents: List<Document>, marker: Marker?) {
        // TODO убрать в DocumentActivity поиск
        marker?.let { m ->
            DocumentUtils.findDocument(documents, m.docId)?.let { d ->
                DocumentUtils.findFile(d, m.fileId)?.let { p ->
                    Intent(context, DocumentActivity::class.java).apply {
                        putExtra(DocumentActivity.DOCUMENT, d)
                        putExtra(DocumentActivity.FILE, p)

                        putExtra(DocumentActivity.VERSION, Constants.VersionType.ALL)
                        putExtra(DocumentActivity.DOCUMENT_ID, d.id)
                        putExtra(DocumentActivity.FILE_ID, p.id)
                        putExtra(DocumentActivity.MARKER_ID, m.id)
                        context.startActivity(this)
                    }
                } ?: CustomDialog.error(context, R.string.error_page_not_found)
            } ?: CustomDialog.error(context, R.string.error_document_not_found)
        } ?: CustomDialog.error(context, R.string.error_marker_is_null)
    }
}
