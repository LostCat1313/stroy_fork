package com.choiintack.recursiverecyclerview;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by choiintack on 2017. 1. 31..
 */

public abstract class RecursiveRecyclerAdapter<VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> {
    abstract public void onExpand(VH holder);
    abstract public void onCollapse(VH holder);
    abstract public boolean onItemClick(View view, RecursiveItem item);

    private List<RecursiveItemSet> mItems = new ArrayList<>();

    public void setItems(List items) {
        if (mItems.isEmpty()) {
            for (Object item : items) {
                mItems.add(new RecursiveItemSet(
                        item, false, 0
                ));
            }
        }
    }


    public void addItem(int index, Object item) {
        mItems.add(index, new RecursiveItemSet(
                item, false, 0
        ));
    }

    public void removeItem(int index) {
        mItems.remove(index);
    }

    public Object getItem(int position) {
        return mItems.get(position).getItem();
    }

    public Object getRecursiveItem(int position) {
        return mItems.get(position);
    }

    public List<RecursiveItemSet> getItems() {
        return mItems;
    }

    @Override
    public void onBindViewHolder(final VH holder, final int position) {
        if (getItem(position) instanceof RecursiveItem) {
            final RecursiveItem item = (RecursiveItem) getItem(position);
            final Object _item = mItems.get(position);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemClick(v, item))
                        return;

                    if (expanded(_item)) {
                        removeItems(_item, item.getChildren());
                        onCollapse(holder);
                    } else {
                        addItems(_item, item.getChildren());
                        onExpand(holder);
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mItems.get(position).getDepth();
    }

    public boolean expanded(Object item) {
        int position =  mItems.indexOf(item);
        return mItems.get(position).isExpanded();
    }

    public void addItems(Object parent, List items) {
        int position = mItems.indexOf(parent);
        for (int i = 0; i < items.size(); i ++) {
            Object item = items.get(i);
            mItems.add(position + 1 + i, new RecursiveItemSet(
                    item, false, mItems.get(position).getDepth() + 1
            ));
        }
        mItems.get(position).expanded = true;
        notifyItemRangeInserted(position + 1, items.size());
        notifyItemChanged(position);
    }

    public void addItem(boolean isTopLevel, Object parentItem, Object item) {
        RecursiveItemSet parent = (RecursiveItemSet) parentItem;
        int position = mItems.indexOf(parent);
        RecursiveItem recursiveItem = (RecursiveItem) parent.getItem();

        if (isTopLevel)
            mItems.add(new RecursiveItemSet(item, false, 0));
        else if (parent.isExpanded())
            mItems.add(position + recursiveItem.getChildren().size(), new RecursiveItemSet(
                    item, false, mItems.get(position).getDepth() + 1
            ));
    }

    public void removeItems(Object parent, List items) {
        int position = mItems.indexOf(parent);
        for (int i = 0; i < items.size(); i ++) {
            if (getItem(position + 1) instanceof RecursiveItem) {
                if (expanded(mItems.get(position + 1))) {
                    RecursiveItem item = (RecursiveItem) getItem(position + 1);
                    removeItems(mItems.get(position + 1), item.getChildren());
                }
            }
            mItems.remove(position + 1);
        }
        mItems.get(position).expanded = false;
        notifyItemRangeRemoved(position  + 1, items.size());
        notifyItemChanged(position);
    }
    
    public void clearItems() {
        if (mItems != null) {
            mItems.clear();
        }
    }

    public boolean toggle(VH holder, Object _item) {
//        if (getItem(position) instanceof RecursiveItem) {
            final RecursiveItem item = (RecursiveItem) ((RecursiveItemSet) _item).getItem(); /*(RecursiveItem) getItem(position)*/;
//            final Object _item = mItems.get(position);

            if (expanded(_item)) {
                removeItems(_item, item.getChildren());
                onCollapse(holder);
                return false;
            } else {
                addItems(_item, item.getChildren());
                onExpand(holder);
                return true;
            }
        }

//        return false;
//    }
}
