
import Foundation
import SwiftyJSON

struct Rights {
    struct AccessMask {
        static let read: Int = 0x01
        static let write: Int = 0x02
        static let control: Int = 0x04
    }
    
    enum Role {
        case reader
        case writer
        case admin
        case custom(_ mask: Int)
        var caption: String {
            switch self {
                case .reader: return "читатель"
                case .writer: return "писатель"
                case .admin: return "админ"
                case .custom(_): return "какойто-тип"
            }
        }
        
        var mask: Int {
            switch self {
                case .reader: return AccessMask.read
                case .writer: return AccessMask.read | AccessMask.write
                case .admin: return AccessMask.read | AccessMask.write | AccessMask.control
                case .custom(let mask): return mask
            }
        }
        
        var canRead: Bool {return (mask & AccessMask.read) > 0}
        
        var canWrite: Bool {return (mask & AccessMask.write) > 0}
        
        var canControl: Bool {return (mask & AccessMask.control) > 0}
        
        
        static func mask2role(_ mask: Int) -> Role {
            if mask == AccessMask.read {
                return .reader
            } else if mask == AccessMask.read | AccessMask.write {
                return .writer
            } else if mask == AccessMask.read | AccessMask.write | AccessMask.control {
                return .admin
            } else {
                return .custom(mask)
            }
        }
    }

    static let availableRoles: [Role] = [.reader, .writer, .admin]

}

class User {
    var id: Int
    var name: String
    var email: String?
    var phoneNumber: String?
    var firstName: String?
    var secondName: String?
    var lastName: String?
    var organizationId: Int?
    var positionId: Int?
    var isActive: Bool?
    
    var createdAt: Date
    var updatedAt: Date
    
    
    init(id: Int, name: String) {
        self.id = id
        self.name = name
        self.createdAt = Date()
        self.updatedAt = Date()
    }
    
    init(json: JSON) {
        id = json["id"].intValue
        name = json["name"].stringValue
        email = json["email"].string
        phoneNumber = json["phoneNumber"].string
        firstName = json["firstName"].string
        secondName = json["secondName"].string
        lastName = json["lastName"].string
        positionId = json["positionId"].int
        isActive = json["isActive"].bool
        
        createdAt = json["createdAt"].string?.prefix(19).getDate(dateFormat: "yyyy-MM-dd'T'HH:mm:ss") ?? Date()
        updatedAt = json["updatedAt"].string?.prefix(19).getDate(dateFormat: "yyyy-MM-dd'T'HH:mm:ss") ?? Date()
    }
    
    init(data: [String:Any]) {
        id = data["id"] as! Int
        name = data["name"] as! String
        email = data["email"] as? String
        phoneNumber = data["phoneNumber"] as? String
        firstName = data["firstName"] as? String
        secondName = data["secondName"] as? String
        lastName = data["lastName"] as? String
        positionId = data["positionId"] as? Int
        isActive = data["isActive"] as? Bool
        
        createdAt = (data["createdAt"] as! String).prefix(19).getDate(dateFormat: "yyyy-MM-dd'T'HH:mm:ss")
        updatedAt = (data["updatedAt"] as! String).prefix(19).getDate(dateFormat: "yyyy-MM-dd'T'HH:mm:ss")
    }
    
    func getParams() -> [String: Any] {
        var params: [String: Any] = [:]
        params["id"] = id
        params["name"] = name
        params["firstName"] = firstName
        params["secondName"] = secondName
        params["lastName"] = lastName
        params["positionId"] = positionId
        params["organizationId"] = organizationId
        params["phoneNumber"] = phoneNumber
        params["email"] = email
        return params
    }
    
    var iconName: String {
        if let firstName = self.firstName, let lastName = self.lastName, !firstName.isEmpty && !lastName.isEmpty {
            return "\(lastName.prefix(1).uppercased())\(firstName.prefix(1).uppercased())"
        } else {
            return name.prefix(2).uppercased()
        }
    }
}

class UserRole {
    let userId: Int
    let role: Rights.Role
    init(_ json: JSON) {
        userId = json["id"].intValue
        role = Rights.Role.mask2role(json["rightMask"].intValue)
    }
    init(userId: Int, role: Rights.Role) {
        self.userId = userId
        self.role = role
    }
}
