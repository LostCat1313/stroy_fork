import Foundation

extension NSNotification.Name {
    static let didGetForbidden = NSNotification.Name("didGetForbidden")
    static let currentProjectDidChange = NSNotification.Name("currentProjectDidChange")
    
    static let pushNotificationReceivedOnLaunch = NSNotification.Name("pushNotificationReceivedOnLaunch")
    static let pushNotificationReceivedOnBackground = NSNotification.Name("pushNotificationReceivedOnBackground")
    static let pushNotificationReceivedOnForeground = NSNotification.Name("pushNotificationReceivedOnForeground")
}
