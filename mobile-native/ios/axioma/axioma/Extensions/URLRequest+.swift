//
//  URLRequest+.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 07.06.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Alamofire
import Foundation

extension URLRequest {
    
    mutating func sign() {
        if let sessionId = Session.id {
            self.setValue("application/json", forHTTPHeaderField: "Content-Type")
            self.setValue("Bearer \(sessionId)", forHTTPHeaderField: "Authorization")
        }
    }
    
    mutating func encodeParamsAndSign(_ params: [String:String]) {
        let params = params.map({ key, value in
            return self.urlencodeParam(key, value: value)
        }).joined(separator: "&")
        
        self.httpBody = params.asData()
        self.sign()
    }
    
    func urlencodeParam(_ param: String, value: String) -> String {
        let valueCharset = (CharacterSet.urlHostAllowed as NSCharacterSet).mutableCopy() as! NSMutableCharacterSet
        valueCharset.removeCharacters(in: "&") // remove & to properly encode it
        valueCharset.removeCharacters(in: "+")
        let valEnc = value.addingPercentEncoding(withAllowedCharacters: valueCharset as CharacterSet)
        return String(format: "%@=%@", param, valEnc!)
    }
}

