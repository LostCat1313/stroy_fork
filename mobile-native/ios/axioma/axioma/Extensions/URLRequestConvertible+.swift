//
//  URLRequestConvertible+.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 07.06.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Alamofire

extension URLRequestConvertible {
    
    func baseURLString() -> String {
        return Configuration.serverUrl
    }
    
}
