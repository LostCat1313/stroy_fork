//
//  ColorPickerSlider.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 01.10.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation
import UIKit


protocol ColorPickerSliderDelegate {
    func colorPickerSlider(didSelect color: UIColor)
}

open class ColorPickerSlider: UISlider {
    
    
    private static let hueImageHeight: CGFloat = 12
    var hueImageHeight: CGFloat { return ColorPickerSlider.hueImageHeight}
    
    private static let hueImageStroke: CGFloat = 1
    var hueImageStroke: CGFloat { return ColorPickerSlider.hueImageStroke}
    
    private static let sliderHeight: CGFloat = 28
    var sliderHeight: CGFloat { return ColorPickerSlider.sliderHeight}
    
    private static let sliderStroke: CGFloat = 2
    var sliderStroke: CGFloat { return ColorPickerSlider.sliderStroke}
    
    private static let bwInterval: CGFloat = 7
    var bwInterval: CGFloat { return ColorPickerSlider.bwInterval}
    
    private var hueImage: UIImage!
    
    var delegate: ColorPickerSliderDelegate?
    //open var bwBounds: CGFloat = 2
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    private func initialize() {
        backgroundColor = UIColor.clear
        minimumTrackTintColor = UIColor.clear
        maximumTrackTintColor = UIColor.clear
        addTarget(self, action: #selector(onSliderValueChange), for: UIControlEvents.valueChanged)
        setThumbImage(generateThumbImage(CGSize(width: sliderHeight, height: sliderHeight), color: currentColor), for: .normal)
        minimumTrackTintColor = UIColor.clear
        let size: CGSize = CGSize(width: frame.width, height: hueImageHeight)
        hueImage = generateHUEImage(size)
    }
    
    var currentColor: UIColor {
        get {
            switch value {
            case minimumValue:
                return UIColor.black
            case maximumValue:
                return UIColor.white
            default:
                return UIColor(hue: CGFloat(value), saturation: 1, brightness: 1, alpha: 1)
            }
        }
        set(c) {
            var hue: CGFloat = 0
            var brightness: CGFloat = 0
            c.getHue(&hue, saturation: nil, brightness: &brightness, alpha: nil)
            
            if hue == 0 {
                value = brightness == 0 ? minimumValue : maximumValue
            } else {
                value = Float(hue)
            }
            setThumbImage(generateThumbImage(CGSize(width: sliderHeight, height: sliderHeight), color: c), for: .normal)
        }
    }
    
    var textPlaceholderColor: UIColor {
        switch value {
        case maximumValue:
            return UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        default:
            return UIColor(red: 1, green: 1, blue: 1, alpha: 0.5)
        }
    }
    
    var textColor: UIColor {
        switch value {
        case maximumValue:
            return UIColor(red: 0, green: 0, blue: 0, alpha: 1)
        default:
            return UIColor(red: 1, green: 1, blue: 1, alpha: 1)
        }
    }
    
    override open func draw(_ rect: CGRect) {
        super.draw(rect)
        let sliderImageRect = CGRect(x: rect.origin.x, y: (rect.size.height - hueImageHeight) * 0.5,
                                     width: rect.width, height: hueImageHeight)
        if hueImage != nil {
            hueImage.draw(in: sliderImageRect)
        }
        
    }
    
    @objc func onSliderValueChange(slider: UISlider) {
        let col = currentColor
        slider.setThumbImage(generateThumbImage(CGSize(width: sliderHeight, height: sliderHeight), color: col), for: .normal)
        delegate?.colorPickerSlider(didSelect: col)
    }
}

fileprivate extension ColorPickerSlider {
    
    func generateThumbImage(_ size: CGSize, color: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        let r = (min(size.width, size.height) / 2) - sliderStroke
        let thumbPath = UIBezierPath(arcCenter: CGPoint(x: size.width / 2, y: size.height / 2), radius: r, startAngle: 0, endAngle: CGFloat.pi * 2, clockwise: true)
        thumbPath.lineWidth = sliderStroke
        color.setFill()
        //UIColor.white.setStroke()
        color.setStroke()
        thumbPath.fill()
        thumbPath.stroke()
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
    
    func generateHUEImage(_ size: CGSize) -> UIImage {
        
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        
        let clientRect = CGRect(origin: CGPoint(x: hueImageStroke, y: hueImageStroke),
                                size: CGSize(width: size.width - hueImageStroke * 2, height: size.height - hueImageStroke * 2))
        let cornR = size.height / 2
        
        let blackRect = CGRect(x: clientRect.origin.x, y: clientRect.origin.y, width: bwInterval, height: clientRect.height)
        let blackPath = UIBezierPath(roundedRect: blackRect, cornerRadius: cornR)
        
        let whiteRect = CGRect(x: clientRect.origin.x + clientRect.width - bwInterval, y: clientRect.origin.y,
                               width: bwInterval, height: clientRect.height)
        let whitePath = UIBezierPath(roundedRect: whiteRect, cornerRadius: cornR)
        
        UIColor.black.setFill()
        blackPath.fill()
        UIColor.white.setFill()
        whitePath.fill()
        
        for x: Int in 0..<Int(clientRect.width) {
            if x < Int(cornR) || x >= Int(clientRect.width - cornR) {
                continue
            }
            UIColor(hue: CGFloat(CGFloat(x) / size.width), saturation: 1.0, brightness: 1.0, alpha: 1.0).set()
            let bar = CGRect(x: clientRect.origin.x + CGFloat(x), y: clientRect.origin.y, width: 1, height: clientRect.height)
            UIRectFillUsingBlendMode(bar, x < Int(clientRect.width / 2) ? .saturation : .color)
        }
        
        /*
        UIColor.white.set()
        let borderPath = UIBezierPath(roundedRect: clientRect, cornerRadius: size.height / 2)
        borderPath.lineWidth = hueImageStroke
        borderPath.stroke()
        */
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
}
