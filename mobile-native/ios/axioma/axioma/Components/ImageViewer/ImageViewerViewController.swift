//
//  ImageViewerViewController.swift
//  axioma
//
//  Created by sss on 30.07.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit
import Kingfisher
import AVFoundation

class ImageViewerViewController: UIViewController {
    var srcImage: UIImage?
    var srcUrl: URL?
    var minimalSize = CGSize.zero
    
    @IBOutlet weak var imgPanel: UIView!
    @IBOutlet weak var image: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        if let img = srcImage {
            image.image = img
            self.minimalSize = AVMakeRect(aspectRatio: self.image.image!.size, insideRect: self.image.bounds).size
        } else if let url = srcUrl {
            image.kf.setImage(with: url, placeholder: nil,
                     options: [.transition(ImageTransition.fade(1)), .requestModifier(imageRequestModifier)],
                     completionHandler: { _, _, _, _ in
                        self.minimalSize = AVMakeRect(aspectRatio: self.image.image!.size, insideRect: self.image.bounds).size
                    }
            
            )
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func onBack(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    static func show(_ over: UIViewController, image: UIImage) {
        let controller = ImageViewerViewController()
        //controller.modalPresentationStyle = .overCurrentContext
        controller.srcImage = image
        over.present(controller, animated: true)
    }
    static func show(_ over: UIViewController, url: URL) {
        let controller = ImageViewerViewController()
        //controller.modalPresentationStyle = .overCurrentContext
        controller.srcUrl = url
        over.present(controller, animated: true)
    }
  
    @IBAction func onPinch(_ sender: UIPinchGestureRecognizer) {
        switch sender.state {
            case .changed:
                
                let t = image.transform
                image.transform = image.transform.concatenating(CGAffineTransform(scaleX: sender.scale, y: sender.scale))
                if image.frame.width < minimalSize.width || image.frame.height < minimalSize.height {
                    UIView.animate(withDuration: 0.4, delay: 0, options: .curveEaseOut, animations: {
                        self.image.transform = t
                    }, completion: { _ in
                    
                    })
                }
                sender.scale = 1
            default: break
        }
    }
    
    @IBAction func onPan(_ sender: UIPanGestureRecognizer) {
        switch sender.state {
            case .changed:
                let d = sender.translation(in: self.imgPanel)
                image.transform =
                    image.transform.concatenating(CGAffineTransform(translationX: d.x, y: d.y))
                sender.setTranslation(CGPoint.zero, in: self.imgPanel)
            default: break
        }
    }
}


extension ImageViewerViewController :  UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }

}
