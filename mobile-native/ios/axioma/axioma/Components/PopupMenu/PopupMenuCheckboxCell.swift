//
//  PopupMenuCheckboxCell.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 05.07.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit

protocol PopUpMenuCheckboxCellDelegate {
    func popUpMenuCheckboxCell(onTap cell: PopupMenuCheckboxCell)
}

class PopupMenuCheckboxCell: UICollectionViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var checkBoxImage: UIImageView!
    var delegate: PopUpMenuCheckboxCellDelegate!
    var itemData: CheckBoxMenuItem!
    
    var checked: Bool = false {
        didSet {
            itemData.checked = checked
            if checked {
                checkBoxImage.image = UIImage(named: "checkbox-intermediate")
            } else {
                checkBoxImage.image = UIImage(named: "checkbox-blank-outline")
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let grTap = UITapGestureRecognizer(target: self, action: #selector(self.onTap(_:)))
        self.addGestureRecognizer(grTap)
    }

    func configure(_ itemData: CheckBoxMenuItem) {
        self.itemData = itemData
        titleLabel.text = itemData.title
        self.checked = itemData.checked
    }
    
    @objc func onTap(_ sender: UITapGestureRecognizer) {
        self.checked = !self.checked
        delegate.popUpMenuCheckboxCell(onTap: self)
    }
}
