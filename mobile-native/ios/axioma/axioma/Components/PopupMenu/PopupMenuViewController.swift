//
//  PopupMenuViewController.swift
//  axioma
//
//  Created by sss on 04.07.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit

protocol PopupMenuItemProtocol {
    var title: String {get}
    var hasIcon: Bool {get}
    var icon: UIImage? {get}
    var enabled: Bool {get}
    
}

class PopupMenuItem: PopupMenuItemProtocol {
    var title: String
    var click: (_ action: PopupMenuItem)->()
    var icon: UIImage?
    var hasIcon: Bool {
        return icon != nil
    }
    var enabled: Bool
    
    init(title: String, icon: UIImage? = nil, enabled: Bool = true, click: @escaping (_ action: PopupMenuItem)->()) {
        self.title = title
        self.click = click
        self.icon = icon
        self.enabled = enabled
    }
}

class CheckBoxMenuItem: PopupMenuItemProtocol {
    var title: String
    var hasIcon: Bool = true
    var icon: UIImage?
    var checked: Bool
    var changed: (_ item: CheckBoxMenuItem)->()
    var enabled: Bool
    
    init(title: String, checked: Bool, enabled: Bool = true, changed: @escaping (_ item: CheckBoxMenuItem)->()) {
        self.title = title
        self.checked = checked
        self.changed = changed
        self.enabled = enabled
    }
}
class PopupMenuStyle {
    var bkColor = Configuration.Colors.dark
}
class PopupMenuViewController: UIViewController {
    
    static let cellFont: UIFont = UIFont.systemFont(ofSize: 20)
    var cellFont: UIFont {
        return PopupMenuViewController.cellFont
    }
    static let cellMargin: CGSize = CGSize(width: 30, height: 20)
    var cellMargin: CGSize {
        return PopupMenuViewController.cellMargin
    }
    
    static let cellWithIconMargin: CGSize = CGSize(width: 60, height: 20)
    var cellWithIconMargin: CGSize {
        return PopupMenuViewController.cellWithIconMargin
    }
    
    static let menuMax: CGSize = CGSize(width: 500, height: 300)
    var menuMax: CGSize {
        return PopupMenuViewController.menuMax
    }
    
    var style = PopupMenuStyle()
    var cellSize = CGSize.zero
    
    @IBOutlet weak var menuFrame: UIView!
    @IBOutlet weak var cvMenu: UICollectionView!
    var at: CGPoint = CGPoint.zero
    private var menuItems: [PopupMenuItemProtocol] = []
    var owner: UIViewController?
    var cancel: (()->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        cvMenu.register(UINib(nibName: "PopupMenuItemCell", bundle: nil), forCellWithReuseIdentifier: "PopupMenuItemCell")
        cvMenu.register(UINib(nibName: "PopupMenuCheckboxCell", bundle: nil), forCellWithReuseIdentifier: "PopupMenuCheckboxCell")
        initialize()
    }
    
    private func initialize() {
        menuFrame.translatesAutoresizingMaskIntoConstraints = true
        
        cellSize = menuItems.reduce(CGSize.zero) { acc, item in
            let sz = (item.title as NSString).size(withAttributes: [NSAttributedStringKey.font: PopupMenuViewController.cellFont])
            let mr = item.hasIcon ? cellWithIconMargin : cellMargin
            return CGSize(width: max(sz.width + mr.width, acc.width),
                          height: max(sz.height + mr.height, acc.height))
        }
        var needH = CGFloat(menuItems.count) * cellSize.height
        if needH <= menuMax.height {
            cvMenu.isScrollEnabled = false
        } else {
            needH = menuMax.height
            cvMenu.isScrollEnabled = true
        }
        let needW = min(menuMax.width, cellSize.width)
        
        let dX = owner!.view.frame.width - (at.x + needW)
        let dY = owner!.view.frame.height - (at.y + needH)
        
        let o = CGPoint(x: at.x + (dX < 0 ? dX : 0), y: at.y - (dY < 0 ? needH : 0))
        menuFrame.frame = CGRect(origin: o, size: CGSize(width: needW, height: needH))
        menuFrame.backgroundColor = style.bkColor
    }
    @IBAction func onTapBackground(_ sender: UITapGestureRecognizer) {
        dismiss(animated: false, completion: cancel)
    }
    
    private func onActionClick(menuItem: PopupMenuItem) {
        dismiss(animated: false, completion: {menuItem.click(menuItem)})
    }
    
    static func popup(over: UIViewController, at: CGPoint, menuItems: [PopupMenuItemProtocol], style: PopupMenuStyle? = nil, cancel: (()->())? = nil) {
        let controller = PopupMenuViewController()
        controller.modalPresentationStyle = .overCurrentContext
        controller.owner = over
        controller.at = at
        controller.menuItems = menuItems
        controller.cancel = cancel
        if let s = style {
            controller.style = s
        }
        over.present(controller, animated: false)
    }
}

extension PopupMenuViewController: PopupMenuItemCellDelegate {
    func popupMenuItemCell(onTap cell: PopupMenuItemCell) {
        dismiss(animated: false, completion: { cell.itemData.click(cell.itemData)})
    }
}

extension PopupMenuViewController: PopUpMenuCheckboxCellDelegate {
    func popUpMenuCheckboxCell(onTap cell: PopupMenuCheckboxCell) {
        cell.itemData.changed(cell.itemData)
    }
}

extension PopupMenuViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return menuItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell:UICollectionViewCell?
        if let item = menuItems[indexPath.item] as? PopupMenuItem {
            let itemCell: PopupMenuItemCell = cvMenu.dequeueReusableCell(withReuseIdentifier: "PopupMenuItemCell", for: indexPath) as! PopupMenuItemCell
            itemCell.configure(item)
            itemCell.delegate = self
            cell = itemCell
        } else {
            let item = menuItems[indexPath.item] as! CheckBoxMenuItem
            let itemCell = cvMenu.dequeueReusableCell(withReuseIdentifier: "PopupMenuCheckboxCell", for: indexPath) as! PopupMenuCheckboxCell
            itemCell.configure(item)
            itemCell.delegate = self
            cell = itemCell
        }
        cell?.backgroundColor = style.bkColor
        return cell!
    }
}

extension PopupMenuViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return cellSize
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
}
