//
//  PopupMenuItemCell.swift
//  axioma
//
//  Created by sss on 03.07.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit

protocol PopupMenuItemCellDelegate {
    func popupMenuItemCell(onTap cell: PopupMenuItemCell)
}

class PopupMenuItemCell: UICollectionViewCell {

    @IBOutlet weak var btnAction: UIButton!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var leftPosBtnAction: NSLayoutConstraint!
    @IBOutlet weak var viewDelimiter: UIView!
    var delegate: PopupMenuItemCellDelegate!
    var itemData: PopupMenuItem!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(_ itemData: PopupMenuItem) {
        if itemData.title == "-" {
            btnAction.isHidden = true
            icon.isHidden = true
            viewDelimiter.isHidden = false
        } else {
            viewDelimiter.isHidden = true
            if itemData.hasIcon {
                icon.image = itemData.icon
                icon.isHidden = false
                btnAction.contentEdgeInsets.left = icon.frame.origin.x * 2 + icon.frame.width
            } else {
                icon.image = nil
                icon.isHidden = true
                btnAction.contentEdgeInsets.left = 10
            }
            btnAction.isHidden = false
            self.itemData = itemData
            btnAction.titleLabel?.font = PopupMenuViewController.cellFont
            btnAction.setTitle(itemData.title, for: .normal)
           
            if itemData.enabled {
                icon.alpha = 1.0
                btnAction.alpha = 1.0
                btnAction.isEnabled = true
            } else {
                icon.alpha = 0.3
                btnAction.alpha = 0.3
                btnAction.isEnabled = false
            }
        }
    }
    
    @IBAction func onBtnAction(_ sender: UIButton) {
        delegate.popupMenuItemCell(onTap: self)
    }
    

}
