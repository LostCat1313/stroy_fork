//
//  UsersPickerViewController.swift
//  axioma
//
//  Created by sss on 30/10/2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit

protocol UsersPickerCellDelegate : class {
    func usersPicker(click userId: Int) -> (user: User, caption: String, state: UserPickerItemState)?
    func usersPicker(changeRole userId: Int,
                     completion: @escaping (_ info: (user: User, caption: String, state: UserPickerItemState)) -> ())
}

protocol UsersPickerControllerDelegate : class {
    func usersPickerController(getCaptionFor picker: UsersPickerViewController) -> String
    func usersPickerController(getApplyCaptionFor picker: UsersPickerViewController) -> String
    
    
    func usersPickerController(apply users: [(user: User, caption: String, state: UserPickerItemState)])
    
    //Current user can change
    func usersPickerController(canChangeFor picker: UsersPickerViewController) -> Bool
    
    //Only one can be selected
    func usersPickerController(isRadioFor picker: UsersPickerViewController) -> Bool
    
    
}

enum UserPickerItemRoleMode {
    case disabled // hides role selector!!
    case enabled(_ role: Rights.Role)
}


enum UserPickerItemState {
    case included(readOnly: Bool, roleMode: UserPickerItemRoleMode)
    case excluded(readOnly: Bool, roleMode: UserPickerItemRoleMode)
}

class UsersPickerViewController: UIViewController {
    weak var delegate: UsersPickerControllerDelegate?
    var users: [(user: User, caption: String, state: UserPickerItemState)] = []
    var caption: String?
    
    
    @IBOutlet weak var labelCaption: UILabel!
    @IBOutlet weak var cvUsers: UICollectionView!
    @IBOutlet weak var btnApply: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cvUsers.register(UINib(nibName: "UserViewCell", bundle: nil), forCellWithReuseIdentifier: "UserViewCell")
        
        labelCaption.text = delegate?.usersPickerController(getCaptionFor: self) ?? ""
        
        if delegate?.usersPickerController(canChangeFor: self) ?? false {
            btnApply.isEnabled = true
            btnApply.alpha = 1
        } else {
            btnApply.isEnabled = false
            btnApply.alpha = 0.4
        }
        
        btnApply.setTitle(delegate?.usersPickerController(getApplyCaptionFor: self) ?? "ПРИМЕНИТЬ" , for: .normal)
        cvUsers.reloadData()
    }

    static func show(_ over: UIViewController, delegate: UsersPickerControllerDelegate, users: [(user: User, caption: String, state: UserPickerItemState)]) {
        let controller = UsersPickerViewController()
        controller.users = users
        controller.delegate = delegate
        controller.modalPresentationStyle = .overCurrentContext
        over.present(controller, animated: false)
    }

    @IBAction func onBtnApply(_ sender: UIButton) {
        let d = delegate
        let u = users
        self.dismiss(animated: false) {
            d?.usersPickerController(apply: u)
        }
    }
    
    @IBAction func onBtnCancel(_ sender: UIButton) {
        self.dismiss(animated: false) {
        }
    }
}

extension UsersPickerViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return users.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: UserViewCell = cvUsers.dequeueReusableCell(withReuseIdentifier: "UserViewCell", for: indexPath) as! UserViewCell
        let item = users[indexPath.item]
        cell.delegate = self
        cell.configure(item, isRadio: delegate?.usersPickerController(isRadioFor: self) ?? false)
        return cell
    }
}

extension UsersPickerViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: cvUsers.frame.width, height: 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
}

extension UsersPickerViewController : UsersPickerCellDelegate {
    func usersPicker(changeRole userId: Int, completion: @escaping ((user: User, caption: String, state: UserPickerItemState)) -> ()) {
        if let idx = users.firstIndex(where: {$0.user.id == userId}) {
            UserRolesPickerController.show(self) { res in
                switch res {
                    case .selected(let role):
                        self.users[idx].state = .included(readOnly: false, roleMode: .enabled(role))
                        completion(self.users[idx])
                    default:
                        break
                }
            }
        }
    }
    
    func usersPicker(click userId: Int) -> (user: User, caption: String, state: UserPickerItemState)? {
        
        func onCheckBoxesClick(idx: Int) -> (user: User, caption: String, state: UserPickerItemState)?  {
            switch users[idx].state {
                case .excluded(let readOnly, let roleMode):
                    if !readOnly {
                        users[idx].state = .included(readOnly: false, roleMode: roleMode)
                    }
                    
                case .included(let readOnly, let roleMode):
                    if !readOnly {
                        users[idx].state = .excluded(readOnly: false, roleMode: roleMode)
                    }
            }
            return users[idx]
        }
        
        func onRadioBoxesClick(idx: Int) -> (user: User, caption: String, state: UserPickerItemState)?  {
            var reloadIdx: [Int] = []
            
            switch users[idx].state {
                case .excluded(let readOnly, let roleMode):
                    if !readOnly {
                        users = users.enumerated().map { idx2, item in
                            switch item.state {
                                case .excluded(let readOnly, let roleMode):
                                    if idx == idx2 {
                                        return (user: item.user, caption: item.caption, state: .included(readOnly: readOnly, roleMode: roleMode))
                                    } else {
                                        return item
                                    }
                                case .included(let readOnly, let roleMode):
                                    if !readOnly {
                                        reloadIdx.append(idx2)
                                        return (user: item.user, caption: item.caption, state: .excluded(readOnly: readOnly, roleMode: roleMode))
                                    } else {
                                        return item
                                    }
                            }
                        }
                        users[idx].state = .included(readOnly: false, roleMode: roleMode)
                    }
                    
                default:
                    break

            }
            cvUsers.reloadItems(at: reloadIdx.map {IndexPath(item: $0, section: 0)})
            return users[idx]
        }
        
        
        if let idx = users.firstIndex(where: {$0.user.id == userId}) {
            if delegate?.usersPickerController(isRadioFor: self) ?? false {
                return onRadioBoxesClick(idx: idx)
            } else {
                return onCheckBoxesClick(idx: idx)
            }
        } else {
            return nil
        }
    }
    

}
