//
//  UserViewCell.swift
//  axioma
//
//  Created by sss on 30/10/2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit

class UserViewCell: UICollectionViewCell {
    weak var delegate: UsersPickerCellDelegate?
    var userId: Int = 0
    var isRadio = false
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var btnUserName: UIButton!
    @IBOutlet weak var btnUserRole: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func configure(_ item: (user: User, caption: String, state: UserPickerItemState), isRadio: Bool) {
        self.isRadio = isRadio
        userId = item.user.id
        btnUserName.setTitle(item.caption, for: .normal)
        
        switch item.state {
            case .included(let readOnly, let roleState):
                imgView.image = UIImage(named: isRadio ? "radiobox-marked" : "checkbox-marked")
                switch roleState {
                    case .disabled:
                        btnUserRole.isHidden = true
                    
                    case .enabled(let role):
                        btnUserRole.isHidden = false
                        btnUserRole.setTitle(role.caption, for: .normal)
                        btnUserRole.isEnabled = !readOnly
                }
                alpha = readOnly ? 0.4 : 1.0
            
            case .excluded(let readOnly, _):
                btnUserRole.isHidden = true
                imgView.image = UIImage(named:  isRadio ? "radiobox-blank" : "checkbox-blank-outline-black")
                alpha = readOnly ? 0.4 : 1.0
        }

    }
    @IBAction func onBtnUserNameClick(_ sender: UIButton) {
        if let item = delegate?.usersPicker(click: userId) {
            configure(item, isRadio: self.isRadio)
        }
    }
    @IBAction func onBtnUserRoleClick(_ sender: Any) {
        delegate?.usersPicker(changeRole: userId) { info in
            self.configure(info, isRadio: self.isRadio)
        }
    }
}
