//
//  ProgressViewController.swift
//  axioma
//
//  Created by sss on 16.08.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit

class ProgressViewController: UIViewController {
    
    @IBOutlet weak var lCaption: UILabel!
    @IBOutlet weak var lProgress: UILabel!
    @IBOutlet weak var fullProgressView: UIView!
    @IBOutlet weak var currProgressWidth: NSLayoutConstraint!
    
    private var __progress: Float = 0
    var progress: Float {
        get {
            return __progress
        }
        set (v) {
            __progress = v
            refresh()
        }
    }
    
    private var __caption: String = ""
    var caption: String {
        get {
            return __caption
        }
        set (v) {
            __caption = v
            refresh()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refresh()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    private func refresh() {
        if lCaption != nil {
            lCaption.text = __caption
            currProgressWidth.constant = fullProgressView.frame.width * CGFloat(max(0, min(1.0, __progress)))
            lProgress.text = String(format: "%.1f", __progress * 100)
        }
    }

    static func show(over: UIViewController, caption: String, progress: Float) -> ProgressViewController {
        let controller = ProgressViewController()
        controller.modalPresentationStyle = .overCurrentContext
        controller.caption = caption
        controller.progress = progress
        over.present(controller, animated: false)
        return controller
    }
}
