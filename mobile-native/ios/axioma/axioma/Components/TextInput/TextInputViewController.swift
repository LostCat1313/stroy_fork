//
//  TextInputViewController.swift
//  axioma
//
//  Created by sss on 10.07.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit

class TextInputViewController: UIViewController {
    private var text: String?
    private var owner: UIViewController?
    private var onText:((_ text: String)->())?
    private var onCancel:(()->())?
    @IBOutlet weak var btnOK: UIButton!
    @IBOutlet weak var textEdit: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Configuration.Colors.transparentDark
        textEdit.text = text
        refreshUI()
        textEdit.becomeFirstResponder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    static func popup(over: UIViewController, text: String? = nil, onText:@escaping (_ text: String)->(), cancel:(()->())? = nil) {
        let controller = TextInputViewController()
        controller.modalPresentationStyle = .overCurrentContext
        controller.owner = over
        controller.text = text
        controller.onText = onText
        controller.onCancel = cancel
        over.present(controller, animated: false)
    }

    @IBAction func onBackTap(_ sender: UITapGestureRecognizer) {
         dismiss(animated: false, completion: onCancel)
    }
    @IBAction func onCancel(_ sender: Any) {
        dismiss(animated: false, completion: onCancel)
    }
    @IBAction func onBtnOK(_ sender: Any) {
        dismiss(animated: false, completion: {
            self.onText?(self.textEdit.text ?? "")
        })
    }
    
    
    func refreshUI() {
        if (textEdit.text?.count ?? 0) > 0 && textEdit.text != text {
            btnOK.isEnabled = true
            btnOK.alpha = 1
        } else {
            btnOK.isEnabled = false
            btnOK.alpha = 0.3
        }
    }
    
    @IBAction func onTextFieldChanged(_ sender: UITextField) {
        refreshUI() 
    }
   
}

extension TextInputViewController : UITextFieldDelegate {

}
