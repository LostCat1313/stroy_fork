//
//  UserRolesPickerController.swift
//  axioma
//
//  Created by sss on 22/11/2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit

enum UserRolesPickerResult {
    case cancel
    case selected(_ role: Rights.Role)
}

class UserRolesPickerController: UIViewController {
    var completion: ((_ res: UserRolesPickerResult) -> ())?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    static func show(_ over: UIViewController, completion: @escaping (_ res: UserRolesPickerResult) -> ()) {
        let controller = UserRolesPickerController()
        controller.completion = completion
        controller.modalPresentationStyle = .overCurrentContext
        over.present(controller, animated: false)
    }

    @IBAction func onTap(_ sender: Any) {
        dismiss(animated: false) {
            self.completion?(.cancel)
        }
    }
    @IBAction func onBtnReader(_ sender: UIButton) {
        dismiss(animated: false) {
            self.completion?(.selected(.reader))
        }
    }
    @IBAction func onBtnWriter(_ sender: UIButton) {
         dismiss(animated: false) {
            self.completion?(.selected(.writer))
        }
    }
    @IBAction func onBtnAdmin(_ sender: UIButton) {
        dismiss(animated: false) {
            self.completion?(.selected(.admin))
        }
    }
}
