//
//  ProjectNotesViewController.swift
//  axioma
//
//  Created by sss on 26/10/2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit
protocol ProjectNotesDelegate {
    func projectNotes(pick: PageMarkerNote)
}
class ProjectNotesViewController: UIViewController {
    var project: ProjectData!
    var completion: (()->())?
    var model: ProjectModel.View?
    var notes: [PageMarkerNote] = []
    
    @IBOutlet weak var spinnerView: UIView!
    @IBOutlet weak var cvNotes: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        cvNotes.register(UINib(nibName: "ProjectNoteViewCell", bundle: nil), forCellWithReuseIdentifier: "ProjectNoteViewCell")
        refresh()
    }
    
    private func refresh() {
        showSpinner()
        let finish: (_ error: Error?) -> () = { error in
            self.hideSpinner()
            if let e = error {
                self.displayError(e)
            } else {
                self.cvNotes.reloadData()
            }
        }
        
        let loadMarkers = {
            DocumentAPI.loadProjectMarkers(projectId: self.project.id, type: .note) { error, markers in
                if error == nil {
                    let notes: [PageMarkerNote] = markers!.reduce(into: []) { acc, item in
                        if let m = item as? PageMarkerNote {
                            acc.append(m)
                        }
                    }
                    self.notes = notes
                }
                finish(error)
            }
        }
        
        DocumentAPI.projectDocuments(projectId: project.id) { error, documents in
            if error == nil {
                self.model = ProjectModel.View(project: self.project, documents: documents!)
                loadMarkers()
            } else {
                finish(error)
            }
        }
    }
    
    static public func show(_ over: UIViewController, project: ProjectData, completion: (()->())? = nil) {
        let controller = ProjectNotesViewController()
        controller.project = project
        controller.completion = completion
        over.present(controller, animated: true)
           
    }
    
    func displayError(_ error: Error) {
        let description = error.localizedDescription
        let dlg = Session.makeErrorDialog("Error", message: description)
        self.present(dlg, animated: true)
    }
    
    
    @IBAction func onBtnClose(_ sender: UIButton) {
        self.dismiss(animated: true, completion: completion)
    }
}

//SPINNER
extension ProjectNotesViewController {
    func showSpinner() {
        spinnerView.isHidden = false
    }
    
    func hideSpinner() {
        spinnerView.isHidden = true
    }
}

extension ProjectNotesViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return notes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: ProjectNoteViewCell = cvNotes.dequeueReusableCell(withReuseIdentifier: "ProjectNoteViewCell", for: indexPath) as! ProjectNoteViewCell
        cell.configure(notes[indexPath.item])
        cell.delegate = self
        return cell
    }
}

extension ProjectNotesViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: cvNotes.frame.width, height: 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
}

extension ProjectNotesViewController : ProjectNotesDelegate {
    func projectNotes(pick: PageMarkerNote) {
        if let d = model?.findDocument(docId: pick.ownerDocId), let p = d.findPage(pageId: pick.ownerPageId) {
        
        PageAnnotatesViewController.show(self,
            project: model!, doc: d,
            activePage: p, activeMarker: pick) { _ in
            }
        }
        
    }
}
