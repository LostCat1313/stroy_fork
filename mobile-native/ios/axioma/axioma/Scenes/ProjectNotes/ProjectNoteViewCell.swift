//
//  ProjectNoteViewCell.swift
//  axioma
//
//  Created by sss on 26/10/2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit

class ProjectNoteViewCell: UICollectionViewCell {
    var note: PageMarkerNote!
    var delegate: ProjectNotesDelegate?
    
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var btnNote: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure(_ note: PageMarkerNote) {
        self.note = note
        btnNote.setTitle(note.note, for: .normal)
        imgIcon.image = UIImage(named: note.isDraft ? "note_chev_gray" : "note_chev")
    }

    @IBAction func onBtnNote(_ sender: UIButton) {
        delegate?.projectNotes(pick: note)
    }
}
