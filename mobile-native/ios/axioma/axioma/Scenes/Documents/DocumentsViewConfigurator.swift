//
//  DocumentsViewConfigurator.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 20.06.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation
import UIKit

// MARK: Connect View, Interactor, and Presenter

extension DocumentsViewInteractor: DocumentsViewControllerOutput {
}

extension DocumentsViewPresenter: DocumentsViewInteractorOutput {
}

class DocumentsViewConfigurator {
    // MARK: Object lifecycle
    static let sharedInstance = DocumentsViewConfigurator()
    
    // MARK: Configuration
    func configure(viewController: DocumentsViewController) {
        let router = DocumentsViewRouter()
        router.viewController = viewController
        
        let presenter = DocumentsViewPresenter()
        presenter.output = viewController
        
        let interactor = DocumentsViewInteractor()
        interactor.output = presenter
        //interactor.worker = DocumentsViewWorker()
        
        viewController.output = interactor
        viewController.router = router
    }
}
