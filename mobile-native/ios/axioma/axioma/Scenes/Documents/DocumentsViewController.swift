//
//  DocumentsViewController.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 20.06.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit
enum DocumentsViewState {
    case view, newDirectory, newDocument, newVersion
    case menu( _ cell: DocumentsItem)
}

protocol DocumentsViewControllerInput {
    func displayError(_ error: Error)
}

protocol DocumentsViewControllerOutput {
    func getDocuments(request: ProjectModel.Request)
    
}

extension DocumentsViewController: DocumentsViewPresenterOutput {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        router.passDataToNextScene(segue: segue)
    }
    
    func displayDocuments(viewModel: ProjectModel.View) {
        projectModel = viewModel
        refreshDocs()
        hideSpinner()
    }
    
    func displayError(_ error: Error) {
        var description = error.localizedDescription
        
        if let err = error as? ProjectModel.Errors {
            switch err {
            case .serverError(let msg):
                description = msg
            }
        }
        
        let dlg = Session.makeErrorDialog("Error", message: description)
        self.present(dlg, animated: true)
    }
}

class DocumentsViewController: UIViewController {

    // VIP
    var output: DocumentsViewControllerOutput!
    var router: DocumentsViewRouter!
    
    @IBOutlet weak var cvDocuments: UICollectionView!
    @IBOutlet weak var btnPlus: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var spinnerView: UIView!
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var menuView: UIView!
    
    var folderStates: [Int: Bool] = [:]
    
    var projectModel: ProjectModel.View?
    var active: [DocumentsItem] = []
    
    
    var state: DocumentsViewState = .view {
        didSet {
            switch state {
                
                case .view:
                    btnPlus.isHidden = false
                    btnBack.setImage(UIImage(named:"arrow-left"), for: .normal)
                    menuView.isHidden = true
                    labelTitle.text = "Документы"
                
                case .newDirectory:
                    labelTitle.text = "Выбери место каталога"
                    btnPlus.isHidden = true
                    menuView.isHidden = true
                    btnBack.setImage(UIImage(named:"close"), for: .normal)
                        
                case .newDocument:
                    labelTitle.text = "Выбери каталог документа"
                    btnPlus.isHidden = true
                    menuView.isHidden = true
                    btnBack.setImage(UIImage(named:"close"), for: .normal)
                
                case .newVersion:
                    labelTitle.text = "Выбери изменяемый документ"
                    btnPlus.isHidden = true
                    menuView.isHidden = true
                    btnBack.setImage(UIImage(named:"close"), for: .normal)
                
                case .menu(_):
                    btnPlus.isHidden = true
                    btnBack.setImage(UIImage(named:"arrow-left"), for: .normal)
                    menuView.isHidden = false
            }
            refreshDocs()
        }
    }

    func refreshDocs() {
        func collectDocs(acc: inout [DocumentsItem], doc: DocumentsItem, depth: Int) {
            doc.depth = depth
            if let dir = doc as? DocumentsDir {
                dir.expanded = folderStates[dir.doc.id] ?? dir.expanded
            }
            acc.append(doc)
            if let dir = doc as? DocumentsDir, dir.expanded {
                for subdoc in dir.children {
                    collectDocs(acc: &acc, doc: subdoc, depth: depth + 1)
                }
            }
        }
        let model: [DocumentsItem] = projectModel!.documents.reduce(into: []) { acc, doc in
            acc.append(DocumentsItem.create(doc, delegate: self))
        }
        active = []
        switch state {
            case .newDirectory:
                let stubDoc = Document(title: "Каталог проекта", isFolder: true)
                let cellDoc = DocumentsDir(stubDoc, delegate: self)
                active.insert(cellDoc, at: 0)
       
        default:
                break
        }
        for doc in model {
            collectDocs(acc: &active, doc: doc, depth: 0)
        }
        cvDocuments.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        cvDocuments.register(UINib(nibName: "DocumentsItemCell", bundle: nil), forCellWithReuseIdentifier: "DocumentsItemCell")
        DocumentsViewConfigurator.sharedInstance.configure(viewController: self)
        
        if let project = Session.currentProject {
            let request = ProjectModel.Request(project: project)
            self.output.getDocuments(request: request)
            showSpinner()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    @IBAction func onBack(_ sender: Any) {
        switch state {
            case .view: self.dismiss(animated: true, completion: nil)
            default:
                state = .view
        }
    }
    @IBAction func onBtnPlus(_ sender: UIButton) {
        let isWriter = self.projectModel?.project.isCurrentWriter ?? false
        let actions = [
            PopupMenuItem(title: "НОВЫЙ КАТАЛОГ", enabled: isWriter, click: { a in
                self.state = .newDirectory
            }),
            PopupMenuItem(title: "НОВЫЙ ДОКУМЕНТ", enabled: isWriter, click: { a in
                self.state = .newDocument
            }),
            
            PopupMenuItem(title: "ИЗМЕНЕННЫЙ ДОКУМЕНТ", enabled: isWriter, click: { a in
                self.state = .newVersion
            }),
        ];
        let at = btnPlus.frame.origin
        //let at2 = view.convert(at, from: topBar)
        let style = PopupMenuStyle()
        style.bkColor = Configuration.Colors.orange
        PopupMenuViewController.popup(over: self, at: at, menuItems: actions, style: style)
    }
    
    @IBAction func onBtnRename(_ sender: Any) {
        switch state {
            case .menu(let cell):
                let onText: (_ text: String)->() = { text in
                    self.showSpinner()
                    cell.doc.title = text
                    DocumentAPI.updateDocument(cell.doc) { _, error in
                        self.hideSpinner()
                        if let e = error {
                            self.displayError(e)
                        } else {
                            let request = ProjectModel.Request(project: self.projectModel!.project)
                            self.output.getDocuments(request: request)
                        }
                        self.state = .view
                    }
                }
                let cancel = {
                    self.state = .view
                }
                
                TextInputViewController.popup(over: self, text: cell.doc.title, onText: onText, cancel: cancel)
            
            default:
                state = .view
        }
    
    }
    
    @IBAction func onBtnDelete(_ sender: Any) {
        switch state {
            case .menu(let cell):
                let alert = UIAlertController(title: "Удаление \(cell.doc.title)",
                    message: "Вы действительно уверены ?", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "НЕТ", style: .default, handler: { _ in
                    self.state = .view
                }))
                alert.addAction(UIAlertAction(title: "ДА", style: .destructive, handler: { _ in
                    DocumentAPI.deleteDocument(id: cell.doc.id) { error in
                        if let e = error {
                            self.displayError(e)
                        } else {
                            let request = ProjectModel.Request(project: self.projectModel!.project)
                            self.output.getDocuments(request: request)
                        }
                        self.state = .view
                    }
                }))
                present(alert, animated: true, completion: nil)
            default:
                state = .view
        }
    }
    
    @IBAction func onTapMenuView(_ sender: UITapGestureRecognizer) {
        state = .view
    }
    
    func getCellParents(cell: DocumentsItem) -> [Document] {
        var d = cell.doc
        var parents: [Document] = [d]
        let roots: [DocumentsItem] = projectModel!.documents.reduce(into: []) { acc, doc in
            acc.append(DocumentsItem.create(doc, delegate: self))
        }
        while let pid = d.parentId, pid > 0 {
            if let parentD = roots.first(where: {$0.doc.id == pid}) {
                parents.insert(parentD.doc, at: 0)
                d = parentD.doc
            } else {
                fatalError("WTF")
            }
        }
        return parents
    }
}

extension DocumentsViewController: DocumentsCellDelegate {
    func documentsCell(didTap cell: DocumentsDoc) {
        switch state {
            case .view:
                DocumentViewController.show(self, project: projectModel!, doc: cell.doc)
            case .newVersion:
                DispatchQueue.main.async {self.state = .view}
                let path = getCellParents(cell: cell).map {$0.title}.joined(separator: "/")
                ChangeDocumentViewController.show(over: self, doc: cell.doc, path: path) {
                    self.state = .view
                    let request = ProjectModel.Request(project: self.projectModel!.project)
                    self.output.getDocuments(request: request)
                }
            
            default:
                break
        }
    }
    
    func documentsCell(didTap cell: DocumentsDir, isExpandTap: Bool) {
        if isExpandTap {
            folderStates[cell.doc.id] = !cell.expanded
            refreshDocs()
        } else {
            switch state {
                case .newDirectory:
                    TextInputViewController.popup(over: self, onText: { text in
                        DocumentAPI.createFolder(projectId: self.projectModel!.project.id, parentId: cell.doc.id, title: text) {
                            [weak self] error in
                                if let e = error {
                                    self?.displayError(e)
                                    self?.state = .view
                                } else {
                                    self?.state = .view
                                    self?.output.getDocuments(request: ProjectModel.Request(project: self!.projectModel!.project))
                                }
                        }
                    })
                case .newDocument:
                    let path = getCellParents(cell: cell).map {$0.title}.joined(separator: "/")
                    DispatchQueue.main.async {self.state = .view}
                    CreateDocumentViewController.show(over: self, parent: cell.doc, path: path) {
                        if let project = Session.currentProject {
                            let request = ProjectModel.Request(project: project)
                            self.output.getDocuments(request: request)
                        }
                    }
                
                default:
                    break
            }
        }
    }
    
    func documentsCell(didPress cell: DocumentsItem) {
        switch state {
            case .view:
                let isWriter = projectModel?.project.isCurrentWriter ?? false
                if isWriter {
                    state = .menu(cell)
                }
            default:
                break
        }
    }
}


extension DocumentsViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return active.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: DocumentsItemCell = cvDocuments.dequeueReusableCell(withReuseIdentifier: "DocumentsItemCell", for: indexPath) as! DocumentsItemCell
        
        cell.configure(cell: active[indexPath.item])
        return cell
    }
}

extension DocumentsViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: cvDocuments.frame.width, height: 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
}

//SPINNER
extension DocumentsViewController {
    func showSpinner() {
        spinnerView.isHidden = false
    }
    
    func hideSpinner() {
        spinnerView.isHidden = true
    }
    
}

