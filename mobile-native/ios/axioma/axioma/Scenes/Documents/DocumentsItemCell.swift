//
//  DocumentsItemCell.swift
//  axioma
//
//  Created by sss on 21.06.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit
protocol DocumentsCellDelegate {
    func documentsCell(didTap doc: DocumentsDoc)
    func documentsCell(didTap dir: DocumentsDir, isExpandTap: Bool)
    func documentsCell(didPress item: DocumentsItem)
}

class DocumentsItem {
    var depth = 0
    let doc: Document
    var delegate: DocumentsCellDelegate
    init(_ doc: Document, delegate: DocumentsCellDelegate) {
        self.doc = doc
        self.delegate = delegate
    }
    static func create(_ doc: Document, delegate: DocumentsCellDelegate) -> DocumentsItem {
        return doc.isFolder ? DocumentsDir(doc, delegate: delegate)
                            : DocumentsDoc(doc, delegate: delegate)
    }
}

class DocumentsDir : DocumentsItem {
    var expanded: Bool = false
    var children: [DocumentsItem] = []
    override init(_ doc: Document, delegate: DocumentsCellDelegate) {
        super.init(doc, delegate:delegate)
        for child in doc.children {
            children.append(DocumentsItem.create(child, delegate:delegate))
        }
    }
}

class DocumentsDoc : DocumentsItem {
    override init(_ doc: Document, delegate: DocumentsCellDelegate) {
        super.init(doc, delegate:delegate)
    }
}

class DocumentsItemCell: UICollectionViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var itemIcon: UIImageView!
    @IBOutlet weak var expandIcon: UIImageView!
    @IBOutlet weak var indent: NSLayoutConstraint!
    @IBOutlet weak var panel: UIView!
    @IBOutlet weak var titleIndent: NSLayoutConstraint!
    var tap: UITapGestureRecognizer?
    var docCell: DocumentsItem?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        tap = UITapGestureRecognizer(target: self, action: #selector(self.onTap(_:)))
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(self.onPress(_:)))
        longPress.minimumPressDuration = 1
        self.addGestureRecognizer(longPress)
    }
    func configure(cell: DocumentsItem) {
        title.text = cell.doc.title
        title.sizeToFit()
        self.removeGestureRecognizer(tap!)
        expandIcon.removeGestureRecognizer(tap!)
        docCell = cell
        panel.backgroundColor = cell.depth == 0 ? Configuration.Colors.projectFolder : UIColor.white
        indent.constant = CGFloat(cell.depth * 20)
        if let dir = cell as? DocumentsDir {
            titleIndent.constant = 10
            itemIcon.isHidden = true
            expandIcon.image = UIImage(named: dir.expanded ? "chevron-down" : "chevron-up")
            expandIcon.isHidden = cell.doc.children.count == 0
            self.addGestureRecognizer(tap!)
            
        } else if let _ = cell as? DocumentsDoc {
            titleIndent.constant = 20 + itemIcon.frame.width
            expandIcon.isHidden = true
            itemIcon.image = UIImage(named: "file-multiple-orange")
            itemIcon.isHidden = false
            self.addGestureRecognizer(tap!)
        }
            
    }
    
    @objc func onTap(_ sender: UITapGestureRecognizer) {
        if let cell = docCell as? DocumentsDoc {
            cell.delegate.documentsCell(didTap: cell)
        } else if let cell = docCell as? DocumentsDir {
            let pt = sender.location(in: self)
            let inExpand = !expandIcon.isHidden && expandIcon.frame.contains(pt)
            cell.delegate.documentsCell(didTap: cell, isExpandTap: inExpand)
        }
    }
    
    @objc func onPress(_ sender: UILongPressGestureRecognizer) {
        docCell?.delegate.documentsCell(didPress: docCell!)
    }
    
}
