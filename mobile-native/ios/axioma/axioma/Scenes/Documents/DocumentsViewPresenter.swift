//
//  DocumentsViewPresenter.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 20.06.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation

protocol DocumentsViewPresenterInput {
    func presentError(_ error: Error)
    func presentDocuments(response: ProjectModel.Response)
}

protocol DocumentsViewPresenterOutput: class {
    func displayError(_ error: Error)
    func displayDocuments(viewModel: ProjectModel.View)
}

class DocumentsViewPresenter: NSObject, DocumentsViewPresenterInput {
    weak var output: DocumentsViewPresenterOutput!
    
    func presentError(_ error: Error) {
        self.output.displayError(error)
    }
    
    func presentDocuments(response: ProjectModel.Response) {
        let viewModel = ProjectModel.View(project: response.project, documents: response.documents)
        self.output.displayDocuments(viewModel: viewModel)
    }
}
