//
//  CreateDocumentViewController.swift
//  axioma
//
//  Created by sss on 11.07.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit

protocol CreateDocumentDelegate {
    func createDocument(pickPage: Int)
    func createDocument(delPage: Int)
}

class CreateDocumentViewController: UIViewController {

    @IBOutlet weak var folderFrame: UIView!
    @IBOutlet weak var currFolder: UILabel!
    @IBOutlet weak var cvPages: UICollectionView!
    @IBOutlet weak var docTitle: UITextField!
    @IBOutlet weak var docShortTitle: UITextField!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var spinnerView: UIView!
    
    var cancel: (()->())?
    var created: (()->())?
    var path: String?
    var parentDoc: Document?
    var images: [URL] = []
    var lastPickIdx = -1
    
    private var tempUrls: Set<URL> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        folderFrame.backgroundColor = Configuration.Colors.projectFolder
        currFolder.text = path
        cvPages.register(UINib(nibName: "CreateDocumentPageViewCell", bundle: nil), forCellWithReuseIdentifier: "CreateDocumentPageViewCell")
        docTitle.delegate = self
        configureUI()
        
        let grTap = UITapGestureRecognizer(target: self, action: #selector(self.onTap(_:)))
        self.view.addGestureRecognizer(grTap)
    }

    private func clearTempFiles() {
        let files = AppFiles()
        
        for url in tempUrls {
            _ = files.deleteFile(at: .Documents, withName: url.lastPathComponent)
        }
    }
    
    @IBAction func onCancel(_ sender: Any) {
        dismiss(animated: true) {
            self.cancel?()
            self.clearTempFiles()
        }
    }
    
    @IBAction func onBackBtn(_ sender: Any) {
        dismiss(animated: true) {
            self.cancel?()
            self.clearTempFiles()
        }
    }
    
    @IBAction func onBtnSave(_ sender: UIButton) {
        let progressView = ProgressViewController.show(over: self, caption: "Выгрузка файлов", progress: 0)
        let handleError: ((_ e: Error)->()) = { e in
            progressView.dismiss(animated: false, completion: nil)
            let alert = UIAlertController(title: "Ошибка создания документа", message: e.localizedDescription, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
        
        if let parent = parentDoc {
            DocumentAPI.createDocument(projectId: parent.projectId,
                                       parentId: parent.id, title: docTitle.text!,
                                       shortTitle: docShortTitle.text)
            { doc, error in
                if let e = error {
                    handleError(e)
                } else {
                    let onProgress:(_ progress: Progress) -> () = { progress in
                        progressView.progress = Float(progress.fractionCompleted)
                    }
                    DocumentAPI.uploadFiles(documentId: doc!.id, versionId: doc!.defaultVersion, files: self.images, uploadProgress: onProgress)
                    { json, error in
                        if let e = error {
                            handleError(e)
                        } else {
                            progressView.dismiss(animated: false) {
                                self.dismiss(animated: true) {
                                    self.created?()
                                    self.clearTempFiles()
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    @objc func onTap(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    static func show(over: UIViewController, parent: Document, path: String, cancel:(()->())? = nil, created:(()->())? = nil) {
        let controller = CreateDocumentViewController()
        controller.path = path
        controller.cancel = cancel
        controller.created = created
        controller.parentDoc = parent
        over.present(controller, animated:true)
    }
    
    private func configureUI() {
        if (docTitle.text?.count ?? 0) > 0 && images.count > 0 {
            btnSave.isEnabled = true
            btnSave.setTitleColor(Configuration.Colors.enabledText, for: .normal)
        } else {
            btnSave.isEnabled = false
            btnSave.setTitleColor(Configuration.Colors.disabledText, for: .normal)
        }
    }
    
    func convertPdfToImages(pdf: URL) {
        if let images = pdfToImages(url: pdf) {
            for image in images {
                UIImageWriteToSavedPhotosAlbum(image, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
            }
        }
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            let ac = UIAlertController(title: "Ошибка сохранения", message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        }
    }

}
extension CreateDocumentViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        configureUI()
    }
}

extension CreateDocumentViewController: CreateDocumentDelegate {
    
    static func createPdfPages(_ controller: UIViewController, completion: @escaping (URL)->() = {_ in }) {
        let ac = UIAlertController(title: "", message: "Выбери документ", preferredStyle: .actionSheet)
        let files = AppFiles()
        
        for url in files.urls() ?? [] {
            if url.pathExtension == "pdf" {
                let width = ac.view.bounds.width - 20
                let label = UILabel(frame: CGRect(origin: CGPoint(x:0, y:0), size: CGSize(width: width, height: 30)))
                label.text = url.lastPathComponent
                label.textAlignment = .center
                let imageView = UIImageView(frame: CGRect(origin: CGPoint(x:0, y:0), size: CGSize(width: width, height: 200)))
                
                if let document = CGPDFDocument(url as CFURL), let image = pdfToImage(document: document, pageNumber: 1) {
                    imageView.image = image
                } else {
                    continue
                }
                
                imageView.contentMode = .scaleAspectFit
                let size = CGSize(width:  imageView.bounds.size.width, height: imageView.bounds.size.height + label.bounds.size.height)
                UIGraphicsBeginImageContextWithOptions(size, imageView.isOpaque, 0.0)
                defer { UIGraphicsEndImageContext() }
                
                let context = UIGraphicsGetCurrentContext()
                label.layer.render(in: context!)
                context?.translateBy(x: 0, y: label.bounds.size.height)
                imageView.layer.render(in: context!)
                
                let finalImage = UIGraphicsGetImageFromCurrentImageContext()
                
                let action = UIAlertAction(title: "", style: .default, handler: { _ in
                    completion(url)
                })
                action.setValue(finalImage?.withRenderingMode(UIImageRenderingMode.alwaysOriginal), forKey: "image")
                ac.addAction(action)
            }
        }
        
        ac.addAction(UIAlertAction(title: "Отмена", style: .cancel))
        controller.present(ac, animated: true)
    }
    
    private func addPgfPages(pickPage: Int, pdf: URL) {
        if let items = pdfToImageFiles(url: pdf) {
            if pickPage == images.count {
                images.append(contentsOf: items.map {$0.url})
            } else {
                images.remove(at: pickPage)
                images.insert(contentsOf: items.map {$0.url}, at: pickPage)
            }
            
            for item in items {
                tempUrls.insert(item.url)
            }
            
            cvPages.reloadData()
            configureUI()
        }
    }
    
    func createDocument(pickPage: Int) {
        if pickPage < images.count {
            do {
                let d = try Data(contentsOf: images[pickPage])
                if let img = UIImage(data: d) {
                    ImageViewerViewController.show(self, image: img)
                }
            }
            catch {
                print("Couldn't view image file \"\(images[pickPage])\". " + error.localizedDescription)
            }
            return
        }
        
        let ac = UIAlertController(title: "", message: "Выбери источник", preferredStyle: .actionSheet)
        
        ac.addAction(UIAlertAction(title: "Библиотека изображений", style: .default, handler: { _ in
            self.lastPickIdx = pickPage
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                self.showSpinner()
                let picker = UIImagePickerController()
                picker.sourceType = .photoLibrary
                picker.delegate = self
                self.present(picker, animated: true, completion: self.hideSpinner)
            }
        }))
        
        ac.addAction(UIAlertAction(title: "Документы приложения", style: .default, handler: { _ in
            CreateDocumentViewController.createPdfPages(self) { url in
                self.addPgfPages(pickPage: pickPage, pdf: url)
            }
        }))
        
        ac.addAction(UIAlertAction(title: "Отмена", style: .cancel))
        self.present(ac, animated: true)
    }
    
    func createDocument(delPage: Int) {
        images.remove(at: delPage)
        cvPages.reloadData()
        configureUI()
    }

}

extension CreateDocumentViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let url = info[UIImagePickerControllerImageURL] as? URL {
            picker.dismiss(animated: true, completion: nil)
            if lastPickIdx == images.count {
                images.append(url)
            } else {
                images[lastPickIdx] = url
            }
            cvPages.reloadData()
            configureUI()
        }
    }
}

extension CreateDocumentViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: CreateDocumentPageViewCell = cvPages.dequeueReusableCell(withReuseIdentifier: "CreateDocumentPageViewCell", for: indexPath) as! CreateDocumentPageViewCell
        cell.configure(indexPath.item, url: indexPath.item < images.count ? images[indexPath.item] : nil)
        cell.delegate = self
        return cell
    }
}

extension CreateDocumentViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w = (cvPages.frame.width / 3) - 3
        return CGSize(width: w, height: w)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
}

//SPINNER
extension CreateDocumentViewController {
    func showSpinner() {
        spinnerView.isHidden = false
    }
    
    func hideSpinner() {
        spinnerView.isHidden = true
    }
}
