//
//  CreateDocumentPageViewCell.swift
//  axioma
//
//  Created by sss on 25.07.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit
import Kingfisher

class CreateDocumentPageViewCell: UICollectionViewCell {
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var btnAdd: UIButton!
    var url: URL?
    var pageIdx = -1
    var delegate: CreateDocumentDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let tapGr = UITapGestureRecognizer(target: self, action: #selector(self.onImageTap(_:)))
        self.addGestureRecognizer(tapGr)

    }
    
    func configure(_ pageIdx: Int, url: URL?, readOnly: Bool = false) {
        self.pageIdx = pageIdx
        self.url = url
        if let u = url {
            btnDelete.isHidden = readOnly
            btnAdd.isHidden = true
            image.contentMode = .center
            image.kf.setImage(with: u, placeholder: nil,
                              options: [.transition(ImageTransition.none), .requestModifier(imageRequestModifier)],
             completionHandler: { image, error, cacheType, imageURL in
                if image == nil {
                    DispatchQueue.main.async {
                        self.image.contentMode = .center
                        self.image.image = UIImage(named: "alert")
                    }
                } else {
                    self.image.contentMode = .scaleAspectFit
                }
            })
        } else {
            btnDelete.isHidden = true
            btnAdd.isHidden = false
            image.image = nil
        }
    }
    
    @IBAction func onDelete(_ sender: UIButton) {
        delegate?.createDocument(delPage: pageIdx)
    }
    
    @objc func onImageTap(_ sender: UITapGestureRecognizer) {
        if image.frame.contains(sender.location(in: image)) {
            delegate?.createDocument(pickPage: pageIdx)
        }
    }

}

