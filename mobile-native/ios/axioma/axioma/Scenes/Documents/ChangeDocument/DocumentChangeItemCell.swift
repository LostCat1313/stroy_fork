//
//  DocumentChangeItemCell.swift
//  axioma
//
//  Created by sss on 13.07.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit
import Kingfisher

class DocumentChangeItemCell: UICollectionViewCell {
    @IBOutlet weak var srcImage: UIImageView!
    @IBOutlet weak var dstImage: UIImageView!
    var delegate: DocumentChangeDelegate?
    var pageIdx: Int = 0
    override func awakeFromNib() {
        super.awakeFromNib()
        let tapGr = UITapGestureRecognizer(target: self, action: #selector(self.onDstTap(_:)))
        self.addGestureRecognizer(tapGr)
    }
    func configure(_ pageNo: Int, src: UIImage, page: Page?) {
        pageIdx = pageNo
        srcImage.image = src
        if let p = page {
            if let url = URL(string: p.thumbUrl) {
                dstImage.kf.setImage(with: url, placeholder: nil,
                    options: [.transition(ImageTransition.fade(1)), .requestModifier(imageRequestModifier)],
                    completionHandler: { image, error, cacheType, imageURL in
                        if image == nil {
                            DispatchQueue.main.async {
                                self.dstImage.contentMode = .center
                                self.dstImage.image = UIImage(named: "alert")
                            }
                        } else {
                            self.dstImage.contentMode = .scaleAspectFit
                        }
                    }
                )
            }
        } else {
            dstImage.image = UIImage(named: "plus_black")
            dstImage.contentMode = .center
        }
    }
    
    @objc func onDstTap(_ sender: UITapGestureRecognizer) {
        if dstImage.frame.contains(sender.location(in: dstImage)) {
            delegate?.documentChange(pickPage: pageIdx)
        } else if srcImage.frame.contains(sender.location(in: srcImage)) {
            delegate?.documentChange(pickImage: pageIdx)
        }
    }
    
    @IBAction func onBtnDelete(_ sender: UIButton) {
        delegate?.documentChange(deleteImage: pageIdx)
    }
}
