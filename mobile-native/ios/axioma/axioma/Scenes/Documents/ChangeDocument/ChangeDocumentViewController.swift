//
//  ChangeDocumentViewController.swift
//  axioma
//
//  Created by sss on 13.07.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit
protocol DocumentChangeDelegate {
    func documentChange(pickPage forPageNo: Int)
    func documentChange(pickImage forPageNo: Int)
    func documentChange(deleteImage forPageNo: Int)
}

class ChangeDocumentViewController: UIViewController {
    var path: String?
    @IBOutlet weak var pathView: UIView!
    @IBOutlet weak var pathLabel: UILabel!
    @IBOutlet weak var cvPages: UICollectionView!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var spinnerView: UIView!
    var doc: Document?
    var source: [(img: UIImage, url: URL, page: Page?)] = []
    var completed: (()->())?
    var canceled: (()->())?
    private var tempUrls: Set<URL> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        cvPages.register(UINib(nibName: "DocumentChangeItemCell", bundle: nil), forCellWithReuseIdentifier: "DocumentChangeItemCell")
        pathView.backgroundColor = Configuration.Colors.projectFolder
        pathLabel.text = path
        btnSave.isEnabled = false
        btnSave.alpha = 0.3
    }

    private func clearTempFiles() {
        let files = AppFiles()
        
        for url in tempUrls {
            _ = files.deleteFile(at: .Documents, withName: url.lastPathComponent)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func displayError(cap: String = "Ошибка изменения", _ error: Error) {
        let dlg = Session.makeErrorDialog(cap, message: error.localizedDescription)
        self.present(dlg, animated: true)
    }
    
    @IBAction func onBackButton(_ sender: Any) {
        dismiss(animated: true) {
            self.canceled?()
            self.clearTempFiles()
        }
    }
    static func show(over: UIViewController, doc: Document, path: String, canceled:  (()->())? = nil, completed: (()->())? = nil) {
        let controller = ChangeDocumentViewController()
        controller.path = path
        controller.doc = doc
        controller.completed = completed
        controller.canceled = canceled
        over.present(controller, animated:true)
    }
    
    private func addPgfPages(pdf: URL) {
        if let items = pdfToImageFiles(url: pdf) {
            let elements: [(img: UIImage, url: URL, page: Page?)] = items.map {($0.image, $0.url, nil)}
            for item in items {
                tempUrls.insert(item.url)
            }
            source.append(contentsOf: elements)
            self.refresh()
        }
    }
    
    @IBAction func onAddPages(_ sender: UIButton) {
        let ac = UIAlertController(title: "", message: "Выбери источник", preferredStyle: .actionSheet)
        
        ac.addAction(UIAlertAction(title: "Библиотека изображений", style: .default, handler: { _ in
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                self.showSpinner()
                let picker = UIImagePickerController()
                picker.sourceType = .photoLibrary
                picker.delegate = self
                self.present(picker, animated: true, completion: self.hideSpinner)
            }
        }))
        
        ac.addAction(UIAlertAction(title: "Документы приложения", style: .default, handler: { _ in
            CreateDocumentViewController.createPdfPages(self) { url in
                self.addPgfPages(pdf: url)
            }
        }))
        
        ac.addAction(UIAlertAction(title: "Отмена", style: .cancel))
        self.present(ac, animated: true)
    }
    
    @IBAction func onBtnSave(_ sender: UIButton) {
        let progressView = ProgressViewController.show(over: self, caption: "Выгрузка файлов", progress: 0)
        let onProgress:(_ progress: Progress) -> () = { progress in
            progressView.progress = Float(progress.fractionCompleted)
        }
        
        DocumentAPI.createVersion(documentId: doc!.id) {
            version, error in
            guard error == nil else {
                progressView.dismiss(animated: false) {
                    self.displayError(error!)
                }
                return
            }
            let files = self.source.map {$0.url}
            
            DocumentAPI.uploadFiles(documentId: self.doc!.id, versionId: version!.id, files: files, uploadProgress: onProgress) {
                pages, error in
                guard error == nil else {
                    progressView.dismiss(animated: false) {
                        self.displayError(error!)
                    }
                    return
                }
                var pairs: [(id: Int, number: Int)] = []
                if pages!.count != self.source.count {
                    fatalError("Integrity error.")
                }
                var baseIdx = self.doc!.findMaxPageNo()
                for i in 0..<pages!.count {
                    if self.source[i].page != nil {
                        pairs.append((id: pages![i].id, number: self.source[i].page!.number))
                    } else {
                        baseIdx += 1
                        pairs.append((id: pages![i].id, number: baseIdx))
                    }
                }
                DocumentAPI.updatePageNumbers(pages: pairs) { error in
                    guard error == nil else {
                        progressView.dismiss(animated: false) {
                            self.displayError(error!)
                        }
                        return
                    }
                    progressView.dismiss(animated: false) {
                        self.dismiss(animated: true) {
                            self.completed?()
                            self.clearTempFiles()
                        }
                    }
                }
               
            }
        }
    }
    func refresh() {
        self.cvPages.reloadData()
        configureUI()
    }
    
    private func configureUI() {
        btnSave.isEnabled = source.first != nil
        btnSave.alpha = btnSave.isEnabled ? 1 : 0.3
    }
}
extension ChangeDocumentViewController: DocumentChangeDelegate {
    func documentChange(pickPage forPageNo: Int) {
        DocumentPagePickerViewController.show(over: self, doc: doc!, pick: { page in
            self.source[forPageNo].page = page
            self.refresh()
        })
    }
    
    func documentChange(deleteImage forPageNo: Int) {
        self.source.remove(at: forPageNo)
        self.refresh()
    }
    
    func documentChange(pickImage forPageNo: Int) {
        ImageViewerViewController.show(self, url: source[forPageNo].url)
    }
}

extension ChangeDocumentViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage, let url = info[UIImagePickerControllerImageURL] as? URL  {
            picker.dismiss(animated: true, completion: nil)
            source.append((img: image, url: url, page: nil))
            self.refresh()
        }
    }
}
extension ChangeDocumentViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return source.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: DocumentChangeItemCell = cvPages.dequeueReusableCell(withReuseIdentifier: "DocumentChangeItemCell", for: indexPath) as! DocumentChangeItemCell
        cell.configure(indexPath.item, src: source[indexPath.item].img, page: source[indexPath.item].page)
        cell.delegate = self
        return cell
    }
}

extension ChangeDocumentViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: cvPages.frame.width, height: 200)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
}
//SPINNER
extension ChangeDocumentViewController {
    func showSpinner() {
        spinnerView.isHidden = false
    }
    func hideSpinner() {
        spinnerView.isHidden = true
    }
}
