//
//  DocumentsViewInteractor.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 20.06.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol DocumentsViewInteractorInput {
    func getDocuments(request: ProjectModel.Request)
}

protocol DocumentsViewInteractorOutput {
    func presentError(_ error: Error)
    func presentDocuments(response: ProjectModel.Response)
}

class DocumentsViewInteractor: DocumentsViewInteractorInput {
    var output: DocumentsViewInteractorOutput!
    //var worker = DocumentsViewWorker()
    
    func getDocuments(request: ProjectModel.Request) {
        DocumentAPI.projectDocuments(projectId: request.project.id) { [weak self] error, documents in
            guard self != nil else {
                return
            }
            
            if error != nil {
                self?.output.presentError(error!)
            } else {
                let response = ProjectModel.Response(project: request.project, documents: documents!)
                self?.output.presentDocuments(response: response)
            }
        }
    }
}
