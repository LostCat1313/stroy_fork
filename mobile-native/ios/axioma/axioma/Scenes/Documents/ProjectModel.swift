//
//  DocumentsViewModel.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 20.06.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation

struct ProjectModel {
    enum Errors: Error {
        case serverError(msg: String)
    }

    struct Request {
        var project: ProjectData
    }
    
    struct Response {
        var project: ProjectData
        var documents: [Document]
    }
    
    struct View {
        var project: ProjectData
        var documents: [Document] // only roots
        
        public func findDocument(docId: Int)-> Document? {
            func scanDoc(doc: Document) -> Document? {
                if doc.id == docId {
                    return doc
                } else if doc.isFolder {
                    for subdoc in doc.children {
                        if let d = scanDoc(doc: subdoc) {
                            return d
                        }
                    }
                }
                return nil
            }
            
            for doc in documents {
                if let d = scanDoc(doc: doc) {
                    return d
                }
            }
            return nil
        }
    }
}
