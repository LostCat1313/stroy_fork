import Foundation
import Foundation
import SwiftyJSON

protocol DocumentViewInteractorInput {
}

protocol DocumentViewInteractorOutput {
    func presentError(_ error: Error)
}

class DocumentViewInteractor: DocumentViewInteractorInput {
    var output: DocumentViewInteractorOutput!
    //var worker = DocumentsViewWorker()
}
