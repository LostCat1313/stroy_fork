//
//  DocumentViewModel.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 20.06.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON


class Page {
    let id: Int
    let fid: String
    let original: String
    let type: Int
    let docId: Int //owner
    let number: Int
    let url: String
    let thumbUrl: String
    var descriptors: [PageMarker]?
    
    init(data: [String: Any]) {
        id = data["id"] as! Int
        fid = data["fid"] as! String
        original = data["original"] as! String
        type = data["type"] as! Int
        docId = (data["docId"] as? Int) ?? 0
        number = (data["pageNumber"] as? Int) ?? 0
        url = Configuration.serverUrl.appendingFormat(data["url"] as! String)
        thumbUrl = Configuration.serverUrl.appendingFormat(data["thumbUrl"] as! String)
    }
    
    convenience init(json: JSON) {
        self.init(data: json.dictionaryObject!)
    }
}
    
class Version {
    let id: Int
    let documentId: Int
    let pagesCount: Int
    var created: Date
    var pages: [Page]
    
    convenience init(json: JSON) {
        self.init(data: json.dictionaryObject!)
    }
    
    init(data: [String: Any]) {
        id = data["id"] as! Int
        documentId = data["documentId"] as! Int
        created = (data["createdAt"] as! String).prefix(19).getDate(dateFormat: "yyyy-MM-dd'T'HH:mm:ss")
        pagesCount = (data["pagesCount"] as? Int) ?? 0
        pages = []
        
        if let items = data["pages"] as? [[String: Any]] {
            for item in items {
                pages.append(Page(data: item))
            }
        }
    }
}

class Document {
    let id: Int
    var title: String
    var shortTitle: String
    var parentId: Int?
    var projectId: Int
    var isFolder: Bool
    var children: [Document]
    var versions: [Version]
    let created: Date
    let updated: Date
    
    init(title: String, isFolder: Bool) {
        self.id = 0
        self.title = title
        self.shortTitle = ""
        self.parentId = 0
        self.projectId = 0
        self.isFolder = isFolder
        
        self.created = Date()
        self.updated = Date()
        
        self.children = []
        versions = []
    }
    
    convenience init(json: JSON) {
        self.init(data: json.dictionaryObject!)
    }
    
    init(data: [String: Any]) {
        id = data["id"] as! Int
        title = data["title"] as! String
        shortTitle = (data["shortTitle"] as? String) ?? ""
        parentId = data["parentId"] as? Int
        projectId = data["projectId"] as! Int
        isFolder = data["isFolder"] as! Bool
        
        created = (data["createdAt"] as! String).prefix(19).getDate(dateFormat: "yyyy-MM-dd'T'HH:mm:ss")
        updated = (data["updatedAt"] as! String).prefix(19).getDate(dateFormat: "yyyy-MM-dd'T'HH:mm:ss")
        
        
        if let items = data["children"] as? [[String: Any]] {
            children = items.reduce(into: [], {$0.append(Document(data: $1))})
        } else {
            children = []
        }
        
        if let items = data["versions"] as? [[String: Any]] {
            versions = items.reduce(into: [], {$0.append(Version(data: $1))}).sorted(by: {$0.id < $1.id})
        } else {
            versions = []
        }
    }
    
    var hasVersion: Bool {
        return versions.count > 0
    }
    
    var defaultVersion: Int {
        return versions.first?.id ?? -1
    }
    
    func findPage(pageId: Int) -> Page? {
        for v in versions {
            if let p = v.pages.first(where: {$0.id == pageId}) {
                return p
            }
        }
        return nil
    }
    
    func findPageVersion(pageId: Int) -> Version? {
        for v in versions {
            if v.pages.contains(where: {$0.id == pageId}) {
                return v
            }
        }
        return nil
    }
    
    func findPageVersions(pageNumber: Int) -> [Version] {
        let res: [Version] = versions.reduce(into: []) { acc, ver in
            if ver.pages.contains(where: {$0.number == pageNumber}) {
                acc.append(ver)
            }
        }
        return res
    }
    
    func findMaxPageNo() -> Int {
        var max = 0
        versions.forEach { ver in
            ver.pages.forEach { page in
                if page.number > max {
                    max = page.number
                }
            }
        }
        return max
    }
    
    func pageNumbers() -> [Int] {
        var res:[Int:Int] = [:]
        
        versions.forEach { ver in
            ver.pages.forEach { page in
                res[page.number] = page.number
            }
        }
        return Array(res.keys).sorted()
    }
}


