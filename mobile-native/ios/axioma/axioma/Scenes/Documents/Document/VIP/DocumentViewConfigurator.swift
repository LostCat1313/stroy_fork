import Foundation
import UIKit

extension DocumentViewInteractor: DocumentViewControllerOutput {
}

extension DocumentViewPresenter: DocumentViewInteractorOutput {
   
}

class DocumentViewConfigurator {
    // MARK: Object lifecycle
    static let sharedInstance = DocumentViewConfigurator()
    
    // MARK: Configuration
    func configure(viewController: DocumentViewController) {
        let router = DocumentViewRouter()
        router.viewController = viewController
        
        let presenter = DocumentViewPresenter()
        presenter.output = viewController
        
        let interactor = DocumentViewInteractor()
        interactor.output = presenter
        //interactor.worker = DocumentsViewWorker()
        
        viewController.output = interactor
        viewController.router = router
    }
}
