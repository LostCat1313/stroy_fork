
import Foundation
import UIKit

protocol DocumentViewPresenterInput {
    func presentError(_ error: Error)
}

protocol DocumentViewPresenterOutput: class {
    func displayError(_ error: Error)
}

class DocumentViewPresenter: NSObject, DocumentViewPresenterInput {
    weak var output: DocumentViewPresenterOutput!
    
    func presentError(_ error: Error) {
        self.output.displayError(error)
    }
    
}
