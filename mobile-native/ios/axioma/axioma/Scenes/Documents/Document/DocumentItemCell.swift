//
//  DocumentItemCell.swift
//  axioma
//
//  Created by sss on 27.06.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit
import Kingfisher

class DocumentItemCell: UICollectionViewCell {
    var page: Page?
    var pageVer: Int = -1
    weak var delegate: DocumentPagePickerDelegate?
    
    @IBOutlet weak var lPageNo: UILabel!
    @IBOutlet weak var thumb: UIImageView!

    @IBOutlet weak var pageNoWidth: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        let tapGr = UITapGestureRecognizer(target: self, action: #selector(self.onTap(_:)))
        self.addGestureRecognizer(tapGr)

    }
    
    @objc func onTap(_ sender: UITapGestureRecognizer) {
        if let p = page {
            delegate?.documentPagePicker(onTap: p)
        }
    }
    
    private func setThumb() {
        if let p = page, let url = URL(string: p.thumbUrl) {
            thumb.kf.setImage(with: url, placeholder: nil,
                options: [.transition(ImageTransition.fade(1)), .requestModifier(imageRequestModifier)],
                completionHandler: { image, error, cacheType, imageURL in
                    if image == nil {
                        DispatchQueue.main.async {
                            self.thumb.image = UIImage(named: "alert")
                        }
                    }
            })
        }
    }
    
    private func setPageLabel(prefix: String) {
        var cap: String = prefix + "-" + String(page!.number)
        if pageVer > 0 {
            cap += "(\(pageVer))"
        }
        lPageNo.text = cap
        lPageNo.sizeToFit()
        pageNoWidth.constant = lPageNo.frame.width + 20
        
    }
    
    func configure(page: Page, pageVer: Int, prefix: String) {
        self.page = page
        self.pageVer = pageVer
        setThumb()
        setPageLabel(prefix: prefix)

    }

}
