//
//  DocumentViewController.swift
//  axioma
//
//  Created by sss on 27.06.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit
import Kingfisher

enum DocumentViewMode {
    case all, original, newer
}

protocol DocumentViewControllerInput {
    func displayError(_ error: Error)
}

protocol DocumentViewControllerOutput {
    
}
extension DocumentViewController: DocumentViewPresenterOutput {
    func displayError(_ error: Error) {
        var description = error.localizedDescription
        
        if let err = error as? ProjectModel.Errors {
            switch err {
            case .serverError(let msg):
                description = msg
            }
        }
        
        let dlg = Session.makeErrorDialog("Error", message: description)
        self.present(dlg, animated: true)
    }
}

class DocumentViewController: UIViewController {
 
    // VIP
    var output: DocumentViewControllerOutput!
    var router: DocumentViewRouter!
    
    var doc: Document?
    var project: ProjectModel.View?
    
    var viewMode: DocumentViewMode = .all
    var activePages: [(v: Int, p: Page)] = []
    var completion: (()->())?
    
    @IBOutlet weak var btnMode: UIButton!
    @IBOutlet weak var cvPages: UICollectionView!
    @IBOutlet weak var docTitle: UILabel!
    @IBOutlet weak var topBar: UIView!
    
    @IBOutlet weak var btnPlus: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cvPages.register(UINib(nibName: "DocumentItemCell", bundle: nil), forCellWithReuseIdentifier: "DocumentItemCell")
        DocumentViewConfigurator.sharedInstance.configure(viewController: self)
        refreshPages()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onBack(_ sender: UIButton) {
        self.dismiss(animated: true, completion: completion)
    }
    
    @IBAction func onSelectMode(_ sender: Any) {
        let actions = [
            PopupMenuItem(title: "Все", click: { a in
                self.btnMode.setTitle(a.title, for: .normal)
                self.viewMode = .all
                self.refreshPages()
            }),
            PopupMenuItem(title: "Оригинал", click: { a in
                self.btnMode.setTitle(a.title, for: .normal)
                self.viewMode = .original
                self.refreshPages()
            }),
            PopupMenuItem(title: "Свежайшее", click: { a in
                self.btnMode.setTitle(a.title, for: .normal)
                self.viewMode = .newer
                self.refreshPages()
            })
        ];
        let at = CGPoint(x: btnMode.frame.origin.x, y: topBar.frame.height)
        let at2 = view.convert(at, from: topBar)
        PopupMenuViewController.popup(over: self, at: at2, menuItems: actions)
    }
    
    public static func createPageList(doc: Document, mode: DocumentViewMode) -> [(v: Int, p: Page)] {
        var res: [(v: Int, p: Page)] = []
        let numbers = doc.pageNumbers()
        for i in numbers {
            switch mode {
                case .all:
                    var cnt = 0
                    for v in doc.versions {
                        for p in v.pages {
                            if p.number == i {
                                res.append((cnt, p))
                                cnt += 1
                                break
                            }
                        }
                    }
                case .original:
                    let v = doc.versions[0]
                    for p in v.pages {
                        if p.number == i {
                            res.append((0, p))
                            break
                        }
                    }
                case .newer:
                    var cnt = 0
                    var newerP: Page?
                    for v in doc.versions {
                        for p in v.pages {
                            if p.number == i {
                                newerP = p
                                cnt += 1
                                break
                            }
                        }
                    }
                    res.append((cnt - 1, newerP!))
            }
        }

        return res
    }
    
    func refreshPages() {
        docTitle.text = doc!.title
        activePages = []
        btnMode.isHidden = (doc?.versions.count ?? 0) < 2
        activePages = DocumentViewController.createPageList(doc: doc!, mode: viewMode)
        cvPages.reloadData()
    }
    
    static public func show(_ over: UIViewController, project: ProjectModel.View, doc: Document, page: Page? = nil, activeMarker: PageMarker? = nil, completion: (()->())? = nil) {
        let controller = DocumentViewController()
        controller.project = project
        controller.doc = doc
        controller.completion = completion
        over.present(controller, animated: page == nil) {
            if let p = page {
                controller.showPage(page: p, activeMarker: activeMarker)
            }
        }
    }

}

extension DocumentViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return activePages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: DocumentItemCell = cvPages.dequeueReusableCell(withReuseIdentifier: "DocumentItemCell", for: indexPath) as! DocumentItemCell
        let item = activePages[indexPath.item]
        var prefix: String = doc!.shortTitle
        if prefix.count == 0 {
            prefix = doc!.title.split(separator: " ").reduce(into: "", {$0 += $1.prefix(1)}).uppercased()
        }
        cell.configure(page: item.p, pageVer: item.v, prefix: prefix)
        cell.delegate = self
        return cell
    }
}

extension DocumentViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w = (cvPages.frame.width - 4.0 ) / 2
        let h = w // * 3.0 / 4.0
        return CGSize(width: w, height: h)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
}

extension DocumentViewController {
    func showPage(page: Page, activeMarker: PageMarker?) {
        PageAnnotatesViewController.show(self, project: project!, doc: doc!, activePage: page, activeMarker: activeMarker) { res in
            switch res {
            case .gotoLink(let l):
                if let d = self.project?.findDocument(docId: l.refDocId) {
                    self.doc = d
                    self.refreshPages()
                }
                
            default:
                break
                
            }
        }
    }
}

extension DocumentViewController : DocumentPagePickerDelegate {
    func documentPagePicker(onTap page: Page) {
        showPage(page: page, activeMarker: nil)
    }
}
