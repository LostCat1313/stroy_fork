//
//  DocumentPagePickerViewController.swift
//  axioma
//
//  Created by sss on 17.07.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit

protocol DocumentPagePickerDelegate : class {
    func documentPagePicker(onTap page: Page)
}

class DocumentPagePickerViewController: UIViewController {
    var doc: Document?
    var viewMode: DocumentViewMode = .all
    var activePages: [(v: Int, p: Page)] = []
    var cancel: (()->())?
    var pick:((_ p: Page)->())?
    @IBOutlet weak var cvPages: UICollectionView!
    @IBOutlet weak var contentFrame: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Configuration.Colors.transparentDark
        cvPages.register(UINib(nibName: "DocumentItemCell", bundle: nil), forCellWithReuseIdentifier: "DocumentItemCell")
        refreshPages()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onCancelBtn(_ sender: UIButton) {
        dismiss(animated: false, completion: cancel)
    }
    
    @IBAction func onBackTap(_ sender: UITapGestureRecognizer) {
        let pt = sender.location(in: self.view)
        if !contentFrame.frame.contains(pt) {
            dismiss(animated: false, completion: cancel)
        }
    }
    
    static func show(over: UIViewController, doc: Document, cancel: (()->())? = nil, pick:@escaping (_ p: Page)->()) {
        let controller = DocumentPagePickerViewController()
        controller.doc = doc
        controller.modalPresentationStyle = .overCurrentContext
        controller.cancel = cancel
        controller.pick = pick
        over.present(controller, animated: false)
    }
    
    func refreshPages() {
        activePages = []
        //btnMode.isHidden = (model?.document.versions.count ?? 0) < 2
        
        if let numbers = doc?.pageNumbers() {
            for i in numbers {
                switch viewMode {
                case .all:
                    var cnt = 0
                    for v in doc!.versions {
                        for p in v.pages {
                            if p.number == i {
                                activePages.append((cnt, p))
                                cnt += 1
                                break
                            }
                        }
                    }
                case .original:
                    let v = doc!.versions[0]
                    for p in v.pages {
                        if p.number == i {
                            activePages.append((0, p))
                            break
                        }
                    }
                case .newer:
                    var cnt = 0
                    var newerP: Page?
                    for v in doc!.versions {
                        for p in v.pages {
                            if p.number == i {
                                newerP = p
                                cnt += 1
                                break
                            }
                        }
                    }
                    activePages.append((cnt - 1, newerP!))
                }
            }
        }
        cvPages.reloadData()
    }
}

extension DocumentPagePickerViewController: DocumentPagePickerDelegate {
    func documentPagePicker(onTap page: Page) {
        dismiss(animated: false, completion: {self.pick?(page)})
    }
    
}

extension DocumentPagePickerViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return activePages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: DocumentItemCell = cvPages.dequeueReusableCell(withReuseIdentifier: "DocumentItemCell", for: indexPath) as! DocumentItemCell
        let item = activePages[indexPath.item]
        var prefix: String = doc!.shortTitle
        if prefix.count == 0 {
            prefix = doc!.title.split(separator: " ").reduce(into: "", {$0 += $1.prefix(1)}).uppercased()
        }
        cell.configure(page: item.p, pageVer: item.v, prefix: prefix)
        cell.delegate = self
        return cell
    }
}

extension DocumentPagePickerViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w = (cvPages.frame.width - 4.0 ) / 2
        let h = w // * 3.0 / 4.0
        return CGSize(width: w, height: h)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
}
