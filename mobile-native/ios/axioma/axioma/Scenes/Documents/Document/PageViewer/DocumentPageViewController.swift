//
//  DocumentPageViewController.swift
//  axioma
//
//  Created by sss on 03.09.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit
import Kingfisher

enum DocumentPageViewMode {
    case none
    case pageLink(project: ProjectModel.View, link: PageMarkerLink)
    case pageChange(project: ProjectModel.View, change: PageMarkerChange)
}
enum DocumentPageViewResult {
    case ok
    case delete
    case go(doc: Document, page: Page)
}
class DocumentPageViewController: UIViewController {
    var project: ProjectModel.View?
    var mode: DocumentPageViewMode = .none
    var result: ((_ res: DocumentPageViewResult) -> ())?
    
    var goDoc: Document?
    var goPage: Page?
    
    @IBOutlet weak var pageImage: UIImageView!
    @IBOutlet weak var modeTitle: UILabel!
    @IBOutlet weak var pageCaption: UILabel!
    @IBOutlet weak var btnOK: UIButton!
    @IBOutlet weak var btnGO: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    override func viewDidLoad() {
        super.viewDidLoad()
        switch mode {
            case .pageChange(let project, let change):
                setViewFromPageChange(project: project, change: change)

            case .pageLink(let project, let link):
                setViewFromPageLink(project: project, link: link)
            
            default:
                break
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    public static func show(_ over: UIViewController, mode: DocumentPageViewMode, result: @escaping (_ res: DocumentPageViewResult)->()) {
        let controller = DocumentPageViewController()
        controller.mode = mode
        controller.modalPresentationStyle = .overCurrentContext
        controller.result = result
        over.present(controller, animated:true)
        
    }
    
    @IBAction func onBtnOK(_ sender: UIButton) {
        dismiss(animated: true, completion: {self.result?(.ok)})
    }
    
    @IBAction func onBtnGO(_ sender: UIButton) {
        dismiss(animated: true, completion: {self.result?(.go(doc: self.goDoc!, page: self.goPage!))})
    }
    
    @IBAction func onBtnDelete(_ sender: UIButton) {
        dismiss(animated: true, completion: {self.result?(.delete)})
    }
    
    func displayError(_ error: Error) {
        var description = error.localizedDescription
        
        if let err = error as? ProjectModel.Errors {
            switch err {
            case .serverError(let msg):
                description = msg
            }
        }
        
        let dlg = Session.makeErrorDialog("Error", message: description)
        self.present(dlg, animated: true)
    }
    
}

//PageChangeMode
extension DocumentPageViewController {
    func setViewFromPageChange(project: ProjectModel.View, change: PageMarkerChange) {
        
        if !project.project.isCurrentWriter {
            btnDelete.isEnabled = false
            btnDelete.alpha = 0.4
        }
        
        modeTitle.text = "Изменение"
        spinner.startAnimating()
        if let doc = project.findDocument(docId: change.refDocId), let p = doc.findPage(pageId: change.refPageId) {
            goDoc = doc
            goPage = p
            pageCaption.text = change.note
            let completion: (_ image: Image?, _ error: NSError?, _ cacheType: CacheType, _ imageURL: URL?) -> () = { _, _, _, _ in
                self.spinner.stopAnimating()
            }
            
            if let url = URL(string: p.url) {
                pageImage.kf.setImage(with: url, placeholder: nil,
                                      options: [.transition(ImageTransition.fade(1)), .requestModifier(imageRequestModifier)],
                                      completionHandler: completion)
            }
        }
    }
}

//PageLinkMode
extension DocumentPageViewController {
    func setViewFromPageLink(project: ProjectModel.View, link: PageMarkerLink) {
        
        if !project.project.isCurrentWriter {
            btnDelete.isEnabled = false
            btnDelete.alpha = 0.4
        }
        
        modeTitle.text = "Ссылка"
        spinner.startAnimating()
        if let doc = project.findDocument(docId: link.refDocId), let p = doc.findPage(pageId: link.refPageId) {
            goDoc = doc
            goPage = p
            pageCaption.text = link.title
            let completion: (_ image: Image?, _ error: NSError?, _ cacheType: CacheType, _ imageURL: URL?) -> () = { _, _, _, _ in
                self.spinner.stopAnimating()
            }
            if let url = URL(string: p.url) {
                pageImage.kf.setImage(with: url, placeholder: nil,
                                      options: [.transition(ImageTransition.fade(1)), .requestModifier(imageRequestModifier)],
                                      completionHandler: completion)
            }
        } else {
            spinner.stopAnimating()
            btnGO.isEnabled = false
            btnGO.alpha = 0.4
            DispatchQueue.main.async { //because controller can be not present
                
                let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Документ '\(link.title)' не найден в проекте."])
                self.displayError(error)
            }
        }
    }
}
