//
//  PageChangeViewController.swift
//  axioma
//
//  Created by sss on 25.09.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit

enum PageChangeViewResult {
    case cancel
    case ok(note: String, v: Version, p: Page)
}
enum PageChangeVersionCoord {
    case none
    case older(idx: Int)
    case newer(idx: Int)
}

class PageChangeViewController: UIViewController {

    @IBOutlet weak var olderVersionsView: UIView!
    @IBOutlet weak var newerVersionsView: UIView!
    @IBOutlet weak var btnOK: UIButton!
    @IBOutlet weak var changeNoteField: UITextField!
    
    @IBOutlet weak var cvOlder: UICollectionView!
    @IBOutlet weak var cvNewer: UICollectionView!
    
    var result: ((_ res: PageChangeViewResult) -> ())!
    var doc: Document!
    var page: Page!
    var pageVersion: Version!
    
    var pageOlderVersions: [(v: Version, p: Page)] = []
    var pageNewerVersions: [(v: Version, p: Page)] = []
    
    var selected: PageChangeVersionCoord = .none {
        didSet {
            refreshUI()
            cvOlder.reloadData()
            cvNewer.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        changeNoteField.delegate = self
        pageVersion = doc.findPageVersion(pageId: page.id)
        doc.versions.forEach {
            if let p = $0.pages.first(where: {$0.number == self.page.number && $0.id != self.page.id}) {
                if $0.created < pageVersion.created {
                    self.pageOlderVersions.append((v: $0, p: p))
                } else {
                    self.pageNewerVersions.append((v: $0, p: p))
                }
            }
        }
        setOlder()
        setNewer()
        cvOlder.register(UINib(nibName: "PageChangeVersionCell", bundle: nil), forCellWithReuseIdentifier: "PageChangeVersionCell")
        cvNewer.register(UINib(nibName: "PageChangeVersionCell", bundle: nil), forCellWithReuseIdentifier: "PageChangeVersionCell")
        refreshUI()
    }
    
    private func setOlder() {
        olderVersionsView.isHidden = pageOlderVersions.count == 0
    }
    
    private func setNewer() {
        newerVersionsView.isHidden = pageNewerVersions.count == 0
    }
    
    public static func show(_ over: UIViewController, doc: Document, page: Page,
                            result: @escaping ((_ res: PageChangeViewResult)->())) {
        let controller = PageChangeViewController()
        controller.page = page
        controller.result = result
        controller.doc = doc
        over.present(controller, animated:true)
    }

    @IBAction func onBtnCancel(_ sender: UIButton) {
        let onR = result
        dismiss(animated: true) {onR?(.cancel)}
    }
    
    @IBAction func onBtnOK(_ sender: UIButton) {
        let onR = result
        var res: PageChangeViewResult = .cancel
        switch selected {
            case .older(let idx):
                res = .ok(note: changeNoteField.text ?? "", v: pageOlderVersions[idx].v, p: pageOlderVersions[idx].p)
            case .newer(let idx):
                res = .ok(note: changeNoteField.text ?? "", v: pageNewerVersions[idx].v, p: pageNewerVersions[idx].p)
            default:
                fatalError("We must not be here.")
        }
        dismiss(animated: true) {onR?(res)}
    }
    
    private func refreshUI() {
        switch selected {
            case .none:
                btnOK.isEnabled = false
                btnOK.alpha = 0.3
            default:
                btnOK.isEnabled = true
                btnOK.alpha = 1
        }
    }
    
    @IBAction func onBkTap(_ sender: UITapGestureRecognizer) {
        changeNoteField.resignFirstResponder()
    }
}

extension PageChangeViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView === cvOlder {
            return pageOlderVersions.count
        } else if collectionView === cvNewer {
            return pageNewerVersions.count
        } else {
            return -1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView === cvOlder {
            let item = pageOlderVersions[indexPath.item]
            let cell: PageChangeVersionCell = cvOlder.dequeueReusableCell(withReuseIdentifier: "PageChangeVersionCell", for: indexPath) as! PageChangeVersionCell
            var cellSelected = false
            switch selected {
                case .older(let idx): cellSelected = idx == indexPath.item
                default: break
            }
            cell.configure(v: item.v, checked: cellSelected) {
                switch self.selected {
                    case .older(let idx):
                        if idx != indexPath.item {
                            self.selected = .older(idx: indexPath.item)
                        }
                    default: self.selected = .older(idx: indexPath.item)
                }
            }
            return cell
        } else if collectionView === cvNewer {
            let item = pageNewerVersions[indexPath.item]
            let cell: PageChangeVersionCell = cvNewer.dequeueReusableCell(withReuseIdentifier: "PageChangeVersionCell", for: indexPath) as! PageChangeVersionCell
            var cellSelected = false
            switch selected {
                case .newer(let idx): cellSelected = idx == indexPath.item
                default: break
            }
            cell.configure(v: item.v, checked: cellSelected) {
                switch self.selected {
                    case .newer(let idx):
                        if idx != indexPath.item {
                            self.selected = .newer(idx: indexPath.item)
                        }
                    default: self.selected = .newer(idx: indexPath.item)
                }
            }
            return cell
        } else {
            fatalError("Impossible branch")
        }
    }
}

extension PageChangeViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
}

extension PageChangeViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
