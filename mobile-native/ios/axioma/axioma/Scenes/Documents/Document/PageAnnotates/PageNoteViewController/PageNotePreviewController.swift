//
//  PageNotePreviewController.swift
//  axioma
//
//  Created by sss on 21.09.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit

class PageNotePreviewController: UIViewController {
    var over: UIViewController!
    var result: ((_ res: PageNoteResult)->())!
    var images: [PageNoteUrl] = []
    var loading = false
    var src: PageMarkerNote!
    var project: ProjectData!
    
    @IBOutlet weak var labelCaption: UILabel!
    
    @IBOutlet weak var labelNote: UILabel!
    @IBOutlet weak var btnAction: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        if src.isDraft {
            btnAction.setTitle("РЕДАКТИРОВАТЬ", for: .normal)
            if !project.isCurrentWriter {
                btnAction.isEnabled = false
                btnAction.alpha = 0.4
            }
        } else {
            btnAction.setTitle("ПОДРОБНЕЕ", for: .normal)
        }
        if src.images.count > 0 {
            labelCaption.text = "Замечание (Вложений: \(src.images.count))"
        } else {
            labelCaption.text = "Замечание"
        }
        labelNote.text = src.text
        if !project.isCurrentWriter {
            btnDelete.isEnabled = false
            btnDelete.alpha = 0.4
        }
    }

    public static func show(_ over: UIViewController, src: PageMarkerNote, project: ProjectData, result: @escaping (_ res: PageNoteResult)->()) {
        let controller = PageNotePreviewController()
        controller.over = over
        controller.result = result
        controller.src = src
        controller.project = project
        controller.modalPresentationStyle = .overCurrentContext
        over.present(controller, animated:false)
    }

    @IBAction func backgroundTap(_ sender: UITapGestureRecognizer) {
        dismiss(animated: false, completion: {self.result?(.ok)})
    }
    
    @IBAction func onBtnOK(_ sender: UIButton) {
        dismiss(animated: false, completion: {self.result?(.ok)})
    }
    
    @IBAction func onBtnAction(_ sender: UIButton) {
        dismiss(animated: false) {
            PageNoteViewController.show(self.over, project: self.project, src: self.src, result: self.result)
        }
    }
    
    @IBAction func onBtnDelete(_ sender: UIButton) {
        dismiss(animated: false, completion: {self.result?(.delete)})
    }
}
