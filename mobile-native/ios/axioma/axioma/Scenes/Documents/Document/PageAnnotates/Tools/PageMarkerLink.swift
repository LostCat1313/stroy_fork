
import Foundation
import UIKit
import SwiftyJSON

class PageMarkerLink : PageMarker {
    struct Design {
        static let iconSize: CGSize = CGSize(width: 32, height: 32)
        static let textFont: UIFont = UIFont.systemFont(ofSize: 14)
        static let textColor: UIColor = UIColor.white
        static let backColor: UIColor = Configuration.Colors.markerLinkBkColor
    }
    
    private var linkView: UIView?
    private var linkIcon: UIImageView?
    private var linkText: UILabel?
    
    
    let refDocId: Int
    let refPageId: Int
    let position: CGPoint
    
    
    init(ownerDocId: Int, ownerPageId: Int, position: CGPoint, refDoc: Document, refPage: Page) {
        self.refDocId = refDoc.id
        self.refPageId = refPage.id
        self.position = position
        super.init(ownerDocId: ownerDocId, ownerPageId: ownerPageId, text: refDoc.title + "\(refPage.number)", title: refDoc.title + " Страница \(refPage.number)")
    }
    
    override init?(json: JSON) {
        if let d = json["params"].dictionaryObject {
            self.refDocId = d["refDocId"] as! Int
            self.refPageId = d["refFileId"]  as! Int
            let x = CGFloat((d["x"] as! NSNumber).floatValue)
            let y = CGFloat((d["y"] as! NSNumber).floatValue)
            self.position = CGPoint(x: x, y: y)
            super.init(json: json)
        } else {
            return nil
        }
        
    }
    
    override func getParams() -> [String: Any] {
        var d: [String: Any] = [:]
        d["x"] = Int(self.position.x)
        d["y"] = Int(self.position.y)
        d["refDocId"] = refDocId
        d["refFileId"] = refPageId
        return d
    }
    
    override func getType() -> PageMarkerTypeId {
        return .link
    }
    
    override func getView() -> UIView? {
        return linkView
    }
    
    override func update(at: CGPoint, owner: UIView) {
        if linkView == nil {
            linkView = UIView(frame: CGRect(origin: CGPoint.zero, size: CGSize.zero))
            linkIcon = UIImageView(frame: CGRect(origin: CGPoint.zero, size: Design.iconSize))

            linkIcon?.image = UIImage(named: "link_chev")
            linkIcon?.contentMode = .scaleAspectFit
            linkText = UILabel(frame: CGRect.zero)
            linkText?.font = Design.textFont
            linkText?.textColor = Design.textColor
            linkText?.backgroundColor = Design.backColor
            linkText?.text = text
            linkText?.sizeToFit()
            linkText?.textAlignment = .center
            linkText?.frame.origin = CGPoint(x: Design.iconSize.width + 1, y: 0)
            linkText?.frame.size = CGSize(width: linkText!.frame.width + 10, height: linkText!.frame.height + 4)
            linkText?.cornerRadius = linkText!.frame.height / 2
            linkView?.frame.size = CGSize(width: Design.iconSize.width + linkText!.frame.width + 1, height: max(Design.iconSize.height, linkText!.frame.height))
            linkView?.clipsToBounds = true
            linkView?.addSubview(linkIcon!)
            linkView?.addSubview(linkText!)
            owner.addSubview(linkView!)
        }
        let ofs = CGPoint(x: at.x - Design.iconSize.width / 2, y: at.y - linkView!.frame.height)
        linkView?.frame.origin = ofs
    }
   
    
}
