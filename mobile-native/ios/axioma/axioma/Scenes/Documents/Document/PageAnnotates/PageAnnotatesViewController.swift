//
//  PageAnnotatesViewController.swift
//  axioma
//
//  Created by sss on 20.08.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit
import Kingfisher
import AVFoundation

enum PageAnnotatorState {
    case preview, change, link, notice
    case curve(marker: PageMarkerCurve)
    case rect(marker: PageMarkerRect)
    case editText(marker: PageMarkerText)
    case moveText(marker: PageMarkerText)
}

enum PageAnnotatorResult {
    case canceled
    case gotoLink(link: PageMarkerLink)
}


class PageAnnotatesViewController: UIViewController {
    @IBOutlet weak var topBar: UIView!
    @IBOutlet weak var btnPageCursor: UIButton!
    @IBOutlet weak var pageImage: UIImageView!
    @IBOutlet weak var drawPanel: PageAnnotatesView!
    @IBOutlet weak var imgPanel: UIView!
    @IBOutlet weak var imgPanelBottom: NSLayoutConstraint!
    @IBOutlet weak var btnLeft: UIButton!
    @IBOutlet weak var btnRight: UIButton!
    @IBOutlet weak var btnTools: UIButton!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var toolbarView: UIView!
    @IBOutlet weak var extToolbarPanel: UIView!
    @IBOutlet weak var colorPickerSlider: ColorPickerSlider!
    @IBOutlet weak var sizeSlider: UISlider!
    @IBOutlet weak var toolbarSizeButton: UIButton!
    @IBOutlet weak var editTextMarker: UITextField!
    @IBOutlet weak var filterButton: UIButton!
    
    var result:((_ res: PageAnnotatorResult)->())?
    var doc: Document?
    var activePage: Page?
    var activeMarker: PageMarker?
    var project: ProjectModel.View?
    
    var pages: [(v: Int, p: Page)] = []
    var activePageIdx: Int = 0
    
    private var markerLayers: [PageMarkerTypeId] = [.edit, .link, .note, .text, .rect, .curve]
    var markers: [PageMarker] = []
    
    var minimalSize: CGSize = CGSize.zero
    
    var state: PageAnnotatorState = .preview {
        didSet {
            stateDidSet()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        editTextMarker.delegate = self
        editTextMarker.returnKeyType = .done
        colorPickerSlider.delegate = self
        drawPanel.delegate = self
        
        pages = DocumentViewController.createPageList(doc: doc!, mode: .all)
        
        //init toolbar
        let toolbar:UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: 30))
        //create left side empty space so that done button set on right side
        let flexSpace = UIBarButtonItem(barButtonSystemItem:    .flexibleSpace, target: nil, action: nil)
        let doneBtn: UIBarButtonItem = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(self.cancelButtonKeyboard))
        toolbar.setItems([flexSpace, doneBtn], animated: false)
        toolbar.sizeToFit()
        editTextMarker.inputAccessoryView = toolbar
        
        if let p = activePage {
            for (idx, item) in pages.enumerated() {
                if item.p.id == p.id {
                    activePageIdx = idx
                    break
                }
            }
        } else {
            activePage = pages[activePageIdx].p
        }
        refresh()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func cancelButtonKeyboard() {
        editTextMarker.endEditing(true)
        switch state {
            case .editText(let marker):
                drawPanel.update(marker: marker)
                state = .preview
            default:
                break
        }
    }
    
    private func openDrawableMarkerToolbar() {
        if self.toolbarView.isHidden {
            self.toolbarView.transform = CGAffineTransform.identity.translatedBy(x: 0.0, y: self.toolbarView.frame.size.height)
            self.toolbarView.isHidden = false
            
            UIView.animate(withDuration: 0.5, animations: {
                self.toolbarView.transform = CGAffineTransform.identity
            })
        }
    }
    
    private func stateDidSet() {
        imgPanel.isUserInteractionEnabled = true
        imgPanel.alpha = 1.0
        switch state {
            case .link, .notice, .change:
                markers.forEach{$0.view?.alpha = 0.3}
            case .preview:
                markers.forEach{$0.view?.alpha = 1}
                self.editTextMarker.isHidden = true
                //if !toolbarView.isHidden {
                //    onCloseToolbar(self)
                //}
            case .curve(let marker):
                toolbarSizeButton.setImage(UIImage(named: "format-line-weight"), for: .normal)
                self.colorPickerSlider.currentColor = marker.color
                self.sizeSlider.minimumValue = 1
                self.sizeSlider.maximumValue = 20
                self.sizeSlider.value = Float(marker.width)
                openDrawableMarkerToolbar()
            case .rect(let marker):
                toolbarSizeButton.setImage(UIImage(named: "format-line-weight"), for: .normal)
                self.colorPickerSlider.currentColor = marker.color
                self.sizeSlider.minimumValue = 1
                self.sizeSlider.maximumValue = 20
                self.sizeSlider.value = Float(marker.width)
                openDrawableMarkerToolbar()
            case .editText(let marker):
                toolbarSizeButton.setImage(UIImage(named: "format-size"), for: .normal)
                self.colorPickerSlider.currentColor = marker.color
                self.editTextMarker.isHidden = false
                self.editTextMarker.text = marker.getText() ?? ""
                self.editTextMarker.textColor = marker.color
                self.editTextMarker.font = marker.font
                self.editTextMarker.becomeFirstResponder()
                // remove marker from draw panel
                drawPanel.remove(marker: marker)
                imgPanel.isUserInteractionEnabled = false
                imgPanel.alpha = 0.2
            case .moveText(let marker):
                self.editTextMarker.isHidden = true
                toolbarSizeButton.setImage(UIImage(named: "format-size"), for: .normal)
                self.colorPickerSlider.currentColor = marker.color
                self.sizeSlider.minimumValue = 10
                self.sizeSlider.maximumValue = 50
                self.sizeSlider.value = Float(marker.font.pointSize)
                openDrawableMarkerToolbar()
                // put marker to draw panel
                drawPanel.update(marker: marker)
        }
    }
    
    @IBAction func onFilter(_ sender: Any) {
        
        func updateHiddenProperty() {
            for marker in markers {
                marker.isHidden = !markerLayers.contains(where: {$0 == marker.getType()})
            }
            drawPanel.setNeedsDisplay()
        }
        
        let menuItems: [PopupMenuItemProtocol] = [
            CheckBoxMenuItem(title: "Изменения", checked: markerLayers.contains(where: {$0 == .edit}), changed: { item in
                self.markerLayers = self.markerLayers.filter { $0 != .edit }
                if item.checked {
                    self.markerLayers.append(.edit)
                }
                updateHiddenProperty()
            }),
            CheckBoxMenuItem(title: "Ссылки", checked: markerLayers.contains(where: {$0 == .link}), changed: { item in
                self.markerLayers = self.markerLayers.filter { $0 != .link }
                if item.checked {
                    self.markerLayers.append(.link)
                }
                updateHiddenProperty()
            }),
            CheckBoxMenuItem(title: "Замечания", checked: markerLayers.contains(where: {$0 == .note}), changed: { item in
                self.markerLayers = self.markerLayers.filter { $0 != .note }
                if item.checked {
                    self.markerLayers.append(.note)
                }
                updateHiddenProperty()
            }),
            
            CheckBoxMenuItem(title: "Тексты", checked: markerLayers.contains(where: {$0 == .text}), changed: { item in
                self.markerLayers = self.markerLayers.filter { $0 != .text }
                if item.checked {
                    self.markerLayers.append(.text)
                }
                updateHiddenProperty()
            }),
            CheckBoxMenuItem(title: "Линии", checked: markerLayers.contains(where: {$0 == .curve}), changed: { item in
                self.markerLayers = self.markerLayers.filter { $0 != .curve }
                if item.checked {
                    self.markerLayers.append(.curve)
                }
                updateHiddenProperty()
            }),
            CheckBoxMenuItem(title: "Прямоугольники", checked: markerLayers.contains(where: {$0 == .rect}), changed: { item in
                self.markerLayers = self.markerLayers.filter { $0 != .rect }
                if item.checked {
                    self.markerLayers.append(.rect)
                }
                updateHiddenProperty()
            })
        ]
        
        let at = CGPoint(x: filterButton.frame.origin.x, y: topBar.frame.height)
        let at2 = view.convert(at, from: topBar)
        PopupMenuViewController.popup(over: self, at: at2, menuItems: menuItems)
    }
    
    @IBAction func onShareButton(_ sender: Any) {

        showSpinner()
        
        DispatchQueue.main.async {
            var image = self.pageImage.image!
            self.drawPanel.transform = CGAffineTransform.identity
            
            for item in self.markers {
                if let marker = item as? DrawableMarker {
                    image = self.drawPanel.renderToImage(image, marker: marker)
                }
            }
            
            self.drawPanel.transform = self.pageImage.transform
            self.hideSpinner()
            
            self.shareImage(image)
        }
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            let ac = UIAlertController(title: "Ошибка сохранения", message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        } else {
            let ac = UIAlertController(title: "Сохранено!", message: "Изображение сохранено в ваших фотографиях.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        }
    }
    
    @IBAction func onBtnBack(_ sender: UIButton) {
        checkMarkerChanges {
            self.dismiss(animated: true) {
                self.result?(.canceled)
            }
        }
    }
    
    @IBAction func onPalette(_ sender: Any) {
        if sizeSlider.isHidden {
            extToolbarPanel.isHidden = !extToolbarPanel.isHidden
        }
        sizeSlider.isHidden = true
        colorPickerSlider.isHidden = false
    }
    
    @IBAction func onChangeSize(_ sender: Any) {
        if colorPickerSlider.isHidden {
            extToolbarPanel.isHidden = !extToolbarPanel.isHidden
        }
        sizeSlider.isHidden = false
        colorPickerSlider.isHidden = true
    }
    
    @IBAction func onChangeSizeValue(_ sender: UISlider) {
        switch state {
            case .curve(let marker):
                marker.width = CGFloat(sender.value)
                state = .curve(marker: marker)
                drawPanel.update(marker: marker)
            case .rect(let marker):
                marker.width = CGFloat(sender.value)
                state = .rect(marker: marker)
                drawPanel.update(marker: marker)
            case .moveText(let marker):
                marker.font = UIFont(name: marker.font.fontName, size: CGFloat(sender.value))!
                state = .moveText(marker: marker)
                drawPanel.update(marker: marker)
            default:
                break
        }
    }
    
    private func append(marker: PageMarker) {
        if !self.markers.contains(where: {$0 === marker}) {
            self.markers.append(marker)
        }
    }
    
    @IBAction func onCloseToolbar(_ sender: Any) {
        let transform = self.toolbarView.transform.translatedBy(x: 0.0, y: self.toolbarView.frame.size.height)
        extToolbarPanel.isHidden = true
        sizeSlider.isHidden = true
        colorPickerSlider.isHidden = true
        
        UIView.animate(withDuration: 0.5, animations: {
            self.toolbarView.transform = transform
        }, completion: { _ in
            self.toolbarView.isHidden = true
            self.toolbarView.transform = CGAffineTransform.identity
            
            switch self.state {
                case .curve(let marker):
                    self.state = .preview
                    self.append(marker: marker)
                case .rect(let marker):
                    self.state = .preview
                    self.markers = self.markers.filter { !($0 === marker) }
                    self.append(marker: marker)
                case .moveText(let marker):
                    self.state = .preview
                    self.append(marker: marker)
                default:
                    break
            }
        })
    }
    
    @IBAction func onDeleteDrawableMarker(_ sender: Any) {
        func deleteMarker(marker: PageMarker) {
            // remove from markers
            markers = markers.filter { !($0 === marker) }
            
            if marker.isSaved {
                DocumentAPI.deleteMarker(marker: marker) { [weak self] error in
                    if error != nil {
                        self?.displayError("Ошибка удаления маркера.", error!)
                    } else {
                        self?.drawPanel.remove(marker: marker as! DrawableMarker)
                    }
                }
            } else {
                drawPanel.remove(marker: marker as! DrawableMarker)
            }
        }
        
        switch self.state {
            case .curve(let marker):
                self.state = .preview
                deleteMarker(marker: marker)
            case .rect(let marker):
                self.state = .preview
                deleteMarker(marker: marker)
            case .moveText(let marker):
                self.state = .preview
                deleteMarker(marker: marker)
            default:
                break
        }
    }
    
    private func saveMarker(marker: PageMarker) {
        if marker.isSaved {
            DocumentAPI.updateMarker(marker: marker) { err in
                guard err == nil else {
                    self.displayError("Изменение маркера", err!)
                    return
                }
            }
        } else {
            DocumentAPI.createMarker(docId: self.doc!.id, pageId: self.activePage!.id, marker: marker) { err, newMarker in
                guard err == nil else {
                    self.displayError("Создание маркера", err!)
                    return
                }
                
                if !self.markers.contains(where: {$0 === marker}) {
                    self.markers.insert(newMarker!, at: 0)
                }
                self.drawPanel.clear()
                self.updateMarkers()
            }
        }
    }
    
    @IBAction func onCheckDrawableMarker(_ sender: Any) {
        
        switch self.state {
            case .curve(let marker):
                state = .preview
                marker.isChanged = false
                saveMarker(marker: marker)
            case .rect(let marker):
                state = .preview
                if marker.ltPoint != nil && marker.rbPoint != nil {
                    marker.isChanged = false
                    saveMarker(marker: marker)
                }
            case .moveText(let marker):
                state = .preview
                marker.isChanged = false
                saveMarker(marker: marker)
            default:
                break
        }
        
        onCloseToolbar(sender)
        drawPanel.setNeedsDisplay()
    }
    
    static func show(_ over: UIViewController, project: ProjectModel.View, doc: Document, activePage: Page?, activeMarker: PageMarker? = nil, result: @escaping (_ res: PageAnnotatorResult)->()) {
        let controller = PageAnnotatesViewController()
        controller.project = project
        controller.doc = doc
        controller.activePage = activePage
        controller.result = result
        controller.activeMarker = activeMarker
        //controller.modalPresentationStyle = .overCurrentContext
        over.present(controller, animated: true)
    }
    
    private func updateMarkers() {
        
        for item in markers {
            item.isHidden = !markerLayers.contains(where: {$0 == item.getType()})
            
            if let change = item as? PageMarkerChange {
                let viewPos = pageImage.convertPoint(fromImagePoint: change.position)
                let at = pageImage.convert(viewPos, to: imgPanel)
                item.update(at: at, owner: imgPanel)
            } else  if let link = item as? PageMarkerLink {
                let viewPos = pageImage.convertPoint(fromImagePoint: link.position)
                let at = pageImage.convert(viewPos, to: imgPanel)
                item.update(at: at, owner: imgPanel)
            } else if let note = item as? PageMarkerNote {
                let viewPos = pageImage.convertPoint(fromImagePoint: note.position)
                let at = pageImage.convert(viewPos, to: imgPanel)
                item.update(at: at, owner: imgPanel)
            }
            
            if let marker = item as? DrawableMarker {
                self.drawPanel.update(marker: marker)
            }
            
            if let a = activeMarker {
                if a.id == item.id {
                    item.view?.superview?.bringSubview(toFront: item.view!)
                    item.alpha = 1
                } else {
                    item.alpha = 0.3
                }
            }
        }
        
        if activeMarker != nil {
            activeMarker = nil
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 5, execute: {
                self.markers.forEach { $0.alpha = 1 }
                self.drawPanel.setNeedsDisplay()
            })
        }
    }
    
    func tryTapOnMarker(tap: UITapGestureRecognizer) {
        var pt = tap.location(in: imgPanel)
        for marker in markers {
            if let v = marker.view, v.frame.contains(pt) {
                if let c = marker as? PageMarkerChange {
                    handleChangeClick(change: c)
                    return
                } else if let l = marker as? PageMarkerLink {
                    handleLinkClick(link: l)
                    return
                } else if let n = marker as? PageMarkerNote {
                    handleNoteClick(note: n)
                    return
                }
            }
        }
        
        // drawable markers
        pt = tap.location(in: pageImage)
        if let marker = drawPanel.hitTest(pt) {
            if let curve = marker as? PageMarkerCurve {
                state = .curve(marker: curve)
            } else if let rect = marker as? PageMarkerRect {
                state = .rect(marker: rect)
            } else if let text = marker as? PageMarkerText {
                state = .editText(marker: text)
            }
        } else {
            state = .preview
            
            if !toolbarView.isHidden {
                onCloseToolbar(self)
            }
        }
    }
    
    @IBAction func onPinch(_ sender: UIPinchGestureRecognizer) {
        switch sender.state {
            case .changed:
                let t = pageImage.transform
                pageImage.transform = pageImage.transform.concatenating(CGAffineTransform(scaleX: sender.scale, y: sender.scale))
                drawPanel.transform = drawPanel.transform.concatenating(CGAffineTransform(scaleX: sender.scale, y: sender.scale))
                if pageImage.frame.width < minimalSize.width || pageImage.frame.height < minimalSize.height {
                    UIView.animate(withDuration: 0.4, delay: 0, options: .curveEaseOut, animations: {
                        self.pageImage.transform = t
                        self.drawPanel.transform = t
                    }, completion: { _ in
                        self.updateMarkers()
                    })
                } else {
                    updateMarkers()
                }
                sender.scale = 1
            
            default:
                break
        }
    }
    
    @IBAction func onPan(_ sender: UIPanGestureRecognizer) {
        let pt = sender.location(in: self.pageImage)
        let imagePoint = self.pageImage.convertPoint(fromViewPoint: pt)
        
        switch sender.state {
            case .began:
                switch state {
                    case .moveText, .preview:
                        if let marker = drawPanel.hitTest(pt) {
                            if let curve = marker as? PageMarkerCurve {
                                state = .curve(marker: curve)
                            } else if let rect = marker as? PageMarkerRect {
                                state = .rect(marker: rect)
                            } else if let text = marker as? PageMarkerText {
                                state = .moveText(marker: text)
                            }
                        } else {
                            state = .preview
                            if !toolbarView.isHidden {
                                onCloseToolbar(sender)
                            }
                        }
                    default:
                        break
                }
                
                switch state {
                    case .curve(let marker):
                        marker.points = [imagePoint]
                        drawPanel.update(marker: marker)
                        state = .curve(marker: marker)
                    case .rect(let marker):
                        marker.ltPoint = imagePoint
                        drawPanel.update(marker: marker)
                        state = .rect(marker: marker)
                    case .moveText(let marker):
                        marker.points = [imagePoint]
                        drawPanel.update(marker: marker)
                        state = .moveText(marker: marker)
                    default: break
                }
            case .changed:
                switch state {
                    case .curve(let marker):
                        marker.points.append(imagePoint)
                        drawPanel.update(marker: marker)
                        state = .curve(marker: marker)
                    case .rect(let marker):
                        marker.rbPoint = imagePoint
                        drawPanel.update(marker: marker)
                        state = .rect(marker: marker)
                    case .moveText(let marker):
                        marker.points = [imagePoint]
                        drawPanel.update(marker: marker)
                        state = .moveText(marker: marker)
                    default:
                        let d = sender.translation(in: self.imgPanel)
                        pageImage.transform = pageImage.transform.concatenating(CGAffineTransform(translationX: d.x, y: d.y))
                        drawPanel.transform = drawPanel.transform.concatenating(CGAffineTransform(translationX: d.x, y: d.y))
                        sender.setTranslation(CGPoint.zero, in: self.imgPanel)
                        updateMarkers()
                }
            case .ended:
                switch state {
                    case .moveText:
                        //state = .preview
                        break
                    default:
                        break
                }
            default:
                break
        }
    }
    
    @IBAction func onTap(_ sender: UITapGestureRecognizer) {
        switch state {
            case .preview, .curve, .rect, .moveText:
                if sender.state == .ended {
                    tryTapOnMarker(tap: sender)
                }
            case .link, .notice, .change:
                switch sender.state {
                    case .ended:
                        let pt = sender.location(in: pageImage)
                        //pageImage.convert(CGPoint.zero, to: .)
                        let imgRect = AVMakeRect(aspectRatio: pageImage.image!.size, insideRect: pageImage.bounds)
                        if imgRect.contains(pt) {
                            let imagePt = pageImage.convertPoint(fromViewPoint: pt)
                            switch state {
                                case .link: onCreateLink(at: imagePt)
                                case .notice: onCreateNote(at: imagePt)
                                case .change: onCreateChange(at: imagePt)
                                default: fatalError()
                            }
                        } else {
                            state = .preview
                        }
                    
                    default: break
                }
            default:
                break
        }
        
    }
    
    private func refresh() {
        let oldCurrIdx = activePageIdx
        state = .preview
        self.drawPanel.clear()
        showSpinner()
        pageImage.transform = CGAffineTransform.identity
        drawPanel.transform = CGAffineTransform.identity
        self.markers.forEach{$0.view?.removeFromSuperview()}
        self.markers = []
        
        btnPageCursor.setTitle("\(activePageIdx + 1)/\(pages.count)", for: .normal)
        activePage = pages[activePageIdx].p
        if let p = activePage, let u = URL(string: p.url) {
            let onLoaded: (_ img: UIImage?, _ error: NSError?, _ t: CacheType, _ url: URL?) -> () = { _, error, _, _ in
                if let e = error {
                    self.hideSpinner()
                    self.btnTools.isEnabled = false
                    self.btnTools.alpha = 0.4
                    self.displayError("Сбой загрузки страницы", e)
                } else {
                    self.minimalSize = AVMakeRect(aspectRatio: self.pageImage.image!.size, insideRect: self.pageImage.bounds).size
                    if p.descriptors == nil {
                        DocumentAPI.loadMarkers(docId: p.docId, pageId: p.id) { err, markers in
                            if let m = markers {
                                self.markers = m
                                self.updateMarkers()
                            }
                            if oldCurrIdx == self.activePageIdx {
                                self.hideSpinner()
                            }
                        }
                    } else if oldCurrIdx == self.activePageIdx {
                        self.updateMarkers()
                        self.hideSpinner()
                    }
                }
            }
            
            pageImage.kf.setImage(with: u, placeholder: nil,
                                  options: [.transition(ImageTransition.fade(1)), .requestModifier(imageRequestModifier)],
                                  completionHandler: onLoaded
                
            )
        }
    }
    
    private func checkMarkerChanges(completion: @escaping () -> ()) {
        
        var hasChanges = false
        for marker in markers {
            if let m = marker as? DrawableMarker, m.isChanged {
                hasChanges = true
                break
            }
        }
        
        if !hasChanges {
            completion()
            return
        }
        
        let dialogMessage = UIAlertController(title: "", message: "Есть не сохранненые изменения в макрерах", preferredStyle: .alert)
        
        let exitAction = UIAlertAction(title: "Перейти без сохранения", style: .default, handler: { (action) -> Void in
            completion()
        })
        
        let saveAction = UIAlertAction(title: "Сохранить", style: .default) { (action) -> Void in
            for marker in self.markers {
                if let m = marker as? DrawableMarker, m.isChanged {
                    self.saveMarker(marker: m as! PageMarker)
                }
            }
            completion()
        }
        
        let canceAction = UIAlertAction(title: "Отмена", style: .default) { (action) -> Void in
           
        }
        
        //Add OK and Cancel button to dialog message
        dialogMessage.addAction(exitAction)
        dialogMessage.addAction(saveAction)
        dialogMessage.addAction(canceAction)
        
        // Present dialog message to user
        self.present(dialogMessage, animated: true, completion: nil)
    }
    
    @IBAction func onBtnLeft(_ sender: UIButton) {
        if activePageIdx > 0 {
            checkMarkerChanges() {
                self.activePageIdx -= 1
                self.refresh()
            }
        }
    }
    
    @IBAction func onBtnRight(_ sender: UIButton) {
        if activePageIdx + 1 < pages.count {
            checkMarkerChanges() {
                self.activePageIdx += 1
                self.refresh()
            }
        }
    }
    
    @IBAction func onBtnTools(_ sender: UIButton) {
        let changeEnabled = doc!.findPageVersions(pageNumber: activePage!.number).count > 1
        let menuItems: [PopupMenuItemProtocol] = [
            
            PopupMenuItem(title: "Изменение", icon: UIImage(named: "content-cut"), enabled: changeEnabled) { item in
                self.state = .change
            },
            PopupMenuItem(title: "Ссылка", icon: UIImage(named: "link_white")) { item in
                self.state = .link
            },
            PopupMenuItem(title: "Замечание", icon: UIImage(named: "star_white")) { item in
                self.state = .notice
            },
            PopupMenuItem(title: "-") { item in
                
            },
            PopupMenuItem(title: "Текст", icon: UIImage(named: "format-title")) { item in
                let marker = PageMarkerText(ownerDocId: self.doc!.id, ownerPageId: self.activePage!.id)
                var pos = self.editTextMarker.frame.origin
                pos.x = self.view.frame.size.width/2
                let pt = self.view.convert(CGPoint(x: pos.x, y: pos.y), to: self.pageImage)
                marker.points = [self.pageImage.convertPoint(fromViewPoint: pt)]
                self.state = .editText(marker: marker)
            },
            PopupMenuItem(title: "Линия", icon: UIImage(named: "lead-pencil")) { item in
                let marker = PageMarkerCurve(ownerDocId: self.doc!.id, ownerPageId: self.activePage!.id)
                self.state = .curve(marker: marker)
            },
            PopupMenuItem(title: "Прямоугольник", icon: UIImage(named: "vector-rectangle")) { item in
                let marker = PageMarkerRect(ownerDocId: self.doc!.id, ownerPageId: self.activePage!.id)
                self.state = .rect(marker: marker)
            }
        ]
       
        let at = CGPoint(x: btnTools.frame.origin.x, y: topBar.frame.height - 1)
        let at2 = view.convert(at, from: topBar)
        PopupMenuViewController.popup(over: self, at: at2, menuItems: menuItems)
        
    }
    
    func displayError(_ cap: String, _ error: Error) {
        let dlg = Session.makeErrorDialog(cap, message: error.localizedDescription)
        self.present(dlg, animated: true)
    }
}

extension PageAnnotatesViewController :  UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
}

extension PageAnnotatesViewController {
    private func uploadFiles(_ urls: [URL], completion: @escaping ( _ files: [Page]?, _ error: Error?) -> ()) {
        let progressView = ProgressViewController.show(over: self, caption: "Выгрузка ресурсов", progress: 0)
        let onProgress:(_ progress: Progress) -> () = { progress in
            progressView.progress = Float(progress.fractionCompleted)
        }
        FileAPI.uploadFiles(files: urls, uploadProgress: onProgress) {
            files, error in
            progressView.dismiss(animated: false){ completion(files, error)}
        }
    }
}

//PROGRESS SPINNER
extension PageAnnotatesViewController {
    func showSpinner() {
        btnTools.isEnabled = false
        btnTools.alpha = 0.3
        spinner.startAnimating()
    }
    
    func hideSpinner() {
        btnTools.isEnabled = true
        btnTools.alpha = 1.0
        spinner.stopAnimating()
    }
    
}

//LINK
extension PageAnnotatesViewController  {
    func onCreateLink(at: CGPoint) {
        PageLinkViewController.show(self, project: project!) { res in
            self.state = .preview
            switch res {
                case .pick(let doc, let page):
                    let linkItem = PageMarkerLink(ownerDocId: self.doc!.id, ownerPageId: self.activePage!.id,
                                                  position: at, refDoc: doc, refPage: page)
            
                    DocumentAPI.createMarker(docId: self.doc!.id, pageId: self.activePage!.id, marker: linkItem) { err, _ in
                        guard err == nil else {
                            self.displayError("Создание объекта линк", err!)
                            return
                        }
                        self.markers.insert(linkItem, at: 0)
                        self.updateMarkers()
                    }
                default: break
            }
        }
    }
    
    func handleLinkClick(link: PageMarkerLink) {
        DocumentPageViewController.show(self, mode: .pageLink(project: project!, link: link)) {
            res in
            switch res {
                case .ok:
                    break
                
                case .delete:
                    self.showSpinner()
                    DocumentAPI.deleteMarker(marker: link) { err in
                        self.hideSpinner()
                        if let e = err {
                            self.displayError("Удаление", e)
                        } else {
                            if let idx = self.markers.firstIndex(where: {$0.id == link.id}) {
                                link.view?.removeFromSuperview()
                                self.markers.remove(at: idx)
                            }
                        }
                    }
                
                case .go(_, let p):
                    if let u = URL(string: p.url) {
                        self.markers.forEach{$0.view?.removeFromSuperview()}
                        self.markers = []
                        self.drawPanel.clear()
                        
                        let onLoaded: (_ img: UIImage?, _ error: NSError?, _ t: CacheType, _ url: URL?) -> () = { _, _, _, _ in
                            if p.descriptors == nil {
                                DocumentAPI.loadMarkers(docId: p.docId, pageId: p.id) { err, markers in
                                    if let m = markers {
                                        self.markers = m
                                        self.updateMarkers()
                                    }
                                }
                            }
                        }
                        self.pageImage.kf.setImage(with: u, placeholder: nil,
                                                   options: [.transition(ImageTransition.fade(1)), .requestModifier(imageRequestModifier)],
                                                   completionHandler: onLoaded
                            
                        )
                    }
            }
        }
    }
}

//NOTE
extension PageAnnotatesViewController  {
    func onCreateNote(at: CGPoint) {
        PageNoteViewController.show(self, project: project!.project) { res in
            self.state = .preview
            switch res {
                case .note(let note, let isDraft, let images):
                    let createMarker: (_ pages: [Page]) -> () = { pages in
                        var currIdx = 0
                        let allPages: [Page] = images.reduce(into: []) { acc, item in
                            switch item {
                                case .page(let p):
                                    acc.append(p)
                                case .url(_):
                                    acc.append(pages[currIdx])
                                    currIdx += 1
                            }
                        }
                        
                        let noteItem = PageMarkerNote(ownerDocId: self.doc!.id, ownerPageId: self.activePage!.id,
                                                  position: at, note: note, images: allPages.map {$0.id}, isDraft: isDraft)
                        
                        DocumentAPI.createMarker(docId: self.doc!.id, pageId: self.activePage!.id, marker: noteItem) { err, createdMarker in
                            guard err == nil else {
                                self.displayError("Создание объекта замечание", err!)
                                return
                            }
                            self.markers.insert(createdMarker!, at: 0)
                            self.updateMarkers()
                        }
                    }
                    let localImages: [URL] = images.reduce(into: []) { acc, item in
                        switch item {
                        case .url(let u): acc.append(u)
                        default: break
                        }
                    }
                    if localImages.count > 0 {
                        self.uploadFiles(localImages) { files, error in
                            guard error == nil else {
                                self.displayError("Создание замечания", error!)
                                return
                            }
                            createMarker(files!)
                        }
                    } else {
                        createMarker([])
                    }
                
                default: break
            }
        }
    }
    
    func handleNoteClick(note: PageMarkerNote) {
        
        PageNotePreviewController.show(self, src: note, project: project!.project) { res in
            self.state = .preview
            switch res {
                case .ok:
                    break
                
            case .delete:
                self.showSpinner()
                DocumentAPI.deleteMarker(marker: note) { err in
                    self.hideSpinner()
                    if let e = err {
                        self.displayError("Удаление", e)
                    } else {
                        if let idx = self.markers.firstIndex(where: {$0.id == note.id}) {
                            note.view?.removeFromSuperview()
                            self.markers.remove(at: idx)
                        }
                    }
                }
                
                case .note(let newNote, let isDraft, let images):
                    let createMarker: (_ pages: [Page]) -> () = { pages in
                        var currIdx = 0
                        let allPages: [Page] = images.reduce(into: []) { acc, item in
                            switch item {
                            case .page(let p):
                                acc.append(p)
                            case .url(_):
                                acc.append(pages[currIdx])
                                currIdx += 1
                            }
                        }
                        note.note = newNote
                        note.images = allPages.map {$0.id}
                        note.isDraft = isDraft
            
                        DocumentAPI.updateMarker(marker: note) { err in
                            guard err == nil else {
                                self.displayError("Изменение объекта замечание", err!)
                                return
                            }
                        }
                    }
                    let localImages: [URL] = images.reduce(into: []) { acc, item in
                        switch item {
                        case .url(let u): acc.append(u)
                        default: break
                        }
                    }
                    
                    if localImages.count > 0 {
                        self.uploadFiles(localImages) { files, error in
                            guard error == nil else {
                                self.displayError("Редактирование замечания", error!)
                                return
                            }
                            createMarker(files!)
                        }
                    } else {
                        createMarker([])
                    }
                    
                

            }
        }
    }
}

//CHANGE
extension PageAnnotatesViewController  {
    func onCreateChange(at: CGPoint) {
        PageChangeViewController.show(self, doc: doc!, page: activePage!) { res in
            self.state = .preview
            switch res {
                case .ok(let note, _, let p):
                    let changeItem = PageMarkerChange(ownerDocId: self.doc!.id, ownerPageId: self.activePage!.id,
                                                      note: note, position: at, refDoc: self.doc!, refPage: p)
                    
                    DocumentAPI.createMarker(docId: self.doc!.id, pageId: self.activePage!.id, marker: changeItem) { err, _ in
                        guard err == nil else {
                            self.displayError("Создание объекта \"изменение\"", err!)
                            return
                        }
                        self.markers.insert(changeItem, at: 0)
                        self.updateMarkers()
                    }
                
                default:
                    break
            }
        }
    }
    
    func handleChangeClick(change: PageMarkerChange) {
        
        DocumentPageViewController.show(self, mode: .pageChange(project: project!, change: change)) {
            res in
            switch res {
                case .ok: break
                
                case .delete:
                    self.showSpinner()
                    DocumentAPI.deleteMarker(marker: change) { err in
                        self.hideSpinner()
                        if let e = err {
                            self.displayError("Удаление", e)
                        } else {
                            if let idx = self.markers.firstIndex(where: {$0.id == change.id}) {
                                change.view?.removeFromSuperview()
                                self.markers.remove(at: idx)
                            }
                        }
                    }
                
            
                case .go(_, let p):
                    if let idx = self.pages.firstIndex(where:{$0.p.id == p.id}) {
                        self.activePageIdx = idx
                        self.refresh()
                    }
            }
        }
    }
}

extension PageAnnotatesViewController: ColorPickerSliderDelegate {
    func colorPickerSlider(didSelect color: UIColor) {
        switch state {
            case .curve(let marker):
                marker.color = color
                state = .curve(marker: marker)
                drawPanel.update(marker: marker)
            case .rect(let marker):
                marker.color = color
                state = .rect(marker: marker)
                drawPanel.update(marker: marker)
            case .moveText(let marker):
                marker.color = color
                state = .moveText(marker: marker)
                drawPanel.update(marker: marker)
            default:
                break
        }
    }
}

extension PageAnnotatesViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        guard !(textField.text ?? "").isEmpty else {
            state = .preview
            return true
        }
        
        switch state {
            case .editText(let marker):
                marker.text = textField.text!
                state = .moveText(marker: marker)
                
                drawPanel.update(marker: marker)
                openDrawableMarkerToolbar()
            default:
                break
        }
        
        return true
    }
}

// share image to chat
extension PageAnnotatesViewController {
    func shareImage(_ image: UIImage) {
        let controller = RoomsViewController()
        controller.setShareImages(images: [image])
        self.present(controller, animated: true)
    }
}

extension PageAnnotatesViewController: PageAnnotatesViewDelegate {
    func pageAnnotatesView(_ view: PageAnnotatesView, convertToViewPoints points: [CGPoint]) -> [CGPoint] {
        return points.map { pageImage.convertPoint(fromImagePoint: $0) }
    }
}
