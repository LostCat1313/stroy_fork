//
//  PageAnnotatesView.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 26.09.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit

protocol PageAnnotatesViewDelegate {
    func pageAnnotatesView(_ view: PageAnnotatesView, convertToViewPoints points: [CGPoint]) -> [CGPoint]
}

class PageAnnotatesView: UIView {
    private var markers: [UUID: DrawableMarker] = [:]
    var changedMarkerAlpha: CGFloat = 0.5
    
    var delegate: PageAnnotatesViewDelegate!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.clear
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        backgroundColor = UIColor.clear
    }
    
    func update(marker: DrawableMarker) {
        markers[marker.uuid] = marker
        setNeedsDisplay()
    }
    
    func clear() {
        markers = [:]
        setNeedsDisplay()
    }
    
    func remove(marker: DrawableMarker) {
        markers.removeValue(forKey: marker.uuid)
        setNeedsDisplay()
    }
    
    func bezierQuad(_ beg: CGPoint, _ end: CGPoint, _ cp: CGPoint, _ lp: CGPoint?, roughRadius: CGFloat) -> [CGPoint] {
        let step = 5
        let d: CGFloat = 1.0 / CGFloat(step)
        var t: CGFloat = 0.0
        var res: [CGPoint] = []
        var prev = lp
        for _ in 0...step - 1 {
            let revT = 1.0 - t
            let curr = CGPoint(x: revT * revT * beg.x + 2 * t * revT * cp.x + t * t * end.x,
                               y: revT * revT * beg.y + 2 * t * revT * cp.y + t * t * end.y)
            if prev == nil || (fabs(curr.x - prev!.x) > roughRadius || fabs(curr.y - prev!.y) > roughRadius) {
                res.append(curr)
            }
            prev = curr
            t += d
        }
        return res;
    }
    
    func bezierLineQuadConvert(src: [CGPoint], roughRadius: CGFloat = 1) -> [CGPoint] {
        if src.count > 2 {
            var res: [CGPoint] = []
            res.append(src[0])
            for idx in 0...src.count - 3 {
                let cnt = src.count
                let beg = CGPoint(x: (src[idx].x + src[(idx + 1) % cnt].x) / 2, y: (src[idx].y + src[(idx + 1) % cnt].y) / 2)
                let end = CGPoint(x: (src[((idx + 1) % cnt)].x + src[(idx + 2) % cnt].x) / 2, y: (src[(idx + 1) % cnt].y + src[(idx + 2) % cnt].y) / 2)
                res += bezierQuad(beg, end, src[(idx + 1) % cnt], res.last, roughRadius: roughRadius)
            }
            res.append(src.last!)
            return res
        } else {
            return src
        }
    }
    
    fileprivate func drawPoints(_ points: [CGPoint], _ width: CGFloat, _ color: UIColor, useSpline: Bool = true) -> UIBezierPath? {
        if (points.count > 0) {
            let p = UIBezierPath()
            let rPath = useSpline ? bezierLineQuadConvert(src: points, roughRadius: 1) : points
            for (idx, point) in rPath.enumerated() {
                if idx == 0 {
                    p.move(to: point)
                } else {
                    p.addLine(to: point)
                }
            }
            UIColor.clear.setFill()
            color.setStroke()
            p.fill()
            p.lineWidth = width
            p.lineJoinStyle = .round
            p.lineCapStyle = .round
            p.stroke()
            return p
        }
        return nil
    }
    
    fileprivate func drawText(_ text: String, point: CGPoint, font: UIFont, color: UIColor = UIColor.black) -> CGRect {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        
        let attributes = [
            NSAttributedStringKey.paragraphStyle: paragraphStyle,
            NSAttributedStringKey.font: font,
            NSAttributedStringKey.foregroundColor: color
        ]
        
        let attributedString = NSAttributedString(string: text, attributes: attributes)
        
        let maxTextWidth = self.frame.size.width / self.transform.a
        let size = text.size(withAttributes: attributes)
        var width = size.width
        var height = size.height
        
        if size.width > maxTextWidth {
            width = maxTextWidth
            let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
            let boundingBox = attributedString.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
            height = boundingBox.height
        }
        
        let rect = CGRect(x: point.x - width/2, y: point.y - height/2, width: width, height: height)
        attributedString.draw(in: rect)
        return rect
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        for (_, marker) in markers {
            let points = delegate.pageAnnotatesView(self, convertToViewPoints: marker.points)
            
            if points.count == 0 || marker.isHidden {
                continue
            }
        
            let color = marker.isChanged ? marker.color.withAlphaComponent(changedMarkerAlpha) : marker.color.withAlphaComponent(marker.alpha ?? 1.0)
            _ = drawPoints(points, marker.width, color, useSpline: marker.useSpline())
            if let text = marker.getText(), !text.isEmpty {
                _ = drawText(text, point: points[0], font: marker.font, color: color)
            }
        }
    }
    
    func hitTest(_ point: CGPoint) -> DrawableMarker? {
        
        for (_, marker) in markers {
            let points = delegate.pageAnnotatesView(self, convertToViewPoints: marker.points)
            
            if points.count == 0 || marker.isHidden {
                continue
            }
            
            if let text = marker.getText(), !text.isEmpty {
                let rect = drawText(text, point: points[0], font: marker.font)
                if rect.contains(point) {
                    return marker
                }
            } else if points.count > 1 {
                let dp: CGFloat = max(7, marker.width)
                var maxX = points[0].x
                var minX = points[0].x
                var maxY = points[0].y
                var minY = points[0].y
                for pt in points {
                    if pt.x < minX {
                        minX = pt.x
                    }
                    if pt.x > maxX {
                        maxX = pt.x
                    }
                    if pt.y < minY {
                        minY = pt.y
                    }
                    if pt.y > maxY {
                        maxY = pt.y
                    }
                }
                let rect = CGRect(x: minX - dp, y: minY - dp, width: 2*dp + maxX - minX, height: 2*dp + maxY - minY)
                if rect.contains(point) {
                    return marker
                }
            }
        }
        return nil
    }
    
    private func image(marker: DrawableMarker) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(self.frame.size, false, 0)
        
        let points = delegate.pageAnnotatesView(self, convertToViewPoints: marker.points)
        _ = drawPoints(points, marker.width, marker.color, useSpline: marker.useSpline())
        if let text = marker.getText(), !text.isEmpty {
            _ = drawText(text, point: points[0], font: marker.font, color: marker.color)
        }
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
    func renderToImage(_ sourceImage: UIImage, marker: DrawableMarker) -> UIImage {
        
        if let image = self.image(marker: marker) {
            let size = sourceImage.size
            let aspect = frame.size.height/frame.size.width
            let drawSize = CGSize(width: size.width, height: aspect*size.width)
            let dy = (size.height - drawSize.height)/2
            UIGraphicsBeginImageContextWithOptions(size, false, 0)
            sourceImage.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
            image.draw(in: CGRect(x: 0, y: dy, width: drawSize.width, height: drawSize.height))
            let result = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return result!
        }
        return sourceImage
    }
}
