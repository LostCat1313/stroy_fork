//
//  PageMarkerCurve.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 28.09.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class PageMarkerCurve: PageMarker, DrawableMarker {
    var uuid: UUID = UUID()
    var points: [CGPoint] = [] {
        didSet {
            isChanged = true
        }
    }
    
    var width: CGFloat = 5.0 {
        didSet {
            isChanged = true
        }
    }
    
    var color = UIColor.black {
        didSet {
            isChanged = true
        }
    }
    
    var font = UIFont.systemFont(ofSize: 12.0) {
        didSet {
            isChanged = true
        }
    }
    
    var isChanged: Bool = true
    
    private var __alpha: CGFloat?
    override var alpha: CGFloat? {
        get {
            return __alpha
        }
        
        set {
            __alpha = newValue
        }
    }
    
    private var __isHidden: Bool = false
    override var isHidden: Bool {
        get {
            return __isHidden
        }
        
        set {
            __isHidden = newValue
        }
    }
    
    init(ownerDocId: Int, ownerPageId: Int, points: [CGPoint] = []) {
        super.init(ownerDocId: ownerDocId, ownerPageId: ownerPageId, text: "", title: "")
    }
    
    override init?(json: JSON) {
        super.init(json: json)
        
        if let params = json["params"].dictionaryObject {
            self.points = []
            if let pointsData = params["points"] as? [[String: Any]] {
                for pointData in pointsData {
                    let x = CGFloat((pointData["x"] as! NSNumber).floatValue)
                    let y = CGFloat((pointData["y"] as! NSNumber).floatValue)
                    let point = CGPoint(x: x, y: y)
                    points.append(point)
                }
            }
            
            if let hex = params["color"] as? String {
                self.color = UIColor.init(hex: hex)!
            }
            
            self.width = CGFloat((params["width"] as! NSNumber).floatValue)
        }
        isChanged = false
    }
    
    override func getType() -> PageMarkerTypeId {
        return .curve
    }
    
    override func getView() -> UIView? {
        return nil
    }
    
    func useSpline() -> Bool {
        return true
    }
    
    func getText() -> String? {
        return nil
    }
    
    override func getParams() -> [String: Any] {
        var params: [String: Any] = [:]
        var pointsData: [[String: Float]] = []
        for point in points {
            var data: [String: Float] = [:]
            data["x"] = Float(point.x)
            data["y"] = Float(point.y)
            pointsData.append(data)
        }
        
        params["points"] = pointsData
        params["color"] = "#" + color.toARGBString()
        params["width"] = width
        return params
    }
}
