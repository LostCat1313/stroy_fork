//
//  PageLinkViewController.swift
//  axioma
//
//  Created by sss on 27.08.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit
enum PageLinkResult {
    case cancel
    case pick(doc: Document, page: Page)
}

class PageLinkViewController: UIViewController {
    @IBOutlet weak var cvDocs: UICollectionView!
    var project : ProjectModel.View?
    var folderStates: [Int: Bool] = [:]
    var docs: [DocumentsItem] = []
    var result: ((_ res: PageLinkResult)->())?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
         cvDocs.register(UINib(nibName: "DocumentsItemCell", bundle: nil), forCellWithReuseIdentifier: "DocumentsItemCell")
        refreshDocs()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func refreshDocs() {
        func collectDocs(acc: inout [DocumentsItem], doc: DocumentsItem, depth: Int) {
            doc.depth = depth
            if let dir = doc as? DocumentsDir {
                dir.expanded = folderStates[dir.doc.id] ?? dir.expanded
            }
            acc.append(doc)
            if let dir = doc as? DocumentsDir, dir.expanded {
                for subdoc in dir.children {
                    collectDocs(acc: &acc, doc: subdoc, depth: depth + 1)
                }
            }
        }
        let roots: [DocumentsItem] = project!.documents.reduce(into: []) { acc, doc in
            acc.append(DocumentsItem.create(doc, delegate: self))
        }
        docs = []
        for doc in roots {
            collectDocs(acc: &docs, doc: doc, depth: 0)
        }
        cvDocs.reloadData()
    }
    
    @IBAction func onBtnCancel(_ sender: UIButton) {
        dismiss(animated: true, completion: {self.result?(.cancel)})
    }
    
    public static func show(_ over: UIViewController, project: ProjectModel.View,
                            result: @escaping ((_ res: PageLinkResult)->())) {
        let controller = PageLinkViewController()
        controller.project = project
        controller.result = result
        over.present(controller, animated:true)
    }
}

extension PageLinkViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return docs.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: DocumentsItemCell = cvDocs.dequeueReusableCell(withReuseIdentifier: "DocumentsItemCell", for: indexPath) as! DocumentsItemCell
        cell.configure(cell: docs[indexPath.item])
        return cell
    }
}

extension PageLinkViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: cvDocs.frame.width, height: 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
}

extension PageLinkViewController: DocumentsCellDelegate {
    func documentsCell(didTap cell: DocumentsDoc) {
        DocumentPagePickerViewController.show(over: self, doc: cell.doc) { pickedPage in
            self.dismiss(animated: true, completion: {
                self.result?(.pick(doc: cell.doc, page: pickedPage))
            })
        }
    }
    
    func documentsCell(didTap cell: DocumentsDir, isExpandTap: Bool) {
        if isExpandTap {
            folderStates[cell.doc.id] = !cell.expanded
            refreshDocs()
        }
    }
    
    func documentsCell(didPress cell: DocumentsItem) {
    }
    
}
