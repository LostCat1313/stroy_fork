import Foundation
import UIKit
import SwiftyJSON

enum PageMarkerTypeId : Int {
    case edit = 0
    case link = 1
    case note = 2
    case text = 3
    case rect = 4
    case curve = 5
}

class PageMarker {
    let id: Int
    var text: String
    let title: String
    let ownerDocId: Int
    let ownerPageId: Int
    let createdBy: Int
    let created: Date
    
    var isSaved: Bool {
        return id != 0
    }
    
    init(ownerDocId: Int, ownerPageId: Int, createdBy: Int = Session.user?.id ?? 0, text: String, title: String) {
        self.id = 0
        self.text = text
        self.title = title
        self.ownerDocId = ownerDocId
        self.ownerPageId = ownerPageId
        self.createdBy = createdBy
        self.created = Date()
    }
    
    init?(json: JSON) {
        self.id = json["id"].intValue
        self.ownerDocId = json["docId"].intValue
        self.ownerPageId = json["holderId"].intValue
        self.text = json["text"].stringValue
        self.title = json["title"].stringValue
        self.createdBy = json["createdBy"].intValue
        let c = json["created"].stringValue
        self.created = c.count >= 19 ? c.prefix(19).getDate(dateFormat: "yyyy-MM-dd'T'HH:mm:ss") : Date()
    }
    
    var type: PageMarkerTypeId {
        return getType()
    }
    
    var view: UIView? {
        return getView()
    }
    
    var alpha: CGFloat? {
        get {
            return self.view?.alpha
        }
        
        set {
            self.view?.alpha = newValue ?? 1.0
        }
    }
    
    var isHidden: Bool {
        get {
            return self.view?.isHidden ?? true
        }
        
        set {
            self.view?.isHidden = newValue
        }
    }
    
    func getView() -> UIView? {
        return nil
    }
    
    func getType() -> PageMarkerTypeId {
        fatalError("This method should be overwritten.")
    }
    
    func getParams() -> [String: Any] {
        fatalError("This method should be overwritten.")
    }
    
    public static func create(json: JSON) -> PageMarker? {
        if let type = PageMarkerTypeId(rawValue: json["type"].intValue) {
            switch type {
                case .edit:
                    return PageMarkerChange(json: json)
                case .link:
                    return PageMarkerLink(json: json)
                case .note:
                    return PageMarkerNote(json: json)
                case .curve:
                    return PageMarkerCurve(json: json)
                case .rect:
                    return PageMarkerRect(json: json)
                case .text:
                    return PageMarkerText(json: json)
            }
        } else {
            fatalError("Unknown page marker format.")
        }
    }
    
    func update(at: CGPoint, owner: UIView)  {
        fatalError("Этот метод должен быть перекрыт.")
    }
    
}

protocol DrawableMarker {
    var points: [CGPoint] {get set}
    var width: CGFloat {get set}
    var color: UIColor {get set}
    var font: UIFont {get set}
    var uuid: UUID {get set}
    func useSpline() -> Bool
    func getText() -> String?
    var isChanged: Bool {get set}
    var alpha: CGFloat? {get set}
    var isHidden: Bool {get set}
}
