//
//  PageNoticeViewController.swift
//  axioma
//
//  Created by sss on 10.09.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit

enum PageNoteResult {
    case ok
    case delete
    case note(note: String, isDraft: Bool, images: [PageNoteUrl])
}

enum PageNoteUrl {
    case url(_ url: URL)
    case page(_ page: Page)
}

class PageNoteViewController: UIViewController {
    var result: ((_ res: PageNoteResult)->())?
    var images: [PageNoteUrl] = []
    var lastPickIdx = -1
    var loading = false
    var src: PageMarkerNote?
    var project: ProjectData!
    var chargerMode = false
    
    @IBOutlet weak var cvFiles: UICollectionView!
    @IBOutlet weak var btnOK: UIButton!
    @IBOutlet weak var notice: UITextField!
    @IBOutlet weak var saveTypeView: UIView!
    @IBOutlet weak var photoTitle: UILabel!
    @IBOutlet weak var spinnerView: UIView!
    
    var isDraft: Bool {return src?.isDraft ?? true}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        notice.delegate = self
        notice.text = src?.note
        if isDraft {
            notice.isEnabled = true
            notice.becomeFirstResponder()
            photoTitle.text = "Прикрепить фото"
            notice.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        } else {
            notice.isEnabled = false
            photoTitle.text = "Прикрепленные фото"
        }
        if let s = src {
            loading = true
            var onDepth: ((_ idx: Int) -> ())?
            onDepth = { idx in
                if idx < s.images.count {
                    FileAPI.getFilePage(fileId: s.images[idx]) { page, error in
                        onDepth?(idx + 1)
                        if error == nil {
                            self.images.append(.page(page!))
                            self.cvFiles.reloadData()
                        }
                    }
                } else {
                    self.loading = false
                    if self.isDraft {  //enable + button
                        self.cvFiles.reloadData()
                    }
                }
            }
            onDepth?(0)
        }
       
        cvFiles.register(UINib(nibName: "CreateDocumentPageViewCell", bundle: nil), forCellWithReuseIdentifier: "CreateDocumentPageViewCell")
        configureUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func displayError(_ error: Error) {
        let description = error.localizedDescription
        let dlg = Session.makeErrorDialog("Error", message: description)
        self.present(dlg, animated: true)
    }
    
    public static func show(_ over: UIViewController, project: ProjectData, src: PageMarkerNote? = nil, result: @escaping (_ res: PageNoteResult)->()) {
        let controller = PageNoteViewController()
        controller.result = result
        controller.src = src
        controller.project = project
        over.present(controller, animated:true)
    }
    
    func configureUI() {
        if (notice.text?.count ?? 0) > 0 {
            btnOK.isEnabled = true
            btnOK.alpha = 1
        } else {
            btnOK.isEnabled = false
            btnOK.alpha = 0.4
        }
    }
    
    @IBAction func onBtnOK(_ sender: UIButton) {
        if isDraft {
            saveTypeView.isHidden = false
        } else {
            dismiss(animated: true, completion: {self.result?(.ok)})
        }
    }
    
    @IBAction func onBtnCancel(_ sender: UIButton) {
        notice.resignFirstResponder()
        dismiss(animated: true, completion: {self.result?(.ok)})
    }
    
    @IBAction func onBtnSaveDraft(_ sender: UIButton) {
        let res = PageNoteResult.note(note: notice.text!, isDraft: true, images: images)
        dismiss(animated: true, completion: {self.result?(res)})
    }
    
    @IBAction func onBtnSaveNote(_ sender: UIButton) {
        let res = PageNoteResult.note(note: notice.text!, isDraft: false, images: images)
        dismiss(animated: true, completion: {self.result?(res)})
    }
    
    @IBAction func onBtnCancelSave(_ sender: UIButton) {
        saveTypeView.isHidden = true
    }
    
    @IBAction func onBtnCharger(_ sender: UIButton) {
        showSpinner()
        chargerMode = true
        UserAPI.getUsers { allUsers, error in
            self.hideSpinner()
            if error == nil {
                let validUsers = allUsers!.filter {$0.firstName != nil && $0.lastName != nil}
                let rights: [UserRightsInfo] = ProjectsViewWorker.projectUserRights(project: self.project, allUsers: validUsers).reduce(into: []) { acc, info in
                    switch info.state {
                        case .included(_, let roleMode):
                            switch roleMode {
                                case .enabled(let role):
                                    if role.canWrite {
                                        let newItem = UserRightsInfo(user: info.user, caption: info.caption,
                                                                     state: .excluded(readOnly: false, roleMode: .disabled))
                                        acc.append(newItem)
                                    }
                                default:
                                    break
                            }
                        
                        default:
                            break
                    }
                }
                UsersPickerViewController.show(self, delegate: self, users: rights)
            } else {
                self.displayError(error!)
            }
        }
    }
    
   
    @IBAction func onBtnSubscribers(_ sender: Any) {
        showSpinner()
        chargerMode = false
        UserAPI.getUsers { allUsers, error in
            self.hideSpinner()
            if error == nil {
                let validUsers = allUsers!.filter {$0.firstName != nil && $0.lastName != nil}
                let rights: [UserRightsInfo] = ProjectsViewWorker.projectUserRights(project: self.project, allUsers: validUsers).reduce(into: []) { acc, info in
                    switch info.state {
                    case .included(_, let roleMode):
                        switch roleMode {
                        case .enabled(let role):
                            if role.canWrite {
                                let newItem = UserRightsInfo(user: info.user, caption: info.caption,
                                                             state: .included(readOnly: false, roleMode: .disabled))
                                acc.append(newItem)
                            }
                        default:
                            break
                        }
                        
                    default:
                        break
                    }
                }
                UsersPickerViewController.show(self, delegate: self, users: rights)
            } else {
                self.displayError(error!)
            }
        }
        
    }
    
    @IBAction func onBkTap(_ sender: UITapGestureRecognizer) {
        switch sender.state {
            case .ended:
                notice.resignFirstResponder()
            default:
                break
        }
    }
}

extension PageNoteViewController {
    @objc func textFieldDidChange(_ textField: UITextField) {
        configureUI()
    }
}

extension PageNoteViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let url = info[UIImagePickerControllerImageURL] as? URL {
            picker.dismiss(animated: true, completion: nil)
            if lastPickIdx == images.count {
                images.append(.url(url))
            } else {
                images[lastPickIdx] = .url(url)
            }
            cvFiles.reloadData()
            configureUI()
        }
    }
}

extension PageNoteViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count + (isDraft && !loading ? 1 : 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: CreateDocumentPageViewCell = cvFiles.dequeueReusableCell(withReuseIdentifier: "CreateDocumentPageViewCell", for: indexPath) as! CreateDocumentPageViewCell
        if indexPath.item < images.count {
            switch images[indexPath.item] {
                case .url(let url):
                    cell.configure(indexPath.item, url: url, readOnly: !isDraft)
            
                case .page(let p):
                    cell.configure(indexPath.item, url: URL(string: p.thumbUrl)!, readOnly: !isDraft)
            }
        } else {
            cell.configure(indexPath.item, url: nil, readOnly: !isDraft)
        }
        
        cell.delegate = self
        return cell
    }
}

extension PageNoteViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w = (cvFiles.frame.width / 3) - 3
        return CGSize(width: w, height: w)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
}

extension PageNoteViewController: CreateDocumentDelegate {
    
    func createDocument(pickPage: Int) {
        if pickPage == images.count {
            lastPickIdx = pickPage
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                showSpinner()
                let picker = UIImagePickerController()
                picker.sourceType = .photoLibrary
                picker.delegate = self
                present(picker, animated: true, completion: hideSpinner)
            }
        } else {
            switch images[pickPage] {
                case .page(let p):
                    ImageViewerViewController.show(self, url: URL(string: p.url)!)
                case .url(let u):
                    ImageViewerViewController.show(self, url: u)
            }
        }
    }
    
    func createDocument(delPage: Int) {
        images.remove(at: delPage)
        cvFiles.reloadData()
        configureUI()
    }
    
}

//SPINNER
extension PageNoteViewController {
    func showSpinner() {
        spinnerView.isHidden = false
    }
    
    func hideSpinner() {
        spinnerView.isHidden = true
    }
}
extension PageNoteViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension PageNoteViewController: UsersPickerControllerDelegate {
    func usersPickerController(getCaptionFor picker: UsersPickerViewController) -> String {
        return chargerMode ? "Ответственный за замечание" : "Трекеры замечания"
    }
    
    func usersPickerController(getApplyCaptionFor picker: UsersPickerViewController) -> String {
        return chargerMode ? "НАЗНАЧИТЬ" : "УСТАНОВИТЬ"
    }
    
    func usersPickerController(apply users: [(user: User, caption: String, state: UserPickerItemState)]) {
        let rights: [(userId: Int, role: Rights.Role)] = users.reduce(into: []) { acc, item in
            switch item.state {
            case .included(_, let roleMode):
                switch roleMode {
                case .enabled(let role):
                    acc.append((userId: item.user.id, role: role))
                default:
                    break
                }
                
            default:
                break
            }
        }
        
    }
    
    func usersPickerController(canChangeFor picker: UsersPickerViewController) -> Bool {
        return project.isCurrentWriter
    }
    
    func usersPickerController(isRadioFor picker: UsersPickerViewController) -> Bool {
        return chargerMode ? true : false
    }
}
