//
//  PageMarkerText.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 28.09.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class PageMarkerText: PageMarker, DrawableMarker {
    static let defaultSize: CGFloat = 25
    static let defaultFont = UIFont.systemFont(ofSize: defaultSize)
    var uuid: UUID = UUID()
    
    var points: [CGPoint] = [] {
        didSet {
            isChanged = true
        }
    }
    
    var width: CGFloat = 5.0 {
        didSet {
            isChanged = true
        }
    }
    
    var color = UIColor.black {
        didSet {
            isChanged = true
        }
    }
    
    var font = defaultFont {
        didSet {
            isChanged = true
        }
    }
    
    var isChanged: Bool = true
    
    private var __alpha: CGFloat?
    override var alpha: CGFloat? {
        get {
            return __alpha
        }
        
        set {
            __alpha = newValue
        }
    }
    
    private var __isHidden: Bool = false
    override var isHidden: Bool {
        get {
            return __isHidden
        }
        
        set {
            __isHidden = newValue
        }
    }
    
    override func getType() -> PageMarkerTypeId {
        return .text
    }
    
    override func getView() -> UIView? {
        return nil
    }
    
    func useSpline() -> Bool {
        return false
    }
    
    func getText() -> String? {
        return self.text
    }
    
    init(ownerDocId: Int, ownerPageId: Int) {
        super.init(ownerDocId: ownerDocId, ownerPageId: ownerPageId, text: "", title: "")
    }
    
    override init?(json: JSON) {
        super.init(json: json)
        
        if let params = json["params"].dictionaryObject {
            if let pointData = params["point"] as? [String: Any] {
                let x = CGFloat((pointData["x"] as! NSNumber).floatValue)
                let y = CGFloat((pointData["y"] as! NSNumber).floatValue)
                points = [CGPoint(x: x, y: y)]
            }
            
            if let hex = params["color"] as? String {
                self.color = UIColor.init(hex: hex)!
            }
            
            //self.width = CGFloat(((params["width"] as? NSNumber) ?? 5.0).floatValue)
            
            if let fontName = params["fontName"] as? String {
                let fontSize = CGFloat((params["fontSize"] as! NSNumber).floatValue)
                font = UIFont(name: fontName, size: CGFloat(fontSize))!
            }
        }
        isChanged = false
    }
    
    override func getParams() -> [String: Any] {
        var params: [String: Any] = [:]
        let point = points.count > 0 ? points[0]: CGPoint()
        
        var data: [String: Float] = [:]
        data["x"] = Float(point.x)
        data["y"] = Float(point.y)
        
        params["point"] = data
        params["color"] = "#" + color.toARGBString()
        params["width"] = width
        params["fontSize"] = Float(font.pointSize)
        params["fontName"] = font.fontName
        return params
    }
}
