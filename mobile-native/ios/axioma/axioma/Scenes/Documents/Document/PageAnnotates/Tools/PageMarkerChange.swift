//
//  PageMarkerChange.swift
//  axioma
//
//  Created by sss on 27.09.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class PageMarkerChange : PageMarker {
    struct Design {
        static let iconSize: CGSize = CGSize(width: 32, height: 32)
        static let backColor: UIColor = Configuration.Colors.markerChangeBkColor
    }
    
    private var changeView: UIView?
    private var changeIcon: UIImageView?
    
    
    let refDocId: Int
    let refPageId: Int
    let position: CGPoint
    
    var note: String {
        get {
            return self.text
        }
        set (v) {
            self.text = v
        }
    }
    
    init(ownerDocId: Int, ownerPageId: Int, note: String, position: CGPoint, refDoc: Document, refPage: Page) {
        self.refDocId = refDoc.id
        self.refPageId = refPage.id
        self.position = position
        super.init(ownerDocId: ownerDocId, ownerPageId: ownerPageId, text: note, title: refDoc.title + " Страница \(refPage.number)")
    }
    
    override init?(json: JSON) {
        if let d = json["params"].dictionaryObject {
            self.refDocId = d["refDocId"] as! Int
            self.refPageId = d["refFileId"]  as! Int
            let x = CGFloat((d["x"] as! NSNumber).floatValue)
            let y = CGFloat((d["y"] as! NSNumber).floatValue)
            self.position = CGPoint(x: x, y: y)
            super.init(json: json)
        } else {
            return nil
        }
    }
    
    override func getParams() -> [String: Any] {
        var d: [String: Any] = [:]
        d["x"] = Int(self.position.x)
        d["y"] = Int(self.position.y)
        d["refDocId"] = refDocId
        d["refFileId"] = refPageId
        return d
    }
    
    override func getType() -> PageMarkerTypeId {
        return .edit
    }
    
    override func getView() -> UIView? {
        return changeView
    }
    
    override func update(at: CGPoint, owner: UIView) {
        if changeView == nil {
            changeView = UIView(frame: CGRect(origin: CGPoint.zero, size: CGSize.zero))
            changeIcon = UIImageView(frame: CGRect(origin: CGPoint.zero, size: Design.iconSize))
            
            changeIcon?.image = UIImage(named: "change_chev")
            changeIcon?.contentMode = .scaleAspectFit
            changeIcon?.frame.size = Design.iconSize
            changeView?.frame.size = Design.iconSize
            changeView?.clipsToBounds = true
            changeView?.addSubview(changeIcon!)
            owner.addSubview(changeView!)
        }
        let ofs = CGPoint(x: at.x - Design.iconSize.width / 2, y: at.y - changeView!.frame.height)
        changeView?.frame.origin = ofs
    }
    
}
