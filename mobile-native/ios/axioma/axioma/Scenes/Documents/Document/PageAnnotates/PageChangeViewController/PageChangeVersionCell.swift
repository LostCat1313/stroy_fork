//
//  PageChangeVersionCell.swift
//  axioma
//
//  Created by sss on 25.09.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit

class PageChangeVersionCell: UICollectionViewCell {

    @IBOutlet weak var btnItem: UIButton!
    @IBOutlet weak var radioIcon: UIImageView!
    
    var click: (()->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func onBtnItem(_ sender: UIButton) {
        click?()
    }
    
    func configure(v: Version, checked: Bool, click: @escaping ()->()) {
        let dateFmt = DateFormatter()
        dateFmt.dateFormat = "dd.MM.yyyy HH:mm"
        btnItem.setTitle(dateFmt.string(from: v.created), for: .normal)
        radioIcon.image = UIImage(named: checked ? "radiobox-marked" : "radiobox-blank")
        self.click = click
        
    }
}
