//
//  PageMarkerNotice.swift
//  axioma
//
//  Created by sss on 14.09.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class PageMarkerNote : PageMarker {
    struct Design {
        static let iconSize: CGSize = CGSize(width: 32, height: 32)
        static let backColor: UIColor = Configuration.Colors.markerNoticeBkColor
    }
    
    private var noteView: UIView?
    private var noteIcon: UIImageView?

    var note: String {
        get {
            return self.text
        }
        set (v) {
            self.text = v
        }
    }
    
    var images: [Int]
    let position: CGPoint
    var isDraft: Bool
    
    init(ownerDocId: Int, ownerPageId: Int, position: CGPoint, note: String, images: [Int], isDraft: Bool = true) {
        
        self.images = images
        self.position = position
        self.isDraft = isDraft
        super.init(ownerDocId: ownerDocId, ownerPageId: ownerPageId, text: note, title: "")
    }
    
    override init?(json: JSON) {
        if let d = json["params"].dictionaryObject {
            self.isDraft = d["isDraft"] as? Bool ?? false
            self.images = d["attachments"] as? [Int] ?? []
            let x = CGFloat((d["x"] as? NSNumber ?? NSNumber(value: 0.0)).floatValue)
            let y = CGFloat((d["y"] as? NSNumber ?? NSNumber(value: 0.0)).floatValue)
            self.position = CGPoint(x: x, y: y)
            super.init(json: json)
        } else {
            return nil
        }
    }
    
    override func getParams() -> [String: Any] {
        var d: [String: Any] = [:]
        d["x"] = self.position.x
        d["y"] = self.position.y
        d["isDraft"] = isDraft
        if images.count > 0 {
            d["attachments"] = images
        }
        return d
    }
    
    override func getType() -> PageMarkerTypeId {
        return .note
    }
    
    override func getView() -> UIView? {
        return noteView
    }
    
    override func update(at: CGPoint, owner: UIView) {
        if noteView == nil {
            noteView = UIView(frame: CGRect(origin: CGPoint.zero, size: CGSize.zero))
            noteIcon = UIImageView(frame: CGRect(origin: CGPoint.zero, size: Design.iconSize))
            
            noteIcon?.image = UIImage(named: isDraft ? "note_chev_gray" : "note_chev")
            noteIcon?.contentMode = .scaleAspectFit
            noteIcon?.frame.size = Design.iconSize
        
            noteView?.frame.size = Design.iconSize
            noteView?.clipsToBounds = true
            noteView?.addSubview(noteIcon!)
            owner.addSubview(noteView!)
        }
        let ofs = CGPoint(x: at.x - Design.iconSize.width / 2, y: at.y - noteView!.frame.height)
        noteView?.frame.origin = ofs
    }
    
    
}
