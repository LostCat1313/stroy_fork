//
//  DocumentsViewRouter.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 20.06.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation
import UIKit

protocol DocumentsViewRouterInput {
    
}

class DocumentsViewRouter: DocumentsViewRouterInput {
    weak var viewController: DocumentsViewController!
    
    // MARK: Navigation
    
    
    
    // MARK: Communication
    
    func passDataToNextScene(segue: UIStoryboardSegue) {
        // NOTE: Teach the router which scenes it can communicate with
        
        //if segue.identifier == "ShowSomewhereScene" {
        //    passDataToScene(segue)
        //}
    }
    
    func passDataToScene(segue: UIStoryboardSegue) {
        // NOTE: Teach the router how to pass data to the next scene
        
        // let someWhereViewController = segue.destinationViewController as! SomeWhereViewController
        // someWhereViewController.output.name = viewController.output.name
    }
}
