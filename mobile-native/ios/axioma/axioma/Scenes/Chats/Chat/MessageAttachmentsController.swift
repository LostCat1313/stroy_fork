//
//  MessageAttachmentsController.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 23.08.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit

protocol MessageAttachmentsControllerDelegate {
    func messageAttachmentsController(onRemove url: URL)
}

class MessageAttachmentsController: UIViewController {
    var collectionView: UICollectionView!
    var delegate: MessageAttachmentsControllerDelegate!
    
    var images: [URL] = [] {
        didSet {
            collectionView.isHidden = images.count == 0
            collectionView.reloadData()
        }
    }

    func initController(collectionView: UICollectionView) {
        self.collectionView = collectionView
        self.collectionView.register(UINib(nibName: "MessageAttachmentCell", bundle: nil), forCellWithReuseIdentifier: "MessageAttachmentCell")
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension MessageAttachmentsController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 150, height: 150-12)
    }
    
}

extension MessageAttachmentsController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let url = images[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MessageAttachmentCell", for: indexPath) as! MessageAttachmentCell
        cell.configure(url: url)
        cell.delegate = self
        return cell
    }
}

extension MessageAttachmentsController: MessageAttachmentCellDelegate {
    func messageAttachmentCell(onRemove cell: MessageAttachmentCell) {
        delegate.messageAttachmentsController(onRemove: cell.url)
    }
}
