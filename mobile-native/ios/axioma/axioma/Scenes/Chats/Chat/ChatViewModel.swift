//
//  ChatViewModel.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 25.06.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation
import SwiftyJSON
import UIKit

enum ChatMessageType {
    case text
    case withImage
    case incomingText
    case incomingWithImage
}

class ChatRoom {
    var id: Int
    var name: String?
    var isPrivate: Bool
    
    var createdAt: Date
    var createdBy: Int
    var createdByUser: User?
    
    var lastMessageId: Int?
    var lastMessage: ChatMessage?
    
    var targetUserId: Int?
    var targetUser: User?
    
    var updatedAt: Date?
    var users: [User]?
    
    init(id: Int, name: String, isPrivate: Bool, createdAt: Date, createdBy: Int) {
        self.id = id
        self.name = name
        self.isPrivate = isPrivate
        self.createdAt = createdAt
        self.createdBy = createdBy
    }
    
    init(data: [String:Any]) {
        self.id = data["id"] as! Int
        self.name = data["name"] as? String
        self.isPrivate = data["isPrivate"] as! Bool
        self.createdAt = (data["createdAt"] as! String).prefix(19).getDate(dateFormat: "yyyy-MM-dd'T'HH:mm:ss")
        self.createdBy = data["createdBy"] as! Int
        
        if let data = data["createdByUser"] as? [String: Any] {
            self.createdByUser = User(data: data)
        }
        
        if let data = data["lastMessage"] as? [String: Any] {
            self.lastMessage = ChatMessage(data: data)
        }
        
        if let dataList = data["users"] as? [[String: Any]] {
            users = []
            for userData in dataList {
                users?.append(User(data: userData))
            }
        }
    }
    
    var iconName: String? {
        var result: String?
        
        if let name = self.name, !name.isEmpty {
            let words = name.components(separatedBy: " ")
            
            if words.count == 1 {
                result = words[0].prefix(min(words[0].count, 2)).uppercased()
            } else if words.count > 1 {
                result = words[0].prefix(1).uppercased() + words[1].prefix(1).uppercased()
            }
        }
        return result
    }
    
    func findUser(id: Int) -> User? {
        for user in users ?? [] {
            if user.id == id {
                return user
            }
        }
        return nil
    }
}

class ProjectFile {
    var docId: Int?
    var docVersionId: Int?
    var fid: String
    var original: String
    var pageNumber: Int?
    var type: Int
    var actual: Bool?
    var versionNumber: Int?
    
    var url: String
    var thumbUrl: String
    var descriptors: [PageMarker]?
    
    init(data: [String: Any]) {
        docId = data["docId"] as? Int
        docVersionId = data["docVersionId"] as? Int
        fid = data["fid"] as! String
        original = data["original"] as! String
        pageNumber = data["pageNumber"] as? Int
        type = data["type"] as! Int
        actual = data["actual"] as? Bool
        versionNumber = data["versionNumber"] as? Int
        url = data["url"] as! String
        thumbUrl = data["thumbUrl"] as! String
    }
}

class Attachment {
    var id: Int
    var messageId: Int
    var type: Int
    var targetId: Int
    var createdAt: Date
    var updatedAt: Date
    var file: ProjectFile
    
    init(data: [String: Any]) {
        id = data["id"] as! Int
        messageId = data["messageId"] as! Int
        type = data["type"] as! Int
        targetId = data["targetId"] as! Int
        createdAt = (data["createdAt"] as! String).prefix(19).getDate(dateFormat: "yyyy-MM-dd'T'HH:mm:ss")
        updatedAt = (data["updatedAt"] as! String).prefix(19).getDate(dateFormat: "yyyy-MM-dd'T'HH:mm:ss")
        file = ProjectFile(data: data["file"] as! [String: Any])
    }
}

class ChatMessage {
    var id: Int
    var roomId: Int
    var room: ChatRoom?
    var senderId: Int
    var user: User?
    var content: String

    var createdAt: Date
    var updatedAt: Date
    
    var attachments: [Attachment]?
    
    var viewType: ChatMessageType {
        if Session.user?.id == senderId {
            if attachments != nil {
                return .withImage
            } else {
                return .text
            }
        } else if attachments != nil {
            return .incomingWithImage
        } else {
            return .incomingText
        }
    }
    
    init(id: Int, roomId: Int, room: ChatRoom, senderId: Int, user: User, content: String, createdAt: Date, updatedAt: Date) {
        self.id = id
        self.roomId = roomId
        self.room = room
        self.senderId = senderId
        self.user = user
        self.content = content
        self.createdAt = createdAt
        self.updatedAt = updatedAt
    }
    
    convenience init(json: JSON) {
        self.init(data: json.dictionaryObject!)
    }
    
    init(data: [String: Any]) {
        id = data["id"] as! Int
        senderId = data["senderId"] as! Int
        
        if let roomData = data["room"] as? [String: Any] {
            roomId = roomData["id"] as! Int
            room = ChatRoom(data: roomData)
        } else {
            roomId = data["roomId"] as! Int
        }
        
        content = data["content"] as! String
        createdAt = (data["createdAt"] as! String).prefix(19).getDate(dateFormat: "yyyy-MM-dd'T'HH:mm:ss")
        
        if let date = data["updatedAt"] as? String {
            updatedAt = date.prefix(19).getDate(dateFormat: "yyyy-MM-dd'T'HH:mm:ss")
        } else {
            updatedAt = createdAt
        }
        
        if let userData = data["user"] as? [String: Any] {
            user = User(data: userData)
        }
        
        if let attachmentList = data["attachments"] as? [[String: Any]], attachmentList.count > 0 {
            attachments = []
            for attachmentData in attachmentList {
                attachments?.append(Attachment(data: attachmentData))
            }
        }
    }
    
    func calculateContainerSize(maxWidth: CGFloat, font: UIFont = UIFont.systemFont(ofSize: 14)) -> CGSize {
        var height: CGFloat = 17
        var width = content.width(withConstrainedHeight: height, font: font)
        if width > maxWidth {
            width = maxWidth
            height = content.height(withConstrainedWidth: width, font: font)
        }
        return CGSize(width: width, height: height)
    }
}

struct ChatViewModel {
    static let messageTextBorderWidth: CGFloat = 65 + 50 + 14
    
    enum Errors: Error {
        case serverError(msg: String)
    }
    
    enum DateSectionItem {
        case message(ChatMessage)
        case incomingMessage(ChatMessage)
        case attachment(index: Int, message: ChatMessage, attachmen: Attachment)
        
        var message: ChatMessage {
            switch self {
                case .attachment(_, let mess, _):
                    return mess
                case .message(let mess):
                    return mess
                case .incomingMessage(let mess):
                    return mess
            }
        }
    }
    
    struct DateSection {
        var key: String // yyyy:mm:dd used for section sort
        var title: String
        var items: [DateSectionItem]
    }
    
    struct Messages {
        struct Request {
            var room: ChatRoom
        }
        
        struct Response {
            var messages: [ChatMessage]
        }
        
        struct ViewModel {
            var sections: [DateSection]
        }
    }
    
    struct Connect {
        struct Request {
            
        }
        
        struct Response {
            
        }
        
        struct ViewModel {
            
        }
    }

    struct Disconnect {
        struct Request {
            
        }
        
        struct Response {
            
        }
        
        struct ViewModel {
            
        }
    }
    
    struct SendMessage {
        struct Request {
            var roomId: Int
            var content: String
            var ids: [Int]?
        }
        
        struct Response {
            
        }
        
        struct ViewModel {
            
        }
    }
    
    struct UploadFiles {
        struct Request {
            var urls: [URL]
            var uploadProgress:((_ progress: Progress)->())?
        }
        
        struct Response {
            var urls: [URL]
            var ids: [Int]
        }
        
        struct ViewModel {
            var urls: [URL]
            var ids: [Int]
        }
    }
}
