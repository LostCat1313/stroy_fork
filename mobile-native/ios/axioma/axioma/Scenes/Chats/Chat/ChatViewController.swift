//
//  ChatViewController.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 11.07.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit
import Kingfisher

protocol ChatViewControllerInput {
    func displayError(_ error: Error)
    func displayMessages(viewModel: ChatViewModel.Messages.ViewModel)
    func connect(viewModel: ChatViewModel.Connect.ViewModel)
    func disconnect(viewModel: ChatViewModel.Disconnect.ViewModel)
    func uploadFiles(viewModel: ChatViewModel.UploadFiles.ViewModel)
}

protocol ChatViewControllerOutput {
    func fetchMessages(request: ChatViewModel.Messages.Request)
    func connect(request: ChatViewModel.Connect.Request)
    func disconnect(request: ChatViewModel.Disconnect.Request)
    func sendMessage(request: ChatViewModel.SendMessage.Request)
    func uploadFiles(request: ChatViewModel.UploadFiles.Request)
}

class ChatViewController: UIViewController {

    enum State {
        case chatList
        case addAttachment
        case editAttachment
    }
    
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var chatNameLabel: UILabel!
    @IBOutlet weak var chatIconLabel: DesignableLabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var attachmentsView: UICollectionView!
    @IBOutlet weak var textContainerView: UIView!
    @IBOutlet weak var textHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var attachmentsViewHeightConstraint: NSLayoutConstraint!
    var progressView: ProgressViewController?
    
    // VIP
    var output: ChatViewControllerOutput!
    var router: ChatViewRouter!
    var room: ChatRoom!
    var message: ChatMessage?
    var sections: [ChatViewModel.DateSection] = []
    var attacnmentViewController: MessageAttachmentsController!
    private var tempFiles: [URL] = []
    private var state: State = .chatList
    
    private var images: [URL] = [] {
        didSet {
            attachmentsViewHeightConstraint.constant = images.count > 0 ? 150 : 0
            attacnmentViewController.images = self.images
        }
    }
    
    private func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func setShareImages(images: [UIImage]) {
        for image in images {
            if let data = UIImageJPEGRepresentation(image, 0.8) {
                let uid = UUID().uuidString
                let filename = getDocumentsDirectory().appendingPathComponent("\(uid).jpg")
                
                do {
                    try data.write(to: filename)
                    self.images.append(filename)
                    self.tempFiles.append(filename)
                    print("[share] create temp file:\(filename)")
                } catch {
                    print("[share] couldn't write file:\(filename)")
                }
            }
        }
    }
    
    init(room: ChatRoom, message: ChatMessage? = nil) {
        super.init(nibName: "ChatViewController", bundle: Bundle.main)
        ChatViewConfigurator.sharedInstance.configure(viewController: self)
        self.room = room
        self.message = message
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        attacnmentViewController = MessageAttachmentsController()
        attacnmentViewController.initController(collectionView: attachmentsView)
        attacnmentViewController.delegate = self
        
        // Do any additional setup after loading the view.
        collectionView.register(UINib(nibName: "ChatUserMessageCell", bundle: nil), forCellWithReuseIdentifier: "ChatUserMessageCell")
        collectionView.register(UINib(nibName: "ChatMyMessageCell", bundle: nil), forCellWithReuseIdentifier: "ChatMyMessageCell")
        collectionView.register(UINib(nibName: "ChatAttachmentCell", bundle: nil), forCellWithReuseIdentifier: "ChatAttachmentCell")
        collectionView.register(UINib(nibName: "ChatSectionHeaderView", bundle: nil), forSupplementaryViewOfKind:UICollectionElementKindSectionHeader, withReuseIdentifier: "ChatSectionHeaderView")
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.prefetchDataSource = self
        textField.delegate = self
        
        let grTap = UITapGestureRecognizer(target: self, action: #selector(self.onTap(_:)))
        collectionView.addGestureRecognizer(grTap)
    }
    
    @objc func onTap(_ sender: UITapGestureRecognizer) {
        textField.endEditing(true)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear(notification:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear(notification:)), name: .UIKeyboardWillHide, object: nil)
        
        state = .chatList
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow , object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide , object: nil)
        
        //clearAttachmentImages()
    }
    
    private func clearAttachmentImages() {
        images = []
        
        // remove temp files
        let fileManager = FileManager.default
        for filename in tempFiles {
            do {
                try fileManager.removeItem(at: filename)
                print("[remove] delete file:\(filename)")
            } catch {
                print("[remove] couldn't delete file:\(filename)")
            }
        }
        
        tempFiles = []
    }
    
    @objc
    func keyboardWillAppear(notification: NSNotification?) {
        
        guard let keyboardFrame = notification?.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue else {
            return
        }
        
        let keyboardHeight: CGFloat
        if #available(iOS 11.0, *) {
            keyboardHeight = keyboardFrame.cgRectValue.height - self.view.safeAreaInsets.bottom
        } else {
            keyboardHeight = keyboardFrame.cgRectValue.height
        }
        
        textHeightConstraint.constant = keyboardHeight + 50
    }
    
    @objc
    func keyboardWillDisappear(notification: NSNotification?) {
        textHeightConstraint.constant = 50
    }
    
    override func viewDidAppear(_ animated: Bool) {
        chatNameLabel.text = room.name
        chatIconLabel.text = room.iconName ?? ""
        
        if sections.count == 0 {
            let request = ChatViewModel.Connect.Request()
            self.output.connect(request: request)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    @IBAction func onSend(_ sender: Any) {
        if images.count > 0 {
            progressView = ProgressViewController.show(over: self, caption: "Выгрузка файлов", progress: 0)
            let onProgress:(_ progress: Progress) -> () = { progress in
                self.progressView!.progress = Float(progress.fractionCompleted)
            }
            
            let request = ChatViewModel.UploadFiles.Request(urls: images, uploadProgress: onProgress)
            output.uploadFiles(request: request)
        } else if let text = textField.text, !text.isEmpty {
            let request = ChatViewModel.SendMessage.Request(roomId: room.id, content: text, ids: nil)
            output.sendMessage(request: request)
            textField.text = nil
        }
    }
    
    @IBAction func onAttachment(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let picker = UIImagePickerController()
            picker.sourceType = .photoLibrary
            picker.delegate = self
            present(picker, animated: true, completion: nil)
        }
    }
    
    @IBAction func onReturn(_ sender: Any) {
        let request = ChatViewModel.Disconnect.Request()
        self.output.disconnect(request: request)
    }
    
}

extension ChatViewController: ChatViewControllerInput {
    
    
    func displayError(_ error: Error) {
        progressView?.dismiss(animated: false, completion: nil)
        var description = error.localizedDescription
        
        if let err = error as? ChatViewModel.Errors {
            switch err {
                case .serverError(let msg):
                    description = msg
            }
        }
        
        let dlg = Session.makeErrorDialog("Error", message: description)
        self.present(dlg, animated: true)
    }
    
    func displayMessages(viewModel: ChatViewModel.Messages.ViewModel) {
        for section in viewModel.sections {
            if let s = sections.first(where: { $0.key == section.key }) {
                var sec = s
                for item in section.items {
                    sec.items.append(item)
                }
                sections = sections.filter { $0.key != section.key }
                sections.append(sec)
            } else {
                sections.append(section)
            }
        }
        
        sections = sections.sorted(by: {$0.key < $1.key})
        
        DispatchQueue.main.async {
            self.collectionView.reloadData()

            if self.message == nil {
                self.scrollToBottom(animated: true)
            } else {
                // scroll to message
                self.scrollToMessage(self.message!)
                self.message = nil
            }
        }
    }
    
    func connect(viewModel: ChatViewModel.Connect.ViewModel) {
        let request = ChatViewModel.Messages.Request(room: room)
        self.output.fetchMessages(request: request)
    }
    
    func disconnect(viewModel: ChatViewModel.Disconnect.ViewModel) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func uploadFiles(viewModel: ChatViewModel.UploadFiles.ViewModel) {
        if let text = textField.text {
            let request = ChatViewModel.SendMessage.Request(roomId: room.id, content: text, ids: viewModel.ids)
            output.sendMessage(request: request)
            clearAttachmentImages()
            textField.text = nil
        }
        
        progressView?.dismiss(animated: false, completion: nil)
    }
}

extension ChatViewController: UICollectionViewDelegate {
    
}

extension ChatViewController: UICollectionViewDelegateFlowLayout {
    
    func scrollToBottom(animated: Bool) {
        var offsetY: CGFloat = 0
        
        for (section, _) in sections.enumerated() {
            let items = self.collectionView.numberOfItems(inSection: section)
            let size = self.collectionView(collectionView, layout: collectionView.collectionViewLayout, referenceSizeForHeaderInSection: section)
            offsetY += size.height + self.collectionView(collectionView, layout: collectionView.collectionViewLayout, minimumLineSpacingForSectionAt: section)
            
            for item in 0..<items {
                offsetY += getMessageHeight(forItemAt: IndexPath(item: item, section: section)) + self.collectionView(collectionView, layout: collectionView.collectionViewLayout, minimumLineSpacingForSectionAt: section)
            }
        }
        
        collectionView.layoutIfNeeded()
        collectionView.performBatchUpdates({
            let scrollRect = CGRect(x: 0, y: max(offsetY - collectionView.bounds.height, 0), width: collectionView.frame.width, height: offsetY)
            self.collectionView.scrollRectToVisible(scrollRect, animated: true)
        })
    }
    
    func scrollToMessage(_ message: ChatMessage) {
        var offsetY: CGFloat = 0
        
        for section in 0..<self.sections.count-1 {
            let rows = self.collectionView.numberOfItems(inSection: section)
            let size = self.collectionView(collectionView, layout: collectionView.collectionViewLayout, referenceSizeForHeaderInSection: section)
            offsetY += size.height + self.collectionView(collectionView, layout: collectionView.collectionViewLayout, minimumLineSpacingForSectionAt: section)
            
            for row in 0..<rows {
                offsetY += getMessageHeight(forItemAt: IndexPath(item: row, section: section)) + self.collectionView(collectionView, layout: collectionView.collectionViewLayout, minimumLineSpacingForSectionAt: section)
                let item = sections[section].items[row]
                if item.message.id == message.id {
                    collectionView.layoutIfNeeded()
                    let scrollRect = CGRect(x: 0, y: max(offsetY - collectionView.bounds.height, 0), width: collectionView.frame.width, height: offsetY)
                    self.collectionView.scrollRectToVisible(scrollRect, animated: true)
                    return
                }
            }
        }
    }
    
    private func getMessageHeight(forItemAt indexPath: IndexPath) -> CGFloat {
        let item = sections[indexPath.section].items[indexPath.row]
        var height: CGFloat = 100
        
        switch item {
            case .incomingMessage(let message):
                let cell: ChatUserMessageCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChatUserMessageCell", for: indexPath) as! ChatUserMessageCell
                height = cell.getHeight(text: message.content, cellWidth: collectionView.frame.width*0.8)
            case .attachment(_, _, _):
                height = 214
            case .message(let message):
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChatMyMessageCell", for: indexPath) as! ChatMyMessageCell
                height = cell.getHeight(text: message.content, cellWidth: collectionView.frame.width*0.8)
        }
        return height
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: getMessageHeight(forItemAt: indexPath))
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
            case UICollectionElementKindSectionHeader:
                let item = sections[indexPath.section]
                
                let view = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "ChatSectionHeaderView", for: indexPath) as! ChatSectionHeaderView
                view.title.text = item.title
                return view
            default:
                return UICollectionReusableView()
        }
    }
}

extension ChatViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return sections.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {    
        return sections[section].items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let item = sections[indexPath.section].items[indexPath.row]
        
        switch item {
            case .message(let message):
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChatMyMessageCell", for: indexPath) as! ChatMyMessageCell
                cell.configure(message: message, cellWidth: collectionView.frame.width*0.8)
                return cell
            case .incomingMessage(let message):
                message.user = room.findUser(id: message.senderId)
                let cell: ChatUserMessageCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChatUserMessageCell", for: indexPath) as! ChatUserMessageCell
                cell.configure(message: message, cellWidth: collectionView.frame.width*0.8)
                return cell
            case .attachment(_, _, let attachment):
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChatAttachmentCell", for: indexPath) as! ChatAttachmentCell
                cell.delegate = self
                cell.configure(attachment: attachment)
                return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        let item = sections[indexPath.section].items[indexPath.row]
        
        switch item {
            case .attachment(_, _, let attachment):
                let imageUrl = Configuration.serverUrl.appendingFormat(attachment.file.thumbUrl)
                if let url = URL(string: imageUrl) {
                    if let attachmentImage = (cell as! ChatAttachmentCell).image {
                        attachmentImage.kf.setImage(
                            with: url,
                            placeholder: nil,
                            options: [.transition(ImageTransition.fade(1)), .requestModifier(imageRequestModifier)],
                            progressBlock: { _, _ in },
                            completionHandler: { image, error, cacheType, imageURL in
                                if image == nil {
                                    //
                                }
                            }
                        )
                    }
                }
            default:
                break
        }
    }
    
}

extension ChatViewController: UICollectionViewDataSourcePrefetching {
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        var urls: [URL] = []
        for indexPath in indexPaths {
            let item = sections[indexPath.section].items[indexPath.row]
            
            switch item {
                case .attachment(_, _, let attachment):
                    let imageUrl = Configuration.serverUrl.appendingFormat(attachment.file.thumbUrl)
                    if let url = URL(string: imageUrl) {
                        urls.append(url)
                    }
                default:
                    break
            }
        }
        
        ImagePrefetcher(urls: urls, options: [.requestModifier(imageRequestModifier)]).start()
    }
}

extension ChatViewController: UITextFieldDelegate {
    
}

extension ChatViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let url = info[UIImagePickerControllerImageURL] as? URL {
            picker.dismiss(animated: true, completion: nil)
            images.append(url)
        }
    }
}

extension ChatViewController: MessageAttachmentsControllerDelegate {
    func messageAttachmentsController(onRemove url: URL) {
        images = images.filter { $0.absoluteString != url.absoluteString }
    }
}

extension ChatViewController: ChatAttachmentCellDelegate {
    func chatAttachmentCell(onSelect cell: ChatAttachmentCell) {
        router.navigateToAttachmnetMarkerEditor(image: cell.image.image!)
    }
}

extension ChatViewController: ChatAttachmentMarkerEditorDelegate {
    func chatAttachmentMarkerEditor(shareImage: UIImage) {
        clearAttachmentImages()
        setShareImages(images: [shareImage])
    }
}
