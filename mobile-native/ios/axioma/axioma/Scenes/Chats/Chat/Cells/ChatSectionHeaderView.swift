//
//  ChatSectionHeaderView.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 31.07.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit

class ChatSectionHeaderView: UICollectionReusableView {
    @IBOutlet weak var title: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
