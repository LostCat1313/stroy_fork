//
//  ChatUserMessageCell.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 28.07.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit

class ChatUserMessageCell: UICollectionViewCell {
    @IBOutlet weak var messageContainer: UIView!
    @IBOutlet weak var userIconLabel: DesignableLabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var messageText: UITextView!
    @IBOutlet weak var attachmentContainer: UIView!
    @IBOutlet weak var attachmentLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    //@IBOutlet weak var image: UIImageView!
    @IBOutlet weak var imageHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var messageWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var messageHeightConstraint: NSLayoutConstraint!
    
    var message: ChatMessage!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configure(message: ChatMessage, cellWidth: CGFloat) {
        self.message = message
        
        userNameLabel.text = message.user?.name
        userIconLabel.text = message.user?.iconName
        messageText.text = message.content
        
        let size = calculateBubbleSize(text: message.content, maxWidth: cellWidth - (messageContainer.frame.origin.x + userIconLabel.frame.origin.x))
        messageHeightConstraint.constant = size.height
        messageWidthConstraint.constant = size.width
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        timeLabel.text = dateFormatter.string(from: message.createdAt)
    }
    
    private func calculateBubbleSize(text: String, maxWidth: CGFloat) -> CGSize {
        messageText.text = text
        let maxTextWidth = maxWidth - 2*messageText.frame.origin.x
        let size = messageText.sizeThatFits(CGSize(width: maxTextWidth, height: CGFloat.infinity))
        return CGSize(width: max(50, size.width + 2*messageText.frame.origin.x), height: max(50, size.height + 2*messageText.frame.origin.y))
    }
    
    func getHeight(text: String, cellWidth: CGFloat) -> CGFloat {
        let size = calculateBubbleSize(text: text, maxWidth: cellWidth - (messageContainer.frame.origin.x + userIconLabel.frame.origin.x))
        return size.height + messageContainer.frame.origin.y + userIconLabel.frame.origin.y
    }

}
