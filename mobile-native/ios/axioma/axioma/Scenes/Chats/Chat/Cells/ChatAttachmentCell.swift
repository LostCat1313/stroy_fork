//
//  ChatAttachmentCell.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 09.08.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit

protocol ChatAttachmentCellDelegate: class {
    func chatAttachmentCell(onSelect cell: ChatAttachmentCell)
}

class ChatAttachmentCell: UICollectionViewCell {
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var timeLabel: UILabel!
    
    weak var delegate: ChatAttachmentCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let grTap = UITapGestureRecognizer(target: self, action: #selector(self.onTap(_:)))
        self.addGestureRecognizer(grTap)
    }

    @objc func onTap(_ sender: UITapGestureRecognizer) {
        delegate?.chatAttachmentCell(onSelect: self)
    }
    
    func configure(attachment: Attachment) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        timeLabel.text = dateFormatter.string(from: attachment.createdAt)
    }
}
