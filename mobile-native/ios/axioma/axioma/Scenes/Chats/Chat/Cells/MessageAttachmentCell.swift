//
//  MessageAttachmentCell.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 23.08.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit
import Kingfisher

protocol MessageAttachmentCellDelegate {
    func messageAttachmentCell(onRemove cell: MessageAttachmentCell)
}

class MessageAttachmentCell: UICollectionViewCell {

    @IBOutlet weak var image: UIImageView!
    
    var url: URL!
    var delegate: MessageAttachmentCellDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func onRemove(_ sender: Any) {
        delegate.messageAttachmentCell(onRemove: self)
    }
    
    func configure(url: URL) {
        self.url = url
        
        image.kf.setImage(with: url, placeholder: nil,
                          options: [.transition(ImageTransition.none), .requestModifier(imageRequestModifier)],
                          completionHandler: { image, error, cacheType, imageURL in
                            if image == nil {
                                DispatchQueue.main.async {
                                    self.image.image = UIImage(named: "alert")
                                }
                            }
        })
        
    }
}
