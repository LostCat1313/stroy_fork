//
//  ChatViewInteractor.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 11.07.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol ChatViewInteractorInput {
    func fetchMessages(request: ChatViewModel.Messages.Request)
    func connect(request: ChatViewModel.Connect.Request)
    func disconnect(request: ChatViewModel.Disconnect.Request)
    func sendMessage(request: ChatViewModel.SendMessage.Request)
    func uploadFiles(request: ChatViewModel.UploadFiles.Request)
}

protocol ChatViewInteractorOutput {
    func presentError(_ error: Error)
    func presentMessages(response: ChatViewModel.Messages.Response)
    func connect(response: ChatViewModel.Connect.Response)
    func disconnect(response: ChatViewModel.Disconnect.Response)
    func uploadFiles(response: ChatViewModel.UploadFiles.Response)
}

class ChatViewInteractor: ChatViewInteractorInput {
    var output: ChatViewInteractorOutput!
    //var worker = ChatViewWorker()
    
    func fetchMessages(request: ChatViewModel.Messages.Request) {
        if let manager = Session.socketManager() {
            let socket = manager.defaultSocket
            
            if socket.status == .connected {
                socket.emitWithAck("join", ["roomId": request.room.id]).timingOut(after: 0) { data in
                    print("[debug] get room messages: \(data.count)")
                    var messages: [ChatMessage] = []
                    for room in data as! [[String: Any]] {
                        let messagesData = room["messages"] as! [[String:Any]]
                        for message in messagesData {
                            messages.append(ChatMessage(data: message))
                            print("[debug] message: \(message)")
                        }
                    }
                    let response = ChatViewModel.Messages.Response(messages: messages)
                    self.output.presentMessages(response: response)
                }
            }
        }
    }
    
    func connect(request: ChatViewModel.Connect.Request) {
        if let manager = Session.socketManager() {
            let socket = manager.defaultSocket
            
            socket.disconnect()
            socket.off("init")
            socket.off("message")
            
            socket.on("init") { [weak self] data, ack in
                if self != nil {
                    let response = ChatViewModel.Connect.Response()
                    self?.output.connect(response: response)
                }
                socket.off("init")
            }
            
            socket.on("message") { [weak self] data, ask in
                if self != nil {
                    var messages: [ChatMessage] = []
                    if let messageList = data as? [[String: Any]] {
                        for messageData in messageList {
                            messages.append(ChatMessage(data: messageData))
                        }
                    }
                    let response = ChatViewModel.Messages.Response(messages: messages)
                    self!.output.presentMessages(response: response)
                } else {
                    socket.off("message")
                }
            }
            
            socket.connect()
        }
    }
    
    func disconnect(request: ChatViewModel.Disconnect.Request) {
        if let manager = Session.socketManager() {
            let socket = manager.defaultSocket
        
            if socket.status == .connected {
                socket.disconnect()
            }
            
            let response = ChatViewModel.Disconnect.Response()
            self.output.disconnect(response: response)
        }
    }
    
    func sendMessage(request: ChatViewModel.SendMessage.Request) {
        if let manager = Session.socketManager() {
            let socket = manager.defaultSocket
            
            if socket.status == .connected {
                var message: [String: Any] = [:]
                message["roomId"] = request.roomId
                message["content"] = request.content
                
                if request.ids != nil {
                    var attachments: [[String:Any]] = []
                    for id in request.ids! {
                        var data: [String: [String: Any]] = [:]
                        data["file"] = ["id": id]
                        attachments.append(data)
                    }
                    
                    message["attachments"] = attachments
                }
                
                socket.emit("message", message)
            }
        }
    }
    
    func uploadFiles(request: ChatViewModel.UploadFiles.Request) {
        
        do {
            let path = try RestAPI.uploadFiles.path.asURL()
            FileAPI.uploadFiles(path: path, files: request.urls, uploadProgress: request.uploadProgress) {[weak self] files, error in
                if self == nil {
                    return
                }
                
                if error != nil {
                    self!.output.presentError(error!)
                } else {
                    let ids = files!.map { $0.id }
                    let response = ChatViewModel.UploadFiles.Response(urls: request.urls, ids: ids)
                    self!.output.uploadFiles(response: response)
                }
            }
        } catch {
            
        }
    }
}
