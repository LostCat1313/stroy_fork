//
//  ChatViewPresenter.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 11.07.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation

protocol ChatViewPresenterInput {
    func presentError(_ error: Error)
    func presentMessages(response: ChatViewModel.Messages.Response)
    func connect(response: ChatViewModel.Connect.Response)
    func disconnect(response: ChatViewModel.Disconnect.Response)
    func uploadFiles(response: ChatViewModel.UploadFiles.Response)
}

protocol ChatViewPresenterOutput: class {
    func displayError(_ error: Error)
    func displayMessages(viewModel: ChatViewModel.Messages.ViewModel)
    func connect(viewModel: ChatViewModel.Connect.ViewModel)
    func disconnect(viewModel: ChatViewModel.Disconnect.ViewModel)
    func uploadFiles(viewModel: ChatViewModel.UploadFiles.ViewModel)
}

class ChatViewPresenter: NSObject, ChatViewPresenterInput {
    weak var output: ChatViewPresenterOutput!
    
    func presentError(_ error: Error) {
        self.output.displayError(error)
    }
    
    func presentMessages(response: ChatViewModel.Messages.Response) {
        var messagesDict: [String: [ChatMessage]] = [:]
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        for message in response.messages {
            let key = dateFormatter.string(from: message.createdAt)
            if let _ = messagesDict[key] {
                messagesDict[key]!.append(message)
            } else {
                messagesDict[key] = [message]
            }
        }
        
        // create sections
        var sections: [ChatViewModel.DateSection] = []
        let sectionFormater = DateFormatter()
        sectionFormater.dateFormat = "d MMMM"
        
        for (key, messages) in messagesDict {
            let date = dateFormatter.date(from: key)
            let title = sectionFormater.string(from: date!)
            var items: [ChatViewModel.DateSectionItem] = []
            
            for message in messages {
                
                if !message.content.isEmpty || (message.attachments ?? []).count == 0 {
                    if Session.user?.id == message.senderId {
                        items.append(ChatViewModel.DateSectionItem.message(message))
                    } else {
                        items.append(ChatViewModel.DateSectionItem.incomingMessage(message))
                    }
                }
                
                for (index, attachment) in (message.attachments ?? []).enumerated() {
                    items.append(ChatViewModel.DateSectionItem.attachment(index: index, message: message, attachmen: attachment))
                }
            }
            
            sections.append(ChatViewModel.DateSection(key: key, title: title, items: items))
        }
        
        sections = sections.sorted(by: {$0.key < $1.key})
        
        let viewModel = ChatViewModel.Messages.ViewModel(sections: sections)
        output.displayMessages(viewModel: viewModel)
    }
    
    func connect(response: ChatViewModel.Connect.Response) {
        let viewModel = ChatViewModel.Connect.ViewModel()
        output.connect(viewModel: viewModel)
    }
    
    func disconnect(response: ChatViewModel.Disconnect.Response) {
        let viewModel = ChatViewModel.Disconnect.ViewModel()
        output.disconnect(viewModel: viewModel)
    }
    
    func uploadFiles(response: ChatViewModel.UploadFiles.Response) {
        let viewModel = ChatViewModel.UploadFiles.ViewModel(urls: response.urls, ids: response.ids)
        output.uploadFiles(viewModel: viewModel)
    }
}
