//
//  ChatViewRouter.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 11.07.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation
import UIKit

protocol ChatViewRouterInput {
    
}

class ChatViewRouter: ChatViewRouterInput {
    weak var viewController: ChatViewController!
    
    // MARK: Navigation
    func navigateToAttachmnetMarkerEditor(image: UIImage) {
        let controller = ChatAttachmentMarkerEditor(image: image)
        controller.delegate = viewController
        viewController.present(controller, animated: true, completion: nil)
    }
    
    // MARK: Communication
    
    func passDataToNextScene(segue: UIStoryboardSegue) {
        // NOTE: Teach the router which scenes it can communicate with
        
        //if segue.identifier == "ShowSomewhereScene" {
        //    passDataToScene(segue)
        //}
    }
    
    func passDataToScene(segue: UIStoryboardSegue) {
        // NOTE: Teach the router how to pass data to the next scene
        
        // let someWhereViewController = segue.destinationViewController as! SomeWhereViewController
        // someWhereViewController.output.name = viewController.output.name
    }
}
