//
//  ChatViewConfigurator.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 11.07.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation
import UIKit

// MARK: Connect View, Interactor, and Presenter

extension ChatViewController: ChatViewPresenterOutput {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        router.passDataToNextScene(segue: segue)
    }
}

extension ChatViewInteractor: ChatViewControllerOutput {
}

extension ChatViewPresenter: ChatViewInteractorOutput {
}

class ChatViewConfigurator {
    // MARK: Object lifecycle
    static let sharedInstance = ChatViewConfigurator()
    
    // MARK: Configuration
    func configure(viewController: ChatViewController) {
        let router = ChatViewRouter()
        router.viewController = viewController
        
        let presenter = ChatViewPresenter()
        presenter.output = viewController
        
        let interactor = ChatViewInteractor()
        interactor.output = presenter
        //interactor.worker = ChatViewWorker()
        
        viewController.output = interactor
        viewController.router = router
    }
}
