//
//  ChatAttachmentMarkerEditor.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 20/11/2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit

protocol MarkerEditorProtocol {
    func closeDrawableMarkerToolbar()
    func openDrawableMarkerToolbar()
}

protocol ChatAttachmentMarkerEditorDelegate: class {
    func chatAttachmentMarkerEditor(shareImage: UIImage)
}

class ChatAttachmentMarkerEditor: UIViewController {

    @IBOutlet weak var topBar: UIView!
    @IBOutlet weak var imgPanel: UIView!
    @IBOutlet weak var pageImage: UIImageView!
    @IBOutlet weak var drawPanel: PageAnnotatesView!
    @IBOutlet weak var editTextMarker: UITextField!
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var toolbarView: UIView!
    @IBOutlet weak var btnTools: UIButton!
    
    @IBOutlet weak var extToolbarPanel: UIView!
    @IBOutlet weak var colorPickerSlider: ColorPickerSlider!
    @IBOutlet weak var sizeSlider: UISlider!
    @IBOutlet weak var toolbarSizeButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    
    weak var image: UIImage!
    weak var delegate: ChatAttachmentMarkerEditorDelegate?
    
    var markers: [PageMarker] = []
    var minimalSize: CGSize = CGSize.zero
    
    var state: PageAnnotatorState = .preview {
        didSet {
            stateDidSet()
        }
    }
    
    private func stateDidSet() {
        if !imgPanel.isUserInteractionEnabled {
            imgPanel.isUserInteractionEnabled = true
            imgPanel.alpha = 1.0
        }
        
        switch state {
            case .link, .notice, .change:
                break
            case .preview:
                self.editTextMarker.isHidden = true
            
            case .curve(let marker):
                toolbarSizeButton.setImage(UIImage(named: "format-line-weight"), for: .normal)
                self.colorPickerSlider.currentColor = marker.color
                self.sizeSlider.minimumValue = 1
                self.sizeSlider.maximumValue = 20
                self.sizeSlider.value = Float(marker.width)
                openDrawableMarkerToolbar()
            case .rect(let marker):
                toolbarSizeButton.setImage(UIImage(named: "format-line-weight"), for: .normal)
                self.colorPickerSlider.currentColor = marker.color
                self.sizeSlider.minimumValue = 1
                self.sizeSlider.maximumValue = 20
                self.sizeSlider.value = Float(marker.width)
                openDrawableMarkerToolbar()
            case .editText(let marker):
                toolbarSizeButton.setImage(UIImage(named: "format-size"), for: .normal)
                self.colorPickerSlider.currentColor = marker.color
                self.editTextMarker.isHidden = false
                self.editTextMarker.text = marker.getText() ?? ""
                self.editTextMarker.textColor = marker.color
                self.editTextMarker.font = marker.font
                self.editTextMarker.becomeFirstResponder()
                // remove marker from draw panel
                drawPanel.remove(marker: marker)
                imgPanel.isUserInteractionEnabled = false
                imgPanel.alpha = 0.2
            case .moveText(let marker):
                self.editTextMarker.isHidden = true
                toolbarSizeButton.setImage(UIImage(named: "format-size"), for: .normal)
                self.colorPickerSlider.currentColor = marker.color
                self.sizeSlider.minimumValue = 10
                self.sizeSlider.maximumValue = 50
                self.sizeSlider.value = Float(marker.font.pointSize)
                openDrawableMarkerToolbar()
                // put marker to draw panel
                drawPanel.update(marker: marker)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        editTextMarker.delegate = self
        editTextMarker.returnKeyType = .done
        colorPickerSlider.delegate = self
        drawPanel.delegate = self
        drawPanel.changedMarkerAlpha = 1.0
        
        //init toolbar
        let toolbar:UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: 30))
        //create left side empty space so that done button set on right side
        let flexSpace = UIBarButtonItem(barButtonSystemItem:    .flexibleSpace, target: nil, action: nil)
        let doneBtn: UIBarButtonItem = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(self.cancelButtonKeyboard))
        toolbar.setItems([flexSpace, doneBtn], animated: false)
        toolbar.sizeToFit()
        editTextMarker.inputAccessoryView = toolbar
        
        // Do any additional setup after loading the view.
        pageImage.image = image
        
        hideSpinner()
    }

    init(image: UIImage) {
        super.init(nibName: "ChatAttachmentMarkerEditor", bundle: Bundle.main)
        self.image = image
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @objc func cancelButtonKeyboard() {
        editTextMarker.endEditing(true)
        switch state {
            case .editText(let marker):
                drawPanel.update(marker: marker)
                state = .preview
            default:
                break
        }
    }
    
    @IBAction func onBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onPalette(_ sender: Any) {
        if sizeSlider.isHidden {
            extToolbarPanel.isHidden = !extToolbarPanel.isHidden
        }
        sizeSlider.isHidden = true
        colorPickerSlider.isHidden = false
    }
    
    @IBAction func onChangeSize(_ sender: Any) {
        if colorPickerSlider.isHidden {
            extToolbarPanel.isHidden = !extToolbarPanel.isHidden
        }
        sizeSlider.isHidden = false
        colorPickerSlider.isHidden = true
    }
    
    @IBAction func onChangeSizeValue(_ sender: UISlider) {
        switch state {
            case .curve(let marker):
                marker.width = CGFloat(sender.value)
                state = .curve(marker: marker)
                drawPanel.update(marker: marker)
            case .rect(let marker):
                marker.width = CGFloat(sender.value)
                state = .rect(marker: marker)
                drawPanel.update(marker: marker)
            case .moveText(let marker):
                marker.font = UIFont(name: marker.font.fontName, size: CGFloat(sender.value))!
                state = .moveText(marker: marker)
                drawPanel.update(marker: marker)
            default:
                break
        }
    }
    
    @IBAction func onTools(_ sender: Any) {
        
        let menuItems: [PopupMenuItemProtocol] = [
            PopupMenuItem(title: "Текст", icon: UIImage(named: "format-title")) { item in
                let marker = PageMarkerText(ownerDocId: 0, ownerPageId: 0)
                var pos = self.editTextMarker.frame.origin
                pos.x = self.view.frame.size.width/2
                let pt = self.view.convert(CGPoint(x: pos.x, y: pos.y), to: self.pageImage)
                marker.points = [self.pageImage.convertPoint(fromViewPoint: pt)]
                self.state = .editText(marker: marker)
            },
            PopupMenuItem(title: "Линия", icon: UIImage(named: "lead-pencil")) { item in
                let marker = PageMarkerCurve(ownerDocId: 0, ownerPageId: 0)
                self.state = .curve(marker: marker)
            },
            PopupMenuItem(title: "Прямоугольник", icon: UIImage(named: "vector-rectangle")) { item in
                let marker = PageMarkerRect(ownerDocId: 0, ownerPageId: 0)
                self.state = .rect(marker: marker)
            }
        ]
        
        let at = CGPoint(x: btnTools.frame.origin.x, y: topBar.frame.height - 1)
        let at2 = view.convert(at, from: topBar)
        PopupMenuViewController.popup(over: self, at: at2, menuItems: menuItems)
    }
    
    @IBAction func onShare(_ sender: Any) {
        if self.markers.count == 0 {
            return
        }

        showSpinner()
        
        DispatchQueue.main.async {
            var image = self.pageImage.image!
            self.drawPanel.transform = CGAffineTransform.identity
            
            for item in self.markers {
                if let marker = item as? DrawableMarker {
                    image = self.drawPanel.renderToImage(image, marker: marker)
                }
            }
            
            self.drawPanel.transform = self.pageImage.transform
            self.hideSpinner()
            
            self.shareImage(image)
        }
    }
    
    @IBAction func onCloseToolbar(_ sender: Any) {
        closeDrawableMarkerToolbar()
    }
    
    func shareImage(_ image: UIImage) {
        self.delegate?.chatAttachmentMarkerEditor(shareImage: image)
        dismiss(animated: false, completion: nil)
    }
    
    @IBAction func onDeleteDrawableMarker(_ sender: Any) {
        func deleteMarker(marker: PageMarker) {
            // remove from markers
            markers = markers.filter { !($0 === marker) }
            drawPanel.remove(marker: marker as! DrawableMarker)
        }
        
        switch self.state {
            case .curve(let marker):
                self.state = .preview
                deleteMarker(marker: marker)
            case .rect(let marker):
                self.state = .preview
                deleteMarker(marker: marker)
            case .moveText(let marker):
                self.state = .preview
                deleteMarker(marker: marker)
            default:
                break
        }
        
        self.shareButton.isEnabled = self.markers.count > 0
    }
    
    @IBAction func onTap(_ sender: UITapGestureRecognizer) {
        switch state {
            case .preview, .curve, .rect, .moveText:
                if sender.state == .ended {
                    tryTapOnMarker(tap: sender)
                }
            default:
                break
        }
    }
    
    @IBAction func onPan(_ sender: UIPanGestureRecognizer) {
        let pt = sender.location(in: self.pageImage)
        let imagePoint = self.pageImage.convertPoint(fromViewPoint: pt)
        
        print("[debug] pan pt.x:\(pt.x), pt.y:\(pt.y)")
        
        switch sender.state {
        case .began:
            
            switch state {
                case .moveText, .preview:
                    if let marker = drawPanel.hitTest(pt) {
                        if let curve = marker as? PageMarkerCurve {
                            state = .curve(marker: curve)
                        } else if let rect = marker as? PageMarkerRect {
                            state = .rect(marker: rect)
                        } else if let text = marker as? PageMarkerText {
                            state = .moveText(marker: text)
                        }
                    } else {
                        state = .preview
                        
                        if !toolbarView.isHidden {
                            closeDrawableMarkerToolbar()
                        }
                    }
                default: break
            }
            
            switch state {
                case .curve(let marker):
                    marker.points = [imagePoint]
                    drawPanel.update(marker: marker)
                    state = .curve(marker: marker)
                case .rect(let marker):
                    marker.ltPoint = imagePoint
                    drawPanel.update(marker: marker)
                    state = .rect(marker: marker)
                case .moveText(let marker):
                    marker.points = [imagePoint]
                    drawPanel.update(marker: marker)
                    state = .moveText(marker: marker)
                default: break
            }
        case .changed:
            switch state {
                case .curve(let marker):
                    marker.points.append(imagePoint)
                    drawPanel.update(marker: marker)
                    state = .curve(marker: marker)
                case .rect(let marker):
                    marker.rbPoint = imagePoint
                    drawPanel.update(marker: marker)
                    state = .rect(marker: marker)
                case .moveText(let marker):
                    marker.points = [imagePoint]
                    drawPanel.update(marker: marker)
                    state = .moveText(marker: marker)
                default:
                    let d = sender.translation(in: self.imgPanel)
                    pageImage.transform = pageImage.transform.concatenating(CGAffineTransform(translationX: d.x, y: d.y))
                    drawPanel.transform = drawPanel.transform.concatenating(CGAffineTransform(translationX: d.x, y: d.y))
                    sender.setTranslation(CGPoint.zero, in: self.imgPanel)
                    updateMarkers()
            }
        case .ended:
            switch self.state {
                case .curve(let marker):
                    append(marker: marker)
                case .rect(let marker):
                    append(marker: marker)
                case .moveText(let marker):
                    append(marker: marker)
                default:
                    break
            }
        default:
            break
        }
    }
    
    @IBAction func onPinch(_ sender: UIPinchGestureRecognizer) {
        switch sender.state {
            case .changed:
                let t = pageImage.transform
                pageImage.transform = pageImage.transform.concatenating(CGAffineTransform(scaleX: sender.scale, y: sender.scale))
                drawPanel.transform = drawPanel.transform.concatenating(CGAffineTransform(scaleX: sender.scale, y: sender.scale))
                if pageImage.frame.width < minimalSize.width || pageImage.frame.height < minimalSize.height {
                    UIView.animate(withDuration: 0.4, delay: 0, options: .curveEaseOut, animations: {
                        self.pageImage.transform = t
                        self.drawPanel.transform = t
                    }, completion: { _ in
                        self.updateMarkers()
                    })
                } else {
                    updateMarkers()
                }
                sender.scale = 1
            
            default:
                break
        }
    }
    
    @IBAction func onCheckDrawableMarker(_ sender: Any) {
        switch self.state {
            case .curve, .rect, .moveText:
                closeDrawableMarkerToolbar()
            default:
                break
        }
        
        drawPanel.setNeedsDisplay()
    }
    
    private func updateMarkers() {
        for item in markers {
            if let marker = item as? DrawableMarker {
                self.drawPanel.update(marker: marker)
            }
        }
    }
    
    func tryTapOnMarker(tap: UITapGestureRecognizer) {
        // drawable markers
        let pt = tap.location(in: pageImage)
        if let marker = drawPanel.hitTest(pt) {
            if let curve = marker as? PageMarkerCurve {
                state = .curve(marker: curve)
            } else if let rect = marker as? PageMarkerRect {
                state = .rect(marker: rect)
            } else if let text = marker as? PageMarkerText {
                state = .editText(marker: text)
            }
        } else {
            state = .preview
            
            if !toolbarView.isHidden {
                closeDrawableMarkerToolbar()
            }
        }
    }
}

extension ChatAttachmentMarkerEditor: MarkerEditorProtocol {
    
    internal func openDrawableMarkerToolbar() {
        if self.toolbarView.isHidden {
            self.toolbarView.transform = CGAffineTransform.identity.translatedBy(x: 0.0, y: self.toolbarView.frame.size.height)
            self.toolbarView.isHidden = false
            
            UIView.animate(withDuration: 0.5, animations: {
                self.toolbarView.transform = CGAffineTransform.identity
            })
        }
    }
    
    private func append(marker: PageMarker) {
        if !self.markers.contains(where: {$0 === marker}) {
            self.markers.append(marker)
        }
    }
    
    func closeDrawableMarkerToolbar() {
        let transform = self.toolbarView.transform.translatedBy(x: 0.0, y: self.toolbarView.frame.size.height)
        extToolbarPanel.isHidden = true
        sizeSlider.isHidden = true
        colorPickerSlider.isHidden = true
        
        UIView.animate(withDuration: 0.5, animations: {
            self.toolbarView.transform = transform
        }, completion: { _ in
            self.toolbarView.isHidden = true
            self.toolbarView.transform = CGAffineTransform.identity
            
            switch self.state {
                case .curve(let marker):
                    self.state = .preview
                    self.append(marker: marker)
                case .rect(let marker):
                    self.state = .preview
                    self.append(marker: marker)
                case .moveText(let marker):
                    self.state = .preview
                    self.append(marker: marker)
                default:
                    break
            }
            
            self.shareButton.isEnabled = self.markers.count > 0
        })
    }
}

extension ChatAttachmentMarkerEditor {
    func showSpinner() {
        btnTools.isEnabled = false
        btnTools.alpha = 0.3
        spinner.startAnimating()
    }
    
    func hideSpinner() {
        btnTools.isEnabled = true
        btnTools.alpha = 1.0
        spinner.stopAnimating()
    }
}

extension ChatAttachmentMarkerEditor: ColorPickerSliderDelegate {
    func colorPickerSlider(didSelect color: UIColor) {
        switch state {
            case .curve(let marker):
                marker.color = color
                state = .curve(marker: marker)
                drawPanel.update(marker: marker)
            case .rect(let marker):
                marker.color = color
                state = .rect(marker: marker)
                drawPanel.update(marker: marker)
            case .moveText(let marker):
                marker.color = color
                state = .moveText(marker: marker)
                drawPanel.update(marker: marker)
            default:
                break
        }
    }
}

extension ChatAttachmentMarkerEditor: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        guard !(textField.text ?? "").isEmpty else {
            state = .preview
            return true
        }
        
        switch state {
        case .editText(let marker):
            marker.text = textField.text!
            state = .moveText(marker: marker)
            
            drawPanel.update(marker: marker)
            openDrawableMarkerToolbar()
        default:
            break
        }
        
        return true
    }
}

extension ChatAttachmentMarkerEditor: PageAnnotatesViewDelegate {
    func pageAnnotatesView(_ view: PageAnnotatesView, convertToViewPoints points: [CGPoint]) -> [CGPoint] {
        return points.map { pageImage.convertPoint(fromImagePoint: $0) }
    }
}
