//
//  RoomsViewPresenter.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 12.07.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation

protocol RoomsViewPresenterInput {
    func presentError(_ error: Error)
    func presentRooms(response: RoomsViewModel.RoomList.Response)
    func newChatUsers(response: RoomsViewModel.ChatUsers.Response)
    func createRoom(response: RoomsViewModel.CreateRoom.Response)
}

protocol RoomsViewPresenterOutput: class {
    func displayError(_ error: Error)
    func displayRooms(viewModel: RoomsViewModel.RoomList.ViewModel)
    func newChatUsers(viewModel: RoomsViewModel.ChatUsers.ViewModel)
    func createRoom(viewModel: RoomsViewModel.CreateRoom.ViewModel)
}

class RoomsViewPresenter: NSObject, RoomsViewPresenterInput {
    weak var output: RoomsViewPresenterOutput!
    
    func presentError(_ error: Error) {
        self.output.displayError(error)
    }
    
    func presentRooms(response: RoomsViewModel.RoomList.Response) {
        let viewModel = RoomsViewModel.RoomList.ViewModel(rooms: response.rooms)
        self.output.displayRooms(viewModel: viewModel)
    }
    
    func newChatUsers(response: RoomsViewModel.ChatUsers.Response) {
        let viewModel = RoomsViewModel.ChatUsers.ViewModel(roomName: response.roomName, users: response.users)
        output.newChatUsers(viewModel: viewModel)
    }
    
    func createRoom(response: RoomsViewModel.CreateRoom.Response) {
        let viewModel = RoomsViewModel.CreateRoom.ViewModel(room: response.room)
        output.createRoom(viewModel: viewModel)
    }
}
