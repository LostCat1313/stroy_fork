//
//  RoomsViewConfigurator.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 12.07.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation
import UIKit

// MARK: Connect View, Interactor, and Presenter

extension RoomsViewController: RoomsViewPresenterOutput {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        router.passDataToNextScene(segue: segue)
    }
}

extension RoomsViewInteractor: RoomsViewControllerOutput {
}

extension RoomsViewPresenter: RoomsViewInteractorOutput {
}

class RoomsViewConfigurator {
    // MARK: Object lifecycle
    static let sharedInstance = RoomsViewConfigurator()
    
    // MARK: Configuration
    func configure(viewController: RoomsViewController) {
        let router = RoomsViewRouter()
        router.viewController = viewController
        
        let presenter = RoomsViewPresenter()
        presenter.output = viewController
        
        let interactor = RoomsViewInteractor()
        interactor.output = presenter
        //interactor.worker = RoomsViewWorker()
        
        viewController.output = interactor
        viewController.router = router
    }
}
