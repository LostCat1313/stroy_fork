//
//  RoomsViewInteractor.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 12.07.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation
import SwiftyJSON
import SocketIO

protocol RoomsViewInteractorInput {
    func fetchRooms(request: RoomsViewModel.RoomList.Request)
    func newChatUsers(request: RoomsViewModel.ChatUsers.Request)
    func createRoom(request: RoomsViewModel.CreateRoom.Request)
}

protocol RoomsViewInteractorOutput {
    func presentError(_ error: Error)
    func presentRooms(response: RoomsViewModel.RoomList.Response)
    func newChatUsers(response: RoomsViewModel.ChatUsers.Response)
    func createRoom(response: RoomsViewModel.CreateRoom.Response)
}

class RoomsViewInteractor: RoomsViewInteractorInput {
    var output: RoomsViewInteractorOutput!
    //var worker = RoomsViewWorker()
    
    func fetchRooms(request: RoomsViewModel.RoomList.Request) {
        if let manager = Session.socketManager() {
            let socket = manager.defaultSocket
            
            socket.off("init")
            socket.on("init") { [weak self] data, ack in
                if data.count > 0 {
                    let roomsData = (data[0] as! [String: Any])["rooms"] as! [[String: Any]]
                    print("Get Init rooms:\(roomsData.count)")
                    var rooms: [ChatRoom] = []
                    for roomData in roomsData {
                        let room = ChatRoom(data: roomData)
                        rooms.append(room)
                    }
                    
                    let response = RoomsViewModel.RoomList.Response(rooms: rooms)
                    self?.output.presentRooms(response: response)
                }
                socket.off("init")
            }
            
            if socket.status == .notConnected || socket.status == .disconnected {
                socket.connect()
            } else {
                manager.reconnect()
            }
        }
    }
    
    func newChatUsers(request: RoomsViewModel.ChatUsers.Request) {
        jsonRequest(RestAPI.contacts) { [weak self] json, error in
            guard self != nil else {
                return
            }
            
            if error == nil {
                if let result = json?.dictionary {
                    if let error = result["error"]?.dictionaryObject {
                        self?.output.presentError(RoomsViewModel.Errors.serverError(msg: error["message"] as? String ?? "Unknown error"))
                    } else if let data = result["list"]?.arrayObject as? [[String: Any]] {
                        var users: [User] = []
                        for itemData in data {
                            let user = User(data: itemData)
                            if Session.user?.id != user.id {
                                users.append(user)
                            }
                        }
                        
                        let response = RoomsViewModel.ChatUsers.Response(roomName: request.roomName, users: users)
                        self?.output.newChatUsers(response: response)
                    }
                }
            } else {
                self?.output.presentError(error!)
            }
            
        }
    }
    
    func createRoom(request: RoomsViewModel.CreateRoom.Request) {
        if let manager = Session.socketManager(), request.users.count > 0 {
            let socket = manager.defaultSocket
            
            if socket.status == .connected {
                var params: [String: Any] = [:]
                params["userIds"] = request.users.map { $0.id }
                params["name"] = request.roomName
                
                socket.emitWithAck("join", params).timingOut(after: 3.0) { [weak self] data in
                    var rooms: [ChatRoom] = []
                    for room in data as! [[String: Any]] {
                        rooms.append(ChatRoom(data: room["room"] as! [String: Any]))
                    }
                    
                    socket.disconnect()
                    let response = RoomsViewModel.CreateRoom.Response(room: rooms[0])
                    self?.output.createRoom(response: response)
                }
            }
        }
    }
}
