//
//  UserChatViewCell.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 16.07.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit

protocol UserChatViewCellDelegate {
    func userChatViewCell(onTap cell: UserChatViewCell)
}

class UserChatViewCell: UICollectionViewCell {
    @IBOutlet weak var chatIconLabel: DesignableLabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var noteLabel: UILabel!
    
    var delegate: UserChatViewCellDelegate!
    var room: ChatRoom!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let grTap = UITapGestureRecognizer(target: self, action: #selector(self.onTap(_:)))
        self.addGestureRecognizer(grTap)
    }

    func configure(room: ChatRoom) {
        self.room = room
        self.nameLabel.text = room.name
        self.chatIconLabel.text = room.iconName ?? ""
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        timeLabel.text = dateFormatter.string(from: room.createdAt)
        noteLabel.text = room.lastMessage?.content
    }
    
    override func prepareForReuse() {
        chatIconLabel.text = nil
        nameLabel.text = nil
        timeLabel.text = nil
        noteLabel.text = nil
    }
    
    @objc func onTap(_ sender: UITapGestureRecognizer) {
        delegate.userChatViewCell(onTap: self)
    }
}
