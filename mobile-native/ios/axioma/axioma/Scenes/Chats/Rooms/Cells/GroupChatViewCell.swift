//
//  RoomsViewCell.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 16.07.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit

protocol GroupChatViewCellDelegate {
    func groupChatViewCell(onTap cell: GroupChatViewCell)
}

class GroupChatViewCell: UICollectionViewCell {
    
    @IBOutlet weak var chatIconLabel: DesignableLabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var noteLabel: UILabel!
    
    var room: ChatRoom!
    var delegate: GroupChatViewCellDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let grTap = UITapGestureRecognizer(target: self, action: #selector(self.onTap(_:)))
        self.addGestureRecognizer(grTap)
    }

    func configure(room: ChatRoom) {
        self.room = room
        self.nameLabel.text = room.name
        self.chatIconLabel.text = room.iconName ?? ""
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        timeLabel.text = dateFormatter.string(from: room.createdAt)
        userNameLabel.text = room.createdByUser?.name
        
        if let content = room.lastMessage?.content {
            noteLabel.text = content
            if userNameLabel.text != nil && !content.isEmpty {
                userNameLabel.text = userNameLabel.text! + ":"
            }
        } else {
            noteLabel.text = nil
        }
    }
    
    override func prepareForReuse() {
        chatIconLabel.text = nil
        nameLabel.text = nil
        timeLabel.text = nil
        userNameLabel.text = nil
        noteLabel.text = nil
    }
    
    @objc func onTap(_ sender: UITapGestureRecognizer) {
        delegate.groupChatViewCell(onTap: self)
    }
}
