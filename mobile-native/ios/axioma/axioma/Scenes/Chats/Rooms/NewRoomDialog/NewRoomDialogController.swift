//
//  NewRoomDialogController.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 17.08.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit

protocol NewRoomDialogControllerDelegate {
    func newRoomDialogController(onAccept text: String)
    func newRoomDialogController(onCancel sender: NewRoomDialogController?)
}

class NewRoomDialogController: UIViewController {

    var delegate: NewRoomDialogControllerDelegate!
    
    @IBOutlet weak var roomName: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    init() {
        super.init(nibName: "NewRoomDialogController", bundle: Bundle.main)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @IBAction func onCancel(_ sender: Any) {
        roomName.endEditing(true)
        dismiss(animated: false) {
            self.delegate.newRoomDialogController(onCancel: nil)
        }
    }
    
    @IBAction func onOk(_ sender: Any) {
        roomName.endEditing(true)
        if self.roomName.text != nil {
            dismiss(animated: false) {
                self.delegate.newRoomDialogController(onAccept: self.roomName.text!)
            }
        }
    }
    
}
