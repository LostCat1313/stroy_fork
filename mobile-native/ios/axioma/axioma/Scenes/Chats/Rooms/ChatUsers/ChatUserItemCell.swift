//
//  ChatUserItemCell.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 20.08.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit

protocol ChatUserItemCellDelegate {
    func chatUserItemCell(onTap sender: ChatUserItemCell)
}

class ChatUserItemCell: UICollectionViewCell {
    @IBOutlet weak var checkboxImage: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    
    var delegate: ChatUserItemCellDelegate!
    var user: User!
    
    var selectedItem: Bool = false {
        didSet {
            if selectedItem {
                checkboxImage.image = UIImage(named: "checkbox-marked-outline")
            } else {
                checkboxImage.image = UIImage(named: "checkbox-blank-outline")
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let grTap = UITapGestureRecognizer(target: self, action: #selector(self.onTap(_:)))
        self.addGestureRecognizer(grTap)
    }
    
    func configure(user: User) {
        userNameLabel.text = user.name
        self.user = user
        selectedItem = false
    }
    
    @objc func onTap(_ sender: UITapGestureRecognizer) {
        selectedItem = !selectedItem
        delegate.chatUserItemCell(onTap: self)
    }

}
