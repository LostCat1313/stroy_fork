//
//  ChatUsersController.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 20.08.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit

protocol ChatUsersControllerDelegate {
    func chatUsersController(onAccept viewModel: RoomsViewModel.ChatUsers.ViewModel)
}

class ChatUsersController: UIViewController {
    var viewModel: RoomsViewModel.ChatUsers.ViewModel!
    var selectedUsers: [User] = []
    var delegate: ChatUsersControllerDelegate!

    var users: [User] {
        return viewModel.users
    }
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        collectionView.register(UINib(nibName: "ChatUserItemCell", bundle: nil), forCellWithReuseIdentifier: "ChatUserItemCell")
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    

    init(viewModel: RoomsViewModel.ChatUsers.ViewModel) {
        super.init(nibName: "ChatUsersController", bundle: Bundle.main)
        self.viewModel = viewModel
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @IBAction func acceptButton(_ sender: Any) {
        dismiss(animated: false) {
            self.viewModel.users = self.selectedUsers
            self.delegate.chatUsersController(onAccept: self.viewModel)
        }
    }
    
    @IBAction func cancelButton(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }

}


extension ChatUsersController: UICollectionViewDelegate {
    
}

extension ChatUsersController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: 40)
    }
}

extension ChatUsersController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return users.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let user = users[indexPath.item]
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChatUserItemCell", for: indexPath) as! ChatUserItemCell
        cell.configure(user: user)
        cell.delegate = self
        return cell
    }
}

extension ChatUsersController: ChatUserItemCellDelegate {
    func chatUserItemCell(onTap sender: ChatUserItemCell) {
        selectedUsers = selectedUsers.filter { $0.id != sender.user.id }
        if sender.selectedItem {
            selectedUsers.append(sender.user)
        }
    }
}
