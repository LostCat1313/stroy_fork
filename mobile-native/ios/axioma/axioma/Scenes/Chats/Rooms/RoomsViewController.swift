//
//  RoomsViewController.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 12.07.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit

protocol RoomsViewControllerInput {
    func displayError(_ error: Error)
    func displayRooms(viewModel: RoomsViewModel.RoomList.ViewModel)
    func newChatUsers(viewModel: RoomsViewModel.ChatUsers.ViewModel)
    func createRoom(viewModel: RoomsViewModel.CreateRoom.ViewModel)
}

protocol RoomsViewControllerOutput {
    func fetchRooms(request: RoomsViewModel.RoomList.Request)
    func newChatUsers(request: RoomsViewModel.ChatUsers.Request)
    func createRoom(request: RoomsViewModel.CreateRoom.Request)
}

class RoomsViewController: UIViewController {
    enum RoomsControllerStates {
        case wait
        case ready
        case none
    }
    
    // VIP
    var output: RoomsViewControllerOutput!
    var router: RoomsViewRouter!
    var rooms: [ChatRoom] = []
    var state: RoomsControllerStates = .none
    var shareImages: [UIImage]?
    
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var topbar: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        collectionView.register(UINib(nibName: "GroupChatViewCell", bundle: nil), forCellWithReuseIdentifier: "GroupChatViewCell")
        collectionView.register(UINib(nibName: "UserChatViewCell", bundle: nil), forCellWithReuseIdentifier: "UserChatViewCell")
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.prefetchDataSource = self
    }

    override func viewDidAppear(_ animated: Bool) {
        switch state {
            case .wait:
                break
            default:
                let request = RoomsViewModel.RoomList.Request()
                self.output.fetchRooms(request: request)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setShareImages(images: [UIImage]) {
        shareImages = images
    }
    
    @IBAction func onReturn(_ sender: Any) {
        
        if let manager = Session.socketManager() {
            let socket = manager.defaultSocket
            
            //if socket.status == .connected {
                socket.disconnect()
            //}
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onNewChat(_ sender: Any) {
        self.state = .wait
        self.router.newChatRoom()
    }
    
    @IBAction func onMenu(_ sender: Any) {
        let menuItems: [PopupMenuItemProtocol] = [
            PopupMenuItem(title: "Новый чат", click: { a in
                self.state = .wait
                self.router.newChatRoom()
            }),
        ]
        let at = CGPoint(x: menuButton.frame.origin.x, y: topbar.frame.height)
        let at2 = view.convert(at, from: topbar)
        PopupMenuViewController.popup(over: self, at: at2, menuItems: menuItems)
    }
    
    init() {
        super.init(nibName: "RoomsViewController", bundle: Bundle.main)
        configureController()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func configureController() {
        RoomsViewConfigurator.sharedInstance.configure(viewController: self)
    }
    
}

extension RoomsViewController: RoomsViewControllerInput {
    func displayError(_ error: Error) {
        var description = error.localizedDescription
        
        if let err = error as? RoomsViewModel.Errors {
            switch err {
            case .serverError(let msg):
                description = msg
            }
        }
        
        let dlg = Session.makeErrorDialog("Error", message: description)
        self.present(dlg, animated: true)
    }
    
    func displayRooms(viewModel: RoomsViewModel.RoomList.ViewModel) {
        rooms = viewModel.rooms
        collectionView.reloadData()
        state = .ready
    }
    
    func newChatUsers(viewModel: RoomsViewModel.ChatUsers.ViewModel) {
        state = .wait
        router.goChatUsersPage(viewModel: viewModel)
    }
    
    func createRoom(viewModel: RoomsViewModel.CreateRoom.ViewModel) {
        //rooms.insert(viewModel.room, at: 0)
        //collectionView.reloadData()
        state = .ready
        
        let request = RoomsViewModel.RoomList.Request()
        self.output.fetchRooms(request: request)
        
    }
}

extension RoomsViewController: UICollectionViewDelegate {
    
}

extension RoomsViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: 68)
    }
}

extension RoomsViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return rooms.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let room = rooms[indexPath.item]
        
        if room.isPrivate {
            let cell: UserChatViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "UserChatViewCell", for: indexPath) as! UserChatViewCell
            cell.configure(room: room)
            cell.delegate = self
            return cell
        } else {
            let cell: GroupChatViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "GroupChatViewCell", for: indexPath) as! GroupChatViewCell
            cell.configure(room: room)
            cell.delegate = self
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        // This will cancel all unfinished downloading task when the cell disappearing.
        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
    }
}

extension RoomsViewController: UICollectionViewDataSourcePrefetching {
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        
    }
}

extension RoomsViewController: UserChatViewCellDelegate {
    func userChatViewCell(onTap cell: UserChatViewCell) {
        router.goChatRoom(room: cell.room)
    }
}

extension RoomsViewController: GroupChatViewCellDelegate {
    func groupChatViewCell(onTap cell: GroupChatViewCell) {
        router.goChatRoom(room: cell.room)
    }
}

extension RoomsViewController: NewRoomDialogControllerDelegate {
    func newRoomDialogController(onAccept text: String) {
        let request = RoomsViewModel.ChatUsers.Request(roomName: text)
        output.newChatUsers(request: request)
    }
    
    func newRoomDialogController(onCancel sender: NewRoomDialogController?) {
        state = .ready
    }
}

extension RoomsViewController: ChatUsersControllerDelegate {
    func chatUsersController(onAccept viewModel: RoomsViewModel.ChatUsers.ViewModel) {
        let request = RoomsViewModel.CreateRoom.Request(roomName: viewModel.roomName, users: viewModel.users)
        output.createRoom(request: request)
    }
}
