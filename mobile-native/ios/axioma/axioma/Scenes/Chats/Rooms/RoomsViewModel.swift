//
//  RoomsViewModel.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 12.07.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation

struct RoomsViewModel {
    enum Errors: Error {
        case serverError(msg: String)
    }
    
    struct RoomList {
        struct Request {
            
        }
        
        struct Response {
            var rooms: [ChatRoom]
        }
        
        struct ViewModel {
            var rooms: [ChatRoom]
        }
    }
    
    struct ChatUsers {
        struct Request {
            var roomName: String
        }
        
        struct Response {
            var roomName: String
            var users: [User]
        }
        
        struct ViewModel {
            var roomName: String
            var users: [User]
        }
    }
    
    struct CreateRoom {
        struct Request {
            var roomName: String
            var users: [User]
        }
        
        struct Response {
            var room: ChatRoom
        }
        
        struct ViewModel {
            var room: ChatRoom
        }
    }
}
