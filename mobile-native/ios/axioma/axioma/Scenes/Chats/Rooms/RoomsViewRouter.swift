//
//  RoomsViewRouter.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 12.07.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation
import UIKit

protocol RoomsViewRouterInput {
    
}

class RoomsViewRouter: RoomsViewRouterInput {
    weak var viewController: RoomsViewController!
    
    // MARK: Navigation
    
    func goChatRoom(room: ChatRoom) {
        let manager = Session.socketManager()
        let socket = manager!.defaultSocket
        socket.disconnect()
        
        let controller = ChatViewController(room: room)
    
        self.viewController.present(controller, animated: true) {
            if let images = self.viewController.shareImages {
                controller.setShareImages(images: images)
            }
        }
    }
    
    func newChatRoom() {
        let controller = NewRoomDialogController()
        controller.modalPresentationStyle = .overCurrentContext
        controller.delegate = self.viewController
        self.viewController.present(controller, animated: false)
    }
    
    func goChatUsersPage(viewModel: RoomsViewModel.ChatUsers.ViewModel) {
        let controller = ChatUsersController(viewModel: viewModel)
        controller.modalPresentationStyle = .overCurrentContext
        controller.delegate = self.viewController
        self.viewController.present(controller, animated: false)
    }
    
    // MARK: Communication
    
    func passDataToNextScene(segue: UIStoryboardSegue) {
        // NOTE: Teach the router which scenes it can communicate with
        
        //if segue.identifier == "ShowSomewhereScene" {
        //    passDataToScene(segue)
        //}
    }
    
    func passDataToScene(segue: UIStoryboardSegue) {
        // NOTE: Teach the router how to pass data to the next scene
        
        // let someWhereViewController = segue.destinationViewController as! SomeWhereViewController
        // someWhereViewController.output.name = viewController.output.name
    }
}
