//
//  NotificationsViewController.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 22.06.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit

protocol NotificationsViewControllerInput {
    func displayError(_ error: Error)
    func displayNotifications(viewModel: NotificationsViewModel.NotificationList.ViewModel)
}

protocol NotificationsViewControllerOutput {
    func fetchNotifications(request: NotificationsViewModel.NotificationList.Request)
}

protocol NotificationsViewControllerDelegate {
    func notificationsViewController(onSelect notification: Notification, project: ProjectData?)
}

class NotificationsViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    private var notifications: [Notification] = []
    private var allNotifications: [Notification] = []
    private var projects: [ProjectData]?
    private(set) var filters: [NotificationFilter] = [.documents, .notes, .messages, .project]
    var cursor: CursorState = .start(10)
    var delegate: NotificationsViewControllerDelegate!
    
    var allTypes: [NotificationType] {
        var result: [NotificationType] = []
        for filter in filters {
            result.append(contentsOf: filter.types)
        }
        return result
    }
    
    // VIP
    var output: NotificationsViewControllerOutput!
    var router: NotificationsViewRouter!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        NotificationsViewConfigurator.sharedInstance.configure(viewController: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        collectionView.register(UINib(nibName: "NotificationCell", bundle: nil), forCellWithReuseIdentifier: "NotificationCell")
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.prefetchDataSource = self
    }

    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    func initView() {
        self.allNotifications = []
        cursor = .start(10)
        projects = Session.projects
        nextPage()
    }
    
    func nextPage() {
        let request = NotificationsViewModel.NotificationList.Request(cursor: cursor.next())
        self.output.fetchNotifications(request: request)
        cursor = .waiting
    }
    
    func setData(notifications: [Notification], clear: Bool = true) {
        if clear {
            self.allNotifications = []
        }
        
        allNotifications.append(contentsOf: notifications)
        setFilters(filters: filters)
    }
    
    func setFilters(filters: [NotificationFilter]) {
        self.filters = filters
        notifications = []
        for notification in allNotifications {
            let found = allTypes.first {$0 == notification.type}
            if found != nil {
                notifications.append(notification)
            }
        }
        
        collectionView.reloadData()
    }
}

extension NotificationsViewController: NotificationsViewControllerInput {
    func displayError(_ error: Error) {
        var description = error.localizedDescription
        
        if let err = error as? NotificationsViewModel.Errors {
            switch err {
            case .serverError(let msg):
                description = msg
            }
        }
        
        let dlg = Session.makeErrorDialog("Error", message: description)
        self.present(dlg, animated: true)
    }
    
    func displayNotifications(viewModel: NotificationsViewModel.NotificationList.ViewModel) {
        cursor = viewModel.cursor
        setData(notifications: viewModel.notifications, clear: false)
    }
}

extension NotificationsViewController: UICollectionViewDelegate {
    
}

extension NotificationsViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: 150)
    }
}

extension NotificationsViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return notifications.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let notification = notifications[indexPath.item]
        
        switch(notification.type) {
            default:
                let cell: NotificationCell = collectionView.dequeueReusableCell(withReuseIdentifier: "NotificationCell", for: indexPath) as! NotificationCell
                let project = projects?.first(where: { prj in prj.id == notification.projectId })
                cell.configure(notification: notification, project: project)
                cell.delegate = self
                return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        // This will cancel all unfinished downloading task when the cell disappearing.
        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row > notifications.count - 2, let _ = cursor.params {
            nextPage()
        }
    }
}

extension NotificationsViewController: UICollectionViewDataSourcePrefetching {
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        
    }
}

extension NotificationsViewController: NotificationCellDelegate {
    func notificationCell(onTap cell: NotificationCell) {
        delegate.notificationsViewController(onSelect: cell.notification, project: cell.project)
    }
}
