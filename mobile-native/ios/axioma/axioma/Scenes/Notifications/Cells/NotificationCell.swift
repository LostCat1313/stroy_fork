//
//  NotificationNoteCell.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 22.06.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit
import SwiftyJSON
import Crashlytics

protocol NotificationCellDelegate {
    func notificationCell(onTap cell: NotificationCell)
}

class NotificationCell: UICollectionViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var projectNameLabel: UILabel!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var topBorderView: UIView!
    @IBOutlet weak var icon: UIImageView!
    
    var delegate: NotificationCellDelegate!
    var project: ProjectData?
    var notification: Notification! {
        didSet {
            icon.image = UIImage(named: notification.type.icon)
            topBorderView.backgroundColor = notification.type.color
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let grTap = UITapGestureRecognizer(target: self, action: #selector(self.onTap(_:)))
        self.addGestureRecognizer(grTap)
    }

    @IBAction func onMenu(_ sender: Any) {
        
        //NotificationMessage.show(notification: notification) {
        //    print("+++++++")
        //}

    }
    
    func configure(notification: Notification, project: ProjectData?) {
        self.notification = notification
        titleLabel.text = notification.title
        textLabel.text = notification.content
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        dateLabel.text = dateFormatter.string(from: notification.createdAt)
        projectNameLabel.text = project?.title ?? ""
        self.project = project
    }
    
    @objc func onTap(_ sender: UITapGestureRecognizer) {
        delegate.notificationCell(onTap: self)
    }
    
}
