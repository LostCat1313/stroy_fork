//
//  NotificationsViewPresenter.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 22.06.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation

protocol NotificationsViewPresenterInput {
    func presentError(_ error: Error)
    func presentNotifications(response: NotificationsViewModel.NotificationList.Response)
}

protocol NotificationsViewPresenterOutput: class {
    func displayError(_ error: Error)
    func displayNotifications(viewModel: NotificationsViewModel.NotificationList.ViewModel)
}

class NotificationsViewPresenter: NSObject, NotificationsViewPresenterInput {
    weak var output: NotificationsViewPresenterOutput!
    
    func presentError(_ error: Error) {
        self.output.displayError(error)
    }
    
    func presentNotifications(response: NotificationsViewModel.NotificationList.Response) {
        let viewModel = NotificationsViewModel.NotificationList.ViewModel(cursor: response.cursor, notifications: response.notifications)
        self.output.displayNotifications(viewModel: viewModel)
    }
    
}
