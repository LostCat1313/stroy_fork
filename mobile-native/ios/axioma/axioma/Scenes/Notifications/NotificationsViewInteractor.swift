//
//  NotificationsViewInteractor.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 22.06.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol NotificationsViewInteractorInput {
    func fetchNotifications(request: NotificationsViewModel.NotificationList.Request)
}

protocol NotificationsViewInteractorOutput {
    func presentError(_ error: Error)
    func presentNotifications(response: NotificationsViewModel.NotificationList.Response)
}

class NotificationsViewInteractor: NotificationsViewInteractorInput {
    var output: NotificationsViewInteractorOutput!
    //var worker = NotificationsViewWorker()
    
    func fetchNotifications(request: NotificationsViewModel.NotificationList.Request) {
        guard let params = request.cursor.params else {
            return
        }
        
        jsonRequest(RestAPI.notifications(params.offset, params.limit)) { [weak self] json, error in
            guard self != nil else {
                return
            }
            
            if error == nil {
                if let result = json?.dictionary {
                    if let error = result["error"]?.dictionaryObject {
                        self?.output.presentError(NotificationsViewModel.Errors.serverError(msg: error["message"] as? String ?? "Unknown error"))
                    } else if let data = result["list"]?.array {
                        var notifications: [Notification] = []
                        for itemData in data {
                            if let _ = NotificationType(rawValue: itemData["type"].intValue) {
                                notifications.append(Notification(json: itemData))
                            }
                        }
                        
                        let cursor = notifications.count == params.limit ? request.cursor : .eod
                        let response = NotificationsViewModel.NotificationList.Response(cursor: cursor, notifications: notifications)
                        self?.output.presentNotifications(response: response)
                    }
                }
            } else {
                self?.output.presentError(error!)
            }
            
        }
    }
}
