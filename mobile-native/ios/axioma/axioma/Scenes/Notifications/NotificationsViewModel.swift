//
//  NotificationsViewModel.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 22.06.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

enum CursorState {
    case start(Int)
    case fetch(Int, Int)
    case eod
    case waiting
    
    func next() -> CursorState {
        switch self {
            case .start(let limit):
                return .fetch(0, limit)
            case .fetch(let offset, let limit):
                return .fetch(offset + limit, limit)
            case .waiting:
                return .waiting
            default:
                return .eod
        }
    }
    
    var params: (offset: Int, limit: Int)? {
        switch self {
            case .start(let limit):
                return (0, limit)
            case .fetch(let offset, let limit):
                return (offset, limit)
            default:
                return nil
        }
    }
}

enum NotificationType: Int {
    case activationPushCode = 1
    case chatMessage = 201 // ?
    case chatRoomAdded = 202
    case documentAdded = 101
    case documentRemoved = 102
    case documentPagesAdded = 103
    case documentUpdated = 104
    case documentDescriptorAdded = 105
    case documentVersionAdded = 106
    case projectUserAdded = 107
    case projectUserChanged = 108
    case projectUserRemoved = 109
    
    case markerEdit = 10501
    case markerLink = 10502
    case markerNote = 10506
    
    var icon: String {
        switch(self) {
            case .documentAdded, .documentRemoved, .documentUpdated, .documentPagesAdded:
                return "file-orange"
            case .markerEdit:
                return "content-cut-red"
            case .markerLink:
                return "link-green"
            case .markerNote:
                return "star-lil"
            case .chatMessage:
                return "message_green"
            case .documentDescriptorAdded:
                return "note_green"
            default:
                return "file"
        }
    }
    
    var color: UIColor {
        switch(self) {
            case .documentAdded, .documentUpdated, .documentRemoved, .documentPagesAdded:
                return UIColor.orange
            case .markerEdit:
                return UIColor(red: 174/255, green: 19/255, blue: 124/255, alpha: 1)
            case .markerLink:
                return UIColor(red: 0/255, green: 150/255, blue: 136/255, alpha: 1)
            case .markerNote:
                return UIColor(red: 156/255, green: 39/255, blue: 176/255, alpha: 1)
            case .chatMessage:
                return UIColor(red: 76/255, green: 175/255, blue: 80/255, alpha: 1)
            case .documentDescriptorAdded:
                return UIColor(red: 199/255, green: 213/255, blue: 55/255, alpha: 1)
            default:
                return UIColor.gray
        }
    }
}

enum NotificationFilter: Int {
    case documents = 0
    case notes = 1
    case messages = 2
    case project = 3
    
    var types: [NotificationType] {
        switch(self) {
            case .documents:
                return [.documentAdded, .documentRemoved, .documentUpdated, .documentPagesAdded]
            case .notes:
                return [.documentDescriptorAdded, .markerEdit, .markerLink, .markerNote]
            case .messages:
                return [.chatMessage, .activationPushCode]
            case .project:
                return [.projectUserAdded, .projectUserChanged, .projectUserRemoved]
        }
    }
}


class Notification {
    var id: Int
    var type: NotificationType
    var title: String
    var createdAt: Date
    var updatedAt: Date
    var projectId: Int
    var targetId: Int?
    var organization: String?
    var content: String?
    var message: ChatMessage?
    var document: Document?
    var descriptor: PageMarker?
    var data: [String: Any]?
    
    init(id: Int, projectId: Int, type: NotificationType, title: String, createdAt: Date, updatedAt: Date) {
        self.id = id
        self.projectId = projectId
        self.type = type
        self.title = title
        self.createdAt = createdAt
        self.updatedAt = updatedAt
    }
    
    convenience init(json: JSON) {
        self.init(data: json.dictionaryObject!)
    }
    
    init(data: [String: Any]) {
        self.data = data
        id = data["id"] as! Int
        type = NotificationType(rawValue: data["type"] as! Int)!
        title = data["title"] as! String
        content = data["content"] as? String
        createdAt = (data["createdAt"] as! String).prefix(19).getDate(dateFormat: "yyyy-MM-dd'T'HH:mm:ss")
        updatedAt = (data["updatedAt"] as! String).prefix(19).getDate(dateFormat: "yyyy-MM-dd'T'HH:mm:ss")
        projectId = (data["projectId"] as? Int) ?? 0
        targetId = data["targetId"] as? Int
        
        if let documentData = data["document"] as? [String: Any] {
            document = Document(data: documentData)
        }
        
        if let messageData = data["message"] as? [String: Any] {
            message = ChatMessage(data: messageData)
        }
        
        if let markerData = data["descriptor"] as? [String: Any] {
            descriptor = PageMarker.create(json: JSON(markerData))
            
            if let markerType = PageMarkerTypeId(rawValue: markerData["type"] as! Int), type == .documentDescriptorAdded {
                switch markerType {
                case .edit:
                    type = .markerEdit
                    title = "Изменение"
                case .link:
                    type = .markerLink
                    title = "Ссылка"
                case .note:
                    type = .markerNote
                    title = "Замечание"
                default:
                    break
                }
            }
            
        }
    }
}

struct NotificationsViewModel {
    enum Errors: Error {
        case serverError(msg: String)
    }
    
    struct NotificationList {
        struct Request {
            var cursor: CursorState
        }
        
        struct Response {
            var cursor: CursorState
            var notifications: [Notification]
        }
        
        struct ViewModel {
            var cursor: CursorState
            var notifications: [Notification]
        }
    }
    
}
