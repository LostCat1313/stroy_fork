//
//  NotificationsViewConfigurator.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 22.06.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation
import UIKit

// MARK: Connect View, Interactor, and Presenter

extension NotificationsViewController: NotificationsViewPresenterOutput {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        router.passDataToNextScene(segue: segue)
    }
}

extension NotificationsViewInteractor: NotificationsViewControllerOutput {
}

extension NotificationsViewPresenter: NotificationsViewInteractorOutput {
}

class NotificationsViewConfigurator {
    // MARK: Object lifecycle
    static let sharedInstance = NotificationsViewConfigurator()
    
    // MARK: Configuration
    func configure(viewController: NotificationsViewController) {
        let router = NotificationsViewRouter()
        router.viewController = viewController
        
        let presenter = NotificationsViewPresenter()
        presenter.output = viewController
        
        let interactor = NotificationsViewInteractor()
        interactor.output = presenter
        //interactor.worker = NotificationsViewWorker()
        
        viewController.output = interactor
        viewController.router = router
    }
}
