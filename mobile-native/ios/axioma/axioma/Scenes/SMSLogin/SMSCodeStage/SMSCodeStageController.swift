//
//  SMSCodeStageController.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 05/12/2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit
import SwiftyJSON
import FirebaseAuth

protocol SMSCodeStageControllerDelegate: class {
    func smsCodeStageController(onLogin profile: User, countSummary: CountSummary)
}

class SMSCodeStageController: UIViewController {
    @IBOutlet weak var smsCodeField: UITextField!
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var topFormConstraint: NSLayoutConstraint!
    @IBOutlet weak var activateButton: UIButton!
    
    var topFormConstraintDefault: CGFloat = 0
    private var phone: String?
    private var code: String?
    weak var delegate: SMSCodeStageControllerDelegate?
    private var activationCode: String?
    private var verificationID: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShowNotification(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHideNotification(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        NotificationCenter.default.addObserver(self, selector:#selector(self.handlePushNotification(_:)), name: .pushNotificationReceivedOnForeground, object: nil)
        
        smsCodeField.delegate = self
        topFormConstraintDefault = topFormConstraint.constant
        //self.smsCodeField.text = code
        //activateButton.isEnabled = (code ?? "").count > 0
        let grTap = UITapGestureRecognizer(target: self, action: #selector(self.onTap(_:)))
        self.view.addGestureRecognizer(grTap)
        
        DispatchQueue.main.async {
            self.startCheckPhone(phone: self.phone!)
        }
    }

    init(phone: String, code: String?) {
        self.phone = phone
        self.code = code
        super.init(nibName: "SMSCodeStageController", bundle: Bundle.main)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self);
    }
    
    @objc func handlePushNotification(_ notification: Foundation.Notification) {
        /*
        if let eventString = notification.userInfo?["event"] as? String {
            let json = JSON.parse(eventString)
            let notification = Notification(json: json)
            
            switch notification.type {
                case .activationPushCode:
                    //smsCodeField.text = notification.content
                    activationCode = notification.content
                    activateButton.isEnabled = true
                    NotificationMessage.show(notification: notification) {
                    }
                default:
                    break
            }
        }*/
    }
    
    @objc func keyboardWillShowNotification(notification: NSNotification) {
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.topFormConstraint.constant = self.topFormConstraintDefault - (self.logoImage.frame.size.height + 2)
        })
    }
    
    @objc func onTap(_ sender: UITapGestureRecognizer) {
        smsCodeField.resignFirstResponder()
    }
    
    @objc func keyboardWillHideNotification(notification: NSNotification) {
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.topFormConstraint.constant = self.topFormConstraintDefault
        })
    }
    
    private func startCheckPhone(phone: String) {
        PhoneAuthProvider.provider().verifyPhoneNumber(phone, uiDelegate: nil) { (verificationID, error) in
            if error == nil {
                self.verificationID = verificationID
                /*
                let credential = PhoneAuthProvider.provider().credential(withVerificationID: verificationID!, verificationCode: code)
                Auth.auth().signInAndRetrieveData(with: credential) { (authResult, error) in
                    if let error = error {
                        self.presentError(error)
                    } else {
                        UserDefaults.standard.removeObject(forKey: "authVerificationID")
                        var socialDict = [String:Any]()
                        socialDict["registrationType"] = "signupByPhone"
                        for userProviderData in authResult!.user.providerData {
                            if userProviderData.providerID == "phone" {
                                socialDict["phoneNumber"] = userProviderData.phoneNumber
                            }
                        }
                        self.output.continueToUserDetails(socialDict: socialDict)
                    }
                }
                */
                print("[startCheckPhone] valid number")
            } else {
                self.displayError(error!)
            }
        }
    }
    
    private func verify() {
        if let verificationCode = smsCodeField.text {
            let credential = PhoneAuthProvider.provider().credential(withVerificationID: self.verificationID!, verificationCode: verificationCode)
            Auth.auth().signInAndRetrieveData(with: credential) { (authResult, error) in
                if let error = error {
                    self.displayError(error)
                } else {
                    for userProviderData in authResult!.user.providerData {
                        if userProviderData.providerID == "phone" {
                            print("[verify] phone:\(userProviderData.phoneNumber!)")
                            self.activateUser(phone: userProviderData.phoneNumber!)
                            break
                        }
                    }
                }
            }
        }
    }
    
    private func activateUser(phone: String) {
        if let code = self.code {
            jsonRequest(RestAPI.activate(phone: phone, code: code)) { json, error in
                if error != nil {
                    self.displayError(error!)
                } else {
                    DispatchQueue.main.async {
                        Session.activate(token: json!["token"].stringValue)
                        
                        if let signup = json!["signup"].bool {
                            if signup {
                                let profile = User(id: 0, name: phone)
                                profile.phoneNumber = phone
                                let controller = ProfileStageController(profile: profile)
                                controller.delegate = self
                                self.present(controller, animated: false)
                            } else if let userData = json?["user"] {
                                let profile = User(json: userData)
                                
                                if profile.name.isEmpty || (profile.lastName?.isEmpty ?? true) {
                                    profile.phoneNumber = phone
                                    let controller = ProfileStageController(profile: profile)
                                    controller.delegate = self
                                    self.present(controller, animated: false)
                                } else {
                                    var countSummary = CountSummary()
                                    if let summary = json?["countSummary"].dictionaryObject {
                                        countSummary = CountSummary(data: summary)
                                    }
                                    
                                    self.dismiss(animated: false) {
                                        self.delegate?.smsCodeStageController(onLogin: profile, countSummary: countSummary)
                                    }
                                }
                            }
                        } else {
                            print("[activate] error:\(json!.stringValue)")
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func onActivate(_ sender: Any) {
        verify()
    }
    
    @IBAction func onBack(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
    
    func displayError(_ error: Error) {
        let description = error.localizedDescription
        
        let dlg = Session.makeErrorDialog("Error", message: description) {
            // onClose
        }
        
        self.present(dlg, animated: true)
    }
}

extension SMSCodeStageController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var text = textField.text ?? ""
        text = (text as NSString).replacingCharacters(in: range, with: string)
        activateButton.isEnabled = text.count > 0
        return true
    }
}

extension SMSCodeStageController: ProfileStageControllerDelegate {
    
    func profileStageController(onCancel sender: ProfileStageController) {
        dismiss(animated: false, completion: nil)
    }
    
    func profileStageController(onLogin profile: User, countSummary: CountSummary) {
        dismiss(animated: false) {
            self.delegate?.smsCodeStageController(onLogin: profile, countSummary: countSummary)
        }
    }
}

