//
//  ProfileStageController.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 06/12/2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit

protocol ProfileStageControllerDelegate: class {
    func profileStageController(onCancel sender: ProfileStageController)
    func profileStageController(onLogin profile: User, countSummary: CountSummary)
}

class ProfileStageController: UIViewController {
    enum State {
        case none
        case editName
        case editLastName
        case editSecondName
        case editPosition
        case editEmail
        case editOrganization
    }

    @IBOutlet weak var organizationMenuButon: UIButton!
    @IBOutlet weak var positionMenuButton: UIButton!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var secondNameTextField: UITextField!
    @IBOutlet weak var positionTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var organizationTextField: UITextField!
    @IBOutlet weak var topViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var nextButton: UIButton!
    
    private var state: State = .none {
        didSet {
            let height = view.frame.size.height
            var sy = height
            
            switch state {
                case .editSecondName:
                    sy = secondNameTextField.frame.origin.y + 60 + keyboardHeight
                case .editPosition:
                    sy = positionTextField.frame.origin.y + 60 + keyboardHeight
                case .editEmail:
                    sy = emailTextField.frame.origin.y + 60 + keyboardHeight
                case .editOrganization:
                    sy = organizationTextField.frame.origin.y + 60 + keyboardHeight
                default:
                    break
            }
            
            if sy > height {
                topViewConstraint.constant = height - sy
            } else {
                topViewConstraint.constant = 0
            }
            
            checkProfile()
        }
    }
    
    private var keyboardHeight: CGFloat = 0
    private var organizations: [Organization]?
    private var positions: [OrganizationPosition]?
    private var profile = User(id: 0, name: "")
    weak var delegate: ProfileStageControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //init toolbar
        let toolbar:UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: 30))
        //create left side empty space so that done button set on right side
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneBtn: UIBarButtonItem = UIBarButtonItem(title: "Next", style: .done, target: self, action: #selector(self.nextButtonKeyboard))
        toolbar.setItems([flexSpace, doneBtn], animated: false)
        toolbar.sizeToFit()
        nameTextField.inputAccessoryView = toolbar
        lastNameTextField.inputAccessoryView = toolbar
        secondNameTextField.inputAccessoryView = toolbar
        positionTextField.inputAccessoryView = toolbar
        emailTextField.inputAccessoryView = toolbar
        organizationTextField.inputAccessoryView = toolbar
        
        nameTextField.delegate = self
        lastNameTextField.delegate = self
        secondNameTextField.delegate = self
        positionTextField.delegate = self
        emailTextField.delegate = self
        organizationTextField.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHideNotification(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        
        let grTap = UITapGestureRecognizer(target: self, action: #selector(self.onTap(_:)))
        self.view.addGestureRecognizer(grTap)
    }

    init(profile: User) {
        self.profile = profile
        super.init(nibName: "ProfileStageController", bundle: Bundle.main)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self);
    }
    
    @objc func onTap(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
        state = .none
    }
    
    @objc func keyboardWillHideNotification(notification: NSNotification) {
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            //self.topFormConstraint.constant = self.topFormConstraintDefault
        })
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            keyboardHeight = keyboardSize.height
        }
    }
    
    @objc func nextButtonKeyboard() {
        view.endEditing(true)
        switch state {
            case .editName:
                lastNameTextField.becomeFirstResponder()
            case .editLastName:
                secondNameTextField.becomeFirstResponder()
            case .editSecondName:
                positionTextField.becomeFirstResponder()
            case .editPosition:
                emailTextField.becomeFirstResponder()
            case .editEmail:
                organizationTextField.becomeFirstResponder()
            case .editOrganization:
                organizationTextField.endEditing(true)
                if isValidProfile {
                    onNext(self)
                }
            default:
                break
        }
    }
    
    private func showOrganizationsMenu() {
        guard organizations != nil else {
            return
        }
        
        let ac = UIAlertController(title: "Организации", message: "Выбери", preferredStyle: .actionSheet)
        for organization in organizations! {
            ac.addAction(UIAlertAction(title: organization.name, style: .default, handler: { _ in
                self.profile.organizationId = organization.id
                self.organizationTextField.text = organization.name
            }))
        }
        ac.addAction(UIAlertAction(title: "Выход", style: .cancel))
        self.present(ac, animated: true)
    }
    
    @IBAction func onOrgMenu(_ sender: Any) {
        if organizations == nil {
            ApplicationAPI.organizations { organizations, error in
                if error == nil {
                    self.organizations = organizations
                    DispatchQueue.main.async {
                        self.showOrganizationsMenu()
                    }
                } else {
                    self.displayError(error!)
                }
            }
        } else {
            self.showOrganizationsMenu()
        }
    }
    
    private func showPositionsMenu() {
        guard positions != nil else {
            return
        }
        
        let ac = UIAlertController(title: "Должности", message: "Выбери", preferredStyle: .actionSheet)
        for position in positions ?? [] {
            ac.addAction(UIAlertAction(title: position.name, style: .default, handler: { _ in
                self.profile.positionId = position.id
                self.positionTextField.text = position.name
            }))
        }
        ac.addAction(UIAlertAction(title: "Выход", style: .cancel))
        self.present(ac, animated: true)
    }
    
    @IBAction func onPositionsMenu(_ sender: Any) {
        if positions == nil {
            ApplicationAPI.positions { positions, error in
                if error == nil {
                    self.positions = positions
                    DispatchQueue.main.async {
                        self.showPositionsMenu()
                    }
                } else {
                    self.displayError(error!)
                }
            }
        } else {
            self.showPositionsMenu()
        }
    }
    
    @IBAction func onNext(_ sender: Any) {
        profile.firstName = nameTextField.text
        profile.secondName = secondNameTextField.text
        profile.lastName = lastNameTextField.text
        profile.email = emailTextField.text
        
        var params: [String: Any] = [:]
        var entry: [String:Any] = profile.getParams()
        entry["positionName"] = positionTextField.text
        entry["organizationName"] = organizationTextField.text
        params["entry"] = entry
        
        jsonRequest(RestAPI.updateProfile(params)) { json, error in
            if error != nil {
                 self.displayError(error!)
            } else {
                let user = User(json: json!)
                
                var countSummary = CountSummary()
                if let summary = json?["countSummary"].dictionaryObject {
                    countSummary = CountSummary(data: summary)
                }
                
                self.dismiss(animated: false) {
                    self.delegate?.profileStageController(onLogin: user, countSummary: countSummary)
                }
            }
        }
    }
    
    @IBAction func onBack(_ sender: Any) {
        dismiss(animated: true) {
            self.delegate?.profileStageController(onCancel: self)
        }
    }
    
    func displayError(_ error: Error) {
        let dlg = Session.makeErrorDialog("Error", message: error.localizedDescription)
        self.present(dlg, animated: true)
    }
    
    private var isValidProfile: Bool {
        let name = (nameTextField.text ?? "").replacingOccurrences(of: " ", with: "")
        let lastName = (lastNameTextField.text ?? "").replacingOccurrences(of: " ", with: "")
        return name.count > 0 && lastName.count > 0
    }
    
    private func checkProfile() {
        if isValidProfile {
            nextButton.isEnabled = true
            nextButton.alpha = 1
        } else {
            nextButton.isEnabled = false
            nextButton.alpha = 0.5
        }
    }
    
}

extension ProfileStageController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField === nameTextField {
            state = .editName
        } else if textField === lastNameTextField {
            state = .editLastName
        } else if textField === secondNameTextField {
            state = .editSecondName
        } else if textField === positionTextField {
            state = .editPosition
        } else if textField === emailTextField {
            state = .editEmail
        } else if textField === organizationTextField {
            state = .editOrganization
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var text = textField.text ?? ""
        text = (text as NSString).replacingCharacters(in: range, with: string)
        //activateButton.isEnabled = text.count > 0
        return true
    }
}
