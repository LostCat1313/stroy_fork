//
//  PhoneStageController.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 04/12/2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit
import FirebaseAuth

protocol PhoneStageControllerDelegate: class {
    func phoneStageController(onLogin profile: User)
}

class PhoneStageController: UIViewController {

    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var phoneNumberField: UITextField!
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var topFormConstraint: NSLayoutConstraint!
    @IBOutlet weak var countryCode: UIButton!
    
    private var topFormConstraintDefault: CGFloat = 0
    weak var delegate: PhoneStageControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShowNotification(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHideNotification(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        phoneNumberField.delegate = self
        
        // Do any additional setup after loading the view.
        topFormConstraintDefault = topFormConstraint.constant
        
        let grTap = UITapGestureRecognizer(target: self, action: #selector(self.onTap(_:)))
        self.view.addGestureRecognizer(grTap)
    }

    init() {
        super.init(nibName: "PhoneStageController", bundle: Bundle.main)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    deinit {
        NotificationCenter.default.removeObserver(self);
    }
    
    @objc func onTap(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    @objc func keyboardWillShowNotification(notification: NSNotification) {
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.topFormConstraint.constant = self.topFormConstraintDefault - (self.logoImage.frame.size.height + 2)
        })
    }
    
    @objc func keyboardWillHideNotification(notification: NSNotification) {
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.topFormConstraint.constant = self.topFormConstraintDefault
        })
    }
    
    @IBAction func onBack(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
    
    @IBAction func onCounrtyCode(_ sender: Any) {
    }
    
    @IBAction func onSend(_ sender: Any) {
        if let number = phoneNumberField.text, let prefix = self.countryCode.titleLabel?.text {
            let phone = "\(prefix)\(number)"
            jsonRequest(RestAPI.signup(phone: phone, regId: Session.regId!)) { json, error in
                if error != nil {
                    self.displayError(error!)
                } else {
                    DispatchQueue.main.async {
                        let code = json!["activationCode"].stringValue
                        let controller = SMSCodeStageController(phone: phone, code: code)
                        controller.delegate = self
                        self.present(controller, animated: true)
                    }
                }
            }
        }
    }
    
    func displayError(_ error: Error) {
        let description = error.localizedDescription
        let dlg = Session.makeErrorDialog("Error", message: description)
        self.present(dlg, animated: true)
    }
}

extension PhoneStageController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var text = textField.text ?? ""
        text = (text as NSString).replacingCharacters(in: range, with: string)
        sendButton.isEnabled = text.count == 10
        return true
    }
}

extension PhoneStageController: SMSCodeStageControllerDelegate {
    func smsCodeStageController(onLogin profile: User, countSummary: CountSummary) {
        dismiss(animated: false) {
            if let sessionId = Session.id {
                Session.login(id: profile.id, name: profile.name, sessionId: sessionId)
                Session.countSummary = countSummary
                self.delegate?.phoneStageController(onLogin: profile)
            } else {
                let dlg = Session.makeErrorDialog("Ошибка", message: "Идентификатор сессии не определен!")
                self.present(dlg, animated: true)
            }
        }
    }
}
