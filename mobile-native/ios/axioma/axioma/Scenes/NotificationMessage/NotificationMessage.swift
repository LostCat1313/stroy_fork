//
//  NotificationMessage.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 30/01/2019.
//  Copyright © 2019 Andrei Okoneshnikov. All rights reserved.
//

import UIKit
import AVFoundation

class NotificationMessage: UIViewController {

    enum State {
        case hidden
        case showing(notification: Notification)
        
        var notification: Notification? {
            switch self {
            case .showing(let notification):
                return notification
            default:
                return nil
            }
        }
    }
    
    private var state: State = .hidden {
        didSet {
            switch state {
            case .showing(_):
                self.setMessageText()
            default:
                break
            }
        }
    }
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var messageTextView: UITextView!
    
    var onAction: (() -> ())?
    private static var pendingCount: Int = 0
    
    public static let instance: NotificationMessage = NotificationMessage()
    
    init() {
        super.init(nibName: "NotificationMessage", bundle: Bundle.main)
        self.modalPresentationStyle = .overCurrentContext
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    class func show(notification: Notification, delay: Double? = 3.0, onAction: @escaping () -> ()) {
        
        NotificationMessage.hide() {
            if let topController = UIApplication.topViewController() {
                topController.view.addSubview(instance.view)
                NotificationMessage.instance.state = .showing(notification: notification)
                NotificationMessage.instance.onAction = onAction
                pendingCount += 1
                let count = pendingCount
                // to play sound
                DispatchQueue.main.async {
                    let systemSoundId: SystemSoundID = 1114
                    AudioServicesPlaySystemSound(systemSoundId)
                    
                    if delay != nil {
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + delay!) {
                            if count == self.pendingCount {
                                NotificationMessage.hide()
                            }
                        }
                    }
                }
            
            }
        }
        
    }
    
    class func hide(completion: (()->())? = nil) {
        DispatchQueue.main.async {
            instance.view.removeFromSuperview()
            completion?()
        }
    }

    @IBAction func onClose(_ sender: Any) {
        NotificationMessage.hide()
    }
    
    @IBAction func onSelect(_ sender: Any) {
        NotificationMessage.hide() {
            self.onAction?()
        }
    }
    
    private func setMessageText() {
        guard let notification = state.notification else {
            return
        }
        
        messageTextView.text = "\(notification.title). \(notification.content ?? "")"
        image.image = UIImage(named: notification.type.icon)
    }
    
}
