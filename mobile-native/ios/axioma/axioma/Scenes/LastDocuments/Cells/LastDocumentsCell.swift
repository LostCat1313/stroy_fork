//
//  LastDocumentCell.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 05.09.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit

protocol LastDocumentsCellDelegate {
    func lastDocumentsCell(onTap sender: LastDocumentsCell)
}

class LastDocumentsCell: UICollectionViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var projectName: UILabel!
    
    var document: Document!
    var project: ProjectData?
    var delegate: LastDocumentsCellDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let grTap = UITapGestureRecognizer(target: self, action: #selector(self.onTap(_:)))
        self.addGestureRecognizer(grTap)
    }

    func configure(document: Document, project: ProjectData?) {
        self.document = document
        self.project = project
        title.text = document.title
        projectName.text = project?.title
    }
    
    @objc func onTap(_ sender: UITapGestureRecognizer) {
        delegate.lastDocumentsCell(onTap: self)
    }
}
