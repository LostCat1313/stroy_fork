//
//  LastDocumentsViewController.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 05.06.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit

protocol LastDocumentsViewControllerInput {
    func displayError(_ error: Error)
    func documentsList(viewModel: LastDocumentsViewModel.DocumentsList.ViewModel)
}

protocol LastDocumentsViewControllerOutput {
    func documentsList(request: LastDocumentsViewModel.DocumentsList.Request)
}

protocol LastDocumentsViewControllerDelegate {
    func lastDocumentsViewController(selectDocument document: Document, project: ProjectData)
}

class LastDocumentsViewController: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    
    var documents: [Document] = []
    private var projects: [ProjectData]?
    var delegate: LastDocumentsViewControllerDelegate!
    
    // VIP
    var output: LastDocumentsViewControllerOutput!
    var router: LastDocumentsViewRouter!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        LastDocumentsViewConfigurator.sharedInstance.configure(viewController: self)
        
        collectionView.register(UINib(nibName: "LastDocumentsCell", bundle: nil), forCellWithReuseIdentifier: "LastDocumentsCell")
        
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    func initView() {
        projects = Session.projects
        let request = LastDocumentsViewModel.DocumentsList.Request()
        output.documentsList(request: request)
    }
    
}

extension LastDocumentsViewController: LastDocumentsViewControllerInput {
    func displayError(_ error: Error) {
        let description = error.localizedDescription
        let dlg = Session.makeErrorDialog("Error", message: description)
        self.present(dlg, animated: true)
    }
    
    func documentsList(viewModel: LastDocumentsViewModel.DocumentsList.ViewModel) {
        documents = viewModel.documents
        
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
}

extension LastDocumentsViewController: UICollectionViewDelegate {
    
}

extension LastDocumentsViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: 50)
    }
}

extension LastDocumentsViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return documents.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let document = documents[indexPath.item]
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LastDocumentsCell", for: indexPath) as! LastDocumentsCell
        let project = projects?.first(where: { prj in prj.id == document.projectId })
        cell.configure(document: document, project: project)
        cell.delegate = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        // This will cancel all unfinished downloading task when the cell disappearing.
        
    }
    
}

extension LastDocumentsViewController: LastDocumentsCellDelegate {
    func lastDocumentsCell(onTap sender: LastDocumentsCell) {
        if sender.project != nil {
            delegate.lastDocumentsViewController(selectDocument: sender.document, project: sender.project!)
        }
    }
}
