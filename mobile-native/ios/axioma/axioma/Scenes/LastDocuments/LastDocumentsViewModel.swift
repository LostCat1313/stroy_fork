//
//  LastDocumentsViewModel.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 04.09.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation

struct LastDocumentsViewModel {
    struct DocumentsList {
        struct Request {
            
        }
        
        struct Response {
            var documents: [Document]
        }
        
        struct ViewModel {
            var documents: [Document]
        }
    }
}
