//
//  LastDocumentsViewConfigurator.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 04.09.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation
import UIKit

// MARK: Connect View, Interactor, and Presenter

extension LastDocumentsViewController: LastDocumentsViewPresenterOutput {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        router.passDataToNextScene(segue: segue)
    }
}

extension LastDocumentsViewInteractor: LastDocumentsViewControllerOutput {
}

extension LastDocumentsViewPresenter: LastDocumentsViewInteractorOutput {
}

class LastDocumentsViewConfigurator {
    // MARK: Object lifecycle
    static let sharedInstance = LastDocumentsViewConfigurator()
    
    // MARK: Configuration
    func configure(viewController: LastDocumentsViewController) {
        let router = LastDocumentsViewRouter()
        router.viewController = viewController
        
        let presenter = LastDocumentsViewPresenter()
        presenter.output = viewController
        
        let interactor = LastDocumentsViewInteractor()
        interactor.output = presenter
        //interactor.worker = LastDocumentsViewWorker()
        
        viewController.output = interactor
        viewController.router = router
    }
}
