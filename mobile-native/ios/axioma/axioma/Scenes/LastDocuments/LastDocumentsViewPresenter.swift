//
//  LastDocumentsViewPresenter.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 04.09.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation

protocol LastDocumentsViewPresenterInput {
    func presentError(_ error: Error)
    func documentsList(response: LastDocumentsViewModel.DocumentsList.Response)
}

protocol LastDocumentsViewPresenterOutput: class {
    func displayError(_ error: Error)
    func documentsList(viewModel: LastDocumentsViewModel.DocumentsList.ViewModel)
}

class LastDocumentsViewPresenter: NSObject, LastDocumentsViewPresenterInput {
    weak var output: LastDocumentsViewPresenterOutput!
    
    func presentError(_ error: Error) {
        self.output.displayError(error)
    }
    
    func documentsList(response: LastDocumentsViewModel.DocumentsList.Response) {
        let viewModel = LastDocumentsViewModel.DocumentsList.ViewModel(documents: response.documents)
        output.documentsList(viewModel: viewModel)
    }
}
