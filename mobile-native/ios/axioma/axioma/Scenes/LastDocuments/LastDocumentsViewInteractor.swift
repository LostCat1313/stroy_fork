//
//  LastDocumentsViewInteractor.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 04.09.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol LastDocumentsViewInteractorInput {
    func documentsList(request: LastDocumentsViewModel.DocumentsList.Request)
}

protocol LastDocumentsViewInteractorOutput {
    func presentError(_ error: Error)
    func documentsList(response: LastDocumentsViewModel.DocumentsList.Response)
}

class LastDocumentsViewInteractor: LastDocumentsViewInteractorInput {
    var output: LastDocumentsViewInteractorOutput!
    //var worker = LastDocumentsViewWorker()
    
    func documentsList(request: LastDocumentsViewModel.DocumentsList.Request) {
        
        jsonRequest(RestAPI.lastDocuments) { [weak self] json, error in
            guard self != nil else {
                return
            }
            if error == nil {
               var documents: [Document] = []
                if let items = json?.dictionary?["list"]?.array {
                    for item in items {
                        documents.append(Document(json: item))
                    }
                }
                let response = LastDocumentsViewModel.DocumentsList.Response(documents: documents)
                self?.output.documentsList(response: response)
                
            } else {
                self?.output.presentError(error!)
            }
            
        }
    }
}
