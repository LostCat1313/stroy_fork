//
//  ProjectItemCell.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 16.06.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit
import Kingfisher

protocol ProjectItemCellDelegate: class {
    func projectItemCell(selectProject projectInfo: ProjectData)
    func projectItemCell(onAction sender: ProjectItemCell)
    func projectItemCell(onChangeProject sender: ProjectItemCell)
    func projectItemCell(onDeleteProject sender: ProjectItemCell)
    func projectItemCell(onError error: Error)
    func projectItemCell(getController for: ProjectItemCell) -> UIViewController
}

class ProjectItemCell: UICollectionViewCell {

    @IBOutlet weak var createdLabel: UILabel!
    @IBOutlet weak var membersCountLabel: UILabel!
    @IBOutlet weak var documentsCountLabels: UILabel!
    @IBOutlet weak var projectNameLabel: UILabel!
    @IBOutlet weak var projectImage: UIImageView!
    @IBOutlet weak var shadowBorderView: UIView!
    @IBOutlet weak var actonsButton: UIButton!
    @IBOutlet weak var actionMenuView: UIView!
    
    @IBOutlet weak var btnDelete: UIButton!
    weak var delegate: ProjectItemCellDelegate!
    let defaultImage = UIImage(named: "login_bg")
    private(set) var projectInfo: ProjectData!
    
    @IBAction func onSelectProject(_ sender: Any) {
        delegate.projectItemCell(selectProject: projectInfo)
    }
    
    @IBAction func onActionsMenu(_ sender: Any) {
        delegate.projectItemCell(onAction: self)
        actionMenuView.isHidden = !actionMenuView.isHidden
    }
    
    @IBAction func onChangeProject(_ sender: Any) {
        actionMenuView.isHidden = true
        delegate.projectItemCell(onChangeProject: self)
    }
    
    @IBAction func onDeleteProject(_ sender: Any) {
        actionMenuView.isHidden = true
        delegate.projectItemCell(onDeleteProject: self)
    }
    
    @IBAction func onBtnUsers(_ sender: Any) {
        hideMenu()
        UserAPI.getUsers { allUsers, error in
            if error == nil {
                let validUsers = allUsers!.filter {$0.firstName != nil && $0.lastName != nil}
                let rights = ProjectsViewWorker.projectUserRights(project: self.projectInfo, allUsers: validUsers)
                let vc = self.delegate.projectItemCell(getController: self)
                UsersPickerViewController.show(vc, delegate: self, users: rights)
            } else {
                self.delegate.projectItemCell(onError: error!)
            }
        }
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        actionMenuView.isHidden = true
        let grTap = UITapGestureRecognizer(target: self, action: #selector(self.onTap(_:)))
        self.addGestureRecognizer(grTap)
    }

    func hideMenu() {
        actionMenuView.isHidden = true
    }
    
    @objc func onTap(_ sender: UITapGestureRecognizer) {
        actionMenuView.isHidden = true
    }

    
    func configure(_ projectInfo: ProjectData) {
        self.projectInfo = projectInfo
        if projectInfo.isCurrentAdmin {
            btnDelete.isEnabled = true
            btnDelete.alpha = 1
        } else {
            btnDelete.isEnabled = false
            btnDelete.alpha = 0.4
        }
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd MMM yyyy"
        createdLabel.text = dateFormatterPrint.string(from: projectInfo.created)
        projectNameLabel.text = projectInfo.title
        membersCountLabel.text = "\(projectInfo.usersCount)"
        documentsCountLabels.text = "\(projectInfo.documentsCount)"
        projectImage.image = defaultImage
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        projectImage.image = defaultImage
        actionMenuView.isHidden = true
    }
}
extension ProjectItemCell: UsersPickerControllerDelegate {
    func usersPickerController(getCaptionFor picker: UsersPickerViewController) -> String {
        return "Пользователи проекта"
    }
    
    func usersPickerController(getApplyCaptionFor picker: UsersPickerViewController) -> String {
        return "СОХРАНИТЬ"
    }
    
    func usersPickerController(apply users: [(user: User, caption: String, state: UserPickerItemState)]) {
        let rights: [(userId: Int, role: Rights.Role)] = users.reduce(into: []) { acc, item in
            switch item.state {
                case .included(_, let roleMode):
                    switch roleMode {
                        case .enabled(let role):
                            acc.append((userId: item.user.id, role: role))
                        default:
                            break
                    }
                
                default:
                    break
            }
        }
        
        UserAPI.setProjectUsers(projectId: projectInfo.id, rights: rights) { error in
            if let e = error {
                self.delegate.projectItemCell(onError: e)
            } else {
                self.projectInfo.users = rights.map {UserRole(userId: $0.userId, role: $0.role)}
            }
        }
    }
    
    func usersPickerController(canChangeFor picker: UsersPickerViewController) -> Bool {
        return projectInfo.isCurrentAdmin
    }
    
    func usersPickerController(isRadioFor picker: UsersPickerViewController) -> Bool {
        return false
    }
}
