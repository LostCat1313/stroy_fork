//
//  ProjectsViewInteractor.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 16.06.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol ProjectsViewInteractorInput {
    func projectsList(request: ProjectsViewModel.ProjectsList.Request)
    func uploadFile(request: ProjectsViewModel.UploadFile.Request)
    func createProject(request: ProjectsViewModel.CreateProject.Request)
    func updateProject(request: ProjectsViewModel.UpdateProject.Request)
    func deleteProject(request: ProjectsViewModel.DeleteProject.Request)
}

protocol ProjectsViewInteractorOutput {
    func presentError(_ error: Error)
    func projectsList(response: ProjectsViewModel.ProjectsList.Response)
    func uploadFile(response: ProjectsViewModel.UploadFile.Response)
    func createProject(response: ProjectsViewModel.CreateProject.Response)
    func updateProject(response: ProjectsViewModel.UpdateProject.Response)
    func deleteProject(response: ProjectsViewModel.DeleteProject.Response)
}

class ProjectsViewInteractor: ProjectsViewInteractorInput {
    var output: ProjectsViewInteractorOutput!
    //var worker = ProjectsViewWorker()
    
    func projectsList(request: ProjectsViewModel.ProjectsList.Request) {
        ProjectAPI.loadProjects() { [weak self] projects, error in
            guard self != nil else {
                return
            }
            
            if error != nil {
                self?.output.presentError(error!)
            } else {
                let response = ProjectsViewModel.ProjectsList.Response(projects: projects!)
                self?.output.projectsList(response: response)
            }
        }
    }
    
    func uploadFile(request: ProjectsViewModel.UploadFile.Request) {
        FileAPI.uploadFiles(files: [request.url], uploadProgress: request.uploadProgress) { [weak self] files, error in
            if self == nil {
                return
            }
            
            if error != nil {
                self!.output.presentError(error!)
            } else {
                let ids = files!.map { $0.id }
                let response = ProjectsViewModel.UploadFile.Response(url: request.url, fileId: ids[0])
                self!.output.uploadFile(response: response)
            }
        }
    }
    
    func createProject(request: ProjectsViewModel.CreateProject.Request) {
        ProjectAPI.createProject(title: request.name, imageId: request.fileId ?? 0) { [weak self] project, error in
            if self == nil {
                return
            }
            
            if error != nil {
                self!.output.presentError(error!)
            } else {
                let response = ProjectsViewModel.CreateProject.Response(project: project!)
                self!.output.createProject(response: response)
            }
        }
    }
    
    func updateProject(request: ProjectsViewModel.UpdateProject.Request) {
        ProjectAPI.updateProject(projectId: request.projectId, title: request.name, imageId: request.fileId ?? 0) { [weak self] project, error in
            if self == nil {
                return
            }
            
            if error != nil {
                self!.output.presentError(error!)
            } else {
                let response = ProjectsViewModel.UpdateProject.Response(project: project!)
                self!.output.updateProject(response: response)
            }
        }
    }
    
    func deleteProject(request: ProjectsViewModel.DeleteProject.Request) {
        jsonRequest(RestAPI.deleteProject(request.projectId)) { [weak self] _, error in
            if error != nil {
                self?.output.presentError(error!)
            } else {
                let response = ProjectsViewModel.DeleteProject.Response(projectId: request.projectId)
                self?.output.deleteProject(response: response)
            }
        }
    }

}
