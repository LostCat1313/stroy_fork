//
//  ProjectsViewConfigurator.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 16.06.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation
import UIKit

// MARK: Connect View, Interactor, and Presenter

extension ProjectsViewController: ProjectsViewPresenterOutput {
    //override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    //    router.passDataToNextScene(segue: segue)
    //}
}

extension ProjectsViewInteractor: ProjectsViewControllerOutput {
}

extension ProjectsViewPresenter: ProjectsViewInteractorOutput {
}

class ProjectsViewConfigurator {
    // MARK: Object lifecycle
    static let sharedInstance = ProjectsViewConfigurator()
    
    // MARK: Configuration
    func configure(viewController: ProjectsViewController) {
        let router = ProjectsViewRouter()
        router.viewController = viewController
        
        let presenter = ProjectsViewPresenter()
        presenter.output = viewController
        
        let interactor = ProjectsViewInteractor()
        interactor.output = presenter
        //interactor.worker = ProjectsViewWorker()
        
        viewController.output = interactor
        viewController.router = router
    }
}
