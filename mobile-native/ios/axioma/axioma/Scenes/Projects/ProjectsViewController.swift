//
//  ProjectsViewController.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 16.06.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit
import Kingfisher

protocol ProjectsViewControllerInput {
    func displayError(_ error: Error)
    func projectsList(viewModel: ProjectsViewModel.ProjectsList.ViewModel)
    func uploadFile(viewModel: ProjectsViewModel.UploadFile.ViewModel)
    func createProject(viewModel: ProjectsViewModel.CreateProject.ViewModel)
    func updateProject(viewModel: ProjectsViewModel.UpdateProject.ViewModel)
    func deleteProject(viewModel: ProjectsViewModel.DeleteProject.ViewModel)
   
}

protocol ProjectsViewControllerOutput {
    func projectsList(request: ProjectsViewModel.ProjectsList.Request)
    func uploadFile(request: ProjectsViewModel.UploadFile.Request)
    func createProject(request: ProjectsViewModel.CreateProject.Request)
    func updateProject(request: ProjectsViewModel.UpdateProject.Request)
    func deleteProject(request: ProjectsViewModel.DeleteProject.Request)
}

class ProjectsViewController: UIViewController {
    enum States {
        case none
        case loading
        case loaded
        case editProjectData(project: ProjectData?)
        case uploadingImage(url: URL, projectName: String, project: ProjectData?)
        case creatingProject()
        case updatingProject(project: ProjectData)
    }
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var plusButton: UIButton!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    var progressView: ProgressViewController?
    var state: States = .none {
        didSet {
            switch state {
                case .loading, .uploadingImage, .creatingProject, .updatingProject:
                    spinner?.startAnimating()
                default:
                    spinner?.stopAnimating()
            }
        }
    }
    
    // VIP
    var output: ProjectsViewControllerOutput!
    var router: ProjectsViewRouter!

    var projects: [ProjectData] = []
    
    @IBAction func onBackButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        collectionView.register(UINib(nibName: "ProjectItemCell", bundle: nil), forCellWithReuseIdentifier: "ProjectItemCell")
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.prefetchDataSource = self
        
        // load data
        state = .loading
        let request = ProjectsViewModel.ProjectsList.Request()
        self.output.projectsList(request: request)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: Initializers
    
    init() {
        super.init(nibName: "ProjectsViewController", bundle: Bundle.main)
        configureController()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    private func configureController() {
        ProjectsViewConfigurator.sharedInstance.configure(viewController: self)
    }

    @IBAction func onPlusButton(_ sender: Any) {
        let actions = [
            PopupMenuItem(title: "Новый проект", click: { a in
                self.state = .editProjectData(project: nil)
                self.router.navigateToEditProjectDialog()
    
            }),
        ]
        
        let at = CGPoint(x: plusButton.frame.origin.x, y: plusButton.frame.origin.y - 45)
        let style = PopupMenuStyle()
        style.bkColor = Configuration.Colors.orange
        PopupMenuViewController.popup(over: self, at: at, menuItems: actions, style: style)
    }
    
}

extension ProjectsViewController: ProjectsViewControllerInput {
    
    func displayError(_ error: Error) {
        progressView?.dismiss(animated: false, completion: nil)
        let description = error.localizedDescription
        
        let dlg = Session.makeErrorDialog("Error", message: description) {
            // onClose
        }
        
        self.present(dlg, animated: true)
        state = .loaded
    }
    
    func projectsList(viewModel: ProjectsViewModel.ProjectsList.ViewModel) {
        projects = viewModel.projects
        Session.projects = projects
        state = .loaded
        collectionView.reloadData()
    }
    
    func uploadFile(viewModel: ProjectsViewModel.UploadFile.ViewModel) {
        progressView?.dismiss(animated: false, completion: nil)
        
        switch state {
            case .uploadingImage(_, let projectName, let project):
                if project == nil {
                    state = .creatingProject()
                    let request = ProjectsViewModel.CreateProject.Request(name: projectName, fileId: viewModel.fileId)
                    output.createProject(request: request)
                } else {
                    state = .updatingProject(project: project!)
                    let request = ProjectsViewModel.UpdateProject.Request(projectId: project!.id, name: projectName, fileId: viewModel.fileId)
                    output.updateProject(request: request)
                }
            default:
                break
        }
        
    }
    
    func createProject(viewModel: ProjectsViewModel.CreateProject.ViewModel) {
        projects.insert(viewModel.project, at: 0)
        Session.projects = projects
        
        collectionView.reloadData()
        state = .loaded
    }
    
    func updateProject(viewModel: ProjectsViewModel.UpdateProject.ViewModel) {
        projects = projects.filter {$0.id != viewModel.project.id}
        
        projects.insert(viewModel.project, at: 0)
        Session.projects = projects
        
        collectionView.reloadData()
        state = .loaded

    }
    
    func deleteProject(viewModel: ProjectsViewModel.DeleteProject.ViewModel) {
        projects = projects.filter { $0.id != viewModel.projectId }
        Session.projects = projects
        state = .loaded
        collectionView.reloadData()
    }
    
}

extension ProjectsViewController: UICollectionViewDelegate {
    
}

extension ProjectsViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: 320)
    }
}

extension ProjectsViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return projects.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: ProjectItemCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProjectItemCell", for: indexPath) as! ProjectItemCell
        cell.configure(projects[indexPath.item])
        cell.delegate = self
        cell.projectImage.kf.indicatorType = .activity
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        // This will cancel all unfinished downloading task when the cell disappearing.
        (cell as! ProjectItemCell).projectImage.kf.cancelDownloadTask()
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        guard let imageUrl = projects[indexPath.item].imageUrl else {
            return
        }

        if let url = URL(string: imageUrl) {
            if let projectImage = (cell as! ProjectItemCell).projectImage {
                projectImage.kf.setImage(
                    with: url,
                    placeholder: nil,
                    options: [.transition(ImageTransition.fade(1)), .requestModifier(imageRequestModifier)],
                    progressBlock: { receivedSize, totalSize in
                        print("\(indexPath.row + 1): \(receivedSize)/\(totalSize)")
                    },
                    completionHandler: { image, error, cacheType, imageURL in
                        if image == nil {
                            DispatchQueue.main.async {
                                projectImage.image = (cell as! ProjectItemCell).defaultImage
                            }
                        }
                    }
                )
            }
        }
    }
}

extension ProjectsViewController: UICollectionViewDataSourcePrefetching {
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        
        var urls: [URL] = []
        for indexPath in indexPaths {
            if let imageUrl = projects[indexPath.item].imageUrl, let url = URL(string: imageUrl) {
                urls.append(url)
            }
        }
        
        ImagePrefetcher(urls: urls, options: [.requestModifier(imageRequestModifier)]).start()
 
    }
}

extension ProjectsViewController: ProjectItemCellDelegate {
    
    func projectItemCell(getController for: ProjectItemCell) -> UIViewController {
        return self
    }
    
    func projectItemCell(onError error: Error) {
        displayError(error)
    }
    
    func projectItemCell(selectProject projectInfo: ProjectData) {
        Session.currentProject = projectInfo
        let dlg = Session.makeMessageDialog("Проекты", message: "Выбран текущий проект:\(projectInfo.title)")
        self.present(dlg, animated: true)
    }
    
    func projectItemCell(onAction sender: ProjectItemCell) {
        for cell in collectionView.visibleCells {
            if let projectCell = cell as? ProjectItemCell {
                projectCell.hideMenu()
            }
        }
    }
    
    func projectItemCell(onChangeProject sender: ProjectItemCell) {
        switch state {
            case .loaded:
                self.state = .editProjectData(project: sender.projectInfo)
                router.navigateToEditProjectDialog(project: sender.projectInfo)
            default:
                break
        }
    }
    
    func projectItemCell(onDeleteProject sender: ProjectItemCell) {
        let alert = UIAlertController(title: "Удаление проекта: \(sender.projectInfo.title)",
            message: "Вы действительно хотите удалить проект?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "НЕТ", style: .default, handler: { _ in} ))
        alert.addAction(UIAlertAction(title: "ДА", style: .destructive, handler: { _ in
            self.state = .loading
            let request = ProjectsViewModel.DeleteProject.Request(projectId: sender.projectInfo.id)
            self.output.deleteProject(request: request)
        }))
        present(alert, animated: true, completion: nil)
    }
}

extension ProjectsViewController: NewProjectDialogDelegate {
    
    func newProjectDialog(onCancel sender: Any?) {
        switch state {
            case .editProjectData(_):
                state = .loaded
            default:
                break
        }
    }
    
    func newProjectDialog(onAccept projectName: String, newFileUrl: URL?, clearImage: Bool) {
        switch state {
            case .editProjectData(let project):
                if let url = newFileUrl {
                    progressView = ProgressViewController.show(over: self, caption: "Загрузка изображения", progress: 0)
                    let onProgress:(_ progress: Progress) -> () = { progress in
                        self.progressView!.progress = Float(progress.fractionCompleted)
                    }
                    
                    state = .uploadingImage(url: url, projectName: projectName, project: project)
                    let request = ProjectsViewModel.UploadFile.Request(url: url, uploadProgress: onProgress)
                    output.uploadFile(request: request)
                } else {
                    if project == nil {
                        state = .creatingProject()
                        let request = ProjectsViewModel.CreateProject.Request(name: projectName, fileId: nil)
                        output.createProject(request: request)
                    } else {
                        let imageId = clearImage ? nil : project?.imageId
                        state = .updatingProject(project: project!)
                        let request = ProjectsViewModel.UpdateProject.Request(projectId: project!.id, name: projectName, fileId: imageId)
                        output.updateProject(request: request)
                    }
                }
            default:
                break
        }
    }
}
