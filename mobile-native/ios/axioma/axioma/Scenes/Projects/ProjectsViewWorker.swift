//
//  ProjectsViewWorker.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 04.07.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation

typealias UserRightsInfo = (user: User, caption: String, state: UserPickerItemState)

class ProjectsViewWorker {
    
    class func projectUserRights(project: ProjectData, allUsers: [User]) -> [UserRightsInfo] {
        let rights: [UserRightsInfo] = allUsers.reduce(into: []) { acc, item in
            let projUsers: [UserRole] = project.users ?? []
            let includedRole =  projUsers.first(where: {$0.userId == item.id})
            let fName: String = item.firstName ?? "???"
            let lName: String = item.lastName ?? "???"
            var caption = "\(fName) \(lName)"
            var readOnly = false
            if item.id == project.ownerId {
                caption += " (владелец)"
                readOnly = true
            } else if item.id == Session.user!.id {
                caption += " (текущий)"
                readOnly = true
            }
            if let r = includedRole {
                let itemState = UserPickerItemState.included(readOnly: readOnly, roleMode: .enabled(r.role))
                acc.append((user: item, caption: caption, state: itemState))
            } else {
                let itemState = UserPickerItemState.excluded(readOnly: readOnly, roleMode: .enabled(.reader))
                acc.append((user: item, caption: caption, state: itemState))
            }
        }
        return rights
    }

}
