//
//  ProjectsViewPresenter.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 16.06.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation

protocol ProjectsViewPresenterInput {
    func presentError(_ error: Error)
    func projectsList(response: ProjectsViewModel.ProjectsList.Response)
    func uploadFile(response: ProjectsViewModel.UploadFile.Response)
    func createProject(response: ProjectsViewModel.CreateProject.Response)
    func updateProject(response: ProjectsViewModel.UpdateProject.Response)
    func deleteProject(response: ProjectsViewModel.DeleteProject.Response)

}

protocol ProjectsViewPresenterOutput: class {
    func displayError(_ error: Error)
    func projectsList(viewModel: ProjectsViewModel.ProjectsList.ViewModel)
    func uploadFile(viewModel: ProjectsViewModel.UploadFile.ViewModel)
    func createProject(viewModel: ProjectsViewModel.CreateProject.ViewModel)
    func updateProject(viewModel: ProjectsViewModel.UpdateProject.ViewModel)
    func deleteProject(viewModel: ProjectsViewModel.DeleteProject.ViewModel)

}

class ProjectsViewPresenter: NSObject, ProjectsViewPresenterInput {
    weak var output: ProjectsViewPresenterOutput!
    
    func presentError(_ error: Error) {
        self.output.displayError(error)
    }
    
    func projectsList(response: ProjectsViewModel.ProjectsList.Response) {
        let viewModel = ProjectsViewModel.ProjectsList.ViewModel(projects: response.projects)
        self.output.projectsList(viewModel: viewModel)
    }
    
    func uploadFile(response: ProjectsViewModel.UploadFile.Response) {
        let viewModel = ProjectsViewModel.UploadFile.ViewModel(url: response.url, fileId: response.fileId)
        output.uploadFile(viewModel: viewModel)
    }
    
    func createProject(response: ProjectsViewModel.CreateProject.Response) {
        let viewModel = ProjectsViewModel.CreateProject.ViewModel(project: response.project)
        output.createProject(viewModel: viewModel)
    }
    
    func updateProject(response: ProjectsViewModel.UpdateProject.Response) {
        let viewModel = ProjectsViewModel.UpdateProject.ViewModel(project: response.project)
        output.updateProject(viewModel: viewModel)
    }
    
    func deleteProject(response: ProjectsViewModel.DeleteProject.Response) {
        let viewModel = ProjectsViewModel.DeleteProject.ViewModel(projectId: response.projectId)
        output.deleteProject(viewModel: viewModel)
    }
    
}
