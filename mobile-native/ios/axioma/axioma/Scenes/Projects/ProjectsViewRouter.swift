//
//  ProjectsViewRouter.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 29/11/2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation
import UIKit

protocol ProjectsViewRouterInput {
    
}

class ProjectsViewRouter: ProjectsViewRouterInput {
    weak var viewController: ProjectsViewController!
    
    // MARK: Navigation
    
    func navigateToEditProjectDialog(project: ProjectData? = nil) {
        let controller = NewProjectDialog(project: project)
        controller.modalPresentationStyle = .overCurrentContext
        controller.delegate = viewController
        viewController.present(controller, animated: true)
    }
    
    // MARK: Communication
    
    func passDataToNextScene(segue: UIStoryboardSegue) {
        // NOTE: Teach the router which scenes it can communicate with
        
        //if segue.identifier == "ShowSomewhereScene" {
        //    passDataToScene(segue)
        //}
    }
    
    func passDataToScene(segue: UIStoryboardSegue) {
        // NOTE: Teach the router how to pass data to the next scene
        
        // let someWhereViewController = segue.destinationViewController as! SomeWhereViewController
        // someWhereViewController.output.name = viewController.output.name
    }
}
