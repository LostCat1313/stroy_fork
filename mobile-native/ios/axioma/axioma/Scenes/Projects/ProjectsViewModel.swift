//
//  ProjectsViewModel.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 16.06.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class ProjectData {
    var id: Int
    var ownerId: Int
    var title: String
    var created: Date
    var usersCount: Int
    var documentsCount: Int
    var imageId: Int?
    var imageUrl: String?
    var image: UIImage?
    var users: [UserRole]?
    
    init(json: JSON) {
        id = json["id"].intValue
        ownerId = json["createdBy"].intValue
        title = json["title"].stringValue
        created = json["createdAt"].stringValue.prefix(19).getDate(dateFormat: "yyyy-MM-dd'T'HH:mm:ss")
        usersCount = Int(json["usersCount"].stringValue) ?? 0
        documentsCount = Int(json["documentsCount"].stringValue) ?? 0
        imageId = json["imageId"].intValue
        imageUrl = json["image"].dictionary?["url"]?.string
        users = json["users"].array?.reduce(into: []) {acc, itemJson in
            acc?.append(UserRole(itemJson))
        }
    }
    var isCurrentOwner: Bool {
        if let currId = Session.user?.id {
            
            return ownerId == currId
        }
        return false
    }
    var isCurrentReader: Bool {
        if let currId = Session.user?.id {
            return ownerId == currId || users?.contains(where: {$0.userId == currId && $0.role.canRead}) ?? false
        }
        return false
    }
    
    var isCurrentWriter: Bool {
        if let currId = Session.user?.id {
            return ownerId == currId || users?.contains(where: {$0.userId == currId && $0.role.canWrite}) ?? false
        }
        return false
    }
    
    var isCurrentAdmin: Bool {
        if let currId = Session.user?.id {
            return ownerId == currId || users?.contains(where: {$0.userId == currId && $0.role.canControl}) ?? false
        }
        return false
    }
}

struct ProjectsViewModel {
    enum Errors: Error {
        case serverError(msg: String)
    }
    
    struct ProjectsList {
        struct Request {
            
        }
        
        struct Response {
            var projects: [ProjectData]
        }
        
        struct ViewModel {
            var projects: [ProjectData]
        }
    }
    
    struct UploadFile {
        struct Request {
            var url: URL
            var uploadProgress:((_ progress: Progress)->())?
        }
        
        struct Response {
            var url: URL
            var fileId: Int
        }
        
        struct ViewModel {
            var url: URL
            var fileId: Int
        }
    }
    
    struct CreateProject {
        struct Request {
            var name: String
            var fileId: Int?
        }
        
        struct Response {
            var project: ProjectData
        }
        
        struct ViewModel {
            var project: ProjectData
        }
    }
    
    struct UpdateProject {
        struct Request {
            var projectId: Int
            var name: String
            var fileId: Int?
        }
        
        struct Response {
            var project: ProjectData
        }
        
        struct ViewModel {
            var project: ProjectData
        }
    }
    
    struct DeleteProject {
        struct Request {
            var projectId: Int
        }
        
        struct Response {
            var projectId: Int
        }
        
        struct ViewModel {
            var projectId: Int
        }
    }



}
