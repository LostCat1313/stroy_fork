//
//  NewProjectDialog.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 29/10/2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit
import Kingfisher

protocol NewProjectDialogDelegate: class {
    func newProjectDialog(onCancel sender: Any?)
    func newProjectDialog(onAccept projectName: String, newFileUrl: URL?, clearImage: Bool)
}

class NewProjectDialog: UIViewController {
    enum State {
        case none
        case edit(project: ProjectData?, newFileUrl: URL?, clearImage: Bool)
    }
    
    @IBOutlet weak var projectName: UITextField!
    @IBOutlet weak var projectImage: UIImageView!
    @IBOutlet weak var cleanImageButton: UIButton!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var btnImage: UIButton!
    let defaultImage = UIImage(named: "login_bg")
    
    private var state: NewProjectDialog.State = .none
    weak var delegate: NewProjectDialogDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        spinner.stopAnimating()
        
        // Do any additional setup after loading the view.
        projectName.delegate = self
        
        switch state {
            case .edit(let project, _, _):
                let isAdmin = project?.isCurrentAdmin ?? true
                if isAdmin {
                    btnAccept.isEnabled = true
                    btnAccept.alpha = 1
                } else {
                    btnAccept.isEnabled = false
                    btnAccept.alpha = 0.4
                }
                projectName.text = project?.title
                cleanImageButton.isHidden = project?.imageUrl == nil
                self.projectImage.image = self.defaultImage
                
                if let imageUrl = project?.imageUrl, let url = URL(string: imageUrl) {
                    projectImage.kf.setImage(
                        with: url,
                        placeholder: nil,
                        options: [.transition(ImageTransition.fade(1)), .requestModifier(imageRequestModifier)],
                        progressBlock: { receivedSize, totalSize in
                            
                    },
                        completionHandler: { image, error, cacheType, imageURL in
                            if image != nil {
                                DispatchQueue.main.async {
                                    self.projectImage.image = image
                                }
                            } else {
                                self.projectImage.image = self.defaultImage
                            }
                    }
                    )
            }
            
            case .none:
                btnAccept.isEnabled = true
                btnAccept.alpha = 1
        }
        
        let grTap = UITapGestureRecognizer(target: self, action: #selector(self.onTap(_:)))
        self.view.addGestureRecognizer(grTap)
    }

    init(project: ProjectData? = nil) {
        super.init(nibName: "NewProjectDialog", bundle: Bundle.main)
        state = .edit(project: project, newFileUrl: nil, clearImage: false)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @objc func onTap(_ sender: UITapGestureRecognizer) {
        projectName.resignFirstResponder()
    }
    
    @IBAction func onImage(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let picker = UIImagePickerController()
            picker.sourceType = .photoLibrary
            picker.delegate = self
            present(picker, animated: true, completion: nil)
        }
    }
    
    @IBAction func onCleanImage(_ sender: Any) {
    
        projectImage.image = nil
        cleanImageButton.isHidden = true
        
        switch state {
            case .edit(let project, _, _):
                state = .edit(project: project, newFileUrl: nil, clearImage: true)
            default:
                break
        }
    }
    
    @IBAction func onCancel(_ sender: Any) {
        self.dismiss(animated: true) {
            self.delegate.newProjectDialog(onCancel: nil)
        }
    }
    
    @IBAction func onAccept(_ sender: Any) {
        self.dismiss(animated: true) {
            if let text = self.projectName.text {
                switch self.state {
                    case .edit(_, let newFileUrl, let clearImage):
                        self.delegate.newProjectDialog(onAccept:text, newFileUrl: newFileUrl, clearImage: clearImage)
                    default:
                        break
                }
            }
        }
    }
    
}

extension NewProjectDialog: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage, let url = info[UIImagePickerControllerImageURL] as? URL {
            picker.dismiss(animated: true, completion: nil)
            projectImage.image = image
            cleanImageButton.isHidden = false
            
            switch state {
                case .edit(let project, _, let clearImage):
                    state = .edit(project: project, newFileUrl: url, clearImage: clearImage)
                default:
                    break
            }
        }
    }
}

extension NewProjectDialog: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}
