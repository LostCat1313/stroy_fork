//
//  LoginViewController.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 07.06.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit

protocol LoginViewControllerInput {
    func displayError(_ error: Error)
    func didLogin(viewModel: LoginViewModel.ViewModel)
}

protocol LoginViewControllerOutput {
    func login(request: LoginViewModel.Request)
}

class LoginViewController: UIViewController {

    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var loginText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var topFormSpaceConstraint: NSLayoutConstraint!
    
    var topFormSpaceConstraintDefault: CGFloat = 0
    
    // VIP
    var output: LoginViewControllerOutput!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShowNotification(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHideNotification(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        loginText.delegate = self
        passwordText.delegate = self
        topFormSpaceConstraintDefault = topFormSpaceConstraint.constant
        Session.close()
        
        let grTap = UITapGestureRecognizer(target: self, action: #selector(self.onTap(_:)))
        self.view.addGestureRecognizer(grTap)
    }

    @objc func onTap(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self);
    }
    
    @IBAction func onLoginButton(_ sender: Any) {
        // clear sessionId
        Session.close()
        
        if loginText.text != nil && passwordText != nil {
            let request = LoginViewModel.Request(userName: loginText.text!, password: passwordText.text!)
            self.output.login(request: request)
        }
    }
    
    @IBAction func onSMSLogin(_ sender: Any) {
        let controller = PhoneStageController()
        controller.delegate = self
        self.present(controller, animated: false, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func keyboardWillShowNotification(notification: NSNotification) {
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.topFormSpaceConstraint.constant = self.topFormSpaceConstraintDefault - (self.logoImage.frame.size.height + 2)
        })
    }
    
    @objc func keyboardWillHideNotification(notification: NSNotification) {
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            //self.topFormSpaceConstraint.constant += self.logoImage.frame.size.height + 2
        })
    }
}

extension LoginViewController: LoginViewControllerInput {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        LoginViewConfigurator.sharedInstance.configure(viewController: self)
    }
    
    func displayError(_ error: Error) {
        var description = error.localizedDescription
        
        if let err = error as? LoginViewError {
            switch err {
                case .emptyUserName:
                    description = "Заполните поле Логин"
                case .emptyPassword:
                    description = "Введите пароль"
                case .invalidLoginOrPassword:
                    description = "Не правильный логин или пароль"
                case .serverError(_):
                    description = "Сетевая ошибка." //WTF??
            }
        }
        
        let dlg = Session.makeErrorDialog("Error", message: description)
        self.present(dlg, animated: true)
    }
    
    func didLogin(viewModel: LoginViewModel.ViewModel) {
        let user = viewModel.user
        Session.login(id: user.id, name: user.name, sessionId: viewModel.sessionId)
        
        if let summary = viewModel.countSummary {
            Session.countSummary = summary
        }
        self.dismiss(animated: true, completion: nil)
    }
}

extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
}

extension LoginViewController: PhoneStageControllerDelegate {
    func phoneStageController(onLogin profile: User) {
        self.dismiss(animated: false, completion: nil)
    }
}
