//
//  LoginViewModel.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 11.06.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation
import SwiftyJSON

enum LoginViewError: Error {
    case emptyUserName
    case emptyPassword
    case invalidLoginOrPassword
    case serverError(msg: String)
}

struct LoginViewModel {
    struct Request {
        var userName: String
        var password: String
    }
    
    struct Response {
        var sessionId: String
        var user: User
        var countSummary: CountSummary?
    }
    
    struct ViewModel {
        var sessionId: String
        var user: User
        var countSummary: CountSummary?
    }
}
