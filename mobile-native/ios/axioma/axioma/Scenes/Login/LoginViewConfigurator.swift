//
//  LoginViewConfigurator.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 11.06.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation
import UIKit

// MARK: Connect View, Interactor, and Presenter

extension LoginViewController: LoginViewPresenterOutput {
    //override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    //    router.passDataToNextScene(segue: segue)
    //}
}

extension LoginViewInteractor: LoginViewControllerOutput {
}

extension LoginViewPresenter: LoginViewInteractorOutput {
}

class LoginViewConfigurator {
    // MARK: Object lifecycle
    static let sharedInstance = LoginViewConfigurator()
    
    // MARK: Configuration
    func configure(viewController: LoginViewController) {
        //let router = LoginViewRouter()
        //router.viewController = viewController
        
        let presenter = LoginViewPresenter()
        presenter.output = viewController
        
        let interactor = LoginViewInteractor()
        interactor.output = presenter
        //interactor.worker = LoginViewWorker()
        
        viewController.output = interactor
        //viewController.router = router
    }
}
