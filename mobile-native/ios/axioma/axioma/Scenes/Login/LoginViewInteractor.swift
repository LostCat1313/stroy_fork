//
//  LoginViewInteractor.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 11.06.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol LoginViewInteractorInput {
    func login(request: LoginViewModel.Request)
}

protocol LoginViewInteractorOutput {
    func presentError(_ error: Error)
    func presentLogin(response: LoginViewModel.Response)
}

class LoginViewInteractor: LoginViewInteractorInput {
    var output: LoginViewInteractorOutput!
    //var worker = LoginViewWorker()
    
    func login(request: LoginViewModel.Request) {
        if request.userName.isEmpty {
            return self.output.presentError(LoginViewError.emptyUserName)
        }
        
        if request.password.isEmpty {
            return self.output.presentError(LoginViewError.emptyPassword)
        }
        
        
        jsonRequest(RestAPI.login(name: request.userName, pass: request.password)) { [weak self] json, error in
            guard self != nil else {
                return
            }
            
            if error == nil {
                if let result = json?.dictionaryValue {
                    if let e = result["error"]?.dictionaryObject {
                        self?.output.presentError(LoginViewError.serverError(msg: e["message"] as? String ?? "???"))
                    } else if let token = result["token"]?.string {
                    
                        let user = User(json: result["user"]!)
                        
                        var countSummary = CountSummary()
                        if let summary = json?["countSummary"].dictionaryObject {
                            countSummary = CountSummary(data: summary)
                        }
 
                        let response = LoginViewModel.Response(sessionId: token, user: user, countSummary: countSummary)
                        self?.output.presentLogin(response: response)
                    }
                }
            } else {
                self?.output.presentError(error!)
            }
            
        }
    }
}
