//
//  LoginViewPresenter.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 11.06.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation

protocol LoginViewPresenterInput {
    func presentError(_ error: Error)
    func presentLogin(response: LoginViewModel.Response)
}

protocol LoginViewPresenterOutput: class {
    func displayError(_ error: Error)
    func didLogin(viewModel: LoginViewModel.ViewModel)
}

class LoginViewPresenter: NSObject, LoginViewPresenterInput {
    weak var output: LoginViewPresenterOutput!
    
    func presentError(_ error: Error) {
        self.output.displayError(error)
    }
    
    func presentLogin(response: LoginViewModel.Response) {
        let viewModel = LoginViewModel.ViewModel(sessionId: response.sessionId, user: response.user, countSummary: response.countSummary)
        self.output.didLogin(viewModel: viewModel)
    }
}
