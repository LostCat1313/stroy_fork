//
//  PersonalNoteViewController.swift
//  axioma
//
//  Created by sss on 09.10.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit
enum PersonalNoteViewResult {
    case none
    case changed
}

class PersonalNoteViewController: UIViewController {
    var note: PersonalNote!
    var completion: ((_ res: PersonalNoteViewResult)->())!
    var res: PersonalNoteViewResult = .none
    
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var spinnerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = note.id == 0 ? "Новая заметка" : "Заметка"
        textView.text = note.text
        btnSave.isHidden = true
    }
    
    static public func show(_ over: UIViewController, note: PersonalNote, completion: @escaping ((_ res: PersonalNoteViewResult)->())) {
        let controller = PersonalNoteViewController()
        controller.completion = completion
        controller.note = note
        over.present(controller, animated: true)
    }
    
    func displayError(_ error: Error) {
        let description = error.localizedDescription
        let dlg = Session.makeErrorDialog("Error", message: description)
        self.present(dlg, animated: true)
    }
    
    @IBAction func onBtnClose(_ sender: UIButton) {
        dismiss(animated: true, completion: {self.completion(self.res)})
    }
    
    @IBAction func onBtnSave(_ sender: UIButton) {
        let tmpNote = PersonalNote(id: note.id, text: textView.text)
        showSpinner()
        if tmpNote.id != 0 {
            PersonalNotesAPI.update(note: tmpNote) { err in
                self.hideSpinner()
                if let e = err {
                    self.displayError(e)
                } else {
                    self.btnSave.isHidden = true
                    self.res = .changed
                }
            }
        } else {
            PersonalNotesAPI.create(note: tmpNote) { new, err in
                self.hideSpinner()
                if let e = err {
                    self.displayError(e)
                } else {
                    self.btnSave.isHidden = true
                    self.note = new
                    self.res = .changed
                }
            }
        }
        
    }
    
}

extension PersonalNoteViewController : UITextViewDelegate {
    public func textViewDidChange(_ textView: UITextView) {
        btnSave.isHidden = textView.text.count == 0
    }
}

//SPINNER
extension PersonalNoteViewController {
    func showSpinner() {
        spinnerView.isHidden = false
    }
    
    func hideSpinner() {
        spinnerView.isHidden = true
    }
}
