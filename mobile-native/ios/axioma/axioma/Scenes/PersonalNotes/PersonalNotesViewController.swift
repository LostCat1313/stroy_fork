//
//  PersonalNotesViewController.swift
//  axioma
//
//  Created by sss on 08.10.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit

protocol PersonalNotesViewDelegate {
    func personalNote(didPick note: PersonalNote)
    func personalNote(delete note: PersonalNote)
    func personalNote(menuOpened cell: PersonalNoteViewCell)
    func personalNote(panBegan cell: PersonalNoteViewCell)
}


class PersonalNotesViewController: UIViewController {
    var completion: (()->())?
    var notes: [PersonalNote] = []
    var openedCells: [PersonalNoteViewCell] = []
    
    @IBOutlet weak var cvNotes: UICollectionView!
    @IBOutlet weak var spinnerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        cvNotes.register(UINib(nibName: "PersonalNoteViewCell", bundle: nil), forCellWithReuseIdentifier: "PersonalNoteViewCell")
        refresh()
    }
    
    private func refresh() {
        showSpinner()
        PersonalNotesAPI.get { notes, error in
            //DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 5, execute: {
            if let v = notes {
                self.notes = v
                self.cvNotes.reloadData()
            } else {
                self.displayError(error!)
            }
            self.hideSpinner()
            // })
        }
    
    }
    
    static public func show(_ over: UIViewController, completion: (()->())? = nil) {
        let controller = PersonalNotesViewController()
        controller.completion = completion
        over.present(controller, animated: true)
    }
    
    func displayError(_ error: Error) {
        let description = error.localizedDescription
        let dlg = Session.makeErrorDialog("Error", message: description)
        self.present(dlg, animated: true)
    }
    
    @IBAction func onBtnClose(_ sender: UIButton) {
        self.dismiss(animated: true, completion: completion)
    }
    
    @IBAction func onBtnNewNote(_ sender: UIButton) {
        PersonalNoteViewController.show(self, note: PersonalNote(text: "")) { res in
            if res == .changed {
                self.refresh()
            }
        }
    }
}

extension PersonalNotesViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return notes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: PersonalNoteViewCell = cvNotes.dequeueReusableCell(withReuseIdentifier: "PersonalNoteViewCell", for: indexPath) as! PersonalNoteViewCell
        cell.delegate = self
        cell.configure(notes[indexPath.item], bkColor: indexPath.item % 2 == 0 ? UIColor.white : UIColor(hex: "#ffffe1")!)
        return cell
    }
}

extension PersonalNotesViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: cvNotes.frame.width, height: 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
}

//SPINNER
extension PersonalNotesViewController {
    func showSpinner() {
        spinnerView.isHidden = false
    }
    
    func hideSpinner() {
        spinnerView.isHidden = true
    }
}

extension PersonalNotesViewController : PersonalNotesViewDelegate {
    func personalNote(didPick note: PersonalNote) {
        PersonalNoteViewController.show(self, note: note) { res in
            if res == .changed {
                self.refresh()
            }
        }
    }
    
    func personalNote(delete note: PersonalNote) {
        showSpinner()
        PersonalNotesAPI.delete(note: note) { err in
            if let e = err {
                self.hideSpinner()
                self.displayError(e)
            } else {
                if let idx = self.notes.firstIndex(where: {$0.id == note.id}) {
                    self.notes.remove(at: idx)
                    self.cvNotes.reloadData()
                }
                self.hideSpinner()
            }
        }
        
    }
    
    func personalNote(menuOpened cell: PersonalNoteViewCell) {
        self.openedCells.append(cell)
    }
    
    func personalNote(panBegan cell: PersonalNoteViewCell) {
        self.openedCells.forEach {
            if $0 !== cell {
                $0.closeMenu()
            }
        }
        self.openedCells = []

    }
}
