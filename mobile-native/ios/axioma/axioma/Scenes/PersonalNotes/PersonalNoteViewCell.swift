//
//  PersonalNoteViewCell.swift
//  axioma
//
//  Created by sss on 08.10.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit

class PersonalNoteViewCell: UICollectionViewCell {
    var delegate: PersonalNotesViewDelegate?
    var note: PersonalNote!

    @IBOutlet weak var btnLabel: UIButton!
    @IBOutlet weak var buttonView: UIView!
    @IBOutlet weak var menuView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let panGr = UIPanGestureRecognizer(target: self, action: #selector(onPan))
        buttonView.addGestureRecognizer(panGr)
        buttonView.translatesAutoresizingMaskIntoConstraints = true
    }
    
    func configure(_ note: PersonalNote, bkColor: UIColor) {
        self.note = note
        btnLabel.setTitle(note.text, for: .normal)
        buttonView.frame = CGRect(origin: CGPoint.zero, size: self.frame.size)
        buttonView.backgroundColor = bkColor
        //menuView.backgroundColor = bkColor

    }

    @IBAction func onBtnLabel(_ sender: UIButton) {
        delegate?.personalNote(didPick: note)
    }
    
    @IBAction func onBtnDelete(_ sender: UIButton) {
        delegate?.personalNote(delete: note)
        
    }
    
    @objc func onPan(_ pan: UIPanGestureRecognizer) {
        switch pan.state {
            case .began, .changed:
                delegate?.personalNote(panBegan: self)
                let dt = pan.translation(in: buttonView)
                let ofsX = buttonView.frame.origin.x + dt.x
                buttonView.frame.origin = CGPoint(x: max(-menuView.frame.width, min(0, ofsX)), y: buttonView.frame.origin.y)
                pan.setTranslation(CGPoint.zero, in: buttonView)
            case .cancelled, .ended, .failed:
                let ofsX = buttonView.frame.origin.x
                var newOfsX: CGFloat = 0
                if fabs(ofsX) > menuView.frame.width / 2 {
                    newOfsX = -menuView.frame.width
                    delegate?.personalNote(menuOpened: self)
                }
                UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
                    self.buttonView.frame.origin = CGPoint(x: newOfsX, y: self.buttonView.frame.origin.y)
                })
            default: break
        }
    }
    
    func closeMenu() {
        if buttonView.frame.origin.x < 0 {
            UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
                self.buttonView.frame.origin = CGPoint(x: 0, y: self.buttonView.frame.origin.y)
            })
        }
    }
}
