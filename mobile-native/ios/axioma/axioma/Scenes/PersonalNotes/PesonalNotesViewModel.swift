//
//  PesonalNotesViewModel.swift
//  axioma
//
//  Created by sss on 08.10.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation
import SwiftyJSON

class PersonalNote {
    let id: Int
    let text: String
    init (id: Int = 0, text: String) {
        self.id = id
        self.text = text
    }
    init (_ json: JSON) {
        self.id = json["id"].intValue
        self.text = json["content"].stringValue
    }
}
