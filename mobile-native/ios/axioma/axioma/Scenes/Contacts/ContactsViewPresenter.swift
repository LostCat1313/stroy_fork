//
//  ContactsViewPresenter.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 11.09.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation

protocol ContactsViewPresenterInput {
    func presentError(_ error: Error)
    func fetchContacts(response: ContactsViewModel.ServerContacts.Response)
    func fetchContacts(response: ContactsViewModel.PhoneContacts.Response)
}

protocol ContactsViewPresenterOutput: class {
    func displayError(_ error: Error)
    func fetchContacts(viewModel: ContactsViewModel.ServerContacts.ViewModel)
    func fetchContacts(viewModel: ContactsViewModel.PhoneContacts.ViewModel)
}

class ContactsViewPresenter: NSObject, ContactsViewPresenterInput {
    weak var output: ContactsViewPresenterOutput!
    
    func presentError(_ error: Error) {
        self.output.displayError(error)
    }

    
    func fetchContacts(response: ContactsViewModel.ServerContacts.Response) {
        var contacts: [SearchPhoneContact] = []
        for contact in response.phoneContacts {
            if let srvContact = response.contacts.first(where: { cleanPhoneNumber(contact.phoneNumber) == cleanPhoneNumber($0.phoneNumber) }) {
                contact.id = srvContact.id
                contact.position = srvContact.position
                contact.organization = srvContact.organization
                contact.contactType = srvContact.contactType
            }
            contacts.append(contact)
        }
        let viewModel = ContactsViewModel.ServerContacts.ViewModel(contacts: contacts)
        output.fetchContacts(viewModel: viewModel)
    }
    
    func fetchContacts(response: ContactsViewModel.PhoneContacts.Response) {
        let contacts = response.contacts.sorted(by: { $0.name < $1.name })
        let viewModel = ContactsViewModel.PhoneContacts.ViewModel(contacts: contacts)
        output.fetchContacts(viewModel: viewModel)
    }
}
