//
//  ContactsViewInteractor.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 11.09.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation
import SwiftyJSON
import ContactsUI

protocol ContactsViewInteractorInput {
    func fetchContacts(request: ContactsViewModel.ServerContacts.Request)
    func fetchContacts(request: ContactsViewModel.PhoneContacts.Request)
}

protocol ContactsViewInteractorOutput {
    func presentError(_ error: Error)
    func fetchContacts(response: ContactsViewModel.ServerContacts.Response)
    func fetchContacts(response: ContactsViewModel.PhoneContacts.Response)
}

class ContactsViewInteractor: ContactsViewInteractorInput {
    var output: ContactsViewInteractorOutput!
    //var worker = ContactsViewWorker()
    
    func fetchContacts(request: ContactsViewModel.ServerContacts.Request) {
        jsonRequest(RestAPI.contacts) { [weak self] json, error in
            guard self != nil else {
                return
            }
            
            if error == nil {
                if let items = json?.dictionaryObject?["list"] as? [[String: Any]] {
                    var contacts: [SearchPhoneContact] = []
                    for item in items {
                        contacts.append(SearchPhoneContact(data: item))
                    }
                    let response = ContactsViewModel.ServerContacts.Response(phoneContacts: request.phoneContacts, contacts: contacts)
                    self?.output.fetchContacts(response: response)
                }
            } else {
                self?.output.presentError(error!)
            }
            
        }
    }
    
    func fetchContacts(request: ContactsViewModel.PhoneContacts.Request) {
        let contactStore = CNContactStore()
        var contacts: [SearchPhoneContact] = []
        
        let keys = [
            CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
            CNContactPhoneNumbersKey,
            CNContactEmailAddressesKey,
            CNContactImageDataKey,
            CNContactThumbnailImageDataKey,
            CNContactImageDataAvailableKey
            ] as [Any]
        let request = CNContactFetchRequest(keysToFetch: keys as! [CNKeyDescriptor])
        
        do {
            try contactStore.enumerateContacts(with: request) {
                (contact, stop) in
                // Array containing all unified contacts from everywhere
                
                for phoneNumber in contact.phoneNumbers {
                    if let label = phoneNumber.label {
                        let localizedLabel = CNLabeledValue<CNPhoneNumber>.localizedString(forLabel: label)
                        
                        print("[parse contact] \(contact.givenName) \(contact.familyName) tel:\(localizedLabel) -- \(phoneNumber.value.stringValue), email: \(contact.emailAddresses)")
                        
                        if contact.givenName.count > 0 {
                            let phoneContact = SearchPhoneContact(name: contact.givenName, lastName: contact.familyName, phoneNumber: phoneNumber.value.stringValue)
                            if contact.imageDataAvailable {
                                phoneContact.photo = UIImage(data: contact.imageData!)
                            }
                            contacts.append(phoneContact)
                        }
                    }
                }
            }
            
            let response = ContactsViewModel.PhoneContacts.Response(contacts: contacts)
            self.output.fetchContacts(response: response)
        } catch {
            output.presentError(error)
        }
    }
}
