//
//  ContactsViewModel.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 11.09.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation
import UIKit

enum ContactType {
    case server
    case phone
}

class Organization {
    var id: Int
    var name: String
    
    init(data: [String: Any]) {
        id = data["id"] as! Int
        name = data["name"] as! String
    }
}

class OrganizationPosition {
    var id: Int
    var name: String
    
    init(data: [String: Any]) {
        id = data["id"] as! Int
        name = data["name"] as! String
    }
}

class PhoneContact {
    var id: Int?
    var name: String
    
    var firstName: String
    var secondName: String
    var lastName: String
    var phoneNumber: String
    var position: OrganizationPosition?
    var organization: Organization?
    var contactType: ContactType
    var email: String?
    var adress: String?
    
    var photo: UIImage?
    var photoURI: URL?
    
    init(data: [String: Any]) {
        id = data["id"] as? Int
        name = data["name"] as! String
        firstName = data["firstName"] as! String
        secondName = (data["secondName"] as? String) ?? ""
        lastName = data["lastName"] as! String
        phoneNumber = data["phoneNumber"] as! String
        
        if let data = data["position"] as? [String: Any] {
            position = OrganizationPosition(data: data)
        }
        
        if let data = data["organization"] as? [String: Any] {
            organization = Organization(data: data)
        }
        contactType = .server
    }
    
    init(name: String, lastName: String, phoneNumber: String, contactType: ContactType = .phone) {
        self.name = name
        firstName = ""
        secondName = ""
        self.lastName = lastName
        self.phoneNumber = phoneNumber
        self.contactType = contactType
    }
    
    var iconName: String? {
        var result: String?
        
        let words = name.components(separatedBy: " ")
        
        if words.count == 1 {
            if lastName.count > 0 {
                result = words[0].prefix(1).uppercased() + lastName.prefix(1).uppercased()
            } else {
                result = words[0].prefix(min(words[0].count, 2)).uppercased()
            }
        } else if words.count > 1 {
            result = words[0].prefix(1).uppercased() + words[1].prefix(1).uppercased()
        }
        
        return result
    }
}

enum SerchResultType {
    case fio
    case phone
    case organization
    case position
}

class SearchPhoneContact: PhoneContact {
    var searchResultType: SerchResultType = .fio
}

struct ContactsViewModel {
    struct ServerContacts {
        struct Request {
            var phoneContacts: [SearchPhoneContact]
        }
        
        struct Response {
            var phoneContacts: [SearchPhoneContact]
            var contacts: [SearchPhoneContact]
        }
        
        struct ViewModel {
            var contacts: [SearchPhoneContact]
        }
    }
    
    struct PhoneContacts {
        struct Request {
            
        }
        
        struct Response {
            var contacts: [SearchPhoneContact]
        }
        
        struct ViewModel {
            var contacts: [SearchPhoneContact]
        }
    }
}
