//
//  ContactsViewRouter.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 11.09.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation
import UIKit

protocol ContactsViewRouterInput {
    
}

class ContactsViewRouter: ContactsViewRouterInput {
    weak var viewController: ContactsViewController!
    
    // MARK: Navigation
    func goContact(contact: PhoneContact) {
        let controller = CallPageController(contact: contact)
        self.viewController.present(controller, animated: true)
    }
    
    // MARK: Communication
    
    func passDataToNextScene(segue: UIStoryboardSegue) {
        // NOTE: Teach the router which scenes it can communicate with
        
        //if segue.identifier == "ShowSomewhereScene" {
        //    passDataToScene(segue)
        //}
    }
    
    func passDataToScene(segue: UIStoryboardSegue) {
        // NOTE: Teach the router how to pass data to the next scene
        
        // let someWhereViewController = segue.destinationViewController as! SomeWhereViewController
        // someWhereViewController.output.name = viewController.output.name
    }
}
