//
//  ContactsViewWorker.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 11.09.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation

func cleanPhoneNumber(_ phone: String) -> String {
    
    return phone
        .replacingOccurrences(of: "+7", with: "8")
        .components(separatedBy: CharacterSet.decimalDigits.inverted).joined(separator: "")
}
