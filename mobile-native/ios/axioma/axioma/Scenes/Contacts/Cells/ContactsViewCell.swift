//
//  ContactsViewCell.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 12.09.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit

protocol ContactsViewCellDelegate {
    func contactsViewCell(onTap cell: ContactsViewCell)
}

class ContactsViewCell: UICollectionViewCell {

    @IBOutlet weak var userIconLabel: DesignableLabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var noteLabel: UILabel!
    @IBOutlet weak var appIcon: UIImageView!
    
    var contact: SearchPhoneContact!
    var delegate: ContactsViewCellDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let grTap = UITapGestureRecognizer(target: self, action: #selector(self.onTap(_:)))
        self.addGestureRecognizer(grTap)
    }

    func configure(contact: SearchPhoneContact) {
        self.contact = contact
        userIconLabel.text = contact.iconName
        nameLabel.text = contact.name + " " + contact.lastName
        
        switch contact.searchResultType {
            case .phone:
                noteLabel.text = contact.phoneNumber
            case .position:
                noteLabel.text = contact.position?.name
            case .organization:
                noteLabel.text = contact.organization?.name
            default:
                noteLabel.text = contact.position != nil ?  "\(contact.position!.name), \(contact.phoneNumber)" : contact.phoneNumber
        }
        
        
        appIcon.isHidden = contact.contactType == .phone
    }
    
    @objc func onTap(_ sender: UITapGestureRecognizer) {
        delegate.contactsViewCell(onTap: self)
    }
}
