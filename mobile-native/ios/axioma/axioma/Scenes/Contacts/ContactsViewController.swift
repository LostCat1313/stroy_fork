//
//  ContactsViewController.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 11.09.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit

protocol ContactsViewControllerInput {
    func displayError(_ error: Error)
    func fetchContacts(viewModel: ContactsViewModel.ServerContacts.ViewModel)
    func fetchContacts(viewModel: ContactsViewModel.PhoneContacts.ViewModel)
}

protocol ContactsViewControllerOutput {
    func fetchContacts(request: ContactsViewModel.ServerContacts.Request)
    func fetchContacts(request: ContactsViewModel.PhoneContacts.Request)
}

class ContactsViewController: UIViewController {

    enum State {
        case none
        case fullList(contacts: [SearchPhoneContact])
        case searchList(contacts: [SearchPhoneContact], searchText: String)
        
        var contacts: [SearchPhoneContact] {
            switch self {
                case .fullList(let contactList), .searchList(let contactList, _):
                    return contactList
                default:
                    return []
            }
        }
    }
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var contacts: [SearchPhoneContact] = []
    let iconColors: [UIColor] = [UIColor(red: 199/255, green: 213/255, blue: 55/255, alpha: 1),
                                 UIColor(red: 106/255, green: 69/255, blue: 156/255, alpha: 1),
                                 UIColor(red: 163/255, green: 69/255, blue: 159/255, alpha: 1),
                                 UIColor(red: 160/255, green: 136/255, blue: 126/255, alpha: 1),
                                 UIColor(red: 174/255, green: 213/255, blue: 130/255, alpha: 1),
                                 UIColor(red: 228/255, green: 115/255, blue: 115/255, alpha: 1),
                                 UIColor(red: 80/255, green: 193/255, blue: 248/255, alpha: 1),
                                 UIColor(red: 77/255, green: 208/255, blue: 226/255, alpha: 1),
                                 UIColor(red: 150/255, green: 116/255, blue: 208/255, alpha: 1),
                                 UIColor(red: 51/255, green: 0/255, blue: 255/255, alpha: 1),
                                 ]
    
    private var state: State = .none {
        didSet {
            switch state {
                case .fullList, .none:
                    searchBar.text = nil
                    searchBar.endEditing(true)
                    searchBar.isHidden = true
                    titleLabel.isHidden = false
                    searchButton.isHidden = false
                case .searchList:
                    searchBar.isHidden = false
                    titleLabel.isHidden = true
                    searchButton.isHidden = true
            }
        }
    }
    
    // VIP
    var output: ContactsViewControllerOutput!
    var router: ContactsViewRouter!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        searchBar.delegate = self
        
        // SearchBar
        let textColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1)
        if let textField = searchBar.value(forKey: "searchField") as? UITextField {
            textField.font = UIFont(name: "AvenirNext", size: 14.0)
            textField.textColor = textColor
            
            if let searchBarLabel = textField.value(forKey: "placeholderLabel") as? UILabel {
                searchBarLabel.font = UIFont(name: "AvenirNext", size: 14.0)
                searchBarLabel.textColor = textColor
            }
            
            if let glassIconView = textField.leftView as? UIImageView {
                glassIconView.image = UIImage(named: "search")
            }
        }
        
        (searchBar.value(forKey: "cancelButton") as! UIButton).setTitle("Отмена", for: .normal)
        (searchBar.value(forKey: "cancelButton") as! UIButton).setTitleColor(UIColor.white, for: .normal)
        
        // Do any additional setup after loading the view.
        collectionView.register(UINib(nibName: "ContactsViewCell", bundle: nil), forCellWithReuseIdentifier: "ContactsViewCell")
        collectionView.delegate = self
        collectionView.dataSource = self
    }

    override func viewDidAppear(_ animated: Bool) {
        switch state {
            case .searchList:
                break
            default:
                let request = ContactsViewModel.PhoneContacts.Request()
                output.fetchContacts(request: request)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    init() {
        super.init(nibName: "ContactsViewController", bundle: Bundle.main)
        ContactsViewConfigurator.sharedInstance.configure(viewController: self)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    @IBAction func onSearch(_ sender: Any) {
        switch state {
            case .fullList(let contacts):
                state = .searchList(contacts: contacts, searchText: "")
                searchBar.becomeFirstResponder()
            default:
                break
        }
    }
    
    @IBAction func onReturn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension ContactsViewController: ContactsViewControllerInput {
    func displayError(_ error: Error) {
        let description = error.localizedDescription
        let dlg = Session.makeErrorDialog("Error", message: description)
        self.present(dlg, animated: true)
    }
    
    func fetchContacts(viewModel: ContactsViewModel.ServerContacts.ViewModel) {
        contacts = viewModel.contacts
        state = .fullList(contacts: contacts)
        collectionView.reloadData()
    }
    
    func fetchContacts(viewModel: ContactsViewModel.PhoneContacts.ViewModel) {
        //contacts = viewModel.contacts
        //collectionView.reloadData()
        
        let request = ContactsViewModel.ServerContacts.Request(phoneContacts: viewModel.contacts)
        output.fetchContacts(request: request)
    }
}

extension ContactsViewController: UICollectionViewDelegate {
    
}

extension ContactsViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: 68)
    }
}

extension ContactsViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return state.contacts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let contact = state.contacts[indexPath.item]
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ContactsViewCell", for: indexPath) as! ContactsViewCell
    
        cell.configure(contact: contact)
        cell.userIconLabel.backgroundColor = iconColors[abs(contact.name.hashValue) % 10]
        cell.delegate = self
        return cell
    }
}

extension ContactsViewController: ContactsViewCellDelegate {
    func contactsViewCell(onTap cell: ContactsViewCell) {
        router.goContact(contact: cell.contact)
    }
}

extension ContactsViewController: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        var result: [SearchPhoneContact] = []
        for contact in contacts {
            contact.searchResultType = .fio
            
            if contact.name.lowercased().contains(searchText.lowercased()) {
                result.append(contact)
                continue
            }
            
            if contact.firstName.lowercased().contains(searchText.lowercased()) {
                result.append(contact)
                continue
            }
            
            if contact.secondName.lowercased().contains(searchText.lowercased()) {
                result.append(contact)
                continue
            }
            
            if contact.lastName.lowercased().contains(searchText.lowercased()) {
                result.append(contact)
                continue
            }
            
            if contact.phoneNumber.lowercased().contains(searchText.lowercased()) {
                contact.searchResultType = .phone
                result.append(contact)
                continue
            }
            
            if let positionName = contact.position?.name, positionName.lowercased().contains(searchText.lowercased()) {
                contact.searchResultType = .position
                result.append(contact)
                continue
            }
            
            if let organizationName = contact.organization?.name, organizationName.lowercased().contains(searchText.lowercased()) {
                contact.searchResultType = .organization
                result.append(contact)
                continue
            }
        }
        
        state = .searchList(contacts: result, searchText: searchText)
        collectionView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        for contact in self.contacts {
            contact.searchResultType = .fio
        }
        state = .fullList(contacts: self.contacts)
        collectionView.reloadData()
    }
}
