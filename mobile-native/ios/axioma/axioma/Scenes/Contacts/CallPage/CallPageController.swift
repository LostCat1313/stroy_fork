//
//  CallPageController.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 14.09.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit

protocol CallPageControllerInput {
    func displayError(_ error: Error)
    func selectRoom(viewModel: CallPageModel.SelectRoom.ViewModel)
    func createRoom(viewModel: CallPageModel.CreateRoom.ViewModel)
}

protocol CallPageControllerOutput {
    func selectRoom(request: CallPageModel.SelectRoom.Request)
    func createRoom(request: CallPageModel.CreateRoom.Request)
}

class CallPageController: UIViewController {
    
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var phoneNumber: UILabel!
    @IBOutlet weak var positionLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var adressLabel: UILabel!
    
    var contact: PhoneContact!
    
    // VIP
    var output: CallPageControllerOutput!
    var router: CallPageRouter!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    init(contact: PhoneContact) {
        super.init(nibName: "CallPageController", bundle: Bundle.main)
        self.contact = contact
        CallPageConfigurator.sharedInstance.configure(viewController: self)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.userName.text = contact.name + " " + contact.lastName
        self.phoneNumber.text = contact.phoneNumber
        
        positionLabel.text = contact.position?.name
        emailLabel.text = contact.email
        adressLabel.text = contact.adress
        
        switch contact.contactType {
            case .phone:
                actionButton.backgroundColor = UIColor(red: 118/255, green: 214/255, blue: 1.0, alpha: 1.0)
                actionButton.setImage(UIImage(named: "account-plus"), for: .normal)
            case .server:
                actionButton.backgroundColor = UIColor(red: 0, green: 165/255, blue: 0, alpha: 1.0)
                actionButton.setImage(UIImage(named: "message-white"), for: .normal)
        }
    }
    
    @IBAction func onCall(_ sender: Any) {
        let urlString:String = "tel://\(cleanPhoneNumber(contact.phoneNumber))"
        
        if let phoneCallURL = URL(string: urlString) {
            if (UIApplication.shared.canOpenURL(phoneCallURL)) {
                UIApplication.shared.open(phoneCallURL, options: [:], completionHandler: nil)
            }
        }
    }
    
    @IBAction func onEmail(_ sender: Any) {
    }
    
    @IBAction func onLocation(_ sender: Any) {
    }
    
    @IBAction func onAction(_ sender: Any) {
        switch contact.contactType {
            case .server:
                let request = CallPageModel.SelectRoom.Request(contact: contact)
                output.selectRoom(request: request)
            case .phone:
                break
        }
    }
    
    @IBAction func onBack(_ sender: Any) {
        if let manager = Session.socketManager() {
            let socket = manager.defaultSocket
            
            if socket.status == .connected {
                socket.disconnect()
            }
        }
        
        dismiss(animated: true, completion: nil)
    }
    
}

extension CallPageController: CallPageControllerInput {
    func displayError(_ error: Error) {
        let description = error.localizedDescription
        let dlg = Session.makeErrorDialog("Error", message: description)
        self.present(dlg, animated: true)
    }
    
    func selectRoom(viewModel: CallPageModel.SelectRoom.ViewModel) {
        if let room = viewModel.room {
            router.goToChat(room: room)
        } else {
            router.newChatRoom(roomName: userName.text!)
        }
    }
    
    func createRoom(viewModel: CallPageModel.CreateRoom.ViewModel) {
        if let manager = Session.socketManager() {
            let socket = manager.defaultSocket
            
            if socket.status == .connected {
                socket.disconnect()
            }
        }
        
        router.goToChat(room: viewModel.room)
    }
}

extension CallPageController: NewRoomDialogControllerDelegate {
    func newRoomDialogController(onAccept text: String) {
        if !text.isEmpty {
            let request = CallPageModel.CreateRoom.Request(userId: contact.id!, roomName: text)
            output.createRoom(request: request)
        }
    }
    
    func newRoomDialogController(onCancel sender: NewRoomDialogController?) {
        
    }
}

