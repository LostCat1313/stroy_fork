//
//  CallPageConfigurator.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 18.09.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation
import UIKit

// MARK: Connect View, Interactor, and Presenter

extension CallPageController: CallPagePresenterOutput {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        router.passDataToNextScene(segue: segue)
    }
}

extension CallPageInteractor: CallPageControllerOutput {
}

extension CallPagePresenter: CallPageInteractorOutput {
}

class CallPageConfigurator {
    // MARK: Object lifecycle
    static let sharedInstance = CallPageConfigurator()
    
    // MARK: Configuration
    func configure(viewController: CallPageController) {
        let router = CallPageRouter()
        router.viewController = viewController
        
        let presenter = CallPagePresenter()
        presenter.output = viewController
        
        let interactor = CallPageInteractor()
        interactor.output = presenter
        //interactor.worker = CallPageWorker()
        
        viewController.output = interactor
        viewController.router = router
    }
}
