//
//  CallPageRouter.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 18.09.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation
import UIKit

protocol CallPageRouterInput {
    
}

class CallPageRouter: CallPageRouterInput {
    weak var viewController: CallPageController!
    
    // MARK: Navigation
    func goToChat(room: ChatRoom) {
        let controller = ChatViewController(room: room)
        self.viewController.present(controller, animated: true)
    }
    
    func newChatRoom(roomName: String) {
        let controller = NewRoomDialogController()
        controller.modalPresentationStyle = .overCurrentContext
        controller.delegate = self.viewController
        
        self.viewController.present(controller, animated: false) {
            controller.roomName.text = roomName
        }
    }
    
    // MARK: Communication
    
    func passDataToNextScene(segue: UIStoryboardSegue) {
        // NOTE: Teach the router which scenes it can communicate with
        
        //if segue.identifier == "ShowSomewhereScene" {
        //    passDataToScene(segue)
        //}
    }
    
    func passDataToScene(segue: UIStoryboardSegue) {
        // NOTE: Teach the router how to pass data to the next scene
        
        // let someWhereViewController = segue.destinationViewController as! SomeWhereViewController
        // someWhereViewController.output.name = viewController.output.name
    }
}
