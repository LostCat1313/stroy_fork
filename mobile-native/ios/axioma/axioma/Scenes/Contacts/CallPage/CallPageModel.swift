//
//  CallPageModel.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 18.09.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation

struct CallPageModel {
    struct SelectRoom {
        struct Request {
            var contact: PhoneContact
        }
        
        struct Response {
            var contact: PhoneContact
            var rooms: [ChatRoom]
        }
        
        struct ViewModel {
            var contact: PhoneContact
            var room: ChatRoom?
        }
    }
    
    struct CreateRoom {
        struct Request {
            var userId: Int
            var roomName: String
        }
        
        struct Response {
            var room: ChatRoom
        }
        
        struct ViewModel {
            var room: ChatRoom
        }
    }
}
