//
//  CallPagePresenter.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 18.09.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation

protocol CallPagePresenterInput {
    func presentError(_ error: Error)
    func selectRoom(response: CallPageModel.SelectRoom.Response)
    func createRoom(response: CallPageModel.CreateRoom.Response)
}

protocol CallPagePresenterOutput: class {
    func displayError(_ error: Error)
    func selectRoom(viewModel: CallPageModel.SelectRoom.ViewModel)
    func createRoom(viewModel: CallPageModel.CreateRoom.ViewModel)
}

class CallPagePresenter: NSObject, CallPagePresenterInput {
    weak var output: CallPagePresenterOutput!
    
    func presentError(_ error: Error) {
        self.output.displayError(error)
    }
    
    func selectRoom(response: CallPageModel.SelectRoom.Response) {
        // select chat by user id
        var selected: ChatRoom?
        for room in response.rooms {
            if let users = room.users, room.isPrivate, users.contains(where: { $0.id == response.contact.id }) {
                selected = room
                break
            }
        }
        
        let viewModel = CallPageModel.SelectRoom.ViewModel(contact: response.contact, room: selected)
        output?.selectRoom(viewModel: viewModel)
    }
    
    func createRoom(response: CallPageModel.CreateRoom.Response) {
        let viewModel = CallPageModel.CreateRoom.ViewModel(room: response.room)
        output.createRoom(viewModel: viewModel)
    }
}
