//
//  CallPageInteractor.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 18.09.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation
import SwiftyJSON


protocol CallPageInteractorInput {
    func selectRoom(request: CallPageModel.SelectRoom.Request)
    func createRoom(request: CallPageModel.CreateRoom.Request)
}

protocol CallPageInteractorOutput {
    func presentError(_ error: Error)
    func selectRoom(response: CallPageModel.SelectRoom.Response)
    func createRoom(response: CallPageModel.CreateRoom.Response)
}

class CallPageInteractor: CallPageInteractorInput {
    var output: CallPageInteractorOutput!
    //var worker = CallPageWorker()
    
    func selectRoom(request: CallPageModel.SelectRoom.Request) {
        if let manager = Session.socketManager() {
            let socket = manager.defaultSocket
            socket.disconnect()
            
            socket.on("init") { [weak self] data, ack in
                if data.count > 0 && self != nil {
                    if let roomsData = (data[0] as! [String: Any])["rooms"] as? [[String: Any]] {
                        var rooms: [ChatRoom] = []
                        
                        for roomData in roomsData {
                            let room = ChatRoom(data: roomData)
                            rooms.append(room)
                        }
                        
                        socket.off("init")
                        let response = CallPageModel.SelectRoom.Response(contact: request.contact, rooms: rooms)
                        self!.output.selectRoom(response: response)
                    }
                }
                socket.off("init")
            }
            
            socket.connect()
        }
    }
    
    func createRoom(request: CallPageModel.CreateRoom.Request) {
        if let manager = Session.socketManager() {
            let socket = manager.defaultSocket
            
            socket.disconnect()
            
            socket.on("init") { [weak self] data, ack in
                socket.off("init")
                
                var params: [String: Any] = [:]
                params["userIds"] = [request.userId]
                params["name"] = request.roomName
                
                socket.emitWithAck("join", params).timingOut(after: 3.0) { data in
                    var rooms: [ChatRoom] = []
                    for room in data as! [[String: Any]] {
                        rooms.append(ChatRoom(data: room["room"] as! [String: Any]))
                    }
                    
                    socket.disconnect()
                    let response = CallPageModel.CreateRoom.Response(room: rooms[0])
                    self?.output.createRoom(response: response)
                }
                
            }
            
            socket.connect()
        }
    }
}
