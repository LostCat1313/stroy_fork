//
//  ContactsViewConfigurator.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 11.09.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation
import UIKit

// MARK: Connect View, Interactor, and Presenter

extension ContactsViewController: ContactsViewPresenterOutput {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        router.passDataToNextScene(segue: segue)
    }
}

extension ContactsViewInteractor: ContactsViewControllerOutput {
}

extension ContactsViewPresenter: ContactsViewInteractorOutput {
}

class ContactsViewConfigurator {
    // MARK: Object lifecycle
    static let sharedInstance = ContactsViewConfigurator()
    
    // MARK: Configuration
    func configure(viewController: ContactsViewController) {
        let router = ContactsViewRouter()
        router.viewController = viewController
        
        let presenter = ContactsViewPresenter()
        presenter.output = viewController
        
        let interactor = ContactsViewInteractor()
        interactor.output = presenter
        //interactor.worker = ContactsViewWorker()
        
        viewController.output = interactor
        viewController.router = router
    }
}
