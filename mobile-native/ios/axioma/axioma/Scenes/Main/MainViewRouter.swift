//
//  MainViewRouter.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 08.06.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation
import UIKit

protocol MainViewRouterInput {
    
}

class MainViewRouter: MainViewRouterInput {
    weak var viewController: MainViewController!
    
    // MARK: Navigation
    func goToDocument(projectModel: ProjectModel.View, document: Document) {
        DocumentViewController.show(self.viewController, project: projectModel, doc: document)
    }
    
    func goToPage(projectModel: ProjectModel.View, document: Document, page: Page) {
        DocumentViewController.show(self.viewController, project: projectModel, doc: document, page: page)
    }
    
    func goToMarker(projectModel: ProjectModel.View, document: Document, page: Page, marker: PageMarker) {
        DocumentViewController.show(self.viewController, project: projectModel, doc: document, page: page, activeMarker: marker)
    }
    
    func navigateToLoginForm() {
        if let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController {
            self.viewController.present(controller, animated: false, completion: nil)
        }
    }
    
    func goToChat(room: ChatRoom, message: ChatMessage?) {
        let controller = ChatViewController(room: room, message: message)
        self.viewController.present(controller, animated: true)
    }
    
    // MARK: Communication
    
    func passDataToNextScene(segue: UIStoryboardSegue) {
        // NOTE: Teach the router which scenes it can communicate with
       
        if segue.identifier == "toLastDocuments" {
            let controller = segue.destination as! LastDocumentsViewController
            controller.delegate = self.viewController
        } else if segue.identifier == "toNotificationsView" {
            let controller = segue.destination as! NotificationsViewController
            controller.delegate = self.viewController
        }
    }

}
