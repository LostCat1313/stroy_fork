//
//  MainViewConfigurator.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 08.06.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation
import UIKit

// MARK: Connect View, Interactor, and Presenter

extension MainViewController: MainViewPresenterOutput {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        router.passDataToNextScene(segue: segue)
    }
}

extension MainViewInteractor: MainViewControllerOutput {
}

extension MainViewPresenter: MainViewInteractorOutput {
}

class MainViewConfigurator {
    // MARK: Object lifecycle
    static let sharedInstance = MainViewConfigurator()
    
    // MARK: Configuration
    func configure(viewController: MainViewController) {
        let router = MainViewRouter()
        router.viewController = viewController
        
        let presenter = MainViewPresenter()
        presenter.output = viewController
        
        let interactor = MainViewInteractor()
        interactor.output = presenter
        interactor.worker = MainViewWorker()
        
        viewController.output = interactor
        viewController.router = router
    }
}
