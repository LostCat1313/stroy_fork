//
//  MainViewController.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 02.06.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit
import SwiftyJSON

protocol MainViewControllerInput {
    func displayError(_ error: Error)
    func checkSession(viewModel: MainViewModel.CheckSession.ViewModel)
    func projectsList(viewModel: ProjectsViewModel.ProjectsList.ViewModel)
    func selectDocument(viewModel: MainViewModel.SelectDocument.ViewModel)
    func selectChat(viewModel: MainViewModel.FetchRoom.ViewModel)
    func selectPage(viewModel: MainViewModel.SelectPage.ViewModel)
    func selectMarker(viewModel: MainViewModel.SelectMarker.ViewModel)
}

protocol MainViewControllerOutput {
    func checkSession(request: MainViewModel.CheckSession.Request)
    func projectsList(request: ProjectsViewModel.ProjectsList.Request)
    func selectDocument(request: MainViewModel.SelectDocument.Request)
    func selectChat(request: MainViewModel.FetchRoom.Request)
    func selectPage(request: MainViewModel.SelectPage.Request)
    func selectMarker(request: MainViewModel.SelectMarker.Request)
}

protocol MainViewControllerDelegate {
    func mainViewController(onProject project: ProjectData)
}

class MainViewController: UIViewController {
    
    // VIP
    var output: MainViewControllerOutput!
    var router: MainViewRouter!
    var sessionChecked = false
    
    private var afterLoadProjects: (() -> ())?
    private var projects: [ProjectData]?
    var delegate: MainViewControllerDelegate!
    
    enum ControllerPage {
        case stream
        case documents
    }
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var streamContainerView: UIView!
    @IBOutlet weak var documentsContainerView: UIView!
    @IBOutlet weak var streamButton: UIButton!
    @IBOutlet weak var documentsButton: UIButton!
    @IBOutlet weak var filterButton: UIButton!
    @IBOutlet weak var topBar: UIView!
    
    let toolbarColor = UIColor(red: 99/255, green: 99/255, blue: 99/255, alpha: 1)
    
    var sideMenu: PGSideMenu? {
        return self.parent as? PGSideMenu
    }
    
    var notificationsViewController: NotificationsViewController? {
        for child in self.childViewControllers {
            if let controller = child as? NotificationsViewController {
                return controller
            }
        }
        return nil
    }
    
    var lastDocumentsViewController: LastDocumentsViewController? {
        for child in childViewControllers {
            if let controller = child as? LastDocumentsViewController {
                return controller
            }
        }
        return nil
    }
    
    @IBAction func onMenu(_ sender: Any) {
        sideMenu?.toggleLeftMenu()
    }
    
    @IBAction func onFilter(_ sender: Any) {
        guard let filters = self.notificationsViewController?.filters else {
            return
        }
        
        let checkedDocuments = filters.first {$0 == .documents} != nil
        let checkedMessages = filters.first {$0 == .messages} != nil
        let checkedNotes = filters.first {$0 == .notes} != nil
        let checkedProject = filters.first {$0 == .project} != nil
        
        let menuItems: [PopupMenuItemProtocol] = [
            CheckBoxMenuItem(title: "Документы", checked: checkedDocuments, changed: { item in
                if let filters = self.notificationsViewController?.filters {
                    var newFilters = filters.filter { $0 != .documents }
                    if item.checked {
                        newFilters.append(.documents)
                    }
                    self.notificationsViewController!.setFilters(filters: newFilters)
                }
            }),
            CheckBoxMenuItem(title: "Сообщения", checked: checkedMessages, changed: { item in
                if let filters = self.notificationsViewController?.filters {
                    var newFilters = filters.filter { $0 != .messages }
                    if item.checked {
                        newFilters.append(.messages)
                    }
                    self.notificationsViewController!.setFilters(filters: newFilters)
                }
            }),
            CheckBoxMenuItem(title: "Замечания", checked: checkedNotes, changed: { item in
                if let filters = self.notificationsViewController?.filters {
                    var newFilters = filters.filter { $0 != .notes }
                    if item.checked {
                        newFilters.append(.notes)
                    }
                    self.notificationsViewController!.setFilters(filters: newFilters)
                }
            }),
            CheckBoxMenuItem(title: "Проект", checked: checkedProject, changed: { item in
                if let filters = self.notificationsViewController?.filters {
                    var newFilters = filters.filter { $0 != .project }
                    if item.checked {
                        newFilters.append(.notes)
                    }
                    self.notificationsViewController!.setFilters(filters: newFilters)
                }
            })
        ];
        let at = CGPoint(x: filterButton.frame.origin.x, y: topBar.frame.height)
        let at2 = view.convert(at, from: topBar)
        PopupMenuViewController.popup(over: self, at: at2, menuItems: menuItems)
    }
    
    var currentPage: ControllerPage = .stream {
        didSet {
            updateButtons()
        }
    }
    
    func updateButtons() {
        switch currentPage {
            case .stream:
                filterButton.isHidden = false
                streamButton.backgroundColor = UIColor.white
                documentsButton.backgroundColor = UIColor.clear
                streamButton.titleLabel?.textColor = toolbarColor
                documentsButton.titleLabel?.textColor = UIColor.white
            case .documents:
                filterButton.isHidden = true
                streamButton.backgroundColor = UIColor.clear
                streamButton.titleLabel?.textColor = UIColor.white
                documentsButton.backgroundColor = UIColor.white
                documentsButton.titleLabel?.textColor = toolbarColor
        }
    }
    
    @IBAction func onStreamPage(_ sender: Any) {
        scrollView?.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        currentPage = .stream
    }
    
    @IBAction func onDocumentsPage(_ sender: Any) {
        scrollView?.setContentOffset(CGPoint(x: streamContainerView.frame.size.width, y: 0), animated: true)
        self.currentPage = .documents
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        MainViewConfigurator.sharedInstance.configure(viewController: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        scrollView.delegate = self
        
        NotificationCenter.default.addObserver(self, selector:#selector(self.handleBackgroundPushNotification(_:)), name: .pushNotificationReceivedOnBackground, object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(self.handlePushNotification(_:)), name: .pushNotificationReceivedOnForeground, object: nil)
    }

    override func viewDidAppear(_ animated: Bool) {
        // check user login
        updateButtons()
        
        if !Session.isAuthorized() {
            self.router.navigateToLoginForm()
        } else if !self.sessionChecked {
            let request = MainViewModel.CheckSession.Request()
            self.output.checkSession(request: request)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func handleBackgroundPushNotification(_ notification: Foundation.Notification) {
        if let eventString = notification.userInfo?["event"] as? String {
            let json = JSON.parse(eventString)
            let notification = Notification(json: json)
            
            if self.projects == nil {
                self.afterLoadProjects = {
                    if let projects = self.projects, let project = projects.first(where: { $0.id == notification.projectId }) {
                        self.notificationsViewController(onSelect: notification, project: project)
                    } else {
                        self.notificationsViewController(onSelect: notification, project: nil)
                    }
                }
            } else if let project = self.projects!.first(where: { $0.id == notification.projectId }) {
                self.notificationsViewController(onSelect: notification, project: project)
            } else {
                self.notificationsViewController(onSelect: notification, project: nil)
            }
        }
    }
    
    @objc func handlePushNotification(_ notification: Foundation.Notification) {
        if let eventString = notification.userInfo?["event"] as? String {
            let json = JSON.parse(eventString)
            let notification = Notification(json: json)
            
            switch notification.type {
                case .activationPushCode:
                    break
                default:
                    NotificationMessage.show(notification: notification) {
                        if self.projects == nil {
                            self.afterLoadProjects = {
                                if let projects = self.projects, let project = projects.first(where: { $0.id == notification.projectId }) {
                                    self.notificationsViewController(onSelect: notification, project: project)
                                } else {
                                    self.notificationsViewController(onSelect: notification, project: nil)
                                }
                            }
                        } else if let project = self.projects!.first(where: { $0.id == notification.projectId }) {
                            self.notificationsViewController(onSelect: notification, project: project)
                        } else {
                            self.notificationsViewController(onSelect: notification, project: nil)
                        }
                    }
                    
                    // update stream
                    if let _ = self.projects?.first(where: { $0.id == notification.projectId }) {
                        notificationsViewController?.initView()
                    } else if projects != nil {
                        let request = ProjectsViewModel.ProjectsList.Request()
                        output.projectsList(request: request)
                    }
            }
        }
    }
    
    func selectProject(_ project: ProjectData, completion: (() -> ())? = nil) {
        DispatchQueue.main.async {
            if Session.currentProject?.id != project.id {
                Session.currentProject = project
                let dlg = Session.makeMessageDialog("Проекты", message: "Выбран текущий проект:\(project.title)") {
                    self.updateButtons()
                    completion?()
                }
                self.present(dlg, animated: true)
            } else {
                completion?()
            }
        }
    }
    
}

extension MainViewController: UIScrollViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.x == 0 {
            currentPage = .stream
        } else {
            currentPage = .documents
        }
    }
    
}

extension MainViewController: MainViewControllerInput {
    
    func displayError(_ error: Error) {
        var description = error.localizedDescription
        
        if let err = error as? MainViewError {
            switch err {
                case .serverError(let msg):
                    description = msg
            }
        }
        
        print("[Error] Error:\(description)")
        
        let dlg = Session.makeErrorDialog("Error", message: description) {
            // onClose
        }
        
        self.present(dlg, animated: true)
    }
    
    func checkSession(viewModel: MainViewModel.CheckSession.ViewModel) {
        if !viewModel.valid {
            self.router.navigateToLoginForm()
        } else {
            sessionChecked = true
            
            if Session.projects == nil {
                let request = ProjectsViewModel.ProjectsList.Request()
                output.projectsList(request: request)
            } else {
                notificationsViewController?.initView()
                lastDocumentsViewController?.initView()
            }
        }
    }
    
    func projectsList(viewModel: ProjectsViewModel.ProjectsList.ViewModel) {
        Session.projects = viewModel.projects
        self.projects = viewModel.projects
        
        notificationsViewController?.initView()
        lastDocumentsViewController?.initView()
        
        afterLoadProjects?()
    }
    
    func selectDocument(viewModel: MainViewModel.SelectDocument.ViewModel) {
        let projectModel = ProjectModel.View(project: viewModel.project, documents: viewModel.documents)
        if let document = projectModel.findDocument(docId: viewModel.documentId) {
            router.goToDocument(projectModel: projectModel, document: document)
        } else {
            let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Документ '\(viewModel.title!)' не найден в проекте."])
            displayError(error)
        }
    }
    
    func selectChat(viewModel: MainViewModel.FetchRoom.ViewModel) {
        router.goToChat(room: viewModel.room, message: viewModel.notification.message)
    }
    
    func selectPage(viewModel: MainViewModel.SelectPage.ViewModel) {
        let projectModel = ProjectModel.View(project: viewModel.project, documents: viewModel.documents)
        if let document = projectModel.findDocument(docId: viewModel.documentId), let page = document.findPage(pageId: viewModel.pageId) {
            router.goToPage(projectModel: projectModel, document: document, page: page)
        } else {
            let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Страница документа '\(viewModel.title!)' не найдена в проекте"])
            displayError(error)
        }
    }
    
    func selectMarker(viewModel: MainViewModel.SelectMarker.ViewModel) {
        let projectModel = ProjectModel.View(project: viewModel.project, documents: viewModel.documents)
        if let document = projectModel.findDocument(docId: viewModel.marker.ownerDocId), let page = document.findPage(pageId: viewModel.marker.ownerPageId) {
            router.goToMarker(projectModel: projectModel, document: document, page: page, marker: viewModel.marker)
        } else {
            let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Страница документа '\(viewModel.title!)' для маркера не найдена в проекте"])
            displayError(error)
        }
    }
    
}

extension MainViewController: LastDocumentsViewControllerDelegate {
    func lastDocumentsViewController(selectDocument document: Document, project: ProjectData) {
        selectProject(project) {
            let request = MainViewModel.SelectDocument.Request(project: project, documentId: document.id, title: document.title)
            self.output.selectDocument(request: request)
        }
    }
}

extension MainViewController: NotificationsViewControllerDelegate {
    func notificationsViewController(onSelect notification: Notification, project: ProjectData?) {
        switch(notification.type) {
            case .documentAdded, .documentRemoved, .documentUpdated, .documentPagesAdded:
                if let document = notification.document, let project = project {
                    selectProject(project) {
                        let request = MainViewModel.SelectDocument.Request(project: project, documentId: document.id, title: document.title)
                        self.output.selectDocument(request: request)
                    }
                }
            case .chatMessage:
                if let roomId = notification.message?.roomId {
                    let request = MainViewModel.FetchRoom.Request(roomId: roomId, notification: notification)
                    output.selectChat(request: request)
                }
            case .documentDescriptorAdded, .markerEdit, .markerLink, .markerNote:
                if let d = notification.descriptor,
                   let project = projects?.first(where: { $0.id == notification.projectId }) {
                    selectProject(project) {
                        let request = MainViewModel.SelectMarker.Request(project: project, marker: d, title: "--")
                        self.output.selectMarker(request: request)
                    }
                } else {
                    let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Ошибка перехода. Отсутствует описание маркера."])
                    displayError(error)
                }
            default:
                break
        }
    }
}

extension MainViewController: MenuViewControllerDelegate {
    func menuViewController(onSelect actionType: MenuActionType) {
        sessionChecked = false
        router.navigateToLoginForm()
    }
}
