//
//  MainViewInteractor.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 08.06.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation

protocol MainViewInteractorInput {
    func checkSession(request: MainViewModel.CheckSession.Request)
    func projectsList(request: ProjectsViewModel.ProjectsList.Request)
    func selectDocument(request: MainViewModel.SelectDocument.Request)
    func selectChat(request: MainViewModel.FetchRoom.Request)
    func selectPage(request: MainViewModel.SelectPage.Request)
    func selectMarker(request: MainViewModel.SelectMarker.Request)
}

protocol MainViewInteractorOutput {
    func presentError(_ error: Error)
    func checkSession(response: MainViewModel.CheckSession.Response)
    func projectsList(response: ProjectsViewModel.ProjectsList.Response)
    func selectDocument(response: MainViewModel.SelectDocument.Response)
    func selectChat(response: MainViewModel.FetchRoom.Response)
    func selectPage(response: MainViewModel.SelectPage.Response)
    func selectMarker(response: MainViewModel.SelectMarker.Response)
}

class MainViewInteractor: MainViewInteractorInput {
    var output: MainViewInteractorOutput!
    var worker = MainViewWorker()
    
    //
    
    func checkSession(request: MainViewModel.CheckSession.Request) {
        jsonRequest(RestAPI.ping) { [weak self] json, error in
            guard self != nil else {
                return
            }
            
            if error == nil {
                if let result = json?.dictionaryValue {
                    if let error = result["error"]?.dictionaryObject {
                        if let name = error["name"] as? String, name == "FORBIDDEN" {
                            let response = MainViewModel.CheckSession.Response(valid: false)
                            self?.output.checkSession(response: response)
                        } else {
                            self?.output.presentError(MainViewError.serverError(msg: error["message"] as? String ?? "Unknown error"))
                        }
                    } else {
                        let response = MainViewModel.CheckSession.Response(valid: true)
                        self?.output.checkSession(response: response)
                    }
                } else {
                    let response = MainViewModel.CheckSession.Response(valid: false)
                    self?.output.checkSession(response: response)
                }
            } else {
                if error!.localizedDescription.contains("Forbidden") {
                    let response = MainViewModel.CheckSession.Response(valid: false)
                    self?.output.checkSession(response: response)
                } else {
                    self?.output.presentError(error!)
                }
            }
            
        }
    }
    
    func projectsList(request: ProjectsViewModel.ProjectsList.Request) {
        ProjectAPI.loadProjects() { [weak self] projects, error in
            guard self != nil else {
                return
            }
            
            if error != nil {
                self?.output.presentError(error!)
            } else {
                let response = ProjectsViewModel.ProjectsList.Response(projects: projects!)
                self?.output.projectsList(response: response)
            }
        }
    }
    
    func selectDocument(request: MainViewModel.SelectDocument.Request) {
        DocumentAPI.projectDocuments(projectId: request.project.id) { [weak self] error, documents in
            guard self != nil else {
                return
            }
            
            if error == nil {
                let response = MainViewModel.SelectDocument.Response(project: request.project, documentId: request.documentId, documents: documents!, title: request.title)
                self!.output.selectDocument(response: response)
            } else {
                self?.output.presentError(error!)
            }
        }
    }
    
    func selectChat(request: MainViewModel.FetchRoom.Request) {
        if let manager = Session.socketManager() {
            let socket = manager.defaultSocket
            
            socket.off("init")
            socket.on("init") { [weak self] data, ack in
                if self != nil && data.count > 0 {
                    if let roomData = ((data[0] as! [String: Any])["rooms"] as! [[String: Any]]).first(where: { $0["id"] as! Int == request.roomId }) {
                        let room = ChatRoom(data: roomData)
                        let response = MainViewModel.FetchRoom.Response(room: room, notification: request.notification)
                        self!.output.selectChat(response: response)
                    }
                }
                socket.off("init")
            }
            
            if socket.status == .notConnected || socket.status == .disconnected {
                socket.connect()
            } else {
                manager.reconnect()
            }
        }
    }
    
    func selectPage(request: MainViewModel.SelectPage.Request) {
        DocumentAPI.projectDocuments(projectId: request.project.id) { [weak self] error, documents in
            guard self != nil else {
                return
            }
            
            if error == nil {
                let response = MainViewModel.SelectPage.Response(project: request.project, documentId: request.documentId, pageId: request.pageId, documents: documents!, title: request.title)
                self!.output.selectPage(response: response)
            } else {
                self?.output.presentError(error!)
            }
        }
    }
    
    func selectMarker(request: MainViewModel.SelectMarker.Request) {
        DocumentAPI.projectDocuments(projectId: request.project.id) { [weak self] error, documents in
            guard self != nil else {
                return
            }
            
            if error == nil {
                let response = MainViewModel.SelectMarker.Response(project: request.project, marker: request.marker, documents: documents!, title: request.title)
                self!.output.selectMarker(response: response)
            } else {
                self?.output.presentError(error!)
            }
        }
    }
}
