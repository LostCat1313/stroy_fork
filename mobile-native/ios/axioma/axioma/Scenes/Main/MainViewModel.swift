//
//  MainViewModel.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 08.06.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation

enum MainViewError:  Error {
    case serverError(msg: String)
}

struct MainViewModel {
    struct CheckSession {
        
        struct Request {
            
        }
        
        struct Response {
            var valid: Bool
        }
        
        struct ViewModel {
            var valid: Bool
        }
    }
    
    struct SelectDocument {
        struct Request {
            var project: ProjectData
            var documentId: Int
            var title: String?
        }
        
        struct Response {
            var project: ProjectData
            var documentId: Int
            var documents: [Document]
            var title: String?
        }
        
        struct ViewModel {
            var project: ProjectData
            var documentId: Int
            var documents: [Document]
            var title: String?
        }
    }
    
    struct SelectPage {
        struct Request {
            var project: ProjectData
            var documentId: Int
            var pageId: Int
            var title: String?
        }
        
        struct Response {
            var project: ProjectData
            var documentId: Int
            var pageId: Int
            var documents: [Document]
            var title: String?
        }
        
        struct ViewModel {
            var project: ProjectData
            var documentId: Int
            var pageId: Int
            var documents: [Document]
            var title: String?
        }
    }
    
    struct SelectMarker {
        struct Request {
            var project: ProjectData
            var marker: PageMarker
            var title: String?
        }
        
        struct Response {
            var project: ProjectData
            var marker: PageMarker
            var documents: [Document]
            var title: String?
        }
        
        struct ViewModel {
            var project: ProjectData
            var marker: PageMarker
            var documents: [Document]
            var title: String?
        }
    }
    
    struct FetchRoom {
        struct Request {
            var roomId: Int
            var notification: Notification
        }
        
        struct Response {
            var room: ChatRoom
            var notification: Notification
        }
        
        struct ViewModel {
            var room: ChatRoom
            var notification: Notification
        }
    }
}
