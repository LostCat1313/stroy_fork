//
//  MainViewPresenter.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 08.06.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation

protocol MainViewPresenterInput {
    func presentError(_ error: Error)
    func checkSession(response: MainViewModel.CheckSession.Response)
    func projectsList(response: ProjectsViewModel.ProjectsList.Response)
    func selectDocument(response: MainViewModel.SelectDocument.Response)
    func selectChat(response: MainViewModel.FetchRoom.Response)
    func selectPage(response: MainViewModel.SelectPage.Response)
}

protocol MainViewPresenterOutput: class {
    func displayError(_ error: Error)
    func checkSession(viewModel: MainViewModel.CheckSession.ViewModel)
    func projectsList(viewModel: ProjectsViewModel.ProjectsList.ViewModel)
    func selectDocument(viewModel: MainViewModel.SelectDocument.ViewModel)
    func selectChat(viewModel: MainViewModel.FetchRoom.ViewModel)
    func selectPage(viewModel: MainViewModel.SelectPage.ViewModel)
    func selectMarker(viewModel: MainViewModel.SelectMarker.ViewModel)
}

class MainViewPresenter: NSObject, MainViewPresenterInput {
    weak var output: MainViewPresenterOutput!
    
    func presentError(_ error: Error) {
        self.output.displayError(error)
    }
    
    func checkSession(response: MainViewModel.CheckSession.Response) {
        let viewModel = MainViewModel.CheckSession.ViewModel(valid: response.valid)
        self.output.checkSession(viewModel: viewModel)
    }
    
    func projectsList(response: ProjectsViewModel.ProjectsList.Response) {
        let viewModel = ProjectsViewModel.ProjectsList.ViewModel(projects: response.projects)
        self.output.projectsList(viewModel: viewModel)
    }
    
    func selectDocument(response: MainViewModel.SelectDocument.Response) {
        let viewModel = MainViewModel.SelectDocument.ViewModel(project: response.project, documentId: response.documentId, documents: response.documents, title: response.title)
        output.selectDocument(viewModel: viewModel)
    }
    
    func selectChat(response: MainViewModel.FetchRoom.Response) {
        let viewModel = MainViewModel.FetchRoom.ViewModel(room: response.room, notification: response.notification)
        output.selectChat(viewModel: viewModel)
    }
    
    func selectPage(response: MainViewModel.SelectPage.Response) {
        let viewModel = MainViewModel.SelectPage.ViewModel(project: response.project, documentId: response.documentId,
                                                           pageId: response.pageId, documents: response.documents, title: response.title)
        output.selectPage(viewModel: viewModel)
    }
    
    func selectMarker(response: MainViewModel.SelectMarker.Response) {
        let viewModel = MainViewModel.SelectMarker.ViewModel(project: response.project, marker: response.marker,
                                                            documents: response.documents, title: response.title)
        output.selectMarker(viewModel: viewModel)
    }
}
