//
//  PersonalNotesAPI.swift
//  axioma
//
//  Created by sss on 08.10.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

struct PersonalNotesAPI  {
    public static func get(completion: @escaping (_ notes: [PersonalNote]?, Error?) -> ()) {
        jsonRequest(RestAPI.getNotes) { json, error in
            if error != nil {
                completion(nil, error)
            } else {
                let list = json!["list"].array!
                let notes: [PersonalNote] = list.reduce(into: []) { acc, json in
                    let note = PersonalNote(json)
                    acc.append(note)
                }
                completion(notes, nil)
            }
        }
    }
    
    public static func create(note: PersonalNote, completion: @escaping (_ note: PersonalNote?, _ err: Error?) -> ()) {
        var entry: [String: Any] = [:]
        entry["content"] = note.text
        var params: [String: Any] = [:]
        params["entry"] = entry
        jsonRequest(RestAPI.createNote(params)) { json, error in
            if error == nil {
                completion(PersonalNote(json!), nil)
            } else {
                completion(nil, error)
            }
        }
    }
    
    public static func update(note: PersonalNote, completion: @escaping (_ err: Error?) -> ()) {
        var entry: [String: Any] = [:]
        entry["content"] = note.text
        var params: [String: Any] = [:]
        params["entry"] = entry
        jsonRequest(RestAPI.updateNote(noteId: note.id, params: params)) { json, error in
            completion(error)
        }
    }
    
    public static func delete(note: PersonalNote, completion: @escaping (_ err: Error?) -> ()) {
        jsonRequest(RestAPI.deleteNote(noteId: note.id)) { json, error in
            completion(error)
        }
    }
}
