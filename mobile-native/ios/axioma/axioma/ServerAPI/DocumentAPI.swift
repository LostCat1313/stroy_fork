//
//  DocumentAPI.swift
//  axioma
//
//  Created by sss on 25.07.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire



struct DocumentAPI  {
    
    public static func createFolder(projectId: Int, parentId: Int, title: String, completion: @escaping (Error?) -> ()) {
        var params: [String: Any] = [:]
        var entry: [String:Any] = [:]
        
        entry["title"] = title
        if parentId > 0 {
            entry["parentId"] = parentId
        }
        entry["projectId"] = projectId
        entry["isFolder"] = true
        params["entry"] = entry
        jsonRequest(RestAPI.createDocument(params), completion: {completion($1)})
    }
    
    public static func createDocument(projectId: Int, parentId: Int, title: String, shortTitle: String?, completion: @escaping (_ doc: Document?, _ error: Error?) -> ()) {
        var params: [String: Any] = [:]
        var entry: [String:Any] = [:]
        
        entry["title"] = title
        entry["shortTitle"] = shortTitle ?? ""
        if parentId > 0 {
            entry["parentId"] = parentId
        } else {
            fatalError("We cannot create document at the root..")
        }
        entry["projectId"] = projectId
        entry["isFolder"] = false
        params["entry"] = entry
        jsonRequest(RestAPI.createDocument(params), completion: { json, error in
            if error != nil {
                completion(nil, error)
            } else {
               completion(Document(json: json!), nil)
            }
        })
    }
    
    public static func updateDocument(_ doc: Document, completion: @escaping (_ doc: Document?, _ error: Error?) -> ()) {
        var params: [String: Any] = [:]
        var entry: [String:Any] = [:]
        
        entry["title"] = doc.title
        entry["shortTitle"] = doc.shortTitle
        if let pid = doc.parentId {
            entry["parentId"] = pid
        }
    
        entry["projectId"] = doc.projectId
        entry["isFolder"] = doc.isFolder
        params["entry"] = entry
        jsonRequest(RestAPI.updateDocument(documentId: doc.id, params: params), completion: { json, error in
            if error != nil {
                completion(nil, error)
            } else {
                completion(Document(json: json!), nil)
            }
        })
    }
    
    public static func createVersion(documentId: Int, completion: @escaping (Version?, Error?) -> ()) {
        var params: [String: Any] = [:]
        var entry: [String:Any] = [:]
        entry["title"] = "Version from ios"
        entry["documentId"] = documentId
        params["entry"] = entry
        jsonRequest(RestAPI.createVersion(params)) { json, error in
            if let e = error {
                completion(nil, e)
            } else {
                completion(Version(json: json!), nil)
            }
        }
    }
    
    public static func updatePageNumbers(pages: [(id: Int, number: Int)], completion: @escaping (Error?) -> ()) {
        var params: [String: Any] = [:]
        var arr: [[String:Any]] = []
        for page in pages {
            var entry: [String:Any] = [:]
            entry["id"] = page.id
            entry["pageNumber"] = page.number
            entry["actual"] = true
            arr.append(entry)
        }
        params["pageList"] = arr
        jsonRequest(RestAPI.updatePage(params)) { json, error in
            completion(error)
        }
    }
    
    public static func uploadFiles(documentId: Int, versionId: Int, files: [URL], uploadProgress:((_ progress: Progress)->())? = nil, completion: @escaping (_ pages: [Page]?, _ error: Error?) -> ()) {
        do {
            let path = try RestAPI.uploadFilesToVersion(docId: "\(documentId)", verId: "\(versionId)").path.asURL()
            FileAPI.uploadFiles(path: path, files: files, uploadProgress: uploadProgress, completion: completion)
        } catch {
            completion(nil, error)
        }
    }

    public static func deleteDocument(id: Int,  completion: @escaping (Error?) -> ()) {
        jsonRequest(RestAPI.deleteDocument(docId: id)) { json, error in
            completion(error)
        }
    }
    
    public static func loadMarkers(docId: Int, pageId: Int,  completion: @escaping (_ err: Error?, _ markers: [PageMarker]?) -> ()) {
        jsonRequest(RestAPI.getMarkers(docId: docId, pageId: pageId)) { json, error in
            if error == nil {
                let list = json!["list"].array!
                let markers: [PageMarker] = list.reduce(into: []) { acc, itemJson in
                    if let m = PageMarker.create(json: itemJson) {
                        if let dm = m as? DrawableMarker {
                            let marker = dm as! PageMarker
                            acc.append(marker)
                        } else {
                            acc.append(m)
                        }
                    }
                }
                completion(nil, markers)
            } else {
                completion(error, nil)
            }
        }
    }
    
    public static func loadProjectMarkers(projectId: Int, type: PageMarkerTypeId, completion: @escaping (_ err: Error?, _ markers: [PageMarker]?) -> ()) {
        jsonRequest(RestAPI.getProjectMarkers(projectId: projectId, type: type)) { json, error in
            if error == nil {
                let list = json!["list"].array!
                let markers: [PageMarker] = list.reduce(into: []) { acc, itemJson in
                    if let m = PageMarker.create(json: itemJson) {
                        if let dm = m as? DrawableMarker {
                            let marker = dm as! PageMarker
                            acc.append(marker)
                        } else {
                            acc.append(m)
                        }
                    }
                }
                completion(nil, markers)
            } else {
                completion(error, nil)
            }
        }
    }
    
    public static func createMarker(docId: Int, pageId: Int, marker: PageMarker, completion: @escaping (_ err: Error?, _ marker: PageMarker?) -> ()) {
        var entry: [String: Any] = [:]
        entry["type"] = marker.type.rawValue
        entry["text"] = marker.text
        entry["title"] = marker.title
        entry["docId"] = docId
        entry["holderId"] = pageId
        entry["params"] = marker.getParams()
        var params: [String: Any] = [:]
        params["entry"] = entry
        
        jsonRequest(RestAPI.createMarker(params)) { json, error in
            if error == nil {
                let marker = PageMarker.create(json: json!)
                completion(nil, marker)
            } else {
                completion(error, nil)
            }
        }
    }
    
    public static func updateMarker(marker: PageMarker, completion: @escaping (_ err: Error?) -> ()) {
        var entry: [String: Any] = [:]
        entry["type"] = marker.type.rawValue
        entry["text"] = marker.text
        entry["title"] = marker.title
        entry["docId"] = marker.ownerDocId
        entry["holderId"] = marker.ownerPageId
        entry["holderType"] = 1
        entry["params"] = marker.getParams()
        var params: [String: Any] = [:]
        params["entry"] = entry
        
        jsonRequest(RestAPI.updateMarker(markerId: marker.id, params: params)) { json, error in
            completion(error)
        }
    }
    
    public static func deleteMarker(marker: PageMarker, completion: @escaping (_ err: Error?) -> ()) {
        jsonRequest(RestAPI.deleteMarker(markerId: marker.id)) { json, error in
            completion(error)
        }
    }
    
    public static func projectDocuments(projectId: Int, completion: @escaping (Error?, [Document]?) -> ()) {
        jsonRequest(RestAPI.documents(projectId)) { json, error in
            if error == nil {
                var documents: [Document] = []
                if let data = json?.dictionary?["list"]?.array {
                    for documentJson in data {
                        documents.append(Document(json: documentJson))
                    }
                }
                completion(nil, documents)
            } else {
                completion(error, nil)
            }
            
        }
    }
}
