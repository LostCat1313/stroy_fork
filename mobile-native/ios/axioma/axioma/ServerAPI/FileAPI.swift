
import Foundation
import Alamofire
import SwiftyJSON

struct FileAPI  {
    
    public static func getFilePage(fileId: Int, completion: @escaping (_ page: Page?, _ error: Error?) -> ()) {
        jsonRequest(RestAPI.file(fileId: fileId)) { json, error in
            if error != nil {
                completion(nil, error)
            } else {
                completion(Page(json: json!["entry"]), nil)
            }
        }
    }
    
    public static func uploadFiles(files: [URL], uploadProgress:((_ progress: Progress)->())? = nil, completion: @escaping (_ pages: [Page]?, _ error: Error?) -> ()) {
        do {
            let path = try RestAPI.uploadFiles.path.asURL()
            FileAPI.uploadFiles(path: path, files: files, uploadProgress: uploadProgress, completion: completion)
        } catch {
            completion(nil, error)
        }
    }
    
    public static func uploadFiles(path: URL, files: [URL], uploadProgress:((_ progress: Progress)->())? = nil, completion: @escaping (_ pages: [Page]?, _ error: Error?) -> ()) {
        let mfdProc:(_ mfd: MultipartFormData) -> () = { mfd in
            for (idx, file) in files.enumerated() {
                mfd.append(file, withName: "page\(idx)")
            }
        }
        let encodedProc: (_ mng: SessionManager.MultipartFormDataEncodingResult) -> () = { res in
            switch res {
                case .failure(let e):
                    completion(nil, e)
                case .success(let request, _, _):
                    request.uploadProgress { progress in
                        uploadProgress?(progress)
                    }
                    request.responseJSON { responce in
                        switch responce.result {
                            case .failure(let e):
                                completion(nil, e)
                            
                            case .success(let data):
                                let json = JSON(data)
                                if let e = jsonFindError(answer: JSON(data)) {
                                    completion(nil, e)
                                } else {
                                    let pages = json["files"].arrayValue.map{Page(json: $0)}
                                    completion(pages, nil)
                                }
                        }
                    }
            }
        }
        
        var headers: HTTPHeaders = ["Content-Type": "application/json"]
        if let sessionId = Session.id {
            headers["Authorization"] = "Bearer \(sessionId)"
        }
        Alamofire.upload(multipartFormData: mfdProc, to: path, headers: headers, encodingCompletion: encodedProc)
    }
    
}
