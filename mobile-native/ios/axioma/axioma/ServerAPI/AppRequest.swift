//
//  AppRequest.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 08.06.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

public func jsonFindError(answer: JSON) -> Error? {
    if answer["error"] != nil {
        let e = answer["error"]
        if e["status"].intValue == 403 {
            NotificationCenter.default.post(name: .didGetForbidden, object: nil)
        }
        return NSError(domain: "", code: e["status"].intValue, userInfo: [NSLocalizedDescriptionKey: e["message"].stringValue])
    }
    return nil
}

public func jsonRequest(_ request:URLRequestConvertible, completion: @escaping (JSON?, Error?) -> Void) {
    let task = Alamofire.request(request)
    task.validate(statusCode: 200..<300)
    task.responseJSON(queue: nil) { response in
        
        switch(response.result) {
            case .success(let jsonData):
                let json = JSON(jsonData)
                if let e = jsonFindError(answer: json) {
                    if let req = response.request {
                        NSLog("[jsonRequest] Error sending request to %@", req.url!.absoluteString)
                        NSLog("[jsonRequest] Error is: %@", e.localizedDescription)
                    }
                    completion(nil, e)
                } else {
                    if let req = response.request {
                        NSLog("[jsonRequest] Sent request to %@", req.url!.absoluteString)
                    }
                    completion(json, nil)
                }
            
            case .failure(let error):
                if let req = response.request {
                    NSLog("[jsonRequest] Error sending request to %@", req.url!.absoluteString)
                    NSLog("[jsonRequest] Error is: %@", error.localizedDescription)
                }
                completion(nil, error)
                break
        }
        
    }
    
}
