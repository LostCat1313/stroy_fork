//
//  UserAPI.swift
//  axioma
//
//  Created by sss on 30/10/2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation
import SwiftyJSON

struct  UserAPI {
    public static func getUsers(completion: @escaping (_ users: [User]?, _ error: Error?) -> ()) {
        jsonRequest(RestAPI.getUsers) { json, error in
            if error != nil {
                completion(nil, error)
            } else {
                if let list = json?["list"].array ?? json?["users"].array {
                    let users: [User] = list.reduce(into: []) {acc, itemJson in
                        let user = User(json: itemJson)
                        acc.append(user)
                    }
                    completion(users, nil)
                } else {
                    fatalError("Unrecognized format.")
                }
            }
        }
    }
    
    public static func getProjectUsers(projectId: Int, completion: @escaping (_ users: [UserRole]?, _ error: Error?) -> ()) {
        jsonRequest(RestAPI.getProjectUsers(projectId: projectId)) { json, error in
            if error != nil {
                completion(nil, error)
            } else {
                if let list = json?["list"].array ?? json?["users"].array {
                    let users: [UserRole] = list.reduce(into: []) {acc, itemJson in
                        acc.append(UserRole(itemJson))
                    }
                    completion(users, nil)
                } else {
                    fatalError("Unrecognized format.")
                }
            }
        }
    }
    
    public static func setProjectUsers(projectId: Int, rights: [(userId: Int, role: Rights.Role)], completion: @escaping (Error?) -> ()) {
        var params: [String: Any] = [:]
        let itemsRights: [[String: Any]] = rights.reduce(into: []) { acc, item in
            var itemDic: [String: Any] = [:]
            itemDic["UserId"] = item.userId
            itemDic["rightMask"] = item.role.mask
            acc.append(itemDic)
        }
        params["rights"] = itemsRights
        jsonRequest(RestAPI.setProjectUsers(projectId: projectId, params: params)) { json, error in
            completion(error)
        }
    }
    
}
