//
//  ApplicationAPI.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 10/12/2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation

struct ApplicationAPI {
    
    public static func organizations(completion: @escaping (_ organizations: [Organization]?, Error?) -> ()) {
        jsonRequest(RestAPI.organizations) { json, error in
            if error != nil {
                completion(nil, error)
            } else {
                let list = json!["list"].array!
                let organizations: [Organization] = list.reduce(into: []) { acc, itemJson in
                    if let data = itemJson.dictionaryObject {
                        acc.append(Organization(data: data))
                    }
                }
                completion(organizations, nil)
            }
        }
    }
    
    public static func positions(completion: @escaping (_ positions: [OrganizationPosition]?, Error?) -> ()) {
        jsonRequest(RestAPI.positions) { json, error in
            if error != nil {
                completion(nil, error)
            } else {
                let list = json!["list"].array!
                let positions: [OrganizationPosition] = list.reduce(into: []) { acc, itemJson in
                    if let data = itemJson.dictionaryObject {
                        acc.append(OrganizationPosition(data: data))
                    }
                }
                completion(positions, nil)
            }
        }
    }
}
