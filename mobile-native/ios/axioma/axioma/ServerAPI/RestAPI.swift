//
//  ProjectAPI.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 15.06.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

enum RestAPI: URLRequestConvertible {
    
    case signup(phone: String, regId: String)
    
    case activate(phone: String, code: String)
    case updateProfile([String: Any])
    case login(name: String, pass: String)
    
    case ping
    case postPing(regId: String)
    
    case contacts
    case organizations
    case positions
    case projects
    case createProject([String: Any])
    case updateProject([String: Any])
    case deleteProject(Int)
    case notifications(Int, Int)
    case notes
    case lastDocuments
    case documents(Int)
    case document(String)
    case documentPages(String)
    case file(fileId: Int)
    case projectUsers(String)
    
    case createDocument([String: Any])
    case updateDocument(documentId: Int, params: [String: Any])
    
    case createVersion([String: Any])
    case updatePage([String: Any])
    
    case uploadFiles
    case uploadFileToVersion(String, String, Data)
    case uploadFilesToVersion(docId: String, verId: String)
    
    case deleteDocument(docId: Int)
    
    //MARKERS (DESCRIPTORS)
    case getMarkers(docId: Int, pageId: Int)
    case getProjectMarkers(projectId: Int, type: PageMarkerTypeId)
    
    case createMarker([String: Any])
    case updateMarker(markerId: Int, params: [String: Any])
    case deleteMarker(markerId: Int)
   
    //PERSONAL NOTES
    case getNotes
    case createNote([String: Any])
    case updateNote(noteId: Int, params: [String: Any])
    case deleteNote(noteId: Int)
    
    //USERS
    case getUsers
    case getProjectUsers(projectId: Int)
    case setProjectUsers(projectId: Int, params: [String: Any])
    
    var method: HTTPMethod {
        switch self {
           
            case .login, .updateProfile, .postPing:
                return .post
            
            case .uploadFiles:
                return .post
            
            case .uploadFileToVersion(_, _, _):
                return .post
            
            case .uploadFilesToVersion(_, _):
                return .post
            
            case .createDocument(_):
                return .post
            
            case .updateDocument(_, _):
                return .put
            
            case .createVersion, .createProject:
                return .post
            
            case .updatePage(_):
                return .post
            
            case .deleteDocument(_):
                return .delete;
            
            case .createMarker(_):
                return .post
            
            case .updateMarker, .updateProject:
                return .put
            
            case .deleteMarker(_):
                return .delete
            
            case .createNote(_):
                return .post
            
            case .updateNote(_, _):
                return .put
           
            case .deleteNote(_):
                return .delete
            
            case .setProjectUsers(_, _):
                return .put

            case .deleteProject:
                return .delete
            
            default:
                return .get
        }
    }
    
    var path: String {
        
        switch self {
        
            case .signup:
                return self.baseURLString().appendingFormat("/api/authenticate/signup")
            
            case .activate:
                return self.baseURLString().appendingFormat("/api/authenticate/activate")
            
            case .updateProfile:
                return self.baseURLString().appendingFormat("/api/users/signup/update")
            
            case .organizations:
                return self.baseURLString().appendingFormat("/api/organizations")
            
            case .positions:
                return self.baseURLString().appendingFormat("/api/positions")
            
            case .login(_, _):
                return self.baseURLString().appendingFormat("/api/authenticate")
            
            case .ping, .postPing:
                return self.baseURLString().appendingFormat("/api/ping")
            
            case .contacts:
                return self.baseURLString().appendingFormat("/api/contacts")
            
            case .projects:
                return self.baseURLString().appendingFormat("/api/projects?all=false")
            
            case .createProject:
                return self.baseURLString().appendingFormat("/api/projects")
            
            case .updateProject(let projectInfo):
                let projectId = (projectInfo["entry"] as! [String: Any])["id"] as! Int
                return self.baseURLString().appendingFormat("/api/projects/\(projectId)")
            
            case .deleteProject(let projectId):
                return self.baseURLString().appendingFormat("/api/projects/\(projectId)")
            
            case .notifications(let offset, let limit):
                return self.baseURLString().appendingFormat("/api/notifications?offset=\(offset)&limit=\(limit)")
 
            case .notes:
                return self.baseURLString().appendingFormat("/api/notes")
            
            case .documents(let projectId):
                return self.baseURLString().appendingFormat("/api/projects/\(projectId)/documents?plain=false")
            
            case .lastDocuments:
                return self.baseURLString().appendingFormat("/api/documents/last")
            
            case .document(let documentId):
                return self.baseURLString().appendingFormat("/api/documents/\(documentId)")
            
            case .createDocument(_):
                return self.baseURLString().appendingFormat("/api/documents")
            
            case .updateDocument(let documentId, _):
                return self.baseURLString().appendingFormat("/api/documents/\(documentId)")
            
            
            case .createVersion(_):
                return self.baseURLString().appendingFormat("/api/document/versions")
            
            case .updatePage(_):
                return self.baseURLString().appendingFormat("/api/pages/update")
            
            case .documentPages(let documentId):
                return self.baseURLString().appendingFormat("/api/documents/\(documentId)/pages")
            
            case .file(let fileId):
                return self.baseURLString().appendingFormat("/api/files/\(fileId)")
            
            case .projectUsers(let projectId):
                return self.baseURLString().appendingFormat("/api/projects/\(projectId)/users/?")
            
            case .uploadFiles:
                return self.baseURLString().appendingFormat("/api/files/upload")
            
            case .uploadFileToVersion(let documentId, let versionId, _):
                return self.baseURLString().appendingFormat("/api/documents/\(documentId)/\(versionId)/pages/upload")
      
            case .uploadFilesToVersion(let documentId, let versionId):
                return self.baseURLString().appendingFormat("/api/documents/\(documentId)/\(versionId)/pages/upload")
        
            case .deleteDocument(let documentId):
                return self.baseURLString().appendingFormat("/api/documents/\(documentId)")
            
            case .getMarkers(let docId, let pageId):
                return self.baseURLString().appendingFormat("/api/descriptors?docId=\(docId)&holderId=\(pageId)")
            
            case .getProjectMarkers(let projectId, let type):
                return self.baseURLString().appendingFormat("/api/descriptors?projectId=\(projectId)&type=\(type.rawValue)")
            
            case .createMarker(_):
                return self.baseURLString().appendingFormat("/api/descriptors")
            
            case .updateMarker(let markerId, _):
                return self.baseURLString().appendingFormat("/api/descriptors/\(markerId)")
            
            case .deleteMarker(let markerId):
                return self.baseURLString().appendingFormat("/api/descriptors/\(markerId)")
            
            case .getNotes:
                return self.baseURLString().appendingFormat("/api/notes")
            
            case .createNote(_):
                return self.baseURLString().appendingFormat("/api/notes")
            
            case .updateNote(let id, _):
                return self.baseURLString().appendingFormat("/api/notes/\(id)")
        
            case .deleteNote(let id):
                return self.baseURLString().appendingFormat("/api/notes/\(id)")
            
            case .getUsers:
                return self.baseURLString().appendingFormat("/api/users")
            
            case .getProjectUsers(let projectId):
                return self.baseURLString().appendingFormat("/api/projects/\(projectId)/users")
            
            case .setProjectUsers(let projectId, _):
                return self.baseURLString().appendingFormat("/api/projects/\(projectId)/users")
            
        }
    }
    
    /// Returns a URL request or throws if an `Error` was encountered.
    ///
    /// - throws: An `Error` if the underlying `URLRequest` is `nil`.
    ///
    /// - returns: A URL request.
    public func asURLRequest() throws -> URLRequest {
        
        var urlRequest = URLRequest(url: try self.path.asURL())
        urlRequest.httpMethod = method.rawValue
        
        switch self {
            case .login(let userName, let password):
                var parameters: [String:String] = [:]
                parameters["username"] = userName
                parameters["password"] = password
                urlRequest.encodeParamsAndSign(parameters)
            
            case .signup(let phone, let regId):
                var parameters: [String:String] = [:]
                parameters["phone"] = phone
                parameters["deviceType"] = "1"
                parameters["regId"] = regId
                urlRequest = try URLEncoding.queryString.encode(urlRequest, with: parameters)
            
            case .activate(let phone, let code):
                var parameters: [String:String] = [:]
                parameters["phone"] = phone
                parameters["code"] = code
                urlRequest = try URLEncoding.queryString.encode(urlRequest, with: parameters)
            
            case .organizations:
                break
            
            case .createDocument(let params),
                 .updateDocument(_, let params),
                 .createVersion(let params),
                 .updatePage(let params),
                 .createMarker(let params),
                 .createNote(let params),
                 .updateNote(_, let params),
                 .createProject(let params),
                 .updateProject(let params),
                 .setProjectUsers(_, let params),
                 .updateProfile(let params):
                    urlRequest.httpBody = JSON(params).rawString()?.asData()
                    urlRequest.sign()
            
            case .postPing(let regId):
                var params: [String:Any] = [:]
                params["regId"] = regId
                params["deviceType"] = 1
                urlRequest.httpBody = JSON(params).rawString()?.asData()
                urlRequest.sign()
            
            case .uploadFileToVersion(_, _, let data):
                let formData = Alamofire.MultipartFormData()
                let mimeType = "application/octet-stream"
                formData.append(data, withName: "page", mimeType: mimeType)
                urlRequest.setValue(formData.contentType, forHTTPHeaderField: "Content-Type")
                urlRequest.httpBody = try? formData.encode()
                urlRequest.sign()
            
            case .uploadFiles:
                fatalError("Not implemented.")
            
            case .uploadFilesToVersion(_, _):
                fatalError("Not implemented.")
            
            case .updateMarker(_, let params):
                urlRequest.httpBody = JSON(params).rawString()?.asData()
                urlRequest.sign()
            
            default:
                urlRequest.sign()
        }
        
        
        return urlRequest
    }
}
