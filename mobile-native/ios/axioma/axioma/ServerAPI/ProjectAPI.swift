//
//  ProjectAPI.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 31/10/2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

struct ProjectAPI  {
    
    static func loadProjects(completion: @escaping ([ProjectData]?, Error?) -> ()) {
        jsonRequest(RestAPI.projects) { json, error in
            if error == nil {
                if let result = json?.dictionary {
                    if let error = result["error"]?.dictionaryObject {
                        completion(nil, ProjectsViewModel.Errors.serverError(msg: error["message"] as? String ?? "Unknown error"))
                    } else if let data = result["list"]?.array {
                        var projects: [ProjectData] = []
                        
                        for projectJson in data {
                            let project = ProjectData(json: projectJson)
                            project.imageUrl = project.imageUrl != nil ? Configuration.serverUrl.appendingFormat(project.imageUrl!) : nil
                            projects.append(project)
                        }
                        
                        completion(projects, nil)
                    }
                }
            } else {
                completion(nil, error!)
            }
        }
    }
    
    public static func createProject(title: String, imageId: Int, completion: @escaping (_ project: ProjectData?, Error?) -> ()) {
        var params: [String: Any] = [:]
        var entry: [String:Any] = [:]
        
        entry["id"] = 0
        entry["title"] = title
        entry["imageId"] = imageId
        params["entry"] = entry
        
        jsonRequest(RestAPI.createProject(params)) { json, error in
            if error != nil {
                completion(nil, error)
            } else {
                let project = ProjectData(json: json!)
                completion(project, nil)
            }
        }
    }
    
    public static func updateProject(projectId: Int, title: String, imageId: Int, completion: @escaping (_ project: ProjectData?, Error?) -> ()) {
        var params: [String: Any] = [:]
        var entry: [String:Any] = [:]
        
        entry["id"] = projectId
        entry["title"] = title
        entry["imageId"] = imageId
        params["entry"] = entry
        
        jsonRequest(RestAPI.updateProject(params)) { json, error in
            if error != nil {
                completion(nil, error)
            } else {
                let project = ProjectData(json: json!)
                completion(project, nil)
            }
        }
    }
}
