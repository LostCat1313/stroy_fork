//
//  Configuration.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 07.06.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation
import UIKit

fileprivate let _serverURL = "https://app.ax19.ru"

enum AppConfiguration {
    case Debug
    case TestFlight
    case AppStore
}

class Configuration {
    class Colors {
        static let dark = UIColor(red: CGFloat(0x63)/0xFF, green: CGFloat(0x63)/0xFF, blue: CGFloat(0x63)/0xFF, alpha: 1.0)
        static let orange = UIColor(red: CGFloat(0xFF)/0xFF, green: CGFloat(0x98)/0xFF, blue: CGFloat(0x00)/0xFF, alpha: 1.0)
        static let transparentDark = UIColor(red: CGFloat(0x63)/0xFF, green: CGFloat(0x63)/0xFF, blue: CGFloat(0x63)/0xFF, alpha: 0.5)
        static let projectFolder = UIColor(red: CGFloat(0xAA)/0xFF, green: CGFloat(0xAA)/0xFF, blue: CGFloat(0xAA)/0xFF, alpha: 1.0)
        static let enabledText = UIColor(red: 0, green: 0, blue: 0, alpha: 1.0)
        static let disabledText = UIColor(red: CGFloat(0xAA)/0xFF, green: CGFloat(0xAA)/0xFF, blue: CGFloat(0xAA)/0xFF, alpha: 1.0)
        static let markerChangeBkColor = UIColor(red: CGFloat(0xAD)/0xFF, green: CGFloat(0x14)/0xFF, blue: CGFloat(0x14)/0xFF, alpha: 1.0)
        static let markerLinkBkColor = UIColor(red: CGFloat(0x00)/0xFF, green: CGFloat(0x96)/0xFF, blue: CGFloat(0x88)/0xFF, alpha: 1.0)
        static let markerNoticeBkColor = UIColor(red: CGFloat(0xBF)/0xFF, green: CGFloat(0x00)/0xFF, blue: CGFloat(0xE5)/0xFF, alpha: 1.0)
    }
    static var instance = Configuration()
    
    init() {
        
    }
    
    static var serverUrl: String {
        return _serverURL
    }
    
    private static let isTestFlight = Bundle.main.appStoreReceiptURL?.lastPathComponent == "sandboxReceipt"
    
    // This can be used to add debug statements.
    static var isDebug: Bool {
        #if DEBUG
        return true
        #else
        return false
        #endif
    }
    
    static var appConfiguration: AppConfiguration {
        if isDebug {
            return .Debug
        } else if isTestFlight {
            return .TestFlight
        } else {
            return .AppStore
        }
    }
}
