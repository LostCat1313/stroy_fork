//
//  Session.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 07.06.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation
import UIKit
import CoreData
import SocketIO

class CountSummary {
    var projects: Int = 0
    var documents: Int = 0
    var remarks: Int = 0
    var newMessages: Int = 0
    
    init() {
        
    }
    
    init(projects: Int, documents: Int, remarks: Int, newMessages: Int) {
        self.projects = projects
        self.documents = documents
        self.remarks = remarks
        self.newMessages = newMessages
    }
    
    init(data: [String: Any]) {
        projects = 0 //data["projects"]!.intValue
        documents = 0 //which project? data["documents"]!.intValue
        remarks = data["remarks"] as! Int
        newMessages = data["newMessages"] as! Int
    }
}


class Session {
    private static let instance = Session()
    
    static let currentVersion: String = "0.1"
    
    private let syncRoot = NSRecursiveLock()
    
    private var __id: String?
    private var __socketManager: SocketManager?
    
    static let begT = NSDate().timeIntervalSince1970
    public static var id : String? {
        let d = NSDate().timeIntervalSince1970 - begT
        if d < 40 {
            return instance.__id
        } else {
            return instance.__id
            //return "forbidden"
        }
    }
    
    private var __user: UserProfile?
    
    public static var user: UserProfile? {
        return instance.__user
    }
    private var __genId: Int = 0
    private var __currentProjectHandlers: [Int: ()->()] = [:]
    private var __currentProject: ProjectData?
    private var __countSummary: CountSummary?
    private var __projects: [ProjectData]?
    private var __regId: String?
    
    public static var regId: String? {
        get {
            return instance.__regId
        }
        set (v) {
            instance.__regId = v
        }
    }
    
    public static var currentProject: ProjectData? {
        get {
            return instance.__currentProject
        }
        set (v) {
            if instance.__currentProject !== v {
                instance.__currentProject = v
                instance.saveCurrentProject()
                NotificationCenter.default.post(name: .currentProjectDidChange, object: self)
            }
        }
    }
    
    public static var countSummary: CountSummary? {
        get {
            if instance.__countSummary == nil {
                instance.loadCountSummary()
            }
            
            return instance.__countSummary
        }
        set (v) {
            if instance.__countSummary !== v {
                instance.__countSummary = v
                instance.saveCountSummary()
            }
        }
    }
    
    public static var projects: [ProjectData]? {
        get {
            return instance.__projects
        }
        
        set(value) {
            instance.__projects = value
            instance.loadCurrentProject()
            Session.countSummary?.projects = (value ?? []).count
            NotificationCenter.default.post(name: .currentProjectDidChange, object: self)
        }
    }
    
    init() {
        self.loadSession()
        self.loadLoginInfo()
    }
    
    public struct AppVersion {
        static func get() -> String? {
            return UserDefaults.standard.object(forKey: "AppVersion") as? String
        }
        static func set(_ v: String) {
            UserDefaults.standard.setValue(v, forKey: "AppVersion")
        }
    }
    
  
    static func login(id: Int, name: String, sessionId: String) {
        instance.__id = sessionId
        instance.__user = UserProfile(id: id, name: name)
        saveSession()
        saveLoginInfo(id: id, name: name)
        instance.__projects = nil
        instance.loadSocketManager()
        
        if regId != nil {
            jsonRequest(RestAPI.postPing(regId: regId!), completion: {_,_ in})
        }
    }
    
    static func activate(token: String) {
        instance.__id = token
    }
    
    fileprivate static func saveLoginInfo(id: Int, name: String) {
        UserDefaults.standard.setValue(id, forKey: "userId")
        UserDefaults.standard.setValue(name, forKey: "userName")
    }
    
    fileprivate static func saveSession() {
        UserDefaults.standard.setValue(instance.__id, forKey: "sessionid")
    }
    
    fileprivate func loadSession() {
        __id = UserDefaults.standard.object(forKey: "sessionid") as? String
        loadSocketManager()
    }
    
    fileprivate func loadSocketManager() {
        if __id !=  nil {
            __socketManager = SocketManager(socketURL: URL(string: Configuration.serverUrl)!,
                                            config: [.log(true), .reconnects(false), .connectParams(["token": __id!])])
        }
    }
    
    
    fileprivate func loadLoginInfo() {
        if let id = UserDefaults.standard.object(forKey: "userId") as? Int, let name = UserDefaults.standard.object(forKey: "userName") as? String {
            __user = UserProfile(id: id, name: name)
            loadCountSummary()
            loadCurrentProject()
        }
    }
    
    static func socketManager() -> SocketManager? {
        return instance.__socketManager
    }
    
    static func isAuthorized() -> Bool {
        return instance.__id != nil
    }
    
    static func close() {
        instance.__id = nil;
        instance.__projects = nil
        instance.__socketManager = nil
        saveSession()
    }
    
    private func loadCurrentProject() {
        syncRoot.lock()
        var newCurrProject: ProjectData?
        if let name = __user?.name, let pname = UserDefaults.standard.object(forKey: "currentProjectUser") as? String,
            name == pname, let id = UserDefaults.standard.object(forKey: "currentProjectId") as? Int {
            newCurrProject = __projects?.first(where:{$0.id == id})
        }
        syncRoot.unlock()
        DispatchQueue.main.async {
            Session.currentProject = newCurrProject
        }
    }
    
    private func saveCurrentProject() {
        syncRoot.lock()
        if let name = __user?.name {
            let project = __currentProject
            UserDefaults.standard.setValue(name, forKey: "currentProjectUser")
            UserDefaults.standard.setValue(project?.id, forKey: "currentProjectId")
        }
        syncRoot.unlock()
    }
    
    private func saveCountSummary() {
        syncRoot.lock()
        if let name = __user?.name {
            let summary = __countSummary
            UserDefaults.standard.setValue(name, forKey: "countSummaryUser")
            UserDefaults.standard.setValue(summary?.projects, forKey: "countSummaryProjects")
            UserDefaults.standard.setValue(summary?.documents, forKey: "countSummaryDocuments")
            UserDefaults.standard.setValue(summary?.remarks, forKey: "countSummaryRemarks")
            UserDefaults.standard.setValue(summary?.newMessages, forKey: "countSummaryMessages")
        }
        syncRoot.unlock()
    }
    
    private func loadCountSummary() {
        syncRoot.lock()
        var summary: CountSummary?
        if let name = __user?.name, let savedName = UserDefaults.standard.object(forKey: "countSummaryUser") as? String, name == savedName {
            summary = CountSummary()
            summary?.projects = UserDefaults.standard.object(forKey: "countSummaryProjects") as! Int
            summary?.documents = UserDefaults.standard.object(forKey: "countSummaryDocuments") as! Int
            summary?.remarks = UserDefaults.standard.object(forKey: "countSummaryRemarks") as! Int
            summary?.newMessages = UserDefaults.standard.object(forKey: "countSummaryMessages") as! Int
        }
        __countSummary = summary
        syncRoot.unlock()
    }
    
    class func makeErrorDialog(_ title: String, message: String, onClose: @escaping () -> () =  {  }) -> UIAlertController {
        let ctr = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Dismiss", style: .cancel) { _ in
            onClose()
        }
        ctr.addAction(action)
        return ctr
    }
    
    class func makeMessageDialog(_ title: String, message: String, onClose: @escaping () -> () =  {  }) -> UIAlertController {
        let ctr = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .cancel) { _ in
            onClose()
        }
        ctr.addAction(action)
        
        return ctr
    }
}
