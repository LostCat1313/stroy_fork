//
//  MenuViewController.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 02.06.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit

enum MenuActionType {
    case none
    case stream
    case projects
    case documents
    case notes
    case stars
    case chats
    case contacts
    case settings
    case exit
}

protocol MenuViewControllerDelegate {
    func menuViewController(onSelect actionType: MenuActionType)
}

class MenuViewController: UIViewController {
    
    let menuModel: [MainMenuItemData] = [
        MainMenuItemData(action: .stream, caption: "Лента", icon: "format-list-bulleted-black"),
        MainMenuItemData(action: .projects, caption: "Проекты", icon: "box-shadow-1"),
        MainMenuItemData(action: .documents, caption: "Документы", icon: "file", countBackgroundColor: UIColor(red: 255/255, green: 204/255, blue: 51/255, alpha: 1)),
        MainMenuItemData(action: .notes, caption: "Замечания", icon: "note", countBackgroundColor: UIColor(red: 153/255, green: 255/255, blue: 51/255, alpha: 1)),
        MainMenuItemData(action: .stars, caption: "Личные заметки", icon: "star"),
        MainMenuItemData(action: .chats, caption: "Чаты", icon: "message", countBackgroundColor: UIColor(red: 0/255, green: 144/255, blue: 81/255, alpha: 1)),
        MainMenuItemData(action: .contacts, caption: "Контакты", icon: "account-multiple"),
        MainMenuItemData(action: .settings, caption: "Настройки", icon: "settings"),
        MainMenuItemData(action: .exit, caption: "Выход", icon: "exit-to-app")
    ]

    @IBOutlet weak var cvMenu: UICollectionView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var currProjectTitle: UILabel!
    
    var delegate: MenuViewControllerDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cvMenu.register(UINib(nibName: "MainMenuItemCell", bundle: nil), forCellWithReuseIdentifier: "MainMenuItemCell")
        updateMenuData()
        NotificationCenter.default.addObserver(self, selector: #selector(self.didGetForbidden), name: .didGetForbidden, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.currentProjectDidChange), name: .currentProjectDidChange, object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func didGetForbidden() {
        var ctrls: [UIViewController] = []
        var cursor = self.presentedViewController
        while cursor != nil {
            ctrls.insert(cursor!, at: 0)
            cursor = cursor?.presentedViewController
        }
        var doDismiss: ((_ idx: Int) -> ())?
        doDismiss = { idx in
            if idx < ctrls.count {
                ctrls[idx].dismiss(animated: false) {
                    doDismiss?(idx + 1)
                }
            } else {
                self.delegate?.menuViewController(onSelect: .exit)
            }
        }
        doDismiss?(0)
    }
    
    @objc func currentProjectDidChange() {
         updateMenuData()
    }
    
    func updateMenuData() {
        userName.text = Session.user?.name ?? "---"
        userName.sizeToFit()
        
        if let projectInfo = Session.currentProject {
            currProjectTitle.text = projectInfo.title
        } else {
            currProjectTitle.text = "---"
            currProjectTitle.sizeToFit()
        }
        
        if let summary = Session.countSummary {
            for item in menuModel {
                switch(item.action) {
                    case .projects:
                        item.count = summary.projects
                    case .documents:
                        if let projectInfo = Session.currentProject {
                            item.count = projectInfo.documentsCount
                        } else {
                            item.count = 0
                        }
                    case .notes:
                        item.count = summary.remarks
                    case .chats:
                        item.count = summary.newMessages
                    default: break
                }
            }
        }
        
        cvMenu.reloadData()
    }
    
    var sideMenu: PGSideMenu? {
        return self.parent as? PGSideMenu
    }
}

extension MenuViewController: MainMenuItemCellDelegate {
    
    func mainMenuItemCell(execute cell: MainMenuItemCell) {
        
        if !cell.menuItemData.enabled {
            return
        }
        
        sideMenu?.toggleLeftMenu()
        
        // go to next page
        switch (cell.menuItemData.action) {
            
            case .documents:
                let controller = DocumentsViewController()
                self.present(controller, animated: true) {
                    print("here" + (self.cvMenu == nil ? "1" : "0"))
                }
            
            case .projects:
                let controller = ProjectsViewController()
                self.present(controller, animated: true)
            
            case .notes:
                if let p = Session.currentProject {
                    ProjectNotesViewController.show(self, project: p)
                }
            
            case .chats:
                let controller = RoomsViewController()
                self.present(controller, animated: true)
            
            case .contacts:
                let controller = ContactsViewController()
                self.present(controller, animated: true)
            
            case .stars:
                PersonalNotesViewController.show(self)
            
            case .exit:
                /*
                if let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController {
                    Session.close()
                    self.present(controller, animated: false, completion: nil)
                }*/
                delegate?.menuViewController(onSelect: .exit)
            default: break
        }
    }
}

extension MenuViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return menuModel.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: MainMenuItemCell = cvMenu.dequeueReusableCell(withReuseIdentifier: "MainMenuItemCell", for: indexPath) as! MainMenuItemCell
        cell.configure(menuModel[indexPath.item])
        cell.delegate = self
        return cell
    }
}

extension MenuViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let h: CGFloat = min(60, cvMenu.frame.height / CGFloat(menuModel.count))
        return CGSize(width: cvMenu.frame.width, height: h)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
}

extension MenuViewController: MainViewControllerDelegate {
    func mainViewController(onProject project: ProjectData) {
        updateMenuData()
    }
}
