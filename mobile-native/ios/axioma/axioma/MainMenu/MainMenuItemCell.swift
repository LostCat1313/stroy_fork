//
//  MainMenuItemCell.swift
//  axioma
//
//  Created by sss on 15.06.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import UIKit

class MainMenuItemData {
    var action: MenuActionType = .none
    var caption: String
    var icon: String
    var enabled: Bool
    var count: Int = 0
    var countBackgroundColor: UIColor = UIColor.white
    
    init(action: MenuActionType, caption: String, icon: String = "", countBackgroundColor: UIColor = UIColor.lightGray, enabled: Bool = true, count: Int = 0) {
        self.action = action
        self.caption = caption
        self.icon = icon
        self.enabled = enabled
        self.count = count
        self.countBackgroundColor = countBackgroundColor
    }
}

protocol MainMenuItemCellDelegate {
    func mainMenuItemCell(execute cell: MainMenuItemCell)
}

class MainMenuItemCell: UICollectionViewCell {
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var objectsCountLabel: EdgeInsetLabel!
    @IBOutlet weak var tapPanel: UIView!
    
    @IBOutlet weak var btnItem: UIButton!
    var delegate: MainMenuItemCellDelegate!
    var menuItemData: MainMenuItemData!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(_ data: MainMenuItemData) {
        menuItemData = data
        btnItem.setTitle(data.caption, for: .normal)
        btnItem.isEnabled = data.enabled
        icon.image = UIImage(named: data.icon)
        if data.enabled {
            icon.alpha = 1.0
            btnItem.alpha = 1.0
        } else {
            icon.alpha = 0.2
            btnItem.alpha = 0.2
        }
        
        objectsCountLabel.isHidden = data.count == 0
        objectsCountLabel.text = "\(data.count)"
        objectsCountLabel.backgroundColor = data.countBackgroundColor
    }
    
    @IBAction func onBtnItem(_ sender: UIButton) {
        delegate.mainMenuItemCell(execute: self)
    }
}
