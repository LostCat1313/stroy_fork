//
//  PdfUtils.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 15/12/2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation
import CoreGraphics
import UIKit

func pdfToImage(document: CGPDFDocument, pageNumber: Int = 1) -> UIImage? {
    guard let page = document.page(at: pageNumber) else { return nil }
    
    let pageRect = page.getBoxRect(.mediaBox)
    let renderer = UIGraphicsImageRenderer(size: pageRect.size)
    
    let img = renderer.image { ctx in
        UIColor.white.set()
        ctx.fill(pageRect)
        
        ctx.cgContext.translateBy(x: 0.0, y: pageRect.size.height)
        ctx.cgContext.scaleBy(x: 1.0, y: -1.0)
        ctx.cgContext.drawPDFPage(page)
    }
    
    return img
}

func pdfToImages(url: URL) -> [UIImage]? {
    guard let document = CGPDFDocument(url as CFURL) else { return nil }
     var images: [UIImage] = []
    
    for x in 1..<document.numberOfPages+1 {
        if let image = pdfToImage(document: document, pageNumber: x) {
            images.append(image)
        }
    }
    
    return images
}

func pdfToImageFiles(url: URL) -> [(image: UIImage, url: URL)]? {
    guard let images = pdfToImages(url: url) else {
        return nil
    }
    
    let files = AppFiles()
    var result: [(image: UIImage, url: URL)] = []
    
    for (index, image) in images.enumerated() {
        if let data = UIImageJPEGRepresentation(image, 0.8) {
            let prefix = url.deletingPathExtension().lastPathComponent.hashed(.md5)!
            let filename = files.getURL(for: .Documents).appendingPathComponent("\(prefix)_\(index).jpeg")
            
            do {
                try data.write(to: filename)
                result.append((image, filename))
                print("[export pdf] create image file:\(filename)")
            } catch {
                print("[export pdf] couldn't write file:\(filename)")
            }
        }
    }
    return result
}
