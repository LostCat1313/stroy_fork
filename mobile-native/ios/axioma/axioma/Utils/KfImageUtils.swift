//
//  KfImageUtils.swift
//  axioma
//
//  Created by Andrei Okoneshnikov on 19.06.2018.
//  Copyright © 2018 Andrei Okoneshnikov. All rights reserved.
//

import Foundation
import Kingfisher

let imageRequestModifier = AnyModifier { request in
    var r = request
    if let id = Session.id {
        r.setValue("Bearer \(id)", forHTTPHeaderField: "Authorization")
    }
    return r
}
