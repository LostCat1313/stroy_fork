import { 
    CONTACTS_REQUEST,
    CONTACTS_SUCCESS,
    CONTACTS_FAILURE,
    SET_CONTACT_INDEX
} from '../actions/contacts';
import { SET_PROJECT_INDEX } from '../actions/project';


const initialState = {
//     {
//         key: '1',
//         firstName: 'Иван',
//         secondName: 'Иванов',
//         jobTitle: 'Сантехник',
//         hasThumbnail: true,
//         thumbnailPath: '../../../images/logo.png'
//     }, {
//         key: '2',
//         firstName: 'Петя',
//         secondName: 'Петров',
//         jobTitle: 'Прораб',
//         hasThumbnail: false,
//         thumbnailPath: ''
//     },{
//         key: '3',
//         firstName: 'Сидор',
//         secondName: 'Сидоров',
//         jobTitle: 'Дворник',
//         hasThumbnail: false,
//         thumbnailPath: ''
//     },{
//         key: '4',
//         firstName: 'Иван',
//         secondName: 'Петров',
//         jobTitle: 'Работяга',
//         hasThumbnail: false,
//         thumbnailPath: ''
//     },{
//         key: '5',
//         firstName: 'Иван',
//         secondName: 'Кузнецов',
//         jobTitle: 'Работяга',
//         hasThumbnail: false,
//         thumbnailPath: ''
//     }, {
//         key: '6',
//         firstName: 'Иван',
//         secondName: 'Потапов',
//         jobTitle: 'Работяга',
//         hasThumbnail: false,
//         thumbnailPath: ''
//     }
  list: null,
  current: null,
  loading: false
};

export default function (state = initialState, action ) {
  switch(action.type) {
    case CONTACTS_REQUEST:
      return {
        ...state,
        list: null,
        loading: true,
        refresh: false,
      }
    case CONTACTS_SUCCESS:
      return {
        ...state,
        list: action.response.users,
        loading: false,
        refresh: false,
      }
    case CONTACTS_FAILURE:
      return {
        ...state,
        list: null,
        loading: false,
        refresh: false,
        error: 'Ошибка при загрузке списка контактов'
      }
    case SET_PROJECT_INDEX: 
      return {
        ...state,
        list: null,
        loading: false,
        error: null,
        refresh: true
      }
    case SET_CONTACT_INDEX:
      if (state.list && state.list.length > 0) {
        var current = state.list.find((e) => e.id == action.payload);
        if (current) {
          return {
            ...state,
            current: current
          }  
        }
      }
      break;

  }

  return state;
}
