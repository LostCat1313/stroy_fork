
import { combineReducers } from 'redux';

import user from './user';
import notification from './notification';
import project from './project';
import folders from './folders';
import contacts from './contacts';
import lastDocuments from './lastDocuments';

export default combineReducers({
  user,
  notification,
  project,
  folders,
  contacts,
  lastDocuments
});
