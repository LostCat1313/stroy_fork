import { PROJECTS_REQUEST, PROJECTS_SUCCESS, PROJECTS_FAILURE, SET_PROJECT_INDEX } from '../actions/project';

/*

0 - message
1 - remark
2 - file

[
    {
      date: '2017-05-23',
      project: 'Дом у моря',
      title: 'Сообщение от Вадим',
      organization: 'ООО "Восток сервис"',
      text: 'Необходимо добавить замеры',
      imageUri: 'https://facebook.github.io/react/img/logo_og.png',
      documentsNumber: 1,
      participantsNumber: 1
    },
    {
      date: '2017-05-13',
      project: 'Дом у моря',
      title: 'Добавлен файл от Иванова М.И.',
      imageUri: 'https://facebook.github.io/react/img/logo_og.png',
      documentsNumber: 1,
      participantsNumber: 1
    }
  ],

*/

const initialState = {
  list: null,
  current: null,
  loading: false
};

export default function (state = initialState, action ) {
  switch(action.type) {
    case PROJECTS_REQUEST:
      return {
        ...state,
        list: null,
        loading: true
      }
    case PROJECTS_SUCCESS:
      return {
        ...state,
        list: action.response.projects,
        loading: false
      }
    case PROJECTS_FAILURE:
      return {
        ...state,
        list: null,
        loading: false,
        error: 'Ошибка при загрузке списка проектов'
      }
    case SET_PROJECT_INDEX:
      if (state.list && state.list.length > 0) {
        var current = state.list.find((e) => e.id == action.payload);
        if (current) {
          return {
            ...state,
            current: current,
            currentId: current.id
          }  
        }
      }
      break;

  }

  // if (action.type === SET_PROJECT_INDEX) {
  //   return {
  //     ...state,
  //     selectedIndex: action.payload,
  //   };
  // }
  return state;
}
