const childs = [
    {
        key: '1',
        title: 'Раздел 1',
        count: 2
    }, {
        key: '2',
        title: 'Раздел 2',
        count: 559
    }, {
        key: '3',
        title: 'Раздел 3',
        count: 2
    }, {
        key: '4',
        title: 'Раздел N',
        count: 2
    }, {
        key: '5',
        title: 'Раздел 71',
        count: 2
    }, {
        key: '6',
        title: 'Раздел 72',
        count: 559
    }, {
        key: '7',
        title: 'Раздел 73',
        count: 2
    }, {
        key: '8',
        title: 'Раздел 7N',
        count: 2
    }
];

const childs1 = [
    {
        key: '11',
        title: 'Раздел 21',
        count: 2
    }, {
        key: '21',
        title: 'Раздел 22',
        count: 559
    }, {
        key: '31',
        title: 'Раздел 23',
        count: 2
    }, {
        key: '41',
        title: 'Раздел 2N',
        count: 2
    }
];

const initialState = {
  list: [
    {
        key: 'АРХИТЕКТУРНЫЕ решения',
        data: childs,
        expanded: true
    }, {
        key: 'ВЕНТИЛЯЦИЯ',
        data: childs1,
        expanded: true
    },{
        key: 'Электрика',
        data: childs1,
        expanded: true
    },{
        key: 'Водоснабжение',
        data: childs1,
        expanded: true
    },{
        key: 'Землеустройство',
        data: childs1,
        expanded: true
    }, {
        key: 'Еще какое-то устройство',
        data: childs1,
        expanded: true
    }
  ],
  selectedIndex: null,
};

export default function (state = initialState, action ) {
  return state;
}
