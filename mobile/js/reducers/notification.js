
import type { Action } from '../actions/types';
import { 
  SET_NOTIFICATION_INDEX,
  NOTIFICATIONS_REQUEST,
  NOTIFICATIONS_SUCCESS,
  NOTIFICATIONS_FAILURE 
} from '../actions/notification';
import { SET_PROJECT_INDEX } from '../actions/project';

/*

0 - message
1 - remark
2 - file

*/

const initialState = {
  list: null,
  selectedIndex: null,
  loading: false,
  error: null
};

export default function (state = initialState, action ) {
  switch(action.type) {
    case NOTIFICATIONS_REQUEST:
      return {
        ...state,
        list: null,
        loading: true,
        refresh: false
      }
    case NOTIFICATIONS_SUCCESS:
      return {
        ...state,
        list: action.response.list,
        loading: false,
        refresh: false
      }
    case NOTIFICATIONS_FAILURE:
      return {
        ...state,
        list: null,
        loading: false,
        refresh: false,
        error: 'Ошибка при загрузке уведомлений'
      }
      break;
    case SET_PROJECT_INDEX: 
      return {
        ...state,
        list: null,
        loading: false,
        error: null,
        refresh: true
      }
  }

  return state;
}
