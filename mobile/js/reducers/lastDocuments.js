import { 
    LASTDOCUMENTS_REQUEST,
    LASTDOCUMENTS_SUCCESS,
    LASTDOCUMENTS_FAILURE
} from '../actions/lastDocuments';
import { SET_PROJECT_INDEX } from '../actions/project';


const initialState = {
  list: null,
  current: null,
  loading: false
};

export default function (state = initialState, action ) {
  switch(action.type) {
    case LASTDOCUMENTS_REQUEST:
      return {
        ...state,
        list: null,
        loading: true,
        refresh: false,
      }
    case LASTDOCUMENTS_SUCCESS:
      return {
        ...state,
        list: action.response.docs,
        loading: false,
        refresh: false,
      }
    case LASTDOCUMENTS_FAILURE:
      return {
        ...state,
        list: null,
        loading: false,
        refresh: false,
        error: 'Ошибка при загрузке списка последних документов'
      }
    case SET_PROJECT_INDEX: 
      return {
        ...state,
        list: null,
        loading: false,
        error: null,
        refresh: true,
      }
    // case SET_LASTDOCUMENT_INDEX:
    //   if (state.list && state.list.length > 0) {
    //     var current = state.list.find((e) => e.id == action.payload);
    //     if (current) {
    //       return {
    //         ...state,
    //         current: current
    //       }  
    //     }
    //   }
    //   break;

  }

  return state;
}
