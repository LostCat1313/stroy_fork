import { RESTORE_SESSION, RESET_USER, LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_FAILURE } from '../actions/user';
import { REHYDRATE } from 'redux-persist/constants'

const initialState = {
  name: null,
  token: null,
  error: null,
  loading: false,
  initializing: true
};

export default function (state = initialState, action) {
  // console.log(action);
  switch(action.type) {
    case LOGIN_REQUEST:
      return {
        ...state,
        loading: true
      };
    case RESET_USER:
      return {
        ...state,
        name: null,
        token: null,
        error: null,
        loading: false
      };
    case LOGIN_SUCCESS:
      let newState = {
        ...state,
        name: action.username,
        token: action.response.token,
        loading: false
      };
      return newState;
    case LOGIN_FAILURE: 
      return {
        ...state,
        name: null,
        token: null,
        error: action.error,
        loading: false
      };
    // restoring session
    case REHYDRATE:
      var incoming = action.payload.user;
      if (incoming) return { ...state, ...incoming, initializing: false, error: null }
      return { ...state, initializing: false, error: null }
    default:
      return state;
  }
}
