
import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { MenuContext } from 'react-native-popup-menu';

import { StyleProvider } from 'native-base';
import App from './App';
import configureStore from './config/configureStore';
import getTheme from '../native-base-theme/components';

import baseTheme from './themes/base-theme';



function setup():React.Component {
  class Root extends Component {
    constructor() {
      super();

      this.state = {
        isLoading: true,
        store: configureStore(() => this.setState({ isLoading: false })),
      };
    }

    render() {
      return (
        <StyleProvider style={getTheme(baseTheme)}>
          <Provider store={this.state.store}>
              <MenuContext>
                <App isLoading={this.state.isLoading} />
              </MenuContext> 
          </Provider>
        </StyleProvider>
      );
    }
  }

  return Root;
}

export default setup;
