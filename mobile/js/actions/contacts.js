export const SET_CONTACT_INDEX = 'SET_CONTACT_INDEX';
export const CONTACTS_REQUEST = 'CONTACTS_REQUEST';
export const CONTACTS_SUCCESS = 'CONTACTS_SUCCESS';
export const CONTACTS_FAILURE = 'CONTACTS_FAILURE';

export function setCurrentContact(index) {
  return {
    type: SET_CONTACT_INDEX,
    payload: index,
  };
}

export function loadContacts() {
  return {
    types: [CONTACTS_REQUEST, CONTACTS_SUCCESS, CONTACTS_FAILURE],
    uri: '/api/contacts',
    data: {  },
    payload: {  }
  }
}

export function loadProjectContacts(projectId) {
  return {
    types: [CONTACTS_REQUEST, CONTACTS_SUCCESS, CONTACTS_FAILURE],
    uri: '/api/projects/' + projectId + '/contacts',
    data: {  },
    payload: {  }
  }
}