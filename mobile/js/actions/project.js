export const SET_PROJECT_INDEX = 'SET_PROJECT_INDEX';
export const PROJECTS_REQUEST = 'PROJECTS_REQUEST';
export const PROJECTS_SUCCESS = 'PROJECTS_SUCCESS';
export const PROJECTS_FAILURE = 'PROJECTS_FAILURE';

export function setCurrentProject(index) {
  return {
    type: SET_PROJECT_INDEX,
    payload: index,
  };
}

export function loadProjects() {
  return {
    types: [PROJECTS_REQUEST, PROJECTS_SUCCESS, PROJECTS_FAILURE],
    uri: '/api/projects',
    data: {  },
    payload: {  }
  }
}
