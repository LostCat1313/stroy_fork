export const SET_NOTIFICATION_INDEX = 'SET_NOTIFICATION_INDEX';
export const NOTIFICATIONS_REQUEST = 'NOTIFICATIONS_REQUEST';
export const NOTIFICATIONS_SUCCESS = 'NOTIFICATIONS_SUCCESS';
export const NOTIFICATIONS_FAILURE = 'NOTIFICATIONS_FAILURE';

export function setCurrentNotification(index:number) {
  return {
    type: SET_NOTIFICATION_INDEX,
    payload: index,
  };
}

export function loadNotifications() {
  return {
    types: [NOTIFICATIONS_REQUEST, NOTIFICATIONS_SUCCESS, NOTIFICATIONS_FAILURE],
    uri: '/api/notifications',
    data: {  },
    payload: {  }
  }
}
