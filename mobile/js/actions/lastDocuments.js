export const LASTDOCUMENTS_REQUEST = 'LASTDOCUMENTS_REQUEST';
export const LASTDOCUMENTS_SUCCESS = 'LASTDOCUMENTS_SUCCESS';
export const LASTDOCUMENTS_FAILURE = 'LASTDOCUMENTS_FAILURE';

export function loadLastDocuments() {
  return {
    types: [LASTDOCUMENTS_REQUEST, LASTDOCUMENTS_SUCCESS, LASTDOCUMENTS_FAILURE],
    uri: '/api/documents/last',
    data: {  },
    payload: {  }
  }
}

export function loadProjectLastDocuments(projectId) {
  return {
    types: [LASTDOCUMENTS_REQUEST, LASTDOCUMENTS_SUCCESS, LASTDOCUMENTS_FAILURE],
    uri: '/api/projects/' + projectId + '/documents/last',
    data: {  },
    payload: {  }
  }
}