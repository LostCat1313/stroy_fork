export const RESET_USER = 'RESET_USER';
export const LOGIN_REQUEST = 'LOGIN_REQUEST';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAILURE = 'LOGIN_FAILURE';

export function reset() {
  return {
    type: RESET_USER
  };
}

export function login(username, password) {
  return {
    types: [LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_FAILURE],
    uri: '/api/authenticate',
    method: 'post',
    data: { username, password },
    payload: { username }
  }
}
