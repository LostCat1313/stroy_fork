
export default function callAPIMiddleware({ dispatch, getState }) {
  return next => action => {
    const {
      types,
      uri,
      data,
      method,
      shouldCallAPI = () => true,
      payload = {}
    } = action

    if (!types) {
      // Normal action: pass it on
      return next(action)
    }

    if (
      !Array.isArray(types) ||
      types.length !== 3 ||
      !types.every(type => typeof type === 'string')
    ) {
      throw new Error('Expected an array of three string types.')
    }

    if (!uri) {
      throw new Error('Expected uri specified.')
    }


    if (!shouldCallAPI(getState())) {
      return
    }

    var token = getState().user.token;
    
    const [ requestType, successType, failureType ] = types

    dispatch(Object.assign({}, payload, {
      type: requestType
    }));


     var requestUrl = `http://localhost:3000${uri}`;
    //var requestUrl = `http://maintenance.dev.grebenshikov.ru${uri}`;
    console.log(requestType, requestUrl, token, data);

    return fetch(requestUrl, {
        method: !method ? 'get' : method,
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Access-Token': token
        },
        body: method && method !== 'get' ? JSON.stringify(data) : null,
        qs: method === 'get' ? data : null
      })
      .then(result => {
        // console.log(result);
        return result.json()
      })
      .then(response => {
        // console.log('Response', response);
        if (response.error) {
          return dispatch(Object.assign({}, payload, {
            error: response.error,
            type: failureType
          }))
        } else {
          return dispatch(Object.assign({}, payload, {
            response,
            type: successType
          }));  
        }
      }).catch(error => {
        // console.error(error);
        return dispatch(Object.assign({}, payload, {
          error,
          type: failureType
        }));
      });
  }
}