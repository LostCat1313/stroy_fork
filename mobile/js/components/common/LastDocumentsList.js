
import React, { Component } from 'react';
import { FlatList, ActivityIndicator, TouchableOpacity, View } from 'react-native';
import { Text, Content } from 'native-base';
import { connect } from 'react-redux';
import Notification from '../common/Notification';
import styles from './styles';
import { loadLastDocuments, loadProjectLastDocuments } from '../../actions/lastDocuments';

class LastDocumentsList extends Component {

  static propTypes = {
    loadNotifications: React.PropTypes.func,
    loading: React.PropTypes.bool,
    error: React.PropTypes.string,
    list: React.PropTypes.arrayOf(React.PropTypes.object)
  }

  constructor(props) {
    super(props);
  }
  // TODO: refactor to one class
  componentWillMount() {

    this._load(this.props.currentProject);
  }

  componentWillUpdate(nextProps, nextState) {
    if(nextProps.refresh) {
      this._load(nextProps.currentProject);
    }
  }

  _load(project) {
    if(project) {
      this.props.loadProjectLastDocuments(project.id);
    } else {
      this.props.loadLastDocuments();
    }
  }

  _renderItem = (item) => {
      // console.log(item);
      return <TouchableOpacity
              style={styles.listItem}
              onPress={() => this._onPressItem(item)}>

              <Text style={styles.listItemText}>{item.item.title}</Text>
              {/*(item.item.count > 0) && <Text style={styles.listItemCount}>{item.item.count}</Text>*/}

          </TouchableOpacity>
      
  }

  _onPressItem = (item) => {
      alert(item.item.title);
  }

  _itemSeparatorComponent = () => {
      return <View style={{backgroundColor: '#535353', height: 1, marginLeft: 24}}/>
  }

  render() {
    if (this.props.loading) {
      return <ActivityIndicator />
    } else if (this.props.error){
      return <Text>{this.props.error}</Text>
    } else if (!this.props.list || this.props.list.length === 0){
      return <Text>Список пуст</Text>
    }else {
      return <FlatList
            style={styles.row}
            data={this.props.list}
            keyExtractor={(item, index) => item.id + ' ' + index }
            renderItem={ this._renderItem }
            ItemSeparatorComponent={this._itemSeparatorComponent}
        />
    }
    ;
  }
}

function bindAction(dispatch) {
  return {
    loadLastDocuments: () => dispatch(loadLastDocuments()),
    loadProjectLastDocuments: (id) => dispatch(loadProjectLastDocuments(id)),
  };
}

const mapStateToProps = state => ({
  currentProject: state.project.current,
  list: state.lastDocuments.list,
  loading: state.lastDocuments.loading,
  error: state.lastDocuments.error,
  refresh: state.lastDocuments.refresh
});

export default connect(mapStateToProps, bindAction)(LastDocumentsList);
