import React, { PureComponent } from 'react';
import { SectionList, Text, View, TouchableOpacity } from 'react-native';
import Expandable from './Expandable';
import styles from './styles';

class FolderList extends PureComponent {
    expanded = new Map();

    _keyExtractor = (item, index) => item.key;

    _onPressItem = (item) => {
        alert(item.item.title);
    };

    _onPressHeader = (item) => {
        item.section.expanded = !item.section.expanded;
        arr = this.expanded.get(item.section.key);

        item.section.data.map((v) => {
            console.log(v);
            arr[v.key]._toggle(item.section.expanded);
        });
    };

    _addComponentToSection = (sectionKey, itemKey, component) => {
        var components = this.expanded.get(sectionKey);

        if (components === undefined)
            components = [];

        components[itemKey] = component;
        this.expanded.set(sectionKey, components);
    };

    _renderItem = (item) => {
        // console.log(item);
        return (
            <TouchableOpacity
                style={styles.listItem}
                onPress={() => this._onPressItem(item)}>

                <Text style={styles.listItemText}>{item.item.title}</Text>
                {(item.item.count > 0) && <Text style={styles.listItemCount}>{item.item.count}</Text>}

            </TouchableOpacity>
        )
    };

    _renderSectionHeader = (headerItem) => {
        return (
            <Text style={{...styles.sectionHeader, ...styles.sectionHeaderText}}>{headerItem.section.key.toUpperCase()}</Text>
        )
    };

    _itemSeparatorComponent = () => {
        return <View style={{backgroundColor: '#535353', height: 1, marginLeft: 24}}/>
    };

    render() {
        console.log(this.props.items);
        return (
            <SectionList
                sections={this.props.items}
                keyExtractor={this._keyExtractor}
                renderItem={this._renderItem}
                renderSectionHeader={this._renderSectionHeader}
                ItemSeparatorComponent={this._itemSeparatorComponent}
                stickySectionHeadersEnabled
            />
        );
    }
}

export default FolderList;
