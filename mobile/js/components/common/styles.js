import theme from '../../themes/base-theme';

const React = require('react-native');
const { StyleSheet } = React;

export default {
    container: {
        elevation: 4,
        backgroundColor: theme.mainBackgroundColor,
        position: 'absolute',
        bottom: 90,
        right: 20,
        width: 150,
    },
    button: {
        backgroundColor: theme.mainBackgroundColor,
        padding: 10,
        paddingLeft: 20
    },
    sectionHeader: {
        padding: 12,
        paddingLeft: 15,
        backgroundColor: '#DDDDDD',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    sectionHeaderText: {
        color: 'black'
    },
    listItem: {
        padding: 12,
        paddingLeft: 26,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    listItemText: {
        color: 'black'
    },
    listItemCount: {
        paddingLeft: 5,
        paddingRight: 5,
        backgroundColor: '#FA9312',
        color: 'black'
    },
    row: {
        paddingLeft: 7,
        paddingRight: 7
    },
    fab: {
      backgroundColor: theme.mainBackgroundColor,
      shadowColor: '#000',
      shadowOffset: { width: 1, height: 1 },
      shadowOpacity: 0.3,
      shadowRadius: 1.5,
      elevation: 1,
    }
};
