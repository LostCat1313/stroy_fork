
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Card, CardItem, Header, Icon, Left, Right, Body, Text, StyleProvider } from 'native-base';
import { Actions, ActionConst } from 'react-native-router-flux';

import DefaultNotification from './notifications/DefaultNotification'
import FileNotification from './notifications/FileNotification'
import MessageNotification from './notifications/MessageNotification'
import RemarkNotification from './notifications/RemarkNotification'


class Notification extends Component {

  static propTypes = {
    item: React.PropTypes.object,
  }

  render() {
    var notification;


    switch (this.props.item.type){
        case 0:
            return (  
                <MessageNotification item={this.props.item}></MessageNotification>
            );
        case 1:
            return (
                <RemarkNotification item={this.props.item}></RemarkNotification>
            );
        case 2:
            return (
                <FileNotification item={this.props.item}></FileNotification>
            );
        default: 
            return (
                <DefaultNotification item={this.props.item}></DefaultNotification>
            );
    }
  }
}


function bindAction(dispatch) {
  return {
    // closeDrawer: () => dispatch(closeDrawer()),
    // setIndex: index => dispatch(setIndex(index)),
  };
}

export default connect(null, bindAction)(Notification);
