import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Button, View, Text, Fab, IconNB } from 'native-base';
import { Animated } from 'react-native';
import { Actions } from 'react-native-router-flux';

import styles from './styles';

import Menu, {
  MenuContext,
  MenuOptions,
  MenuOption,
  MenuTrigger
} from 'react-native-popup-menu';


const buttons = [
  {
    caption: 'Замечание',
    route: 'blankPage',
  },
  {
    caption: 'Личная заметка',
    route: 'blankPage',
  },
  {
    caption: 'Документ',
    route: 'blankPage',
  },
  {
    caption: 'Сообщение в чат',
    route: 'blankPage',
  }
]

export default class ActionMenu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            actionMenuVisible: false
        };
    }

    getButton(button, i) {
        return (
            <MenuOption
                key={i}
                value={i}
                style={styles.button}>
                <Text style={{ color: '#ffffff' }}>{button.caption}</Text>
            </MenuOption>
        );
    }


    render() {
        return (
            <View>
                <Fab
                    active={ this.state.actionMenuVisible }
                    style={styles.fab}
                    position="bottomRight"
                    onPress={() => this.refs.menu.open()}>
                    <IconNB name="plus" />
                </Fab>
                <Menu
                    ref="menu"
                    onSelect={value => alert(`Selected number: ${value}`)}
                    onClose={() => { this.state.actionMenuVisible = false }}
                    style={ styles.container }>
                    <MenuTrigger></MenuTrigger>
                    <MenuOptions>
                        {
                            buttons.map((v, i) =>
                                this.getButton(v, i)
                            )
                        }
                    </MenuOptions>
                </Menu>
            </View>
        );
    }
}
