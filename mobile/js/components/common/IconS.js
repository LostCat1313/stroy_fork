import React, { Component } from 'react';


import theme from '../../themes/base-theme'
import getTheme from '../../../native-base-theme/components';

import { Icon, StyleProvider } from 'native-base';

class IconS extends Component {
  constructor(props){
    super(props);
    
    if (props.iconFamily) {
      theme.iconFamily = props.iconFamily;  
    } else {
      theme.iconFamily = 'Ionicons';
    }
  }

  render() {
    return (
      <StyleProvider style={getTheme(theme)}>
        <Icon name={this.props.name} />
      </StyleProvider>
    );
  }
}

IconS.propTypes = {
  name: React.PropTypes.string,
  iconFamily: React.PropTypes.string
};

export default IconS;
