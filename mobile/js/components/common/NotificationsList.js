
import React, { Component } from 'react';
import { FlatList, ActivityIndicator } from 'react-native';
import { Text, Content } from 'native-base';
import { connect } from 'react-redux';
import Notification from '../common/Notification';
import styles from './styles';
import { loadNotifications } from '../../actions/notification';

class NotificationsList extends Component {

  static propTypes = {
    loadNotifications: React.PropTypes.func,
    loading: React.PropTypes.bool,
    error: React.PropTypes.string,
    list: React.PropTypes.arrayOf(React.PropTypes.object)
  }

  constructor(props) {
    super(props);
  }
  // TODO: refactor to one class
  componentWillMount() {
    this._load();
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.refresh) {
      this._load(nextProps.currentProject);
    }
  }

  _load(project) {
    this.props.loadNotifications();
  }

  render() {
    if (this.props.loading) {
      return <ActivityIndicator />
    } else if (this.props.error){
      return <Text>{this.props.error}</Text>
    } else if (!this.props.list || this.props.list.length === 0){
      return <Text>Список пуст</Text>
    }else {
      return <FlatList
            style={styles.row}
            data={this.props.list}
            keyExtractor={(item, index) => item.id + ' ' + index }
            renderItem={({item}) => <Notification item={item}/>}
        />
    }
    ;
  }
}

function bindAction(dispatch) {
  return {
    loadNotifications: () => {dispatch(loadNotifications())},
  };
}

const mapStateToProps = state => ({
  currentProject: state.project.current,
  list: state.notification.list,
  loading: state.notification.loading,
  error: state.notification.error,
  refresh: state.notification.refresh,
});

export default connect(mapStateToProps, bindAction)(NotificationsList);
