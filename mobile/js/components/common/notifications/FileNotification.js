import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Image, View } from 'react-native';
import { Card, CardItem, Header, Icon, Left, Right, Body, Text, Grid, Row, Col} from 'native-base';
import { Actions, ActionConst } from 'react-native-router-flux';

import NotificationHeader from './NotificationHeader';

const background = require('../../../../images/logo.png');

class FileNotification extends Component {

  static propTypes = {
    item: React.PropTypes.object,
    // closeDrawer: React.PropTypes.func,
    // navigateTo: React.PropTypes.func,
  }
// You can also declare that a prop is an instance of a class. This uses
  // JS's instanceof operator.
  // optionalMessage: PropTypes.instanceOf(Message),
  // optionalUnion: PropTypes.oneOfType([
  //   PropTypes.string,
  //   PropTypes.number,
  //   PropTypes.instanceOf(Message)
  // ]),
  // navigateTo(route) {
  //   this.props.navigateTo(route, 'home');
  // }

  render() {
    return (
        <Card fileNotification>
          <NotificationHeader 
            item={this.props.item}
            iconName="file"></NotificationHeader>
          <CardItem content>
            <Body>
              <Text>{this.props.item.image.title}</Text>
            </Body>
          </CardItem>
          <CardItem cardBody style={{borderTopWidth: 1, borderBottomWidth: 1, borderColor: '#ccc', marginBottom: 10,}}>
            <Image style={{  resizeMode: 'cover', width: null, height: 70, flex: 1 }} source={{uri: this.props.item.image.uri}} />
          </CardItem>
        </Card>
    );
  }
}

function bindAction(dispatch) {
  return {
    // closeDrawer: () => dispatch(closeDrawer()),
    // setIndex: index => dispatch(setIndex(index)),
  };
}

export default connect(null, bindAction)(FileNotification);
