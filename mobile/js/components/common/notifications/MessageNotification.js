import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Card, CardItem, Header, Icon, Left, Right, Body, Text } from 'native-base';
import { Actions, ActionConst } from 'react-native-router-flux';

import NotificationHeader from './NotificationHeader';

class MessageNotification extends Component {

  static propTypes = {
    item: React.PropTypes.object,
    // closeDrawer: React.PropTypes.func,
    // navigateTo: React.PropTypes.func,
  }
// You can also declare that a prop is an instance of a class. This uses
  // JS's instanceof operator.
  // optionalMessage: PropTypes.instanceOf(Message),
  // optionalUnion: PropTypes.oneOfType([
  //   PropTypes.string,
  //   PropTypes.number,
  //   PropTypes.instanceOf(Message)
  // ]),
  // navigateTo(route) {
  //   this.props.navigateTo(route, 'home');
  // }

  render() {
    return (
        <Card messageNotification>
          <NotificationHeader 
            item={this.props.item}
            iconName="message"></NotificationHeader>
          <CardItem content>
            <Body>
              <Text>
                {this.props.item.text}
              </Text>
            </Body>
          </CardItem>
        </Card>
    );
  }
}

function bindAction(dispatch) {
  return {
    // closeDrawer: () => dispatch(closeDrawer()),
    // setIndex: index => dispatch(setIndex(index)),
  };
}

export default connect(null, bindAction)(MessageNotification);
