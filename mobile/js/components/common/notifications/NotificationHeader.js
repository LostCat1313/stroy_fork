
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Card, CardItem, Header, Icon, IconNB, StyleProvider, Left, Right, Body, Text, Grid, Row, Col  } from 'native-base';
import { Actions, ActionConst } from 'react-native-router-flux';
import Menu, {
  MenuContext,
  MenuOptions,
  MenuOption,
  MenuTrigger
} from 'react-native-popup-menu';

import MaterialIcon from 'react-native-vector-icons/MaterialIcons';

import IconS from '../IconS';



// TODO: https://www.npmjs.com/package/react-native-popup-menu

class NotificationHeader extends Component {

  static propTypes = {
    iconName: React.PropTypes.string,
    iconFamily: React.PropTypes.string,
    item: React.PropTypes.object,
  }

  render() {
    return (
        <CardItem header>
          <Left>
            <IconNB name={this.props.iconName}/>
          </Left>
          <Body>
            <Grid style={{alignSelf: 'stretch'}}>
              <Row style={{marginBottom: 3}}>
                <Text headerTitle ellipsizeMode="tail" numberOfLines={2}> 
                  {this.props.item.title}
                </Text>
              </Row>
              <Row>
                <Col><Text ellipsizeMode="tail" numberOfLines={1}>{this.props.item.date}</Text></Col>
                <Col><Text ellipsizeMode="tail" numberOfLines={1}>{this.props.item.project}</Text></Col>
                <Col><Text ellipsizeMode="tail" numberOfLines={1}>{this.props.item.organization}</Text></Col>
              </Row>
            </Grid>
          </Body>
          <Right style={{marginRight: -15, marginTop: -10}}>
              <Menu onSelect={value => alert(`Selected number: ${value}`)}>
                <MenuTrigger>
                  <IconNB style={{paddingLeft: 7, paddingRight: 7, paddingTop: 10, paddingBottom: 10}} active name="dots-vertical" />
                </MenuTrigger>
                <MenuOptions>
                  <MenuOption value={1} text='One' />
                  <MenuOption value={2}>
                    <Text style={{color: 'red'}}>Two</Text>
                  </MenuOption>
                  <MenuOption value={3} disabled={true} text='Three' />
                </MenuOptions>
              </Menu>
            
          </Right>
        </CardItem>
    );
  }
}

function bindAction(dispatch) {
  return {
    // closeDrawer: () => dispatch(closeDrawer()),
    // setIndex: index => dispatch(setIndex(index)),
  };
}

export default connect(null, bindAction)(NotificationHeader);
