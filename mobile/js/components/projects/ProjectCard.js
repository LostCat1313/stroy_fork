import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Image, View } from 'react-native';
import { Card, CardItem, Header, Icon, IconNB, Left, Right, Body, Text } from 'native-base';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { setCurrentProject } from '../../actions/project';
import Menu, {
  MenuContext,
  MenuOptions,
  MenuOption,
  MenuTrigger
} from 'react-native-popup-menu';

class ProjectCard extends Component {

  static propTypes = {
    item: React.PropTypes.object,
  }

  _onPress = () => {
    this.props.setCurrentProject(this.props.item.id);
  };

  render() {
    const {
      date,
      participantsNumber,
      documentsNumber
    } = this.props.item;
    return (
        <Card projectCard>
          <CardItem header onPress={() => {this._onPress()}}>
            <Left>
              <IconNB style={{color: '#606060'}} name="box-shadow"/>
            </Left>
            <Body>
              <Grid style={{alignSelf: 'stretch'}}>
                <Row style={{marginBottom: 3}}>
                  <Text headerTitle ellipsizeMode="tail" numberOfLines={2}> 
                    {this.props.item.title}
                  </Text>
                </Row>
                <Row>
                  <Col><Text ellipsizeMode="tail" numberOfLines={1}>{'Создан: ' + this.props.item.createdAt}</Text></Col>
                  <Col><Text ellipsizeMode="tail" numberOfLines={1}>{'Участников: ' + this.props.item.participantsNumber}</Text></Col>
                  <Col><Text ellipsizeMode="tail" numberOfLines={1}>{'Документов: ' + this.props.item.documentsNumber}</Text></Col>
                </Row>
              </Grid>
            </Body>
            <Right style={{marginRight: -15, marginTop: -10}} >
              <Menu onSelect={value => alert(`Selected number: ${value}`)}>
                <MenuTrigger>
                  <IconNB style={{paddingLeft: 7, paddingRight: 7, paddingTop: 10, paddingBottom: 10}} active name="dots-vertical" />
                </MenuTrigger>
                <MenuOptions>
                  <MenuOption value={1} text='One' />
                  <MenuOption value={2}>
                    <Text style={{color: 'red'}}>Two</Text>
                  </MenuOption>
                  <MenuOption value={3} disabled={true} text='Three' />
                </MenuOptions>
              </Menu>
            </Right>
          </CardItem>
          <CardItem cardBody onPress={() => {this._onPress()}}>
            <Image style={{ marginTop: 10, resizeMode: 'cover', width: null, height: 200, flex: 1 }} source={{uri: this.props.item.imageUri ?  this.props.item.imageUri : 'https://facebook.github.io/react/img/logo_og.png' }} />
          </CardItem>
        </Card>
    );
  }
}

function bindAction(dispatch) {
  return {
    setCurrentProject: (id) => { return dispatch(setCurrentProject(id)); },
  };
}

export default connect(null, bindAction)(ProjectCard);
