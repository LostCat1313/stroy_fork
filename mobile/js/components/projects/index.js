
import React, { Component } from 'react';
import { FlatList, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { Container, Header, Title, Content, Text, Button, Icon, IconNB, Card, CardItem, Left, Body, Right, Segment } from 'native-base';
import { Grid, Row } from 'react-native-easy-grid';

import { loadProjects } from '../../actions/project';
import styles from './styles';

import ProjectCard from './ProjectCard';

class Projects extends Component {

  static propTypes = {
    name: React.PropTypes.string,
    // index: React.PropTypes.number,
    list: React.PropTypes.arrayOf(React.PropTypes.object),
    // openDrawer: React.PropTypes.func,
  }

  componentWillMount() {
    console.log('Projects loading start');
    this.props.loadProjects();
  }

  componentDidUpdate(prevProps, prevState) {
    if(this.props.currentProject != prevProps.currentProject) {
      Actions.home();
    }
  }

  render() {
    const { props: { name, index, list } } = this;

    return (
      <Container style={styles.container}>
        <Header stretched>
          <Left>
            <Button transparent onPress={() => Actions.refresh({key: 'drawer', open: true })}>
              <Icon active name="menu" />
            </Button>
          </Left>

          <Body style={{alignItems: 'flex-start'}}>
            <Title >Проекты</Title>
          </Body>

          <Right>
            <Button transparent onPress={this.props.openDrawer}>
              <IconNB active name="magnify" />
            </Button>
          </Right>
        </Header>

        <Container>
          { this.props.loading &&
            <ActivityIndicator style={styles.indicator} animating={this.props.loading}/>
          }
          { !this.props.loading && this.props.error &&
            <Text style={styles.error}>{this.props.error}</Text>
          }

          { (!this.props.loading && this.props.list && this.props.list.length > 0) &&
              <FlatList
                style={styles.row}
                data={this.props.list}
                keyExtractor={(item, index) => item.id + '_' + index}
                renderItem={({item}) => <ProjectCard item={item}/>}
              />
          }



        </Container>
      </Container>
    );
  }
}

function bindAction(dispatch) {
  return {
    openDrawer: () => dispatch(openDrawer()),
    loadProjects: () => dispatch(loadProjects())
  };
}

const mapStateToProps = state => ({
  list: state.project.list,
  currentProject: state.project.current,
  loading: state.project.loading,
  error: state.project.error
});


export default connect(mapStateToProps, bindAction)(Projects);
