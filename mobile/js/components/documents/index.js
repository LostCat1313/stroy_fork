
import React, { Component } from 'react';
import { TouchableOpacity, FlatList, ListItem } from 'react-native';
import { connect } from 'react-redux';
import { Actions, ActionConst } from 'react-native-router-flux';
import { Container, Header, Title, Content, Text, Button, Icon, IconNB, Card, CardItem, Left, Body, Right, Segment, Fab } from 'native-base';
import { Grid, Row } from 'react-native-easy-grid';

import Notification from '../common/Notification';
import FolderList from '../common/FolderList';
import ActionMenu from '../common/ActionMenu';

// import { setIndex } from '../../actions/list';
import styles from './styles';

class Documents extends Component {

  static propTypes = {
    name: React.PropTypes.string,
  }

  constructor(props) {
    super(props);
    this.state = {
      seg: 1
    };
  }

  // newPage(index) {
  //   this.props.setIndex(index);
  //   Actions.blankPage();
  // }

  render() {

    var title = this.props.currentProject ? 'Лента проекта' : 'Лента' ;

    return (
      <Container style={styles.container}>
        <Header stretched>
          <Left>
            <Button transparent onPress={() => Actions.refresh({key: 'drawer', open: true })}>
              <IconNB active name="menu" />
            </Button>
          </Left>

          <Body style={{alignItems: 'flex-start'}}>
            <Title >Документы</Title>
          </Body>

          <Right>
            <Button transparent onPress={() => Actions.refresh({key: 'drawer', open: true })}>
              <IconNB active name="magnify" />
            </Button>
          </Right>
        </Header>

        <Container>
            <FolderList items={this.props.folderList}/>
        </Container>

        <ActionMenu />
      </Container>
    );
  }
}

function bindAction(dispatch) {
  return {
    
  };
}

const mapStateToProps = state => ({
  name: state.user.name,
  folderList: state.folders.list,
  currentProject: state.project.current
});

export default connect(mapStateToProps, bindAction)(Documents);
