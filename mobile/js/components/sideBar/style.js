import theme from '../../themes/base-theme';

const React = require('react-native');

const { StyleSheet, Platform, Dimensions } = React;

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;


export default {
  sidebar: {
    flex: 1,
    padding: 0,
    paddingRight: 0,
    paddingTop: 0,
    backgroundColor: '#fff',
  },
  drawerCover: {
    alignSelf: 'stretch',
    // resizeMode: 'cover',
    height: deviceHeight / 3.5,
    width: null,
    position: 'relative',
    marginBottom: 10,
  },
  drawerImage: {
    position: 'absolute',
    // left: (Platform.OS === 'android') ? 30 : 40,
    left: (Platform.OS === 'android') ? deviceWidth / 10 : deviceWidth / 9,
    // top: (Platform.OS === 'android') ? 45 : 55,
    top: (Platform.OS === 'android') ? deviceHeight / 13 : deviceHeight / 12,
    width: 210,
    height: 75,
    resizeMode: 'cover',
  },
  infoBottom: {
    backgroundColor: theme.mainBackgroundColorAlpha(0.6),
    paddingRight: 25,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  infoBottomText: {
    fontSize: theme.titleFontSize
  },
  infoUserName: {
    // backgroundColor: '#445500',
    flex: 1.5,
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'flex-start',
    marginBottom: -3,
    paddingRight: 15
  },
  infoUserNameText: {
    fontSize: theme.subTitleFontSize,
    backgroundColor: 'rgba(0,0,0,0)'
  },
  infoLogo: {
    // backgroundColor: '#440000',
    alignItems: 'center',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  infoLogoImage: {
    flex: 1,
    resizeMode: 'contain'
  }
};
