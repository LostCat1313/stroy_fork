
import React, { Component } from 'react';
import { Image } from 'react-native';
import { connect } from 'react-redux';
import { Container, Content, Text, ListItem, List, Left, Right, Icon, IconNB, Badge, Button } from 'native-base';
import { Actions, ActionConst } from 'react-native-router-flux';
import { Col, Row, Grid } from 'react-native-easy-grid';

import { reset } from '../../actions/user';

import styles from './style';

const drawerCover = require('../../../images/bg01.png');
const drawerImage = require('../../../images/logo_draw@2x.png');

const datas = [
  {
    name: 'Лента',
    route: 'home',
    icon: 'format-list-bulleted',
  },
  {
    name: 'Проекты',
    route: 'projects',
    icon: 'box-shadow',
    bg: '#ccc',
    types: '2',
  },
  {
    name: 'Документы',
    route: 'documents',
    icon: 'file',
    bg: '#477EEA',
    types: '8',
  },
  {
    name: 'Замечания',
    route: 'blankPage',
    icon: 'note',
    bg: '#00de12',
    types: '8',
  },
  {
    name: 'Личные заметки',
    route: 'blankPage',
    icon: 'star',
  },
  {
    name: 'Чаты',
    route: 'blankPage',
    icon: 'message',
  },
  {
    name: 'Контакты',
    route: 'contacts',
    icon: 'account-multiple',
  },
  {
    name: 'Настройки',
    route: 'blankPage',
    icon: 'settings',
  },
  {
    name: 'Выйти',
    icon: 'exit-to-app',
    action: (dispatch) => {
      console.log("Reset authentication");
      dispatch(reset());
    }
  },
];

class SideBar extends Component {

  static contextTypes = {
    drawer: React.PropTypes.object,
  };

  closeDrawer() {
    this.context.drawer.close();
  }

  render() {
    return (
      <Container style={styles.sidebar} >
        <Image source={drawerCover} style={styles.drawerCover}>
            <Grid>
              <Row size={2}>
                <Col>
                  <Row size={1}></Row>
                  <Row size={1}>
                    <Col size={1} style={styles.infoLogo}>
                      <Image style={styles.infoLogoImage} resizeMethod={'scale'} source={drawerImage}/>
                    </Col>
                    <Col size={2}  style={styles.infoUserName}>
                      <Text style={styles.infoUserNameText}>{this.props.currentUser}</Text>
                    </Col>
                  </Row>
                  <Row size={1}></Row>
                </Col>
              </Row>
              <Row size={1} style={styles.infoBottom}>
                <Text style={styles.infoBottomText}>{this.props.currentProject ? this.props.currentProject.title : ''}</Text>
              </Row>
            </Grid>
        </Image>
        <List
          dataArray={datas}
          style={{marginTop: -10}}
          renderRow={data =>
            <ListItem button noBorder onPress={e => {
              if (data.action){
                data.action(this.props.dispatch);
              } else {
                Actions[data.route]({ ...data.custom, title: data.name });
                this.closeDrawer();
              }}} >
              <Left>
                <IconNB active name={data.icon} style={{ color: '#777', fontSize: 26, width: 30 }} />
                <Text>{data.name}</Text>
              </Left>
              {(data.types) &&
              <Right style={{ flex: 0 }}>
                <Badge
                  style={{ borderRadius: 3, height: 25, width: null, backgroundColor: data.bg }}
                >
                  <Text>{`${data.types}`}</Text>
                </Badge>
              </Right>
              }
            </ListItem>}
          >
        </List>
      </Container>
    );
  }
}


const mapStateToProps = state => ({
  currentProject: state.project.current,
  currentUser: state.user.name
});


export default connect(mapStateToProps)(SideBar);
