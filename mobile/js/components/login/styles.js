import theme from '../../themes/base-theme';
import { Platform, Dimensions, PixelRatio } from 'react-native';

const platform = Platform.OS;
const React = require('react-native');
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

export default {
    container: {
        backgroundColor: 'rgb(40,182,246)',
    },
    descriptionBlock: {
        width: deviceWidth,
        height: deviceHeight * 0.3
    },
    descriptionTextContainer: {
        width: deviceWidth,
        height: deviceHeight * 0.3,
        backgroundColor: 'rgba(40,182,246,0.5)'
    },
    descriptionText: {
        color: 'white',
        fontSize: 10,
        padding: 10,
        paddingLeft: 20,
        backgroundColor: 'rgba(0,0,0,0)',
        marginTop: (platform === 'ios') ? 20 : 0
    },
    logo: {
        width: deviceWidth,
        resizeMode: 'contain',
        backgroundColor: '#ffffff',
        height: deviceHeight * 0.25,
    },
    controls: {
        width: deviceWidth,
        flex: 1,
        flexDirection: 'column',
        paddingTop: 30,
        alignItems: 'center',
        backgroundColor: 'rgb(40,182,246)'
    },
    input: {
        padding: 5,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#ffffff',
        height: 30,
        width: 200
    },
};
