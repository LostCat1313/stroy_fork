
import React, { Component } from 'react';
import { ActivityIndicator, Image, StatusBar, TextInput } from 'react-native';
import { connect } from 'react-redux';
import { Container, Content, Button, View, Text } from 'native-base';
import { Actions } from 'react-native-router-flux';
import { login } from '../../actions/user';
import styles from './styles';
import theme from '../../themes/base-theme';

const logo = require('../../../images/logo_login@2x.png');
const background = require('../../../images/bg01.png');

class Login extends Component {

    static propTypes = {
        login: React.PropTypes.func,
    }

    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: ''
        };
    }

    login() {
        this.props.login(this.state.username, this.state.password)
    }

    render() {
        return (
            <Container style={styles.container}>
                <Content bounces={false}>

                    <StatusBar backgroundColor={theme.toolbarDefaultBg} barStyle='light-content'/>

                    <Image source={background} style={styles.descriptionBlock}>
                        <View style={styles.descriptionTextContainer}>
                            <Text style={styles.descriptionText}>Версия 0.1</Text>
                        </View>
                    </Image>

                    <Image source={logo} style={styles.logo} />

                    <View style={styles.controls}>
                        <TextInput
                            style={styles.input}
                            placeholder="Логин"
                            placeholderTextColor='#ffffff'
                            underlineColorAndroid='transparent'
                            onChangeText={username => this.setState({ username })}
                        />
                        <View style={{height: 1.5, width: 200, backgroundColor: 'white'}}/>
                        <TextInput
                            style={styles.input}
                            secureTextEntry
                            placeholder="Пароль"
                            placeholderTextColor='#ffffff'
                            underlineColorAndroid='transparent'
                            onChangeText={password => this.setState({ password })}
                        />
                        <View style={{height: 1.5, width: 250, backgroundColor: 'white'}}/>

                        <Button block light transparent disabled={this.props.loading} onPress={() => this.login()}>
                            <Text>ВХОД</Text>
                        </Button>
                        <ActivityIndicator animating={this.props.loading}/>
                    </View>

                </Content>
            </Container>
        );
    }
}

function bindActions(dispatch) {
    return {
        login: (username, password) => dispatch(login(username, password)),
    };
}

const mapStateToProps = store => ({
  loading: store.user.loading,
  // index: state.list.selectedIndex,
  // list: state.list.list,
});

export default connect(mapStateToProps, bindActions)(Login);
