import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Image, View, Thumbnail } from 'react-native';
import { Card, CardItem, Header, Icon, IconNB, Left, Right, Body, Text } from 'native-base';
import { Actions, ActionConst } from 'react-native-router-flux';
import { Grid, Row, Col } from 'react-native-easy-grid';
import styles from './styles';

class ContactItem extends Component {

  static propTypes = {
    item: React.PropTypes.object,
  }

  _onPress = () => {
    this.props.setCurrentProject(this.props.item.id);
  };

  render() {
    let thumb = null;

    console.log(this.props.item);

    if (this.props.item.hasThumbnail) {
        thumb = <Thumbnail source={{uri: '../../../images/bg01.png'}} />;
    } else {
        thumb = <View style={styles.thumb}>

        </View>;
    }

    console.log("1");

    return (
        <View style={{flex: 1, flexDirection: 'row'}}>
            {thumb}
            <Text>{this.props.item.firstName}</Text>
        </View>
    );
  }
}

function bindAction(dispatch) {
  return {
    // setCurrentProject: (id) => dispatch(setCurrentProject(id)),
  };
}

export default connect(null, bindAction)(ContactItem);
