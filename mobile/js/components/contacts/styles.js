const React = require('react-native');

const { StyleSheet } = React;
export default {
  container: {
    backgroundColor: '#FBFAFA',
  },
  titleContainer: {
      flex: 1,
      flexDirection: 'column',
      justifyContent: 'center',
    //   alignItems: 'center',
  },
  title: {
    fontSize: 16
  },
  subtitle: {
    fontSize: 16,
    color: 'grey'
  },
  mt: {
    marginTop: 7,
  },
  row: {
    paddingLeft: 7,
    paddingRight: 7
  },
  indicator: {

  },
  error: {
    color:'#606060', fontSize: 12, textAlign: 'center'
  },
  thumb: {
      height: 40,
      width: 40,
      borderRadius: 20,
      alignItems: 'center',
      justifyContent: 'center'
  },
  thumbContainer: {
      marginTop: 10,
      marginBottom: 20,
      marginLeft: 20,
      marginRight: 25,
      alignItems: 'center'
  },
  thumbText: {
      color: 'white',
      fontSize: 22,
      fontFamily: 'Roboto',
      backgroundColor: 'transparent'
  }
};
