import React, { Component } from 'react';
import { FlatList, ActivityIndicator, View, TouchableOpacity, Image } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { Container, Header, Title, Content, Text, Button, Icon, Card, CardItem, Left, Body, Right, Segment, Thumbnail } from 'native-base';
import { Grid, Row, Col } from 'react-native-easy-grid';

import { loadContacts, loadProjectContacts } from '../../actions/contacts';
import styles from './styles';

class Contacts extends Component {
    static propTypes = {
        name: React.PropTypes.string,
        list: React.PropTypes.arrayOf(React.PropTypes.object),
        // openDrawer: React.PropTypes.func,
    }

    componentDidMount() {
        console.log('========contacts did mount=======');
        this._load(this.props.currentProject);
    }

    componentWillUpdate(nextProps, nextState) {
        if (nextProps.refresh) {
            this._load(nextProps.currentProject);
        }
    }

    _onPressItem = (item) => {
        alert(item.name);
    };

    _load(project) {
        console.log('=======Contacts loading start========');
        if (project) {
            this.props.loadProjectContacts(project.id);
        } else {
            this.props.loadContacts();
        }
    }

    generateColor () {
        return '#' +  Math.random().toString(16).substr(-6);
    }

    _getContactItem = (item) => {
        let thumb = null;
        let contact = item.item;
        if (contact.hasThumbnail) {
            img = require('../../../images/logo.png');
            thumb = <Image style={styles.thumb} source={img} />;
        } else {
            str = contact.name[0] + contact.name[0];
            thumb = <View style={{...styles.thumb, backgroundColor: this.generateColor()}}>
                <Text style={styles.thumbText}>{str}</Text>
            </View>;
        }

        return (
            <TouchableOpacity
                style={{flex: 1, flexDirection: 'row'}}
                onPress={() => this._onPressItem(contact)}>

                <View style={styles.thumbContainer}>
                    {thumb}
                </View>
                <View style={styles.titleContainer}>
                    <Text style={styles.title}>{contact.name} {contact.name}</Text>
                    <Text style={styles.subtitle}>{contact.position}</Text>
                    <View style={{height:1, backgroundColor: 'grey', marginTop: 10}}/>
                </View>

            </TouchableOpacity>
        );
    }

    render() {
        return (
            <Container style={styles.container}>
                <Header stretched>
                    <Left>
                        <Button transparent onPress={() => Actions.refresh({key: 'drawer', open: true })}>
                        <Icon active name="menu" />
                        </Button>
                    </Left>

                    <Body style={{alignItems: 'flex-start'}}>
                        <Title>Контакты</Title>
                    </Body>

                    <Right>
                        {/* <Button transparent onPress={this.props.openDrawer}>
                        <Icon name="ios-search" />
                        </Button> */}
                    </Right>
                </Header>

                <Container>
                    { this.props.loading &&
                    <ActivityIndicator style={styles.indicator} animating={this.props.loading}/>
                    }
                    { !this.props.loading && this.props.error &&
                    <Text style={styles.error}>{this.props.error}</Text>
                    }

                    { (!this.props.loading && this.props.list && this.props.list.length > 0) &&
                    <FlatList
                        style={styles.row}
                        data={this.props.list}
                        keyExtractor={(item, index) => item.id + ' ' + index }
                        renderItem={this._getContactItem}
                    />
                    } 
                </Container>
            </Container>
        );
    }
}

function bindAction(dispatch) {
    return {
        // openDrawer: () => dispatch(openDrawer()),
        loadContacts: () => dispatch(loadContacts()),
        loadProjectContacts: (projectId) => dispatch(loadProjectContacts(projectId))
    };
}

const mapStateToProps = state => ({
    currentProject: state.project.current,
    list: state.contacts.list,
    loading: state.contacts.loading,
    error: state.contacts.error,
    refresh: state.contacts.refresh,
});

export default connect(mapStateToProps, bindAction)(Contacts);
