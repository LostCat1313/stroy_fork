
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { Container, Header, Title, Content, Text, Button, Icon, IconNB, Left, Right, Body } from 'native-base';

import styles from './styles';

class BlankPage extends Component {

  static propTypes = {
  }

  render() {
    const { props: { title, index, list } } = this;

    return (
      <Container style={styles.container}>
        <Header stretched>
          <Left>
            <Button transparent onPress={() => Actions.refresh({key: 'drawer', open: true })}>
              <Icon active name="menu" />
            </Button>
          </Left>

          <Body style={{alignItems: 'flex-start'}}>
            <Title>{(title) ? this.props.title : 'Blank Page'}</Title>
          </Body>

          <Right>
            {/*<Button transparent onPress={this.props.openDrawer}>
              <Icon name="ios-search" />
            </Button>*/}
          </Right>
        </Header>

        <Content padder>
          <Text>test
            
          </Text>
        </Content>
      </Container>
    );
  }
}
{/* (!isNaN(index)) ? list[index] : 'Create Something Awesome . . .'*/ }
function bindAction(dispatch) {
  return {
  };
}

const mapStateToProps = state => ({
});


export default connect(mapStateToProps, bindAction)(BlankPage);
