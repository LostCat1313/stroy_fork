
import React, { Component } from 'react';
import { TouchableOpacity, FlatList, ListItem } from 'react-native';
import { connect } from 'react-redux';
import { Actions, ActionConst } from 'react-native-router-flux';
import { Container, Header, Title, Content, Text, Button, Icon, IconNB, Card, CardItem, Left, Body, Right, Segment, Fab } from 'native-base';
import { Grid, Row } from 'react-native-easy-grid';

import Notification from '../common/Notification';
import FolderList from '../common/FolderList';
import ActionMenu from '../common/ActionMenu';
import NotificationsList from '../common/NotificationsList';
import LastDocumentsList from '../common/LastDocumentsList';

// import { setIndex } from '../../actions/list';
import styles from './styles';

class Home extends Component {

  static propTypes = {
    name: React.PropTypes.string,
  }

  constructor(props) {
    super(props);
    this.state = {
      seg: 1
    };
  }

  // newPage(index) {
  //   this.props.setIndex(index);
  //   Actions.blankPage();
  // }

  render() {

    var title = this.props.currentProject ? 'Лента проекта' : 'Лента' ;

    return (
      <Container style={styles.container}>
        <Header stretched>
          <Left>
            <Button transparent onPress={() => Actions.refresh({key: 'drawer', open: true })}>
              <IconNB active name="menu" />
            </Button>
          </Left>
          <Body>
            <Segment>
                <Button
                  active={(this.state.seg === 1) ? true : false}
                  onPress={() => this.setState({seg: 1})}>
                  <Text>{title}</Text>
                </Button>
                <Button
                  active={(this.state.seg === 2) ? true : false}
                  onPress={() => this.setState({seg: 2})}
                  >
                  <Text>Документы</Text>
                </Button>
            </Segment>
          </Body>
          <Right>
            <Button transparent onPress={() => Actions.refresh({key: 'drawer', open: true })}>
              <IconNB active name="magnify" />
            </Button>
          </Right>
        </Header>

        <Container>
        {(this.state.seg === 1) && <NotificationsList/> }

        {(this.state.seg === 2) && <LastDocumentsList/>}
        </Container>

        <ActionMenu />
      </Container>
    );
  }
}

function bindAction(dispatch) {
  return {
    
  };
}

const mapStateToProps = state => ({
  name: state.user.name,
  currentProject: state.project.current
});

export default connect(mapStateToProps, bindAction)(Home);
