import React, { Component } from 'react';
import Drawer from 'react-native-drawer';
import SideBar from './components/sideBar';
import {Actions, DefaultRenderer} from 'react-native-router-flux';

export default class NavigationDrawer extends Component {
    render(){
        const state = this.props.navigationState;
        const children = state.children;
        return (
            <Drawer
                ref="navigation"
                open={state.open}
                onOpen={()=>Actions.refresh({key:state.key, open: true})}
                onClose={()=>Actions.refresh({key:state.key, open: false})}
                type="overlay"
                content={<SideBar />}
                tapToClose={true}
                openDrawerOffset={0.2}
                panCloseMask={0.2}
                panOpenMask={0.2}
                negotiatePan={true}
                tweenDuration={150}
                styles={{
                    drawer: {
                      shadowColor: '#000000',
                      shadowOpacity: 0,
                      shadowRadius: 0,
                      elevation: 5
                    },
                    mainOverlay: {
                      opacity: 0,
                      backgroundColor: 'rgba(0, 0, 0, 0.8)',
                      elevation: 8
                    }
                }}
                tweenHandler={(ratio) => {
                    return {
                      drawer: { 
                        shadowRadius: ratio < 0.2 ? ratio * 5 * 5 : 5,
                        shadowOpacity: ratio * 0.3,
                      },
                      mainOverlay: {
                        opacity: ratio / 2,
                      },
                    };
                }}>
                <DefaultRenderer navigationState={children[0]} onNavigate={this.props.onNavigate} />
            </Drawer>
        );
    }
}
