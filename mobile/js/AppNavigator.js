
import React, { Component } from 'react';
import { StatusBar, View } from 'react-native';
import { connect } from 'react-redux';
import { Actions, Router, Scene, Reducer, Switch } from 'react-native-router-flux';

import Login from './components/login/';
import Home from './components/home/';
import Documents from './components/documents/';
import BlankPage from './components/blankPage';
import NavigationDrawer from './NavigationDrawer';
import Projects from './components/projects';
import Contacts from './components/contacts';
import { statusBarColor } from './themes/base-theme';
import { restoreSession } from './actions/user';


const RouterWithRedux = connect()(Router);

class AppNavigator extends Component {

  static propTypes = {
    // isAuthenticated: React.PropTypes.boolean,
    restoreSession: React.PropTypes.func,
  }

  componentWillMount() {
    console.log("======AppNavigator will mount======", this.props);
  }

  render() {

    const component = connect(state => ({
      token: state.user.token,
      initializing: state.user.initializing,
      error: state.user.error
    }))(Switch);

    return (
      <RouterWithRedux>
        <Scene
          key="root"
          tabs
          unmountScenes
          component={component}
          selector={ props => {
            console.log("=====root selector=====", props.token, props.refresh);
            if (props.error) {
              alert(props.error);
            }
            if (props.initializing) {
              return 'loading';
            }
            return props.token ? 'drawer' : 'authenticate';
          }}>
          <Scene key="loading" panHandlers={null} component={View} hideNavBar />
          <Scene key="authenticate" panHandlers={null} component={Login} hideNavBar />
          <Scene key="drawer" component={NavigationDrawer}>
            <Scene
              key="main"
              tabs
            >
              <Scene panHandlers={null} key="home" hideNavBar component={Home} />
              <Scene panHandlers={null} key="documents" hideNavBar component={Documents} />
              <Scene panHandlers={null} key="blankPage" hideNavBar component={BlankPage} />
              <Scene panHandlers={null} key="projects" hideNavBar component={Projects} />
              <Scene panHandlers={null} key="contacts" hideNavBar component={Contacts} />
            </Scene>
          </Scene>
        </Scene>
      </RouterWithRedux>
    );
  }
}


function bindAction(dispatch) {
  return {

  };
}

const mapStateToProps = state => ({
  currentProject: state.project.current
});

export default connect(mapStateToProps, bindAction)(AppNavigator);
