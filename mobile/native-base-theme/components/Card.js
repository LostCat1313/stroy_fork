import { Platform } from 'react-native';
import _ from 'lodash';

import variable from './../variables/platform';

function generateNotificationStyle(variables, color) {
  return {
   // borderTopWidth: 4,
    //borderTopColor: color,
    overflow: 'hidden',
    'NativeBase.CardItem': {
       '.header': {
        borderTopWidth: 4,
        borderTopColor: color,
        paddingBottom: 0,
        paddingTop: 10,
        'NativeBase.Left': {
          marginRight: 10,
          marginTop: 2,
          flex: 0,
          alignSelf: 'flex-start',
          'NativeBase.IconNB': {
            fontSize: variables.iconSizeSmall,
            color: color,
          },
          'NativeBase.Icon': {
            fontSize: variables.iconSizeSmall,
            color: color,
          }
        },
        'NativeBase.Body': {
          'NativeBase.Text': {
            color: '#606060',
            fontSize: 9,
            lineHeight: 12,
            '.headerTitle': {
              fontSize: 16,
              lineHeight: 20
            }
          }
        },
        'NativeBase.Right': {
          marginLeft: 10,
          marginTop: 2,
          flex: 0,
          alignSelf: 'flex-start',
          'NativeBase.IconNB': {
            fontSize: variables.iconSizeSmall,
            color: '#606060',
          },
          'NativeBase.Icon': {
            fontSize: variables.iconSizeSmall,
            color: '#606060',
          }
        }
      },
      '.content': {
        paddingTop: 5
      }
    }
  }
}

export default (variables = variable) => {
  const cardTheme = {
      '.transparent': {
        shadowColor: null,
        shadowOffset: null,
        shadowOpacity: null,
        shadowRadius: null,
        elevation: null,
      },
      marginVertical: 5,
      marginHorizontal: 2,
      flex: 1,
      borderWidth: variables.borderWidth,
      borderRadius: 2,
      borderColor: variables.cardBorderColor,
      flexWrap: 'wrap',
      backgroundColor: variables.cardDefaultBg,
      shadowColor: '#000',
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 0.1,
      shadowRadius: 1.5,
      elevation: 3,
      '.fileNotification': generateNotificationStyle(variables, '#FF9800'),
      '.messageNotification': generateNotificationStyle(variables, '#4CAF50'),
      '.remarkNotification': generateNotificationStyle(variables,  '#8BC34A'),
      '.projectCard': generateNotificationStyle(variables,  '#ccc'),
  };

  return cardTheme;
};
