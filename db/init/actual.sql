select actual.id from (SELECT 
	DISTINCT ON (files."pageNumber") files."pageNumber",
	files.id 
FROM "Files" as files
	INNER JOIN "DocumentVersions" as versions
	  ON versions.id = files."docVersionId"
	WHERE "docId" = 88
	ORDER BY files."pageNumber", versions."createdAt") actual