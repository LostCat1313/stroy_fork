--select * from "Documents" where "Documents"."id" in (
WITH RECURSIVE documents AS (
   SELECT id, "parentId", "projectId", "isFolder", title
   FROM "Documents"
   WHERE "parentId" IS NULL

   UNION

   SELECT children.id, children."parentId", children."projectId", children."isFolder", children.title
   FROM "Documents" as children
      JOIN documents
          ON children."parentId" = documents.id
)

SELECT COUNT(*) FROM documents
--	INNER JOIN "Projects" as project
  --  	ON project.id = documents."projectId"
			INNER JOIN "ProjectUser" as projectUser
				ON projectUser."ProjectId" = documents."projectId"
        	WHERE documents."isFolder" = FALSE 
				AND projectUser."UserId" = 1;
			
	--);