# Docker deploy to maintenance.dev

cd web
./prepare-docker-db.sh  # if want to update db
./prepare-docker-web.sh # compile and prepare web

docker-compose build
docker-compose push


# Docker hint

```
#run image
docker run --name <name> -d <repo> /bin/bash
docker run --name libreoffice -d xcgd/libreoffice

-d              : detached
-a=[]           : Attach to `STDIN`, `STDOUT` and/or `STDERR`
-t              : Allocate a pseudo-tty
--sig-proxy=true: Proxy all received signals to the process (non-TTY mode only)
-i              : Keep STDIN open even if not attached
-v              : virtual dir, like: /host/directory:/container/directory

#stop/remove all containers
docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)

#attach bash
docker exec -i -t libreoffice /bin/bash
docker run -it ... (pseudo terminal and interactive)
docker 

docker run -it --name sconv -v ~/Downloads/:/mnt/store stroy-convert
```

# Libre office conversion

```
libreoffice --headless -env:UserInstallation=file://${tmp} --convert-to html ${myfile}
```

# Run client

webpack is required

```
cd client 
npm install
npm start
```

# Run server

typescript is required

```
cd server
npm install
npm start
```

# Ant

todo
 - deploy
 - update store
 - run

