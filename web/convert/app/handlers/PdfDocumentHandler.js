const BaseHandler = require('./BaseHandler')
const path = require('path')
const uuid = require('uuid/v4');
const fs = require('fs')
const mv = require('mv')
const utils = require('../utils')

class PdfDocumenHandler extends BaseHandler {
  constructor(task) {
    super(task)
    this.pdfPageNumbersCommandTemplate = 'pdfinfo {filename} | grep "^Pages"';
    this.pdfConvertCommandTemplate = 'convert -density 300 -background white -alpha remove -quality 75 {filename}[{page}] page-{page}.jpg'
  }

  formatPdfConvertCommand(filename, page) {
    return this.pdfConvertCommandTemplate
      .replace('{filename}', filename)
      .replace(/\{page\}/g, page)
  }

  formatPdfPageNumbersCommand(filename) {
    return this.pdfPageNumbersCommandTemplate
      .replace('{filename}', filename)
  }

  getNumberOfPages(filename) {
    return new Promise((resolve, reject) => {
      const command = this.formatPdfPageNumbersCommand(filename)
      console.log("Exec: " + command)
      this.exec(command, (error, stdout, stderr) => {
        if(error) return reject(error)
        var pagesNumber = parseInt(stdout.split(':')[1])
        console.log("Pages number: " + pagesNumber)
        resolve(pagesNumber)
      })  
    })
  }

  convertPage(filename, page, tmpPath) {
    return new Promise((resolve, reject) => {
      const command = this.formatPdfConvertCommand(filename, page)
      this.exec(command, { cwd: tmpPath }, (error, stdout, stderr) => {
          if (error) return reject(error)
          resolve(page + ' ok')
        })
    })
  }

  handle() {
    console.log('Handling pdf document...')
    // generating page images with imagemagic
    return new Promise((resolve, reject) => {
      // TODO: make it safe -> verify data
      this.getDocument().then(doc => {
        if(!doc) return reject('Document not found')
        var filename = this.formatPath(doc.file, '.pdf')
        
        if(!fs.existsSync(filename)) return reject('Document has no pdf file ' + filename)

        this.getNumberOfPages(filename).then(number => {
          let convertPromises = []
          let cwd = path.join(this.tmpPath, doc.file.fid)

          if(!fs.existsSync(cwd)) {
            fs.mkdirSync(cwd)
          }
          
          var i = 0;
          var log = ''

          let errorHandler = (error) => {
            log += error
            i++
            if(i < number) runConversion()
          }

          let runConversion = () => {
            this.convertPage(filename, i, cwd).then(() => {
              this.registerPage(doc, cwd, i).then(() => {
                i++
                if(i < number) runConversion()
                else  {
                  // generate thumbnail for the document
                  this.db.Task.create({
                    docId: doc.documentId,
                    refId: doc.id,
                    type: 3,
                    status: 1
                  });
                  resolve('Ok. ' + number)
                }
              }, errorHandler)
            }, errorHandler)
          }
          runConversion() 
        }, error => {
          reject(error)
        })
      }, (error) => reject('Failed to get document'))
    })
  }




  registerPage(doc, tmpPath, pageNumber) {
    return new Promise((resolve, reject) => {
      let pageTemplate = 'page-{page}.jpg'
      let promises = []
      let pageName = pageTemplate.replace('{page}', pageNumber)
      let pagePath = path.join(tmpPath, pageName)
      // IFile
      let pageEntry = {
        fid: uuid(),
        original: pageName,
        type: 0,
        pageNumber: pageNumber + 1,
        actual: true,
        docId: doc.documentId,
        docVersionId: doc.id,
        createdBy: 1, // TODO: by user created task
        modifiedBy: 1
      }
      let registeredPagePath = this.formatPath(pageEntry)
      // TODO: mkdir for registered page path
      console.log('Moving ' + pagePath + ' to ' + registeredPagePath)

      utils.createDir(path.dirname(registeredPagePath))

      mv(pagePath, registeredPagePath, (err) => {
        if (err) {
          console.log(err.code + ' failed to move file')
          return reject(err.code)
        }
        console.log('Creating db entry')
        this.db.File.create(pageEntry).then((res) => {
          // generate thumbnails for pages
          this.db.Task.create({
            docId: doc.id,
            refId: res.id,
            type: 4,
            status: 1
          });

          resolve(res)
        }, () => {
          reject('Failed to create db entry for ' + pageEntry + ' doc.id = ' + doc.id)
        })
      })
    })
  }
}

module.exports = {
  handler: (task) => {
    return new PdfDocumenHandler(task)
  },
  PdfDocumenHandler
}