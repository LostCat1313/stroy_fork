
module.exports = {
  1: require("./PdfDocumentHandler").handler,
  2: require("./LibreOfficeDocumentHandler").handler,
  3: require("./ThumbnailHandler").docHandler,
  4: require("./ThumbnailHandler").fileHandler
}