var PdfDocumentHandler = require('./PdfDocumentHandler').PdfDocumenHandler
const path = require('path')
const fs = require('fs')

class LibreOfficeDocumentHandler extends PdfDocumentHandler {
  constructor(task) {
    super(task)
    this.toPdfConvertCommandTemplate = 'libreoffice --convert-to pdf {filename} --headless'
  }

  formatConvertCommand(filename) {
    return this.toPdfConvertCommandTemplate
      .replace('{filename}', filename)
  }

  handle() {
    console.log('Handling libre office compatible document...')

    return new Promise((resolve, reject) => {
      // TODO: make it safe -> verify data
      this.getDocument().then(doc => {
        if(!doc) {
          return reject('Document not found')
        }

        const filename = this.formatPath(doc.file)

        if(!fs.existsSync(filename)) {
          return reject('Document has file valid ' + filename)
        }

        this.convert(filename).then(() => {
          // todo: add task to create pages for document
          // todo: verify
          this.db.Task.create({
            docId: doc.documentId,
            refId: doc.id,
            type: 1,
            status: 1
          }).then(() => {
            resolve('OK')
          }, () => {
            reject('Failed to create task to render pages')
          });

        }, (error) => {
          reject('Failed to convert document')
        });
      }, () => reject('Failed to get document. ' + error))
    });
  }

  convert(filename) {
    return new Promise((resolve, reject) => {
      const command = this.formatConvertCommand(filename)
      this.exec(command, { cwd: path.dirname(filename) }, (error, stdout, stderr) => {
        if (error) return reject(error)
        resolve(filename)
      })
    })
  }

  withExt(f) {
    let ext = path.extname(f.original);
    return f.fid + (ext ? ext : '')
  }
}

module.exports = {
  handler: (task) => {
    return new LibreOfficeDocumentHandler(task)
  }
} 