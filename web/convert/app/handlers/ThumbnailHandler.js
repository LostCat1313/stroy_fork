const path = require('path')
var BaseHandler = require('./BaseHandler')

class ThumbnailHandler extends BaseHandler {
  constructor(task, refType) {
    super(task)
    this.includePages = true;
    this.convertCommandTemplate = 'convert {source} -quality 75 -resize 500 {dest}'
    this.refType = refType
  }

  getFile() {
    return this.db.File.findOne({
      where: {
        id: this.task.refId
      }
    })
  }

  handle() {
    console.log('Generating image thumbnail...')
    return new Promise((resolve, reject) => {
      resolve(
        this.refType == 'doc' ?
        this.getDocument().then(doc => {
          if(!doc) return reject('Document not found')
          if(!doc.file) return reject('Document hasnt file')
          return this.handleDoc(doc)
        }) :
        this.getFile().then(file => {
          if(!file) return reject('File not found')
          if(file.type == 1) return reject('Wrong file type')
          return this.handleFile(file)
        })
      )
    })
  }

  handleDoc(doc) {
    return new Promise((resolve, reject) => {
      let filename = this.formatPath(doc.file)
      let source = null
      switch(doc.file.type) {
        case 1: 
          if(doc.pages.length == 0) return reject('Document should be multipage but has no pages')
          source = this.formatPath(doc.pages[0])
          break
        case 0:
          source = filename
          break
      }
      resolve(this.convert(path.dirname(filename), source, this.formatPath(doc.file, '.jpg', 'thumb_')))
    })
  }

  handleFile(file) {
    let filename = this.formatPath(file)
    return this.convert(path.dirname(filename), filename, this.formatPath(file, '.jpg', 'thumb_'))
  }

  formatConvertCommand(source, dest) {
    return this.convertCommandTemplate
      .replace('{source}', source)
      .replace('{dest}', dest)
  }

  convert(wd, source, dest) {
    return new Promise((resolve, reject) => {
      const command = this.formatConvertCommand(source, dest)
      this.exec(command, { cwd: wd }, (error, stdout, stderr) => {
        if (error) return reject(error)
        resolve(dest)
      })
    })
  }
}

module.exports = {
  docHandler: (task) => {
    return new ThumbnailHandler(task, 'doc')
  },
  fileHandler: (task) => {
    return new ThumbnailHandler(task, 'file')
  },
  ThumbnailHandler
} 
