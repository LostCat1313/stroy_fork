var path = require('path')
var config = require('../config');
var exec = require('child_process').exec

class BaseHandler {
  constructor(task) {
    this.task = task
    this.db = require('axiom-common-db').db
    this.storePath = config.storePath
    this.tmpPath = config.tmpPath
    this.includePages = false
  }
  // todo: add log logic 

  formatPath(entry, forceExt, prefix) {
    return entry.docId 
      ? path.join(this.storePath, 'documents', `${entry.docId}`, `${entry.type}`, (prefix ? prefix : "") + this.withExt(entry, forceExt))
      : path.join(this.storePath, `${entry.type}`, (prefix ? prefix : "") + this.withExt(entry, forceExt))
  }

  withExt(f, forceExt) {
    let ext = forceExt || path.extname(f.original);
    return f.fid + (ext ? ext : '')
  }

  exec(cmd, options, callback) {
    console.log('Executing ' + cmd + (options && options.cwd ? ' at ' + options.cwd : ''))
    exec(cmd, options, callback)
  }

  generateInclude() {
    let include = [{
      model: this.db.File,
      as: 'file'
    }]

    if(this.includePages) {
      include.push({
        model: this.db.File,
        as: 'pages'
      })
    }
    
    return include
  }

  getDocument() {
    let include = this.generateInclude();

    return this.db.DocumentVersion.findOne({
      where: {
        id: this.task.refId
      },
      include
    })
  }

}

module.exports = BaseHandler