const db = require('axiom-common-db').db
const handlers = require('./handlers')
const sys = require('sys')
const fs = require('fs')
const utils = require('./utils')
const config = require('./config')
const INTERVAL = 5000;
var checker = initialize() // run once per second 

utils.createDir(config.tmpPath)

function done(msg) {
  console.log(msg)
  checker = initialize()
}

function initialize() {
  console.log('Waiting...')  
  return setInterval(worker, INTERVAL)
}

function worker() {
  clearInterval(checker);
  db.Task.findOne({
    where: {
      status: 1
    }
  }).then(task => {
    if(!task) {
      done("There is no active tasks")
      return;
    }

    // TODO: make it async
    console.log('Task pending... ', task.toJSON())
    handle(task)
  }, reason => done);
}

// TASK HANDLING
function handle(task) {
  if(handlers[task.type] 
    && handlers[task.type]
    && typeof handlers[task.type] === 'function') {
    let handler = handlers[task.type](task)
    task.status = 2 // in progress
    task.save()
    handler.handle().then((msg) => {
      task.status = 3 // success
      task.log = msg
      task.save()
      done('OK')
    }, (error) => {
      console.log(error)      
      task.status = 0
      task.log = error
      task.save()
      done(error)
    })
  } else {
    task.status = 0 // error
    task.log = 'Task handler for type ' + task.type + ' not found'
    task.save()
    console.log(task.log)
    done(task.log)
  }
}