var config = require('../config/app.json')

var env       = process.env.NODE_ENV || 'development'

module.exports = config[env]