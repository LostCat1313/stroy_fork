const db = require('axiom-common-db').db;
const events = require('axiom-common-db').events;
var admin = require("firebase-admin");
var serviceAccount = require(__dirname + '/../config/axiom-fcm.json');

console.log("Initialization...");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://axiom-463e2.firebaseio.com"
});

console.log("Admin initialized...");

const INTERVAL = 3000;
var checker = initialize()

function done(msg) {
  console.log(msg)
  checker = initialize()
}

function initialize() {
  console.log('Waiting...')  
  return setInterval(worker, INTERVAL)
}

function worker() {
  clearInterval(checker);
  db.Notification.findAll({
    where: {
      status: 0
    },
    include: [
      {
        model: db.Event,
        include: events.types.include(db)
      },
      {model: db.RegistrationId, as: 'registration'},
    ]
  }).then(notifications => {
    if(!notifications || notifications.length == 0) {
      done("There is no notifications to send...")
      return;
    }
    console.log('Sending notifications... ', notifications.length)
    handle(notifications)
  }, done);
}

function handle(notifications) {
  // TODO: group by event id?
  Promise.all(notifications.map(n => {
    function errorHandler(error) {
      console.log('Error sending message:', error);
      n.status = 2;
      n.save();
      return n.toJSON();
    }

    if(!n.Event) {
      return errorHandler("notification.Event is not defined");
    }

    return events.format(n.Event.toJSON()).then((event) => {
      const payload = {
        notification: {
          title: event.title,
          body: event.content,
          sound: 'default',
          badge: '1'  // count of new messages
        },
        data: {
          event: JSON.stringify(event)
        }
      }
      
      if(!n.registration || !n.registration.regId) {
        return errorHandler("notification.registration || regId is not defined");
      }
      // todo: improve logging
      return admin.messaging().sendToDevice(n.registration.regId, payload)
        .then(response => {
          console.log('message sent results:', JSON.stringify(response));
          if(response.results.length > 0 && response.results[0].error) {
            return errorHandler("failed to send message " + JSON.stringify(response.results[0].error));
          }
          n.status = 1;
          n.save();
          return n.toJSON();
        })
        .catch(errorHandler);
    });
  }))
  .then(r => {
    let success = 0, failed = 0;
    r.forEach(i => {
      if(i.status == 1) success++;
      if(i.status == 2) failed++;
    });
    done("Result: success = " + success + ", failed = " + failed);
  })
  .catch(e => {
    done("Result with error: " + e);
  });
}


