rm ./db/demo/out.sql
rm ./db/demo/store.zip
echo ====DUMPING DB====
pg_dump stroy_db_devel --exclude-table="(\"SequelizeMeta\"|\"Tokens\")" --data-only >> ./db/demo/out.sql
echo ====ZIP STORE====
zip -r ./db/demo/store.zip ./.store