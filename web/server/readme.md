# TsExpressDecorators - Passport

Here an example with a Express Server using `Service` and `Passport.js`.

See [TsExpressDecorators](https://github.com/Romakita/ts-express-decorators) project for more information.

## Install

> **Important!** TsExpressDecorators requires Node >= 4, Express >= 4 and TypeScript >= 2.0.

```batch
npm install

npm run migrate
npm run seed
```

also helpful

```batch
./node_modules/.bin/sequelize db:migrate:undo --name filename
./node_modules/.bin/sequelize db:seed:undo --seed ListOfFiles
```

## Run
```
npm start
```

## ax1.cppdev.org
ssh -q -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no root@82.202.226.138


## remove all

delete from "ChatMessageAttachments";
delete from "ChatMessages";
delete from "ChatRoomUsers";
delete from "ChatRooms";
delete from "Descriptors";
delete from "DocumentVersions";
delete from "Documents";
delete from "Events";
delete from "Files";
delete from "Groups";
delete from "Notes";
delete from "Notifications";
delete from "OrganizationUser";
delete from "Organizations";
delete from "Positions";
delete from "ProjectUser";
delete from "Projects";
delete from "RegistrationIds";
delete from "Rights";
delete from "SequelizeMeta";
delete from "Tasks";
delete from "Tokens";
delete from "UserGroups";