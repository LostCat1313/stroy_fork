import {AuthenticatedMiddleware, EndpointInfo, EndpointMetadata, OverrideMiddleware, Req, Inject, PathParams, IMiddleware} from "@tsed/common";
import {Forbidden} from "ts-httpexceptions";
import {$log} from "ts-log-debug";

import { serverInstance } from '../../app';
import AccessControlService from "../../services/rights/AccessControlService";

@OverrideMiddleware(AuthenticatedMiddleware)
export class AuthMiddleware implements IMiddleware {
    constructor(
    ) {
    }

    use(@EndpointInfo() endpoint: EndpointMetadata,
        @Req() request: any,
        @PathParams('docId') docId: any) {
        const acs = serverInstance.injector.get<AccessControlService>(AccessControlService);
        //retrieve Options passed to the Authenticated() decorators.
        const options = endpoint.store.get(AuthenticatedMiddleware) || {};
        // $log.debug("AuthMiddleware =>", options);
        // $log.debug("AuthMiddleware rightsService initialized =>", !!acs);
        // $log.debug("AuthMiddleware rightsService initialized =>", docId);
        // $log.debug("AuthMiddleware isAuthenticated ? =>", request.isAuthenticated());

        const { entity, operation, role } = options;

        if(role === 'admin' && !acs.isAdmin()) {
          throw new Forbidden("Forbidden. User doesn't have admin status");
        }
        
        // if(entity && operation && !acs.check(entity, operation)) {
        //   throw new Forbidden("Forbidden. User doean't have permission for operation ")
        // }

        if(!request.isAuthenticated()) {
          throw new Forbidden("Forbidden");
        }
    }
}