import { NextFunction as ExpressNext, Request as ExpressRequest, Response as ExpressResponse } from "express";
import { Request, Response, Next, Err, Middleware, IMiddleware } from "@tsed/common";
import DBStorage from "../../services/DBStorage";
import { $log } from "ts-log-debug";

@Middleware()
export class DbMiddleware implements IMiddleware {

  constructor(
    private dbStorage:DBStorage
  ) {

  }

  use(
    @Request() request: ExpressRequest,
    @Next() next: ExpressNext
  ): any {
    const limit = request.query && request.query.limit || request.body && request.body.limit;
    const offset = request.query && request.query.offset || request.body && request.body.offset;

    this.dbStorage.limit = limit;
    this.dbStorage.offset = offset;

    $log.debug("DbMiddleware::use: limit", limit, "offset", offset);

    return next();
  }
}