import { NextFunction as ExpressNext, Request as ExpressRequest, Response as ExpressResponse } from "express";
import { IMiddlewareError, Request, Response, Next, Err, OverrideMiddleware, GlobalErrorHandlerMiddleware } from "@tsed/common";
import { $log } from "ts-log-debug";

@OverrideMiddleware(GlobalErrorHandlerMiddleware)
export class ErrorHandlerMiddleware implements IMiddlewareError {
    constructor() {
      
    }

    use(
        @Err() error: any,
        @Request() request: ExpressRequest,
        @Response() response: ExpressResponse,
        @Next() next: ExpressNext
    ): any {

        if (response.headersSent) {
            return next(error);
        }

        // const toHTML = (message = "") => message.replace(/\n/gi, "<br />");

        // if (error instanceof Exception) {
        //     $log.error("" + error);
        //     response.status(error.status).send(toHTML(error.message));
        //     return next();
        // }

        // if (typeof error === "string") {
        //     response.status(404).send(toHTML(error));
        //     return next();
        // }

        $log.error("ErrorHandlerMiddleware::use: " + error);
        response.status(200).json({
          success: false,
          error: { 
            status: error.status || 500,
            name: error.name,
            message: error.message,
            stack: error.stack // debug for
          }
        });

        return next();
    }
}