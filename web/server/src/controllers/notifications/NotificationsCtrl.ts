import {
  Controller, Authenticated, Get, PathParams, Post, BodyParams, Put, Delete, QueryParams
} from "@tsed/common";

import NotificationService from "../../services/NotificationsService";
import { events } from 'axiom-common-db';

@Controller('/notifications')
export default class NotificationsCtrl {

  constructor(
    private notificationService: NotificationService
  ) {
    
  }

  // get all
  @Get('/')
  @Authenticated()
  async index(): Promise<{ list: ax.INotification[] }> {
    const notifications = await this.notificationService.queryForCurrentUser();

    // console.log('GET NOTIFICATIONS RESULT => ', notifications.length);

    const list = await Promise.all(notifications.map(item => events.format(item.toJSON())));

    return { list };
  }
}