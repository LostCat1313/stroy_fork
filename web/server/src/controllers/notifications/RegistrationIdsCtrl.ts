import {
  Controller, Authenticated, Get, PathParams, Post, BodyParams, Put, Delete, QueryParams
} from "@tsed/common";

import RegistrationIdsService from "../../services/RegistrationIdsService";
import { events } from 'axiom-common-db';

@Controller('/regids')
export default class RegistrationIdsCtrl {

  constructor(
    private registrationIdsService: RegistrationIdsService
  ) {
    
  }

  @Get('/')
  @Authenticated()
  async index(): Promise<{ list: any[] }> {
    const list = await this.registrationIdsService.queryForCurrentUser();
    return { list };
  }

  @Post('/')
  @Authenticated()
  add(
    @BodyParams('entry') entry: ax.IRegistrationId
    ): Promise<ax.IRegistrationId> {
    return this.registrationIdsService.create({
      ...entry,
      deviceType: 0
    });
  }

  @Put('/:id')
  @Authenticated()
  update(
    @PathParams('id') id:number,
    @BodyParams('entry') entry: ax.IRegistrationId
    ) {
    return this.registrationIdsService.update(id, entry);
  }

  @Delete('/:id')
  @Authenticated()
  delete(
    @PathParams('id') id:number,
  ) {
    return this.registrationIdsService.delete(id);
  }

}