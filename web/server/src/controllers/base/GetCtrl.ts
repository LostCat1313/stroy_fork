import { Controller, All, Get, RouteService, Authenticated, RouterController } from "@tsed/common";
import BaseEntryService from '../../common/BaseEntryService';
import * as Express from 'express';

// @Controller('/')
export default class GetCtrl<T, ES extends BaseEntryService<T>> {

  constructor(
    private entriesService: BaseEntryService<T>
  ) {
  }

  @Get('/')
  @Authenticated()
  public index(): any {
    return this.entriesService.query().then(entries => {
        // console.log(`GET LIST FROM ${this.entriesService.constructor.name} =>`, entries.length);
      return {
        list: entries
      };
    });
  }
}