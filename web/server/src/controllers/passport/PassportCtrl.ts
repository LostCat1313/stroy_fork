import {NotFound} from "ts-httpexceptions";
import * as Express from "express";
import {
  Controller,
  Get,
  Post,
  BodyParams,
  QueryParams,
  Required,
  Request,
  Response,
  Next,
  Authenticated
} from "@tsed/common";
import PassportLocalService from '../../services/PassportLocalService';
import * as Passport from 'passport';
import { $log } from "ts-log-debug";
import UsersService from "../../services/UsersService";
import EventManager from "../../services/events/EventManager";
import RegistrationIdsService from "../../services/RegistrationIdsService";

export interface PassportRequest extends Express.Request {
    login(user, handler): any;
    logout();
    user: any;
}

@Controller("/authenticate")
export class PassportCtrl{

    constructor(
        private passportLocalService: PassportLocalService,
        private usersService: UsersService,
        private registrationIdService: RegistrationIdsService
    ) {
    }

    /**
     * Authenticate user with local info (in Database).
     * @param email
     * @param password
     * @param request
     * @param response
     */
    @Post('/')
    public login(
        @Required() @BodyParams('username') username:string,
        @Required() @BodyParams('password') password:string,
        @BodyParams('regId') regId: string,
        @BodyParams('deviceType') deviceType: number,
        @Request() request:PassportRequest,
        @Response() response:Express.Response
    ) {
        return new Promise<ax.IUser>((resolve, reject) => {
            Passport
                .authenticate('login', (err, user:ax.IUser) => {
                    if (err) {
                        reject(err);
                    }
                    request.login(user, (err) => {
                        if (err) {
                            reject(err);
                        }
                        resolve(user);
                    });
                })(request, response, () => {

                });
        })
            .catch((err) => {
                if(err && err.message === "Failed to serialize user into session") {
                    throw new NotFound('user not found');
                }
                return Promise.reject(err);
            });

    }

    /**
     * Try to register new account
     */
    @Get('/signup')
    public async signup(
        @QueryParams('phone') phone:string,
        @QueryParams('regId') regId: string,
        @QueryParams('deviceType') deviceType: number
    ) {
      const result = await this.usersService.register(phone);
      if(regId) {
        await this.registrationIdService.registerUser(result.id, regId, deviceType);
      }
      
      // EventManager.instance.emit(EventManager.ACTIVATION_CODE, {
      //   userId: result.id,
      //   code: result.activationCode
      // });

      return result;
    }

    @Get('/activate')
    public activate(
        @QueryParams('phone') phone:string,
        @QueryParams('code') code:string,
    ) {
      /// 1234 debug code
      return this.usersService.activate(phone, code);
    }

    /**
     * Disconnect user
     * @param request
     */
    @Get('/logout')
    public logout(
        @Request() request: PassportRequest
    ) {
        this.passportLocalService.logout(request);
        request.logout();
        return { status: "Disconnected" };
    }
}
