import {
  Controller, Authenticated, Get, PathParams, Post, BodyParams, Put, Delete, QueryParams
} from "@tsed/common";

import OrganizationsService from '../../services/OrganizationsService';

@Controller('/organizations')
export default class OrganizationsCtrl {
  constructor(
    private organizationsService: OrganizationsService
  ) {

  }

  @Get('/')
  index(): PromiseLike<{ list: ax.IOrganization[] }> {
    return this.organizationsService.query().then(organizations => {
      // console.log('GET ORGANIZATIONS RESULT => ', organizations.length);
      return {
        list: organizations
      };
    });
  }

  @Post('/')
  @Authenticated()
  add(
    @BodyParams('entry') organization: ax.IOrganization
    ): Promise<ax.IOrganization> {
    return this.organizationsService.create(organization);
  }


  @Put('/:id')
  @Authenticated()
  update(
    @PathParams('id') id:number,
    @BodyParams('entry') organization: ax.IOrganization
    ) {
    return this.organizationsService.update(id, organization);
  }

  @Delete('/:id')
  @Authenticated()
  delete(
    @PathParams('id') id:number,
  ) {
    return this.organizationsService.delete(id);
  }


  @Get('/:id/users')
  @Authenticated()
  getUsers(
    @PathParams('id') id:number,
  ) {
    return this.organizationsService.usersIndex(id);
  }

  @Put('/:id/users')
  @Authenticated()
  putUsers(
    @PathParams('id') id:number,
    @BodyParams('userIds') userIds: string[]
  ) {
    return this.organizationsService.updateUsers(id, userIds.map(id => parseInt(id)));
  }
}