import {
  Controller, Authenticated, Get, PathParams, Post, BodyParams, Put, Delete, QueryParams
} from "@tsed/common";

import PositionsService from '../../services/PositionsService';

@Controller('/positions')
export default class PositionsCtrl {
  constructor(
    private positionsService: PositionsService
  ) {

  }

  @Get('/')
  @Authenticated()
  index(): PromiseLike<{ list: ax.IPosition[] }> {
    return this.positionsService.query().then(positions => {
      // console.log('GET positionS RESULT => ', positions.length);
      return {
        list: positions
      };
    });
  }

  @Post('/')
  @Authenticated()
  add(
    @BodyParams('entry') position: ax.IPosition
    ): Promise<ax.IPosition> {
    return this.positionsService.create(position);
  }


  @Put('/:id')
  @Authenticated()
  update(
    @PathParams('id') id:number,
    @BodyParams('entry') position: ax.IPosition
    ) {
    return this.positionsService.update(id, position);
  }

  @Delete('/:id')
  @Authenticated()
  delete(
    @PathParams('id') id:number,
  ) {
    return this.positionsService.delete(id);
  }
}