import {
  Controller, Authenticated, Get, PathParams, Post, BodyParams, Put, Delete, Request
} from "@tsed/common";

import { $log } from "ts-log-debug";
import * as Express from "express";
// import * as Promise from 'bluebird';
import * as se from 'sequelize';
import { NotFound } from "ts-httpexceptions";
import PagesService from '../../services/PagesService';
import FileService from '../../services/FileService';
import DocumentVersionsService from "../../services/documents/DocumentVersionsService";

@Controller("/documents")
export default class DocumentPagesCtrl {

  constructor(
    private pagesService: PagesService,
    private fileService: FileService,
    private documentVersionsService: DocumentVersionsService
  ) { }

  @Get('/:docId/pages/')
  @Authenticated()
  async index(
    @PathParams('docId') docId: string
  ): Promise<{ list: ax.IOrganization[] }> {
    let pages = await this.pagesService.queryDocumentPages(Number(docId))
    return { list: pages };
  }

  @Get('/:docId/:docVersionId/pages/')
  @Authenticated()
  async versionIndex(
    @PathParams('docId') docId: number,
    @PathParams('docVersionId') docVersionId: number
  ): Promise<{ list: ax.IOrganization[] }> {
    let pages = await this.pagesService.queryDocumentPages(docId, docVersionId);
    return { list: pages };
  }

  @Put('/:docId/pages/:id')
  @Authenticated()
  update(
    @PathParams('id') id:number,
    @BodyParams('entry') entry:any
    ) {
    return this.pagesService.update(id, entry);
  } 

  @Post('/:docId/pages/upload')
  @Authenticated()
  async uploadPagesToDocument(
    @Request() req:any,
    @PathParams('docId') docId:number
  ) {
    if (!req.files)
      return { "success": false, "error": "no files were uploaded" };
    
    var documentVersion = await this.documentVersionsService.getDefault(docId);

    const pageNumber = req.pageNumber 
      ? +req.pageNumber : +documentVersion.get('pagesCount') + 1

    $log.debug(`ADDING PAGE FOR docId=${docId} and docVersionId=${documentVersion.id}`);
    $log.debug(documentVersion.toJSON());

    return await this.pagesService.processUploadPages(
      req.files, docId, documentVersion.id, pageNumber);
  }

  @Post('/:docId/:docVersionId/pages/upload')
  @Authenticated()
  async uploadPagesToDocumentVersion(
    @Request() req: any,
    @PathParams('docId') docId: number,
    @PathParams('docVersionId') docVersionId: number
  ) {
    if (!req.files)
      return { "success": false, "error": "no files were uploaded" };

    let documentVersion = await this.documentVersionsService.find(docVersionId);

    if(!documentVersion) throw new Error('Document version with id ' + docVersionId + ' not found');

    const pageNumber = req.pageNumber 
      ? +req.pageNumber : +documentVersion.get('pagesCount') + 1

    $log.debug(`ADDING PAGE FOR docId=${docId} and docVersionId=${docVersionId}`);
    $log.debug(documentVersion.toJSON());

    return await this.pagesService.processUploadPages(
      req.files, docId, docVersionId, pageNumber);
  }
}