import {
    Controller, Authenticated, Get, PathParams, Post, BodyParams, Put, Delete
} from "@tsed/common";

import {$log} from "ts-log-debug";
import * as Express from "express";
// import * as Promise from 'bluebird';
import * as se from 'sequelize';
import {NotFound} from "ts-httpexceptions";
import DocumentsService from '../../services/documents/DocumentsService';
import TasksService from '../../services/TasksService';

@Controller("/documents")
export default class DocumentsCtrl {

    constructor(
        private documentsService: DocumentsService,
        private tasksService: TasksService
    ) {

    }

    @Get('/last')
    @Authenticated()
    async last(
      @BodyParams('projectId') projectId:string
    ): Promise<{list: ax.IDocument[]}> {
      let list = await this.documentsService.last(projectId);
      return { list };
    }

    @Get('/:id')
    @Authenticated()
    async find(
        @PathParams('id') id:number
    ): Promise<{entry: se.Instance<ax.IDocument>}> {

        // console.log('ID =>', id);
        const document = await this.documentsService.find(id);
        // console.log('Result=>', document);

        if (document){
            return { entry: document };
        }

        throw new NotFound("Document not found");
    }

    @Get('/:id/status')
    @Authenticated()
    status(
      @PathParams('id') id:number
    ): PromiseLike<{ ready:number }> {
      return this.tasksService.getDocumentStatus(id);
    }

    @Put('/:id')
    @Authenticated()
    update(
        @PathParams('id') id:number,
        @BodyParams('entry') entry: any
    ) {
      return this.documentsService.update(id, entry);
    }

    @Post('/')
    @Authenticated()
    create(
        @PathParams('id') id:number,
        @BodyParams('entry') entry: any
    ) {
      return this.documentsService.create(entry);
    }

    @Delete('/:id')
    @Authenticated()
    delete(
        @PathParams('id') id:number
    ): Promise<any> {
        return this.documentsService.delete(id);
    }

    @Get('/:id/file')
    @Authenticated()
    file(
        @PathParams('id') id:number
    ): Promise<any> {
        return null;
    }
}