import {
  Controller, Authenticated, Get, PathParams, Post, BodyParams, QueryParams, Put, Delete
} from "@tsed/common";

import {$log} from "ts-log-debug";
import * as Express from "express";
import * as se from 'sequelize';
import {NotFound} from "ts-httpexceptions";
import DescriptorsService from '../../services/documents/DescriptorsService';

@Controller("/descriptors")
export default class DescriptorsCtrl {

  constructor(
      private descriptorsService: DescriptorsService
  ) {}

  @Get('/')
  @Authenticated()
  async index(
    @QueryParams('docId') docId: string,
    @QueryParams('holderId') holderId: string,
    @QueryParams('projectId') projectId: string,
    @QueryParams('type') type: string,
  ): Promise<{ list: se.Instance<ax.IDescriptor>[] }> {
    const list = projectId
      ? await this.descriptorsService.getProjectDescriptors(projectId, type) 
      : await this.descriptorsService.getDescriptors(docId, holderId, type);
    return { list };
  }

  @Put('/:id')
  @Authenticated()
  async update(
      @PathParams('id') id:number,
      @BodyParams('entry') entry:any
  ) {
    var result = await this.descriptorsService.update(id, entry);
    var json = result.toJSON();
    if(json.params && json.params.length > 0) {
      json.params = JSON.parse(json.params);
    }
    return json;
  }

  @Post('/')
  @Authenticated()
  create(
      @PathParams('id') id:number,
      @BodyParams('entry') entry: any
  ) {
    return this.descriptorsService.create(entry);
  }

  @Delete('/:id')
  @Authenticated()
  delete(
      @PathParams('id') id:number
  ): Promise<any> {
      return this.descriptorsService.delete(id);
  }
}