import {
  Controller, Authenticated, Get, PathParams, Post, BodyParams, Put, Delete, Request
} from "@tsed/common";

import { $log } from "ts-log-debug";
import * as Express from "express";
// import * as Promise from 'bluebird';
import * as se from 'sequelize';
import { NotFound } from "ts-httpexceptions";
import DocumentVersionsService from '../../services/documents/DocumentVersionsService';
import FileService from '../../services/FileService';

@Controller("/document/versions")
export default class DocumentsCtrl {

  constructor(
      private documentVersionsService: DocumentVersionsService,
      private fileService: FileService
  ) {

  }


  @Put('/:id')
  @Authenticated()
  update(
      @PathParams('id') id:number,
      @BodyParams('entry') entry: any
  ) {
    return this.documentVersionsService.update(id, entry);
  }

  @Post('/')
  @Authenticated()
  create(
      @PathParams('id') id:number,
      @BodyParams('entry') entry: any
  ) {
    return this.documentVersionsService.create(entry);
  }

  @Delete('/:id')
  @Authenticated()
  delete(
      @PathParams('id') id:number
  ): Promise<any> {
    return this.documentVersionsService.delete(id);
  }

  @Get('/:id/file')
  @Authenticated()
  file(
      @PathParams('id') id:number
  ): Promise<any> {
      return null;
  }

  @Post('/:docId/upload')
  @Authenticated()
  async upload(
    @Request() req: any,
    @PathParams('docId') docId: number
  ) {
    if (!req.files)
      throw new Error("No files were uploaded");
    if(req.files.count > 1) 
      throw new Error("One file per version");

    let result = await this.fileService.processUpload(req.files);

    var file = result.files[0];
    
    // create version
    return await this.documentVersionsService.create(<any>{
      fileId: file.id,
      documentId: docId,
      title: "Document version"
    });
  }
}