import {
  Controller, Authenticated, Get, PathParams, Post, BodyParams, Put, Delete, QueryParams
} from "@tsed/common";

import GroupsService from '../../services/rights/GroupsService';

@Controller('/groups')
export default class GroupsCtrl {
  constructor(
    private groupsService: GroupsService
  ) {

  }

  @Get('/')
  @Authenticated()
  async index(): Promise<{ list:ax.IGroup[] }> {
    const list = await this.groupsService.queryWithUsers();
    // console.log('GroupCtrl::index:', list.length);
    return { list };
  }

  @Post('/')
  @Authenticated()
  add(
    @BodyParams('entry') group:ax.IGroup
  ): Promise<ax.IGroup> {
    return this.groupsService.create(group);
  }


  @Put('/:id')
  @Authenticated()
  update(
    @PathParams('id') id:number,
    @BodyParams('entry') group:ax.IGroup
  ) {
    // console.log('GroupCtrl::update:', group);
    return this.groupsService.update(id, group);
  }

  @Delete('/:id')
  @Authenticated()
  delete(
    @PathParams('id') id:number,
  ) {
    return this.groupsService.delete(id);
  }


  @Get('/:id/users')
  @Authenticated()
  getUsers(
    @PathParams('id') id:number,
  ) {
    return this.groupsService.usersIndex(id);
  }

  @Put('/:id/users')
  @Authenticated()
  putUsers(
    @PathParams('id') id:number,
    @BodyParams('userIds') userIds:number[]
  ) {
    return this.groupsService.updateUsers(id, userIds);
  }
}