import {
  Controller, Authenticated, Get, PathParams, Post, BodyParams, Put, Delete, QueryParams
} from "@tsed/common"
import * as se from 'sequelize'

import RightsService from '../../services/rights/RightsService'

@Controller('/rights')
export default class RightsCtrl {
  constructor(
    private rightsService: RightsService
  ) {

  }

  @Get('/')
  @Authenticated()
  async index():Promise<{ list:ax.IRight[] }> {
    const list = await this.rightsService.query()
    // console.log('GET Rights RESULT => ', list.length)
    return { list }
  }


  @Get('/')
  @Authenticated()
  async indexSpecificRights(
    @QueryParams('refId') refId,
    @QueryParams('entity') entity
  ):Promise<{ list:ax.IRight[] }> {
    const list = await this.rightsService.querySpecificRights(refId, entity);
    return { list };
  }

  @Post('/')
  @Authenticated({ role: 'admin' })
  async add(
    @BodyParams('entry') right:ax.IRight,
    @BodyParams('entries') rights:ax.IRight[],
  ): Promise<ax.IRight[]> {
    if(rights && rights.length > 0) {
      return await Promise.all(rights.map(i => this.rightsService.create(i)))
    }
    return [await this.rightsService.create(right)]
  }


  @Put('/:id')
  @Authenticated({ role: 'admin' })
  update(
    @PathParams('id') id:number,
    @BodyParams('entry') right:ax.IRight
  ) {
    return this.rightsService.update(id, right)
  }

  @Delete('/:id')
  @Authenticated({ role: 'admin' })
  delete(
    @PathParams('id') id:number,
  ) {
    return this.rightsService.delete(id)
  }
}