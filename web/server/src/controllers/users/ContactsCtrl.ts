import {
    Controller, Authenticated, Get, PathParams, Post, BodyParams, Put, Delete, Request
} from "@tsed/common";

import UsersService from '../../services/UsersService';

@Controller("/contacts")
export default class ContactsCtrl {

  constructor(
      private usersService: UsersService
  ) {
    
  }

  @Get('/')
  @Authenticated()
  index(
    @Request() request, 
  ): PromiseLike<{list: any}> {
    return this.usersService.contacts(request.user.id);
  }


}