import {
  Controller, Authenticated, Get, PathParams, QueryParams, Post, BodyParams, Put, Delete, RouterController
} from "@tsed/common";

import NotesService from '../../services/NotesService';

@Controller("/Notes")
export default class NotesCtrl {

  constructor(
    private notesService: NotesService,
  ) {
    // super(NotesService);
  }

  @Get('/')
  @Authenticated()
  index(): PromiseLike<{ list: ax.INote[] }> {
    return this.notesService.query().then(Notes => {
      // console.log('GET NoteS RESULT => ', Notes.length);
      return {
        list: Notes
      };
    });
  }

  @Post('/')
  @Authenticated()
  add(
    @BodyParams('entry') note: ax.INote
    ): Promise<ax.INote> {
    return this.notesService.create(note);
  }


  @Put('/:id')
  @Authenticated()
  update(
    @PathParams('id') id:number,
    @BodyParams('entry') note: ax.INote
    ) {
    return this.notesService.update(id, note);
  }

  @Delete('/:id')
  @Authenticated()
  delete(
    @PathParams('id') id:number,
  ) {
    return this.notesService.delete(id);
  }

  @Delete('/')
  @Authenticated()
  async deleteBy(
    @QueryParams('userId') userId:number,
    @QueryParams('noteIds') noteIds:number[],
  ) {
    if(noteIds) {
      let result = [];
      if(!Array.isArray(noteIds)) {
        noteIds = [noteIds];
      }
      // console.log('NotesCtrl::deleteBy: ', noteIds);
      for(let i = 0; i < noteIds.length; i++) {
        result.push(await this.notesService.delete(noteIds[i]));
      }
      return {result: result.map(i => i.id)};
    }
    return await this.notesService.deleteBy(userId);
  }


}