import {
    Controller, Authenticated, Get, PathParams, Post, BodyParams, Put, Delete, RouterController
} from "@tsed/common";

import UsersService from '../../services/UsersService';

@Controller("/users")
export default class UsersCtrl {

  constructor(
      private usersService: UsersService,
  ) {
  }

  @Get('/projectscount')
  @Authenticated()
  count() {
    return this.usersService.getProjectsCount();
  }

  @Get('/')
  @Authenticated()
  async index(): Promise<{list: ax.IUser[]}> {
    const users = await this.usersService.query();
 
    // console.log('GET USERS RESULT => ', users.length);

    return {
      list: users
    }
  }
  
  @Post('/')
  @Authenticated()
  add(
    @BodyParams('entry') user: ax.IUser
  ): Promise<ax.IUser> {
    return this.usersService.create(user);
  }

  @Put('/:id')
  @Authenticated()
  update(
      @PathParams('id') id:number,
      @BodyParams('entry') user: ax.IUser
  ) {
    return this.usersService.update(id, user);
  }

  @Delete('/:id')
  @Authenticated()
  delete(
    @PathParams('id') id:number,
  ) {
      return this.usersService.delete(id);
  }
  
  @Post('/signup/update')
  @Authenticated()
  registerUpdate(
    @BodyParams('entry') entry:ax.IUser
  ) {
    return this.usersService.registerUpdate(entry);
  }


  @Put('/:id/chpass')
  @Authenticated()
  changePassword(
      @PathParams('id') id:number,
      @BodyParams('password') password:string
  ) {
      return this.usersService.changePassword(id, password);
  }
}
