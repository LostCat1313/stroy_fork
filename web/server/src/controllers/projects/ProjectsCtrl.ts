import {
    Controller, Authenticated, Get, PathParams, Post, BodyParams, Put, Delete, QueryParams
} from "@tsed/common";

import ProjectsService from '../../services/ProjectsService';
import DocumentsService from '../../services/documents/DocumentsService';
import FileService from '../../services/FileService';
import { Project } from '../../models'

@Controller('/projects')
export default class ProjectsCtrl {
  constructor(
    private projectsService: ProjectsService,
    private documentsService: DocumentsService,
    private fileService: FileService
  ) {

  }

  @Get('/')
  @Authenticated()
  async index(
    @QueryParams('all') all: boolean
  ): Promise<{list: ax.IProject[]}> {
    const ids = await this.projectsService.getUserProjectsIds();
    const projects = await this.projectsService.getByIds(all ? null : ids );

    // console.log('GET PROJECTS RESULT => ', projects.length);
    
    return {
      list: projects.map(p => {
        const result = p.toJSON()
        const users = result.users.map(u => {
          const rightMask = u.rights && u.rights.length > 0 ? u.rights[0].rightMask : 0;
          return {
            id: u.id,
            rightMask
          };
        })
        return {
          ...result,
          users,
          image: this.fileService.formatEntry(p.image)
        };
      })
    };
  }

  @Post('/')
  @Authenticated()
  add(
    @BodyParams('entry') project:Project
  ): Promise<ax.IProject> {
    return this.projectsService.create(project);
  }


  @Put('/:id')
  @Authenticated()
  update(
      @PathParams('id') id:number,
      @BodyParams('entry') project:Project
  ) {
    return this.projectsService.update(id, project);
  }

  @Delete('/:id')
  @Authenticated()
  delete(
    @PathParams('id') id:number,
  ) {
      return this.projectsService.delete(id);
  }

  @Get('/:projectId/documents')
  @Authenticated()
  async documentsIndex(
    @PathParams('projectId') projectId: string,
    @QueryParams('plain') plain: string,
    @QueryParams('parentId') parentId: string,
    @QueryParams('unfoldSections') unfoldSections: boolean
  ) {
    if(plain === 'true') {
      return await this.documentsService.index(projectId, parentId, unfoldSections);
    } 
    return {
      list: await this.documentsService.getDocumentHierarchy(Number(projectId))
    };
  }

  @Post('/:projectId/documents')
  @Authenticated()
  documentsAdd(
    @PathParams('projectId') projectId: string,
    @BodyParams('parentId') parentId: string,
    @BodyParams('title') title: string,
    @BodyParams('file') file: any
  ) {
    return this.documentsService.add(title, projectId, parentId);
  }

  @Post('/:projectId/documents/add_folder')
  @Authenticated()
  documentsAddFolder(
    @PathParams('projectId') projectId: string,
    @BodyParams('parentId') parentId: string,
    @BodyParams('title') title: string,
  ) {
    return this.documentsService.addFolder(title, projectId, parentId);  
  }

  @Get('/documents/:id')
  @Authenticated()
  documentsShow(
    @PathParams('id') documentId: string
  ) {
    return this.documentsService.index  
  }

  @Get('/:projectId/documents/last')
  @Authenticated()
  async documentsLastDocs(
    @PathParams('projectId') projectId: string
  ) {
    let list = await this.documentsService.last(projectId);
    return { list };
  }

  @Get('/:projectId/contacts')
  @Authenticated()
  contacts(
    @PathParams('projectId') projectId: string,
  ) {
    return this.projectsService.getContacts(projectId);  
  }
}