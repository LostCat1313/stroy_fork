import {
  Controller, Authenticated, Get, PathParams, Post, BodyParams, Put, Delete, QueryParams
} from "@tsed/common";

import ProjectsService from '../../services/ProjectsService';
import UsersService from '../../services/UsersService';

@Controller('/projects')
export default class ProjectUsers {
  constructor(
    private projectsService: ProjectsService,
    private usersService: UsersService
  ) {

  }
  @Get('/:id/users')
  @Authenticated()
  projectUsers(
    @PathParams('id') id:number,
  ) {
    return this.projectsService.getProjectUsers(id);  
  }

  @Put('/:id/users')
  @Authenticated()
  updateProjectUsers(
    @PathParams('id') id:number,
    @BodyParams('userIds') userIds: string[],
    @BodyParams('rights') rights: any[]
  ) {
    if(userIds) {
      return this.projectsService.updateProjectUsers(id, userIds.map(id => parseInt(id)));  
    } else if(rights) {
      return this.projectsService.setProjectUsersRights(id, rights);  
    }
  }
}
