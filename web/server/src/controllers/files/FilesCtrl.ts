import {
  Controller, Authenticated, Get, PathParams, Post, BodyParams, Put, Delete, Request, Response, QueryParams
} from "@tsed/common";

import { $log } from "ts-log-debug";
import * as Express from "express";
// import * as Promise from 'bluebird';
import * as se from 'sequelize';
import { NotFound } from "ts-httpexceptions";
import FileService from '../../services/FileService';

@Controller("/files")
export default class FilesCtrl {
  constructor(
    private fileService: FileService
  ) {

  }

  @Get('/')
  @Authenticated()
  async getFiles(
    @QueryParams('limit') limit:number, ///???? move to service
    @QueryParams('offset') offset:number
  ) {
    this.fileService.setLimit(limit, offset);
    const list = await this.fileService.query();
    const count = await this.fileService.count();
    return { list, count };
  }

  @Get('/:id')
  @Authenticated()
  public getFileById(
    @PathParams('id') id:number
  ) {
    return this.fileService.find(id).then((e:any) => {
      return {
        entry: this.fileService.formatEntry(e)
      };
    });
  }

  @Get('/s/:fid')
  @Authenticated()
  public getFile(
    @PathParams('fid') fid: string,
    @Response() response: any
    ) {
    let clearFid = fid.split('.')[0];
    let isThumb = false;
    const parts = clearFid.split('_');
    if(parts.length > 1) {
      isThumb = parts[0] == 'thumb';
      clearFid = parts[1];
    }
    return this.fileService.getByFid(clearFid, isThumb).then((f: any) => {
      if(f.stream == null) {
        response.send(404);
      } else {
        response.writeHead(200, {
          "Content-Type": f.mimetype,
          "Content-Disposition": "attachment; filename=" + f.original
        });
        f.stream.pipe(response);
      }
    });
  }

  @Delete('/:id')
  @Authenticated()
  delete(
    @PathParams('id') id:number
    ): Promise<any> {
    return this.fileService.delete(id);
  }

  @Post('/upload')
  @Authenticated()
  public upload(
    @Request() req: any,
    @BodyParams('type') type: number
    ) {
    if (!req.files)
      return { "success": false, "error": "no files were uploaded" };

    return this.fileService.processUpload(req.files);
  }
}