import {
  Controller, Authenticated, Get, PathParams, Post, BodyParams, Put, Delete, Request, Response
} from "@tsed/common";

import {$log} from "ts-log-debug";
import * as Express from "express";
// import * as Promise from 'bluebird';
import * as se from 'sequelize';
import {NotFound} from "ts-httpexceptions";
import PagesService from '../../services/PagesService';

@Controller("/pages")
export default class PagesCtrl {
  constructor(
      private pageService: PagesService
  ) {

  }

  @Post('/update')
  @Authenticated()
  update(
    @BodyParams('pageList') pageList:any[],
    ): Promise<any> {
    return this.pageService.setPageNumber(pageList);
  }


  @Delete('/:id')
  @Authenticated()
  delete(
    @PathParams('id') id:number
    ): Promise<any> {
    return this.pageService.delete(id);
  }

  @Post('/:id/actual')
  @Authenticated()
  activate(
    @PathParams('id') id:number,
    @BodyParams('actual') actual:boolean
    ): Promise<any> {
    return this.pageService.activate(id, actual);
  }
}