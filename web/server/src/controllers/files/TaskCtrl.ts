import {
  Controller, Authenticated, Get, PathParams, Post, BodyParams, Put, Delete, RouterController
} from "@tsed/common";

import { $log } from "ts-log-debug";
import * as Express from "express";
import { NotFound } from "ts-httpexceptions";
import TasksService from '../../services/TasksService';
// import * as Promise from 'bluebird';
import GetCtrl from '../base/GetCtrl';

@Controller("/tasks")
export default class TasksCtrl {

  constructor(
    private tasksService: TasksService,
  ) {
  }
  @Get('/')
  @Authenticated()
  index(): PromiseLike<{ list: ax.ITask[] }> {
    return this.tasksService.query().then(tasks => {
      // console.log('GET TASKS RESULT => ', tasks.length);
      return {
        list: tasks
      };
    });
  }

  @Post('/')
  @Authenticated()
  add(
    @BodyParams('entry') task: ax.ITask
    ): Promise<ax.ITask> {
    return this.tasksService.create(task);
  }


  @Put('/:id')
  @Authenticated()
  update(
    @PathParams('id') id:number,
    @BodyParams('entry') task: ax.ITask
    ) {
    return this.tasksService.update(id, task);
  }

  @Delete('/:id')
  @Authenticated()
  delete(
    @PathParams('id') id:number,
  ) {
    return this.tasksService.delete(id);
  }
}