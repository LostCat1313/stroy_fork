import {
  Controller, Authenticated, Get, PathParams, QueryParams, Post, BodyParams, Put, Delete, RouterController
} from "@tsed/common";

import DeepLinksService from '../../services/DeepLinksService';
import ProjectsService from '../../services/ProjectsService';

@Controller("/deeplinks")
export default class DeepLinksCtrl {

  constructor(
    private deepLinksService: DeepLinksService,
    private projectsService: ProjectsService,
  ) {
  }

  @Get('/')
  @Authenticated()
  index(): PromiseLike<{ list: ax.IDeepLink[] }> {
    return this.deepLinksService.query().then(list => {
      return {
        list
      };
    });
  }

  @Get('/:key')
  @Authenticated()
  async apply(
    @PathParams('key') key:string,
  ) {
    const entry = await this.deepLinksService.applyProjectInvitation(key);
    const project = this.projectsService.find(entry.projectId);
    return { success: true, project }
  }

  @Post('/new')
  @Authenticated()
  getNew(
    @BodyParams('projectId') projectId:number,
    @BodyParams('rightMask') rightMask:number,
    @BodyParams('phone') phone:string,
    @BodyParams('data') data:any
  ): PromiseLike<{url:string}> {
    return this.deepLinksService.createProjectInvitation(projectId, { 
      rightMask: rightMask || 0b001,
      phone
    });
  }

  @Post('/')
  @Authenticated()
  add(
    @BodyParams('entry') entry: ax.IDeepLink
    ): Promise<ax.IDeepLink> {
    return this.deepLinksService.create(entry);
  }


  @Put('/:id')
  @Authenticated()
  update(
    @PathParams('id') id:number,
    @BodyParams('entry') entry: ax.IDeepLink
    ) {
    return this.deepLinksService.update(id, entry);
  }

  @Delete('/:id')
  @Authenticated()
  delete(
    @PathParams('id') id:number,
  ) {
    return this.deepLinksService.delete(id);
  }

}