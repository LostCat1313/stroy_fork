import { 
  Controller,
  Get,
  Post,
  RouteService,
  Authenticated,
  BodyParams 
} from "@tsed/common"
import RegistrationIdsService from "../services/RegistrationIdsService"
import UsersService from "../services/UsersService"

@Controller("/api")
export class RestCtrl {

  constructor(
    private routeService:RouteService,
    private registrationIdsService:RegistrationIdsService,
    private usersService:UsersService
  ) {}

  @Get('/')
  public getRoutes() {
    return this.routeService.getAll()
  }

  @Get('/ping')
  @Authenticated()
  public async ping() {
    const countSummary = await this.usersService.getCountSummary()
    const user = this.usersService.currentUser.toJSON();
    return {
      pong: true, 
      userId: user.id,
      user: {
        ...user,
        password: null
      },
      countSummary
    }
  }

  @Post('/ping')
  @Authenticated()
  async postPing(
    @BodyParams('regId') regId: string,
    @BodyParams('deviceType') deviceType: number
  ) {
    await this.registrationIdsService.register(regId, deviceType || 0)
    const countSummary = await this.usersService.getCountSummary()
    const user = this.usersService.currentUser.toJSON();
    return {
      pong: true,
      userId: user.id,
      user: {
        ...user,
        password: null
      },
      countSummary 
    }
  }

}