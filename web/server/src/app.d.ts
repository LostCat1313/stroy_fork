
declare namespace ax {
  export interface IWithId {
    id?:number,
    createdAt?:string,
    updatedAt?:string
  }

  export interface IWithModification {
    createdBy?:number;
    modifiedBy?:number;
  }

  export interface IProject extends IWithId, IWithModification {
    title:string;
    imageId:number; 
  }

  export interface IWithRights {
    rightCodes?:string[];
    rights?:IRight[];
  }
  export interface IUser extends IWithId, IWithRights {
    name?:string;
    password?:string,
    email?:string;
    phoneNumber?:string;
    activationCode?:string;
    isActive?:boolean;
  }
  export interface IDocument extends IWithId {
    title:string,
    children:any[];
    projectId: number;
    isFolder:boolean;
  }
  export interface IDocumentVersion extends IWithId {
    title?:string;
    documentId:number;
    file?:IFile;
    pages?:any[];
  }

  export interface IFile extends IWithId, IWithModification {
    fid:string;
    original:string; 
    type:number;
    pageNumber:number;
    docId:number;
    actual:boolean;
  }

  export interface IDeepLink extends IWithId, IWithModification {
    projectId:number;
    key:string;
    data:string;
  }

  export interface INote extends IWithId {
    userId:number;
    text:string;
  }
  
  export interface IOrganization extends IWithId {
    name:string
  }

  export interface IPosition extends IWithId {

  }
  export interface INotification extends IWithId {
    
  }
  export interface IDescriptor extends IWithId, IWithModification {
    type:number;
    params:any;
    title:string;
    text:string;
    docId:number;
    holderId:number;
    holderType:number;
  }

  export interface IPage extends IWithId, IWithModification {
    pageNumber:number;
    fid:string;
    docId:number;
  }

  export interface ITask extends IWithId {
    log:string,
    docId:number,
    refId:number,
    type:number,
    status:number
  }

  export interface IChatRoom extends IWithId, IWithModification {
    name:string,
    isPrivate:boolean,
    targetUserId:number,
    users?:IUser
  }

  export interface IChatRoomSummary {
    room?:IChatRoom,
    count:Number,
    messages:IChatMessage[]
  }

  export interface IChatMessage extends IWithId {
    content: string;
    roomId: number;
    senderId: number;
    userIds?: number[];
  }

  export interface IChatMessageAttachment {
    targetId: number,
    type: number,
    messageId: number,
    file: any
  }

  export interface IRegistrationId extends IWithId {
    regId:string,
    deviceType:number,
    userId?:number
  }
  
  export interface IEvent extends IWithId {
    type: number,
    title?: string,
    content?: string,
    projectId?: number,
    targetId: number
  }

  export interface INotification extends IWithId {
    userId: number,
    eventId: number,
    status?: number
  }

  export interface IGroup extends IWithId, IWithRights {
    parentId:number;
    name:string;
    userIds?:number[];
    users?:IUser[];
  }

  export interface IRight extends IWithId {
    refId:number;
    entity:string;
    operation:string;
    sign:number;
  }
}
