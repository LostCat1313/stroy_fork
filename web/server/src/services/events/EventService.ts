import { Service } from "@tsed/common";
import UsersService from '../UsersService';
import BaseEntryService from '../../common/BaseEntryService';


@Service()
export default class EventService extends BaseEntryService<ax.IEvent> {
  constructor(
    private userService: UsersService
  ) {
    super('Event');
    this.allowedFields = {
      update: ['type', 'title', 'content', 'projectId', 'targetId'],
      create: ['type', 'title', 'content', 'projectId', 'targetId']
    }
  }
} 

