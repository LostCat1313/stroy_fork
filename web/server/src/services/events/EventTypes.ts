export namespace EventTypes {
  export const CHAT_MESSAGE:number = 201;
  export const CHAT_ROOM_ADDED:number = 202;

  export const ACTIVATION_CODE:number = 1;

  export const PROJECT_DOCUMENT_ADDED:number = 101;
  export const PROJECT_DOCUMENT_REMOVED:number = 102;
  export const PROJECT_DOCUMENT_PAGES_ADDED:number = 103;
  export const PROJECT_DOCUMENT_UPDATED:number = 104;
  export const PROJECT_DOCUMENT_DESCRIPTOR_ADDED:number = 105;
  export const PROJECT_DOCUMENT_VERSION_ADDED:number = 106;
  export const PROJECT_USER_ADDED:number = 107;
  export const PROJECT_USER_CHANGED:number = 108;
  export const PROJECT_USER_REMOVED:number = 109;
}