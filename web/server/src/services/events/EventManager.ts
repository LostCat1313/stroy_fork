import { InjectorService } from "@tsed/common";
import { EventEmitter } from 'events';
import { $log } from 'ts-log-debug';

import { events } from 'axiom-common-db';

import EventService from "./EventService";
import NotificationsService from "../NotificationsService";
import { EventTypes } from "./EventTypes";
import ProjectsService from "../ProjectsService";
import DocumentsService from "../documents/DocumentsService";
import UsersService from '../UsersService';



export default class EventManager {
  static instance:EventManager = new EventManager();

  static CHAT_MESSAGE = 'chat::message';
  static CHAT_ROOM = 'chat::room';
  
  static ACTIVATION_CODE = 'activationCode';

  static PROJECT_DOCUMENT_ADDED = 'document::added'
  static PROJECT_USER_ADDED = 'user::added'
  static PROJECT_USER_REMOVED = 'user::removed'
  static PROJECT_USER_CHANGED = 'user::changed'
  static PROJECT_DOCUMENT_VERSION_ADDED = 'document::versionadded'
  static PROJECT_DOCUMENT_DESCRIPTOR_ADDED = 'document::descriptoradded';


  private emitter:EventEmitter;
  
  private eventService:EventService;
  private usersService:UsersService;
  private notificationService:NotificationsService;
  private projectService:ProjectsService;
  private documentService:DocumentsService;

  constructor() {
    this.emitter = new EventEmitter();
  }

  init() {
    this.eventService = InjectorService.invoke<EventService>(EventService);
    this.usersService = InjectorService.invoke<UsersService>(UsersService);
    this.notificationService = InjectorService.invoke<NotificationsService>(NotificationsService);
    this.projectService = InjectorService.invoke<ProjectsService>(ProjectsService);
    this.documentService = InjectorService.invoke<DocumentsService>(DocumentsService);
    // console.log(this);
    this.subscribeChat();
    this.subscribeDocuments();
    this.subscribeActivationCode();
    this.subscribeProjectChange();
  }

  public static emitChatMessage(data) {
    EventManager.instance.emitter.emit(EventManager.CHAT_MESSAGE, data);
  }

  public static emitProjectUserAdded(data: {
    userId: number,
    projectId: number,
    rightMask: number
  }) {
    EventManager.instance.emitter.emit(EventManager.PROJECT_USER_ADDED, data);
  }

  public static emitProjectUserChanged(data: {
    userId: number,
    projectId: number,
    rightMask: number
  }) {
    EventManager.instance.emitter.emit(EventManager.PROJECT_USER_CHANGED, data);
  }

  public static emitProjectUserRemoved(data: {
    userId: number,
    projectId: number
  }) {
    EventManager.instance.emitter.emit(EventManager.PROJECT_USER_REMOVED, data);
  }

  public static emitProjectDocumentDescriptorAdded(data) {
    EventManager.instance.emitter.emit(EventManager.PROJECT_DOCUMENT_DESCRIPTOR_ADDED, data);
  }

  public static emitProjectDocumentAdded(data) {
    EventManager.instance.emitter.emit(EventManager.PROJECT_DOCUMENT_ADDED, data);
  }

  public static emitProjectDocumentVersionAdded(data) {
    EventManager.instance.emitter.emit(EventManager.PROJECT_DOCUMENT_VERSION_ADDED, data);
  }

  public static emitActivationCode(data) {
    EventManager.instance.emitter.emit(EventManager.ACTIVATION_CODE, data);
  }

  private subscribeChat() {
    // make more common
    this.emitter.on(EventManager.CHAT_MESSAGE, (data) => {
      $log.debug('EventManager::CHAT_MESSAGE: ', data)
      this.createEvent(data.userIds, {
        type: EventTypes.CHAT_MESSAGE,
        targetId: data.message.id
      });
    });
  }

  private subscribeActivationCode() {
    this.emitter.on(EventManager.ACTIVATION_CODE, (data) => {
      $log.debug('EventManager::ACTIVATION_CODE: ', data)
      this.createEvent([data.userId], {
        type: EventTypes.ACTIVATION_CODE,
        targetId: data.userId,  // probably regId?
        title: 'Код активации',
        content: data.code
      });
    });
  }

  private subscribeDocuments() {
    this.emitter.on(EventManager.PROJECT_DOCUMENT_ADDED, (data) => {
      this.createEventForProject({
        type: EventTypes.PROJECT_DOCUMENT_ADDED,
        projectId: data.projectId,
        targetId: data.entry.id
      });
    });

    this.emitter.on(EventManager.PROJECT_DOCUMENT_DESCRIPTOR_ADDED, (data) => {
      this.createEventForProject({
        type: EventTypes.PROJECT_DOCUMENT_DESCRIPTOR_ADDED,
        projectId: data.projectId,
        targetId: data.entry.id
      });
    });
  }

  private subscribeProjectChange() {

    const rights = [
      { text: 'Чтение', value: 0b001 },
      { text: 'Чтение и запись', value: 0b011 },
      { text: 'Управление', value: 0b111 },
    ];

    this.emitter.on(EventManager.PROJECT_USER_ADDED, async ({
      userId, projectId, rightMask
    }) => {
      const project:any = await this.projectService.find(projectId);

      const rightText = (rights.find(r => rightMask == r.value) ||
        {text: rightMask}).text;

      this.createEvent([userId], {
        type: EventTypes.PROJECT_USER_ADDED,
        projectId: projectId,
        targetId: null,
        title: 'Добавление в проект',
        content: `Вы были добавлены в проект '${project.title}' c правами '${rightText}'`
      });
    });

    this.emitter.on(EventManager.PROJECT_USER_REMOVED, async ({
      userId, projectId
    }) => {
      const project:any = await this.projectService.find(projectId);

      this.createEvent([userId], {
        type: EventTypes.PROJECT_USER_REMOVED,
        projectId: projectId,
        targetId: null,
        title: 'Удаление из проекта',
        content: `Вы были удалены из проекта '${project.title}'`
      });
    });

    this.emitter.on(EventManager.PROJECT_USER_CHANGED, async ({ userId, projectId, rightMask }) => {

      const project:any = await this.projectService.find(projectId);

      const rightText = (rights.find(r => rightMask == r.value) ||
        {text: rightMask}).text;

      this.createEvent([userId], {
        type: EventTypes.PROJECT_USER_CHANGED,
        projectId,
        targetId: null,
        title: 'Изменение прав в проекте',
        content: `Ваши права в проекте '${project.title}' изменились на '${rightText}'`
      });
    });
  }

  private async createEvent(userIds, eventData: { 
    type:number,
    projectId?:number,
    targetId:number,
    title?:string,
    content?:string
  }) {
    try {
      $log.debug("EventManager::createEvent:", userIds, userIds.length, eventData);

      // do not notify users on his actions
      if(this.usersService.currentUser) {
        userIds = userIds.filter(id => id != this.usersService.currentUser.id);
      }

      const model = await events.format(eventData);

      const result = await this.eventService.create(model);

      // sending notifications
      await this.notificationService.createForUsers(
        result.id, 
        userIds, 
        { ...result.toJSON(), ...model });

      return result;
    } 
    catch(e) {
      console.error('EventManager::createEvent', e);
    }
  }

  private async createEventForProject(eventData) {
    try {
      if(!eventData.projectId) throw new Error('eventData.projectId required');
      let userIds = await this.projectService.getProjectUserIds(eventData.projectId);
      const project = await this.projectService.find(eventData.projectId);
      await this.createEvent(userIds, eventData);
    }  
    catch(e) {
      console.error('EventManager::createEventForProject', e);
    }
  }

}