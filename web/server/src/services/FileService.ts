import { Service } from "@tsed/common";
import DBStorage from './DBStorage';
import BaseEntryService from '../common/BaseEntryService';
import EventManager from './events/EventManager';
import * as fs from 'fs';
import * as path from 'path';
import * as se from 'sequelize';
import TasksService from "./TasksService";
import { ThumbnailHandler } from 'axiom-convert';

@Service()
export default class FileService extends BaseEntryService<ax.IFile> {

  private storePath: string;

  constructor(
    private taskService:TasksService
  ) {
    super('File');
    this.allowedFields = {
      update: ['fid', 'type', 'original', 'mimetype', 'docId', 'actual', 'docVersionId', 'pageNumber', 'createdBy', 'modifiedBy'],
      create: ['fid', 'type', 'original', 'mimetype', 'docId', 'actual', 'docVersionId', 'pageNumber', 'createdBy', 'modifiedBy']
    }

    this.storePath = path.join(__dirname, '..', '..', '.store');
    if (!fs.existsSync(this.storePath))
      fs.mkdirSync(this.storePath);
  }

  protected order(): any {
    return [['createdAt', 'DESC']];
  }

  async query() {
    const list = await super.query();
    return list.map(i => this.formatEntry(i));
  }

  private withExt(f, forceExt = null, prefix = null) {
    let ext = forceExt || path.extname(f.original);
    return (prefix ? prefix : "") + f.fid + (ext ? ext : '')
  }

  prepareFileInfo(f:any, additional:any = null) {
    let fid = this.uuid();
    let storePath = this.storePath;
    let type = this.identifyType(f.name);
    additional = additional || {};
    let res = { fid, original: f.name, type, ...additional, ...f };
    res.targetName = this.formatPath(res);
    this.createDir(path.dirname(res.targetName));
    // console.log("FILE ENTRY PREPARED", res);
    return res;
  }

  createDir(targetDir:string) {
    const sep = path.sep;
    const initDir = path.isAbsolute(targetDir) ? sep : '';
    targetDir.split(sep).reduce((parentDir, childDir) => {
      const curDir = path.resolve(parentDir, childDir);
      if (!fs.existsSync(curDir)) {
        fs.mkdirSync(curDir);
      }
      return curDir;
    }, initDir);
  }

  getByFid(fid: string, isThumb = false) {
    const prefix = isThumb ? 'thumb_' : null;
    const ext = isThumb ? '.jpg' : null;
    return this.dbModel.findOne({where : { fid }}).then((f:any) => {
      let filePath = this.formatPath(f, ext, prefix);
      const processingPath = path.join(this.storePath, 'processing.png');
      const exists = fs.existsSync(filePath);
      return {
        mimetype: f.mimetype,
        original: f.original,
        // TODO: check if file exists! work on types ${f.type}
        stream: exists ?
          fs.createReadStream(filePath) :
            isThumb && fs.existsSync(processingPath) ? 
            fs.createReadStream(processingPath) :
            null
      };
    });
  }

  formatPath(entry:ax.IFile, forceExt = null, prefix = null) {
    return entry.docId 
      ? path.join(this.storePath, 'documents', `${entry.docId}`, `${entry.type}`, this.withExt(entry, forceExt, prefix))
      : path.join(this.storePath, `${entry.type}`, this.withExt(entry, forceExt, prefix));
  }

  formatUrl(entry:ax.IFile) {
    return '/api/files/s/' + this.withExt(entry);
  }

  formatThumbUrl(entry:ax.IFile) {
    return '/api/files/s/thumb_' + this.withExt(entry);
  }

  formatEntry(entry:ax.IFile) {
    if(!entry) return null;
    return {
      id: entry.id,
      fid: entry.fid,
      original: entry.original,
      type: entry.type,
      docId: entry.docId,
      pageNumber: entry.pageNumber,
      actual: entry.actual,
      url: this.formatUrl(entry),
      thumbUrl: this.formatThumbUrl(entry)
    }
  }

  identifyType(fileName) {
    let ext = path.extname(fileName).toLowerCase();
    if(['.doc', '.pdf', '.docx', '.odt'].indexOf(ext) != -1)
      return 1;
    return 0;
  }


  processUpload(files: any[]) {
    let prepared = Object.keys(files).map(key => {
      return this.prepareFileInfo(files[key]);
    });

    // console.log(`files number uploaded ${prepared.length}`);

    return this.saveFiles(prepared);
  }

  async saveFiles(prepared:any[]) {
    let filesToAdd:any[] = await Promise.all(prepared.map(f => {
      return new Promise(function (resolve, reject) {
        fs.writeFile(f.targetName, f.data, function (err) {
          if (err) reject({ err, ...f });
          else resolve(f);
        });
      });
    }));

    let dbFiles = await Promise.all(filesToAdd.map(f => {
      return this.create(<any>{
        ...f,
        original: f.name
      });
    }));

    let files = dbFiles.map(f => {
      return this.formatEntry(f);
    });

    // console.log("files added >>>>> " + files.length);
    
    await Promise.all(files.filter(f => f.type == 0).map(this.generateThumbnail));

    files.filter(f => f.type != 0).forEach(f => this.taskService.planThumbTask(f));

    return { files };
  }

  async generateThumbnail(file) {
    const handler = new ThumbnailHandler();
    return handler.handleFile(file);
  }
}