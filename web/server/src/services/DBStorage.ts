import {Service, ExpressApplication, Inject} from "@tsed/common";

import {IAnyModel, IRightModel, IToken} from '../common/DBTypes';
import ChatMessageAttachmentService from "./chat/ChatMessageAttachmentService";

@Service()
export default class DBStorage {
    db:{
        User: IAnyModel,
        Document: IAnyModel,
        DocumentVersion: IAnyModel,
        Project: IAnyModel,
        ProjectUserRight: IAnyModel,
        Position: IAnyModel,
        Organization: IAnyModel,
        File: IAnyModel,
        Task: IAnyModel,
        Notes: IAnyModel,
        Token: IToken,
        Descriptor: IAnyModel,
        ChatMessage: IAnyModel,
        ChatMessageAttachment: IAnyModel,
        ChatMessageStatus: IAnyModel,
        ChatRoom: IAnyModel,
        ChatRoomUser: IAnyModel,
        RegistrationId: IAnyModel,
        Event: IAnyModel,
        Group: IAnyModel,
        Right: IRightModel,
        sequelize: any
    };
    currentUser:any;
    sequelize:any;
    offset:number;
    limit:number;
    
    constructor(
    ){
        this.db = require('axiom-common-db').db;
        this.sequelize = this.db.sequelize;
    }
}
