import { Service } from "@tsed/common";
import DBStorage from './DBStorage';
import BaseEntryService from '../common/BaseEntryService';
import * as se from "sequelize";


@Service()
export default class OrganizationsService extends BaseEntryService<ax.IOrganization> {
  constructor(){
    super('Organization');
    this.allowedFields = {
      update: ['name'],
      create: ['name']
    }
  }

  async usersIndex(id:number) {
    let org = await this.find(id);
    let users = await (<any>org).getUsers();
    return {
      list: users.map(u => ({id: u.id, name: u.name}))
    }
  }

  async updateUsers(id:number, ids:number[]) {
    let org = await this.dbStorage.db.Organization.findById(id);
    let users = await this.dbStorage.db.User.findAll({where: {id: {$in: ids}}});
    (<any>org).setUsers(users);
    return {message: 'success'};
  }
}