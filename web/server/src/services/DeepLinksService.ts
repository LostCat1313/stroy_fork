import { Service } from "@tsed/common";
import DBStorage from './DBStorage';
import UsersService from './UsersService';
import BaseEntryService from '../common/BaseEntryService';
import * as se from "sequelize";
import * as fetch from "node-fetch";
import { NotFound } from "ts-httpexceptions";
import { $log } from 'ts-log-debug';

const BRANCH_LIVE_KEY = 'key_live_ogJAnYKapJgHZxB6hwOpjncgBxpZwAEd';
const BRANCH_API_URL = 'https://api2.branch.io/v1/url';

@Service()
export default class DeepLinksService extends BaseEntryService<ax.IDeepLink> {
  constructor() {
    super('DeepLink');
    this.allowedFields = {
      update: ['data', 'key', 'type', 'projectId', 'createdBy', 'modifiedBy'],
      create: ['data', 'key', 'type', 'projectId', 'createdBy', 'modifiedBy']
    }
  }

  async getByKey(key:String):Promise<any> {
    return this.dbModel.find(<any>{
      where: { key }
    });
  }

  private async createLink(key:string):Promise<{url:string}> {
    const result = await fetch(BRANCH_API_URL, {
      method: 'POST',
      body:    JSON.stringify({
        branch_key: BRANCH_LIVE_KEY,
        data: {
          custom_key: key
        }
      }),
      headers: { 'Content-Type': 'application/json' },
    });

    const jsonResult = await result.json();

    $log.debug('DeepLinkService::createLink', jsonResult);

    return jsonResult;
  }

  async createProjectInvitation(projectId:number, { rightMask, phone }):Promise<{url:string}> {
    const key = this.md5(projectId + '_' + rightMask  + '_' + new Date())

    $log.debug('DeepLinkService::createProjectInvitation key', key);

    const e = await this.create({
      key,
      projectId,
      data: JSON.stringify({rightMask, phone})
    });

    return await this.createLink(e.key);
  }

  async applyProjectInvitation(key) {
    const entry:any = await this.getByKey(key);
    const data = JSON.parse(entry.data || '{}');
    await this.dbStorage.db.ProjectUserRight.create({
      UserId: this.dbStorage.currentUser.id,
      ProjectId: entry.id,
      rightMask: data.rightMask, // all rights
    });
    entry.activated = new Date();
    return await entry.save();
  }
}