import { Service } from "@tsed/common";
import DBStorage from './DBStorage';
import UsersService from './UsersService';
import BaseEntryService from '../common/BaseEntryService';
import * as se from "sequelize";
import { NotFound } from "ts-httpexceptions";

@Service()
export default class NotesService extends BaseEntryService<ax.INote> {
  constructor(
    private userService: UsersService
  ) {
    super('Notes');
    this.allowedFields = {
      update: ['content', 'userId'],
      create: ['content', 'userId']
    }
  }

  /**
   * Query list of all entries
   * @returns {ax.IUser[]}
   */
  public query(): PromiseLike<any[]> {
    return this.dbModel.findAll({ 
      order: this.order(),
      where: {
        userId: this.dbStorage.currentUser.id
      }
    });
  }

  /**
   * Delete entry
   * @param id 
   * @returns {Promise<{id}>}
   */
  async delete(id:number): Promise<{ id }> {
    let entry = await this.dbModel.findById(id, { where: {
        userId: this.dbStorage.currentUser.id
      }
    });
   
    if(!entry) throw new NotFound("Entry not found");
    
    let resultId = entry.getDataValue('id');

    await entry.destroy();

    return { id: resultId };
  }


  /**
   * Delete notes by userId
   * @returns Promise<{result}>
   */
  async deleteBy(userId:number): Promise<{result}> {
    let entries = await this.dbModel.findAll({ 
      order: this.order(),
      where: {
        userId: userId || this.dbStorage.currentUser.id
      }
    });
    
    let result = [];

    for(let i = 0; i < entries.length; i++) {
      result.push(entries[i].getDataValue('id'));
      await entries[i].destroy();
    }

    return { result };
  }

  /**
   * @inheritdoc
   */
  protected prepareEntry(entry: ax.INote, operation: 'update' | 'create') {
    entry.userId = this.dbStorage.currentUser.id;
    super.prepareEntry(entry, operation);
  }
}