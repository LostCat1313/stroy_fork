import { Service } from "@tsed/common";
import DBStorage from './DBStorage';
import BaseEntryService from '../common/BaseEntryService';
import * as se from "sequelize";


@Service()
export default class PositionsService extends BaseEntryService<ax.IPosition> {
  constructor(){
    super('Position');
    this.allowedFields = {
      update: ['name'],
      create: ['name']
    }
  }
}