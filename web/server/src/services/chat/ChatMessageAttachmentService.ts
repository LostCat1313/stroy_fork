import { Service } from "@tsed/common";
import BaseEntryService from '../../common/BaseEntryService';
import FileService from "../FileService";


@Service()
export default class ChatMessageAttachmentService extends BaseEntryService<ax.IChatMessageAttachment> {
  constructor(
    private fileService: FileService
  ) {
    super('ChatMessageAttachment');
    this.allowedFields = {
      update: ['messageId', 'targetId', 'type'],
      create: ['messageId', 'targetId', 'type']
    }
  }

  public createEntries(entries: any[]) {
    if(!entries) return Promise.resolve([]);
    return Promise
      .all(entries.map(e => this.create(e)))
      .then(this.formatEntries.bind(this));
  }

  public formatEntries(list: any[]) {
    return Promise.all(
      list.map(li => {
        return this.fileService
          .find(li.targetId)
          .then(f => {
            return {
              ...li.toJSON(),
              file: this.fileService.formatEntry(<any>f)
            };
          })
      })
    );
  }

  protected prepareEntry(entry: any, operation: 'update' | 'create') {
    if(entry.file) {
      entry.targetId = entry.file.id;
    }
    entry.type = 0;
    super.prepareEntry(entry, operation);
  }
}