import { Service, BeforeRoutesInit, HttpServer, Inject, Err} from "@tsed/common"
import { $log } from 'ts-log-debug'

import DBStorage from "../DBStorage"
import ChatRoomService from "./ChatRoomService"
import ChatMessageService from "./ChatMessageService"
import UsersService from "../UsersService"
import EventManager from "../events/EventManager"
import ChatMessageAttachmentService from "./ChatMessageAttachmentService"

@Service()
export default class ChatService implements BeforeRoutesInit {

  private activeUsers:{id:any, socket:any}[]
  private io:any

  constructor(
    private dbStorage: DBStorage,
    private usersService: UsersService,
    private chatRoomService: ChatRoomService,
    private chatMessageService: ChatMessageService,
    private chatMessageAttachmentService: ChatMessageAttachmentService,
    @Inject(HttpServer) private  httpServer: HttpServer
  ) {
    this.activeUsers = []
  }

 /**
   * IBeforeRoutesInit implementation. Used for initialization
   */
  public $beforeRoutesInit() {
    const io = require('socket.io')(this.httpServer)
    this.attach(io)
  }

 /**
   * Initialize socket.io for chat
   * @param roomId
   * @returns {string}
   */
  public attach(io) {
    this.io = io
    // auth middleware
    io.use((socket, next) => {
      let token = socket.handshake.query.token
      return this.validate(token).then(() => {
        return next()
      }, error => {
        return next(new Error('Authentication error: ' + error))
      })
    })
    
    io.on('connection', socket => {
      let token = socket.handshake.query.token
      this.login(socket, token).then(result => {
        // move handler to separate classes
        socket.on('message', (params, cb) => {
          this.onMessage(socket, params)
            .then(cb)
            .catch(error => console.error('ChatService::onMessage', error))
        })
        socket.on('disconnect', () => this.onDisconnect(socket))
        socket.on('getHistory', (params, cb) => {
          this.onGetHistory(socket, params)
            .then(cb)
            .catch(error => console.error('ChatService::onGetHistory: ', error))
        })
        socket.on('deleteRoom', (params, cb) => {
          this.onDeleteRoom(socket, params)
            .then(cb)
            .catch(error => console.error('ChatService::onDeleteRoom: ', error))
        })
        socket.on('join', (params, cb) => {
          this.onJoin(socket, params)
            .then(cb)
            .catch(error => console.error('ChatService::onJoin: ',error))
        })
        socket.on('update', (params) => {
          this.onUpdate(socket, params)
            .then()
            .catch(error => console.error('ChatService::onUpdate: ', error))
        })
      }, error => {
        this.sendError(socket, error)
        console.error('ChatService::login ' + error)
      })
    })
  }

  /**
   * Handle socket on 'getHistory' event logic
   * @param socket
   * @param params
   * @returns {Promise<ax.IChatRoomSummary>} last messages of the room
   */
  private async onMessage(socket, data) {
    $log.debug('ChatService::onMessage: ', data)
    const user = this.currentUser(socket)

    if(!user) throw new Error('onMessage: current user not found')
    if(!data.content) data.content = ""

    const message = {
      senderId: user.id,
      content: data.content.trim(),
      roomId: data.roomId
    }
    const hasAttachments = !!data.attachments && data.attachments.length > 0
    if(message.content.length == 0 && !hasAttachments) {
      $log.debug('ChatService::onMessage: message is empty')
      return
    }
    // todo: handle attachments
    const result = await this.chatMessageService.create(<any>message)

    const attachments = await this.chatMessageAttachmentService.createEntries(
        (data.attachments || []).map(a => { 
          return {
            ...a, messageId: result.id
          }
        })
      )

    const messageJson = result.toJSON()
    // send to all users separate logic
    await this.chatRoomService.updateLastMessage(data.roomId, messageJson.id)
    const room = await this.chatRoomService.findWithId(data.roomId, user.id)

    const userIds = room.users.map(u => u.id)
    // todo: check current user
    const currentUser = room.users.find(u => u.id == user.id)

    EventManager.emitChatMessage({
      message:messageJson,
      currentUser,
      userIds: userIds.filter(id => {
        return id != currentUser.id
      })
    });

    const body = {
      room: { ...room, users: undefined },
      user: currentUser,
      senderId: user.id,
      content: message.content,
      attachments: attachments,
      createdAt: messageJson.createdAt,
      roomId: room.id,
      id: messageJson.id
    }

    this.sendRoomMessage(room.id, {
      ...body
    })

    return body
  }

  /**
   * Handle socket on 'getHistory' event logic
   * @param socket
   * @param params
   * @returns {Promise<ax.IChatRoomSummary>} last messages of the room
   */
  private async onGetHistory(socket, params):Promise<ax.IChatRoomSummary> {
    $log.debug('ChatService::onGetHistory: ', params)
    const user = this.activeUsers.find(({ socket: s }) => s === socket)
    if(!user) throw Error(`user not found for socket ${socket}`)
    if(!params.roomId) throw Error(`roomId param required ${JSON.stringify(params)}`)

    // let currentRoom = await this.chatRoomService.findWithId(params.roomId, user.id)
    const count = await this.chatMessageService.count({ roomId:params.roomId })
    const messages = await this.chatMessageService.history(params.roomId, params.offset)

    return {
      messages,
      count
    }
  }
  
  private async onDeleteRoom(socket, params):Promise<{ id }> {
    $log.debug('ChatService::onDeleteChat: ', params)
    const user = this.currentUser(socket)
    if(!user) throw Error(`user not found for socket ${socket}`)
    if(!params.roomId) throw Error(`roomId param required ${JSON.stringify(params)}`)

    // validate if user can delete this room
    if((await this.chatRoomService.findWithId(params.roomId, user.id)).createdBy != user.id) 
      throw new Error('you do not have permission to perform this operation)')

      /* REBOOT123456789123456 */

    const result = await this.chatRoomService.delete(params.roomId)

    return result
  }

  /**
   * Handle socket 'join' event logic
   * @param socket
   * @param params
   */
  private async onJoin(socket, params: {
    roomId?: number,
    userIds?: number[], // length == 1 -> private room, else -> group
    name?: string
    limit?: number
  }) {
    $log.debug('ChatService::onJoin: ', params)
    const user = this.currentUser(socket)
    let currentRoom = null 

    if(!user) throw new Error(`current user not defined: ${user}`)
    currentRoom = params.roomId ? 
      await this.chatRoomService.findWithId(params.roomId, user.id) :
      null
    
    $log.debug('ChatService::onJoin: currentRoom ', !!currentRoom)
    if(!currentRoom && !params.userIds) {
      throw new Error('currentRoom required or usersIds to create room')
    }
    let result = null
    if(!currentRoom) {
      result = await this.resolvePrivateRoomByIds(user.id, params.userIds)
      if(result == null) {
        result = await this.resolveNewRoom(user.id, params.name, params.userIds)
      }
    } 
    else {
      result = await this.updateRoomUserIds(user.id, currentRoom, params.userIds)
      if(result == null) {
        let messages = await this.chatMessageService.history(currentRoom.id, params.limit)
        let count = await this.chatMessageService.count({ roomId: currentRoom.id })
        result = {
          room: currentRoom,
          count,
          messages
        }
      }
    }
    if(result == null) throw new Error(`failed to join to room ${JSON.stringify(params)}`)
    this.updateStatus(result.room.users)
    this.joinUsersToRoom(result.id, result.room.users.map(u => u.id))
    return result
  }

  private async onUpdate(socket, params){
    const user = this.currentUser(socket)
    let data = params.viewed.map(messageId =>
    ({
        userId: user.id,
        messageId
      }))
    $log.debug('ChatService::onUpdate: ', data)
    try {
      await this.dbStorage.db.ChatMessageStatus.bulkCreate(data)
    } catch(err) {
      $log.debug('bulkCreate:', err)
    }
  }

  private onDisconnect(userSocket) {
    this.deactivate(userSocket)
  }

  private async login(socket, token) {
    $log.debug('ChatService::login: ', token)

    const user = await this.dbStorage.db.Token.getUserByToken(token)
    const contacts = await this.usersService.contacts(user.id)
    const rooms = await this.chatRoomService.rooms(user.id)

    rooms.list.forEach(item => {
      this.updateStatus(item.users)
      socket.join(this.getRoomCName(item.id))
    })

    this.updateStatus(contacts.list)

    socket.emit('init', {
        contacts: contacts.list,
        rooms: rooms.list
    })

    this.activate({ id: user.id, socket })  
  }

  private updateStatus(list) {
    list.forEach(u => {
      u.status = this.activeUsers.findIndex(user => user.id == u.id) >= 0 ? 'online' : 'offline' 
    })
  }

  private async validate(token){
    if(!token) throw new Error('token required')
    const user = await this.dbStorage.db.Token.getUserByToken(token)
    if(user === null) throw new Error('user not found')
  }

  private currentUser(userSocket) {
    $log.debug("ChatService::currentUser: active users count " + this.activeUsers.length)
    $log.debug("ChatService::currentUser: socket ", userSocket.id, " of ",this.activeUsers.map(au => au.socket.id))
    return this.activeUsers.find(({ socket }) => socket.id == userSocket.id)
  }

  private deactivate(userSocket) {
    const i = this.activeUsers.findIndex(({ socket }) => socket.id === userSocket.id)

    if (i > -1) {
      const user = this.activeUsers[i];
      this.activeUsers.splice(i, 1)
      if(this.activeUsers.findIndex(({ id }) => id === user.id) == -1) {
        this.notifyUsers(user.socket, 'status',
        { user: user.id, status: 'offline' })
      }
      
    }
  }

  private activate(user) {
    // if (!this.isActive(user)) {
      this.activeUsers.push(user)
      this.notifyUsers(user.socket, 'status',
        { user: user.id, status: 'online' })
    // }
  }

  private joinUsersToRoom(roomId:number, userIds:number[]) {
    this.activeUsers.map(au => {
      if(userIds.indexOf(au.id) > -1) {
        au.socket.join(this.getRoomCName(roomId))
      }
    })
  }

  private sendError(socket, err) {
    socket.emit('error', err)
  }

  private notifyUsers(socket, event, arg) {
    socket.broadcast.emit(event, arg)
  }

  private isActive(user) {
    return this.activeUsers.findIndex(({ id }) => id == user.id) > -1
  }

  private sendRoomMessage(roomId, message) {
    this.io.to(this.getRoomCName(roomId)).emit('message', message)
  }

  /**
   * Get room code name
   * @param roomId
   * @returns {string}
   */
  private getRoomCName(roomId):string {
    return 'room-' + roomId
  }

  /**
   * Update room users
   * @returns {Promise<ax.IChatRoomSummary>}
   */
  private async updateRoomUserIds(user, room, userIds):Promise<ax.IChatRoomSummary> {
    if(userIds && userIds.length > 1) {
      const newUserIds = [user.id, ...userIds.filter(u => u.id != user.id)]
      room.setUsers(newUserIds)
      const users = await this.dbStorage.db.User.findAll({
        where: {id: { $in: newUserIds }}
      })
      return {
        room: {
          ...room,
          users
        },
        count: 0,
        messages: []
      }
    }
    return null
  }

  /**
   * Get or create room for several users
   * @returns {Promise<ax.IChatRoomSummary>}
   */
  private async resolveNewRoom(currentUserId, roomName, userIds):Promise<ax.IChatRoomSummary> {
    if(userIds && userIds.length > 1) {
      const result = await this.chatRoomService.create(<any>{
        isPrivate: false,
        createdBy: currentUserId,
        name: roomName
      })
      const newUserIds = [currentUserId, ...userIds.filter(u => u.id != currentUserId)]
      // todo: rejects & then for everything
      result.setUsers(newUserIds)
      const users = await this.dbStorage.db.User.findAll({
          where: {id: { $in: newUserIds }}
        })
      const message = await this.chatMessageService.create({
        content: "Room have been created",
        roomId: result.id,
        senderId: 0,
        userIds
      })
      return {
        room: {
          ...result.toJSON(),
          users: users.map(u => u.toJSON())
        },
        count: 0,
        messages: [message.toJSON()]
      }
    }
    return null
  }

  /**
   * Get or create room private (2 users)
   * @returns {Promise<ax.IChatRoomSummary>}
   */
  private async resolvePrivateRoomByIds(currentUserId, userIds):Promise<ax.IChatRoomSummary> {
    if(userIds && userIds.length == 1) {
      let targetUserId = userIds[0]
      const room = await this.chatRoomService.findPrivate(currentUserId, targetUserId)
      if(room) {
        const messages = await this.chatMessageService.history(room.id)
        const count = await this.chatMessageService.count({ roomId: room.id })
        return { 
          room,
          count,
          messages
        } 
      }
      else {
        // don't need to create room if there is no messages! client logic - enough stub?
        // and create on first message
        
        const result = await this.chatRoomService.create({
          isPrivate: true,
          createdBy: currentUserId,
          targetUserId: targetUserId,
          name: ""
        })
        const room = await this.chatRoomService.findWithId(result.id, currentUserId)
        return {
          room,
          count: 0,
          messages: []
        }
      }
    }
    return null
  }
}