import { Service } from "@tsed/common";
import UsersService from '../UsersService';
import EventManager from '../events/EventManager';
import BaseEntryService from '../../common/BaseEntryService';

@Service()
export default class ChatRoomService extends BaseEntryService<ax.IChatRoom> {
  constructor() {
    super('ChatRoom');
    this.allowedFields = {
      update: ['createdBy', 'targetUserId', 'isPrivate', 'name'],
      create: ['createdBy', 'targetUserId', 'isPrivate', 'name']
    }
  }

  async findPrivate(userId1, userId2) {
    const room = await this.dbModel.findOne({
      where: <any>{
        isPrivate: true,
        $or: [
          { createdBy: userId1, targetUserId: userId2 },
          { createdBy: userId2, targetUserId: userId1 }
        ]
      },
      include: [{
        model: this.dbStorage.db.ChatMessage,
        as: 'lastMessage'
      },{
        model: this.dbStorage.db.User,
        as: 'createdByUser'
      },{
        model: this.dbStorage.db.User,
        as: 'targetUser'
      }]
    })
    return this.roomToJSON(room, userId1);
  }

  async findWithId(id, userId) {
    const room = await this.dbModel.findOne({
      where: { id },
      include: [{
        model: this.dbStorage.db.User,
        as: 'users'
      },{
        model: this.dbStorage.db.ChatMessage,
        as: 'lastMessage'
      },{
        model: this.dbStorage.db.User,
        as: 'createdByUser'
      },{
        model: this.dbStorage.db.User,
        as: 'targetUser'
      }]
    });
    return this.roomToJSON(room, userId);
  }

  async rooms(userId) {
    const list = await this.dbModel.findAll({
      where: <any>{
        $or: [{
          id: {
            $in: this.se.literal(`(SELECT "roomId" from "ChatRoomUsers" WHERE "userId"=${userId})`)
          }
        }, {
          isPrivate: true,
          createdBy: userId
        }, {
          isPrivate: true,
          targetUserId: userId
        }]
      },
      order: [['updatedAt', 'DESC']],
      include: [{
        model: this.dbStorage.db.User,
        as: 'users'
      },{
        model: this.dbStorage.db.ChatMessage,
        as: 'lastMessage'
      },{
        model: this.dbStorage.db.User,
        as: 'createdByUser'
      },{
        model: this.dbStorage.db.User,
        as: 'targetUser'
      }],
      attributes: this.attributes({userId})
    });
    return {
      list: list.map(room => {
        return this.roomToJSON(room, userId);
      })
    };
  }

  public roomToJSON(entry, userId) {
    if(!entry) return null;
    var room = entry.toJSON(); 
    if(room.isPrivate && room.createdByUser && room.targetUser) {
      // TODO: move this logic to client?
      let user = room.createdByUser.id != userId ? room.createdByUser : room.targetUser;
      room.name = user.firstName + ' ' + user.lastName;
      room.users = [room.createdByUser, room.targetUser];
    }
    return room;
  }

  public roomName(room, userId) {
    if(room.isPrivate && room.createdByUser && room.targetUser) {
      let user = room.createdByUser.id != userId ? room.createdByUser : room.targetUser;
      return user.firstName + ' ' + user.lastName;
    }
    return room.name;
  }

  /**
   * Query list of all entries
   * @returns {ax.IChatRoom[]}
   */
  public query(): PromiseLike<any[]> {
    return this.dbModel.findAll({ 
      order: this.order(),
      where: {
        userId: this.dbStorage.currentUser.id
      }
    });
  }

  async updateLastMessage(roomId, lastMessageId) {
    await this.dbModel.update(<any>{
      lastMessageId
    }, {
      where: { id:roomId }
    })
  }

  attributes({userId}):any[] {
    return [
      ...Object.keys(this.dbModel.attributes),
      [this.se.literal(`(
        SELECT COUNT(*)::int
        FROM public."ChatRooms"
        FULL OUTER JOIN public."ChatRoomUsers"
        ON public."ChatRooms"."id" = public."ChatRoomUsers"."roomId"
        FULL OUTER JOIN public."ChatMessages"
        ON public."ChatMessages"."roomId" = public."ChatRooms"."id"
        FULL OUTER JOIN public."ChatMessagesStatus" 
        ON public."ChatMessages"."id" = public."ChatMessagesStatus"."messageId"
        WHERE (public."ChatRooms"."targetUserId" = ${userId}
        OR public."ChatRoomUsers"."userId" = ${userId})
        AND public."ChatMessagesStatus"."id" IS NULL 
        AND public."ChatMessages"."roomId" = "ChatRoom"."id"
      )`),'unviewedMessagesCount']
    ];
  }
}