import { Service } from "@tsed/common";
import BaseEntryService from '../../common/BaseEntryService';
import FileService from "../FileService";
import { $log } from "ts-log-debug";
import EventManager from "../events/EventManager";
import UsersService from "../UsersService";


@Service()
export default class ChatMessageService extends BaseEntryService<ax.IChatMessage> {
  constructor(
    private fileService: FileService,
    private usersService: UsersService
  ) {
    super('ChatMessage');
    this.allowedFields = {
      update: ['content', 'roomId', 'senderId'],
      create: ['content', 'roomId', 'senderId']
    }
  }

  async count(where) {
    if(!where || !where.roomId || typeof where.roomId !== "number") {
      throw new Error("{ roomId } required");
    }
    return await this.dbModel.count({
      where: {
        roomId: where.roomId
      }
    });
  }

  async history(roomId, limit = 20, offset = 0) {
    let messages = await this.dbModel.findAll({
      order: [['createdAt', 'DESC']],
      where: {
        roomId
      },
      include: [{
        model: this.dbStorage.db.ChatMessageAttachment,
        as: 'attachments',
        include: [{
          model: this.dbStorage.db.File,
          as: 'file'
        }]
      }],
      attributes: this.attributes({}),
      limit: limit,
      offset
    });

    let results = messages.map(m => {
      let result = m.toJSON();
      if(result.attachments && result.attachments.length > 0) {
        result.attachments = result.attachments.map(a => {
          a.file = this.fileService.formatEntry(a.file);
          return a;
        });
      }
      return result;
    });

    results.reverse();
    
    return results;
  }

  protected async entryCreated(data: ax.IChatMessage, entry: any):Promise<any> {
    $log.debug('ChatMessageService::ENTRY' + entry.toJSON());
    const userIds = data.userIds;
    EventManager.emitChatMessage({
      message: {id: entry.id},
      userIds
    })
    return await this.find(entry.id);
  }

  /**
   * Query list of all entries
   * @returns {ax.IChatMessage[]}
   */
  public query(): PromiseLike<any[]> {
    return this.dbModel.findAll({ 
      order: this.order(),
      where: {
        userId: this.dbStorage.currentUser.id
      }
    });
  }

  attributes({}):any[] {
    return [
      ...Object.keys(this.dbModel.attributes),
      [this.se.literal(`
      EXISTS(SELECT FROM public."ChatMessagesStatus"
      WHERE public."ChatMessagesStatus"."messageId" = "ChatMessage"."id")
      `),'messageStatus']
    ];
  }
}