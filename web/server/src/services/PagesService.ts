import { Service } from "@tsed/common";
import DBStorage from './DBStorage';
import UsersService from './UsersService';
import FileService from './FileService';
import BaseEntryService from '../common/BaseEntryService';
import * as se from "sequelize";

@Service()
export default class PagesService extends BaseEntryService<ax.IPage> {
  constructor(
    private userService: UsersService,
    private fileService: FileService
  ) {
    super('File');
    this.allowedFields = {
      update: ['content', 'userId'],
      create: ['content', 'userId']
    }
  }

  public queryDocumentPages(docId: number, docVersionId:number = 0): PromiseLike<any> {
    let where = docVersionId ? {
      docId,
      docVersionId
    }: {
      docId: docId,
      actual: true
    };

    return this.dbModel.findAll({
      order: [['pageNumber', 'ASC']],
      where,
      include: [{
        model: this.dbStorage.db.Descriptor,
        as: 'descriptors'
      }]
    }).then(pages => {
      return pages.map(p => {
        const page = p.toJSON();
        return {
          ...this.fileService.formatEntry(page),
          descriptors:page.descriptors
        };
      });
    });
  }

  public queryDocumentVersionPages(docId: number, docVersionId:number): PromiseLike<any> {
    return this.dbModel.findAll({
      order: [['pageNumber', 'ASC']],
      where: {
        docId: docId,
        docVersionId: docVersionId
      },
      include: [{
        model: this.dbStorage.db.Descriptor,
        as: 'descriptors'
      }]
    }).then(pages => {
      return pages.map(p => {
        const page = p.toJSON();
        return {
          ...this.fileService.formatEntry(page),
          descriptors:page.descriptors
        };
      });
    });
  }

  public delete(id:number): Promise<{ id }> {
    return this.fileService.delete(id).then(res => {
      // todo: reorder pages
      return res;
    });
  }

  
  async activate(id:number, actual:boolean): Promise<any> {
    let res = await this.find(id);
    let p = res.toJSON();
    if(actual && p.docId) {
      await this.dbModel.update({
        actual: false
      },{
        where: {
          docId: p.docId,
          pageNumber: p.pageNumber,
          id: {
            $not: p.id
          }
        }
      });
    }
    res.set('actual', <any>actual);
    await res.save();
    return res.toJSON();
  }

  /**
   * @inheritdoc
   */
  protected prepareEntry(entry: ax.IPage, operation: 'update' | 'create') {
    if (operation === 'create') {
      entry.createdBy = this.dbStorage.currentUser.id;
    }
    entry.modifiedBy = this.dbStorage.currentUser.id;
    // add file id
    super.prepareEntry(entry, operation);
  }

  async processUploadPages(files:any[], docId:number, docVersionId:number = 0, pageNumber:number=0) {
    // todo: validate only images
    let prepared = Object.keys(files).map((key, index) => {
      return this.fileService.prepareFileInfo(
        files[key], {docId, docVersionId, pageNumber:pageNumber + index});
    });

    if (prepared.find(v => v.type != 0))
      throw new Error("Only images can be uploaded");

    // console.log(`pages number uploaded ${prepared.length}`);

    return await this.fileService.saveFiles(prepared);
  }

  async setPageNumber(pageList:any[]) {
    let result = await Promise.all(pageList.map(p => {
      return this.dbModel.update({
        pageNumber: p.pageNumber
      }, { where: { id: p.id } });
    }));
    let activateResult = await Promise.all(pageList.map(p => {
      if(typeof p.actual !== 'undefined') 
        return this.activate(p.id, p.actual);
      else 
        return Promise.resolve();
    }));
    return { success: true };
  }

}