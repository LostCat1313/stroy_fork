import { Service } from "@tsed/common";
import DBStorage from './DBStorage';
import BaseEntryService from '../common/BaseEntryService';
import * as se from "sequelize";


@Service()
export default class RegistrationIdsService extends BaseEntryService<ax.IRegistrationId> {
  constructor(){
    super('RegistrationId');
    this.allowedFields = {
      update: ['regId', 'deviceType'],
      create: ['regId', 'deviceType']
    }
  }

  /**
   * Create a new entry
   * @param entry
   * @returns {{id: any, name: string}}
   */
  async create(entry: ax.IRegistrationId): Promise<any> {
    this.prepareEntry(entry, 'create');
    const e = await this.dbModel.create({
      ...entry,
      userId: this.dbStorage.currentUser.id
    });
    const result = await this.entryCreated(entry, e);
    return result;
  }


  async register(regId, deviceType) {
    if(!this.dbStorage.currentUser) throw new Error("you should login first and initialize session");
    return this.registerUser(this.dbStorage.currentUser.id, regId, deviceType);
  }

  async registerUser(userId, regId, deviceType) {
    const existing = await this.dbModel.find({where: { regId }});
    
    if(existing) {
      if(existing.get('userId') != userId ) {
        existing.set('userId', userId);
        return await existing.save();
      }
      // throw new Error(`regId ${regId} already exists for current user`);
    }
    else {
      return await this.dbModel.create({
        regId,
        userId,
        deviceType
      });
    }
  }

  protected prepareEntry(entry: any, operation: 'update' | 'create') {
    super.prepareEntry(entry, operation);
    if(this.dbStorage.currentUser) {
      entry.userId = this.dbStorage.currentUser.id;
    }
  }
}