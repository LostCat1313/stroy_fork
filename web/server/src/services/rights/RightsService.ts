import { Service } from "@tsed/common";
import * as se from 'sequelize';

import BaseEntryService from '../../common/BaseEntryService';
import { IRightModel, IRightInstance } from "../../common/DBTypes";

@Service()
export default class RightsService extends BaseEntryService<ax.IRight, IRightModel> {
  constructor(
  ) {
    super('Right');
    this.allowedFields = {
      update: ['refId', 'entity', 'operation', 'sign'],
      create: ['refId', 'entity', 'operation', 'sign']
    }
  }

  querySpecificRights(refId:number,entity:string) {
    return this.dbModel.findAll({
      where: {
        entity,
        refId
      }
    })
  }

  /**
   * Rights avaliable values
   * @returns {any}
   */
  public getConfig():any {
    return {
      operations: this.dbStorage.db.Right.operations,
      entities: this.dbStorage.db.Right.entities
    };
  }

}
