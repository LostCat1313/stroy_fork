import { Service } from "@tsed/common";
import * as se from 'sequelize';

import BaseEntryService from '../../common/BaseEntryService';
import { IRightModel, IRightInstance } from "../../common/DBTypes";
import RightsService from "./RightsService";
import { serverInstance } from '../../app';
import DBStorage from "../DBStorage";

@Service()
export default class AccessControlService {

  protected dbStorage:DBStorage;

  constructor(
    private rightsService:RightsService
  ) {
    this.dbStorage = serverInstance.injector.get<DBStorage>(DBStorage);    
  }

  /**
   * Rights avaliable values
   * @returns {any}
   */
  public getConfig():any {
    return {
      operations: this.dbStorage.db.Right.operations,
      entities: this.dbStorage.db.Right.entities
    };
  }

  async check(entity:string, operation:string, params:any) {
    return false;
  }

  isAdmin() {
    return this.dbStorage.currentUser.isAdmin;
  }

}
