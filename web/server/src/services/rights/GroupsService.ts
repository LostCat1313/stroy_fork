import { Service } from "@tsed/common";
import * as se from 'sequelize';

import BaseEntryService from '../../common/BaseEntryService';
import RightsService from "./RightsService";

@Service()
export default class GroupsService extends BaseEntryService<ax.IGroup> {
  constructor(
    private rightsService:RightsService
  ) {
    super('Group');
    this.allowedFields = {
      update: ['name', 'parentId'],
      create: ['name', 'parentId']
    }
  }

  /**
   * Query list of all entries
   * @returns {Promise<any[]>}
   */
  public async queryWithUsers():Promise<any[]> {
    return await this.dbModel.findAll({
      order: this.order(),
      include: [{
        model: this.dbStorage.db.User,
        as: 'users'
      },{
        model: this.dbStorage.db.Right,
        as: 'rights'
      }],
      limit: this.dbStorage.limit,
      offset: this.dbStorage.offset
    });
  }

  /**
   * Query list of all group users
   * @returns {Promise<any[]>}
   */
  async usersIndex(id:number) {
    let group = await this.find(id);
    return (<any>group).getUsers();
  }

  /**
   * Update group entry
   * @returns {Promise<se.Instance<ax.IGroup>>}
   */
  async update(id:number, entry:ax.IGroup): Promise<se.Instance<ax.IGroup>> {
    const {userIds, rightCodes} = entry;
    let result = await super.update(id, entry);
    if(userIds) {
      let users = await this.dbStorage.db.User.findAll({where: {id: {$in: userIds}}});
      (<any>result).setUsers(users);
    }
    if(rightCodes) {
    }
    return result;
  }

  /**
   * Update list of group users
   * @returns {Promise<{message:string}>}
   */
  async updateUsers(id:number, ids:number[]) {
    let group = await this.dbModel.findById(id);
    let users = await this.dbStorage.db.User.findAll({where: {id: {$in: ids}}});
    (<any>group).setUsers(users);
    return {message: 'success'};
  }
}
