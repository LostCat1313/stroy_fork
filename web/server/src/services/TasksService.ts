import { Service } from "@tsed/common";
import DBStorage from './DBStorage';
import BaseEntryService from '../common/BaseEntryService';
import * as se from "sequelize";
import * as path from 'path';

@Service()
export default class TasksService extends BaseEntryService<ax.ITask> {
  constructor() {
    super('Task');
    this.allowedFields = {
      update: ['docId', 'refId', 'type', 'status'],
      create: ['docId', 'refId', 'type', 'status']
    }
  }

  public planDocumentTask(docId, refId, fileId) {
    this.dbStorage.db.File.findById(fileId).then((file:any)=> {
      if(file) {
        this.generateTask(docId, refId, file)
      } else {
        // console.log("TaskService::plan document failed - file not found ", fileId)
      }
    }, (err) => console.log("TaskService::plan document failed", err));
  }

  public planThumbTask(file) {
    if(file.type == 0) {
      // console.log('plan thumb task>>>>>');
      this.generateTask(0, 0, file);
    }
  }

  public getDocumentStatus(docId) {
    return this.dbModel.count({
      where: {
        docId: docId,
        $or: [ {status: 1}, {status: 2} ]
      }
    }).then(v => {
      return { ready: v > 0 ? 0 : 1 }; // 0 - processing, 1 - ready
    });
  }

  private generateTask(docId, refId, file) {
    switch(file.type) {
      case 1:
        // render the document
        this.create(<any>{
          docId: docId,
          refId: refId,
          status: 1,
          type: path.extname(file.original).toLowerCase() === '.pdf' ? 1 : 2
        });
        break;
      case 0:
        // thumbnail for file
        // this.create(<any>{
        //   docId: docId,
        //   refId: file.id,
        //   status: 1,
        //   type: 4
        // });
        break;
    }
  }

  protected initializeSubscriptions() {
    // possibly mark task to check it ?
    // this.eventManager.on(EventManager.FILE_THUMB_NOT_FOUND, (file) => {
    //   if(file.docId) {  // page
    //     this.dbModel.findOne({
    //       where: {
    //         refId: file.id,
    //         type: 4
    //       }
    //     }).then(task => {
          
    //     });
    //   } 
    //   else {
    //     this.dbStorage.db.Document.findOne({
    //       where: {
    //         fileId: file.id
    //       }
    //     }).then(doc => {
    //       if(!doc) return; // no document for document file
    //     });
    //   }
    // });
  }
}