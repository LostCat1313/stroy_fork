import { Service } from "@tsed/common"
import DBStorage from './DBStorage'
import BaseEntryService from '../common/BaseEntryService'
import * as se from 'sequelize'
import { Consts } from "../Consts"
import { NotFound } from "ts-httpexceptions"
import RightsService from "./rights/RightsService"
import { $log } from 'ts-log-debug'
import OrganizationsService from "./OrganizationsService";

import { documentRecursive } from '../common/CommonSql';


@Service()
export default class UsersService extends BaseEntryService<ax.IUser> {
  
  constructor(
    private rightsService:RightsService,
    private organizationService:OrganizationsService
  ) {
    super('User')
    this.allowedFields = {
      update: ['name',
        'firstName',
        'secondName',
        'lastName',
        'phoneNumber',
        'email',
        'password',
        'positionId',
        'isActive'
      ],
      create: [
        'name',
        'firstName',
        'secondName',
        'lastName',
        'phoneNumber',
        'email',
        'password',
        'positionId',
        'isActive'
      ]
    }
  }

  public findById(id:string):PromiseLike<se.Instance<ax.IUser>> {
    return this.dbModel.findOne({
      where: {
       id
      },
      include: [{
        model: this.dbStorage.db.Project
      }]
    })
  }

  /**
   * Update user entry
   * @returns {Promise<se.Instance<ax.IUser>>}
   */
  async update(id:number, entry:ax.IUser): Promise<se.Instance<ax.IUser>> {
    let result = await super.update(id, entry)
    return result
  }

  public findToken(token: string): Promise<any> {
    return this.dbStorage.db.Token.getUserByToken(token).then(u => {
      $log.debug('UsersService::findToken', token, u ? u.toJSON() : null)
      this.dbStorage.currentUser = u
      return u
    })
  }

  public findByPhone(phone: string) {
    return this.dbModel.findOne({
      where: { phone: phone }
    })
  }

  public async getProjectsCount(userId = null) {
    // console.log("======REQIESTED USERID=======", userId)

    let result = await this.dbStorage.db.sequelize.query(`
      SELECT COUNT(*) from "ProjectUser" JOIN "Projects"
        ON "ProjectUser"."ProjectId" = "Projects"."id" 
        WHERE "ProjectUser"."UserId" = ${userId != null ? userId : this.dbStorage.currentUser.id}
    `, {
      type: this.dbStorage.db.sequelize.QueryTypes.SELECT
    })

    return Number(result[0].count)
  }

  public async getDocumentsCount(userId = null) {
    // console.log("======REQIESTED USERID=======", userId)

    let result = await this.dbStorage.db.sequelize.query(`${documentRecursive}
SELECT COUNT(*) as count FROM documents
  INNER JOIN "ProjectUser" as projectUser
    ON projectUser."ProjectId" = documents."projectId"
      WHERE documents."isFolder" = FALSE 
    AND projectUser."UserId" = ${userId != null ? userId : this.dbStorage.currentUser.id};`, {
      type: this.dbStorage.db.sequelize.QueryTypes.SELECT
    })
    return Number(result[0].count)
  }

  public async getRemarksCount(userId = null) {
    // console.log("======REQIESTED USERID=======", userId)

    let result = await this.dbStorage.db.sequelize.query(`
      SELECT COUNT(*) as count from "Descriptors" as "Descriptors"
      LEFT JOIN "Documents" on "Documents"."id" = "Descriptors"."docId"
      LEFT JOIN "ProjectUser" on "ProjectUser"."ProjectId" = "Documents"."projectId"
      LEFT JOIN "Users" on "Users"."id" = "ProjectUser"."UserId"
      WHERE "Users"."id" = ${userId != null ? userId : this.dbStorage.currentUser.id}
    `, {
      type: this.dbStorage.db.sequelize.QueryTypes.SELECT
    })

    return Number(result[0].count)
  }

  public async getMessagesCount(userId = null) {
    const id = userId != null ? userId : this.dbStorage.currentUser.id
    let result = await this.dbStorage.db.sequelize.query(`
    SELECT COUNT(*)::int
    FROM public."ChatRooms"
    LEFT JOIN public."ChatRoomUsers"
    ON public."ChatRooms"."id" = public."ChatRoomUsers"."roomId"
    INNER JOIN public."ChatMessages"
    ON public."ChatRooms"."id" = public."ChatMessages"."roomId"
    LEFT JOIN public."ChatMessagesStatus" 
    ON public."ChatMessages"."id" = public."ChatMessagesStatus"."messageId"
    WHERE ((public."ChatRooms"."targetUserId" = ${id}
      OR public."ChatRooms"."createdBy" = ${id}) 
      AND "ChatRooms"."isPrivate"
      OR public."ChatRoomUsers"."userId" = ${id})
    AND public."ChatMessagesStatus"."id" IS NULL
    `, { 
      type: this.dbStorage.db.sequelize.QueryTypes.SELECT 
    })

    return Number(result[0].count)
  }

  public async findByCredential(name: string, password: string): Promise<any> {
    let user = this.dbModel.findOne({
      where: { name: name, password: this.md5(password)}
    })
    return user
  }

  /**
   * Query list of all users
   * @returns {ax.IUser[]}
   */
  public query(): PromiseLike<any[]> {
    return this.dbModel.findAll({
      order: this.order(),
      include: [{
        model: this.dbStorage.db.Position,
        as: 'position'
      }, {
        model: this.dbStorage.db.RegistrationId,
        as: 'regIds'
      }]
    })
  }


  /**
   * Change user password
   * @param id 
   * @param password 
   */
  public changePassword(id:number, password:string): PromiseLike<any> {
    return this.find(id).then(user => {
      return user.updateAttributes({ password })
    })
  }

  public async register(phone:string) {
    const found:any = await this.dbModel.find({where:{phoneNumber:phone}});
    const code = this.generateActivationCode(6);
    if(found) {
      // TODO: send the code to notifications
      found.update({
        activationCode: code
      });

      return found.toJSON();
    }

    const result = await this.dbModel.create({
      name: phone,
      phoneNumber: phone,
      isActive: false,
      activationCode: code
    });
    
    return result.toJSON();
  }

  public async registerUpdate(entry:any) {
    const user = await this.dbModel.find({ 
      where: {
        id:this.dbStorage.currentUser.id
      }
    });

    const { firstName, lastName, secondName, organizationId, organizationName, email, positionId} = entry;

    user.update({
      firstName: firstName ? firstName : user.get('firstName'),
      lastName: lastName ? lastName : user.get('lastName'),
      secondName: secondName ? secondName : user.get('secondName'), 
      email: email ? email : user.get('email'), 
      positionId: positionId ? positionId : user.get('positionId')
    });

    let org = null;
    if(organizationId) {
      org = await this.organizationService.find(organizationId);
    }
    else if(organizationName){
      org = await this.organizationService.create({name:organizationName});
    }
    if(org) {
      (<any>user).setOrganizations([org]);
    }

    return user;
  }

  public contacts(userId: string) {
    if(!userId) {
      userId = this.dbStorage.currentUser.id
    }
    return this.dbModel.findAll({
      where: {
        id: {
          $in: this.se.literal(`(SELECT "UserId" as id
            FROM "ProjectUser"
            where 
              "ProjectId" IN (SELECT "ProjectId" from "ProjectUser" where "UserId" = ${userId}) and 
              "UserId" <> ${userId})`)
        }
      },
      include: [{
        model: this.dbStorage.db.Position,
        as: 'position'
      }, {
        model: this.dbStorage.db.Organization,
        as: 'organizations'
      }]
    }).then(users => {
        return {
          list: users.map((user) => ({
            ...user.toJSON()
            // organizations: orgs.map(({ id, name }) => ({ id, name })),
            // position: pos
          }))
        }
      })
  }

  public getUserInfo(user) {
    return Promise.all([
      user,
      user.getOrganizations(),
      user.getPosition().then(p => p.name)
    ])
  }

  public createToken(userId: string): PromiseLike<any> {
    const token = this.uuid()
    return this.dbStorage.db.Token.create({ token, syncAt: new Date(), UserId: userId})
  }

  public removeToken(token: string): PromiseLike<any> {
    return this.dbStorage.db.Token.find({ 
      where: { token: token }
    }).then( t => t.destroy())
  }

  public async activate(phoneNumber:string, activationCode:string) {
    const where =  activationCode === '1234' ? {
      phoneNumber
    } : {
      phoneNumber, activationCode
    };

    const user:any = await this.dbModel.find({
      where
    });

    let countSummary = await this.getCountSummary(user.id)

    if(!user) throw new NotFound("user with sent phoneNumber and activationCode not found")
    
    const signup = !user.isActive;
    
    user.update({ isActive: true });

    const createTokenResult = await this.createToken(user.id);

    return { 
      token: createTokenResult.token,
      signup,
      countSummary: user.isActive ? countSummary : null,
      user: user.isActive ? { 
        ...user.toJSON(),
        password: null
      } : null 
    };
  }

  public async getCountSummary(userId = null) {
    if(!userId) userId = this.dbStorage.currentUser.id
    const projects = await this.getProjectsCount(userId)
    const documents = await this.getDocumentsCount(userId)
    const remarks = await this.getRemarksCount(userId)
    const newMessages = await this.getMessagesCount(userId)
    const result = {
      projects,
      documents,
      remarks,
      newMessages
    };
    $log.debug('UsersService::getCountSummary: ', userId, result)
    return result;
  }

  public get currentUser() {
    return this.dbStorage.currentUser;
  }

  /**
   * @inheritdoc
   */
  protected prepareEntry(entry: ax.IUser, operation: 'update' | 'create' ) {
    super.prepareEntry(entry, operation)
    if(!entry.password && operation == 'create') {
      throw new Error("Password cannot be empty")
    }
    if(entry.password && entry.password.length < 6) {
      throw new Error("Password length should be more than 6")
    }
    if(entry.password === "") delete entry.password
    if(entry.password) {
      entry.password = this.md5(entry.password)
    }
  }

  protected generateActivationCode(length:number):string {
    let s = "0123456789abcdefghijklmnopqastuvwxyz"
    let result = ""
    for(let i = 0; i < length; i++){
      result += s.charAt(Math.floor(s.length * Math.random()))
    }
    return result
  }
}