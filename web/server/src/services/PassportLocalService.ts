
import { 
  Service,
  BeforeRoutesInit,
  AfterRoutesInit,
  ExpressApplication,
  Inject,
  ServerSettingsService
} from '@tsed/common'
import * as Passport from 'passport'
import { Strategy } from 'passport-local'
import { $log } from 'ts-log-debug'

import UsersService from './UsersService'
import ProjectsService from './ProjectsService'
import BearerStrategy from '../utils/TokenStrategy'
import RightsService from './rights/RightsService'

@Service()
export default class PassportLocalService implements BeforeRoutesInit, AfterRoutesInit {

  constructor(
    private usersService: UsersService,
    private projectsService: ProjectsService,
    private rightsService: RightsService,
    private serverSettings: ServerSettingsService,
    @Inject(ExpressApplication) private  expressApplication: ExpressApplication
  ) {

    // used to serialize the user for the session
    Passport.serializeUser(PassportLocalService.serialize)

    // used to deserialize the user
    Passport.deserializeUser(this.deserialize.bind(this))
  }

  $beforeRoutesInit() {
    const options: any = this.serverSettings.get("passport") || {} as any
    const {userProperty, pauseStream} = options

    this.expressApplication.use(Passport.initialize({userProperty, session: false}))
      .use(Passport.session({pauseStream}))
      .use(/^\/api\/(?!(authenticate)).*/, this.middlewareToken())
  }

  $afterRoutesInit() {
      this.initializeLogin()
      // this.initializeSignup()
  }

  /**
   *
   * @param user
   * @param done
   */
  static serialize(data, done) {
    // console.log(data)
    done(null, data.userId)
  }

  /**
   *
   * @param id
   * @param done
   */
  public deserialize(id, done) {
    this.usersService.find(id).then(user => {
      done(null, user)
    })
  }

  // =========================================================================
  // LOCAL LOGIN =============================================================
  // =========================================================================
  // we are using named strategies since we have one for login and one for signup
  // by default, if there was no name, it would just be called 'local'

  public initializeLogin() {

    Passport.use('login', new Strategy({
      // by default, local strategy uses username and password, we will override with email
      usernameField: 'username',
      passwordField: 'password',
      session: false,
      passReqToCallback: true // allows us to pass back the entire request to the callback
    }, this.onLocalLogin))

  }

  private onLocalLogin = async (req, username, password, done) => {
    //$log.debug('LocalLogin', login, password)
    let user = await this.usersService.findByCredential(username, password)

    if (!user) {
      return done(null, false) // req.flash is the way to set flashdata using connect-flash
    }

    // all is well, return successful user
    let d = await this.usersService.createToken(user.id)
    
    let countSummary = await this.usersService.getCountSummary(user.id)
    let rights = this.rightsService.getConfig()

    return done(null, {
      userId: user.id,
      user: {
        ...user.toJSON(),
        password: null
      },
      token: d.token,
      countSummary,
      rights
    }, { message: 'User is ok!' })
  }

  public logout(req) {
    const header = req.headers.authorization
    if (!header) {
      return
    }
    const token = header.split(' ').pop()
    if (!token) {
      return
    }

    return this.usersService.removeToken(token)
  }


  public initTokenAuthentication() {
    Passport.use(new BearerStrategy(this.onTokenAuthentication))
  }

  private onTokenAuthentication = (token, done) => {
    // console.log("ouch", token)
    return this.usersService.findToken(token).then(user => {
      $log.debug("PassportLocalService::onTokenAuthentication: ", user.toJSON())
      if (!user) { return done(null, false) }
      user.token = token
      return done(null, user, { scope: 'all' })
    })
  }

  public middlewareToken() {
    this.initTokenAuthentication()
    return Passport.authenticate('bearer', { session: false })
  }
}