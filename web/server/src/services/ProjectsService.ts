import { Service } from "@tsed/common";
import * as se from 'sequelize'
import { $log } from 'ts-log-debug';

import DBStorage from './DBStorage';
import UsersService from './UsersService';
import BaseEntryService from '../common/BaseEntryService';
import EventManager from "./events/EventManager";

@Service()
export default class ProjectsService extends BaseEntryService<ax.IProject> {
  constructor(
    private usersService: UsersService
  ){
    super('Project');
    this.allowedFields = {
      update: ['title', 'imageId', 'createdBy', 'modifiedBy'],
      create: ['title', 'imageId', 'createdBy', 'modifiedBy']
    }
  }

  /**
   * @inheritdoc
   */
  public getByIds(ids:any[]): PromiseLike<any[]> {
    let where:any = {
    };
    if(ids) {
      where = {
        id: {
          $in: ids          
        }
      }
    }
    return this.dbModel.findAll({
      order: this.order(),
      where,
      attributes:this.attributes(),
      include:this.include()
    });
  }

  protected async entryCreated(data: ax.IProject, entry: any):Promise<any> {
    // add default rights to current user
    await this.dbStorage.db.ProjectUserRight.create({
      UserId: this.dbStorage.currentUser.id,
      ProjectId: entry.id,
      rightMask: 7, // all rights
    });
    return await super.entryCreated(data, entry)
  }

  /**
   * Find an entry by its ID.
   * @param id
   * @returns {Promise<se.Instance<ax.IProject>>}
   */
  public find(id: number): PromiseLike<se.Instance<ax.IProject>> {
    return this.dbModel.findById(id, {
      attributes: this.attributes(),
      include: this.include()
    });
  }

  attributes():any[] {
    return [
      ...Object.keys(this.dbModel.attributes),
      [this.se.literal(`(
        SELECT COUNT("Documents"."id") FROM "Documents"
        WHERE "Documents"."projectId" = "Project"."id" 
          AND NOT "Documents"."isFolder"
      )`), 'documentsCount'],
      [this.se.literal(`(
        SELECT COUNT("ProjectUser"."id") FROM "ProjectUser"
        WHERE "ProjectUser"."ProjectId" = "Project"."id" 
      )`), 'usersCount']
    ]
  }

  include():any[] {
    return [{
      model: this.dbStorage.db.File,
      as: 'image'
    }, {
      model: this.dbStorage.db.User,
      as: 'users',
      include: [{
        model: this.dbStorage.db.ProjectUserRight,
        as: 'rights',
        where: this.se.literal(`"users->rights"."ProjectId" = "users->ProjectUser"."ProjectId"`)
      }]
    }
    // , {
    //   model: this.dbStorage.db.ProjectUserRight,
    //   as: 'rights',
    //   where: {
    //     UserId: this.dbStorage.currentUser.id
    //   }
    // }
    ]
  }

  async getUserProjectsIds() {
    const query = `
      SELECT * FROM "ProjectUser" WHERE "UserId" = ${this.dbStorage.currentUser.id}
    `;
    const result = await this.dbStorage.db.sequelize.query(query, {
      type: this.dbStorage.db.sequelize.QueryTypes.SELECT
    });
    
    return result.map(i => i.ProjectId);
  }

  async getProjectUserIds(projectId) {
    if(!projectId) throw new Error('projectId required');
    const query = `
      SELECT * FROM "ProjectUser" WHERE "ProjectId" = ${projectId}
    `;
    const result = await this.dbStorage.db.sequelize.query(query, {
      type: this.dbStorage.db.sequelize.QueryTypes.SELECT
    });
    
    return result.map(i => i.UserId);
  }

  getContacts(projectId) {
    // return this.find(projectId)
    //   .then(project => (<any>project).getUsers())
    //   .then(users => Promise.all(users.map(u => this.usersService.getUserInfo(u))))
    //   .then(users => {
    //     return {
    //       users: users.map(([user, orgs, pos]) => { 
    //         return {
    //           id: user.id,
    //           name: user.name,
    //           phoneNumber: user.phone_number,
    //           organizations: orgs.map(({id, name}) => ({id, name})),
    //           position: pos
    //         };    
    //       })
    //     }
    //   });
  }

  public async getProjectUsers(projectId) {
    const db = this.dbStorage.db;
    const list = await db.User.findAll({
      include: [{
        model: db.ProjectUserRight, as: 'rights', where: {
          ProjectId: projectId
        }
      }]
    });
    return { list: list.map( i => {
      let json = i.toJSON();
      const rightMask = json.rights && json.rights.length > 0 ? json.rights[0].rightMask || 0 : 0;
      delete json.rights;
      return {
        ...json,
        rightMask
      };
    })};
  }

  public async setProjectUsersRights(projectId:number, rights:{UserId, rightMask}[]) {
    const db = this.dbStorage.db;

    let existingRights = (await db.ProjectUserRight.findAll({
      where: { ProjectId:projectId }
    })).reduce((acc, cur:any) => {
      if(!cur.UserId) return acc;
      return { ...acc, [cur.UserId]: cur.rightMask };
    }, {}); 

    $log.debug('ProjectService::setProjectUsersRights', existingRights);

    const newRights = rights.map(r => {
      if(!existingRights[r.UserId]) {
        // notify that user added to project
        EventManager.emitProjectUserAdded({
          userId: r.UserId,
          projectId,
          rightMask: r.rightMask
        });
      } else if(existingRights[r.UserId] != r.rightMask) {
        // notify that right changed
        EventManager.emitProjectUserChanged({
          userId: r.UserId,
          projectId,
          rightMask: r.rightMask
        });
      }

      delete existingRights[r.UserId];

      return { 
        ProjectId: projectId, 
        UserId: r.UserId, 
        rightMask: r.rightMask 
      };
    });

    Object.keys(existingRights).forEach((userId:any) => {
      // notify that user was removed from project
      EventManager.emitProjectUserRemoved({
        userId,
        projectId
      });
    });

    const destroyed = await db.ProjectUserRight.destroy({
      where: { ProjectId:projectId }
    });

    const result = await db.ProjectUserRight.bulkCreate(newRights);

    return result;
  }

  // deprecated
  public async updateProjectUsers(projectId:number, ids:number[]) {
    const project:any = await this.find(projectId);
    const users = await this.dbStorage.db.User.findAll({where: {id: {$in: ids}}});
    

    project.setUsers(users);

    return { 
      message: 'success'
    };
  }
  

  /**
   * @inheritdoc
   */
  protected prepareEntry(entry: any, operation: 'update' | 'create') {
    if(entry.image) {
      entry.imageId = entry.image.id;
    }
    super.prepareEntry(entry, operation);
  }
}