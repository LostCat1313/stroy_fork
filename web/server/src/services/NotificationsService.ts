import { Service } from '@tsed/common';
import { $log } from 'ts-log-debug';

import DBStorage from './DBStorage';
import UsersService from './UsersService';
import BaseEntryService from '../common/BaseEntryService';
import {EventTypes} from './events/EventTypes';
import * as se from 'sequelize';
import * as common from 'axiom-common-db';
import * as admin from 'firebase-admin';
import { events } from 'axiom-common-db';

import serviceAccount from '../config/axiom-fcm';

// console.log("PUSHNOTIFICATIONS::Initialization...", serviceAccount);

admin.initializeApp({
  credential: admin.credential.cert(<any>serviceAccount),
  databaseURL: "https://axiom-463e2.firebaseio.com"
});


@Service()
export default class NotificationsService extends BaseEntryService<ax.INotification> {
  constructor(
    private userService: UsersService
  ) {
    super('Notification');
    this.allowedFields = {
      update: ['eventId', 'userId', 'status'],
      create: ['eventId', 'userId', 'status']
    }
  }

  async createForUsers(eventId:number, userIds:number[], model: any) {
    // let rids = await this.dbStorage.db.RegistrationId.findAll({
    //     where: {
    //       userId: { $in: userIds }
    //     }
    //   });

    $log.debug("NotificationsService::createForUsers:", userIds, userIds.length, model);

    const notifications = await Promise.all(userIds.map(async (userId:any) => {
      let result = {
        userId,
        rId: null,
        eventId: eventId,
        status: 0
      }
      const r:any = await this.dbStorage.db.RegistrationId.find({where: {userId}});
      const badge = String(await this.userService.getMessagesCount(userId));
      if(!r) return result;
      try {
        result.status = 1;
        const payload = {
          notification: {
            title: model.title,
            body: model.content,
            sound: 'default',
            // TODO: count new messages
            badge
          },
          data: {
            event: JSON.stringify(model)
          }
        };
        $log.debug("PUSHNOTIFICATIONS::trying to send:", r.regId, model);
        const response = await admin.messaging().sendToDevice(r.regId, payload);
        $log.debug("PUSHNOTIFICATIONS::sent results:", JSON.stringify(response));
        result.status = response.results.length > 0 && response.results[0].error ? 2 : 1;
      } catch (e) {
        result.status = 2;
        $log.error("PUSHNOTIFICATIONS::Error", e);
      }
      return result;
    }));

    $log.debug("NotificationsService::notifications:", notifications);    

    return await this.dbModel.bulkCreate(notifications);
  }

  commonInclude() {
    return common.events.types.include(this.dbStorage.db);
    //return this.se.literal(`"Event"."type" = ${type}`);
  }

  /**
   * Query list of all entries
   * @returns {ax.IUser[]}
   */
  public async queryForCurrentUser(): Promise<any[]> {
    const query = {
      where: this.se.literal(`(
        SELECT COUNT("Notifications"."id") FROM "Notifications"
        WHERE "Notifications"."eventId" = "Event"."id" 
          AND "Notifications"."userId" = ${this.dbStorage.currentUser.id}
      )>0`),
      order: [[ 'createdAt', 'DESC' ]],
      limit: this.dbStorage.limit,
      offset: this.dbStorage.offset,
      include: this.commonInclude()
    };
    // console.log(query);
    return await this.dbStorage.db.Event.findAll(<any>query);
    // return await this.dbModel.findAll({ 
    //   order: this.order(),
    //   include: [{
    //     model: this.dbStorage.db.Event
    //   }],
    //   where: {
    //     userId: this.dbStorage.currentUser.id
    //   },
    //   distinct: true,
    //   col: 'Notification.eventId'
    // });
  }
}