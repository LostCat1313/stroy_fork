import { Service } from "@tsed/common";
import DBStorage from '../DBStorage';
import UsersService from '../UsersService';
import BaseEntryService from '../../common/BaseEntryService';
import * as se from "sequelize";
import EventManager from "../events/EventManager";


@Service()
export default class DescriptorsService extends BaseEntryService<ax.IDescriptor> {
  constructor(
    private userService: UsersService
  ) {
    super('Descriptor');
    this.allowedFields = {
      create: [
        'type',
        'params',
        'title',
        'text',
        'docId',
        'holderId',
        'holderType',
        'createdBy',
        'modifiedBy'
      ],
      update: [
        'type',
        'params',
        'title',
        'text',
        'docId',
        'holderId',
        'holderType',
        'createdBy',
        'modifiedBy'
      ]
    }
  }

  /**
   * Query list of descriptors (markers) by docId & holderId
   * @returns {ax.IDescriptor[]}
   */
  public getDescriptors(docId, holderId, type): PromiseLike<se.Instance<ax.IDescriptor>[]> {
    let where = {};
    if(docId) where = { ...where, docId };
    if(holderId) where = { ...where, holderId };
    if(type) where = { ...where, type };

    return this.dbModel.findAll({
      where,
      order: this.order()
    });
  }

 /**
   * Query list of descriptors (markers) by docId & holderId
   * @returns {ax.IDescriptor[]}
   */
  public getProjectDescriptors(projectId, type): PromiseLike<se.Instance<ax.IDescriptor>[]> {
    let where = {};
    if(type) where = { ...where, type };
    return this.dbModel.findAll({
      where,
      include: [{
        model:this.dbStorage.db.Document,
        as: 'document',
        where: { projectId }
      }],
      order: this.order()
    });
  }

  /**
   * @inheritdoc
   */
  protected prepareEntry(entry: ax.IDescriptor, operation: 'update' | 'create') {
    entry.params = JSON.stringify(entry.params);
    super.prepareEntry(<any>entry, operation);
    // entry.userId = this.dbStorage.currentUser.id;
  }

  // TODO: Move to events?
  protected async entryCreated(data, entry) {
    this.dbStorage.db.Document.findById(data.docId).then((document:any) => {
      EventManager.emitProjectDocumentDescriptorAdded({ 
        entry: entry.toJSON(),
        projectId: document.projectId,
        currentUser: this.dbStorage.currentUser,
        userIds: []
      });
    });
    
    return entry;
  }
}