
import { Service } from "@tsed/common";
import BaseEntryService from '../../common/BaseEntryService';
import FileService from '../FileService';
import TaskService from '../TasksService';
import DocumentVersionsService from "./DocumentVersionsService";
import {NotFound} from "ts-httpexceptions";
import EventManager from "../events/EventManager";
import ProjectsService from "../ProjectsService";
import { documentRecursive } from "../../common/CommonSql";

@Service()
export default class DocumentsService extends BaseEntryService<ax.IDocument> {
  
  documentMapper = (entry) => {
    let jsonData = entry.toJSON ? entry.toJSON() : entry;
    return {
      ...jsonData,
      pagesCount: parseInt(entry.get("pagesCount")),
      pages: jsonData.pages ? jsonData.pages.map(p => this.fileService.formatEntry(p)) : [],
      file: entry.versions && entry.versions.length > 0 ? this.fileService.formatEntry(entry.versions[0].file) : null,
      versions: jsonData.versions ? jsonData.versions.map(v => {
        return {
          ...v,
          file: this.fileService.formatEntry(v.file),
          pages: v.pages ? 
            v.pages.map(p => this.fileService.formatEntry(p)) :
            [] 
        };
      }) : []
    };
  }

  constructor(
    private fileService: FileService,
    private taskService: TaskService,
    private projectsService: ProjectsService,
    private documentVersionService: DocumentVersionsService
  ) {
    super('Document');
    this.allowedFields = {
      update: ['title', 'projectId', 'fileId', 'parentId', 'isFolder', 'shortTitle'],
      create: ['title', 'projectId', 'fileId', 'parentId', 'isFolder', 'shortTitle']
    }
  }

  async find(id: number):Promise<any> {
    const e:any = await this.dbModel.find({
      where: { id },
      attributes: this.attributes(),
      include: this.include(),
      order: [
        [{model: this.dbStorage.db.DocumentVersion, as: 'versions'}, 'createdAt', 'DESC'],
        [{model: this.dbStorage.db.File, as: 'pages'}, 'pageNumber', 'ASC']
      ]
    });

    if(!e) {
      throw new NotFound('Document not found');
    }

    return this.documentMapper(e);
  }

  /**
   * Get last updated documents
   * @param projectId
   * @returns {Promise<ax.IDocument[]>}
   */
  async last(projectId?: string): Promise<ax.IDocument[]> {
    const clause = !projectId 
    // ALL PROJECTS FOR CURRENT USER
    ? `INNER JOIN "ProjectUser" as projectUser
ON projectUser."ProjectId" = documents."projectId"
  WHERE documents."isFolder" = FALSE AND projectUser."UserId" = ${this.dbStorage.currentUser.id}`
    // FOR REQUESTED PROJECT
    : `WHERE documents."isFolder" = FALSE AND documents."projectId" = ${projectId}`;

    const idsQuery = `(${documentRecursive} SELECT documents."id" FROM documents ${clause})`

    let docs = await this.dbModel.findAll(<any>{
      order: [["updatedAt", "DESC"]],
      limit: this.limit,
      offset: this.offset,
      attributes: this.attributes(),
      include: [{
        model: this.dbStorage.db.Project,
        as: 'project'
      }],
      where: { 
        isFolder: false,
        id: { $in: this.se.literal(idsQuery) }
      }
    });

    return docs.map(this.documentMapper);
  }


  public unfoldedIndex(projectId) {
    const projectIdCond = projectId 
      ? ' AND root."projectId" = ' + this.dbStorage.db.sequelize.escape(projectId) 
      : '';
    
    const query = `
      WITH RECURSIVE docs(id, "parentId", title, qty, "folderId", "sectionId") AS (
        SELECT folders.id, folders."parentId", folders.title, 0,
          folders.id AS "folderId",
          root.id AS "sectionId"
        FROM "Documents" AS root
        INNER JOIN "Documents" AS folders
        ON root.id = folders."parentId" AND
          root."parentId" IS NULL AND
          folders."isFolder" = TRUE ${projectIdCond}

        UNION ALL

        SELECT
          nested.id, nested."parentId", nested.title,
          CASE
            WHEN nested."isFolder" THEN 0
            ELSE 1
          END AS qty,
          d."folderId",
          "sectionId"
          FROM "Documents" AS nested
          INNER JOIN docs AS d ON nested."parentId" = d.id
        )
        SELECT id, title, 0 as "docsQty", null AS "sectionId" FROM "Documents"
        WHERE "parentId" IS null ${projectIdCond}

        UNION ALL

        SELECT min(id) AS id, min(title) AS title, sum(qty) AS docsQty, "sectionId" FROM docs
        GROUP BY "folderId", "sectionId" where "sectionId" != null
        `;

    return this.dbStorage.db.Project.findById(projectId).then(project => {
      // FIXME выделить в отдельную функцию
      this.dbStorage.db.sequelize.query(query, {
        type: this.dbStorage.db.sequelize.QueryTypes.SELECT
      }).then(result => {
        var sections = result
          .filter(d => d.sectionId === null)
          .map(s => ({
            id: s.id,
            title: s.title,
            isFolder: true,
            documents: []
          }));

        const innerFolders = result.filter(d => d.sectionId !== null);

        innerFolders.forEach(f => {
          const secInd = sections.findIndex(s => s.id === f.sectionId);
          sections[secInd].documents.push({
            id: f.id,
            title: f.title,
            isFolder: true,
            docsQty: f.docsQty
          });
        });

        return {
          project: {
            id: project.get('id'),
            title: project.get('title')
          },
          list: sections
        };
      });
    });
  }

  async folderIndex(projectId, parentId) {
    const project = await this.dbStorage.db.Project.findById(projectId);
    
    const documents = await this.dbModel.findAll({ 
        order: this.order(),
        where: {
          parentId: parentId ? parentId : null,
          projectId: projectId
        },
        attributes: this.attributes(),
        include: this.include()
      });

    return {
      project: {
        id: project.get('id'),
        title: project.get('title')
      },
      list: documents.map(this.documentMapper)
    };
  }

  public index(projectId, parentId, unfoldSections) {
    const isRootDir = !parentId;

    if (isRootDir && unfoldSections) {
      return this.unfoldedIndex(projectId);
    } else {
      return this.folderIndex(projectId, parentId);
    }
  }

  public base64ToBuffer(data) {
    return Buffer.from(data, 'base64');
  }

  // FIXME: проверять parentId. На верхнем уровне не должно быть документов
  public add(title, projectId, parentId, file = null) {

    var file = null;

    if (file) {
      file = null;
    }

    return this.dbModel.create(
      {
        title: title,
        parentId: parentId,
        projectId: projectId,
        isFolder: false,
        file: file
      });
  }

  

  public addFolder(title:string, projectId, parentId) {
    return this.dbModel.create(
      {
        title: title,
        parentId: parentId,
        projectId: projectId,
        isFolder: true
      });
  }
 
  async getDocumentHierarchy(projectId:number) {
    let res = await this.dbModel.findAll(<any>{
      where: {
        projectId
      },
      attributes: this.attributes(),
      include: this.include(),
      order: this.order()
    });
    
    let items = res.map(this.documentMapper);
    items.forEach((item:ax.IDocument) => {
      return item.children = item.isFolder 
        ? items.filter(sub => sub.parentId == item.id)
        : null ;
    });
    return items.filter(item => item.parentId == null);
  }

  protected order() {
    return [
      ['isFolder', 'DESC'],
      ['createdAt', 'DESC'],
      [{model: this.dbStorage.db.File, as: 'pages'}, 'pageNumber', 'ASC']
    ];
  }

  /**
   * @inheritdoc
   */
  protected prepareEntry(entry: any, operation: 'update' | 'create') {
    if(entry.file) {
      entry.fileId = entry.file.id;
    }
    entry.isFolder = !!entry.isFolder;
    super.prepareEntry(entry, operation);
  }

  // TODO: Move to events
  protected async entryCreated(data, entry):Promise<any> {
    // create version of document
    let result = entry;
    let version;
    if(data.fileId) {
      version = await this.documentVersionService.create(<any>{
        fileId: data.fileId,
        documentId: entry.id,
        title: "Document version"
      });

      let file = await this.fileService.find(data.fileId);
      let json = {
        file: this.fileService.formatEntry(file.toJSON()),
        ...version.toJSON()
      };
      // add file information here
      entry.dataValues.versions = [json];
      entry.dataValues.file = json.file;
    } else {
      // create empty version of document
      version = await this.documentVersionService.create(<any>{
        documentId: entry.id,
        title: "Document version"
      });
      entry.dataValues.versions = [{...version.toJSON()}];
    }

    EventManager.emitProjectDocumentAdded({
      entry: entry.toJSON(),
      version: version.toJSON(), // TODO: remove entry from create query!!
      projectId: entry.projectId,
      currentUser: this.dbStorage.currentUser
    });

    return entry;
  }

  protected entryUpdated(data, entry) {
    // console.log('DocumentsService::entryUpdated', data.fileId, entry.fileId);
    if(data.fileId != entry.fileId) {
      // this.taskService.planDocumentTask(entry.id, data.fileId);
    }
  }
  

  attributes(type:string = "document"):any[] {
    switch(type) {
      case "document":
        return [
          ...Object.keys(this.dbModel.attributes),
          [this.se.literal(`(
            SELECT COUNT("Files"."id")::int FROM "Files"
            WHERE "Files"."docId" = "Document"."id" AND "Files"."actual"
          )`), 'pagesCount']
        ];
      case "versions": 
        return [
          ...Object.keys(this.dbStorage.db.DocumentVersion.attributes),
          [this.se.literal(`(
            SELECT COUNT("Files"."id")::int FROM "Files"
            WHERE "Files"."docVersionId" = "versions"."id"
          )`), 'pagesCount']
        ];
      default: throw "type isn't defined";
    }
    
  }

  include():any[] {
    return [{
      model: this.dbStorage.db.File,
      as: 'pages',
      required: false,
      where: {
        id: {
          $in: this.se.literal(`(SELECT actual.id FROM (SELECT 
  DISTINCT ON (selectfiles."pageNumber") selectfiles."pageNumber",
  selectfiles.id 
FROM "Files" as selectfiles
  INNER JOIN "DocumentVersions" as innerversions
    ON innerversions.id = selectfiles."docVersionId"
  WHERE "docId" = 88
  ORDER BY selectfiles."pageNumber", innerversions."createdAt") actual)`)
        }
      }
    }, 
    {
      model: this.dbStorage.db.DocumentVersion,
      as: 'versions',
      attributes: this.attributes("versions"),
      required: false,
      include:[{
        model: this.dbStorage.db.File,
        as: 'file'
      }, {
        model: this.dbStorage.db.File,
        as: 'pages',
      }],
    }];
  }
}
