import { Service } from "@tsed/common";
import FileService from '../FileService';
import TaskService from '../TasksService';
import BaseEntryService from '../../common/BaseEntryService';
import EventManager from "../events/EventManager";


@Service()
export default class DocumentVersionsService extends BaseEntryService<ax.IDocumentVersion> {
  constructor(
    private fileService: FileService,
    private taskService: TaskService
  ) {
    super('DocumentVersion');
    this.allowedFields = {
      update: ['title', 'documentId', 'fileId'],
      create: ['title', 'documentId', 'fileId']
    }
  }

  public find(id:number): PromiseLike<any> {
    return this.dbModel.findOne({
      where: { id },
      include: [<any>{
        model: this.dbStorage.db.File,
        as: 'pages',
        order: [['pages.pageNumber', 'ASC']],
      }],
      attributes: this.attributes()
    });
  }

  async getDefault(documentId:number): Promise<any> {
    var defaultVersion = await this.dbModel.findOne({
      where: { fileId: 0, documentId },
      attributes: this.attributes()
    });

    if(!defaultVersion) {
      var newDefaultVersion = await this.create(<any>{
        fileId: 0,
        documentId: documentId,
        title: "Document version"
      });
      return newDefaultVersion;
    }

    return defaultVersion
  }

  /**
   * @inheritdoc
   */
  protected prepareEntry(entry: any, operation: 'update' | 'create') {
    if(entry.file) {
      entry.fileId = entry.file.id;
    }
    super.prepareEntry(entry, operation);
  }

  protected async entryCreated(data, entry) {
    EventManager.emitProjectDocumentVersionAdded({
      version: entry.toJSON(),
      currentUser: this.dbStorage.currentUser,
      userIds: []
    });
    this.taskService.planDocumentTask(entry.documentId, entry.id, data.fileId);
    return entry;
  }

  protected entryUpdated(data, entry) {
    // console.log('DocumentVersionsService::entryUpdated', data.fileId, entry.fileId);
    if(data.fileId != entry.fileId) {
      this.taskService.planDocumentTask(entry.documentId, entry.id, data.fileId);
    }
  }

  attributes():any[] {
    return [
      ...Object.keys(this.dbModel.attributes),
      [this.se.literal(`(
        SELECT COUNT("Files"."id")::int FROM "Files"
        WHERE "Files"."docVersionId" = "DocumentVersion"."id"
      )`), 'pagesCount']
    ];
  }
}