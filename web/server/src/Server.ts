import {GlobalAcceptMimesMiddleware, ServerLoader, ServerSettings, Inject} from "@tsed/common";
import "@tsed/swagger";
import {$log} from "ts-log-debug";
import { DbMiddleware } from "./middlewares/db/DbMiddleware";

import EventManager from "./services/events/EventManager";

@ServerSettings({
  rootDir: __dirname,
  port: 3000,
  httpsPort: 3003,
  acceptMimes: ["application/json"],
  mount: {
    '/api': "${rootDir}/controllers/**/*.js",
    '/api/v2': "${rootDir}/controllers_v2/**/*.js",
  },
  componentsScan: ["${rootDir}/middlewares/**/*.js", "${rootDir}/services/**/*.js", "${rootDir}/converters/**/*.js"],
  logger: {
    debug: true,
    logRequest: false,
    requestFields: ["reqId", "method", "url", "headers", "query", "params", "duration"]
  },
  swagger: {
    path: "/api-docs"
  }
})
export class Server extends ServerLoader {

    /**
     * This method let you configure the middleware required by your application to works.
     * @returns {Server}
     */
    $onMountingMiddlewares(): void | Promise<any> {

        const cookieParser = require("cookie-parser"),
            bodyParser = require("body-parser"),
            compress = require("compression"),
            fileupload = require('express-fileupload'),
            path = require('path'),
            express = require('express'),
            methodOverride = require("method-override")

        EventManager.instance.init();

        this
            .use(GlobalAcceptMimesMiddleware)
            .use(DbMiddleware)
            .use(cookieParser())
            .use(compress({}))
            .use(fileupload())
            .use(methodOverride())
            .use(bodyParser.json())
            .use(bodyParser.urlencoded({
                extended: true
            }))
            .use('/', express.static(path.join(__dirname, '..', '..', 'client', 'dist')))

        return null;
    }

    $onReady() {
        $log.debug("Server initialized");
    }

    $onServerInitError(error): any {
        $log.error("Server encounter an error =>", error);
    }
}