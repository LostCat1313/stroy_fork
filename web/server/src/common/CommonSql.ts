export const documentRecursive = `WITH RECURSIVE documents AS (
  SELECT id, "parentId", "projectId", "isFolder", title
  FROM "Documents"
  WHERE "parentId" IS NULL
  
  UNION
  
  SELECT children.id, children."parentId", children."projectId", children."isFolder", children.title
  FROM "Documents" as children
      JOIN documents
          ON children."parentId" = documents.id
  )`; 