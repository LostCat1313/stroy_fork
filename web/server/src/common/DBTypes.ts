import * as se from "sequelize";

export interface AnyInstance extends se.Instance<any> {
  toJSON(): any;
};

export interface IAnyModel<I extends AnyInstance = AnyInstance, M extends any = any> extends se.Model<I, M> {
  attributes:any[];
};

export interface IRightModel extends IAnyModel<IRightInstance, ax.IRight> {
  operations:string[];
  entities:string[];
}

export interface IRightInstance extends AnyInstance, ax.IRight {

}

export interface IUserInstance extends AnyInstance, ax.IUser {
    
}


export interface IToken extends IAnyModel {
  getUserByToken(token: string);
}
