import * as crypto from 'crypto';
import * as uuid from 'uuid/v4';
import * as se from 'sequelize';
import * as ex from 'express';
import { EventEmitter } from "events";
import { NotFound } from "ts-httpexceptions";

import { Consts } from '../Consts';
import DBStorage from '../services/DBStorage';
import { serverInstance } from '../app';
import { IAnyModel } from './DBTypes';
import { $log } from 'ts-log-debug';
import EventManager from '../services/events/EventManager';

export default class BaseEntryService<T extends any, M extends IAnyModel = IAnyModel> extends EventEmitter {
  protected allowedFields: {
    update: string[],
    create: string[],
  };

  protected dbStorage:DBStorage;
  protected dbModel:M;
  protected se:se.Sequelize;
  protected limit:number;
  protected offset:number;

  constructor(
    private modelName:string,
  ) {
    super();
    this.dbStorage = serverInstance.injector.get<DBStorage>(DBStorage);
    this.dbModel = this.dbStorage.db[this.modelName];
    this.se = this.dbStorage.sequelize;
    this.initializeSubscriptions();
  }

  /**
   * Find an entry by its ID.
   * @param id
   * @returns {Promise<se.Instance<T>>}
   */
  public find(id: number): PromiseLike<se.Instance<T>> {
    return this.dbModel.findById(id);
  }

  /**
   * Query list of all entries
   * @returns {Promise<any[]>}
   */
  public query():PromiseLike<any[]> {
    return this.dbModel.findAll({
      order: this.order(),
      limit: this.dbStorage.limit,
      offset: this.dbStorage.offset
    });
  }

  
  /**
   * Query list of all entries for current user
   * @returns {Promise<any[]>}
   */
  public queryForCurrentUser():PromiseLike<any[]> {
    return this.dbModel.findAll({
      where: {
        userId: this.dbStorage.currentUser.id
      },
      order: this.order(),
      limit: this.dbStorage.limit,
      offset: this.dbStorage.offset
    });
  }


  public async count(where = null):Promise<number> {
    return await this.dbModel.count();
  }

  /**
   * Update record
   * @param id
   * @param entry
   * @returns {Promise<T>}
   */
  public async update(id:number, entry:T): Promise<se.Instance<T>> {
    this.prepareEntry(entry, 'update');
    const original = { ...entry };
    const e = await this.dbModel.findById(id);
    const result = e.toJSON();
    // TODO: check update new attributes in result
    await e.updateAttributes(entry);
    this.entryUpdated(original, result);
    return e;
  }

  /**
   * Create a new entry
   * @param entry
   * @returns {{id: any, name: string}}
   */
  async create(entry: T): Promise<any> {
    $log.debug(entry.userIds)
    const original = { ...entry };
    this.prepareEntry(entry, 'create');
    const e = await this.dbModel.create(entry);
    const result = await this.entryCreated(original, e);
    return result;
  }

  /**
   * Delete entry
   * @param id 
   * @returns {Promise<{id}>}
   */
  async delete(id:number): Promise<{ id }> {
    let entry = await this.dbModel.findById(id);
   
    if(!entry) throw new NotFound("Entry not found");
    
    let resultId = entry.getDataValue('id');

    await entry.destroy();

    return { id: resultId };
  }

  public setLimit(limit:number, offset:number) {
    this.limit = limit || 0;
    this.offset = offset || 0;
  }
  
  /**
   * Prepare entry before modify operation.
   * @param entry
   * @param operation
   */
  protected prepareEntry(entry: T, operation: 'update' | 'create') {
    if(this.dbStorage.currentUser) {
      if (operation == 'create')
        entry.createdBy = this.dbStorage.currentUser.id;
      entry.modifiedBy = this.dbStorage.currentUser.id;
    } 
    if(!this.allowedFields) {
      throw new Error("BaseEntryService::allowed fields required");
    }
    const fields = this.allowedFields[operation];
    for (const key in entry) {
      if (fields.indexOf(key) < 0) {
        delete entry[key];
      }
    }
    this.emit(Consts.ENTRY_PREPARED, entry);
  }

  protected async entryCreated(data: T, entry: any):Promise<any> {
    $log.debug('entryCreated from EC')
    return await this.find(entry.id)
  }

  protected entryUpdated(data: T, entry: any) {
    
  }

  protected md5(s: string): string {
    return crypto.createHash('md5').update(s).digest('hex');
  }

  protected uuid(): string {
    return uuid();
  }

  protected order(): any {
    return [['createdAt', 'ASC']];
  }

  protected include() {
    return []
  }

  protected attributes(params = null) {
    return []
  }

  protected initializeSubscriptions() {
    
  }
}
