import {
  Controller, Authenticated, Get, PathParams, Post, BodyParams, Put, Delete, Request
} from "@tsed/common";
import { $log } from "ts-log-debug";

import { NotFound } from "ts-httpexceptions";
import PagesService from '../../services/PagesService';
import DocumentVersionsService from "../../services/documents/DocumentVersionsService";
import DocumentsService from "../../services/documents/DocumentsService";
import FileService from '../../services/FileService';


@Controller("/documents")
export default class DocumentPagesCtrlV2 {

  constructor(
    // private pagesService: PagesService,
    private fileService: FileService,
    private documentsService: DocumentsService,
    private documentVersionsService: DocumentVersionsService
  ) { }

  // @Get('/:docId/pages/')
  // @Authenticated()
  // async index(
  //   @PathParams('docId') docId: string
  // ): Promise<{ list: ax.IOrganization[] }> {
  //   let pages = await this.pagesService.queryDocumentPages(Number(docId))
  //   return { list: pages };
  // }

  // @Get('/:docId/:docVersionId/pages/')
  // @Authenticated()
  // async versionIndex(
  //   @PathParams('docId') docId: number,
  //   @PathParams('docVersionId') docVersionId: number
  // ): Promise<{ list: ax.IOrganization[] }> {
  //   let pages = await this.pagesService.queryDocumentPages(docId, docVersionId);
  //   return { list: pages };
  // }

  // @Put('/:docId/pages/:id')
  // @Authenticated()
  // update(
  //   @PathParams('id') id:number,
  //   @BodyParams('entry') entry:any
  //   ) {
  //   return this.pagesService.update(id, entry);
  // } 

  @Post('/:docId/pages/upload')
  @Authenticated()
  async uploadPagesToDocument(
    @Request() req:any,
    @PathParams('docId') docId:number
  ) {

    // console.log(req);

    if(!req.files)
      throw new Error("No PAGES were uploaded");
    
    if(!docId)
      throw new Error("docId is required parameter");
    
    // check that document exists
    const document = await this.documentsService.find(docId);
    
    var documentVersion = await this.documentVersionsService.create(<any>{
      documentId: document.id,
    });
        
    const files = req.files;
    const pageNumbers = req.pageNumbers;
    let pageNumbersOccupied = {};
    let pageCounter = 1;

    let prepared = Object.keys(files).map((key) => {
      const page = files[key];
      let pageNumber = parseInt(req.body[key+'_pageNumber'] + '');
      if(!pageNumbersOccupied[pageNumber]) {
        pageNumbersOccupied[pageNumber] = true;
        if(pageCounter < pageNumber) pageCounter = pageNumber + 1;
      }
      else {
        pageNumber = -1;
      }
      
      return this.fileService.prepareFileInfo(page, {
        docId:document.id,
        docVersionId:documentVersion.id,
        pageNumber
      });
    }).map(i => ({ ...i, pageNumber: i.pageNumber < 0 ? pageCounter++ : i.pageNumber, actual: true}));

    if (prepared.find(v => v.type != 0))
      throw new Error("Only images can be uploaded");

    $log.debug(`>>v2>>DocumentPagesCtrl::uploadPagesToDocument: Pages number uploaded ${prepared.length}`);

    await this.fileService.saveFiles(prepared);

    return await this.documentVersionsService.find(documentVersion.id);
  }
}