import { JsonProperty } from '@tsed/common'
import { Title, Description, Example } from '@tsed/swagger'

export class WithId implements ax.IWithId {
  @Title('id')
  @Description('Model identifier')
  @JsonProperty()
  id:number
}