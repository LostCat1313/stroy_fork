import { JsonProperty } from '@tsed/common'
import { Title, Description, Example } from '@tsed/swagger'

import { WithModification } from './WithModification';

export class Project extends WithModification implements ax.IProject {
  @Title('title')
  @Description('Title of the project')
  @JsonProperty()
  title:string

  @Title('imageId')
  @Description('Id of the File object for project main image')
  @JsonProperty()
  imageId:number
}