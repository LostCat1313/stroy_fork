
import { JsonProperty } from '@tsed/common'
import { Title, Description, Example } from '@tsed/swagger'

import { WithId } from './WithId';

export class WithModification extends WithId implements ax.IWithModification, ax.IWithId {
  @Title('createdBy')
  @Description('Id of the User created project')
  @JsonProperty()
  createdBy:number

  @Title('modifiedBy')
  @Description('Id of the User modified project')
  @JsonProperty()
  modifiedBy:number
}
