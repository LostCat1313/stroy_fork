/// <reference path="./app.d.ts"/>

import {Server} from "./Server";
import {$log} from "ts-log-debug";

$log.info('Initialize server');

export const serverInstance = new Server()

  serverInstance
    .start()
    .then(() => {
        $log.info('Server started...');
    })
    .catch((err) => {
        $log.error(err);
    });
