import * as passport from 'passport-strategy'
import * as util from 'util'
import { $log } from 'ts-log-debug'

/**
* Creates an instance of `Strategy`.
*
* The HTTP Bearer authentication strategy authenticates requests based on
* a bearer token contained in the `Authorization` header field, `access_token`
* body parameter, or `access_token` query parameter.
*
* Applications must supply a `verify` callback, for which the function
* signature is:
*
*     function(token, done) { ... }
*
* `token` is the bearer token provided as a credential.  The verify callback
* is responsible for finding the user who posesses the token, and invoking
* `done` with the following arguments:
*
*     done(err, user, info);
*
* If the token is not valid, `user` should be set to `false` to indicate an
* authentication failure.  Additional token `info` can optionally be passed as
* a third argument, which will be set by Passport at `req.authInfo`, where it
* can be used by later middleware for access control.  This is typically used
* to pass any scope associated with the token.
*
* Options:
*
*   - `realm`  authentication realm, defaults to "Users"
*   - `scope`  list of scope values indicating the required scope of the access
*              token for accessing the requested resource
*
* Examples:
*
*     passport.use(new BearerStrategy(
*       function(token, done) {
*         User.findByToken({ token: token }, function (err, user) {
*           if (err) { return done(err); }
*           if (!user) { return done(null, false); }
*           return done(null, user, { scope: 'read' });
*         });
*       }
*     ));
*
* For further details on HTTP Bearer authentication, refer to [The OAuth 2.0 Authorization Protocol: Bearer Tokens](http://tools.ietf.org/html/draft-ietf-oauth-v2-bearer)
*
* @constructor
* @param {Object} [options]
* @param {Function} verify
* @api public
*/
export default class Strategy extends passport.Strategy {
  name: string
  _verify: any
  _realm: string
  _scope: any
  _passReqToCallback: boolean

  constructor(options, verify = null) {
    super()
    if (typeof options == 'function') {
      verify = options
      options = {}
    }
    if (!verify) { throw new TypeError('HTTPBearerStrategy requires a verify callback') }
  
    passport.Strategy.call(this)
    this.name = 'bearer'
    this._verify = verify
    this._realm = options.realm || 'Users'
    if (options.scope) {
      this._scope = (Array.isArray(options.scope)) ? options.scope : [ options.scope ]
    }
    this._passReqToCallback = options.passReqToCallback
  }

  fail(code) {
    super.fail(code)
  }
  error(error) {
    super.error(error)
  }
  success(user, info) {
    super.success(user, info)
  }
  
  protected authenticate(req) {
    var token

    $log.debug("======Token Strategy======", req.cookies, req.headers)

    if (req.headers && req.headers.authorization) {
      $log.debug("======Authorize by header======")
      token = this.extractToken(req.headers.authorization)
    }

    if (!token && req.cookies && req.cookies.Authorization) {
      $log.debug("======Authorize by cookie======")
      token = this.extractToken(req.cookies.Authorization)
    }

    if (req.body && req.body.access_token) {
      if (token) { return this.fail(400) }
      token = req.body.access_token
    }
    
    if (req.query && req.query.access_token) {
      if (token) { return this.fail(400) }
      token = req.query.access_token
    }

    $log.debug("======Token======", token)
    
    if (!token) { return this.success(null, this._challenge()) }
    
    if (this._passReqToCallback) {
      this._verify(req, token, this.verified.bind(this))
    } else {
      this._verify(token, this.verified.bind(this))
    }
  }

  private extractToken(authorization: string) {
    var parts = authorization.split(' ')
    if (parts.length >= 2) {
      var scheme = parts[0], credentials = parts[1]
        
      if (/^Bearer$/i.test(scheme)) {
        return credentials
      }
    } else {
      return null
    }
  }

  private verified(err, user, info) {
    if (err) { return this.error(err) }
    if (!user) {
      if (typeof info == 'string') {
        info = { message: info }
      }
      info = info || {}
      return this.success(null, info)
    }
    this.success(user, info)
  }

  private _challenge(code:any = null, desc:any = null, uri:string = null) {
    var challenge = 'Bearer realm="' + this._realm + '"'
    if (this._scope) {
      challenge += ', scope="' + this._scope.join(' ') + '"'
    }
    if (code) {
      challenge += ', error="' + code + '"'
    }
    if (desc && desc.length) {
      challenge += ', error_description="' + desc + '"'
    }
    if (uri && uri.length) {
      challenge += ', error_uri="' + uri + '"'
    }
    
    return challenge
    }
}

