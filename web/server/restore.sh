DATE=`date '+%Y-%m-%d_%H:%M:%S'`
dropdb --if-exists 'stroy_db_devel'
createdb 'stroy_db_devel'
#run migration
echo =====INIT DB=====
npm run migrate
psql -d stroy_db_devel -a -f ./db/demo/out.sql

echo =====INIT STORE=====
mv ".store" ".store.backup.$DATE"
unzip ./db/demo/store.zip
