'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
      return queryInterface.createTable('ChatMessages', {
          id: {
              allowNull: false,
              autoIncrement: true,
              primarykey: true,
              type: Sequelize.INTEGER
          },
          senderId: {
              allowNull: false,
              type: Sequelize.INTEGER
          },
          roomId: {
              allowNull: false,
              type: Sequelize.INTEGER
          },
          content: {
              type: Sequelize.TEXT
          },
          createdAt: {
              allowNull: false,
              type: Sequelize.DATE
          },
          updatedAt: {
              allowNull: false,
              type: Sequelize.DATE
          }
      });
  },

  down: function (queryInterface, Sequelize) {
      return queryInterface.dropTable('ChatMessages');
  }
};
