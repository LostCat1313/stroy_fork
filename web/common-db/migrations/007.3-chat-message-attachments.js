'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
      return queryInterface.createTable('ChatMessageAttachments', {
          id: {
              allowNull: false,
              autoIncrement: true,
              primarykey: true,
              type: Sequelize.INTEGER
          },
          messageId: {
              allowNull: false,
              type: Sequelize.INTEGER
          },
          type: {
              allowNull: false,
              type: Sequelize.INTEGER
          },
          targetId: {
            allowNull: false,
            type: Sequelize.INTEGER
          },
          createdAt: {
              allowNull: false,
              type: Sequelize.DATE
          },
          updatedAt: {
              allowNull: false,
              type: Sequelize.DATE
          }
      });
  },

  down: function (queryInterface, Sequelize) {
      return queryInterface.dropTable('ChatMessageAttachments');
  }
};
