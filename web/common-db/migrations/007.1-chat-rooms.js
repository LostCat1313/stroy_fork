'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
      return queryInterface.createTable('ChatRooms', {
          id: {
              allowNull: false,
              autoIncrement: true,
              primarykey: true,
              type: Sequelize.INTEGER
          },
          createdBy: {
              allowNull: false,
              type: Sequelize.INTEGER
          },
          isPrivate: {
              allowNull: false,
              defaultValue: true,
              type: Sequelize.BOOLEAN
          },
          targetUserId: {
            defaultValue: 0,
            type: Sequelize.INTEGER
          },
          name: {
              type: Sequelize.STRING
          },
          createdAt: {
              allowNull: false,
              type: Sequelize.DATE
          },
          updatedAt: {
              allowNull: false,
              type: Sequelize.DATE
          }
      });
  },

  down: function (queryInterface, Sequelize) {
      return queryInterface.dropTable('ChatRooms');
  }
};
