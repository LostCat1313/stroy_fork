'use strict';
// TODO: make only create migrations + generate mock data fill
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('Descriptors', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      type: {
        type: Sequelize.INTEGER
      },
      params: {
        type: Sequelize.JSON
      },
      title: {
        type: Sequelize.STRING
      },
      text: {
        type: Sequelize.TEXT
      },
      docId: {
        type: Sequelize.INTEGER
      },
      holderId: {
        type: Sequelize.INTEGER
      },
      holderType: {
        type: Sequelize.INTEGER
      },
      createdBy: {
        type: Sequelize.INTEGER
      },
      modifiedBy: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('Descriptors');
  }
};