'use strict';

module.exports = {
    up: function (queryInterface, Sequelize) {
        return [
          queryInterface.addColumn('Projects', 'createdBy', Sequelize.INTEGER),
          queryInterface.addColumn('Projects', 'modifiedBy', Sequelize.INTEGER)
        ];
    },
    down: function (queryInterface, Sequelize) {
        return [
          queryInterface.removeColumn('Projects', 'createdBy'),
          queryInterface.removeColumn('Projects', 'modifiedBy')
        ];
    }
};
