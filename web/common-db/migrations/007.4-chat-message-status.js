'use strict';

module.exports = {
    up: function (queryInterface, Sequelize) {
        return queryInterface.createTable('ChatMessagesStatus', {
                id: {
                    allowNull: false,
                    autoIncrement: true,
                    primarykey: true,
                    type: Sequelize.INTEGER
                },
                userId: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                messageId: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                createdAt: {
                    allowNull: false,
                    type: Sequelize.DATE
                },
                updatedAt: {
                    allowNull: false,
                    type: Sequelize.DATE
                }, 
            }).then(() => queryInterface.addConstraint('ChatMessagesStatus',
            ['userId', 'messageId'],
            { type: 'unique', name: 'uniqueStatus' })
        )
    },
    down: function (queryInterface, Sequelize) {
        return queryInterface.dropTable('ChatMessagesStatus')
    }
};