

'use strict';

module.exports = {
    up: function (queryInterface, Sequelize) {
        return [
            queryInterface.addColumn('ChatRooms', 'lastMessageId', Sequelize.INTEGER)
        ];
    },
    down: function (queryInterface, Sequelize) {
        return [
          queryInterface.removeColumn('ChatRooms', 'lastMessageId')
        ];
    }
};
