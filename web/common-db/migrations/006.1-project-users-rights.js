'use strict';

module.exports = {
    up: function (queryInterface, Sequelize) {
        return [
          queryInterface.addColumn('ProjectUser', 'rightMask', Sequelize.INTEGER),
        ];
    },
    down: function (queryInterface, Sequelize) {
        return [
          queryInterface.removeColumn('ProjectUser', 'rightMask'),
        ];
    }
};
