'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return [
      queryInterface.changeColumn('Users', 'email', {
        type: Sequelize.STRING,
        unique: false
      }),
      queryInterface.changeColumn('Users', 'positionId', {
        type: Sequelize.INTEGER,
        allowNull: true
      })
    ];
  },
  down: function(queryInterface, Sequelize) {
    return [
      queryInterface.changeColumn('Users', 'email', {
        type: Sequelize.STRING,
        unique: true
      }),
      queryInterface.changeColumn('Users', 'positionId', {
        type: Sequelize.INTEGER,
        allowNull: false
      })
    ];
  }
};
