'use strict';

module.exports = {
    up: function (queryInterface, Sequelize) {
        return queryInterface.addConstraint('OrganizationUser', {
            OrganizationId: {
                allowNull: false,
                type: Sequelize.INTEGER
            }
        });
    },

    down: function (queryInterface, Sequelize) {
        return queryInterface.dropTable('OrganizationUser');
    }
};
