'use strict';

module.exports = {
    up: function (queryInterface, Sequelize) {
        return [
            queryInterface.addColumn('Documents', 'shortTitle', Sequelize.STRING)
        ];
    },
    down: function (queryInterface, Sequelize) {
        return [
          queryInterface.removeColumn('Documents', 'shortTitle')
        ];
    }
};
