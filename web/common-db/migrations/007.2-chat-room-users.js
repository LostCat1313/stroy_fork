'use strict';

module.exports = {
    up: function (queryInterface, Sequelize) {
        return queryInterface.createTable('ChatRoomUsers', {
            id: {
              allowNull: false,
              autoIncrement: true,
              primaryKey: true,
              type: Sequelize.INTEGER
            },
            roomId: {
              allowNull: false,
              type: Sequelize.INTEGER
            },
            userId: {
              allowNull: false,
              type: Sequelize.INTEGER
            },
            lastEnteredAt: {
              defaultValue: null,
              type: Sequelize.DATE
            },
            createdAt: {
              allowNull: false,
              type: Sequelize.DATE
            },
            updatedAt: {
              allowNull: false,
              type: Sequelize.DATE
            }
        });
    },

    down: function (queryInterface, Sequelize) {
        return queryInterface.dropTable('ChatRoomUsers');
    }
};
