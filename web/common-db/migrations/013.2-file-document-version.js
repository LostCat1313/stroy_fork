'use strict';

module.exports = {
    up: function (queryInterface, Sequelize) {
        return [
            queryInterface.addColumn('Files', 'docVersionId', Sequelize.INTEGER),
            queryInterface.addColumn('Files', 'actual', Sequelize.BOOLEAN),
        ];
    },
    down: function (queryInterface, Sequelize) {
        return [
          queryInterface.removeColumn('Files', 'docVersionId'),
          queryInterface.removeColumn('Files', 'actual'),
        ];
    }
};