'use strict';

module.exports = {
    up: function (queryInterface, Sequelize) {
        return [
          queryInterface.addColumn('Documents', 'createdBy', Sequelize.INTEGER),
          queryInterface.addColumn('Documents', 'modifiedBy', Sequelize.INTEGER)
        ];
    },
    down: function (queryInterface, Sequelize) {
        return [
          queryInterface.removeColumn('Documents', 'createdBy'),
          queryInterface.removeColumn('Documents', 'modifiedBy')
        ];
    }
};
