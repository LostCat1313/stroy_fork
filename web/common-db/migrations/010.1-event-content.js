'use strict';

module.exports = {
    up: function (queryInterface, Sequelize) {
        return [
            queryInterface.addColumn('Events', 'content', Sequelize.TEXT)
        ];
    },
    down: function (queryInterface, Sequelize) {
        return [
          queryInterface.removeColumn('Events', 'content')
        ];
    }
};
