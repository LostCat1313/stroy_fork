'use strict';

module.exports = {
    up: function (queryInterface, Sequelize) {
        return [
            queryInterface.addColumn('Documents', 'isMultiPage', Sequelize.BOOLEAN)
        ];
    },
    down: function (queryInterface, Sequelize) {
        return [
          queryInterface.removeColumn('Documents', 'isMultiPage')
        ];
    }
};
