'use strict';

module.exports = {
    up: function (queryInterface, Sequelize) {
        return queryInterface.createTable('Rights', {
            id: {
              allowNull: false,
              autoIncrement: true,
              primaryKey: true,
              type: Sequelize.INTEGER
            },
            refId: {
              allowNull: false,
              type: Sequelize.INTEGER
            },
            entity: {
              allowNull: false,
              type: Sequelize.STRING
            },
            operation: {
              type: Sequelize.STRING,
              allowNull: false
            },
            sign: {
              allowNull: false,
              type: Sequelize.INTEGER
            },
            groupId: {
              type: Sequelize.INTEGER,
              allowNull: false
            },
            userId: {
              type: Sequelize.INTEGER,
              allowNull: false
            },
            createdAt: {
              allowNull: false,
              type: Sequelize.DATE
            },
            updatedAt: {
              allowNull: false,
              type: Sequelize.DATE
            }
        });
    },

    down: function (queryInterface, Sequelize) {
        return queryInterface.dropTable('Rights');
    }
};
