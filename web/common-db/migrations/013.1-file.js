'use strict';

module.exports = {
    up: function (queryInterface, Sequelize) {
        return [
            queryInterface.addColumn('Files', 'pageNumber', Sequelize.INTEGER),
            queryInterface.addColumn('Files', 'docId', Sequelize.INTEGER),
            queryInterface.addColumn('Files', 'createdBy', Sequelize.INTEGER),
            queryInterface.addColumn('Files', 'modifiedBy', Sequelize.INTEGER)
        ];
    },
    down: function (queryInterface, Sequelize) {
        return [
          queryInterface.removeColumn('Files', 'pageNumber'),
          queryInterface.removeColumn('Files', 'docId'),
          queryInterface.removeColumn('Files', 'createdBy'),
          queryInterface.removeColumn('Files', 'modifiedBy')
        ];
    }
};
