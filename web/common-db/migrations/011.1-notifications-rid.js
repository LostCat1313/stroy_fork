'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return [
      queryInterface.addColumn('Notifications', 'rId', Sequelize.INTEGER)
    ];
  },
  down: function(queryInterface, Sequelize) {
    return [
      queryInterface.removeColumn('Notifications', 'rId')
    ];
  }
};
