'use strict';

module.exports = {
    up: function (queryInterface, Sequelize) {
        return [
            queryInterface.removeColumn('Documents', 'fileId')
        ];
    },
    down: function (queryInterface, Sequelize) {
        return [
          queryInterface.addColumn('Documents', 'fileId', Sequelize.BOOLEAN)
        ];
    }
};
