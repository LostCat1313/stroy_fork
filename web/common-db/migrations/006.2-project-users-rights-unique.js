
'use strict';

module.exports = {
    up: function (queryInterface, Sequelize) {
        return [
          queryInterface.addIndex(
            'ProjectUser',
            ['UserId', 'ProjectId'],
            {
              indexName: 'UserProjectIndex',
              indicesType: 'UNIQUE'
            }
          )
        ];
    },
    down: function (queryInterface, Sequelize) {
      return [
        queryInterface.removeIndex('ProjectUser', 'UserProjectIndex')
      ];
    }
};


