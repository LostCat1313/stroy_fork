'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return [
      queryInterface.addColumn('Users', 'isActive', {
        type: Sequelize.BOOLEAN,
        defaultValue: true
      }),
      queryInterface.addColumn('Users', 'activationCode', Sequelize.STRING)
    ];
  },
  down: function(queryInterface, Sequelize) {
    return [
      queryInterface.removeColumn('Users', 'isActive'),
      queryInterface.removeColumn('Users', 'activationCode')
    ];
  }
};
