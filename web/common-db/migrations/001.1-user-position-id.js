'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.renameColumn('Users', 'PositionId', 'positionId');
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.renameColumn('Users', 'positionId', 'PositionId');
  }
};