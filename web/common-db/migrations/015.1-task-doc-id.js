'use strict';

module.exports = {
    up: function (queryInterface, Sequelize) {
        return [
            queryInterface.addColumn('Tasks', 'docId', Sequelize.INTEGER)
        ];
    },
    down: function (queryInterface, Sequelize) {
        return [
          queryInterface.removeColumn('Tasks', 'docId')
        ];
    }
};
