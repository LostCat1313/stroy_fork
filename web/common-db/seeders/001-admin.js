'use strict';
const md5 =require('js-md5')

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Users', [{
      name: 'User',
      email: '',
      phoneNumber: '',
      firstName: '',
      secondName: '',
      lastName: '',
      password: md5('123123'),
      positionId: 12,
      createdAt: new Date(),
      updatedAt: new Date(),
      activationCode: '',
      isActive: true,
      isAdmin: true
      }]);
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Users', null, {});
  }
};