const types = {
  UNKNOWN: 0,
  CHAT_MESSAGE:201,
  CHAT_ROOM_ADDED:202,
  PROJECT_DOCUMENT_ADDED:101,
  PROJECT_DOCUMENT_REMOVED:102,
  PROJECT_DOCUMENT_PAGES_ADDED:103,
  PROJECT_DOCUMENT_UPDATED:104,
  PROJECT_DOCUMENT_DESCRIPTOR_ADDED:105,
  PROJECT_DOCUMENT_VERSION_ADDED:106
}

module.exports.types = types;
module.exports.getKey = function(type) {
  for(let key in types) {
    if(types[key] == type) return key;
  }
  return 'UNKNOWN';
  //throw new Error('key not found for type ' + type);
}
module.exports.include = function(db) {
  const se = db.sequelize;
  function whereWithType(type) {
    return se.literal(`"Event"."type" = ${type}`);
  }
  return [{
    model: db.ChatMessage,
    where: whereWithType(types.CHAT_MESSAGE),
    required: false,
    as: 'message'
  }, {
    model: db.Document,
    where: whereWithType(types.PROJECT_DOCUMENT_ADDED),
    required: false,
    as: 'document'
  }, {
    model: db.Descriptor,
    where: whereWithType(types.PROJECT_DOCUMENT_DESCRIPTOR_ADDED),
    required: false,
    as: 'descriptor',
    include:[{
      model: db.File,
      as: 'holder'
    }]
  }]
}