const db = require('../models');
const types = require('./eventTypes').types;
const getKey = require('./eventTypes').getKey;

module.exports = function(event){
  console.log("EventFormatter::format type=" + getKey(event.type) + " targetId=" + event.targetId);
  switch(event.type) {
    case types.CHAT_MESSAGE:
      return formatChatMessage(event);
    case types.PROJECT_DOCUMENT_ADDED:
      return formatProjectDocumentAdded(event);
    case types.PROJECT_DOCUMENT_DESCRIPTOR_ADDED:
      return formatProjectDescriptorAdded(event);
  }
  return Promise.resolve(event);
}

function formatChatMessage(event) {
  return db.ChatMessage.findById(event.targetId, {
    include: [
      {model:db.User, as: 'sender'},
      {model:db.ChatRoom, as: 'room'}
    ]
  })
  .then(message => {
    if(!message) return event;
    const sender = message.sender 
      ? " от " + message.sender.firstName + " " + message.sender.lastName : '';
    const title = message.room 
      ? " в чате '" + message.room.name + "'" : '';
    return Object.assign(event, {
      title: message.sender.id != 0 ? sender + (title && " " + title) : "Системное сообщение",
      content: message.content
    });
  });
}

function formatProjectDocumentAdded(event) {
  return db.Document.findById(event.targetId, {
    include: [{model:db.Project, as: 'project'}]
  })
  .then(document => {
    if(!document) return event;
    return Object.assign(event, {
      title: "Документ добавлен", // TODO: add created by
      content: "'" + document.title + "' " + (document.project ? "в проект '" + document.project.title + "'" : "")
    });
  });
}

function formatProjectDescriptorAdded(event) {
  return db.Descriptor.findById(event.targetId, {
    include: [{
      model:db.Document,
      as: 'document',
      include: [{model:db.Project, as: 'project'}]
    }]
  })
  .then(descriptor => {
    if(!descriptor) return event;
    return Object.assign(event, {
      title: "Добавлена метка " + descriptor.type, // TODO: add created by
      content: descriptor.title + ". " + descriptor.text
    });
  });
} 