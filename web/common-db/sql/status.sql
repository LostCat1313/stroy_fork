SELECT * 
FROM public."ChatMessages"
LEFT JOIN public."ChatMessagesStatus" ON
public."ChatMessages"."id" = public."ChatMessagesStatus"."messageId"
INNER JOIN public."ChatRoomUsers"
ON public."ChatRoomUsers"."roomId" = public."ChatMessages"."roomId"
WHERE public."ChatRoomUsers"."userId" = 1
