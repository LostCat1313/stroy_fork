SELECT count(*) 
FROM public."ChatMessages" 
LEFT JOIN public."ChatMessagesStatus" ON
public."ChatMessages"."id" = public."ChatMessagesStatus"."messageId"
WHERE "roomId"=1 AND public."ChatMessagesStatus"."userId"=1

userId =>

SELECT * 
FROM public."ChatMessages" 
LEFT JOIN public."ChatMessagesStatus" ON
public."ChatMessages"."id" = public."ChatMessagesStatus"."messageId"
LEFT JOIN public."ChatRoomUsers"
ON public."ChatRoomUsers"."roomId" = public."ChatMessages"."roomId"
WHERE public."ChatRoomUsers"."userId" = 1

/* count all unviewed messages for particular user */
SELECT COUNT(*)
FROM public."ChatRooms"
FULL OUTER JOIN public."ChatRoomUsers"
ON public."ChatRooms"."id" = public."ChatRoomUsers"."roomId"
FULL OUTER JOIN public."ChatMessages"
ON public."ChatMessages"."roomId" = public."ChatRooms"."id"
FULL OUTER JOIN public."ChatMessagesStatus" 
ON public."ChatMessages"."id" = public."ChatMessagesStatus"."messageId"
WHERE (public."ChatRooms"."targetUserId" = 2
OR public."ChatRoomUsers"."userId" = 2)
AND public."ChatMessagesStatus"."id" IS NULL

/* count unviewed  messages from particular chat for particular user */
SELECT COUNT(*)
FROM public."ChatRooms"
FULL OUTER JOIN public."ChatRoomUsers"
ON public."ChatRooms"."id" = public."ChatRoomUsers"."roomId"
FULL OUTER JOIN public."ChatMessages"
ON public."ChatMessages"."roomId" = public."ChatRooms"."id"
FULL OUTER JOIN public."ChatMessagesStatus" 
ON public."ChatMessages"."id" = public."ChatMessagesStatus"."messageId"
WHERE (public."ChatRooms"."targetUserId" = 2
OR public."ChatRoomUsers"."userId" = 2)
AND public."ChatMessagesStatus"."id" IS NULL 
AND public."ChatMessages"."roomId" = 1