'use strict';
module.exports = function(sequelize, DataTypes) {
  var DocumentVersion = sequelize.define('DocumentVersion', {
      title: DataTypes.STRING,
      documentId: DataTypes.INTEGER,
      fileId: DataTypes.INTEGER
  }, {
    tableName: 'DocumentVersions'
  });

  DocumentVersion.associate = function(models) {
    DocumentVersion.belongsTo(models.File, {foreignKey: 'fileId', as: 'file'});
    DocumentVersion.hasMany(models.File, {foreignKey: 'docVersionId', as: 'pages'});
  }

  return DocumentVersion;
};
