'use strict';

module.exports = function(sequelize, DataTypes) {
    var ChatRoom = sequelize.define('ChatRoom', {
        createdBy: DataTypes.INTEGER,
        targetUserId: DataTypes.INTEGER,
        isPrivate: DataTypes.BOOLEAN,
        name: DataTypes.STRING,
        lastMessageId: DataTypes.INTEGER        
    }, {
        tableName: 'ChatRooms'
    });
    ChatRoom.associate = function(models) {
      ChatRoom.belongsToMany(models.User, {
        through: 'ChatRoomUsers',
        foreignKey: 'roomId',
        otherKey: 'userId',
        as: 'users'
      });
      models.User.belongsToMany(ChatRoom, {
        through: 'ChatRoomUsers',
        foreignKey: 'userId',
        otherKey: 'roomId',
        as: 'rooms'
      });
      // TODO many to many with messages?
      ChatRoom.belongsTo(models.ChatMessage, {as:'lastMessage', foreignKey:'lastMessageId'});
      ChatRoom.belongsTo(models.User, {as:'createdByUser', foreignKey:'createdBy'});
      ChatRoom.belongsTo(models.User, {as:'targetUser', foreignKey:'targetUserId'});

      ChatRoom.hasMany(models.ChatMessage, {as: 'messages', foreignKey: 'roomId', onDelete: 'CASCADE', hooks: true});
      ChatRoom.hasMany(models.ChatRoomUser, {as: 'roomUsers', foreignKey: 'roomId', onDelete: 'CASCADE', hooks: true});
    }

    return ChatRoom;
};
