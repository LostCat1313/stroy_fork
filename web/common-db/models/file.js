'use strict';
module.exports = function (sequelize, DataTypes) {
  var File = sequelize.define('File', {
    fid: DataTypes.STRING,
    type: DataTypes.INTEGER,
    original: DataTypes.STRING,
    mimetype: DataTypes.STRING,
    actual: DataTypes.BOOLEAN,
    docId: DataTypes.INTEGER,
    docVersionId: DataTypes.INTEGER,
    pageNumber: DataTypes.INTEGER,
    createdBy: DataTypes.INTEGER,
    modifiedBy: DataTypes.INTEGER
  });

  File.associate = function(models) {
    File.belongsTo(models.Document, { foreignKey: 'docId', as: 'document' }); // Pages only
    File.belongsTo(models.User, { foreignKey: 'createdBy', as: 'createdByUser' });
    File.belongsTo(models.User, { foreignKey: 'modifiedBy', as: 'modifiedByUser' });
    File.hasMany(models.Descriptor, { foreignKey: 'holderId', as: 'descriptors' });
  }

  return File;
};