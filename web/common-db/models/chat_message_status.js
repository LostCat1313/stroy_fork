'use strict';

module.exports = function(sequelize, DataTypes) {
    var ChatMessageStatus = sequelize.define('ChatMessageStatus', {
        userId: DataTypes.INTEGER,
        messageId: DataTypes.INTEGER,
    }, {
        tableName: 'ChatMessagesStatus',
        indexes: [
            {
                unique: true,
                fields: ['userId', 'messageId']
            }
        ]
    });

    ChatMessageStatus.associate = function (models) {
        ChatMessageStatus.belongsTo(models.ChatMessage, {foreignKey: 'messageId', as: 'message'});
        ChatMessageStatus.belongsTo(models.User, {foreignKey: 'userId', as: 'user'});
        /* ChatMessagesStatus.belongsTo(models.File, {foreignKey: 'targetId', as: 'file'}); */
    }

    return ChatMessageStatus;
};
