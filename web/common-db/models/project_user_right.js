'use strict';
module.exports = function(sequelize, DataTypes) {
  var ProjectUserRight = sequelize.define('ProjectUserRight', {
    rightMask: DataTypes.INTEGER,
    ProjectId: { type: DataTypes.INTEGER, unique: 'UserProjectIndex' },
    UserId:  { type: DataTypes.INTEGER, unique: 'UserProjectIndex' }
  }, { tableName: 'ProjectUser' });

  // ProjectUser.associate = function(models) {
  //   ProjectUser.belongsTo(models.Project, {as: 'project'});
  //   ProjectUser.belongsTo(models.User, {as: 'user' });
  // }

  return ProjectUserRight;
};
