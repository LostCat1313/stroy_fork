'use strict';
module.exports = function(sequelize, DataTypes) {
  var Event = sequelize.define('Event', {
    type: DataTypes.INTEGER,
    title: DataTypes.STRING,
    content: DataTypes.TEXT,
    projectId: DataTypes.INTEGER,
    targetId: DataTypes.INTEGER
  });
  Event.associate = function(models) {
    // associations can be defined here
    Event.belongsTo(models.Project, {foreignKey: 'projectId'});
    Event.belongsTo(models.ChatMessage, {foreignKey: 'targetId', as: 'message'});
    Event.belongsTo(models.Descriptor, {foreignKey: 'targetId', as: 'descriptor'});
    Event.belongsTo(models.Document, {foreignKey: 'targetId', as: 'document'});
    
    // Event.g
  };
  return Event;
};
