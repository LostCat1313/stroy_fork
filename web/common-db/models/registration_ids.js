'use strict';

module.exports = function(sequelize, DataTypes) {
    var RegistrationId = sequelize.define('RegistrationId', {
      regId: DataTypes.STRING,
      deviceType: DataTypes.INTEGER,
      userId: DataTypes.INTEGER
    }, {
      tableName: 'RegistrationIds'
    });

    RegistrationId.associate = function (models) {
      RegistrationId.belongsTo(models.User, {foreignKey: 'userId', as: 'user'});
    }

    return RegistrationId;
};
