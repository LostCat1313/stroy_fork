'use strict';
module.exports = function(sequelize, DataTypes) {
  var Document = sequelize.define('Document', {
      title: DataTypes.STRING,
      shortTitle: DataTypes.STRING,
      parentId: DataTypes.INTEGER,
      projectId: DataTypes.INTEGER,
      // isMultiPage: DataTypes.BOOLEAN,
      isFolder: {type: DataTypes.BOOLEAN, allowNull: false, defaultValue: false}
      // fileId: DataTypes.INTEGER
  });

  Document.associate = function(models) {
    Document.belongsTo(models.Project, {foreignKey: 'projectId', as: 'project'});
    Document.hasMany(models.File, {foreignKey: 'docId', as: 'pages'});
    Document.hasMany(models.DocumentVersion, {foreignKey: 'documentId', as: 'versions'});
    Document.belongsTo(models.User, { foreignKey: 'createdBy', as: 'createdByUser' });
    Document.belongsTo(models.User, { foreignKey: 'modifiedBy', as: 'modifiedByUser' });
  }

  return Document;
};
