module.exports = function (sequelize, DataTypes) {
  var Right = sequelize.define('Right', {
    refId:DataTypes.INTEGER,
    entity:DataTypes.STRING,
    operation:DataTypes.STRING,
    sign:DataTypes.INTEGER,
    userId:DataTypes.INTEGER,
    groupId:DataTypes.INTEGER
  });

  const basicRights = [ 'create', 'read', 'update', 'delete' ];

  Right.operations = {
    'document': [ ...basicRights, 'edit_markers', 'remove_markers', 'edit_pages', 'remove_pages' ],
    'folder': [ ...basicRights ]
  };
  
  Right.entities = Object.keys(Right.operations);

  return Right;
};