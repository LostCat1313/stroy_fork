'use strict';
module.exports = function(sequelize, DataTypes) {
    var Organization = sequelize.define('Organization', {
        name: DataTypes.STRING
    });
    Organization.associate = function(models) {
        Organization.belongsToMany(models.User, {
          through: 'OrganizationUser',
          as: 'users'
        });
        models.User.belongsToMany(Organization, {
          through: 'OrganizationUser',
          as: 'organizations'
        });
        
        Organization.hasMany(/* MODEL, */{as: 'organizationUsers', foreignKey: 'OrganizationId', onDelete: 'CASCADE', hooks: true})
    }
    return Organization;
};
