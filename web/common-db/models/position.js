'use strict';
module.exports = function(sequelize, DataTypes) {
  var Position = sequelize.define('Position', {
    name: DataTypes.STRING
  });
  return Position;
};