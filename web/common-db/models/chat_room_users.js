'use strict';

module.exports = function(sequelize, DataTypes) {
    var ChatRoomUsers = sequelize.define('ChatRoomUser', {
      roomId: DataTypes.INTEGER,
      userId: DataTypes.INTEGER,
      lastEnteredAt: DataTypes.DATE,
    }, {
        tableName: 'ChatRoomUsers'
    });

    ChatRoomUsers.associate = function (models) {
      ChatRoomUsers.belongsTo(models.ChatRoom, {foreignKey: 'roomId', as: 'room'});
      ChatRoomUsers.belongsTo(models.User, {foreignKey: 'userId', as: 'user'});
    }

    return ChatRoomUsers;
};

