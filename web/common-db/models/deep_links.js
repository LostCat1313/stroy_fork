'use strict';
module.exports = function(sequelize, DataTypes) {
  var DeepLinks = sequelize.define('DeepLink', {
    key: DataTypes.STRING,
    data: DataTypes.TEXT,
    type: DataTypes.INTEGER,
    projectId: DataTypes.INTEGER,
    createdBy: DataTypes.INTEGER,
    modifiedBy: DataTypes.INTEGER,
    activated: DataTypes.DATE
  });
  
  DeepLinks.associate = function(models) {
    DeepLinks.belongsTo(models.User, { foreignKey: 'createdBy', as: 'createdByUser' });
    DeepLinks.belongsTo(models.User, { foreignKey: 'modifiedBy', as: 'modifiedByUser' });
    DeepLinks.belongsTo(models.Project, { foreignKey: 'projectId', as: 'project' });
  }

  return DeepLinks;
};