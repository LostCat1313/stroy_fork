'use strict';
module.exports = function(sequelize, DataTypes) {
    var Token = sequelize.define('Token', {
        token: DataTypes.STRING,
        syncAt: DataTypes.DATE,
        updatedAt: DataTypes.DATE
    });

    Token.associate = function(models) {
        Token.belongsTo(models.User);

        Token.getUserByToken = function(t) {
            // remove all expired tokens
            return Token.destroy({
                where: {
                    syncAt: {
                        $or: {
                            // $lt: new Date(new Date() - 24 * 60 * 60 * 1000),
                            $eq: null
                        }
                    }
                }
            }).then(() => {
                return Token.findOne({where: {token: t}, include: [{model: models.User}]})
                    .then(token => {
                        if (token === null)
                            return Promise.resolve(null);
                        else {
                            // console.log(token);
                            return token.update({syncAt: new Date()}).then(() => {
                                return token.getUser();
                            });
                        }
                            
                    });
            });
        };
    }

    

    return Token;
};
