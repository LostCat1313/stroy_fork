'use strict';

module.exports = function(sequelize, DataTypes) {
    var ChatMessageAttachment = sequelize.define('ChatMessageAttachment', {
        messageId: DataTypes.INTEGER,
        type: DataTypes.INTEGER,
        targetId: DataTypes.INTEGER,
    }, {
        tableName: 'ChatMessageAttachments'
    });

    ChatMessageAttachment.associate = function (models) {
      ChatMessageAttachment.belongsTo(models.ChatMessage, {foreignKey: 'messageId', as: 'message'});
      ChatMessageAttachment.belongsTo(models.File, {foreignKey: 'targetId', as: 'file'});
    }

    return ChatMessageAttachment;
};
