'use strict';
module.exports = function(sequelize, DataTypes) {
  var Project = sequelize.define('Project', {
    title: DataTypes.STRING,
    createdBy: DataTypes.INTEGER,
    modifiedBy: DataTypes.INTEGER
  });

  Project.associate = function(models) {
    Project.belongsToMany(models.User, {through: 'ProjectUser', as: 'users'});
    Project.belongsTo(models.File, {foreignKey: 'imageId', as: 'image'});
    Project.belongsTo(models.User, { foreignKey: 'createdBy', as: 'createdByUser' });
    Project.belongsTo(models.User, { foreignKey: 'modifiedBy', as: 'modifiedByUser' });
    Project.hasMany(models.ProjectUserRight, {as: 'rights'});
  }

  return Project;
};
