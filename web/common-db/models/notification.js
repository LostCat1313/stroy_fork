'use strict';
module.exports = function(sequelize, DataTypes) {
  var Notification = sequelize.define('Notification', {
    userId: DataTypes.INTEGER,
    rId:  DataTypes.INTEGER,
    eventId: DataTypes.INTEGER,
    status: DataTypes.INTEGER
  });

  Notification.associate = function(models) {
    // associations can be defined here
    Notification.belongsTo(models.User, {foreignKey: 'userId'});
    Notification.belongsTo(models.RegistrationId, {foreignKey: 'rId', as: 'registration'});
    Notification.belongsTo(models.Event, {foreignKey: 'eventId'});
  };
  
  return Notification;
};