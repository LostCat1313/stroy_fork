module.exports = function (sequelize, DataTypes) {
  var Group = sequelize.define('Group', {
    name:DataTypes.INTEGER,
    parentId:DataTypes.INTEGER
  });

  Group.associate = function(models) {
    Group.hasMany(models.Right, {
      foreignKey: 'groupId',
      as: 'rights'
    });
    Group.belongsToMany(models.User, {
      through: 'UserGroups',
      foreignKey: 'groupId',
      otherKey: 'userId',
      as: 'users'
    });
  }

  return Group;
};