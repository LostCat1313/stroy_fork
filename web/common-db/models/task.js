module.exports = function (sequelize, DataTypes) {
  var Task = sequelize.define('Task', {
    log: DataTypes.STRING,
    docId: DataTypes.INTEGER,
    refId: DataTypes.INTEGER,
    type: DataTypes.INTEGER,
    status: DataTypes.INTEGER
  });

  return Task;
};