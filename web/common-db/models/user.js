'use strict';
module.exports = function(sequelize, DataTypes) {
    var User = sequelize.define('User', {
        name: DataTypes.STRING,
        email: DataTypes.STRING,
        phoneNumber: DataTypes.STRING,
        firstName: DataTypes.STRING,
        secondName: DataTypes.STRING,
        lastName: DataTypes.STRING,
        password: DataTypes.STRING,
        positionId: DataTypes.INTEGER,
        activationCode: DataTypes.STRING,
        isActive: DataTypes.BOOLEAN,
        isAdmin: DataTypes.BOOLEAN
    });
    User.associate = function(models) {
      User.belongsTo(models.Position, {foreignKey: 'positionId', as: 'position'});
      User.hasMany(models.RegistrationId, {foreignKey: 'userId', as: 'regIds'});
      User.hasMany(models.ProjectUserRight, {as: 'rights'});
      User.belongsToMany(models.Group, {through: 'UserGroups', foreignKey: 'userId', otherKey: 'groupId', as: 'groups'});
      User.hasMany(models.Right, {foreignKey: 'userId', as: 'aclRights'});
      User.belongsToMany(models.Project, {through: 'ProjectUser', as: 'projects'});
      //MIGRATIONS
      User.hasMany(models.ChatMessageStatus, {foreignKey: 'userId', as: 'messageStatuses', onDelete: 'SET NULL', hooks: true})
      User.hasMany(models.ChatMessage, {foreignKey: 'senderId', as: 'messages', onDelete: 'SET NULL', hooks: true})
    };
    return User;
};
