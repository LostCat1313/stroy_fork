module.exports = function (sequelize, DataTypes) {
  var Descriptor = sequelize.define('Descriptor', {
    type: DataTypes.INTEGER,
    params: DataTypes.TEXT,
    title: DataTypes.STRING,
    text: DataTypes.TEXT,
    docId: DataTypes.INTEGER,
    holderId: DataTypes.INTEGER,
    holderType: DataTypes.INTEGER,
    createdBy: DataTypes.INTEGER,
    modifiedBy: DataTypes.INTEGER
  });

  Descriptor.associate = function (models) {
    Descriptor.belongsTo(models.Document, {foreignKey: 'docId', as: 'document'});
    Descriptor.belongsTo(models.File, {foreignKey: 'holderId', as: 'holder'});
    Descriptor.belongsTo(models.User, {foreignKey: 'createdBy', as: 'createdByUser'});
    Descriptor.belongsTo(models.User, {foreignKey: 'modifiedBy', as: 'modifiedByUser'});
  }

  return Descriptor;
};