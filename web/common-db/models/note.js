'use strict';
module.exports = function(sequelize, DataTypes) {
  var Notes = sequelize.define('Notes', {
    content: DataTypes.TEXT
  });
  
  Notes.associate = function(models) {
    Notes.belongsTo(models.User, {foreignKey: 'userId'});
  }

  return Notes;
};