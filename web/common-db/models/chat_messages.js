'use strict';

module.exports = function(sequelize, DataTypes) {
    var ChatMessage = sequelize.define('ChatMessage', {
        senderId: DataTypes.INTEGER,
        roomId: DataTypes.INTEGER,
        content: DataTypes.STRING,
    }, {
        tableName: 'ChatMessages'
    });

    ChatMessage.associate = function (models) {
      ChatMessage.belongsTo(models.ChatRoom, {foreignKey: 'roomId', as: 'room'});
      ChatMessage.belongsTo(models.User, {foreignKey: 'senderId', as: 'sender'});
      
      ChatMessage.hasMany(models.ChatMessageAttachment, {foreignKey: 'messageId', as: 'attachments', onDelete: 'CASCADE', hooks: true})
      ChatMessage.hasMany(models.ChatMessageStatus, {foreignKey: 'messageId', as: 'statuses', onDelete: 'CASCADE', hooks: true})
    }

    return ChatMessage;
};
