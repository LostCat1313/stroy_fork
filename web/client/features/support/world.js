const webdriver = require('selenium-webdriver')
const expect = require('chai').expect
require('chai').use(require('chai-as-promised'))

global.driver = new webdriver.Builder()
    .forBrowser('chrome')
    .build()

driver.manage().timeouts().implicitlyWait(2000)

function CustomWorld() {
    this.activeUsers = []
}

module.exports = function() {
    this.World = CustomWorld
}
