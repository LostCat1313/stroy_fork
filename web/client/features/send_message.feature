#language: ru

Функционал: Отправка сообщения 

  Сценарий: Пользователь отправляет сообщение
    Дано в системе существуют пользователи:
      | Имя                | Пароль |
      | Василиса Премудрая |      1 |
      | Иванушка-дурачок   |      2 |
      | Конёк-Горбунок     |      3 |
    И выполнен вход от имени пользователя "Василиса Премудрая"
    Когда я открываю страницу "Чат"
    И пользователь "Иванушка-дурачок" подключается к чату 
    И открываю чат с пользователем "Иванушка-дурачок"
    Тогда список сообщений содержит 0 запись(ей)
    Когда пользователь "Иванушка-дурачок" отправляет сообщение "Привет!" пользователю "Василиса Премудрая"
    Тогда список сообщений содержит записи:
      | Статус   | Текст   |
      | Входящее | Привет! |
    Когда я отправляю сообщение "Ты кто такой будешь?"
    Тогда поле ввода сообщения очищается
    И список сообщений содержит записи:
      | Статус    | Текст                |
      | Входящее  | Привет!              |
      | Исходящее | Ты кто такой будешь? |
    Когда пользователь "Конёк-Горбунок" подключается к чату 
    И открываю чат с пользователем "Конёк-Горбунок"
    Тогда список сообщений содержит 0 запись(ей)
    Когда я отправляю сообщение "Ночь, улица, фонарь, аптека"
    И пользователь "Конёк-Горбунок" отправляет сообщение "Чё?" пользователю "Василиса Премудрая"
    Тогда список сообщений содержит записи:
      | Статус    | Текст                       |
      | Исходящее | Ночь, улица, фонарь, аптека |
      | Входящее  | Чё?                         |
    Когда открываю чат с пользователем "Иванушка-дурачок"
    Тогда список сообщений содержит записи:
      | Статус    | Текст                |
      | Входящее  | Привет!              |
      | Исходящее | Ты кто такой будешь? |
