const webdriver = require('selenium-webdriver'),
      chai = require('chai'),
      expect = chai.expect,
      until = webdriver.until,
      By = webdriver.By,
      db = require('../../db'),
      _ = require('lodash'),
      uuid = require('uuid/v4'),
      io = require('socket.io-client')

chai.use(require('chai-as-promised'))

function getPage(route) {
    return 'http://localhost:8000/' + route
}

module.exports = function() {
    this.Given(/^в системе существуют пользователи:$/, function(table) {
        const specRows = table.hashes(),
              translation = {
                  "Имя": "name",
                  "Пароль": "password"
              }
        
        const users = specRows.map(row => {
            const user = {}
            Object.keys(row).forEach(field => {
                const fieldName = translation[field]
                user[fieldName] = row[field]
            })

            return user
        })
                    
        return db.User.destroy({where: {}})
            .then(() => db.User.bulkCreate(users))
    })

    this.defineStep(/^выполнен вход от имени пользователя "([^"]*)"$/, function(username) {
        return Promise.all([
            db.Token.destroy({where: {}}),
            db.User.findOne({where: {name: username}}),
            driver.get(getPage('login'))
        ]).then(([, user, ]) => {
            if (user === null)
                throw 'Unable to find user with name: ' + username
            return Promise.all([
                driver.findElement(By.css('#login')).sendKeys(user.name),
                driver.findElement(By.css('#password')).sendKeys(user.password)
            ])
        }).then(() => {
            return driver.findElement(By.css("#loginBtn")).click()
        })
    })

    this.defineStep(/^я открываю страницу "([^"]*)"$/, function(pagename) {
        const translation = {
            'Чат': 'chat'
        }

        const url = translation[pagename]
        if (url === undefined)
            throw 'Не могу подобрать URl для страницы ' + pagename

        return driver.findElement(By.css('#chatLink'))
            .click()
            .then(() => driver.wait(
                until.elementLocated(By.css('#users b')),
                undefined,
                'Не найден список пользователей'
            ))
    })

    this.defineStep(/^список пользователей содержит (\d+) запись\(ей\)$/, (recordsCount) => {
        return driver.findElements(By.css('#users ul li'))
            .then(users => expect(users.length).to.be.eql(parseInt(recordsCount)))
    })

    this.defineStep(/^список пользователей содержит записи:$/, (table) => {
        const rows = table.hashes(),
              translation = {
                  "Имя": {fieldName: "name", convert: (x) => x},
                  "Статус": {
                      fieldName: "status",
                      convert: (x) => {
                          if (x === 'В сети')
                              return 'online'
                          else if (x === 'Не в сети')
                              return 'offline'
                          else
                              throw 'Не удалось преобразовать значение ' + x 
                      }
                  }
              }
        
        const users = rows.map(row => {
            const user = {}
            Object.keys(row).forEach(field => {
                const fieldName = translation[field].fieldName
                user[fieldName] = translation[field].convert(row[field])
            })

            return user
        })

        return driver.wait(function() {
            return driver.findElements(By.css('#users ul li'))
            .then(usersElements => {
                return Promise.all(usersElements.map(el => {
                    return Promise.all([
                        el.getText(),
                        el.getAttribute('class')
                    ]).then(([name, className]) => {
                        return {
                            name,
                            status: className.includes('status-online') ? 'online' : 'offline'
                        }
                    })
                }))
            })
            .then(displayedUsers => {
                return _.isEqual(displayedUsers, users)
            })
        })
    })

    this.defineStep(/^пользователь "([^"]*)" подключается к чату$/, function(username) {
        return db.User.findOne({where: {name: username}})
            .then(user => db.Token.create({token: username, UserId: user.id}))
            .then(() => {
                const socket = io('http://localhost:8000')
                this.activeUsers.push({name: username, socket})
                socket.emit('login', username)
            })
    })

    this.defineStep(/^пользователь "([^"]*)" отключается от чата$/, function(username) {
        const i = this.activeUsers.findIndex(u => u.name === username)

        if (i > -1) {
            this.activeUsers[i].socket.close()
            this.activeUsers.splice(i)
        }
    })

    this.defineStep(/^открываю чат с пользователем "([^"]*)"$/, function(username) {
        return driver.findElements(By.css('#users ul li'))
            .then(els => {
                return Promise.all(els.map(e => e.getText()))
                    .then(content => {
                        const i = content.indexOf(username)

                        if (i > -1)
                            return els[i].click()
                        else
                            throw 'Unable to find user with name: ' + username
                    })
            })
    })

    this.defineStep(/^список сообщений содержит (\d+) запись\(ей\)$/, function(recordsCount) {
        return driver.findElements(By.css('#messages ul li'))
            .then(records => expect(records.length).to.be.eql(parseInt(recordsCount)))
    })
    
    this.defineStep(/^пользователь "([^"]*)" отправляет сообщение "([^"]*)" пользователю "([^"]*)"$/, function(from, message, to) {
        const user = this.activeUsers.find(({name}) => name === from)

        return db.User.findOne({where: {name: to}})
            .then(receiver => {
                user.socket.emit('message', {to: receiver.id, content: message})
            })
    })

    this.defineStep(/^список сообщений содержит записи:$/, function(messagesSpec) {
        const rows = messagesSpec.hashes(),
              translation = {
                  "Текст": {fieldName: "text", convert: (x) => x},
                  "Статус": {
                      fieldName: "status",
                      convert: (x) => {
                          if (x === 'Входящее')
                              return 'incoming'
                          else if (x === 'Исходящее')
                              return 'outgoing'
                          else
                              throw 'Не удалось преобразовать значение ' + x 
                      }
                  }
              }

        const messages = rows.map(row => {
            const message = {}
            Object.keys(row).forEach(field => {
                const fieldName = translation[field].fieldName
                message[fieldName] = translation[field].convert(row[field])
            })

            return message
        })

        return driver.wait(function() {
            return driver.findElements(By.css('#messages ul li'))
            .then(els => {
                return Promise.all(els.map(el => {
                    return Promise.all([
                        el.getText(),
                        el.getAttribute('class')
                    ]).then(([text, className]) => {
                        return {
                            text,
                            status: className.includes('incoming') ? 'incoming' : 'outgoing'
                        }
                    })
                }))
            })
            .then(displayedMessages => {
                return _.isEqual(displayedMessages, messages)
            })
        })
    })

    this.defineStep(/^я отправляю сообщение "([^"]*)"$/, function(message) {
        return driver.findElement(By.css('#messageInput'))
            .sendKeys(message)
            .then(() => driver.findElement(By.css('#sendBtn')).click())
    })

    this.defineStep(/^поле ввода сообщения очищается$/, function() {
        return expect(driver.findElement(By.css('#messageInput')).getAttribute('value'))
            .to.eventually.be.eql('')
    })
}
