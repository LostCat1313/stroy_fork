var reducer = require('../../src/reducers/session')

describe('session', () => {

  it('should not change the passed state', (done) => {

    const state = Object.freeze({})
    reducer(state, {type: 'INVALID'})

    done()
  })
})
