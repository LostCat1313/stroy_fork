import React from 'react'
import { shallow } from 'enzyme'
import UsersEdit from 'components/organizations/UsersEdit.js'

describe('<UsersEdit />', function () {

  let component
  beforeEach(function () {
    component = shallow(<UsersEdit />)
  })

  describe('when rendering the component', function () {

    it('should have a className of "usersedit-component"', function () {
      expect(component.hasClass('usersedit-component')).to.equal(true)
    })
  })
})
