import React from 'react'
import { shallow } from 'enzyme'
import PrivateRoute from 'components/PrivateRoute.js'

describe('<PrivateRoute />', function () {

  let component
  beforeEach(function () {
    component = shallow(<PrivateRoute />)
  })

  describe('when rendering the component', function () {

    it('should have a className of "privateroute-component"', function () {
      expect(component.hasClass('privateroute-component')).to.equal(true)
    })
  })
})
