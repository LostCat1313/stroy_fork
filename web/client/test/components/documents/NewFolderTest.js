import React from 'react'
import { shallow } from 'enzyme'
import NewFolder from 'components/documents/NewFolder.js'

describe('<NewFolder />', function () {

  let component
  beforeEach(function () {
    component = shallow(<NewFolder />)
  })

  describe('when rendering the component', function () {

    it('should have a className of "newfolder-component"', function () {
      expect(component.hasClass('newfolder-component')).to.equal(true)
    })
  })
})
