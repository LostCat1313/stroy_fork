import React from 'react'
import { shallow } from 'enzyme'
import UsersIndex from 'components/projects/UsersIndex.js'

describe('<UsersIndex />', function () {

  let component
  beforeEach(function () {
    component = shallow(<UsersIndex />)
  })

  describe('when rendering the component', function () {

    it('should have a className of "usersindex-component"', function () {
      expect(component.hasClass('usersindex-component')).to.equal(true)
    })
  })
})
