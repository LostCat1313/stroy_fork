/* CAUTION: When using the generators, this file is modified in some places.
 *          This is done via AST traversal - Some of your formatting may be lost
 *          in the process - no functionality should be broken though.
 *          This modifications only run once when the generator is invoked - if
 *          you edit them, they are not updated again.
 */
import React, {
  Component
} from 'react';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import blue from '@material-ui/core/colors/blue';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { reset, verifyToken } from '../actions';
import AppComponent from '../components/AppComponent';
import { CssBaseline } from '@material-ui/core';
import { withStore } from 'decorators';

// const mapStateToProps = (state) => ({ Session: state.Session })
  
// const mapDispatchToProps = (dispatch) => {
//   const actions = { reset, verifyToken }
//   const actionMap = { actions: bindActionCreators(actions, dispatch) }
//   return actionMap
// }

const theme = createMuiTheme({
  palette: {
    primary: blue,
  },
  typography: {
    useNextVariants: true,
  },
})
@withStore('Session', { preload: false, actions: { reset, verifyToken } })
export default class App extends Component {
  componentWillReceiveProps(nextProps) {
    // rehydration completed
    const { Session } = this.props;
    const { Session:Next } = nextProps;
    if(Session.initializing && !Next.initializing) {
      if(Next.token)
        this.props.actions.verifyToken().then((result) => {
          if(result.status != 'success') {
            this.props.actions.reset();
          }
        });
      else this.props.actions.reset();
    }
  }

  renderApp() {
    if (this.props.Session.initializing)
      return  <div className="preloading">Загрузка...</div>
    else if (!this.props.Session.verified && this.props.Session.loading)
      return  <div className="preloading">Проверка сессии...</div>
    else
      return <AppComponent/>
  }

  render() {
    return <MuiThemeProvider theme={theme}>
        <CssBaseline/>
        {this.renderApp()}
      </MuiThemeProvider>
  }
}
