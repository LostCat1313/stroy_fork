import React from 'react';

import { EditComponent, TextField } from 'components';
import { createRegId, updateRegId } from 'actions';
import { withStore } from 'decorators';

@withStore('RegId', {preload: false, actions: { createRegId, updateRegId } })
export default class RegIdEdit extends EditComponent {
  constructor(props) {
    super(props, 'RegId');
    this.backUri = '/regids';
  }

  componentDidMount() {
  }

  renderHeader() {
    let entry = this.getEntry();
    return entry
      ? "Редактирование сессии " + entry.id
      : "Новая сессия";
  }

  renderFields() {
    return [
      <TextField
        name="regId"
        label="RegId"
        fullWidth
        required
      />
    ];
  }
}
