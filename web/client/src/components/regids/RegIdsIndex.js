import React from 'react'
import { Link } from 'react-router-dom'

import { Table, Header } from 'components'
import { withStore } from 'decorators'

@withStore('RegId', {actions: ['load', 'delete', 'deleteAll']})
export default class RegIdsIndex extends React.Component {
  constructor(props) {
    super(props)
  }

  deleteRegId = (id) => {
    this.props.actions.deleteRegId(id)
  }

  // deleteAll(e) {
  //   this.props.actions.deleteAllRegIds(this.props.RegId.list.map(i => i.id))
  //   e.preventDefault()
  //   return false
  // }

  render() {
    return (
      <div className="container">
        <Header>Список мобильных сессий</Header>
        <p><Link to="/regids/new">Добавить</Link></p>
        {/* <p><a href="#" onClick={this.deleteAll.bind(this)}>Удалить все</a></p> */}
        <Table
          dataList={this.props.RegId.list}
          headerColumns={[{ type: 'id' }, "RegId", "Тип", { text: "Действия", type: "actions" }]}
          rowColumns={[
            { type: 'id' },
            item => item.regId,
            item => item.deviceType,
            {
              type: 'actions',
              actions: {
                edit: regId => '/regIds/' + regId.id + '/edit',
                remove: regId => ({
                  onClick: () => this.deleteRegId(regId.id)
                })
              }
            }
          ]}
        ></Table>
      </div>
    )
  }
}
