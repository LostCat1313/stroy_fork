import React from 'react'
import { AppBar, Toolbar, IconButton, Typography, withStyles } from '@material-ui/core'
import MenuIcon from '@material-ui/icons/Menu'
import { HashRouter as Router } from 'react-router-dom'

import MenuComponent from './menu/MenuComponent'
import NotAuthenticatedSwitch from './main/NotAuthenticatedSwitch'
import AuthenticatedSwitch from './main/AuthenticatedSwitch'
import { withStore } from 'decorators'

@withStore('Session', {preload: false})
export default class AppComponent extends React.Component {
  render() {
    const { classes } = this.props
    return (
      <Router>
        {this.props.Session.authenticated
          ? <MenuComponent> 
              <AuthenticatedSwitch/>
            </MenuComponent>
          : <NotAuthenticatedSwitch/>
        }
      </Router>
    )
  }
}
