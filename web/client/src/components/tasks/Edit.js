import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { EditComponent, TextField, ComboBox } from 'components'
import { createTask, updateTask } from 'actions'

class Edit extends EditComponent {
  constructor(props) {
    super(props, 'Task')
    this.backUri = '/tasks'
  }

  componentDidMount() {
  }

  renderHeader() {
    let entry = this.getEntry()
    return entry
      ? "Редактирование задачи " + entry.id
      : "Новая задача"
  }

  renderFields() {
    return [
      <TextField
        name="refId"
        label="Ссылка"
        required
      />,
      <ComboBox
        name="type"
        label="Тип преобразования"
        items={[
          { value: 1, text: 'PDF в страницы (1)' },
          { value: 2, text: 'Документ в PDF (2)' },
          { value: 3, text: 'Thumbnail для документа (3)' },
          { value: 4, text: 'Thumbnail для файла (4)' }
        ]}
        required
      />,
      <TextField
        name="status"
        label="Статус"
        defaultValue={1}
        required
      />
    ]
  }
}

function mapStateToProps(state) {
  const props = { Task: state.Task }
  return props
}

function mapDispatchToProps(dispatch) {
  const actions = { createTask, updateTask }
  const actionMap = { actions: bindActionCreators(actions, dispatch) }
  return actionMap
}


export default connect(mapStateToProps, mapDispatchToProps)(Edit)
