import React from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { Table } from 'components'
import { loadTasks, deleteTask } from 'actions'

const mapStateToProps = state => ({ task: state.Task })
// const mapDispatchToProps = { loadTasks, deleteTask }
function mapDispatchToProps(dispatch) {
  const actions = { loadTasks, deleteTask }
  const actionMap = { actions: bindActionCreators(actions, dispatch) }
  return actionMap
}

@connect(mapStateToProps, mapDispatchToProps)
export default class Index extends React.Component {

  constructor(props) {
    super(props)
  }

  componentDidMount() {
    this.props.actions.loadTasks({limit:20})
    this.timer = setInterval(() => {
      this.props.actions.loadTasks({limit:20})
    }, 3000)
  }

  componentWillUnmount() {
    clearInterval(this.timer)
  }

  deleteTask(id) {
    this.props.actions.deleteTask(id)
  }

  renderStatus(status) {
    switch(status) {
      case 0: return 'Ошибка'
      case 1: return 'Ожидает выполнения'
      case 2: return 'В процессе'
      case 3: return 'Завершено'
    }
  }

  render() {
    return (
      <div className="container">
        <h1>Список задач</h1>
        <p><Link to="/tasks/new">Добавить</Link></p>
        <Table
          dataList={this.props.task.list}
          headerColumns={[
            { type: 'id' },
            {
              tooltip: "Ссылка (Тип) / Статус",
              text: <div>Ссылка (Тип)<br/>Статус</div>,
              style: { width: '150px' }
            },
            "Лог",
            { text: "Действия", type: "actions" }]}
          rowColumns={[
            { type: 'id' },
            { 
              render: task => <div>{task.refId + ' (' + task.type + ')'}<br/>{this.renderStatus(task.status)}</div>,
              style: { width: '150px' }
            },
            task => <div style={{wordWrap:'break-word', whiteSpace: 'normal'}}>{task.log}</div>,
            {
              type: 'actions',
              actions: {
                edit: task => '/tasks/' + task.id + '/edit',
                remove: task => ({
                  onClick: () => this.deleteTask(task.id)
                })
              }
            }
          ]}
        ></Table>
      </div>
    )
  }
}
