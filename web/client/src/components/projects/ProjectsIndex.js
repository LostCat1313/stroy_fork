import React from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import moment from 'moment'

import { Typography } from '@material-ui/core'
import { Table, Header } from 'components'
import withStore from '../../decorators/withStore'

@withStore('Project')
export default class ProjectsIndex extends React.Component {

  static propTypes = {
    Project: PropTypes.object
  }

  deleteProject(id) {
    this.props.actions.deleteProject(id)
  }

  render() {
    const url = this.props.match.url
    return (
      <div className="container">
        <Header>Список проектов</Header>
        <Link to="/projects/new">Добавить</Link>
        <Table
          dataList={this.props.Project.list}
          headerColumns={[
            { type: 'id' },
            "Название",
            {
              tooltip: "Документов, Пользователей",
              text: <div>Документов<br/>Пользователей</div>
            },
            "Создан",
            { type: 'actions', text: "Действия" }]}
          rowColumns={[
            { type: 'id' },
            item => item.title,
            item => <div>{item.documentsCount}<br />{item.usersCount}</div>,
            item => moment(item.createdAt).format('DD-MM-YYYY'),
            {
              type: 'actions',
              actions: {
                show: item => '/projects/' + item.id,
                edit: item => '/projects/' + item.id + '/edit',
                folders: item => '/projects/' + item.id + '/documents',
                users: item => '/projects/' + item.id + '/users',
                remove: item => ({
                  onClick: () => this.deleteProject(item.id)
                })
              }
            }
          ]}></Table>
      </div>
    )
  }
}
