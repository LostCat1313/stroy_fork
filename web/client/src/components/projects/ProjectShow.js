import React from 'react'
import { connect } from 'react-redux'

import { BaseComponent } from 'components'

class Show extends BaseComponent {
    constructor(props) {
        super(props)
        this.state = { project: this.getCurrentEntry(this.props.project.list) }
    }

    imageTag() {
        if (this.state.project.image)
            return <img src={this.state.project.image.thumbUrl}/>
        else
            return <p>Изображение не загружено</p>
    }

    render() {
        return (
            <div className="container">
                <h1>Проект</h1>
                <br/>
                <b>Название:</b>
                <br/>
                <p>
                    {this.state.project.title}
                </p>
                <b>Создан:</b>
                <br/>
                <p>
                    {this.state.project.createdAt}
                </p>
                <b>Изображение:</b>
                <br/>
                {this.imageTag()}
            </div>
        )
    }
}

function mapStateToProps(state) {
    const props = { project: state.Project }
    return props
}

export default connect(mapStateToProps, null)(Show)
