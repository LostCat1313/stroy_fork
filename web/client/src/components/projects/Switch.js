import React from 'react'
import { Switch, Route } from 'react-router-dom'

import Edit from './ProjectEdit'
import Show from './ProjectShow'
import ProjectsIndex from './ProjectsIndex'
import DocumentsIndex from 'components/documents/DocumentsIndex'
import ProjectsUsersIndex from 'components/projects/ProjectUsersIndex'
import ProjectsUsersEdit from 'components/projects/ProjectUsersEdit'

export default ({match: {url}}) => {
  return (
    <Switch>
      <Route exact path={url} component={ProjectsIndex}/>
      <Route exact
              path={`${url}/:projectId/documents`}
              component={DocumentsIndex} />
      <Route exact path={`${url}/new`} component={Edit} />
      <Route exact path={`${url}/:id`} component={Show} />
      <Route exact path={`${url}/:id/edit`} component={Edit} />
      {/* <Route exact
          path={`${url}/:projectId/documents/new`}
          component={DocumentsEdit} />
      <PropsRoute exact
          path={`${url}/:projectId/documents/newfolder`}
          isFolder
          component={DocumentsEdit}/> */}
      <Route exact
          path={`${url}/:id/users`}
          component={ProjectsUsersIndex} />
      <Route exact
          path={`${url}/:id/users/edit`}
          component={ProjectsUsersEdit} />
    </Switch>
  )
}