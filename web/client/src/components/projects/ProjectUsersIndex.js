import React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { bindActionCreators } from 'redux'

import { loadProjectUsers } from 'actions'
import { Table, Utils, Header } from 'components'
import { withStore } from 'decorators';

@withStore('Project', { preload: false, actions: { loadProjectUsers } })
class UsersIndex extends React.Component {
  constructor(props) {
    super(props)
    this.state = { users: [], project: null, loading: false }
  }

  componentDidMount() {
    this.setState({ loading: true });
    this.props.actions.loadProjectUsers(this.props.match.params.id).then(() => {
      this.setState({ loading: false });
    })
  }
  // todo: refactor to common function
  getName() {
    const { Project, match } = this.props;
    if (Project.list) {
      var current = Project.list.find(v => v.id == match.params.id)
      if (current) {
        return current.title
      }
    }
    return ""
  }

  render() {
    const { Project, match } = this.props;
    return (
      <div className="container">
        <Header>Участники проекта: {this.getName()} - просмотр</Header>
        <p><Link to={'/projects/' + match.params.id + '/users/edit'}>
          Изменить
        </Link></p>
        <Table
          dataList={Project.users}
          headerColumns={[{type: 'id'}, "Логин", "Имя", "Права"]}
          rowColumns={[
            user => user.id,
            user => user.name,
            user => Utils.renderUserName(user),
            user => Utils.renderRight(user.rightMask),
          ]}></Table>
      </div>
    )
  }
}

export default UsersIndex;
