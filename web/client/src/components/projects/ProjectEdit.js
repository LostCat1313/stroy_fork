import React from 'react'

import { EditComponent, TextField, FileField, LineBreak } from 'components'
import withStore from '../../decorators/withStore';

@withStore('Project', { actions: ['create', 'update'], preload: false })
export default class ProjectEdit extends EditComponent {
  constructor(props) {
    super(props, 'Project')
    this.backUri = '/projects'
  }

  renderHeader() {
    let entry = this.getEntry()
    return entry
      ? "Редактирование проекта " + entry.title
      : "Новый проект"
  }

  renderFields() {
    return [
          <TextField
            name="title"
            label="Название проекта"
            required
          />,
          <LineBreak/>,
          <FileField
            name="image"
            label="Основное изображение"
          />
    ]
  }
}
