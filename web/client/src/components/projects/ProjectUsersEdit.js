import React from 'react';
import { Button } from '@material-ui/core';
import Formsy from 'formsy-react';

import { loadUsers, loadProjectUsers, saveProjectUsers } from 'actions';
import { Table, Utils, CheckBox, Header, Loading } from 'components';
import RightsComboBox from 'components/common/fields/rights/RightsComboBox';
import { withStore } from 'decorators';

@withStore(
  {
    name: 'User',
    preload: false,
    actions: { loadUsers, loadProjectUsers, saveProjectUsers }
  }, 
  {
    name: 'Project',
    preload: false,
  }
)
export default class ProjectUsersEdit extends React.Component {

  formsy = React.createRef();
  state = {
    disabled: false,
    loading: false
  }

  componentDidMount() {
    const { actions, match } = this.props;
    this.setState({ loading: true });
    actions.loadProjectUsers(match.params.id).then(() => {
      actions.loadUsers().then(() => {
        this.setState({ loading: false });
      });
    });
  }
  // todo: refactor to common
  getName() {
    if (this.props.Project.list) {
      var current = this.props.Project.list.find(v => v.id == this.props.match.params.id)
      if (current) {
        return current.name
      }
    }
    return ""
  }

  getProjectUserAdditionalInfo = (user) => {
    if(this.props.Project.users) {
      const u = this.props.Project.users.find(u => u.id == user.id);
      if(u) return { selected: true, rightMask: u.rightMask };
    }
    return { selected: false, rightMask: 1 };
  }
  onSaveClick = (event) => {
    this.formsy.current.submit();
    event.stopPropagation()
  }

  onCancelClick = e => {
    this.props.history.push('/projects/' + this.props.match.params.id + '/users');
  }

  handleSubmit = model => {
    let result = [];
    for(let key in model.selected) {
      if(model.selected[key] && model.right[key]) {
        result.push({ UserId: +key, rightMask: model.right[key] })
      }
    }
    if(result.length > 0) {
      this.setState({ disabled: true });
      this.props.actions.saveProjectUsers({ projectId: this.props.match.params.id, rights: result})
        .then(v => {
          if(v.status === 'success') {
            this.props.history.push('/projects/' + this.props.match.params.id + '/users');
          }
        }, e => {
          // TODO: add toasts to show status of commands
          console.log(e);
          this.setState({ disabled: false });
        });
    } 
    else {
      // TODO: prompt an error
      alert('У выбранных пользователей необходимо указать права');
      this.setState({ disabled: false});
    }
  }

  render() {
    // to state
    let list = this.props.User.list 
      ? this.props.User.list.map(u => ({...u, ...this.getProjectUserAdditionalInfo(u)}))
      : null;

    if(this.state.loading) {
      return <Loading center/>
    }

    return (
      <div className="container">
        <Header>Участники проекта: {this.getName()} - редактирование</Header>
        <Button onClick={this.onSaveClick}>Сохранить</Button>
        <Button onClick={this.onCancelClick}>Отмена</Button>
        <Formsy 
          disabled={this.state.disabled}
          onValidSubmit={this.handleSubmit}
          ref={this.formsy}>
          <Table
            dataList={list}
            headerColumns={
              [{text: ' ', style:{width: '40px'}}, "Логин", "Имя", {text: "Права", style: {width: '200px'}}]
            }
            rowColumns={[
              user => <CheckBox defaultValue={user.selected} name={'selected.' + user.id}/>,
              user => user.name,
              user => Utils.renderUserName(user),
              user => <RightsComboBox defaultValue={user.rightMask} fullWidth placeholder="Не выбрано" name={'right.' + user.id}/>,
            ]}
          ></Table>
        </Formsy>
      </div>
    )
  }
}
