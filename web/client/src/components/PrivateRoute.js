import React from 'react'
import { Route } from 'react-router-dom'

export default class PrivateRoute extends React.Component {
  render() {
      return <Route {...this.props}/>
  }
}
