import React, { Fragment } from 'react'
import { List, ListItem, ListItemIcon, ListItemText, Collapse, withStyles } from '@material-ui/core'
import { ExpandMore, ExpandLess } from '@material-ui/icons'
import { Link, withRouter } from 'react-router-dom'
import PropTypes from 'prop-types'

import MenuIcon from './icons/MenuIcon'
import BaseComponent from './BaseComponent'

const styles = (theme) => ({
  
})

@withRouter
@withStyles(styles, {withTheme:true})
export default class LinkListItemComponent extends BaseComponent {
  static propTypes = {
    to:PropTypes.string,
    onClick:PropTypes.func,
    level:PropTypes.number,
    text:PropTypes.string.isRequired,
    classes:PropTypes.object.isRequired,
    theme:PropTypes.object.isRequired
  }

  state = {
    open: false
  }
  
  handleClick = (e) => {
    const { onClick, nestedItems, history, to } = this.props;
    e.preventDefault();
    if(to) {
      history.push(to);
    }
    if(onClick) {
      onClick(e);
    }
    if(nestedItems) {
      this.setState(prevState => ({ open: !prevState.open }));
    }
  }

  render() {
    const { menuIcon, nestedItems, text, theme, level } = this.props
    const { open } = this.state
    
    return <Fragment>
      <ListItem
        button
        style={{paddingLeft:level> 0 ? level * theme.spacing.unit * 3 : null}}
        onClick={this.handleClick}>
        {menuIcon && <ListItemIcon>
          <MenuIcon iconName={menuIcon}></MenuIcon>
        </ListItemIcon>}
        <ListItemText>
          {text}
        </ListItemText>
        {nestedItems && (open ? <ExpandLess /> : <ExpandMore />)}
      </ListItem>
      {nestedItems && <Collapse in={this.state.open} timeout="auto" unmountOnExit>
        <List component="div" disablePadding>
          {this.addKeys(nestedItems)}
        </List>
      </Collapse>}
    </Fragment>
  }
}