import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'
import CloseCircleIcon from 'mdi-react/CloseCircleIcon'
import InformationIcon from 'mdi-react/InformationIcon'

const styles = (theme) => ({
  controls: {
    position: 'absolute',
    top: '3px',
    right: '3px',
    width: '16px'
  },
  closeIcon: {
    '& path': {
      fill: theme.palette.primary.main
    }
  },
  infoIcon: {
    '& path': {
      fill: theme.palette.primary.main
    }
  }
})

@withStyles(styles)
export default class ListItemControls extends React.Component {
  static propTypes = {
    style:PropTypes.object,
    infoButton:PropTypes.bool,
    deleteButton:PropTypes.bool,
    classes:PropTypes.object.isRequired
  }

  clickHandler = (action, event) => {
    console.log('ListItemControls::action', action)
    if(this.props.onAction) {
      event.preventDefault()
      event.stopPropagation()
      this.props.onAction(action)
      return false
    }
  }

  render() {
    const { classes, deleteButton, infoButton } = this.props
    return <div className={classes.controls}>
      {deleteButton && <CloseCircleIcon 
        className={classes.closeIcon}
        width={16}
        height={16}
        onClick={this.clickHandler.bind(this, 'onDelete')}/>}
      {infoButton && <InformationIcon 
        className={classes.infoIcon}
        width={16}
        height={16}
        onClick={this.clickHandler.bind(this, 'onInfo')}/>}
    </div>
  }
}