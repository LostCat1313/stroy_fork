import React from 'react'
import PropTypes from 'prop-types'
import Formsy from 'formsy-react'
import { Dialog, DialogContent, DialogActions, DialogTitle, withStyles, Button } from '@material-ui/core'

import { MdiIcon } from 'components'
import RightsEditLine from './RightsEditLine'

const styles = theme => ({
  root: {
  },
  icon: {
    marginTop: theme.spacing.unit,
    flex: '0 0 24px'
  },
  content: {
    display:'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  container: {
    display:'flex',
    flexDirection: 'column',
    'overflow-y':'auto'
  }
})

@withStyles(styles)
export default class RightsDialog extends React.Component {
  static propTypes = {
    open:PropTypes.bool.isRequired,
    classes:PropTypes.object.isRequired,
    refId:PropTypes.number,
    onCancel:PropTypes.func
  }

  state = {
    rights: [{key:1}]
  }

  okDialogHandler = () => {
    
  }

  cancelDialogHandler = () => {
    this.props.onCancel()
  }

  rightAddHandler = () => {
    this.setState(prevState => {
      const { rights } = prevState 
      const key = rights.length > 0 ? rights[rights.length-1].key + 1 : 1
      return { rights:[...rights, { key }] }
    })
  }

  rightRemoveHandler = (item, index) => {
    this.setState(prevState => {
      let rights = [...prevState.rights]
      rights.splice(index, 1)
      console.log('RightsDialog::rightRemoveHandler:', item, index, rights)
      return {
        rights
      }
    })
  }

  render() {
    const { open, classes, title, onClose } = this.props
    const { rights } = this.state
    return <Dialog
      className={classes.root}
      open={open}>
      <DialogTitle>
        {title}
      </DialogTitle>
      <DialogContent className={classes.content}>
        <div className={classes.container}>
          {rights.length == 0 && 'Нет элементов для отображения'}
          {rights.map((r, index) => <RightsEditLine 
            key={r.key}
            model={r}
            entity={'document'}
            onRemove={this.rightRemoveHandler.bind(this, r, index)}/>)}
        </div>
        <MdiIcon 
          button
          primary
          onClick={this.rightAddHandler}
          className={classes.icon}
          iconName="PlusCircleIcon"/>
      </DialogContent>
      <DialogActions>
        <Button
          onClick={this.okDialogHandler}>Ок</Button>
        <Button
          onClick={this.cancelDialogHandler}>Отмена</Button>
      </DialogActions>
    </Dialog>
  }
}