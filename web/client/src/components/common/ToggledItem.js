import React from 'react'
import { Typography, ExpansionPanel, ExpansionPanelSummary, ExpansionPanelDetails, withStyles } from '@material-ui/core';
import { ExpandMore } from '@material-ui/icons';
import classNames from 'classnames';

@withStyles(theme => ({
  root: {

  },
  detailsVertical: {
    flexDirection: 'column',
    alignItems: 'center'
  }
}))
export default class ToggledItem extends React.Component {
  
  render() {
    const { classes, title, className, vertical } = this.props;
    // return <div></div>;
    return <ExpansionPanel className={classNames(classes.root, className)}>
      <ExpansionPanelSummary classes={{content: classes.summary, expanded: classes.expanded}} expandIcon={<ExpandMore />}>
        <Typography variant="subtitle1" className={classes.subtitle}>{title}</Typography>
      </ExpansionPanelSummary>
      <ExpansionPanelDetails className={classNames({[classes.detailsVertical]: vertical})}>
        {this.props.children}
      </ExpansionPanelDetails>
    </ExpansionPanel>;
  }
}