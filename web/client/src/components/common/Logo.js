import React from 'react'

const Logo = () => <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" width="100" height="100" viewBox="0 0 48 48">
  <g>
    <path style={{fill: '#29b6f6'}} d="M28.4,15.5a.4.4,0,0,1,.4.4v2H18.4v-2a.4.4,0,0,1,.4-.4h9.6m0-1H18.8a1.4,1.4,0,0,0-1.4,1.4V19H29.8V15.9a1.4,1.4,0,0,0-1.4-1.4Z"/>
    <path style={{fill: '#29b6f6'}} d="M34.8,22.5a.4.4,0,0,1,.4.4v2H12.1v-2a.4.4,0,0,1,.4-.4H34.8m0-1H12.5A1.4,1.4,0,0,0,11,22.9V26H36.2V22.9a1.4,1.4,0,0,0-1.4-1.4Z"/>
    <path style={{fill: '#29b6f6'}} d="M40.8,29.4m0-1H6.5A1.4,1.4,0,0,0,5,29.8v3.1H42.3V29.8a1.4,1.4,0,0,0-1.4-1.4Z"/>
  </g>
</svg>

export default Logo