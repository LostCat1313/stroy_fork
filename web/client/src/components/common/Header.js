import React, { Fragment } from 'react';
import ReactDOM from 'react-dom';


export default class Header extends React.Component {
  el = document.createElement('span');
  headerRoot = document.getElementById('header-root');

  componentDidMount() {
    if(this.headerRoot) {
      this.headerRoot.appendChild(this.el);
    }
  }

  componentWillUnmount() {
    if(this.headerRoot) {
      this.headerRoot.removeChild(this.el);
    }
  }

  render() {
    return ReactDOM.createPortal(
      [" - ", ...this.props.children],
      this.el,
    );
  }
}