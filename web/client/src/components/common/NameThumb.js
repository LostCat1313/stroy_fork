import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'
import classNames from 'classnames'

const styles = (theme) => ({
  root: {
    width: '36px !important',
    flex: '0 0 36px',
    height: '36px',
    lineHeight: '36px',
    background: '#ddd',
    borderRadius: '100px',
    textAlign: 'center'
  }
})

@withStyles(styles)
export default class NameThumb extends React.Component {
  static propTypes = {
    style:PropTypes.object,
    name:PropTypes.string,
    thumb:PropTypes.string,
    classes:PropTypes.object.isRequired
  }

  getAbbr(name) {
    const parts = name.split(' ')
    return parts.length > 1 ?
      parts[0].substr(0, 1) + parts[1].substr(0, 1) :
      parts[0].substr(0, 2) 
  }

  render() {
    const { thumb, name, style, classes, className } = this.props
    if(thumb) {
      return <div className={classes.root} style={style}>
        <img src={thumb}/>
      </div>
    }

    return <div className={classNames(classes.root, className)} style={style}>
      {this.getAbbr(name || '...').toUpperCase()}
    </div>
  }
}