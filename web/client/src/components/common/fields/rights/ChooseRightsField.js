import React from 'react'

import ChooseItemsField from 'components/common/fields/ChooseItemsField'
import { withStore } from 'decorators'
import { Loading } from 'components'
import RightListItem from './RightListItem'
import ChoosenRightsRenderer from './ChoosenRightsRenderer'

@withStore('Right')
export default class ChooseRightsField extends React.Component {
  valuesToItems(values, items) {
    return items.filter(i => values.indexOf(i.id) >= 0)
  }
  render() {
    return <ChooseItemsField
      items={this.props.Right.list}
      valuesToItems={this.valuesToItems}
      itemRenderer={RightListItem}
      choosenItemsRenderer={ChoosenRightsRenderer}
      {...this.props}
    />
  }
}