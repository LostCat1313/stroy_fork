import React from 'react'


import Utils from 'components/common/Utils'

const ChoosenRightsRenderer = (props) => {
  return <span> {props.items && props.items.length > 0 ?
      props.items.map(i => i.code + ` (${i.id})`).join(', ') :
      'Нет выбранных прав'}
    </span> 
}

export default ChoosenRightsRenderer