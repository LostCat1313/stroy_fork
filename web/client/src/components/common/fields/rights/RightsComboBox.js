import React from 'react';

import ComboBox from '../ComboBox';
import { Utils } from 'components';

const rights = Utils.rights;

const RightsComboBox = (props) => (
  <ComboBox
    {...props}
    items={rights}>
  </ComboBox>
);

export default RightsComboBox;