import React from 'react'
import { Checkbox } from '@material-ui/core'

const RightListItem = (props) => {
  const {item} = props
  return <div
    style={{float: 'left', width: '50%'}}>
    <div className="chat-dialog-contact">
      <Checkbox 
          style={{paddingTop: '12px', paddingBottom: '12px'}}
          label={item.code}
          disabled={props.disabled}
          checked={props.checked}
          onCheck={props.onCheck}/>
    </div>
  </div>
}

export default RightListItem
