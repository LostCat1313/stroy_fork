import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import { TextField as MTextField, withStyles } from '@material-ui/core'
import { withFormsy } from 'formsy-react'

import { Utils } from 'components'

const styles = (theme) =>({
  textField: {
    margin:theme.spacing.unit
  },
  centered: {
    textAlign: 'center'
  }
})

@withStyles(styles)
@withFormsy
export default class TextField extends React.Component {
  static propTypes = {
    model:PropTypes.object,
    ...MTextField.propTypes
  }

  constructor(props) {
    super(props)
    const { model, name, defaultValue, setValue } = this.props
    setValue(model ? model[name] : defaultValue)
  }

  changeHandler = (e) => {
    this.props.setValue(e.target.value)
  }
  
  setValue(value) {
    this.props.setValue(value)
  }

  render() {
    const { centered, classes } = this.props;
    let value = this.props.getValue()
    if(this.props.isJSON) {
      value = JSON.stringify(value)
    }
    const error = this.props.showError()
    const errorMessage = this.props.getErrorMessage()
    return <MTextField
      fullWidth
      InputProps={{
        classes: {input:centered && classes.centered}
      }}
      name={this.props.name}
      error={error}
      helperText={errorMessage}
      required={this.props.required}
      label={this.props.label}
      className={classNames(this.props.classes.textField, this.props.className)}
      onChange={this.changeHandler}
      value={value || ''}
    />
  }
}
