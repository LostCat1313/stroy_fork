import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core';
import { withFormsy } from 'formsy-react';

import PhoneNumberBaseField from './phone/PhoneNumberBaseField';

const styles = (theme) =>({
  textField: {
    margin:theme.spacing.unit
  },
  centered: {
    textAlign: 'center'
  }
});

@withStyles(styles)
@withFormsy
export default class TextField extends React.Component {
  static propTypes = {
    model:PropTypes.object,
    ...PhoneNumberBaseField.propTypes
  }

  constructor(props) {
    super(props);
    const { model, name, defaultValue, setValue } = this.props;
    setValue(model ? model[name] : defaultValue);
  }

  changeHandler = (value) => {
    this.props.setValue(value);
  }
  
  setValue(value) {
    this.props.setValue(value);
  }

  render() {
    let value = this.props.getValue();
    if(this.props.isJSON) {
      value = JSON.stringify(value);
    }
    const { centered, classes } = this.props;
    const error = this.props.showError();
    const errorMessage = this.props.getErrorMessage();
    return <PhoneNumberBaseField
      fullWidth
      disableDropdown
      InputProps={{
        classes: {input:centered && classes.centered}
      }}
      defaultCountry="ru"
      countryCodeEditable={false}
      name={this.props.name}
      error={error}
      helperText={errorMessage}
      required={this.props.required}
      label={this.props.label}
      className={classNames(this.props.classes.textField, this.props.className)}
      onChange={this.changeHandler}
      value={value || ''}
    />;
  }
}
