import React from 'react'

import { withStore } from 'decorators'
import { ComboBox } from 'components'

@withStore('Organization')
export default class OrganizationComboBox extends React.Component {
  valuesToItems(values, items) {
    return items.filter(i => values.indexOf(i.id) >= 0)
  }
  render() {
    return <ComboBox
      dataSource={this.props.Organization.list}
      itemMap={(d) => ({ value: d.id, text: d.name })}
      {...this.props}
    />
  }
}

