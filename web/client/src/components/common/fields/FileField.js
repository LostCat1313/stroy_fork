import React from 'react';
import { withStyles } from '@material-ui/core';
import { withFormsy } from 'formsy-react';
import classNames from 'classnames';

import { withStore } from 'decorators'

@withStore('Session', { preload: false })
@withStyles(theme => ({
  root: {
    position: 'relative',
    border: '2px dashed #ddd',
    padding: '20px 10px',
    textAlign: 'center',
    margin: '0 auto 20px',
    maxWidth: '300px' // todo fix
  },
  container: {
    display: 'flex',
    flexDirection: 'column'
  },
  span: {
    color: '#ddd',
    textTransform: 'uppercase'
  },
  input: {
    bottom: 0,
    cursor: 'pointer',
    left: 0,
    opacity: 0,
    position: 'absolute',
    right: 0,
    top: 0,
    width: '100%',
    height: '100%',
    zIndex: 1
  }
}))
@withFormsy
export default class FileField extends React.Component {

  constructor(props) {
    super(props)
    this.state =  {
      loading: false,
      error: null
    }
    const { model, name, value } = this.props
    this.props.setValue(model ? model[name] : value)
  }

  onInputChange = (e) => {
    var data = new FormData()
    data.append('files[]', this.refs.fileInput.files[0])
    this.setState({loading: true})
    // validate file type?
    fetch(this.props.url ? this.props.url : '/api/files/upload', {
      method: 'POST',
      // TODO: move to cookie!!
      headers: { 'Authorization': 'Bearer ' + this.props.Session.token },
      body: data
    }).then((response) => {
      this.setState({loading: false})
      if (response.status == 200) {
        return response.json()
      }
      else {
        // TODO: error to snackbar
        this.setState({
          loading: false,
          error: response.statusText
        })
        this.props.setValue(false)
      }
    }, (error) => {
      this.setState({
        loading: false,
        error: error
      })
      this.props.setValue(false)
    }).then(data => {
      if(this.props.onUpload) {
        this.props.onUpload(this.prepareOnUploadData(data))
      }
      this.props.setValue(data.files ? (this.props.multiple ? data.files : data.files[0]) : data)
    })
  }

  prepareOnUploadData(data) {
    if(this.props.prepareOnUploadData) return this.props.prepareOnUploadData(data)
    return data.files
  }

  render() {
    const { accept, label, uploadOnly, getValue, classes, className } = this.props
    const { loading, error } = this.state

    let files = getValue()
    if(typeof files != 'array' && files) files = [files]
    if(!files) files = []
    return <div className={classNames(classes.root, className)}>
      { loading && <span className={classes.span}>Загрузка...</span> }
      { error && <span className={classes.span}>{error}</span> }
      { !loading && !error && (
        <div>
          { files.length > 0 && !uploadOnly && <div className={classes.container}>
            <span className={classes.span}>{files[0].original}</span>
            <img src={files[0].url} style={{maxWidth:"150px"}}/>
          </div> }
          <input 
            ref="fileInput"
            className={classes.input} 
            type="file" 
            onChange={this.onInputChange}
            accept={accept}/>
          <span className={classes.span}>{label}</span>
        </div> 
      )}
    </div>
  }
}

