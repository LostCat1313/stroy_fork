import React, { Component, Fragment } from 'react';
import { GridList, GridListTile, GridListTileBar, Button, IconButton, withStyles } from '@material-ui/core';
import classNames from 'classnames';
import { withStore } from 'decorators'
import PageNumberField from './PageNumberField';
import { Cancel } from '@material-ui/icons';0

@withStore('Session', { preload: false })
@withStyles(theme => ({
  root: {
    position: 'relative',
    border: '2px dashed #ddd',
    padding: '20px 10px',
    textAlign: 'center',
    margin: '0 auto 20px',
    minWidth: '300px'
  },
  container: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  label: {
    color: '#ddd',
    textTransform: 'uppercase'
  },
  input: {
    bottom: 0,
    cursor: 'pointer',
    left: 0,
    opacity: 0,
    position: 'absolute',
    right: 0,
    top: 0,
    width: '100%',
    height: '100%',
    zIndex: 1
  },
  imgContainer: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: 150,
    height: 150
  },
  cancel: {
    position: 'absolute',
    top: 0,
    right: 0,
    zIndex: 2
  },  
  img: {
    maxWidth: '100%',
    maxHeight: '100%',
  },
  gridList: {
    maxHeight: '100%',
    position: 'relative',
    zIndex: 2
  },
  button: {
    position: 'relative',
    zIndex: 2,
    marginTop: 20
  }
}))
// @withFormsy
export default class PagesField extends Component {
  fileInputRef = React.createRef();
  state = {
    files: []
  }

  onInputChange = (e) => {
    const input = e.target;

    console.log(input.files);

    if (input.files) {
      
      for(let i = 0; i < input.files.length; i++) {
        const f = input.files[i];
        var reader = new FileReader();
        reader.onload = (e) => {
          this.setState(prevState => ({
            files: [...prevState.files, {
              name: f.name,
              src: e.target.result,
              number: '1',
              file: f
            }]
          }));
        }
        reader.readAsDataURL(f);
      }
    }
  }


  readURL(file) {

   
  }

  submit() {
    
    const { url, Session, onUpload } = this.props;
    
    var data = new FormData();

    this.state.files.forEach((f, index) => {
      data.append(`file_${index}`, f.file);
      data.append(`file_${index}_pageNumber`, f.number);
    });

    this.setState({loading: true})
    // validate file type?
    fetch(url ? url : '/api/files/upload', {
      method: 'POST',
      // TODO: move to cookie!!
      headers: { 'Authorization': 'Bearer ' + Session.token },
      body: data
    }).then((response) => {
      this.setState({loading: false})
      if (response.status == 200) {
        return response.json()
      }
      else {
        // TODO: error to snackbar
        this.setState({
          loading: false,
          error: response.statusText
        })
        // this.props.setValue(false)
      }
    }, (error) => {
      this.setState({
        loading: false,
        error: error
      })
      // this.props.setValue(false)
    }).then(data => {
      if(onUpload) {
        onUpload(this.prepareOnUploadData(data))
      }
    })
  }


  prepareOnUploadData(data) {
    if(this.props.prepareOnUploadData) return this.props.prepareOnUploadData(data)
    return data.files
  }

  handleSave = () => {
    this.submit();
  }

  handlePageNumberChange = (index) => (value) => {
    this.setState(prevState => {
      prevState.files[index].number = value
      return {
        files: prevState.files
      }
    })
  }

  handleRemove = (index) => () => {
    this.setState(prevState => {
      prevState.files.splice(index, 1);
      return {
        files: prevState.files
      }
    });
  }

  render() {
    const { accept, label, classes, className } = this.props
    const { loading, error, files } = this.state

    // let files = getValue()
    // if(typeof files != 'array' && files) files = [files]
    // if(!files) files = []


    return <div className={classNames(classes.root, className)}>
      { loading && <div className={classes.label}>Загрузка...</div> }
      { error && <div className={classes.label}>{error}</div> }
      { !loading && !error && (
        <div>
          { files.length > 0 && <Fragment>
              <GridList className={classes.gridList} cols={3} cellHeight={200}> 
                {files.map((f,index) => <GridListTile key={index + f.name} className={classes.tile} classes={{tile:classes.container}}>
                  <IconButton color="primary"
                    className={classes.cancel}
                    onClick={this.handleRemove(index)}
                  ><Cancel/></IconButton>
                  <img src={f.src}/>
                  <GridListTileBar
                    title={f.name}
                    actionIcon={
                      <PageNumberField
                        value={f.number}
                        onChange={this.handlePageNumberChange(index)}
                      />
                    }
                  />
                </GridListTile>)}
              </GridList>
            </Fragment>
          }
          <input 
            ref={this.fileInputRef} 
            className={classes.input} 
            type="file" 
            accept="image/*"
            multiple="multiple"
            onChange={this.onInputChange}
            accept={accept}/>
          <div className={classes.label}>{label}</div>
        </div> 

      )}
      { files.length > 0 && <Button className={classes.button} variant="contained" color="primary" onClick={this.handleSave}>Сохранить</Button> }
    </div>
  }
}