import React from 'react'


import Utils from 'components/common/Utils'

const ChoosenUsersRenderer = (props) => {
  return <span> {props.items && props.items.length > 0 ?
      props.items.map(i => Utils.renderUserName(i) + ` (${i.id})`).join(', ') :
      'Нет выбранных пользователей'}
    </span> 
}

export default ChoosenUsersRenderer