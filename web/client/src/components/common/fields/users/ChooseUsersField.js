import React from 'react'

import ChooseItemsField from 'components/common/fields/ChooseItemsField'
import { withStore } from 'decorators'
import UserListItem from './UserListItem'
import ChoosenUsersRenderer from './ChoosenUsersRenderer'

@withStore('User', { preloadIfNull: true })
export default class ChooseUsersField extends React.Component {
  valuesToItems(values, items) {
    return items.filter(i => values.indexOf(i.id) >= 0)
  }
  render() {
    return <ChooseItemsField
      items={this.props.User.list}
      valuesToItems={this.valuesToItems}
      itemRenderer={UserListItem}
      choosenItemsRenderer={ChoosenUsersRenderer}
      {...this.props}
    />
  }
}