import React, { Fragment } from 'react'
import { Checkbox, FormControlLabel, withStyles } from '@material-ui/core'

import NameThumb from 'components/common/NameThumb'
import Utils from 'components/common/Utils'

const styles = (theme) => ({
  root: {
    // flex: '0.5'
  },
  nameThumb: {
    display: 'inline-block',
    marginRight: theme.spacing.unit
  }
})

@withStyles(styles)
export default class UserListItem extends React.Component {
  render() {
    const {item, classes, disabled, onChange, checked} = this.props
    const name = Utils.renderUserName(item)
    return <div className={classes.root}>
        <FormControlLabel
          className={classes.field}
          control={
            <Checkbox
              checked={checked}
              disabled={disabled}
              onChange={onChange}/>
          }
          label={
            <Fragment> 
              <NameThumb
                thumb={item.thumb}
                name={name}
                className={classes.nameThumb}/>
              {name}
            </Fragment>
          }/>
      </div>
  }
}
