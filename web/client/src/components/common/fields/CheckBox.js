import React from 'react'
import { Checkbox as MCheckbox, FormControlLabel, withStyles } from '@material-ui/core'
import { withFormsy } from 'formsy-react'

const styles = (theme) =>({
  field: {
    margin:theme.spacing.unit,
    width:200,
  },
})

@withStyles(styles)
@withFormsy
export default class CheckBox extends React.Component {
  constructor(props) {
    super(props)
    const { model, name, defaultValue, setValue } = this.props
    const value = model ? model[name] : defaultValue
    setValue(value)
  }

  handleChange = (e) => {
    this.props.setValue(e.target.checked);
    console.log('test')
  }

  render() {
    const { label, classes } = this.props
    const value = this.props.getValue()
    const disabled = this.props.isFormDisabled();
    const control = <MCheckbox
        checked={!!value}
        disabled={disabled}
        onChange={this.handleChange}
        name={this.props.name}/>;
    return label ? <FormControlLabel
      className={classes.field}
      control={control}
      label={label}
    /> : control;
  }
}
