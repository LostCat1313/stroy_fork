import React, { Component } from 'react';
import classNames from 'classnames';
import { withFormsy } from 'formsy-react';
import { Typography, TextField, IconButton, withStyles, MuiThemeProvider, createMuiTheme } from '@material-ui/core';

import grey from '@material-ui/core/colors/grey';

const theme = createMuiTheme({
  palette: {
    type: 'dark',
    primary: grey,
    common: {
      "white": "#000",
      "black": "#fff"
    },
  },
  typography: { useNextVariants: true },
});

@withStyles(theme =>({
  root: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column'
  },
  subRoot: {
    display: 'flex',
    alignItems: 'center',
  },
  control: {
    width: 20,
    height: 20,
    fontSize: '20px',
    textAlign: 'center',
    lineHeight: '20px'
  },
  bigControl: {

  },
  textfield: {
    width: 30,
  },
  textfieldInput: {
    textAlign: 'center',
  }
}))
// @withFormsy
class PageNumberField extends Component {

  static defaultProps = {
    defaultValue: '1',
    blurOnly: true
  }

  constructor(props) {
    super(props);
    
    if(!props.value) {
      this.props.onChange && this.props.onChange('1');
    }

    this.state = {
      minusEnabled: false,
      pluseEnabled: true
    }
  }

  handleKeyPress = (event) => {
    const keyCode = event.keyCode || event.which;
    const keyValue = String.fromCharCode(keyCode);
    if (!/[0-9]/.test(keyValue)) {
      event.preventDefault();
    }
    else if(this.props.getValue() == '1' && keyValue != '1' && keyValue != '0'){
      this.props.onChange && this.props.onChange(value + '');
      event.preventDefault();
    }
  }
  
  handleChange = (e) => {
    let value = parseInt(e.target.value) || 1;
    if(value > 99) value = 99;
    this.props.onChange && this.props.onChange(value + '');
  }

  handleStepChange = (amount) => (e) => {
    let value = parseInt(this.props.value) + amount;
    if(value > 99) value = 99;    
    if(value >= 1) {
      this.props.onChange && this.props.onChange(value + '');
    }
  }
  
  render() { 
    const { classes, className, value} = this.props;

    const plus = classNames({
      [classes.control]: true,
      [classes.controlEnabled]: true,
      [classes.bigControl]: true,
    });

    const minus = classNames({
      [classes.control]: true,
      [classes.controlEnabled]: true,
      [classes.bigControl]: true,
    });

    return <div 
      title="Номер страницы"
      className={classNames(classes.root, className)}>
      <div className={classes.subRoot}>
        <MuiThemeProvider theme={theme}>
          <IconButton onClick={this.handleStepChange(-1)}><div className={minus}>-</div></IconButton>
          <TextField
            color="primary"
            onBlur={this.handleBlur}
            onKeyPress={this.handleKeyPress}
            className={classes.textfield}
            inputProps={{className: classes.textfieldInput}}
            onChange={this.handleChange}
            value={value}
          />
          <IconButton onClick={this.handleStepChange(1)}><div className={plus}>+</div></IconButton>
        </MuiThemeProvider>
      </div>
    </div>;
  }
}
 
export default PageNumberField;