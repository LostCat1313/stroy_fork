import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { withFormsy } from 'formsy-react'
import { Button, Dialog, DialogTitle, DialogContent, DialogActions, FormLabel, withStyles } from '@material-ui/core'

import { LineBreak, MdiIcon } from 'components'
import ChoosenItems from './list/ChoosenItems'
import ChooseItemsList from './list/ChooseItemsList'

const styles = (theme) => ({
  root: {
    display:'flex',
    flexWrap:'wrap'
  },
  dialog: {
    minWidth:'200px',
    minHeight:'200px'
  },
  content: {
    display:'flex',
    flexWrap:'wrap'
  },
  detailed: {
    width:'400px'
  }
})

@withStyles(styles)
@withFormsy
export default class ChooseItemsField extends React.Component {
  static propTypes = {
    classes:PropTypes.object.isRequired,
    setValue:PropTypes.func.isRequired,
    iconName:PropTypes.string
  }

  constructor(props) {
    super(props)
    const { value, model, name, valuesToItems, items, setValue } = this.props
    let initialValue = (model ? model[name] : value) || []
    if(valuesToItems && items && initialValue) {
      initialValue = valuesToItems(initialValue, items)
    }
    this.state = {
      open: false,
      _value: initialValue.slice(),
      preValue: initialValue.slice()
    }
    setValue(initialValue.slice())
  }
  openDialogHandler = () => {
    this.setState({ open: true })
  }
  okDialogHandler = () => {
    this.props.setValue(this.state.preValue)
    this.setState({ _value: this.state.preValue, open: false })
  }
  cancelDialogHandler = () => {
    this.setState(prevState => ({ preValue: prevState._value, open: false }))
  }
  onCheckListChange = (items) => {
    this.setState({ preValue:items })
  }
  render() {
    const { 
      itemRenderer,
      choosenItemsRenderer,
      items,
      classes,
      label,
      iconName
    } = this.props

    const { open, preValue, _value } = this.state

    return <div className={classes.root}>
        <Dialog
          open={open}>
          <DialogTitle>Выбрать позиции списка</DialogTitle>
          <DialogContent className={classes.content}>
            <ChooseItemsList
              itemRenderer={itemRenderer}
              items={items}
              selected={preValue}
              onChange={this.onCheckListChange}/>
          </DialogContent>
          <DialogActions>
            <Button
              onClick={this.okDialogHandler}>Ок</Button>
            <Button
              onClick={this.cancelDialogHandler}>Отмена</Button>
          </DialogActions>
        </Dialog>
        <LineBreak/>
        {!iconName ? <div className={classes.detailed}>
            <FormLabel component="div">{label}</FormLabel>
            <ChoosenItems
              renderer={choosenItemsRenderer}
              selected={_value}
              />
            <Button
              onClick={this.openDialogHandler}>Выбрать</Button>
          </div> : <Fragment>
            <MdiIcon
              button primary
              onClick={this.openDialogHandler}
              iconName={iconName}
            ></MdiIcon>
            <span>({_value.length})</span>
          </Fragment>
        }
      </div>
  }
}
