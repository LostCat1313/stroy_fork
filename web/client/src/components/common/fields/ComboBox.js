import React from 'react'
import classNames from 'classnames'
import { MenuItem, FormControl, InputLabel, Select, Input, withStyles } from '@material-ui/core'
import { withFormsy } from 'formsy-react'

const styles = (theme) =>({
  field: {
    margin:theme.spacing.unit,
  },
})

@withStyles(styles)
@withFormsy
class ComboBox extends React.Component {
  constructor(props) {
    super(props)
    const { model, name, defaultValue, setValue } = this.props
    const value = model ? model[name] : defaultValue
    setValue(value)
  }

  renderComboItem(i) {
    return <MenuItem key={i.value} value={i.value}>{i.text}</MenuItem>
  }

  handleChange = (e) => {
    this.props.setValue(e.target.value)
  }

  renderItems() {
    const { items, dataSource, itemMap } = this.props
    const variants = items
      ? items.map(this.renderComboItem)
      : dataSource && itemMap 
        ? dataSource.map(itemMap).map(this.renderComboItem) : []
    return [<MenuItem key={-1} value={''} disabled>Не выбрано</MenuItem>, ...variants]
  }

  render() {
    const { label, name, classes, placeholder, className, required } = this.props;
    const value = this.props.getValue() || '';
    const disabled = this.props.isFormDisabled();
    return <FormControl disabled={disabled} required={required} fullWidth className={classNames(classes.field, className)}>
        {label && <InputLabel htmlFor={name + '-helper'}>{label}</InputLabel> }
        <Select
          onChange={this.handleChange}
          value={value}
          input={<Input name={name} id={name + '-helper'} />}>
          {this.renderItems()}
        </Select>
      </FormControl>
  }
}

export default ComboBox
