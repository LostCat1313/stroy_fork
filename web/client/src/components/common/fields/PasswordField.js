import React, {Fragment} from 'react'
import { TextField as MTextField, withStyles } from '@material-ui/core'
import { withFormsy } from 'formsy-react'

const styles = (theme) =>({
  textField: {
    margin:theme.spacing.unit
  },
})

@withStyles(styles)
@withFormsy
export default class PasswordField extends React.Component {

  state = {
    password: ''
  }

  changeHandler = (e) => {
    this.setState({
      password: e.target.value
    })
  }

  changeConfirmationHandler = (e) => {
    if(this.state.password == e.target.value) {
      this.props.setValue(this.state.password)
      this.setState({
        error:false
      })
    }
    else {
      this.setState({
        error:true
      })
    }
  }

  render() {
    const { name, classes, required } = this.props
    return <Fragment>
        <MTextField
          fullWidth
          required={required}
          className={classes.textField}
          name={name}
          type="password"
          label="Пароль"
          onChange={this.changeHandler}
        />
        <MTextField
          fullWidth
          className={classes.textField}
          name={name+'_confirmation'}
          type="password"
          label="Подтверждение"
          error={this.state.error}
          helperText={this.state.error ? "Пароль подтвержден не верно" : ""}
          onChange={this.changeConfirmationHandler}
        />
    </Fragment>
  }
}

 