import React from 'react'

const DefaultItemsRenderer = (props) => {
  return <span> {props.items && props.items.length > 0 ?
      props.items.map(i => `(${i})`).join(', ') :
      'Нет выбранных позиций'}
    </span> 
}

export default DefaultItemsRenderer