import React, { Fragment } from 'react'
import { Checkbox, FormControlLabel, withStyles } from '@material-ui/core'


const styles = (theme) => ({
  item: {
    flex: '0.5 0.5 50%'
  }
})

@withStyles(styles)
export default class ChooseItemsList extends React.Component {

  constructor(props) {
    super(props)
    this.state = { selected:props.selected || [] }
  }

  // propTypes = {
  //   defaultChecked: PropTypes.bool,
  //   name: PropTypes.string.isRequired,
  //   onChange: PropTypes.func,
  //   validationError: PropTypes.string,
  //   validationErrors: PropTypes.object,
  //   validations: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  // }

  onClick(uid) {
    if(this.props.onClick) {
      this.props.onClick(uid)
    }
  }

  handleChange(item, event) {
    const value = event.target.checked
    console.log(item, event, value)
    this.setState(prevState => {
      let selected = prevState.selected.slice()
      let dirty = false
      if(value) {
        if(prevState.selected.indexOf(item) < 0) {
          selected.push(item)
          dirty = true
        }
      }
      else {
        let index = prevState.selected.indexOf(item)
        if(index >= 0) {
          selected.splice(index, 1)
          dirty = true
        }
      }

      if(dirty) {
        this.props.onChange(selected)
      }

      return { ...prevState, selected }
    })
  }

  renderItem(item) {
    const ItemRenderer = this.props.itemRenderer
    return ItemRenderer ? <ItemRenderer
        item={item}
        disabled={this.props.disabled}
        checked={this.state.selected.indexOf(item) >= 0}
        onChange={this.handleChange.bind(this, item)}
      /> : <FormControlLabel
        label={item}
        control={
          <Checkbox
            disabled={this.props.disabled}
            checked={this.state.selected.indexOf(item) >= 0}
            onChange={this.handleChange.bind(this, item)}
          />
        }
      />
      
  }

  render() {
    const { classes, items } = this.props
    if(items && items.length > 0) {
      return <Fragment>
        {items && items.map((item, index) => {
          return <div 
            key={index}
            className={classes.item}>
            { this.renderItem(item) }
          </div>
        })}
      </Fragment>
    }
    else {
      return <div className="not-found">Список пуст</div>
    }
  }
}