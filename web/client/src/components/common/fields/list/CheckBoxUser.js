import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'

import CheckBox from 'components/common/fields/CheckBox'
import NameThumb from 'components/common//NameThumb'
import Utils from 'components/common/Utils'

const styles = (theme) => ({
  root: {
    flex: '0.5',
  }
})

@withStyles(styles)
export default class CheckBoxUser extends React.Component {
  static propTypes = {
    classes:PropTypes.object.isRequired,
    user:PropTypes.object.isRequired,
    disabled:PropTypes.bool,
    entityName:PropTypes.string.isRequired
  }

  render() {
    const { user, disabled, entityName, classes }  = this.props
    const name = Utils.renderUserName(user)
    console.log(name)
    return <div className={classes.root}>
        <NameThumb thumb={user.thumb} name={name}/>
        <Checkbox
          disabled={disabled}
          name={entityName + "." + user.id}
          label={name}
        />
    </div>
  }
}
