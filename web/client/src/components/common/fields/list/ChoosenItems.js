import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'

import DefaultItemsRenderer from './DefaultItemsRenderer'

const styles = (theme) => ({
  root: {
    flex: '1 1',
    minHeight: '36px',
    lineHeight: '1.5',
    padding: '8px 16px'
  }
})


@withStyles(styles)
export default class ChoosenItems extends React.Component {

  static propTypes = {
    classes:PropTypes.object.isRequired,
    selected:PropTypes.array,
    renderer:PropTypes.any // component for rendering
  }

  renderItems() {
    const { selected, renderer } = this.props
    const ItemsRenderer = renderer

    return ItemsRenderer 
      ? <ItemsRenderer items={selected}/> 
      : <DefaultItemsRenderer items={selected}/>
  }

  render() {
    const { classes } = this.props
    return <div className={classes.root}>
        {this.renderItems()}
      </div>
  }
}
