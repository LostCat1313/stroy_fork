import React from 'react'

import { withStore } from 'decorators'
import { ComboBox } from 'components'

@withStore('Position')
export default class PositionComboBox extends React.Component {
  valuesToItems(values, items) {
    return items.filter(i => values.indexOf(i.id) >= 0)
  }
  render() {
    return <ComboBox
      dataSource={this.props.Position.list}
      itemMap={(d) => ({ value: d.id, text: d.name })}
      {...this.props}
    />
  }
}

