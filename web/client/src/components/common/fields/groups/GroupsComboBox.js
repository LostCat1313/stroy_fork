import React from 'react'

import { withStore } from 'decorators'
import { Loading, ComboBox } from 'components'

@withStore('Group')
export default class GroupComboBox extends React.Component {
  valuesToItems(values, items) {
    return items.filter(i => values.indexOf(i.id) >= 0)
  }
  render() {
    return <ComboBox
      dataSource={this.props.Group.list}
      itemMap={(d) => ({ value: d.id, text: d.name })}
      {...this.props}
    />
  }
}

