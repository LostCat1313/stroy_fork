import React from 'react'
import AccountIcon from 'mdi-react/AccountIcon'
import PencilIcon from 'mdi-react/PencilIcon'
import CloseCircleIcon from 'mdi-react/CloseCircleIcon'
import InformationIcon from 'mdi-react/InformationIcon'
import FolderOpenIcon from 'mdi-react/FolderOpenIcon'
import { Link } from 'react-router-dom'

import {
  Table,
  TableBody,
  TableFooter,
  TableHead,
  TableCell,
  TableRow,
  Tooltip,
  withStyles
} from '@material-ui/core'

const columnStyles = {
  id: {
    width: '40px',
    textAlign: 'center'
  },
  actions: {
    width: '120px',
    textAlign: 'center'
  },
  _typeDefault: {},
  _rowDefault: {
    paddingLeft: '10px',
    paddingRight: '10px'
  },
  _headerDefault: {
    paddingLeft: '10px',
    paddingRight: '10px'
  }
}

const actionIcons = {
  show: <InformationIcon width={20} height={20} className="edit-icon"/>,
  edit: <PencilIcon  width={20} height={20} className="edit-icon"/>,
  remove: <CloseCircleIcon  width={20} height={20} className="edit-icon"/>,
  folders: <FolderOpenIcon  width={20} height={20} className="edit-icon"/>,
  users: <AccountIcon  width={20} height={20} className="edit-icon"/>
}

@withStyles(theme => ({
  root: {
    
  }
}))
class AppTable extends React.Component {

  constructor(props) {
    super(props)
    const list = props.dataSource ? props.dataSource.list : props.dataList
    this.state = { list }
  }

  componentWillReceiveProps(nextProps) {
    const list = nextProps.dataSource ? nextProps.dataSource.list : nextProps.dataList
    if (list) {
      this.setState({ list })
    }
  }

  renderRow(row, entry, subindex, index) {
    const style = {
      ...columnStyles['_rowDefault'],
      ...columnStyles[row.type ? row.type : '_typeDefault']
    }
    if (row) {
      if (typeof row === "function") {
        return <TableCell
          key={subindex}
          style={style}>{row(entry, index)}</TableCell>
      } else if (typeof row === "object") {
        return <TableCell
          key={subindex}
          style={{
            ...style,
            ...row.style
          }}>
          {this.renderObjectRow(row, entry, subindex, index)}
        </TableCell>
      } else {
        return row
      }
    }
  }

  renderObjectRow(row, entry, subindex, index) {
    switch (row.type) {
      case 'actions':
        return this.renderActionsRowColumn(row, entry)
      case 'id':
        return entry.id
      default:
        if (row.render && typeof row.render === "function")
          return row.render(entry, index)
        throw new Error('Bad object')
    }
  }

  renderActionsRowColumn(row, entry) {
    if(row.actions) {
      let result = []
      for(let key in row.actions) {
        const action = row.actions[key]
        const rendered = action(entry)
        const icon = actionIcons[key] ? actionIcons[key] : key
        switch(typeof rendered) {
          case "object":
            result.push(this.renderAction({
              ...rendered,
              icon,
              key
            }))
            break
          case "string":
            result.push(this.renderAction({uri: rendered, icon, key}))
            break
          default:
            console.warn("Wrong rendered action object", rendered)
            break
        }
      }
      return result
    }
    throw new Error('"actions" field isn\'t defined in row', row)
  }

  renderAction(params) {
    if(params.onClick) {
      return <a key={params.key} href="#" onClick={(e) => {
          e.preventDefault()
          if(params.key == 'remove' && confirm('Подтвердите действие') || params.key != 'remove') {
            params.onClick()
          }
        }}>{params.icon}</a>
    }
    return <Link key={params.key} to={params.uri}>{params.icon}</Link>
  }

  renderHeaderCell(h, index) {
    const style = {
      ...columnStyles['_headerDefault'],
      ...columnStyles[h.type ? h.type : '_typeDefault']
    }
    switch (typeof h) {
      case "string":
        return <TableCell
          key={index}
          root={style}>
          {h}
        </TableCell>
      case "object":
        const type = h.type ? h.type : 'default' // TODO: style is undefined?
        const text = h.text
          ? h.text
          : type.toUpperCase()
        return <TableCell
          key={index}
          style={{
            ...style,
            ...h.style
          }}>
          {text}
        </TableCell>
      case "function":
        return h()
      default:
        throw new Error("Header type is invalid")
    }
  }

  renderHead() {
    const { headerColumns } = this.props;
    return <TableHead>
      <TableRow>
        {headerColumns.map((h, index) => this.renderHeaderCell(h, index))}
      </TableRow>
    </TableHead>
  }


  renderBody() {
    const { rowColumns, showCheckboxes } = this.props
    const { list } = this.state
    return <TableBody>
            {list.map((entry, index) => <TableRow
              key={entry.id}>
              {rowColumns.map((row, subindex) => this.renderRow(row, entry, subindex, index))}
            </TableRow>)}
        </TableBody>
  }

  
  onRowSelectionHandler(rows) {
    const { showCheckboxes } = this.props
    if (showCheckboxes) {
      this.setState(prevState => {
        if (rows === 'all') {
          var newState = {
            list: prevState.list.map(element => {
              element.selected = true
              return element
            })
          }
        } 
        else if (rows) {
          var newState = {
            list: prevState.list.map((element, index) => {
              element.selected = rows.indexOf(index) >= 0
              return element
            })
          }
        }
        this.props.idsChanged && this.props.idsChanged(
          newState.list.filter(i => i.selected).map(i => i.id)
        )
      })
    }
  }

  render() {
    const { showCheckboxes } = this.props
    const { list } = this.state
    switch(true) {
      case list && list.length > 0:
        return <Table>
          {this.renderHead()}
          {this.renderBody()}
        </Table>
      default: 
        return <div>
          Нет данных для отображения
        </div>
    }
  }
}

export default AppTable
