import React, { Component } from 'react';
import { Card, CardHeader, withStyles } from '@material-ui/core';
import MessageIcon from 'mdi-react/MessageIcon';
import FileIcon from 'mdi-react/FileIcon';

@withStyles(theme => ({
  file: {

  },
  message: {

  },
  remark: {

  },
  project: {

  }
}))
class NotificationCard extends Component {
  state = {  }

  renderAvatar(type) {
    switch(type) {
      case 201:
        return <MessageIcon style={{fill:this.resolveColor(type)}}/>
      case 101:
      case 107:
      case 108:
      case 109:
        return <FileIcon style={{fill:this.resolveColor(type)}}/>
      default:
        return
    }
  }
  resolveColor(type) {
    switch(type) {
      case 201:
        return '#4CAF50'
      case 101:
      case 107:
      case 108:
      case 109:
        return '#FF9800'
      case 105:
        return '#8BC34A'
      default:
        return
    }
  }

  render() {
    const { item } = this.props;
    return ( <Card
      style={{
        borderTop: '4px solid',
        borderTopColor: this.resolveColor(item.type)
      }}>
        <CardHeader
          avatar={this.renderAvatar(item.type)}
          title={item.title}
          subheader={item.content}
        />
      </Card> );
  }
}
 
export default NotificationCard;