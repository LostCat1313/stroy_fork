import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'

import BaseComponent from './BaseComponent'
import { AppForm, Header } from 'components'

class EditComponent extends BaseComponent {
  
  constructor(props, entity) {
    super(props);
    var store = entity ? this.props[entity] : this.props.editStore;
    if(!store) throw 'Store isn\'t defined for ' + entity;
    this.store = store;
    this.entity = entity;
    this.state = { _entry: this.getCurrentEntry(store.list) };
    this.updateAction = this.getAction('update');
    this.createAction = this.getAction('create');
  }

  componentWillMount() {
    if(this.props.backUri && this.props.match.params.id && !this.state._entry) {
      console.log("====EDIT LOAD FAILED: going to " + this.backUri + "====")      
      this.props.history.replace(this.backUri)
    }
  }

  getAction(type) {
    return this.props.actions[type + this.entity] 
      ? this.props.actions[type + this.entity]
      : () => {
        console.log(this.props.actions)
        throw `Must return ${type} action`
      }
  }

  getEntry() {
    return this.state._entry
  }

  requestHandler(payload) {
    if(payload.status === "success") {
      console.log("====EDIT SUCCESS====")
      if(this.backUri && this.props.history) {
        if(this.goBack) this.props.history.goBack()
        else this.props.history.push(this.backUri)
      }
    } else {
      console.log(payload)
    }
  }

  errorHandler(error) {
    console.log(error)
  }

  submitForm = (data) => {
    let prepared = this.prepareData(data)
    console.log(prepared)
    if(this.state._entry) {
      this.updateAction(this.state._entry.id, prepared)
        .then(this.requestHandler.bind(this), this.errorHandler)
    }
    else {
      this.createAction(prepared)
        .then(this.requestHandler.bind(this), this.errorHandler)
    }
  }

  prepareData(data) {
    return data
  }
  
  renderHeader() {
    throw "Must be overriden"
  }

  renderForm() {
    return (
      <AppForm
        column
        model={this.state._entry}
        onValidSubmit={this.submitForm}>
        {this.addKeys(this.renderFields())}
      </AppForm>
    )
  }


  renderFields() {
    throw "Must be overriden"
  }

  render() {
    return (
      <div className="container">
        <Header>{this.renderHeader()}</Header>
        {this.renderForm()}
      </div>
    )
  }
}

export default EditComponent