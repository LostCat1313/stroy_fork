import React from 'react'
import MdiIcon from './MdiIcon'

export default class MenuIcon extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    return <MdiIcon {...this.props} style={{fill:'#6f6f6f', ...this.props.style}}/>
  }
}