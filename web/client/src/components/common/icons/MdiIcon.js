import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'
import classNames from 'classnames'

const styles = theme => ({
  primary: {
    '& path': {
      fill: theme.palette.primary.main
    }
  },
  button: {
    cursor: 'pointer'
  }
})

@withStyles(styles)
export default class MdiIcon extends React.Component {

  static propTypes = {
    classes:PropTypes.object.isRequired,
    iconName:PropTypes.string.isRequired,
    onClick:PropTypes.func,
    className:PropTypes.string,
    primary:PropTypes.bool
  }
  constructor(props) {
    super(props)
    this.mdiIconImported = require('mdi-react/' + props.iconName).default
  }

  render() {
    const MdiIcon = this.mdiIconImported
    const { classes, className, style, onClick, primary, button } = this.props
    return <MdiIcon
      className={classNames(primary && classes.primary, button && classes.button, className)}
      onClick={onClick}
      style={style}/>
  }
}