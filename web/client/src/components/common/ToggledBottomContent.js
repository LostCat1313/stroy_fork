import React, { Component } from 'react';
import { Typography, IconButton, Collapse, withStyles } from '@material-ui/core';
import withWidth, { isWidthUp } from '@material-ui/core/withWidth';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import classNames from 'classnames';

@withStyles(theme => ({
  root: {
    overflow: 'hidden',
    display: 'flex',
    flexDirection: 'column'
  },
  button: {
    alignSelf: 'center',
  },
  icon: {
    transition: 'transform 150ms cubic-bezier(0.4, 0, 0.2, 1) 0ms'
  },
  expanded: {
    transform: 'rotate(180deg)'
  },
  collapse: {
  }
}))
@withWidth()
export default class ToggledBottomContent extends Component {
  state = {
    expanded: false,
    showMore: false,
    collapsedHeight: null
  };

  contentRef = React.createRef();

  static defaultProps = {
    maxLines: 15
  };

  componentDidMount() {
    // console.log('ToggledBottomContent::componentDidMount', this.contentRef);
    const { lines, collapsedHeight } = this.props;
    if(this.contentRef.current) {
      setTimeout(() => {
        console.log(this.contentRef.current.offsetHeight, collapsedHeight, this.contentRef.current.offsetHeight > parseInt(collapsedHeight));
        const showMore = this.contentRef.current.offsetHeight > (lines ? lines * 24 : parseInt(collapsedHeight));
        this.setState({ 
          showMore,
          collapsedHeight: showMore 
            ? lines 
              ? lines * 1.5 + 'rem'
              : collapsedHeight
            : (this.contentRef.current.offsetHeight + 'px')
        });
      });
    }
  }
  
  handleClick = () => {
    this.setState(prevState => ({expanded: !prevState.expanded}));
  }

  render() {
    const { classes, children, lines, className, width } = this.props;
    const { expanded, showMore, collapsedHeight } = this.state;
    if(isWidthUp('sm', width)) {
      return <div className={classes.root}>
        <Collapse className={classes.collapse} in={expanded} collapsedHeight={collapsedHeight}>
          <div 
            ref={this.contentRef}
            className={className}
            style={{lineHeight: lines && '1.5rem'}}>
            {children}
          </div>
        </Collapse>
        {showMore && <IconButton className={classes.button} onClick={this.handleClick}>
          <ExpandMoreIcon className={classNames(classes.icon, expanded ? classes.expanded : null)}/>
        </IconButton>}
      </div>;
    }
    return children;
  }
}
