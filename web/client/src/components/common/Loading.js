import React from 'react';

import { CircularProgress } from '@material-ui/core';

const coverStyle = {
  position: 'absolute',
  top: 0,
  left: 0,
  right: 0,
  bottom: 0,
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  zIndex: 1000,
  background: 'rgba(255,255,255,0.8)'
};

const Loading = props => {
  if(props.center)
    return <div className={props.className} style={{display:'flex', alignItems:'center', justifyContent: 'center', height: '100%', maxHeight: props.maxHeight, marginBottom: props.marginBottom}}>
        <CircularProgress color={props.color} classes={{root:props.rootClassName}} size={props.size}/>
      </div>;
  if(props.cover)
    return <div className={props.className} style={coverStyle}><CircularProgress color={props.color} classes={{root:props.rootClassName}} size={props.size}/></div>
  return <CircularProgress color={props.color} classes={{root:props.rootClassName}} size={props.size}/>
}

export default Loading;