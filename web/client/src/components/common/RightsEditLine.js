import React from 'react'
import PropTypes from 'prop-types'
import Formsy from 'formsy-react'
import { withStyles, Button } from '@material-ui/core'

import { MdiIcon, ComboBox, ChooseUsersField } from 'components'
import Utils from 'components/common/Utils'
import { withStore } from 'decorators'

const styles = theme => ({
  root: {
    display:'flex',
    alignItems:'flex-end'
  },
  field: {
    width:'150px'
  }
});

@withStyles(styles)
@withStore([
  {name:'Session', options: {preload:false}},
  {name:'User', options: {preloadIfNull:true}}
])
export default class RightsEditLine extends React.Component {
  static propTypes = {
    model:PropTypes.object.isRequired,
    entity:PropTypes.string.isRequired,
    onRemove:PropTypes.func
  };

  formsy = React.createRef();
  
  clickHandler = () => {
    if(this.props.onRemove) {
      this.props.onRemove();
    };
  }

  getOperations() {
    const { Session, entity } = this.props;
    const operations = Session.config.rights.operations[entity];
    return operations.map(o => ({text: o, value: o}));
  }

  getUsers() {
    const { User } = this.props;
    return User.list.map(u => ({text: Utils.renderUserName(u), value: u.id}));
  }

  render() {
    const { open, classes, model} = this.props;

    return <Formsy ref={this.formsy}>
        <div className={classes.root}>
          {/* <span>{model.key}</span> */}
          <ComboBox 
            name="sign"
            model={model}
            label="Значение" 
            className={classes.field}
            items= {[
              { value: '1', text: 'Разрешить' },
              { value: '0', text: 'Запретить' }
            ]}/>
          {/*<ComboBox 
            className={classes.smallField}
            name="entity" label="Сущность" items= {[
              { value: 'Document', text: 'Документ' },
              { value: 'Chat', text: 'Чат' }
            ]}/> */}
          <ComboBox 
            name="operation"
            model={model}
            label="Операция"
            className={classes.field}
            items={this.getOperations()}/>
          <ComboBox
            name="userId"
            model={model}
            className={classes.field}
            label="Пользователь"
            items={this.getUsers()}/>
          <MdiIcon 
            primary
            button
            onClick={this.clickHandler}
            iconName="CloseCircleIcon"></MdiIcon>
        </div>
      </Formsy>;
  }
}