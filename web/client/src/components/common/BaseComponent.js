import React from 'react'
import queryString from 'query-string'

export default class BaseComponent extends React.Component {
  getCurrentEntry(list, id) {
    id = id || this.props.match.params.id
    var current = list && this.props.match
      ? list.find(u => u.id == id) 
      : null
    return current ? {...current} : null
  }

  getCurrentEntryDB(db) {
    var current = db && this.props.match
      ? db[+this.props.match.params.id]
      : null
    return current ? {...current} : null
  }

  fire(name, arg) {
    if(this.props[name] && typeof this.props[name] === 'function') {
      this.props[name](arg)
    }
  }

  extractQueryParam(key, nextProps) {
    let props = nextProps || this.props
    const parsed = queryString.parse(props.location.search)
    return parsed[key]
  }

  addKeys(arr) {
    return arr && arr.map((obj, idx) => {
      if (obj instanceof Object && obj.hasOwnProperty('key') && obj.key === null) {
        return React.cloneElement(obj, {key: idx})
      }
      return obj
    })
  }
}