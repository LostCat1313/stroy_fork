import React from 'react'

const LineBreak = (props) => {
  return <div style={{width:'100%'}}></div>
}

export default LineBreak