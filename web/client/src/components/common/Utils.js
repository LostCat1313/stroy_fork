export default class Utils {

  static rights = [
    { text: 'Чтение', value: 0b001 },
    { text: 'Чтение и запись', value: 0b011 },
    { text: 'Управление', value: 0b111 },
  ];

  static renderRight(value) {
    const found = this.rights.find(i => i.value == value);
    return found ? found.text : null;
  }

  static renderUserName(user) {
    if(user.firstName && user.lastName) {
      return user.firstName + ' ' + user.lastName
    }
    return user.name
  }
  static findRoomUser(room, id) {
    if(room && room.users) {
      return room.users.find(u => u.id == id)
    }
  }
  
  static copyValidProps(componentType, props) {
    let propsCopy = {}
    let validProps = []
    for(let p in componentType.propTypes) {
      validProps.push(p)
    }
    for(let p in props) {
      if(validProps.indexOf(p) >= 0) propsCopy[p] = props[p]
    }
    return propsCopy
  }
}