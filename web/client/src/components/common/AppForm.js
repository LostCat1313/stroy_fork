import React from 'react'
import PropTypes from 'prop-types'
import Formsy from 'formsy-react'
import classNames from "classnames"
import { Button, withStyles } from '@material-ui/core'

const styles = (theme) => ({
  fieldsContainer: {
    display: 'flex',
    justifyContent: 'center',
    flexWrap: 'wrap'
  },
  columnFieldsContainer: {
    display: 'flex',
    justifyContent: 'flex-start',
    flexWrap: 'none',
    flexDirection: 'column',
    maxWidth: '400px'
  },
  columnButtonsContainer: {
    display: 'flex',
    marginTop: theme.spacing.unit*2
  },
  buttonsContainer: {
    display: 'flex',
    justifyContent: 'flex-end',
    marginTop: theme.spacing.unit*2
  },
  button: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  }
})

@withStyles(styles)
export default class AppForm extends React.Component {

  static propTypes = {
    classes:PropTypes.object.isRequired,
    className:PropTypes.string,
    hideControls:PropTypes.bool
  }
  
  state = { canSubmit: false }

  enableButton = () => {
    this.setState({ canSubmit: true })
  }

  disableButton = () => {
    this.setState({ canSubmit: false })
  }

  submitForm = (data) => {
    if(this.props.onValidSubmit) {
      this.props.onValidSubmit(data)
    }
  }

  cancelForm() {
    if(this.props.onCancel) {
      this.props.onCancel()
    }
  }

  notifyFormError = (data) => {
    console.error('Form error:', data)
  }

  addModelToChildren() {
    return this.props.model ? React.Children.map(this.props.children,
      (child) => React.cloneElement(child, {
        model: this.props.model
      })
    ) : this.props.children
  }

  render() {
    const { classes, onCancel, saveLabel, className, column } = this.props
    return (
      <Formsy
        onValid={this.enableButton}
        onInvalid={this.disableButton}
        onValidSubmit={this.submitForm}
        onInvalidSubmit={this.notifyFormError}
      >
        <div className={classNames(
          column ? 
            classes.columnFieldsContainer : 
            classes.fieldsContainer, className)}>
          {this.addModelToChildren()}
        </div>
        <div className={column ? classes.columnButtonsContainer : classes.buttonsContainer}>
          <Button
            className={classes.button}
            type="submit"
            disabled={!this.state.canSubmit}
          >{saveLabel ? saveLabel : "Сохранить"}</Button>
          {onCancel && <Button
            className={classes.button}
            label=""
            onClick={onCancel}
          >Отмена</Button>}
        </div>
      </Formsy>
    )
  }
}
