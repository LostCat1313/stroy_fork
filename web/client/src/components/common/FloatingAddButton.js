import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import { Button, Menu, MenuItem } from '@material-ui/core'
import AddIcon from '@material-ui/icons/Add'

const styles = (theme) => ({
  root: {
    position: 'fixed',
    bottom: '20px',
    right: '20px'
  },
  popover: {

  }
})

@withStyles(styles)
export default class FloatingAddButton extends React.Component {
  
  state = {
    open: false,
  }

  static propTypes = {
    items: PropTypes.array,
    classes: PropTypes.object.isRequired
  }

  clickHandler = event => {
    const { items, onClick } = this.props
    if(items && items.length > 0) {
      this.setState({
        open: true,
        anchorEl: event.currentTarget,
      })
    }
    else if(onClick) {
      onClick(event)
    }
  }

  closeHandler = (item, event) => {
    console.log(item, event)
    event.stopPropagation()
    if(item && item.onClick) {
      item.onClick()
    }
    this.setState({ anchorEl: null })
  }

  render() {
    const { classes, items } = this.props
    const { anchorEl } = this.state
    return <div className={classes.root}><Button 
      variant="fab"
      color="primary"
      onClick={this.clickHandler}
      aria-label="Add">
        <AddIcon />
        {items && items.length > 0 && <Menu
          id="simple-menu"
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'center',
          }}
          transformOrigin={{
            vertical: 'bottom',
            horizontal: 'right',
          }}
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={this.closeHandler.bind(this, null)}
        >
          {items.map((item, index) => <MenuItem
            key={index}
            onClick={this.closeHandler.bind(this, item)}>
              {item.title}
            </MenuItem>)}
        </Menu>}
      </Button></div>
  }
}
