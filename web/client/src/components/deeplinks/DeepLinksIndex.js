import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import { Button, TextField } from '@material-ui/core';

import { Table, Header, Loading } from 'components';
import { withStore } from 'decorators';

@withStore('Session', { preload: false })
export default class DeepLinksIndex extends Component {

  state = { 
    loading: false,
    result: null
  }

  generateInviteLink = () => {
    fetch('/api/deeplinks/new', {
      method: 'POST',
      headers: { 
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.props.Session.token,
      },
      body: this.state.params
    })
    .then(response => response.json())
    .then(result => {
      console.log(result);
      this.setState({loading: false, result});
    });
  }

  render() {
    const { result, loading, params } = this.state;
    return (
      <div className="container">
        <Header>Deep links</Header>

        <p><Link to="/DeepLink/new">Добавить</Link></p>

        { loading && <Loading/> }

        <pre>{ result ? JSON.stringify(result, null, '  ') : null }</pre>

        <div>
          <TextField multiline onChange={(e) => this.setState({ params: e.target.value })} value={params} />
        </div>
        
        <Button
          disabled={loading}
          onClick={this.generateInviteLink}
        >Генерировать</Button>
      </div>
    )
  }
}
