import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'

import { BaseComponent } from "components"

const styles = (thumbs) => ({
  root: {
    height: '200px',
    width: '200px',
    maxHeight: '200px',
    maxWidth: '200px',
    padding: '5px',
    marginRight: '5px',
    marginTop: '5px',
    marginLeft: '5px',
    background: '#ddd',
    overflow: 'hidden',
    position: 'relative',
  },
  container: {
    display: 'flex',
    alignItems: 'center',
    height: '100%',
    '& img': {
      width: '100%',
    }
  },
  controls: {
    position: 'absolute',
    top: '0px',
    left: '0px',
    right: '0px',
    height: '24px',
    zIndex: '10',
  },
  pageNumber: {
    textAlign: 'center',
    fontWeight: 'bold',
    lineHeight: '24px'
  }
})

@withStyles(styles)
export default class PageThumb extends BaseComponent{
  static propTypes = {
    classes:PropTypes.object.isRequired,
    page:PropTypes.number,
    onClick:PropTypes.func,
    active:PropTypes.bool
  }

  render() {
    const { page, classes, onClick, active} = this.props
    return <div 
      className={classes.root}
      style={{background: active ? '#aaa' : null}}
      onClick={onClick}>
      <div className={classes.controls}>
        {/* !this.props.readOnly && <div 
          className="page-delete"
          onClick={() => {this.onDelete(p.id)}}
        ><CloseCircleIcon color="#404040"/></div>*/}
        <div className={classes.pageNumber}>{page.pageNumber}</div>
        {/*showActive && <div className="page-active">
          <input 
            type="checkbox"
            defaultChecked={p.actual}
            onChange={(e) => {
              this.handleChangeChk(p)
            }}/>
          </div>*/}
      </div>
      <div className={classes.container}>
        <img src={page.thumbUrl} />
      </div>
    </div>
  }
}