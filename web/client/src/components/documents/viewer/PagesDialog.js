import React from 'react'
import { connect } from 'react-redux'
import { Dialog, DialogTitle, DialogContent } from '@material-ui/core'
import { bindActionCreators } from 'redux'
import Formsy from 'formsy-react'
import CloseCircleIcon from 'mdi-react/CloseCircleIcon'

import { deleteDocumentVersion } from 'actions'
import { FileField } from 'components'

function mapStateToProps(state) {
  const props = { page: state.Page }
  return props
}

function mapDispatchToProps(dispatch) {
  const actions = { deleteDocumentVersion }
  const actionMap = { actions: bindActionCreators(actions, dispatch) }
  return actionMap
}


@connect(mapStateToProps, mapDispatchToProps)
export default class VersionsDialog extends React.Component {

  constructor(props) {
    super(props)
    const list = this.props.document ? this.props.document.versions || [] : []
    console.log(list)
    this.state = { list }
  }

  componentDidMount() {
    // this.props.actions.loadPages(this.props.documentId).then(() => {
    //   this.setState({ list })
    // })
  }

  onUpload = (entry) => {
    // add version 
    this.setState(prevState => {
      return {
        list: [
          ...prevState.list,
          entry
        ]
      }
    })
  }

  onDelete(id) {
    this.props.actions.deletePage(id).then(() => {
      this.setState(prevState => {
        return {
          list: prevState.list.filter(i => i.id != id)
        }
      })
    })
  }

  render() {
    if(!this.props.document) throw new Error("No document defined")
    return <Dialog
      ref="dialog"
      modal
      repositionOnUpdate
      autoScrollBodyContent
      open={this.props.open}
    >
      <DialogTitle>
        <div>{this.props.title}<div style={{float:'right'}}>
          <CloseCircleIcon
              className="hovered-icon"
              onClick={this.props.onCancel}
            />  
        </div></div>
      </DialogTitle>
      <DialogContent>
        <Formsy>
          <FileField
            name="file"
            label="Добавить страницы"
            onUpload={this.onUpload}
            prepareOnUploadData={(data) => data.entry}
            url={`/api/documents/${this.props.document.id}/pages/upload`}
            uploadOnly
            required
          />
        </Formsy>
      </DialogContent>
    </Dialog>
  }
}
