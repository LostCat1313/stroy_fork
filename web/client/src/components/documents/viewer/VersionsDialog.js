import React from 'react'
import Formsy from 'formsy-react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Dialog, DialogTitle, DialogContent, Tabs, Tab, withStyles, IconButton, Typography } from '@material-ui/core';
import { Close } from '@material-ui/icons';

import { deleteDocumentVersion } from 'actions'
import { FileField, ToggledItem } from 'components'
import Pages from './Pages'
import { withStore } from 'decorators';
import PagesField from '../../common/fields/PagesField';

@withStore('Page', { preload: false, actions: { deleteDocumentVersion } })
@withStyles(theme => ({
  title: {
    display: 'flex',
    alignItems: 'center'
  },
  titleContent: {
    flexGrow: 1
  },
  fileField: {
    marginTop: theme.spacing.unit * 2,
    width: '100%',
    maxWidth: '100%'
  }
}))
export default class VersionsDialog extends React.Component {

  static propTypes = {
    document:PropTypes.object.isRequired
  }

  constructor(props) {
    super(props)
    const list = this.props.document ? this.props.document.versions || [] : []
    this.state = {
      tabIndex: 0,
      list 
    }
  }

  componentDidMount() {
    // this.props.actions.loadPages(this.props.documentId).then(() => {
    //   this.setState({ list })
    // })
  }

  onUpload(entry) {
    // add version 
    this.setState(prevState => {
      return {
        list: [
          ...prevState.list,
          entry
        ]
      }
    })
  }

  onDelete(id) {
    this.props.actions.deletePage(id).then(() => {
      this.setState(prevState => {
        return {
          list: prevState.list.filter(i => i.id != id)
        }
      })
    })
  }

  handleTabChange = (event, value) => {
    this.setState({tabIndex: value});
  }

  render() {
    const { classes, onCancel, open, title } = this.props;
    const { tabIndex } = this.state;
    if(!this.props.document) throw new Error("No document defined")
    return <Dialog
        fullWidth
        maxWidth="md"
        onClose={onCancel}
        open={open}>
      <DialogTitle disableTypography className={classes.title}>
        <Typography className={classes.titleContent} variant="h6">{title}</Typography>
        <IconButton color="primary" onClick={onCancel}><Close/></IconButton>
      </DialogTitle>
      <DialogContent>
        <Tabs value={tabIndex} onChange={this.handleTabChange}>
          <Tab label="Загрузить документ" />
          <Tab label="Постраничная загрузка" />
        </Tabs>
        {tabIndex == 0 && <Formsy>
          <FileField
            className={classes.fileField}
            name="file"
            accept=".pdf, .doc, .docx"
            label="Добавить версию"
            onUpload={this.onUpload.bind(this)}
            prepareOnUploadData={(data) => data.entry}
            url={`/api/document/versions/${this.props.document.id}/upload`}
            uploadOnly
            required
          />
        </Formsy> }
        {tabIndex == 1 && <PagesField 
          className={classes.fileField}
          label="Выбрать страницы версии"
          url={`/api/v2/documents/${this.props.document.id}/pages/upload`}
        /> }

        {this.state.list.length > 0 && this.state.list.map(item => <ToggledItem 
              key={item.id}
              vertical
              onToggle={()=>{
                setTimeout(()=>{
                  this.forceUpdate()
                })  // todo: wait till pages rendering will be completed
              }}
              title={item.title + ' ' + item.id}>
              <Pages
                documentId={item.documentId}
                documentVersionId={item.id}
                />
            </ToggledItem>)}
        {this.state.list.length == 0 && <div>
          Нет версий
        </div>}
      </DialogContent>
    </Dialog>
  }
}
