import React, { Fragment } from 'react';
// import { connect } from 'react-redux';
import Formsy from 'formsy-react';
import { GridList, GridListTile, Grid, IconButton, Checkbox, withStyles } from '@material-ui/core';
import { Cancel } from '@material-ui/icons';

import { loadPages, deletePage, setPageActual } from 'actions';
import { FileField, Loading } from 'components';
import { withStore } from 'decorators';

@withStyles(theme => ({
  root: {
    
  },
  pageControls: {
    position: 'absolute',
    zIndex: 100,
    display: 'flex',
    left: 0,
    top: 0,
    right: 0,
    alignItems: 'center'
  },
  pageNumber: {
    flex: '1 0 auto',
    textAlign: 'center'
  },
  pageNumberContent: {
    display: 'inline-block',
    borderRadius: '10px',
    padding: '0 5px',
    background: theme.palette.primary.main,
    color: 'white'
  },
  gridList: {
    width: '100%'
  },
  tile: {
    position: 'relative'
  }
}))
@withStore('Page', { preload: false, actions: { loadPages, deletePage, setPageActual } })
export default class Pages extends React.Component {

  state = {
    list: null,
    loading: false
  }

  componentDidMount() {
    this.setState({ loading: true });
    this.props.actions.loadPages(this.props.documentId, this.props.documentVersionId)
      .then(() => {
        const list = this.props.Page.list
        // console.log(list)
        this.setState({
          list: list.slice(0, list.length),
          loading: false
        });
      })
  }

  onUpload(files) {
    this.setState(prevState => {
      return {
        list: [
          ...prevState.list,
          ...files
        ]
      }
    })
  }

  onDelete(id) {
    this.props.actions.deletePage(id).then(() => {
      this.setState(prevState => {
        return {
          list: prevState.list.filter(i => i.id != id)
        }
      })
    })
  }

  activate(id, val) {
    this.setState(prevState => {
      this.state.list.forEach(i => {
        if(i.id == id) i.actual = val
      })
      return {
        list: this.state.list.slice(0)
      }
    })
  }

  handleChangeChk = p => e => {
    let original = p.actual
    this.activate(p.id, !original)
    this.props.actions.setPageActual(p.id, !original).then(() => {
      // if fault then uncheck
    }, () => {
      this.activate(p.id, original)
    })
  }

  render() {
    const { documentVersionId, documentId, readOnly, classes } = this.props;
    const { list } = this.state;

    return (<Fragment>
      {!readOnly && <Formsy>
        <FileField
          name="file"
          label="Добавить страницы"
          onUpload={this.onUpload.bind(this)}
          url={`/api/documents/${documentId}/${documentVersionId}/pages/upload`}
          uploadOnly
          accept="image/*"
          required
        />
      </Formsy>}
      { list && <Fragment>
        <div>{list.length} страниц</div>
        <GridList className={classes.gridList} cellHeight={160} cols={4}>
        { list.map((p) => {
          return <GridListTile key={p.id}>
              <div className={classes.pageControls}>
                {documentVersionId && <Checkbox defaultChecked={p.actual} onChange={this.handleChangeChk(p)}/>}
                <div className={classes.pageNumber}><div className={classes.pageNumberContent}>{p.pageNumber}</div></div>
                {!readOnly && <IconButton color="primary"
                  onClick={() => {this.onDelete(p.id)}}
                  ><Cancel/></IconButton>}
              </div>
            <img src={p.thumbUrl} />
          </GridListTile>
        })}
        </GridList>
      </Fragment> }
    </Fragment>)
  }
}
