import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { withStyles } from '@material-ui/core'
import PropTypes from 'prop-types'

import { loadPages, loadDocumentById, deletePage, setPageActual } from 'actions'
import { BaseComponent, FloatingAddButton } from 'components'
import PageThumb from './PageThumb'
import PageView from './PageView'
import VersionsDialog from './VersionsDialog'
import PagesDialog from './PagesDialog'
import RightsDialog from 'components/common/RightsDialog'

const mapStateToProps = (state) => ({ document: state.Document, page: state.Page })
const mapDispatchToProps = (dispatch) => ({ 
  actions: bindActionCreators({ loadPages, deletePage, loadDocumentById, setPageActual }, dispatch)
})

const barWidth = '256px'
const styles = (theme) => ({
    root: {
      position: 'absolute',
      top: '64px',  // theme.panel.heightc
      bottom: '0px',
      right: '0px',
      left: '0px'
    },
    thumbs: {
      width: barWidth,
      left: '0px',
      bottom: '0px',
      top: '0px',
      position: 'absolute',
      'overflow-y': 'auto',
      'overflow-x': 'hidden',
      borderRight: '1px solid #efefef',
    },
    thumbsContent: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center'
    },
    content: {
      float: 'right',
      overflow: 'auto',
      left: barWidth,
      bottom: '0px',
      top: '0px',
      right: '0px',
      position: 'absolute',
    }
  })

@connect(mapStateToProps, mapDispatchToProps)
@withStyles(styles)
export default class DocumentViewerIndex extends BaseComponent {

  static propTypes = {
    classes:PropTypes.object.isRequired
  }

  constructor(props) {
    super(props)
    this.state = {
      showVersions:false
    }
    
    const document = this.getCurrentEntryDB(this.props.document.db)
    if(document) {
      this.state.document = document
      this.state.list = document.pages || []
      this.state.currentPage = document.pages && document.pages.length > 0 ? document.pages[0].id : null
    }
  }

  componentDidMount() {
    let document = this.state.document
    if(!document) {
      this.reloadDocument()
    } 
  }

  reloadDocument() {
    this.props.actions.loadDocumentById(+this.props.match.params.id)
      .then((result) => {
        if(result.status !== 'success') throw new Error('Failed to get document')
        const document = result.response.entry
        console.log(result.response)
        this.setState({
          document,
          list: document.pages || [],
          currentPage: document.pages && document.pages.length > 0 ? document.pages[0].id : null
        })
      })
  }

  loadPages(id) {
    this.props.actions.loadPages()
        .then(() => {
          const list = this.props.page.list
          this.setState({
            list: list.slice(0, list.length)
          })
        })
  }

  render() {
    if(!this.state.document) {
      return null
    }
    const { classes } = this.props
    const { dialog, document } = this.state
    return (
      <div className={classes.root}>     
        <div 
          className={classes.thumbs}
          ref="thumbsContainer">
          { this.state.list && 
            <div className={classes.thumbsContent} ref="thumbsContainerContent">
                { this.state.list.map((p, index) => {
                  return <PageThumb
                    key={p.id}
                    page={p}
                    active={this.state.currentPage == p.id}
                    onClick={()=>{
                      let pview = window.document.getElementById('page-' + p.id)
                      if(pview) {
                        this.refs.documentContainer.scrollTo(0, pview.offsetTop - 8)
                      }
                    }}/>
                })}
            </div> }
        </div>
        <div
          className={classes.content} 
          ref="documentContainer"
          onScroll={(e)=>{
            const c = e.target
            let top = c.scrollTop + c.offsetHeight / 2
            let children = c.childNodes
            const tc = this.refs.thumbsContainer
            const tcc = this.refs.thumbsContainerContent
            for(let i = children.length - 1; i >= 0; i--) {
              if(top >= children[i].offsetTop) {
                const currentPage = parseInt(children[i].id.split('-')[1])
                console.log(currentPage)
                if(this.state.currentPage != currentPage) {
                  this.setState({
                    currentPage
                  })
                  tc.scrollTo(0, tcc.childNodes[i].offsetTop - 20)
                }
                return                
              }
            }
          }}>
          {this.state.list.length == 0 && <div style={{textAlign:'center', paddingTop: 20}}>Нет страниц</div>}
          { this.state.list.map((p, index) => {
            return <PageView key={p.id} page={p}/>
          })}
        </div>

        <VersionsDialog
          title="Версии"
          document={document}
          open={dialog == 'versions'}
          onCancel={() => { this.setState({dialog:null}) }}
        />
        <PagesDialog
          title="Страницы"
          document={document}
          open={dialog == 'pages'}
          onCancel={() => { 
            this.setState({dialog:null})
          }}
        />
        <RightsDialog
          title="Права на документ"
          open={dialog == 'rights'}
          onCancel={() => { 
            this.setState({dialog:null})
          }}
        />
    <FloatingAddButton items={[
          {title:'Версии', onClick:() => {
            this.setState({
              dialog: 'versions'
            })
          }},
          // {title:'Страницы', onClick:() => {
          //   this.setState({
          //     dialog: 'pages'
          //   })
          // }},
          {title:'Права', onClick:() => {
            this.setState({
              dialog: 'rights'
            })
          }},
        ]}/>
      </div>
    )
  }
}



