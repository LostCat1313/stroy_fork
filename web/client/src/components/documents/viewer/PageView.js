import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Paper from '@material-ui/core/Paper'

import { loadPages, deletePage, setPageActual } from 'actions'

class PageView extends React.Component {

  constructor(props) {
    super(props)
  }

  componentDidMount() {
  }

  render() {
    return (<Paper id={'page-'+this.props.page.id} style={{margin: '15px'}} zDepth={1}>
      <img style={{width:'100%'}} src={this.props.page.url}/>
    </Paper>)
  }
}

function mapStateToProps(state) {
  const props = { 
    // page: state.page 
  }
  return props
}

function mapDispatchToProps(dispatch) {
  const actions = { loadPages, deletePage, setPageActual }
  const actionMap = { actions: bindActionCreators(actions, dispatch) }
  return actionMap
}

export default connect(mapStateToProps, mapDispatchToProps)(PageView)