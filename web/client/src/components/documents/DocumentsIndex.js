import React from 'react'

import { BaseComponent } from 'components'
import FoldersIndex from './folders/FoldersIndex'
import { withStore } from 'decorators'

@withStore('Project', { preloadIfNull: true })
export default class DocumentsIndex extends BaseComponent {
  state = {

  };

  constructor(props) {
    super(props);
  }

  getCurrentProject() {
    return this.props.Project.list.find(p => p.id == this.props.match.params.projectId)
  }


  getParentUrlPart(parentId) {
    if (parentId)
      return '?parentId=' + parentId
    return ''
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      parentId: this.extractQueryParam('parentId', nextProps)
    });
  }

  getTitle() {
    const p = this.getCurrentProject();
    return p && p.title || '-'; 
  }

  render() {
    const projectId = this.props.match.params.projectId
    return (
      <FoldersIndex projectId={projectId} title={this.getTitle()}/>
    )
  }
}
