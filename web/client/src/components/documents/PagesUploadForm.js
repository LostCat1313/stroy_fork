import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { createDocument, updateDocument } from 'actions'

class PagesUploadForm extends React.Component {
  constructor(props) {
    super(props, 'Document')
    this.backUri = '/projects'
    this.goBack = true
    console.log(this.state)
  }

  renderHeader() {
    let entry = this.getEntry()
    return entry
      ? (entry.isFolder
        ? 'Редактирование папки '
        : 'Редактирование документа ') + entry.title
      : this.props.isFolder
        ? 'Новая папка'
        : 'Новый документ'
  }

  prepareData(data) {
    return {
      ...data,
      isFolder: this.isFolder(),
      parentId: this.extractQueryParam('parentId'),
      projectId: this.props.match.params.projectId
    }
  }

  isFolder() {
    return !!(this.props.isFolder || this.getEntry() && this.getEntry().isFolder)
  }
}

function mapStateToProps(state) {
  const props = { Document: state.Document }
  return props
}

function mapDispatchToProps(dispatch) {
  const actions = { createDocument, updateDocument }
  const actionMap = { actions: bindActionCreators(actions, dispatch) }
  return actionMap
}


export default connect(mapStateToProps, mapDispatchToProps)(Edit)
