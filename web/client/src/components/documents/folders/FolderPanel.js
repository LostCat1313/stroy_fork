import React, {Fragment} from 'react'
import { List, ListItem, ListItemText, ListItemIcon, withStyles, Divider } from '@material-ui/core'
import FileMultipleIcon from 'mdi-react/FileMultipleIcon'
import ChevronRightIcon from 'mdi-react/ChevronRightIcon'
import { Link } from 'react-router-dom'
import CloseCircleIcon from 'mdi-react/CloseCircleIcon'
import InformationIcon from 'mdi-react/InformationIcon'

import { BaseComponent, ListItemControls } from 'components'

const styles = (theme) => ({
  root: {
    display: 'flex',
    flex: '0 0 33.33%',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    'overflow-y': 'auto',
    borderRight: '1px #ddd solid'
  },
  chevron: {
    position: 'relative',
    width: '24px',
    height: '24px'
  },
  listItem: {
    position: 'relative',
    flex: '0 0 50px'
  },
  divider: {
    background: '#ddd',
    alignSelf: 'stretch'
  }
})

@withStyles(styles)
export default class FolderPanel extends BaseComponent {

  fireOnClick(item) {
    if(this.props.onClick && item.children && item.isFolder) {
      this.props.onClick(item)
    }
  }

  renderTitle(item) {
    if(item.isFolder) {
      return /*'[' + item.id + ']' +*/ item.title + ' (' + (item.children ? item.children.length : 0) + ')'
    }
    else {
      return <Link to={'/documents/' + item.id}>{item.title}</Link>
    }
  }

  controlsActionHandler = (item, action) => {
    console.log('FolderPanel::controlsActionHandler', item, action)
    this.fire(action, item)
  }

  render() {
    const { classes, style } = this.props
    return <List component="div" className={classes.root} style={style} disablePadding>
      {this.props.list && this.props.list.map((i, index) => {
        return <Fragment key={i.id}>
            <ListItem 
              button
              selected={this.props.selected && i.id == this.props.selected.id}
              className={classes.listItem}
              onClick={() => {
                this.fireOnClick(i)
              }}>
              {i.isFolder && <ListItemIcon><FileMultipleIcon style={{fill:'orange'}}/></ListItemIcon> }
              <ListItemText>{this.renderTitle(i)}</ListItemText>
              {i.isFolder ? <ChevronRightIcon className={classes.chevron} style={{fill:'#999'}}/> : null}
              <ListItemControls 
                deleteButton 
                infoButton
                onAction={this.controlsActionHandler.bind(this, i)} />
            </ListItem>
            <Divider className={classes.divider}/>
          </Fragment>
      })}
      {!this.props.list || this.props.list.length == 0 && <ListItem disabled><ListItemText>Нет документов</ListItemText></ListItem>}
    </List>
  }
}
