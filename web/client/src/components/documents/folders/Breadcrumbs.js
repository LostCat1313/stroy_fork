import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'

const styles = (theme) => ({
  root: {
    lineHeight: '30px',
    paddingLeft: theme.spacing.unit * 2,
    paddingRight: theme.spacing.unit * 2,
    borderBottom: '1px solid #ddd'
  },
  first: {
    fontWeight:'bold'
  }
})

@withStyles(styles)
export default class Breadcrumbs extends React.Component {
  static propTypes ={
    list:PropTypes.array,
    root:PropTypes.string
  }

  renderPath() {
    if(this.props.list && this.props.list.length > 0) {
      return this.props.list.map((item)=>{
        return <span key={item.id}>
          {item.title}{' / '}
        </span>
      })
    }
    return null
  }

  render() {
    const { classes, root } = this.props
    return <div className={classes.root}>
      <span className={classes.first}>{root}{' / '}</span>
      {this.renderPath()}
    </div>
  }
}