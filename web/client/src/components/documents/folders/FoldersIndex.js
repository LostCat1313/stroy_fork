import React from 'react'
import { withStyles } from '@material-ui/core'

import { BaseComponent, FloatingAddButton } from 'components'
import DocumentEditDialog from 'components/documents/DocumentEditDialog'
import FolderPanel from './FolderPanel'
import Breadcrumbs from './Breadcrumbs'
import { withStore } from 'decorators'

const styles = (theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    position: 'absolute',
    top: '64px',
    left: '0px',
    right: '0px',
    bottom: '0px',
    'overflow': 'hidden'
  },
  folders: {
    display: 'flex',
    flex: '1 1',
    width: '100%',
    'overflow-x': 'auto',
    position: 'relative',
    justifyContent: 'flex-start'
  }
})

@withStyles(styles)
@withStore('Document', { prepareParams: (props) => ({ projectId:props.projectId})})
export default class FoldersIndex extends BaseComponent {
  constructor(props) {
    super(props)
    this.state = {
      panels: [],
      selected: []
    }
  }

  componentDidMount() {
    if(this.props.Document) {
      this.setState({
        panels: [this.props.Document.list]
      })
    }
  }

  renderEditDialogTitle() {
    switch(this.state.dialog) {
      case 'document.new':
        return 'Новый документ'
      case 'folder.new':
        return 'Новая папка'
    }
  }

  getSelectedFolder() {
    return this.state.selected.length > 0 ? this.state.selected[this.state.selected.length - 1].id : null
  }

  handleDelete(document) {
    this.props.actions.deleteDocument(document.id)
      .then((response) => {
        if(response.status == 'success') {
          this.setState(prevState => {
            let selected = prevState.selected
            let si = selected.findIndex((v) => v.id == document.id)
            if(si > -1) {
              return {
                selected: selected.slice(0, si),
                panels: prevState.panels.slice(0, si + 1)
                  .map(p => p.filter(i => i.id != document.id))
              }
            }
            else {
              let panels = prevState.panels
              return {
                panels: panels
                  .map(p => p.filter(i => i.id != document.id))
              }
            }
          })
          
        }
      })
  }

  handleInfo(document) {
    this.setState({ currentId: document.id, dialog: true })
  }

  handleClick(document, index) {
    let panels = this.state.panels
    let selected = this.state.selected
    const si = selected.findIndex(i => i.id == document.id)
    console.log(document)
    if(si < 0 || si < selected.length - 1) {
      this.setState({
        panels: [
          ...panels.slice(0, index+1),
          document.children
        ],
        selected: [
          ...selected.slice(0, index),
          document
        ]
      })
    }
    else {
      this.setState({
        panels: panels.slice(0, si+1),
        selected: selected.slice(0, si)
      })
    }
    setTimeout(() => {
      this.refs.folders.scrollLeft = this.refs.folders.scrollWidth
    })
  }



  addItem(item) {
    this.setState(prevState => {
      let panels = prevState.panels
      let selected = prevState.selected[prevState.selected.length - 1]
      if(item.isFolder) {
        panels[panels.length - 1] = [item, ...panels[panels.length - 1]]
      } else {
        panels[panels.length - 1] = [...panels[panels.length - 1], item]
      }
      if(selected) selected.children = panels[panels.length - 1]
      return { panels }
    })    
  }

  completeHandler = (result)=>{
    this.addItem(result)
    this.setState({ 
      dialogOpen:false,
      dialog:null,
      current:null
    })
  }

  cancelHandler = () => { this.setState({ dialogOpen:false }) }

  render() {
    const { classes } = this.props
    return <div className={classes.root}>
      <Breadcrumbs root={this.props.title} list={this.state.selected}/>
      <div ref="folders" className={classes.folders}>
        {this.state.panels.map((p, index) => {
          return <FolderPanel
            key={index}
            list={p}
            selected={this.state.selected[index]}
            onDelete={this.handleDelete.bind(this)}
            onClick={(d) => this.handleClick(d, index)}
            onInfo={(d) => this.handleInfo(d, index)}
          />
        })}
      </div>
      <FloatingAddButton items={[
        {title:'Папка', onClick:() => {
          this.setState({
            dialog: 'folder.new',
            dialogOpen: true
          })
        }},
        {title:'Документ', onClick:() => {
          this.setState({
            dialog: 'document.new',
            dialogOpen: true
          })
        }}
      ]}/>
      <DocumentEditDialog
        title={this.renderEditDialogTitle()}
        open={this.state.dialogOpen}
        documentId={this.state.currentId}
        isFolder={this.state.dialog == 'folder.new'}
        projectId={this.props.projectId}
        parentId={this.getSelectedFolder()}
        onComplete={this.completeHandler}
        onCancel={this.cancelHandler} 
      />
    </div>
  }
}
