import React from 'react'
import { connect } from 'react-redux'

import { BaseComponent, ToggledItem, Header } from 'components'
import Pages from './Pages'
import Versions from './Versions'

const mapStateToProps = state => ({ document: state.Document })

@connect(mapStateToProps)
export default class Show extends BaseComponent {
  constructor(props) {
    super(props)
    this.state = {}
    if(this.props.document && this.props.document.db) {
      this.state.document = this.getCurrentEntryDB(this.props.document.db)
    }
  }

  componentWillMount() {
    if(!this.state.document) {
      this.props.history.replace('/')      
    }
  }

  render() {
    if(!this.state.document) {
      return null
    }
    return (
      <div className="container">
        <Header>Документ {this.state.document.title}</Header>
        <ToggledItem title="Страницы">
          <Pages
            documentId={this.state.document.id}
            readOnly={true}
            />
        </ToggledItem>
        <p>Версии</p>
        <Versions
          document={this.state.document}
          />
      </div>
    )
  }
}
