import React from 'react'
import { Dialog, DialogTitle, DialogContent } from '@material-ui/core'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { EditComponent, TextField, FileField, AppForm, LineBreak } from 'components'
import { createDocument, updateDocument } from 'actions'
import { withStore } from 'decorators';

@withStore('Document', { preload: false, actions: { createDocument, updateDocument }})
export default class DocumentEditDialog extends EditComponent {
  constructor(props) {
    super(props, 'Document')
    console.log(this.state)
  }

  titleRef = React.createRef();
  shortTitleRef = React.createRef();

  getCurrentEntry(db, id) {
    id = id || this.props.documentId
    if(!id) return null
    var current = db
      ? db[id] 
      : null
    return current ? {...current} : null
  }

  componentWillReceiveProps(props, state) {
    this.setState({
      _entry: this.getCurrentEntry(this.store.db, props.documentId)
    })
  }

  prepareData(data) {
    return {
      ...data,
      isFolder: this.isFolder(),
      parentId: this.props.parentId,
      projectId: this.props.projectId
    }
  }

  isFolder() {
    return !!(this.props.isFolder || this.getEntry() && this.getEntry().isFolder)
  }

  requestHandler(payload) {
    if(payload.status === "success" && payload.response) {
      console.log("EditDialog::requestHandler:", payload)
      if(this.props.onComplete) {
        if(payload.response.isFolder) {
          payload.response.children = []
        }
        this.props.onComplete(payload.response)
      }
    } else {
      console.log(payload)
    }
  }

  renderForm() {
    return (
      <AppForm
        model={this.state._entry}
        onValidSubmit={this.submitForm.bind(this)}
        onCancel={this.props.onCancel}
        buttonsStyle={{float:'right'}}>
        {this.addKeys(this.renderFields())}
      </AppForm>
    )
  }

  onUploadHandler(data) {
    if(data && data.length == 1) {
      this.titleRef.current && this.titleRef.current.setValue(data[0].original)
      this.shortTitleRef.current && this.shortTitleRef.current.setValue(data[0].original)
    }
    // setTimeout(() => {
    //   this.forceUpdate()
    // }, 100)
  }

  renderFields() {
    let fields = [
      <TextField
        name="title"
        label="Имя"
        innerRef={this.titleRef}
        required
      />
    ]
    if(!this.isFolder() && !this.props.edit) {
      fields.push(<TextField
          name="shortTitle"
          label="Короткое имя"
          innerRef={this.shortTitleRef}
          required
      />)
      fields.push(<LineBreak/>)
      fields.push(<FileField
        name="file"
        label="Документ"
        required
        onUpload={this.onUploadHandler.bind(this)}
      />)
    }
    return fields
  }

  render() {
    const { title, open } = this.props
    return <Dialog
      modal
      open={open}
      style={{maxHeight: '100%', overflow: 'auto', paddingBottom: '100px'}}>
      <DialogTitle>{title}</DialogTitle>
      <DialogContent>
        {this.renderForm()}
      </DialogContent>
    </Dialog>
  }
}
