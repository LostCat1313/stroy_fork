import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { loadLastDocuments } from 'actions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { Header, Table } from 'components';

const mapStateToProps = (state) => ({
    document:state.Document
  });

const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators({ loadLastDocuments }, dispatch) 
});

@connect(mapStateToProps, mapDispatchToProps)
export default class LastDocuments extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.actions.loadLastDocuments();
  }

  render() {
    return (
      <div className="container">
        <Header>Последние документы</Header>
        <Table
          dataList={this.props.document.last}
          headerColumns={[{ type: 'id' }, "Имя",  "Проект"]}
          rowColumns={[
            { type: 'id' },
            item => <Link to={'/documents/' + item.id}>{item.title}</Link>,
            item => item.project ? item.project.title : '-'
          ]}
        ></Table>
      </div>
    );
  }
}
