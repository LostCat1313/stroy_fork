import React from 'react'
import PropTypes from 'prop-types'
import { Drawer, withStyles, AppBar, Toolbar, IconButton, Typography, Hidden, List } from '@material-ui/core'
import MenuIcon from '@material-ui/icons/Menu'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { withRouter } from 'react-router-dom'

import ListItem from 'components/common/LinkListItem'
import DrawerHeader from './DrawerHeader';
import { logout } from 'actions'
import { BaseComponent } from 'components';

const mapStateTopProps = (state) => ({ Session: state.Session })
const mapDispatchToProps = (dispatch) => ({ actions: bindActionCreators({ logout }, dispatch) })

const drawerWidth = 300

const styles = theme => ({
  root: {
    flexGrow: 1,
    height: '100%',
    zIndex: 1,
    overflow: 'hidden',
    position: 'relative',
    display: 'flex',
    width: '100%',
  },
  appBar: {
    position: 'absolute',
    marginLeft: drawerWidth,
    [theme.breakpoints.up('md')]: {
      width: `calc(100% - ${drawerWidth}px)`,
    },
  },
  navIconHide: {
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
  toolbar: theme.mixins.toolbar,
  drawerDocked: {
    height: '100%'
  },
  drawerPaper: {
    width: drawerWidth,
    display: 'flex',
    flexDirection: 'column',
    [theme.breakpoints.up('md')]: {
      position: 'relative',
    },
  },
  content: {
    flexGrow: 1,
    position: 'relative',
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing.unit * 3,
    overflow: 'auto',
  },
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
})

@withRouter
@withStyles(styles, { withTheme: true })
@connect(mapStateTopProps, mapDispatchToProps)
export default class MenuComponent extends BaseComponent {
  static propTypes = {
    session: PropTypes.object,
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
  }

  state = {
    mobileOpen: false,
  }

  handleDrawerToggle = () => {
    this.setState(state => ({ mobileOpen: !state.mobileOpen }))
  }

  logoutClickHanlder = () => {
    this.props.actions.logout();
  }

  render() {
    const { classes, theme, children, Session } = this.props
    const { projects, documents, remarks, newMessages } = Session ? Session.countSummary : {}
    const menuList = (
      <List component="nav" disablePadding>
        <ListItem to="/" menuIcon="FormatListBulletedIcon" text="Лента"/>
        <ListItem to="/projects" menuIcon="BoxShadowIcon" text={ "Проекты (" + projects + ")" }/>
        <ListItem to="/lastdocuments" menuIcon="FileIcon" text={"Документы (" + documents + ")"}/>
        <ListItem to="/remarks" menuIcon="NoteIcon" text="Замечания"/>
        <ListItem to="/notes" menuIcon="StarIcon" text="Личные заметки"/>
        <ListItem to="/chat" menuIcon="MessageIcon" text={"Чаты (" + newMessages + ")"}/>
        <ListItem to="/contacts" menuIcon="AccountMultipleIcon" text="Контакты"/>
        <ListItem 
          menuIcon="SettingsIcon"
          text="Управление"
          nestedItems={[<ListItem to="/users" text="Пользователи"></ListItem>,
          <ListItem 
            text="Права"
            nestedItems={[
              <ListItem level={2} to="/rights/list" text="Правила"></ListItem>,
              <ListItem level={2} to="/rights/groups" text="Группы"></ListItem>
            ]}
            ></ListItem>,
          <ListItem to="/regids" text="Сессии"></ListItem>,
          <ListItem to="/organizations" text="Организации"></ListItem>,
          <ListItem to="/positions" text="Должности"></ListItem>,
          <ListItem to="/tasks" text="Задачи"></ListItem>,
          <ListItem to="/descriptors" text="Маркеры"></ListItem>,
          <ListItem to="/deeplinks" text="Связанные ссылки"></ListItem>,
          <ListItem to="/files" text="Файлы"></ListItem>]}
        />
        <ListItem menuIcon="LoginVariantIcon" onClick={this.logoutClickHanlder} text="Выход"/>
      </List>
    )

    return (
      <div className={classes.root}>
        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton
              color="inherit"
              aria-label="Open drawer"
              onClick={this.handleDrawerToggle}
              className={classes.navIconHide}
            >
              <MenuIcon />
            </IconButton>
            <Typography variant="h6" color="inherit" noWrap>
              Аксиома <span id="header-root"></span>
            </Typography>
          </Toolbar>
        </AppBar>
        <Hidden mdUp>
          <Drawer
            variant="temporary"
            anchor={theme.direction === 'rtl' ? 'right' : 'left'}
            open={this.state.mobileOpen}
            onClose={this.handleDrawerToggle}
            classes={{
              paper: classes.drawerPaper,
            }}
            ModalProps={{
              keepMounted: true, // Better open performance on mobile.
            }}
          >
            <DrawerHeader/>
            {menuList}
          </Drawer>
        </Hidden>
        <Hidden smDown implementation="css">
          <Drawer
            variant="permanent"
            open
            classes={{
              docked: classes.drawerDocked,
              paper: classes.drawerPaper,
            }}
          >
            <DrawerHeader/>
            {menuList}
          </Drawer>
        </Hidden>
        <main className={classes.content}>
          <div className={classes.toolbar} />
          {children}
        </main>
      </div>
    )
  }
}
