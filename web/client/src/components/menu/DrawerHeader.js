import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'

import Logo from 'components/common/Logo'
import { withStore } from 'decorators'

const styles = (theme) => ({
  background: {
    width: '100%'
  },
  header: {
    position: 'relative'
  },
  top: {
    position: 'absolute',
    bottom: '50px',
    left: '0px',
    right: '0px',
    display: 'flex',
    height: '100px',
    alignItems: 'center'
  },
  bottom: {
    position: 'absolute',
    bottom: '0px',
    width: '100%',
    background: 'rgba(40, 182, 246, 0.25)',
    height: '50px',
  },
  name: {
    flex: 1,
    textAlign: 'right',
    marginRight: '10px'
  }
})

@withStyles(styles)
@withStore('Session', { preload: false })
export default class DrawerHeader extends React.Component {

  static propTypes = {
    classes: PropTypes.object.isRequired
  }

  render() {
    const { classes, Session } = this.props
    return  <div className={classes.header}>
      <img className={classes.background} src="/static/login_bg.png"/>
      <div className={classes.top}>
        <Logo/>
        <div className={classes.name}>{Session.user.firstName}{" "}{Session.user.lastName}</div>
      </div>
      <div className={classes.bottom}></div>
    </div>
  }
}