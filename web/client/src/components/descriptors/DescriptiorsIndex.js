import React from 'react'
import { Link } from 'react-router-dom'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { loadDescriptors, deleteDescriptor } from 'actions'
import { Table, Header } from 'components'
import DescriptorTypes from './DescriptorTypes'

class Index extends React.Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    this.props.actions.loadDescriptors()
  }

  deleteDescriptor(id) {
    this.props.actions.deleteDescriptor(id)
  }

  render() {
    return (
      <div className="container">
        <Header>Список маркеров</Header>
        <p><Link to="/descriptors/new">Добавить</Link></p>
        <Table
          dataList={this.props.descriptor.list}
          headerColumns={[
            { type: 'id' },
            { tooltip: "Заголовок, Тип", text: <div>Заголовок<br />Тип</div> },
            "Текст",
            "Параметры",
            { text: "Действия", type: "actions" }
          ]}
          rowColumns={[
            { type: 'id' },
            item => <div>
              {item.title}
              <br />
              {(DescriptorTypes.find(t => t.value == item.type) || {text: '- (' + item.type + ')'}).text}
            </div>,
            item => item.text,
            item => JSON.stringify(item.params),
            {
              type: 'actions',
              actions: {
                edit: item => `/descriptors/${item.id}/edit`,
                remove: item => ({
                  onClick: () => this.deleteDescriptor(item.id)
                })
              }
            }
          ]}></Table>
      </div>
    )
  }
}

function mapStateToProps(state) {
  const props = { descriptor: state.Descriptor }
  return props
}

function mapDispatchToProps(dispatch) {
  const actions = { loadDescriptors, deleteDescriptor }
  const actionMap = { actions: bindActionCreators(actions, dispatch) }
  return actionMap
}

export default connect(mapStateToProps, mapDispatchToProps)(Index)
