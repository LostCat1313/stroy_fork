import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { EditComponent, TextField, ComboBox } from 'components'
import { createDescriptor, updateDescriptor } from 'actions'
import types from './DescriptorTypes'

class Edit extends EditComponent {
  constructor(props) {
    super(props, 'Descriptor')
    this.backUri = '/descriptors'
  }

  componentDidMount() {

  }

  prepareData(data) {
    return {
      ...data,
      // TODO: add json field
      params: JSON.parse(data.params)
    }
  }

  renderHeader() {
    let entry = this.getEntry()
    return entry
      ? "Редактирование маркера " + entry.id
      : "Новый маркер"
  }

  renderFields() {
    return [
      <ComboBox
        name="type"
        label="Тип"
        items={types}
      />,
      <TextField
        name="params"
        label="Параметры (Json)"
        multiLine={true}
        isJSON={true}
        required
      />,
      <TextField
        name="title"
        label="Заголовок"
        required
      />,
      <TextField
        name="text"
        label="Текст"
        multiLine={true}
        required
      />,
      <TextField
        name="docId"
        label="docId"
        required
      />,
      <TextField
        name="holderId"
        label="holderId"
        required
      />,
      <TextField
        name="holderType"
        label="holderType"
        required
      />
    ]
  }
}

function mapStateToProps(state) {
  const props = { Descriptor: state.Descriptor }
  return props
}

function mapDispatchToProps(dispatch) {
  const actions = { createDescriptor, updateDescriptor }
  const actionMap = { actions: bindActionCreators(actions, dispatch) }
  return actionMap
}


export default connect(mapStateToProps, mapDispatchToProps)(Edit)
