import React from 'react'
import { Switch, Route } from 'react-router-dom'

import GroupEdit from './groups/GroupEdit'
import GroupsIndex from './groups/GroupsIndex'

import RightEdit from './list/RightEdit'
import RightsIndex from './list/RightsIndex'

export default ({match: {url}}) => {
  return (
    <Switch>
      <Route exact path={`${url}/groups/`} component={GroupsIndex}/>
      <Route exact path={`${url}/groups/new`} component={GroupEdit} />
      <Route exact path={`${url}/groups/:id/edit`} component={GroupEdit} />
      <Route exact path={`${url}/list/`} component={RightsIndex}/>
      <Route exact path={`${url}/list/new`} component={RightEdit} />
      <Route exact path={`${url}/list/:id/edit`} component={RightEdit} />
    </Switch>
  )
}
