import React from 'react'
import { Link } from 'react-router-dom'

import { Table, Header } from 'components'
import { withStore } from 'decorators'
import RightsTabs from '../RightsTabs'

const tableActions = {
  remove: item => ({
    onClick: () => this.delete(item.id)
  })
};

@withStore('Right')
export default class GroupsIndex extends React.Component {
  constructor(props) {
    super(props);
  }

  delete(id) {
    this.props.actions.deleteGroup(id);
  }

  render() {
    let list = this.props.Right.list || [];
    return (
      <div className="container">
        <Header>Список правил</Header>
        <p><Link to="/rights/basic/new">Добавить</Link></p>
        <Table
          dataList={list.slice()}
          headerColumns={[{ type: 'id' }, "Тип", "Сущность", "Ссылка", "Операция", { text: "Действия", type: "actions" }]}
          rowColumns={[
            { type: 'id' },
            item => item.sign == 1 ? 'Разрешить' : 'Запретить',
            item => item.entity,
            item => item.refId,
            item => item.operation,
            {
              type: 'actions',
              actions: tableActions
            }
          ]}
        ></Table>
      </div>
    );
  }
}
