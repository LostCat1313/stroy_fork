import React from 'react';

import { EditComponent, ComboBox, TextField } from 'components';
import { withStore } from 'decorators';

@withStore('Session', { preload: false })
@withStore('Right', { actions: ['create'], preload: false })
export default class GroupEdit extends EditComponent {
  constructor(props) {
    super(props, 'Right');
    this.backUri = '/rights/basic';
  }

  renderHeader() {
    return "Новое правило";
  }

  renderFields() {
    const { config } = this.props.Session.rights;
    return [
      <ComboBox
        name="sign"
        label="Тип"
        items={[
          { value: '1', text: 'Разрешить' },
          { value: '0', text: 'Запретить' }
        ]}
      />,
      <ComboBox
        name="Сущность"
        label="Код"
        items={config.entities}
      />,
      <TextField
        name="refId"
        label="Ссылка (0 - глобально)"
      />,
      <ComboBox
        name="operation"
        label="Операция"
        items={config.operations}
      /> // groups/users list
    ];
  }
}
