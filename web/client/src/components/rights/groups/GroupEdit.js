import React from 'react'

import { EditComponent, TextField, LineBreak } from 'components'
import ChooseUsersField from 'components/common/fields/users/ChooseUsersField'
import ChooseItemsField from 'components/common/fields/ChooseItemsField'
import Utils from 'components/common/Utils'
import { withStore } from 'decorators'


@withStore(
  {name: 'Session', options: { preload:false }},
  {name: 'Group', options: { actions: ['create', 'update'], preload: false }}
)
export default class GroupEdit extends EditComponent {
  constructor(props) {
    super(props, 'Group')
    this.backUri = '/rights/groups'
    let entry = this.getEntry()
    if(entry && entry.users) {
      entry.userIds = entry.users.map(u => u.id)
      entry.rightOperations = entry.rights.map(r => r.operation) 
    }
  }

  componentDidMount() {
  }

  renderHeader() {
    let entry = this.getEntry()
    return entry
      ? "Редактирование группы " + entry.id
      : "Новая группа"
  }

  prepareData(data) {
    data.userIds = data.userIds.map(i => i.id);
    return data;
  }

  getOperations() {
    const { Session } = this.props;
    const operations = Session.config.rights.operations;
    return Object.keys(operations).reduce((result, ok) => {
      return result.concat(operations[ok].map(o => ok + '.' + o));
    }, [])
  }

  renderFields() {
    return [
      <TextField
        name="name"
        label="Имя"
        required
      />,
      <ChooseUsersField 
        name="userIds" 
        label="Пользователи"
        required
      />,
      <ChooseItemsField
        name="operations"
        label="Права"
        items={this.getOperations()}
        required
        />
    ]
  }
}
