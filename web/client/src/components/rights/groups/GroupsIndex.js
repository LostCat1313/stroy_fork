import React from 'react'
import { Link } from 'react-router-dom'

import { Table, Header } from 'components'
import { withStore } from 'decorators'
import RightsTabs from '../RightsTabs'

@withStore('Group')
export default class GroupsIndex extends React.Component {
  constructor(props) {
    super(props)
  }

  tableActions = {
    edit: item => '/rights/groups/' + item.id + '/edit',
    remove: item => ({
      onClick: () => this.delete(item.id)
    })
  };

  delete(id) {
    this.props.actions.deleteGroup(id)
  }

  render() {
    let list = this.props.Group.list || []
    return (
      <div className="container">
        <Header>Список групп</Header>
        <p><Link to="/rights/groups/new">Добавить</Link></p>
        <Table
          dataList={list.slice()}
          headerColumns={[{ type: 'id' }, "Имя", { text: "Действия", type: "actions" }]}
          rowColumns={[
            { type: 'id' },
            item => item.name,
            {
              type: 'actions',
              actions: this.tableActions
            }
          ]}
        ></Table>
      </div>
    )
  }
}
