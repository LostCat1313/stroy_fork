import React from 'react'
import { List, ListItem, ListItemAvatar, ListItemText, withStyles } from '@material-ui/core'
import NameThumb from 'components/common/NameThumb'
import Utils from 'components/common/Utils'

const styles = (theme) => ({
  root: {
    padding: 0
  }
})

@withStyles(styles)
export default class ChatContacts extends React.Component {

  onClick(uid) {
    if(this.props.onClick) {
      this.props.onClick(uid)
    }
  }

  render() {
    const { contacts, classes } = this.props
    if (contacts && contacts.length > 0) {
      return <List component="div" className={classes.root}>
        {contacts.map((item) => {
          const name = Utils.renderUserName(item)
          return <ListItem button key={item.id} onClick={()=>{this.onClick(item.id)}}>
            <ListItemAvatar>
              <NameThumb thumb={item.thumb} name={item.name}/>
            </ListItemAvatar>
            <ListItemText 
              primary={name}
              secondary={item.status}/>
          </ListItem>
        })}
      </List>
    }
    else {
      return <div className="not-found">Нет контактов</div>
    }
  }
}