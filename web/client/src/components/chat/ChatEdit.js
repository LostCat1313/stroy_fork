import React from 'react'
import { withStyles } from '@material-ui/core'
import PropTypes from 'prop-types'
import SendIcon from 'mdi-react/SendIcon'
import PaperclipIcon from 'mdi-react/PaperclipIcon'

import ChatTesterDialog from './ChatTesterDialog';

const styles = (theme) => ({
  root: {
    flex: '0 0 48px',
    borderTop: '1px solid #efefef',
    position: 'relative'
  },
  text: {
    position: 'absolute',
    right: '48px',
    bottom: '0px',
    left: '0px',
    top: '0px',
    background: 'white',
    '& textarea': {
      width: '100%',
      border: 'none',
      outline: 'none',
      fontSize: '14px',
      boxShadow: 'none',
      resize: 'none',
      padding: '5px',
    }
  },
  controls: {
    position: 'absolute',
    right: '0px',
    top: '0px',
    bottom: '0px',
    padding: '8px',
    display: 'flex',
    '& svg': {
      cursor: 'pointer',
    },
    '& path': {
      fill: theme.palette.primary.main
    }
  },
})

@withStyles(styles)
export default class ChatEdit extends React.Component {
  static propTypes = {
    classes:PropTypes.object.isRequired,
    attachFileClickHandler:PropTypes.func,
    onSend:PropTypes.func
  }

  state = {
    value:""
  }
  
  sendHandler = (e) => {
    this.fireSend(this.state.value)
    e.preventDefault()
    return false
  }

  attachFileClickHandler = (e) => {
    if(this.props.onFileAttach) {
      this.props.onFileAttach()
    }
    e.preventDefault()
    return false
  }

  keyDownHandler = (e) => {
    if(e.keyCode == 13 && (e.ctrlKey || e.metaKey)) {
      this.fireSend(this.state.value)
      e.preventDefault()
      return false
    }
  }

  fireSend(text) {
    this.setState({value:""})
    if(this.props.onSend) {
      this.props.onSend(text)
    }
  }

  changeHandler = (event) => {
    this.setState({value:event.target.value})
  }

  render() {
    const { classes } = this.props
    return <div className={classes.root}>
      <div className={classes.text}>
        <textarea 
          value={this.state.value}
          onChange={this.changeHandler}
          onKeyDown={this.keyDownHandler}></textarea>
      </div>
      <div className={classes.controls}>
        <ChatTesterDialog />
        <PaperclipIcon width="32" height="32" onClick={this.attachFileClickHandler}/>
        <SendIcon width="32" height="32" onClick={this.sendHandler}/>
      </div>
    </div>
  }
}