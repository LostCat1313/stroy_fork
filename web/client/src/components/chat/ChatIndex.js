import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'

import ChatRooms from './ChatRooms'
import Contacts from './ChatContacts'
import ChatHeader from './ChatHeader'
import ChatEdit from './ChatEdit'
import ChatMessages from './ChatMessages'
import CreateChatDialog from './CreateChatDialog'
import ChatControlPanel from './ChatControlPanel'
import { withStore } from 'decorators'
import Chat from './Chat'
import { Loading } from 'components';

const leftPanelWidth = '256px'

const styles = (theme) => ({
  root: {
    position: 'absolute',
    top: '64px',  // theme.panel.height
    left: '0px',
    right: '0px',
    bottom: '0px',
    overflow: 'hidden',
    display: 'flex'
  },
  leftPanel: {
    width: leftPanelWidth,
    borderRight: '1px solid #efefef'
  },
  rightPanel: {
    flex: '1 1',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center'
  },
  notFound: {
    alignSelf: 'center',
    textAlign: 'center',
    width: '100%'
  },
  itemsPanel: {
    overflow: 'auto',
    height: 'calc(100% - 56px)',
  }
})

@withStore('Session', { preload: false })
@withStyles(styles)
export default class ChatIndex extends React.Component {
  static propTypes = {
    Session: PropTypes.object.isRequired,
    classes: PropTypes.object.isRequired,
  }

  state = {
    rooms: [],
    contacts: [],
    currentRoom: null,
    messages: [],
    createChatDialogOpen: false, 
    panel: 'chats',
    loading: true,
    limit: 20
  }

  onStatusChange = ({ user: id, status }) => {
    console.log('Chat::onStatusChange ' + id + ' ' + status)
    this.setState(prevState => {
      prevState.rooms.forEach(r => {
        let user = r.users.find(u => u.id == id)
        if(user) user.status = status
      })
      let user = prevState.contacts.find(u => u.id == id)
      if(user) user.status = status
      if(prevState.currentRoom && prevState.currentRoom.users) {
        user = prevState.currentRoom.users.find(u => u.id == id)
        if(user) user.status = status
      }
      return {
        currentRoom: { ...prevState.currentRoom },
        rooms: prevState.rooms.slice(),
        contacts: prevState.contacts.slice()
      }
    })
  }

  onInit = ({ rooms, contacts }) => {
    console.log(rooms, rooms.reduce((acc, cur) => {
      return acc + cur.unviewedMessagesCount;
    }, 0));
    this.setState({ 
      rooms,
      contacts,
      loading:false
    });
  }

  componentDidMount() {
    Chat.initialize(this.props.Session.token);

    Chat.socket.on('init', this.onInit)
    Chat.socket.on('status', this.onStatusChange)
    Chat.socket.on('message', this.onMessage)
    Chat.socket.on('error', this.onError)
    Chat.socket.on('disconnect', this.onDisconnect)
  }

  componentWillUnmount() {
    Chat.destroy()
  }

  onError = (error) => {
    // todo: add popup
    alert(error)
  }

  onDisconnect = (reason) => {
    console.log("Chat::Disconnected", reason)
  }

  sendMessage(messageText) {
    const roomId = this.state.currentRoom.id
    const message = {
      roomId,
      content: messageText
    }
    if(messageText.indexOf('test attachment') == 0) {
      alert('test attachment')
      let test = messageText.replace('test attachment', '')
      message.attachments = this.generateAttachments(test)
    }
    console.log(message)
    Chat.socket.emit('message', message) 
  }

  generateAttachments(id) {
    return [{
      file: { id: id }
    }]
  }

  onUserClick = (uid) => {
    Chat.socket.emit('join', {
      userIds: [uid],
      limit: this.state.limit
    }, this.handleJoinResult)
  }

  onRoomClick = (room) => {
    console.log("Chat::onRoomClick: ", room)
    Chat.socket.emit('join', {
      roomId: room.id,
      limit: this.state.limit
    }, this.handleJoinResult)
  }

  onCreateRoom = (room) => {
    console.log("Chat::onCreateRoom: ", room)
    Chat.socket.emit('join', {
      ...room,
      limit: this.state.limit
    }, this.handleJoinResult)
  }

  handleJoinResult = (result) => {
    console.log("Chat::onRoomClick: cb ", result)
    if(!result.room) {
      console.log("Chat::onRoomClick: room is null")
      return
    }
    if(!this.state.currentRoom || result.room.id != this.state.currentRoom.id) {
      this.setState(prevState => {
        let rooms = prevState.rooms
        let index = rooms.findIndex(r => r.id == result.room.id)
        if(index < 0) {
          rooms = [result.room, ...rooms]
        }
        return {
          rooms,
          currentRoom: {
            ...result.room,
            messages: result.messages
          }
        }
      })
    }
  }

  onNewMessageSend = (text) => {
    this.sendMessage(text)
  }

  onChatDialogClose = () => {
    this.setState({ createChatDialogOpen: false })
  }

  onMessage = (message) => {
    console.log('new message ', message)
    if(!message.room) {
      console.warn('onMessage: no room in message body')
      return
    }
    if(this.state.currentRoom && message.room.id == this.state.currentRoom.id) {
      this.setState(prevState => {
        return {
          currentRoom: {
            ...prevState.currentRoom,
            lastMessage: message,
            messages: [...prevState.currentRoom.messages, message]
          }
        }
      })
    } 

    let roomIndex = this.state.rooms.findIndex(r => r.id == message.room.id)

    if(roomIndex >= 0) {
      const deleted = this.state.rooms.splice(roomIndex, 1)
      this.setState(prevState => {
        return {
          rooms: [
            {
              ...message.room,
              lastMessage: message,
              messages: [message],
              users: deleted[0].users
            },
            ...prevState.rooms
          ]
        }
      })
    } 
    else {
      // join room
      this.onRoomClick(message.room)
    }
  }

  onDeleteRoom = (item) => {
    console.log('ChatIndex::onDeleteRoom', item)
    if(!confirm('Подтвердите действие')) return;
    Chat.socket.emit('deleteRoom', {
      roomId: item.id
    }, result => {
      if(result && result.id) {
        this.setState(prevState => ({
          rooms:prevState.rooms.filter(room => room.id != result.id)
        }))
      }
    })
  }

  getCurrentChatMessages() {
    console.log('getcurerntchatmessages ' + this.state.currentChat)
    const page = this.state.messages.find(x => x.uid === this.state.currentChat)

    if(page) {
      console.log('page found')
      return page.messages
    } else {
      console.log('empty')
      return []
    }
  }

  panelChangeHandler = (e, panel) => {
    if(panel == 'newChat') {
      this.setState({ createChatDialogOpen: true })
    }
    else {
      this.setState({ panel })
    }
  }

  render() {
    const { panel, contacts, rooms, currentRoom, createChatDialogOpen, loading } = this.state
    const { classes, Session } = this.props
    return (
      <div className={classes.root}>
        { loading 
          ? <Loading cover/>
          : <Fragment> 
            <div className={classes.leftPanel}>
              <ChatControlPanel
                panel={panel}
                onChange={this.panelChangeHandler}
                />
              <div className={classes.itemsPanel}>
                {panel == "contacts" && <Contacts contacts={contacts} onClick={this.onUserClick} />}
                {panel == "chats" && <ChatRooms 
                  rooms={rooms} 
                  currentRoom={currentRoom} 
                  onClick={this.onRoomClick}
                  onDelete={this.onDeleteRoom}/>}
              </div>
            </div>
            <div className={classes.rightPanel}>
              {currentRoom && <ChatHeader user={Session.user} room={currentRoom}/>}
              {!currentRoom && <div className={classes.notFound}>Выберите чат, чтобы начать общение</div>}
              {currentRoom && <ChatMessages room={currentRoom} messages={currentRoom.messages}/>}
              {currentRoom && <ChatEdit onSend={this.onNewMessageSend}/>}
            </div>
            <CreateChatDialog
              open={createChatDialogOpen}
              contacts={contacts}
              onSubmit={this.onCreateRoom}
              onClose={this.onChatDialogClose}/>
          </Fragment> 
        }
      </div>
    )
  }
}
