import io from 'socket.io-client'

export default class Chat {
  static instance = null;
  
  static get socket() {
    return Chat.instance && Chat.instance.socket;
  }

  static initialize(token) {
    Chat.instance = new Chat(token);
  }

  static destroy() {
    Chat.instance.socket.disconnect();
    Chat.instance = null;
  }

  constructor(token) {
    this.socket = io('', {
      forceNew: true,
      query: { token }
    })
  }
}