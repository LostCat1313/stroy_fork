import React from 'react'
import PropTypes from 'prop-types'
import ContactsIcon from 'mdi-react/AccountBoxOutlineIcon'
import ForumIcon from 'mdi-react/ForumIcon'
import MessagePlusIcon from 'mdi-react/MessagePlusIcon'
import { withStyles, BottomNavigation, BottomNavigationAction } from '@material-ui/core'

const styles = (theme) => ({
  root: {
    width: '100%',
    background: '#efefef'
  },
  selectedItem: {
    paddingTop: '16px !important',
    '& path': {
      fill: theme.palette.primary.main
    }
  }
  
})

const navActions = {
  contacts: <ContactsIcon/>,
  chats: <ForumIcon/>,
  newChat: <MessagePlusIcon/>
}

@withStyles(styles)
export default class ChatControlPanel extends React.Component {
  static propTypes = {
    panel:PropTypes.string.isRequired,
    onChange:PropTypes.func,
    onAddChatClick:PropTypes.func,
    classes:PropTypes.object.isRequired,
  }

  renderAction(value, icon) {
    const { panel, classes } = this.props
    return <BottomNavigationAction key={value} className={panel == value ? classes.selectedItem : null} value={value} icon={icon}/>
  }

  render() {
    const { panel, onChange, classes } = this.props
    return <BottomNavigation value={panel} onChange={onChange} className={classes.root}>
      {['contacts', 'chats', 'newChat'].map(i => this.renderAction(i, navActions[i]))}
    </BottomNavigation>
  }
}
