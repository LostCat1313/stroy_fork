import React from 'react'
import { withStyles, ListItem, ListItemAvatar, ListItemText } from '@material-ui/core'
import PropTypes from 'prop-types'

import NameThumb from 'components/common/NameThumb'

const styles = (theme) => ({
  root: {
    flex: '0 0 56px',
    borderBottom: '1px solid #efefef',
    display: 'flex',
    alignItems: 'center',
    padding: '0px 16px'
  }
})

@withStyles(styles)
export default class ChatHeader extends React.Component {
  static propTypes = {
    classes:PropTypes.object.isRequired
  }

  renderAdditinalInfo(room, user) {
    if(room && room.users && user) {
      console.log("ChatHeader::renderAdditinalInfo", room)
      const users = room.users.filter(u => u.id != user.id)
      if(users.length == 1) return users[0].status == 'online' ? 'В сети': 'Не в сети' 
      else if(users.length > 0) {
        const online = users.filter(u => u.status == 'online')
        return online.length > 0 ? 
          `${online.length + 1} из ${room.users.length} пользователей в сети` :
          `${room.users.length} пользователей`
      }
    }
    return 'Статус неизвестен'
  }

  render() {
    const { classes, room, user } = this.props
    return <ListItem component="div" className={classes.root}>
      <ListItemAvatar>
        <NameThumb thumb={room.thumb} name={room.name}/>
      </ListItemAvatar>
      <ListItemText primary={room.name} secondary={this.renderAdditinalInfo(room, user)}/>
    </ListItem>
  }
}