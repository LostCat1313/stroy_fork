import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { List, ListItem, ListItemText, ListItemAvatar, withStyles } from '@material-ui/core'

import { Utils, NameThumb, ListItemControls, BaseComponent } from 'components'

const styles = (theme) => ({
  listItem: {
    position: 'relative'
  }
})

@withStyles(styles)
export default class ChatRooms extends BaseComponent {
  static propTypes = {
    classes:PropTypes.object.isRequired,
    rooms:PropTypes.array
  }

  controlsActionHandler = (item, action) => {
    this.fire(action, item)
  }

  render() {
    const { classes, rooms } = this.props
    return <List component="div" disablePadding className={classes.root}>
      {rooms && rooms.length > 0 ? rooms.map((item) => <ListItem
          key={item.id}
          button
          className={classes.listItem}
          onClick={this.onClick.bind(this, item, 'select')} 
          selected={this.isItemActive(item)}>
        <ListItemAvatar>
          <NameThumb thumb={item.thumb} name={item.name}/>
        </ListItemAvatar>
        <ListItemText 
          primary={item.name + (item.unviewedMessagesCount ? ` (${item.unviewedMessagesCount})` : '')}
          secondary={this.renderLastMessage(item)}/>
        <ListItemControls 
          deleteButton
          onAction={this.controlsActionHandler.bind(this, item)}
        />
      </ListItem>) 
      : <Fragment>Нет чатов</Fragment> }
    </List>
  }

  onClick(room, action) {
    if(this.props.onClick) {
      this.props.onClick(room, action)
    }
  }

  isItemActive(item) {
    return this.props.currentRoom && this.props.currentRoom.id == item.id
  }

  renderLastMessage(room) {
    if(room.lastMessage) {
      const user = Utils.findRoomUser(room, room.lastMessage.senderId)
      // console.log("renderLastMessage:", user)
      return <Fragment>
        {user && room.users.length > 2 && <span className="user-name">{Utils.renderUserName(user)}:{" "}</span>}
        {room.lastMessage.content}
      </Fragment>
    }
    return <Fragment>Нет сообщений</Fragment>
  }
}
