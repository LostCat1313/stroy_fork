import React from 'react'
import { Dialog, DialogTitle, DialogContent, withStyles } from '@material-ui/core'

import AppForm from 'components/common/AppForm'
import TextField from 'components/common/fields/TextField'
import ChooseItemsField from 'components/common/fields/ChooseItemsField'
import UserListItem from 'components/common/fields/users/UserListItem'
import ChoosenUsersRenderer from 'components/common/fields/users/ChoosenUsersRenderer'
import LineBreak from '../common/LineBreak';

const styles = (theme) => ({
  field: {
    width: '100%',
    marginBottom: theme.spacing.unit * 2
  }
})

@withStyles(styles)
export default class CreateChatDialog extends React.Component {

  handleClose = () => {
    if(this.props.onClose) this.props.onClose()
  }

  handleSubmit = (data) => {
    console.log('====FORM DATA====', data)
    if(this.props.onSubmit) this.props.onSubmit(this.prepareData(data))
    this.handleClose()
  }

  prepareData(data) {
    let result = { name: data.name, userIds:[] }
    if(data.contacts) {
      result.userIds = data.contacts.map(i => i.id)
    }
    return result
  }

  render() {
    const { classes, contacts, open } = this.props
    return (
      <Dialog
        modal={true}
        open={open}>
        <DialogTitle>Создать новый чат</DialogTitle>
        <DialogContent>
          <AppForm 
            onCancel={this.handleClose}
            onValidSubmit={this.handleSubmit}
            ref="appForm">
            <TextField 
              className={classes.field}
              label="Название чата" 
              name="name"
              required/>
            <LineBreak/>
            <ChooseItemsField
              className={classes.field}
              items={contacts}
              itemRenderer={UserListItem}
              choosenItemsRenderer={ChoosenUsersRenderer} 
              name="contacts"
              required/>
          </AppForm>
        </DialogContent>
      </Dialog>
    )
  }
}