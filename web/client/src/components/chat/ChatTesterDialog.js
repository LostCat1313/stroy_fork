import React, { Component, Fragment } from 'react';
import { Button, Dialog, DialogTitle, DialogContent, withStyles, TextField } from '@material-ui/core';

import Chat from './Chat';

const styles = (theme) => ({
  button: {
    // position: 'fixed',
    // bottom: '20px',
    // right: '20px'
  }
})

@withStyles(styles)
export default class ChatTesterDialog extends Component {
  state = {
    open: false,
    value: ''
  }

  handleClick = open => () => {
    this.setState({open});
  }
  handleChange = e => {
    this.setState({value: e.target.value})
  }
  handleButtonClick = () => {
    try {
      Chat.socket.emit('update', JSON.parse(this.state.value))
    } catch(err) {
      alert(err)
    }
  }

  render() {
    const { classes } = this.props;
    const { open } = this.state;
    return <Fragment>
      <Button
        className={classes.button}
        onClick={this.handleClick(true)}
        variant="contained"
        color="primary"
      >Tst</Button>
      <Dialog
        onClose={this.handleClick(false)}
        modal={true}
        maxWidth="md"
        fullWidth
        open={open}>
        <DialogTitle>Тест чата</DialogTitle>
        <DialogContent>
          <TextField
            onChange={this.handleChange}
            value={this.state.value}
          ></TextField>
          <Button
            onClick={this.handleButtonClick}
          >Send</Button>
        </DialogContent>
      </Dialog>
    </Fragment>
  }
}