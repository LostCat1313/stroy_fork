import React from 'react'
import { withStyles, List, ListItem, ListItemText, ListItemAvatar } from '@material-ui/core'
import PropTypes from 'prop-types'

import NameThumb from 'components/common/NameThumb'
import Utils from 'components/common/Utils'

const styles = (theme) => ({
  root: {
    flex: '1 1',
    overflow: 'auto',
    background: 'white',
    display: 'flex',
    alignItems: 'center'
  },
  notFound: {
    width: '100%',
    display: 'grid-cell',
    verticalAlign: 'middle',
    textAlign: 'center'
  },
  secondaryText: {
    paddingLeft: '52px !important'
  }
})

@withStyles(styles)
export default class ChatMessages extends React.Component {
  static propTypes = {
    classes:PropTypes.object.isRequired
  }

  componentDidUpdate() {
    let { container } = this.refs
    let body = container.children[0]
    if(container) {
      if(this.props.messages && this.props.messages.length > 0) {
        if(body.offsetHeight < container.offsetHeight) {
          container.style.overflow = 'hidden'
          container.style.paddingTop = (container.offsetHeight - body.offsetHeight) + 'px'
        } else {
          container.style.overflow = 'auto'
          container.style.paddingTop = '0px'
        }
        container.scrollTop = container.scrollHeight
      }
      else {
        container.style.overflow = null
        container.style.paddingTop = null
      }
    }
  }

  renderMessage(m, index) {
    const user = Utils.findRoomUser(this.props.room, m.senderId)
    const { classes } = this.props
    if(user) {
      const prevId = (index > 0) ? this.props.messages[index-1].senderId : -1
      const name = Utils.renderUserName(user)
      return prevId != m.senderId ? 
        <ListItem key={index}>
          <ListItemAvatar>
            <NameThumb name={name}/>
          </ListItemAvatar>
          <ListItemText primary={name} secondary={m.content}/>
        </ListItem> : 
        <ListItem key={index}>
          <ListItemText classes={{secondary:classes.secondaryText}} secondary={m.content}/>
        </ListItem>
    }
    return null
  }

  render() {
    const { classes, messages } = this.props
    return <div className={classes.root} ref="container">
      {!messages || messages.length == 0 ? <div className={classes.notFound}>В данном чате пока нет сообщений</div> : <List dense>
        {messages.map((m, index) => this.renderMessage(m, index))}
      </List>}
    </div>
  }
}