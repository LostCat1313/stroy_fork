import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { EditComponent, TextField } from 'components'
import { createOrganization, updateOrganization } from 'actions'
function mapStateToProps(state) {
  const props = { Organization: state.Organization }
  return props
}

function mapDispatchToProps(dispatch) {
  console.log({ createOrganization, updateOrganization })
  const actions = { createOrganization, updateOrganization }
  const actionMap = { actions: bindActionCreators(actions, dispatch) }
  return actionMap
}

@connect(mapStateToProps, mapDispatchToProps)
export default class OrganizationEdit extends EditComponent {
    constructor(props) {
      super(props, 'Organization')
      this.backUri = '/organizations'
      console.log(this.props.actions)
    }

    renderHeader() {
      let entry = this.getEntry()
      return entry
        ? "Редактирование организации " + entry.name
        : "Создание организации"
    }

    renderFields() {
        return [
          <TextField
            name="name"
            label="Имя"
            required
          />
        ]
    }
}
