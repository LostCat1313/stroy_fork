import React from 'react'
import { Switch, Route } from 'react-router-dom'

import OrganizationIndex from './OrganizationIndex'
import OrganizationEdit from './OrganizationEdit'
import OrganizationUsersIndex from './OrganizationUsersIndex'
import OrganizationUsersEdit from './OrganizationUsersEdit'

export default ({match: {url}}) => {
  return (
    <Switch>
      <Route exact path={url} component={OrganizationIndex}/>
      <Route exact
            path={`${url}/new`}
            component={OrganizationEdit} />
      <Route exact
            path={`${url}/:id/edit`}
            component={OrganizationEdit} />
      <Route exact
            path={`${url}/:id/users`}
            component={OrganizationUsersIndex} />
      <Route exact
            path={`${url}/:id/users/edit`}
            component={OrganizationUsersEdit} />
    </Switch>
  )
}