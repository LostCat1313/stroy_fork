import React from 'react';
import { Button } from '@material-ui/core';
import Formsy from 'formsy-react';

import { Table, Utils, CheckBox, Header, Loading } from 'components';
import { loadUsers, loadOrganizationUsers, saveOrganizationUsers } from 'actions';
import { withStore } from 'decorators';

@withStore({
  name: 'User',
  preload: false,
  actions: { loadUsers, loadOrganizationUsers, saveOrganizationUsers }
}, {
  name: 'Organization',
  preload: false
})
export default class OrganizationUsersEdit extends React.Component {
  
  formsy = React.createRef();
  state =  {
    loading: false
  };

  constructor(props) {
    super(props)
    this.checkboxes = {}
  }

  componentDidMount() {
    const { actions, match } = this.props;
    this.setState({ loading: true });
    actions.loadOrganizationUsers(match.params.id).then(() => {
      actions.loadUsers().then(() => {
        this.setState({ loading: false });
      });
    });
  }

  getName() {
    const { Organization, match } = this.props;
    if(Organization.list) {
      var current = Organization.list.find(v => v.id == match.params.id)
      if(current) {
        return current.name
      }
    }
    return ""
  }

  isParticipant(user) {
    const { Organization } = this.props;

    if (Organization.users) {
      return !!Organization.users.find(u => u.id == user.id)
    }

    return false
  }

  onSaveClick = (event) => {
    this.formsy.current.submit();
    event.stopPropagation()
  }

  onCancelClick = e => {
    this.props.history.push('/organizations/' + this.props.match.params.id + '/users');
  }

  handleSubmit = (model) => {
    const { actions, history, match } = this.props;    
    let result = [];
    for(let key in model.selected) {
      if(model.selected[key]) {
        result.push(+key)
      }
    }
    actions.saveOrganizationUsers(match.params.id, result)
      .then((v) => {
        if (v.status === 'success') {
          history.push('/organizations/' + match.params.id + '/users')
        }
      }, (e) => {
        // TODO: add toasts to show status of commands
        console.log(e)
      })
    event.stopPropagation()
  }

  render() {
    const { loading } = this.state;
    
    let list = this.props.User.list 
      ? this.props.User.list.map(u=> ({...u, selected: this.isParticipant(u)}))
      : null

    return (
      <div className="container">
        <Header>Участники организации: {this.getName()}</Header>
        <Button onClick={this.onSaveClick}>Сохранить</Button>
        <Button onClick={this.onCancelClick}>Отмена</Button>
        { loading 
          ? <Loading center/>
          : <Formsy 
            disabled={this.state.disabled}
            onValidSubmit={this.handleSubmit}
            ref={this.formsy}>
            <Table
              dataList={list}
              showCheckboxes
              idsChanged={(ids) => {
                this.ids = ids
              }}
              headerColumns={
                [{text: ' ', style:{width: '40px'}}, "Пользователь"]
              }
              rowColumns={[
                user => <CheckBox defaultValue={user.selected} name={'selected.' + user.id}/>,
                user => user.name
              ]}
              isRowSelected={this.isParticipant.bind(this)}
            ></Table>
          </Formsy> }
      </div>
    )
  }
}
