import React from 'react';
import { Link } from 'react-router-dom';
import { Button } from '@material-ui/core';

import { Table, Loading, Header } from 'components';
import { loadOrganizations, loadOrganizationUsers } from 'actions';
import { withStore } from 'decorators';

@withStore('Organization', { preload: false, actions: { loadOrganizations, loadOrganizationUsers } })
export default class OrganizationUsersIndex extends React.Component {
  
  state = { 
    users: [], 
    organization: null 
  }

  componentWillMount() {
    const { actions, match } = this.props;
    Promise.all([actions.loadOrganizations(),
      actions.loadOrganizationUsers(match.params.id)]).then(() => {
        this.setState({ loading: false })
      });
  }

  getName() {
    const { Organization, match } = this.props;

    if (Organization.list) {
      var current = Organization.list.find(v => v.id == match.params.id)
      if (current) {
        return current.name
      }
    }
    return ""
  }

  render() {
    const { Organization, match } = this.props;
    const { loading } = this.state;

    return loading ? <Loading center/> : 
      <div className="container">
        <Header>Участники организации: {this.getName()}</Header>
        <Button component={Link} to={'/organizations/' + match.params.id + '/users/edit'}>
          Изменить
        </Button>
        <Button component={Link} to={'/organizations/'}>
          Назад
        </Button>
        <Table
          dataList={Organization.users}
          headerColumns={["Имя"]}
          rowColumns={[user => user.name]}></Table>
      </div>
  }
}
