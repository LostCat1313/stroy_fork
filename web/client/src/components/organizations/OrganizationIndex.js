import React from 'react'
import { Link } from 'react-router-dom'

import { Table, Header } from 'components';
import { withStore } from 'decorators';
import { loadOrganizations, deleteOrganization } from 'actions'

@withStore('Organization', { actions: { loadOrganizations, deleteOrganization }})
export default class OrganizationIndex extends React.Component {
 
  deleteOrganization = (id) => {
    this.props.actions.deleteOrganization(id)
  };

  tableActions = {
    edit: org => `/organizations/${org.id}/edit`,
    remove: org => ({
      onClick: () => this.deleteOrganization(org.id)
    }),
    users: org => `/organizations/${org.id}/users`
  }

  render() {
    return (
      <div className="container">
        <Header>Список организаций</Header>
        <p><Link to="/organizations/new">Добавить</Link></p>
        <Table
          dataList={this.props.Organization.list}
          headerColumns={[
            { type: 'id' },
            "Имя",
            { text: "Действия", type: "actions" }
          ]}
          rowColumns={[
            { type: 'id' },
            org => org.name,
            {
              type: 'actions',
              actions: this.tableActions
            }
          ]}></Table>
      </div>
    )
  }
}
