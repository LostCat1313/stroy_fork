import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import Formsy from 'formsy-react';
import { Typography, Button, withStyles } from '@material-ui/core';

import Loading from 'components/common/Loading';
import TextField from 'components/common/fields/TextField';
import OrganizationComboBox from 'components/common/fields/organization/OrganizationsComboBox';
import { registerUpdate } from 'actions';
import { withStore } from 'decorators';

const styles = theme => ({
  root: {
    height: '100%',
    margin: 'auto',
    width: 500,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textField: {
    marginBottom: theme.spacing.unit,
    width: 300,
  },
  form: {
    display: 'flex',
    flexWrap: 'wrap',
    flexDirection: 'column',
    alignItems: 'center',
    width: '250px'
  }
});

@withStyles(styles)
@withStore('Session', {preload: false, actions: { registerUpdate }})
export default class Registration extends Component {
  state = {
    canSubmit:false,
    notFoundOrganization:false,
    loading: false,
  }

  constructor(props) {
    super(props);

    if(!this.props.Session.token || this.props.Session.authenticated) {
      this.props.history.push('/');
    }
  }

  disableButton = () => {
    this.setState({ canSubmit: false });
  }

  enableButton = () => {
    this.setState({ canSubmit: true });
  }

  submit = (model) => {
    console.log(model)
    this.setState({ canSubmit: false, loading: true });
    this.props.actions.registerUpdate(model).then(result => {
      if(result.status === 'success') {
        this.props.history.push('/');
      }
      else {
        this.setState({ canSubmit: true, loading: false, error: result.error });
      }
    });
  }

  toggleOrganization = e => {
    this.setState(prevState => ({notFoundOrganization: !prevState.notFoundOrganization}));
    e.preventDefault();
    return false;
  }

  render() {
    const { classes } = this.props;
    const { notFoundOrganization, canSubmit } = this.state;
    return (
      <div className={classes.root}>
        <Typography variant='h2'>Регистрация</Typography>
        <Formsy 
          onValidSubmit={this.submit}
          onValid={this.enableButton}
          onInvalid={this.disableButton}
          className={classes.form}>
          <TextField
            name="firstName"
            required
            label="Имя"/>
          <TextField 
            name="lastName"
            required
            label="Фамилия"/>
          <TextField 
            name="secondName"
            label="Отчество"/>

          { notFoundOrganization
            ? <Fragment>
                <TextField 
                  name="organizationName"
                  required
                  label="Имя новой организации"/>
                <a href="#" onClick={this.toggleOrganization}>Выбрать из списка</a>
              </Fragment>
            : <Fragment>
                <OrganizationComboBox
                  label="Организация"
                  required
                  name="organizationId"/>
                <a href="#" onClick={this.toggleOrganization}>Не нашли организацию?</a>
              </Fragment> }
              { this.state.loading  && <Loading/> }
          <div>
            <Button color="primary" disabled={!canSubmit} type="submit">Продолжить</Button>
            {/* <Button href="#" color="primary">Пропустить</Button> */}
          </div>
        </Formsy>
      </div>
    );
  }
}
