import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Formsy from 'formsy-react';
import compose from 'recompose/compose';
import { Typography, Button, withStyles } from '@material-ui/core';
import { withRouter } from 'react-router-dom';

import PhoneNumberField from 'components/common/fields/PhoneNumberField'
import TextField from 'components/common/fields/TextField';
import PasswordField from 'components/common/fields/PasswordField';
import Loading from 'components/common/Loading';
import withStore from 'decorators/withStore';
import { register } from 'actions';

@withStyles(theme => ({
  root: {
    height: '100%',
    margin: 'auto',
    width: 500,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textField: {
    marginBottom: theme.spacing.unit,
    width: 300,
  },
  form: {
    display: 'flex',
    flexWrap: 'wrap',
    flexDirection: 'column',
    alignItems: 'center',
  }
}))
@withStore({actions: { register }, preload: false})
export default class Registration extends Component {
  state = {
    canSubmit:false
  }

  disableButton = () => {
    this.setState({ canSubmit: false });
  }

  enableButton = () => {
    this.setState({ canSubmit: true });
  }

  submit = (model) => {
    console.log(model)
    this.setState({ canSubmit: false, loading: true });
    this.props.actions.register(model.phone).then((result) => {
      if(!result.error) {
        this.props.history.push('/registration/activate');
      }
      else {
        this.setState({ canSubmit: true, loading: false, error: result.error });
      }
    });
  }

  render() {
    const { classes } = this.props
    return (
      <div className={classes.root}>
        <Typography variant='h2'>Регистрация</Typography>
        <Formsy 
          onValidSubmit={this.submit.bind(this)}
          onValid={this.enableButton}
          onInvalid={this.disableButton}
          className={classes.form}>
          <PhoneNumberField
            fullWidth
            required
            label="Телефон"
            name="phone"
            error={this.state.error}
          />
          { this.state.loading  && <Loading/> }
          <div>
            <Button color="primary" disabled={!this.state.canSubmit} type="submit">Зарегистрироваться</Button>
            <Button href="#" color="primary">Назад</Button>
          </div>
        </Formsy>
      </div>
    );
  }
}
