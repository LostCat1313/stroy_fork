import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Formsy from 'formsy-react';
import compose from 'recompose/compose';
import { Typography, Button, withStyles } from '@material-ui/core';

import PhoneNumberField from 'components/common/fields/PhoneNumberField'
import TextField from 'components/common/fields/TextField';
import PasswordField from 'components/common/fields/PasswordField';
import Loading from 'components/common/Loading';
import { withStore } from 'decorators';
import { activate } from 'actions';

@withStyles(theme => ({
  root: {
    height: '100%',
    margin: 'auto',
    width: 500,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textField: {
    marginBottom: theme.spacing.unit,
    width: 300,
  },
  form: {
    display: 'flex',
    flexWrap: 'wrap',
    flexDirection: 'column',
    alignItems: 'center',
  }
}))
@withStore('Session', {preload: false, actions: {activate}})
export default class Registration extends Component {
  state = {
    canSubmit:false
  }

  constructor(props) {
    super(props);
    if(!this.props.Session.phone) {
      this.props.history.push('/');
    }
  }

  disableButton = () => {
    this.setState({ canSubmit: false });
  }

  enableButton = () => {
    this.setState({ canSubmit: true });
  }

  submit = (model) => {
    console.log(this.props.Session.phone, model.code);
    this.setState({loading: true, canSubmit: false})
    this.props.actions.activate(this.props.Session.phone, model.code).then(result => {
      if(!result.error) {
        if(result.response.signup) {
          this.props.history.push('/registration/update');
        }
        else {
          this.props.history.push('/');
        }
      }
      else {
        this.setState({loading: false, canSubmit: true, error: result.error})
      }
    });
  }

  render() {
    const { classes, Session } = this.props
    return (
      <div className={classes.root}>
        <Typography variant='h2'>Активация</Typography>
        <Formsy 
          onValidSubmit={this.submit.bind(this)}
          onValid={this.enableButton}
          onInvalid={this.disableButton}
          className={classes.form}>
          <Typography>Телефон: {Session.phone}</Typography>
          <TextField
            centered
            name="code"
            required
            error={this.state.error}
            label="Код активации"/>
          { this.state.loading  && <Loading/> }
          <div>
            <Button color="primary" disabled={!this.state.canSubmit} type="submit">Активировать</Button>
            <Button href="#" color="primary">Отмена</Button>
          </div>
        </Formsy>
      </div>
    );
  }
}
