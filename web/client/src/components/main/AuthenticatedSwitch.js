import React from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'

import MainPage from './MainPage'
import NotFound from './NotFound'
import Routes from './Routes'
import UsersSwitch from '../users/Switch'
import ContactsSwitch from '../users/UserContacts'
import NotesSwitch from '../notes/Switch'
import RegIdsSwitch from '../regids/Switch'
import TasksSwitch from '../tasks/Switch'
import PositionsSwitch from '../positions/Switch'
import DescriptorsSwitch from '../descriptors/Switch'
import ProjectsSwitch from '../projects/Switch'
import OrganizationsSwitch from '../organizations/Switch'
import FilesIndex from '../files/FileListIndex'
import RightsSwitch from '../rights/RightsSwitch'
import DeepLinksSwitch from '../deeplinks/DeepLinksSwitch'
import DocumentViewerIndex from '../documents/viewer/DocumentViewerIndex'
import LastDocuments from '../documents/LastDocuments'
import Chat from '../chat/ChatIndex'

export default () => <Switch>
    <Route exact path="/" component={MainPage}/>
    <Route exact path="/routes" component={Routes}/>
    <Route path="/users" component={UsersSwitch} />
    <Route path="/contacts" component={ContactsSwitch} />
    <Route path="/notes" component={NotesSwitch} />
    <Route path="/regids" component={RegIdsSwitch} />
    <Route path="/tasks" component={TasksSwitch} />
    <Route path="/rights" component={RightsSwitch} />
    <Route path="/descriptors" component={DescriptorsSwitch} />
    <Route path="/lastdocuments" component={LastDocuments} />
    <Route path="/projects" component={ProjectsSwitch}/>
    <Route path="/positions" component={PositionsSwitch}/>
    <Route path="/organizations" component={OrganizationsSwitch} />
    <Route path="/deeplinks" component={DeepLinksSwitch}/>
    <Route path="/files" component={FilesIndex}/>
    <Route exact path="/chat" component={Chat} />
    <Route exact
          path="/documents/:id"
          component={DocumentViewerIndex} />
    <Route exact path='/404' component={NotFound}/>
    <Redirect from='*' to='/404'/>
  </Switch>
