import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import Login from './Login';
import Registration from './Registration';
import RegistrationActivate from './RegistrationActivate';
import RegistrationUpdate from './RegistrationUpdate';
import Routes from './Routes';

export default () => <Switch>
    <Route exact path="/" component={Login}/>
    <Route exact path="/registration" component={Registration}/>
    <Route exact path="/registration/activate" component={RegistrationActivate}/>
    <Route exact path="/registration/update" component={RegistrationUpdate}/>
    <Route exact path="/routes" component={Routes}/>
    <Redirect from='*' to='/'/>
  </Switch>;
