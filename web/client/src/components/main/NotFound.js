import React from 'react'

export default class NotFound extends React.Component {
  render() {
    return (
      <div>
        <h1>Страница не найдена</h1>
      </div>
    )
  }
}
