import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Button, TextField, withStyles, Typography, Switch, FormControlLabel, FormGroup } from '@material-ui/core'
import PropTypes from 'prop-types'

import { login } from 'actions'
import Logo from 'components/common/Logo'

const mapStateToProps = (state) => ({ session: state.Session })
const bindActions = (dispatch) => ({ actions: bindActionCreators({ 
  login 
}, dispatch)})

const styles = theme => ({
  root: {
    height: '100%',
    margin: 'auto',
    width: 500,
    display: 'flex',
    flexWrap: 'wrap',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  header: {
    display: 'flex',
    justifyContent: 'center',
  },
  headerText: {
    lineHeight: '100px',
    marginLeft:  theme.spacing.unit,
  },
  textField: {
    marginBottom: theme.spacing.unit,
    width: 300,
  },
  dense: {
    marginTop: 19,
  },
  form: {
    display: 'flex',
    flexWrap: 'wrap',
    flexDirection: 'column',
    alignItems: 'center',
  }
})

@withStyles(styles)
@connect(mapStateToProps, bindActions)
export default class Login extends React.Component {
    state = {
      username: '',
      password: '',
      saveSession: true
    }

    static propTypes = {
      classes: PropTypes.object.isRequired,
    };

    componentWillMount() {
        this.setState({
            username: this.props.session.name
        });
    }

    handleSubmit = (e) => {
        this.props.actions.login(this.state.username, this.state.password);
        e.preventDefault();
        return false;
    };

    handleInputChange = (event) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    render() {
        // const { from } = this.props.location.state || { from: { pathname: '/' } }
        // const { redirectToReferrer } = this.state
        const { classes } = this.props;
        return (
          <div className={classes.root}>
            <div className={classes.header}>
              <Logo/>
              <Typography variant='h2' className={classes.headerText}>Аксиома</Typography>
            </div>
            <form onSubmit={this.handleSubmit}>
              <FormGroup className={classes.form}>
                <TextField
                    type='text'
                    placeholder='Имя'
                    name='username'
                    fullWidth
                    inputProps={{style:{textAlign:'center'}}}
                    className={classes.textField}
                    value={this.state.username}
                    onChange={this.handleInputChange} />
                <TextField
                    type='password'
                    placeholder='Пароль'
                    name='password'
                    fullWidth
                    inputProps={{style:{textAlign:'center'}}}
                    className={classes.textField}
                    value={this.state.password}
                    onChange={this.handleInputChange}  />
                <FormControlLabel
                  control={
                    <Switch
                      name='saveSession'
                      checked={this.state.saveSession}
                      onChange={this.handleInputChange}
                      label='Запомнить'/>
                  }
                  label='Запомнить'
                />
                <div>
                  <Button 
                    color='primary' 
                    className={classes.button}
                    type="submit">Войти</Button>
                  <Button 
                    color='primary'
                    href="#/registration/"
                    className={classes.button}>Регистрация</Button>
                </div>
              </FormGroup>
            </form>
          </div>
        )
    }
}
