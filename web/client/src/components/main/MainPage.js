import React from 'react';
import { Card, CardHeader, Grid, Typography, withStyles } from '@material-ui/core';


import { verifyTokenReg, loadNotifications } from 'actions';
import { withStore } from 'decorators';
import ToggledBottomContent from '../common/ToggledBottomContent';
import NotificationCard from '../common/NotificationCard';

@withStore(
 { name: 'Session', options: { preload: false }},
 { name: 'Notification',
   options: { 
    actions: {
      verifyTokenReg,
      loadNotifications
    }
  } 
})
@withStyles(theme => ({
  header: {
    marginBottom: theme.spacing.unit * 2
  }
}))
export default class MainPage extends React.Component {
  componentDidMount() {
    console.log(this.props.Session)
  }



  /**'.fileNotification': generateNotificationStyle(variables, '#FF9800'),
      '.messageNotification': generateNotificationStyle(variables, '#4CAF50'),
      '.remarkNotification': generateNotificationStyle(variables,  '#8BC34A'),
      '.projectCard': generateNotificationStyle(variables,  '#ccc'), */

  render() {
    const { Notification, classes } = this.props;
    return (
      <div className="container">
        <Typography variant="subtitle1" className={classes.header}>Уведомления</Typography>
        { Notification.list && Notification.list.length > 0 ? <ToggledBottomContent collapsedHeight="372px">
          <Grid container spacing={16}>
            {Notification.list.map(n => {
              return <Grid key={n.id} item md={4} sm={3}>
                <NotificationCard item={n}/>
              </Grid>
            })}
          </Grid>
        </ToggledBottomContent> : <Typography align="center" variant="body2">Нет уведомлений</Typography>}
      </div>
    )
  }
}
