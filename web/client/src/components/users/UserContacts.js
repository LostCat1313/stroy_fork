import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { Table, Header } from 'components'
import { loadContacts } from 'actions'

const mapStateToProps = state => ({ user: state.User })
const mapDispatchToProps = (dispatch) => ({ actions: bindActionCreators({ loadContacts }, dispatch) })

@connect(mapStateToProps, mapDispatchToProps)
export default class UserContacts extends React.Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    this.props.actions.loadContacts()
  }

  formatFullName(user) {
    return `${user.firstName ? user.firstName : '-'} ${user.secondName ? user.secondName : '' } ${user.lastName ? user.lastName : ''}`
  }

  render() {
    return (
      <div className="container">
        <Header>Список контактов</Header>
        {/* <p><Link to="/users/new">Добавить</Link></p> */}
        <Table
          dataList={this.props.user.contacts}
          headerColumns={[
            {type: 'id'},
            { tooltip: "Имя",
            text: <div>Имя<br/>ФИО</div> },
            "Телефон",
            "Должность",
            { text: "Действия", type: "actions" }
          ]}
          rowColumns={[
            { type: 'id' },
            user => <div>{user.name}<br/>{this.formatFullName(user)}</div>,
            user => user.phoneNumber,
            user => user.Position ? user.Position.name : '-',
            {
              type: 'actions',
              actions: {
                // edit: user => `/users/${user.id}/edit`,
                // remove: user => ({
                //   onClick: () => this.deleteUser(user.id)
                // })
              }
            }
          ]}
        ></Table>
      </div>
    )
  }
}
