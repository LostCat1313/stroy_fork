import React from 'react'
import { Link } from 'react-router-dom'
import { Table } from 'components'
import { Loading } from 'components'
import { withStore } from 'decorators'

@withStore('User')
export default class UsersIndex extends React.Component {
  constructor(props) {
    super(props)
  }

  deleteUser(id) {
    this.props.actions.deleteUser(id)
  }

  formatFullName(user) {
    return `${user.firstName ? user.firstName : '-'} ${user.secondName ? user.secondName : '' } ${user.lastName ? user.lastName : ''}`
  }

  render() {
    return (
      <div className="container">
        <h1>Список пользователей</h1>
        <p><Link to="/users/new">Добавить</Link></p>
        <Table
          dataList={this.props.User.list}
          headerColumns={[
            {type: 'id'},
            { tooltip: "Имя",
            text: <div>Имя<br/>ФИО</div> },
            "Телефон",
            "Должность",
            { text: "Действия", type: "actions" }
          ]}
          rowColumns={[
            { type: 'id' },
            user => <div>{user.name}<br/>{this.formatFullName(user)}</div>,
            user => user.phoneNumber,
            user => user.Position ? user.Position.name : '-',
            {
              type: 'actions',
              actions: {
                edit: user => `/users/${user.id}/edit`,
                remove: user => ({
                  onClick: () => this.deleteUser(user.id)
                })
              }
            }
          ]}
        ></Table>
      </div>
    )
  }
}
