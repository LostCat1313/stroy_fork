import React from 'react';

import { EditComponent, TextField, PasswordField, ChooseItemsField, CheckBox, LineBreak } from 'components';
import PositionComboBox from 'components/common/fields/positions/PositionsComboBox';
import GroupsComboBox from 'components/common/fields/groups/GroupsComboBox';
import { Utils, Header } from 'components';
import { withStore } from 'decorators';

@withStore({ 
  name: 'User', options: { actions: ['create', 'update'], preloadIfNull: true }
}, {
  name: 'Session', options: { preload: false }
})
export default class UserEdit extends EditComponent {
  constructor(props) {
    super(props, 'User');
    this.backUri = '/users';
  }

  renderHeader() {
    let entry = this.getEntry();
    return entry
      ? "Редактирование пользователя " + entry.name
      : "Новый пользователь";
  }

  renderFields() {
    let entry = this.getEntry()
    return [
      <TextField
        name="name"
        label="Логин"
        required
      />,
      <TextField
        name="email"
        label="Email"
      />,
      <TextField
        name="phoneNumber"
        label="Телефон"
        required
      />,
      <TextField
        name="firstName"
        label="Имя"
        required
      />,
      <TextField
        name="lastName"
        label="Фамилия"
        required
      />,
      <TextField
        name="secondName"
        label="Отчество"
      />,
      <PositionComboBox
        name="positionId"
        label="Должность"
      />,
      // <ChooseItemsField
      //   name="rightCodes"
      //   label="Права"
      //   items={Utils.getAvailableRights()}
      //   />,
      <CheckBox
        name="isActive"
        label="Активирован"
      />,
      <CheckBox
        name="isAdmin"
        label="Администратор"
      />,
      <PasswordField name="password" required={!entry}/>
    ];
  }

  render() {
    let entry = this.getEntry()
    return (
      <div className="container">
        <Header>{this.renderHeader()}</Header>
        {entry && entry.regIds && entry.regIds.length > 0 && 
        <div><p>RegIds:</p>
          <ul>
            {entry.regIds.map(i => <li key={i.id}>{i.regId}</li>)}
          </ul>
        </div>}
        {this.renderForm()}
      </div>
    );
  }
}
