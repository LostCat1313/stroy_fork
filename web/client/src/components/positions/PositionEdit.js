import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { EditComponent, TextField } from 'components'
import { createPosition, updatePosition } from 'actions'
import { withStore } from 'decorators'

@withStore('Position', { preload: false, actions: { createPosition, updatePosition } })
export default class PositionEdit extends EditComponent {
    constructor(props) {
      super(props, 'Position')
      this.backUri = '/positions'
      console.log(this.props.actions)
    }

    renderHeader() {
      let entry = this.getEntry()
      return entry
        ? "Редактирование должности " + entry.name
        : "Создание должности"
    }

    renderFields() {
        return [
          <TextField
            name="name"
            label="Имя"
            required
          />
        ]
    }
}
