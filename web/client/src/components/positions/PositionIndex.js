import React from 'react'
import { Link } from 'react-router-dom'

import { Table } from 'components'
import { withStore } from 'decorators';

@withStore('Position', { preload: true })
export default class PositionIndex extends React.Component {
  constructor(props) {
    super(props)
  }

  handleDelete = (id) => {
    this.props.actions.deletePosition(id)
  }

  render() {
    return (
      <div className="container">
        <h1>Список должностей</h1>
        <p><Link to="/positions/new">Добавить</Link></p>
        <Table
          dataList={this.props.Position.list}
          headerColumns={[
            { type: 'id' },
            "Имя",
            { text: "Действия", type: "actions" }
          ]}
          rowColumns={[
            { type: 'id' },
            item => item.name,
            {
              type: 'actions',
              actions: {
                edit: item => `/positions/${item.id}/edit`,
                remove: item => ({
                  onClick: () => this.handleDelete(item.id)
                })
              }
            }
          ]}></Table>
      </div>
    )
  }
}
