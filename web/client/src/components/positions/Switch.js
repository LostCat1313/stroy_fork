import React from 'react'
import { Switch, Route } from 'react-router-dom'

import PositionIndex from './PositionIndex'
import PositionEdit from './PositionEdit'

export default ({match: {url}}) => {
  return (
    <Switch>
      <Route exact path={url} component={PositionIndex}/>
      <Route exact
            path={`${url}/new`}
            component={PositionEdit} />
      <Route exact
            path={`${url}/:id/edit`}
            component={PositionEdit} />
    </Switch>
  )
}