import React from 'react'
import { Link } from 'react-router-dom'

import { Table, Header } from 'components'
import { withStore } from 'decorators'

@withStore('Note', {actions: ['load', 'delete', 'deleteAll']})
export default class NotesIndex extends React.Component {
  constructor(props) {
    super(props)
  }

  deleteNote = (id) => {
    this.props.actions.deleteNote(id)
  }

  deleteAll(e) {
    this.props.actions.deleteAllNotes(this.props.Note.list.map(i => i.id))
    e.preventDefault()
    return false
  }

  render() {
    return (
      <div className="container">
        <Header>Список заметок</Header>
        <p><Link to="/notes/new">Добавить</Link></p>
        <p><a href="#" onClick={this.deleteAll.bind(this)}>Удалить все</a></p>
        <Table
          dataList={this.props.Note.list}
          headerColumns={[{ type: 'id' }, "Текст",  { text: "Действия", type: "actions" }]}
          rowColumns={[
            { type: 'id' },
            note => note.content.length > 100 ? note.content.substr(0, 100) + '...' : note.content,
            {
              type: 'actions',
              actions: {
                edit: note => '/notes/' + note.id + '/edit',
                remove: note => ({
                  onClick: () => this.deleteNote(note.id)
                })
              }
            }
          ]}
        ></Table>
      </div>
    )
  }
}
