import React from 'react'
import { Switch, Route } from 'react-router-dom'
import Index from './NotesIndex'
import Edit from './NoteEdit'

export default ({match: {url}}) => {
  return (
    <Switch>
      <Route exact path={url} component={Index}/>
      <Route exact path={`${url}/new`} component={Edit} />
      <Route exact path={`${url}/:id/edit`} component={Edit} />
    </Switch>
  )
}