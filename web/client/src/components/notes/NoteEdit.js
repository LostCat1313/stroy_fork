import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { EditComponent, TextField } from 'components'
import { createNote, updateNote } from 'actions'

class Edit extends EditComponent {
  constructor(props) {
    super(props, 'Note')
    this.backUri = '/notes'
  }

  componentDidMount() {
  }

  renderHeader() {
    let entry = this.getEntry()
    return entry
      ? "Редактирование заметки " + entry.id
      : "Новая заметка"
  }

  renderFields() {
    return [
      <TextField
        name="content"
        label="Текст"
        rows="2"
        rowsMax="5"
        fullWidth
        multiline
        required
      />
    ]
  }
}

function mapStateToProps(state) {
  const props = { Note: state.Note }
  return props
}

function mapDispatchToProps(dispatch) {
  const actions = { createNote, updateNote }
  const actionMap = { actions: bindActionCreators(actions, dispatch) }
  return actionMap
}


export default connect(mapStateToProps, mapDispatchToProps)(Edit)
