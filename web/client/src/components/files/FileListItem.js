import React from 'react'

export default class FileListItem extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <div className="file-item">
        <div className="file-image"><img src={this.props.file.thumbUrl}/></div>
        <div className="file-original">{this.props.file.original}</div>
      </div>
    )
  }
}