import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {loadFiles, deleteFile} from 'actions';

import FileListItem from './FileListItem';
import { Header } from 'components';

const mapStateToProps = (state) => ({ file: state.File });
const mapDispatchToProps = (dispatch) => ({ actions: bindActionCreators({ loadFiles }, dispatch) });

@connect(mapStateToProps, mapDispatchToProps)
export default class FilesIndex extends React.Component {
  componentDidMount() {
    this.props.actions.loadFiles({ limit: 50, offset:0 });
  }

  render() {
    const files = this.props.file.list;
    return (
      <div className="container">
       <Header>Управление файлами</Header>
       { files && files.length > 0 && <div className="files-container">
        { files.map(f => <FileListItem key={f.id} file={f}/>)}
       </div> }
      </div>
    );
  }
}

