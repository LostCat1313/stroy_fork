import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import LoadingComponent from 'components/common/Loading';
import Actions from 'actions';

const withStore = (...args) => WrappedComponent => {
  let entities = [];
  const generateActionNames = name => ({
    load: 'load' + name + 's',
    delete: 'delete' + name,
    deleteAll: 'deleteAll' + name + 's',
    create: 'create' + name,
    update: 'update' + name
  });
  
  const generateOptions = options => ({
    actions: ['load', 'delete'],
    preload: true, 
    preloadParams: {},
    ...options
  });

  if(typeof args[0] === 'string') {
    entities.push({ 
      name:args[0],
      options:generateOptions(args[1] || {}),
      actionNames:generateActionNames(args[0])
    });
  }
  else if(args.every(i => typeof i === 'object')) {
    let a = args;
    if(!args[args.length - 1].name) {
      a = args.slice(0, args.length - 1);
      entities.push({
        name: null,
        options: generateOptions(args[args.length - 1])
      });
    }
    a.forEach(i => {
      entities.push({
        name:i.name,
        options:generateOptions(i.options || i),
        actionNames:generateActionNames(i.name)
      });
    });
  }
  else {
    throw new Error('withStore hoc wrong arguments');
  }

  const mapStateToProps = state => {
    let result = {}
    entities.forEach(e => {
      if(e.name) {
        result[e.name] = state[e.name];
      }
    });
    return result;
  }

  const mapDispatchToProps = (dispatch) => {
    let actions = {};

    entities.forEach(e => {
      if(Array.isArray(e.options.actions)) {
        e.options.actions.forEach(a => {
          const actionName = e.actionNames[a];
          const action = Actions[actionName];
          if(action) {
            actions[actionName] = action;
          }
        });
      }
      else if(typeof e.options.actions === 'object'){
        actions = { ...actions, ...e.options.actions };
      }

    });

    const actionMap = { actions: bindActionCreators(actions, dispatch) };
    return actionMap;
  }

  @connect(mapStateToProps, mapDispatchToProps)
  class AsyncComponent extends Component {
    componentDidMount() {
      entities.forEach( e => {
        const { actions, preload, prepareParams, preloadIfNull} = e.options;
        
        if(e.name && preload /*&& Array.isArray(actions) && actions.indexOf('load') >= 0*/) {
          let params = { };
          
          if(prepareParams) {
            params = {...prepareParams(this.props) };
          }

          const store = this.props[e.name];
          if(store.list != null && preloadIfNull) return true;
          const load = this.props.actions[e.actionNames.load];
          if(!load) { 
            console.error('No load function', this.props.actions);
          }
          load(params);
        }
      });
    }

    render() {
      const Loading = LoadingComponent;

      for(let i = 0; i < entities.length; i++) {
        const store = entities[i].name && this.props[entities[i].name];
        if(entities[i].options.preload && (!store || store.loading || store.list === null)) return <Loading center height={entities[i].options.shrink ? 'auto' : null}/>;
      }

      return (
        <WrappedComponent {...this.props} />
      );
    }
  }
  return AsyncComponent;
}

export default withStore;