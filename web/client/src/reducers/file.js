import { 
  FILES_LOAD,
  FILE_CREATE,
  FILE_DELETE,
  FILE_UPDATE
} from '../actions/const'

import {  
  ListRequestActionReducer,
  DeleteRequestActionReducer,
  CreateRequestActionReducer,
  BaseRequestActionReducer,
  CombinedReducer 
} from './common'

let r = new CombinedReducer({
  initialState: {
    list: null,
    // move to base
    loading: false
  },
  reducers: [
    new ListRequestActionReducer({ type: FILES_LOAD, error: 'Ошибка при загрузке списка файлов' }),
    new CreateRequestActionReducer({ type: FILE_CREATE, error: 'Ошибка при создании файла' }),
    new DeleteRequestActionReducer({ type: FILE_DELETE, error: 'Ошибка при удалении файла' })
  ]
})

export default r.handle.bind(r)

