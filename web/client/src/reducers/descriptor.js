import { 
  DESCRIPTORS_LOAD,
  DESCRIPTOR_CREATE,
  DESCRIPTOR_DELETE,
  DESCRIPTOR_UPDATE
} from '../actions/const'

import {  
  ListRequestActionReducer,
  DeleteRequestActionReducer,
  CreateRequestActionReducer,
  BaseRequestActionReducer,
  CombinedReducer 
} from './common'

let r = new CombinedReducer({
  initialState: {
    list: null,
    // move to base
    loading: false
  },
  reducers: [
    new ListRequestActionReducer({ type: DESCRIPTORS_LOAD, error: 'Ошибка при загрузке списка маркеров' }),
    new CreateRequestActionReducer({ type: DESCRIPTOR_CREATE, error: 'Ошибка при создании маркера' }),
    new DeleteRequestActionReducer({ type: DESCRIPTOR_DELETE, error: 'Ошибка при удалении маркера' })
  ]
})

export default r.handle.bind(r)

