import { 
  POSITIONS_LOAD,
  POSITION_CREATE,
  POSITION_DELETE,
  POSITION_UPDATE
} from '../actions/const'

import {  
  ListRequestActionReducer,
  DeleteRequestActionReducer,
  CreateRequestActionReducer,
  BaseRequestActionReducer,
  CombinedReducer 
} from './common'

let r = new CombinedReducer({
  initialState: {
    list: null,
    // move to base
    loading: false
  },
  reducers: [
    new ListRequestActionReducer({ type: POSITIONS_LOAD, error: 'Ошибка при загрузке списка позиций' }),
    new CreateRequestActionReducer({ type: POSITION_CREATE, error: 'Ошибка при создании позиции' }),
    new DeleteRequestActionReducer({ type: POSITION_DELETE, error: 'Ошибка при удалении позиции' })
  ]
})

export default r.handle.bind(r)

