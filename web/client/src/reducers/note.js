import { 
  NOTES_LOAD,
  NOTE_CREATE,
  NOTE_DELETE,
  NOTES_DELETE_ALL,
  NOTE_UPDATE
} from '../actions/const'

import {  
  ListRequestActionReducer,
  DeleteRequestActionReducer,
  CreateRequestActionReducer,
  DeleteAllRequestActionReducer,
  BaseRequestActionReducer,
  CombinedReducer 
} from './common'

let r = new CombinedReducer({
  initialState: {
    list: null,
    // move to base
    loading: false
  },
  reducers: [
    new ListRequestActionReducer({ type: NOTES_LOAD, error: 'Ошибка при загрузке списка заметок' }),
    new CreateRequestActionReducer({ type: NOTE_CREATE, error: 'Ошибка при создании заметки' }),
    new DeleteRequestActionReducer({ type: NOTE_DELETE, error: 'Ошибка при удалении заметки' }),
    new DeleteAllRequestActionReducer({ type: NOTES_DELETE_ALL, error: 'Ошибка при удалении заметок' }),
  ]
})

export default r.handle.bind(r)

