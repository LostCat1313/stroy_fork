import { 
  RESET_SESSION,
  LOGIN,
  LOGOUT,
  VERIFY_TOKEN,
  REGISTER,
  REGISTER_UPDATE,
  REGISTER_ACTIVATE
} from '../actions/const'
import { REHYDRATE } from 'redux-persist/constants'

const initialState = {
  name: "",
  token: null,
  authenticated: false,
  error: null,
  loading: false,
  verified: false,
  initializing: true
}

function updateCookie(token) {
  document.cookie = "Authorization=Bearer " + token + " path=/"
}

export default function (state = initialState, action) {
  // console.log(action)
  switch(action.type) {
    case RESET_SESSION:
      return {
        ...state,
        token: null,
        error: null,
        loading: false,
        initializing: false,
        authenticated: false,
      }
    case LOGOUT:
      if(action.status == 'success') {
        return {
          ...state,
          token: null,
          error: null,
          loading: false,
          verified: false,
          authenticated: false,
        }
      }
      return {
        ...state,
        token: null,
        loading: true,
        verified: false,
        authenticated: false,
      }
    case LOGIN:
      switch(action.status) {
        case 'success':
          updateCookie(action.response.token)
          return {
            ...state,
            user: action.response.user,
            countSummary: action.response.countSummary,
            config: {
              rights: action.response.rights
            },
            username: action.username,
            token: action.response.token,
            authenticated: true,
            loading: false
          }
        case 'error':
          return {
            ...state,
            name: "",
            token: null,
            error: action.error,
            loading: false,
            authenticated: false,
          }
        default:
          return {
            ...state,
           loading: true,
           authenticated: false,
          }
      }
    // move to registration reducer
    case REGISTER: 
      switch(action.status) {
        case 'success':
          return {
            ...state,
            phone:action.response.phoneNumber
          }
        // case 'error':
        //   return {
        //     ...state,
        //     error:action.error
        //   }
        default: return {
          ...state
        }
      }
    case REGISTER_UPDATE:
      switch(action.status) {
        case 'success':
          return {
            ...state,
            authenticated: state.token && action.response.isActive,
          };
      }
      return {
        ...state
      }
    case REGISTER_ACTIVATE:
      switch(action.status) {
        case 'success':
          return {
            ...state,
            authenticated: !action.response.signup,
            token:action.response.token
          }
        default: return {
          ...state
        }
      }
    case VERIFY_TOKEN: 
      if(action.status) {
        const success = action.status == 'success'
        // TODO: make it generic?
        // add to cookie
        if(success) updateCookie(state.token)
        const countSummary = success ? (action.response.countSummary || state.countSummary) : null
        return {
          ...state,
          countSummary,
          token: success ? state.token : null,
          authenticated: success,
          verified: success,
          loading: false
        }
      }
      break
    // restoring session
    case REHYDRATE:
      var incoming = action.payload.Session
      if(incoming) {
        return { 
          ...state,
          ...incoming,
          verified: false,
          loading: true,
          initializing: false,
          error: null 
        }
      } 
      return {
        ...state,
        initializing: false,
        error: null 
      }
    default:
      return state
  }
  return state
}
