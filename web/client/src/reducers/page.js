import { 
  PAGES_LOAD,
  PAGE_DELETE,
  PAGE_NUMBER_UPDATE
} from '../actions/const'

import {  
  ListRequestActionReducer,
  DeleteRequestActionReducer,
  CreateRequestActionReducer,
  BaseRequestActionReducer,
  CombinedReducer 
} from './common'

let r = new CombinedReducer({
  initialState: {
    list: null,
    // move to base
    loading: false
  },
  reducers: [
    new ListRequestActionReducer({ type: PAGES_LOAD, error: 'Ошибка при загрузке страниц документа' }),
    new DeleteRequestActionReducer({ type: PAGE_DELETE, error: 'Ошибка при удалении страницы' })
  ]
})

export default r.handle.bind(r)

