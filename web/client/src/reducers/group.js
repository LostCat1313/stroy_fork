import { 
  GROUPS_LOAD,
  GROUP_CREATE,
  GROUP_DELETE,
  GROUP_UPDATE
} from '../actions/const'

import {  
  ListRequestActionReducer,
  DeleteRequestActionReducer,
  CreateRequestActionReducer,
  CombinedReducer 
} from './common'

let r = new CombinedReducer({
  initialState: {
    list: null,
    // move to base
    loading: false
  },
  reducers: [
    new ListRequestActionReducer({ type: GROUPS_LOAD, error: 'Ошибка при загрузке списка заметок' }),
    new CreateRequestActionReducer({ type: GROUP_CREATE, error: 'Ошибка при создании заметки' }),
    new DeleteRequestActionReducer({ type: GROUP_DELETE, error: 'Ошибка при удалении заметки' })
  ]
})

export default r.handle.bind(r)

