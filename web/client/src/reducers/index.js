/* eslint-disable import/newline-after-import */
/* Combine all available reducers to a single root reducer.
 *
 * CAUTION: When using the generators, this file is modified in some places.
 *          This is done via AST traversal - Some of your formatting may be lost
 *          in the process - no functionality should be broken though.
 *          This modifications only run once when the generator is invoked - if
 *          you edit them, they are not updated again.
 */
/* Populated by react-webpack-redux:reducer */
import { combineReducers } from 'redux'
import Session from './session'
import Global from './global'
import User from './user'
import Project from './project'
import Document from './document'
import Organization from './organization'
import Position from './position'
import Note from './note'
import Task from './task'
import Page from './page'
import Descriptor from './descriptor'
import Notification from './notification'
import File from './file'
import Group from './group'
import Right from './right'
import RegId from './regId'

const reducers = {
  Session,
  Global,
  User,
  Project,
  Document,
  Organization,
  Position,
  Note,
  Task,
  Page,
  Descriptor,
  Notification,
  File,
  Group,
  Right,
  RegId
}

export default combineReducers(reducers)
