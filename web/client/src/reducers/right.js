import { 
  RIGHTS_LOAD,
  RIGHT_CREATE,
  RIGHT_DELETE,
  RIGHT_UPDATE
} from '../actions/const'

import {  
  ListRequestActionReducer,
  DeleteRequestActionReducer,
  CreateRequestActionReducer,
  CombinedReducer 
} from './common'

let r = new CombinedReducer({
  initialState: {
    list: null,
    // move to base
    loading: false
  },
  reducers: [
    new ListRequestActionReducer({ type: RIGHTS_LOAD, error: 'Ошибка при загрузке списка прав' }),
    new CreateRequestActionReducer({ type: RIGHT_CREATE, error: 'Ошибка при создании права' }),
    new DeleteRequestActionReducer({ type: RIGHT_DELETE, error: 'Ошибка при удалении права' })
  ]
})

export default r.handle.bind(r)

