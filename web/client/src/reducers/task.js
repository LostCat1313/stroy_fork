import { 
  TASKS_LOAD,
  TASK_CREATE,
  TASK_DELETE,
  TASK_UPDATE
} from '../actions/const'

import {  
  ListRequestActionReducer,
  DeleteRequestActionReducer,
  CreateRequestActionReducer,
  BaseRequestActionReducer,
  CombinedReducer 
} from './common'

let r = new CombinedReducer({
  initialState: {
    list: null,
    // move to base
    loading: false
  },
  reducers: [
    new ListRequestActionReducer({ type: TASKS_LOAD, error: 'Ошибка при загрузке списка задач' }),
    new CreateRequestActionReducer({ type: TASK_CREATE, error: 'Ошибка при создании задачи' }),
    new DeleteRequestActionReducer({ type: TASK_DELETE, error: 'Ошибка при удалении задачи' })
  ]
})

export default r.handle.bind(r)

