import { ListRequestActionReducer } from './ListRequestActionReducer'
import { ItemRequestActionReducer } from './ItemRequestActionReducer'
import { DeleteRequestActionReducer } from './DeleteRequestActionReducer'
import { DeleteAllRequestActionReducer } from './DeleteAllRequestActionReducer'
import { CreateRequestActionReducer } from './CreateRequestActionReducer'
import { BaseRequestActionReducer, CombinedReducer } from './BaseRequestActionReducer'

module.exports = {
  ListRequestActionReducer,
  DeleteRequestActionReducer,
  DeleteAllRequestActionReducer,
  CreateRequestActionReducer,
  BaseRequestActionReducer,
  ItemRequestActionReducer,
  CombinedReducer
}
