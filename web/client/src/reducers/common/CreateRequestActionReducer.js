import { 
  BaseRequestActionReducer
} from './BaseRequestActionReducer'

export class CreateRequestActionReducer extends BaseRequestActionReducer {
    constructor(options) {
      super(options)
      this.loading = false;
    }

    updateDB(db, entry) {
      db = db || {}
      if(!entry) {
        console.error('Update DB: Entry is empty')
      }
      db[entry.id] = entry
      entry.parent = db[entry.parentId]
      return db
    }

    success(state, action) {
      return {
        ...super.success(state, action),
        db: this.updateDB(state.db, action.response),
        list: state.list ? state.list.concat([action.response]) : [action.response]
      }
    }
  }