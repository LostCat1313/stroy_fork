import { BaseRequestActionReducer } from './BaseRequestActionReducer'

export class ListRequestActionReducer extends BaseRequestActionReducer {
  constructor(options) {
    super(options)
    this.key = options.key ? options.key : 'list'
    this.stateKey = options.stateKey ? options.stateKey: 'list'
  }
  
  updateDB(db, list, parent) {
    db = db || {}
    if(!list) return
    list.forEach(i => {
      db[i.id] = i
      i.parent = parent
      this.updateDB(db, i.children, i)
    })
    return db
  }

  success(state, action) {
    return {
      ...super.success(state, action),
      db: this.updateDB(state.db, action.response[this.key]),
      [this.stateKey]: action.response[this.key]
    }
  }

  failure(state, action) {
    return {
      ...super.failure(state, action),
      [this.stateKey]: null
    }
  }
}