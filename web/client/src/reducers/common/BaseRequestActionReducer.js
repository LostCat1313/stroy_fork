export class BaseRequestActionReducer {
  constructor(options) {
    this.type = options.type
    this.error = options.error
    this.loading = true
  }

  handle(state, action) {
    if(action.type === this.type) {
      switch(action.status) {
        case 'success':
          return this.success(state, action)
        case 'error':
          return this.failure(state, action)
        default:
          return this.request(state, action)
      }
    }
    return null
  }

  success(state, action) {
    return {
      ...state,
      loading: false,
    }
  }

  failure(state, action) {
    return {
      ...state,
      loading: false,
      error: this.error
    }
  }

  request(state, action) {
    return {
      ...state,
      loading: this.loading,
      error: null
    }
  }
}

export class CombinedReducer {
  constructor(options) {
    this.reducers = options.reducers ? options.reducers : []
    this.funcReducer = options.funcReducer
    this.initialState = options.initialState
  }

  add(reducer) {
    this.reducers.push(reducer)
  }

  handle(state, action) {
    let result = null
    for(let i = 0; i < this.reducers.length; i++) {
      result = this.reducers[i].handle(state, action)
      if(result) return result
    }
    if(this.funcReducer) {
      result = this.funcReducer(state, action)
      if (result) return result
    }
    return state ? state : this.initialState
  }
}