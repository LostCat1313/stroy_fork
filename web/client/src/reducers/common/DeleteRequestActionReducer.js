import { BaseRequestActionReducer } from './BaseRequestActionReducer'

export class DeleteRequestActionReducer extends BaseRequestActionReducer {

  loading = false;

  updateDB(db, id) {
    if(db[id]) {
      let parent = db[id].parent
      delete db[id]
      if(parent && parent.children) {
        parent.children = parent.children.filter(i => i.id != id)
      }
    }
    return db
  }

  success(state, action) {
    
    var index = state.list.findIndex((v) => {
      return v.id == action.response.id
    })

    if (index >= 0)
      state.list.splice(index, 1)
    
    return {
      ...super.success(state, action),
      db: this.updateDB(state.db, action.response.id),
      list: state.list.slice(0),
    }
  }
}