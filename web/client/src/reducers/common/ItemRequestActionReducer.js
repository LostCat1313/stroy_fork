import { BaseRequestActionReducer } from './BaseRequestActionReducer'

export class ItemRequestActionReducer extends BaseRequestActionReducer {
  constructor(options) {
    super(options)
    this.key = options.key ? options.key : 'entry'
    this.stateKey = options.stateKey ? options.stateKey: 'list'
  }
  
  updateDB(db, entry, parent) {
    if(!db) db = {}
    if(!entry) return
    db[entry.id] = entry
    return db
  }

  success(state, action) {
    const initial = state[this.stateKey]
    return {
      ...super.success(state, action),
      db: this.updateDB(state.db, action.response[this.key]),
      [this.stateKey]: initial ? [...initial, action.response[this.key]] : [action.response[this.key]]
    }
  }

  failure(state, action) {
    return {
      ...super.failure(state, action),
      [this.stateKey]: null
    }
  }
}