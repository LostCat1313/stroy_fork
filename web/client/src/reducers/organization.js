import { 
  ORGANIZATIONS_LOAD,
  ORGANIZATION_CREATE,
  ORGANIZATION_DELETE,
  ORGANIZATION_UPDATE,
  ORGANIZATION_USERS_LOAD
} from '../actions/const'

import {  
  ListRequestActionReducer,
  DeleteRequestActionReducer,
  CreateRequestActionReducer,
  BaseRequestActionReducer,
  CombinedReducer 
} from './common'

let r = new CombinedReducer({
  initialState: {
    list: null,
    users: null,
    // move to base
    loading: false
  },
  reducers: [
    new ListRequestActionReducer({ 
      type: ORGANIZATIONS_LOAD,
      error: 'Ошибка при загрузке организаций' 
    }),
    new ListRequestActionReducer({ 
      type: ORGANIZATION_USERS_LOAD,
      error: 'Ошибка при загрузке списка пользователей организации',
      stateKey: 'users' 
    }),
    new CreateRequestActionReducer({ type: ORGANIZATION_CREATE, error: 'Ошибка при создании организации' }),
    new DeleteRequestActionReducer({ type: ORGANIZATION_DELETE, error: 'Ошибка при удалении организации' })
  ]
})

export default r.handle.bind(r)

