import { 
  REGIDS_LOAD,
  REGID_CREATE,
  REGID_DELETE,
  REGIDS_DELETE_ALL,
  REGID_UPDATE
} from '../actions/const'

import {  
  ListRequestActionReducer,
  DeleteRequestActionReducer,
  CreateRequestActionReducer,
  DeleteAllRequestActionReducer,
  BaseRequestActionReducer,
  CombinedReducer 
} from './common'

let r = new CombinedReducer({
  initialState: {
    list: null,
    // move to base
    loading: false
  },
  reducers: [
    new ListRequestActionReducer({ type: REGIDS_LOAD, error: 'Ошибка при загрузке списка заметок' }),
    new CreateRequestActionReducer({ type: REGID_CREATE, error: 'Ошибка при создании заметки' }),
    new DeleteRequestActionReducer({ type: REGID_DELETE, error: 'Ошибка при удалении заметки' }),
    new DeleteAllRequestActionReducer({ type: REGIDS_DELETE_ALL, error: 'Ошибка при удалении заметок' }),
  ]
})

export default r.handle.bind(r)

