import { 
  REDIRECT,
  REDIRECT_RESET, // change to browserHistory 
  GLOBAL_SET_HEADER,
  GLOBAL_SET_SUBHEADER,
} from '../actions/const'

const initialState = {
  loading: false,
  redirectTo: null,
}

export default function (state = initialState, action) {

  switch(action.type) {
    case REDIRECT:
      return {
        ...state,
        redirectTo: action.redirectTo
      }
    case REDIRECT_RESET:
      return {
        ...state,
        redirectTo: null
      }
    case GLOBAL_SET_HEADER:
      return {
        ...state,
        header: action.data.header
      }
    case GLOBAL_SET_SUBHEADER:
      return {
        ...state,
        header: action.data.subheader
      }
    default:
      return state
  }
}
