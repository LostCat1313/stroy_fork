import { 
  PROJECTS_LOAD,
  PROJECT_CREATE,
  PROJECT_DELETE,
  PROJECT_UPDATE,
  PROJECT_USERS_LOAD
} from '../actions/const'

import {  
  ListRequestActionReducer,
  DeleteRequestActionReducer,
  CreateRequestActionReducer,
  BaseRequestActionReducer,
  CombinedReducer 
} from './common'

let r = new CombinedReducer({
  initialState: {
    list: null,
    // move to base
    loading: false
  },
  reducers: [
    new ListRequestActionReducer({ type: PROJECTS_LOAD, error: 'Ошибка при загрузке списка проектов' }),
    new ListRequestActionReducer({
      type: PROJECT_USERS_LOAD,
      error: 'Ошибка при загрузке списка пользователей проекта',
      stateKey: 'users',
      key: 'list'
    }),
    new CreateRequestActionReducer({ type: PROJECT_CREATE, error: 'Ошибка при создании проекта' }),
    new DeleteRequestActionReducer({ type: PROJECT_DELETE, error: 'Ошибка при удалении проекта' })
  ]
})

export default r.handle.bind(r)

