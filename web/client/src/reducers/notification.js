import { 
  NOTIFICATIONS_LOAD,
} from '../actions/const'

import {  
  ListRequestActionReducer,
  DeleteRequestActionReducer,
  CreateRequestActionReducer,
  BaseRequestActionReducer,
  CombinedReducer 
} from './common'

let r = new CombinedReducer({
  initialState: {
    list: null,
    // move to base
    loading: false
  },
  reducers: [
    new ListRequestActionReducer({ type: NOTIFICATIONS_LOAD, error: 'Ошибка при загрузке списка уведомлений' }),
    // new CreateRequestActionReducer({ type: DOCUMENT_CREATE, error: 'Ошибка при создании документа' }),
    // new DeleteRequestActionReducer({ type: DOCUMENT_DELETE, error: 'Ошибка при удалении документа' })
  ]
})

export default r.handle.bind(r)

