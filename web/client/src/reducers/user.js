import { 
  USERS_LOAD,
  CONTACTS_LOAD,
  USER_CREATE,
  USER_DELETE,
  USER_UPDATE
} from '../actions/const'

import {  
  ListRequestActionReducer,
  DeleteRequestActionReducer,
  CreateRequestActionReducer,
  BaseRequestActionReducer,
  CombinedReducer 
} from './common'

let r = new CombinedReducer({
  initialState: {
    list: null,
    // move to base
    loading: false
  },
  reducers: [
    new ListRequestActionReducer({ type: USERS_LOAD, error: 'Ошибка при загрузке списка пользователей' }),
    new CreateRequestActionReducer({ type: USER_CREATE, error: 'Ошибка при создании пользователя' }),
    new DeleteRequestActionReducer({ type: USER_DELETE, error: 'Ошибка при удалении пользователя' })
  ],
  funcReducer: (state, action) => {
    if(action.type == CONTACTS_LOAD && action.response && action.response.list) {
      // todo: to db
      return {
        ...state,
        contacts: action.response.list
      }
    }
  }
})

export default r.handle.bind(r)

