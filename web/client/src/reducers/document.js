import { 
  DOCUMENTS_LOAD,
  LAST_DOCUMENTS_LOAD,
  DOCUMENT_LOAD,
  DOCUMENT_CREATE,
  DOCUMENT_DELETE,
  DOCUMENT_UPDATE,
  DOCUMENT_CURRENT
} from '../actions/const'

import {  
  ListRequestActionReducer,
  DeleteRequestActionReducer,
  CreateRequestActionReducer,
  BaseRequestActionReducer,
  ItemRequestActionReducer,
  CombinedReducer 
} from './common'

let r = new CombinedReducer({
  initialState: {
    list: null,
    db: {},
    // move to base
    loading: false
  },
  reducers: [
    new ItemRequestActionReducer({ type: DOCUMENT_LOAD, error: "Ошибка при загрузке документа" }),
    new ListRequestActionReducer({ type: DOCUMENTS_LOAD, error: 'Ошибка при загрузке списка документов' }),
    new CreateRequestActionReducer({ type: DOCUMENT_CREATE, error: 'Ошибка при создании документа' }),
    new DeleteRequestActionReducer({ type: DOCUMENT_DELETE, error: 'Ошибка при удалении документа' })
  ],
  funcReducer: (state, action) => {
    if(action.type == DOCUMENT_CURRENT && action.data.current) {
      return {
        ...state,
        current: action.data.current
      }
    }
    else if(action.type == LAST_DOCUMENTS_LOAD && action.response && action.response.list) {
      // todo: to db
      return {
        ...state,
        last: action.response.list
      }
    }
  }
})

export default r.handle.bind(r)

