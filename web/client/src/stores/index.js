import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'
import { createLogger } from 'redux-logger'
import callAPIMiddleware from './callAPIMiddleware'
import reducers from '../reducers'
import {persistStore, autoRehydrate} from 'redux-persist'

export default function reduxStore(initialState) {

  //const store = createStore(reducers, initialState,
    //window.devToolsExtension && window.devToolsExtension())
  const loggerMiddleware = createLogger()

  const store = createStore(
    reducers,
    compose(
      applyMiddleware(
        thunk,
        callAPIMiddleware,
        loggerMiddleware
      ),
      autoRehydrate()
    ),
    window.devToolsExtension && window.devToolsExtension())

  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('../reducers', () => {
      // We need to require for hot reloading to work properly.
      const nextReducer = require('../reducers')  // eslint-disable-line global-require

      store.replaceReducer(nextReducer)
    })
  }

  persistStore(store, {whitelist: 'Session'})

  return store
}
