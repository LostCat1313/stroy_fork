import qs from 'query-string'

export default function callAPIMiddleware({ dispatch, getState }) {
  return next => action => {
    const {
      type,
      uri,
      data,
      method,
      shouldCallAPI = () => true,
      payload = {}
    } = action

    if (!method && !uri) {
      // Normal action: pass it on
      return next(action)
    }

    if (!shouldCallAPI(getState())) {
      return
    }

    var token = getState().Session.token


    dispatch(Object.assign({}, payload, {
      type
    }))

    const authorization = token ? { 'Authorization': 'Bearer ' + token } : {  }

    
    var requestUrl = `${uri}`
    
    if (!method || method === 'get' || method === 'delete') {
      requestUrl += '?' + qs.stringify(data)
      console.log(qs.stringify(data))  
    }
    

    return fetch(requestUrl, {
        method: !method ? 'get' : method,
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          ...authorization
        },
        body: method && method !== 'get' && method !== 'delete' ? JSON.stringify(data) : null
        // qs: !method || method === 'get' ? data : null
      })
      .then(result => {
        // console.log(result)
        return result.json()
      })
      .then(response => {
        // console.log('Response', response)
        if (response.error) {
          // temp
          console.error(response.error)
          alert(JSON.stringify(response.error))
          return dispatch(Object.assign({}, payload, {
            error: response.error,
            status: 'error',
            type
          }))
        } else {
          return dispatch(Object.assign({}, payload, {
            response,
            status: 'success',
            type
          }))
        }
      }).catch(error => {
        // console.error(error)
        alert(error)
        return dispatch(Object.assign({}, payload, {
          error,
          status: 'error',
          type
        }))
      })
  }
}
