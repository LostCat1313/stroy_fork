import { 
  GLOBAL_SET_HEADER, GLOBAL_SET_SUBHEADER
} from './const'

function setHeader(header) {
  return {
    type: GLOBAL_SET_HEADER,
    data: { header }
  }
}

function setSubHeader(subheader) {
  return {
    type: GLOBAL_SET_SUBHEADER,
    data: { subheader }
  }
}