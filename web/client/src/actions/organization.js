import { 
  ORGANIZATION_USERS_LOAD,
  ORGANIZATION_USERS_SAVE
} from './const'

function loadOrganizationUsers(orgId) {
  return {
    type: ORGANIZATION_USERS_LOAD,
    uri: '/api/organizations/' + orgId + '/users',
    method: 'get',
    data: {  },
    payload: {  }
  } 
}


function saveOrganizationUsers(orgId, ids) {
  return {
    type: ORGANIZATION_USERS_SAVE,
    uri: '/api/organizations/' + orgId + '/users',
    method: 'put',
    data: { userIds: ids },
    payload: {  }
  } 
}

module.exports = {
  loadOrganizationUsers,
  saveOrganizationUsers
}
