export const LOAD = 'load'
export const CREATE = 'create'
export const UPDATE = 'update'
export const DELETE = 'delete'

function generateType(entity, type, divider = '_') {
  return entity.toUpperCase() + divider + type.toUpperCase()
}

export function generateCrud(entity, options) {
  let result = {}
  let { baseUri, load } = options
  load = load || {}
  let ignore = options.ignore || {}
  if(!ignore[LOAD]) {
    result[LOAD + entity + 's'] = (params) => {
      return {
        type: generateType(entity, LOAD, 'S_'),
        uri: baseUri,
        method: 'get',
        data: {  ...params, ...load.data  },
        payload: {  }
      }
    }
    result[LOAD + entity + 'ById'] = (id, params) => {
      return {
        type: generateType(entity, LOAD, '_'),
        uri: baseUri + id,
        method: 'get',
        data: {  ...params, ...load.data  },
        payload: {  }
      }
    }
  }
    
  if(!ignore[CREATE])
    result[CREATE + entity] = (data) => {
      return {
        type: generateType(entity, CREATE),
        uri: baseUri,
        method: 'post',
        data: {
          entry: {
            ...data
          }
        },
        payload: {  }
      }
    }
  if(!ignore[UPDATE])
    result[UPDATE + entity] = (id, data) => {
      return {
        type: generateType(entity, UPDATE),
        uri: baseUri + id,
        method: 'put',
        data: {
          entry: {
            ...data
          }
        },
        payload: {  }
      }
    }
  if(!ignore[DELETE])
    result[DELETE + entity] = (id) => {
      return {
        type: generateType(entity, DELETE),
        uri: baseUri + id,
        method: 'delete',
        data: {  },
        payload: {  }
      }
    }

  return result
}

export function generateCrudActionTypes(entity) {
  return [
    generateType(entity, UPDATE),
    generateType(entity, DELETE),
    generateType(entity, CREATE),
    generateType(entity, LOAD, 'S_'),
    generateType(entity, LOAD, '_')
  ]
}