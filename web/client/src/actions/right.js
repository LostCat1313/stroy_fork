import { 
  RIGHTS_LOAD
} from './const'

function loadRights() {
  return {
    type: RIGHTS_LOAD,
    uri: '/api/rights',
    method: 'get',
    data: { },
    payload: {  }
  }
}

// TODO: make local id => entry db. setcurrent not good
module.exports = {
  loadRights
}
