import { 
  LOGIN, LOGOUT, VERIFY_TOKEN_REG, RESET_SESSION, VERIFY_TOKEN, REGISTER, REGISTER_UPDATE, REGISTER_ACTIVATE
} from './const'

export function login(username, password) {
  return {
    type: LOGIN,
    uri: '/api/authenticate',
    method: 'post',
    data: { username, password },
    payload: { username }
  }
}

export function logout() {
  return {
    type: LOGOUT,
    uri: '/api/authenticate/logout',
    method: 'get',
    data: {},
    payload: {}
  }
}

export function verifyTokenReg(regId, deviceType) {
  return {
    type: VERIFY_TOKEN_REG,
    uri: '/api/ping',
    method: 'post',
    data: {
      regId,
      deviceType: deviceType || 0
    },
    payload: {}
  }
}

export function verifyToken() {
  return {
    type: VERIFY_TOKEN,
    uri: '/api/ping',
    method: 'get',
    data: {},
    payload: {}
  }
}

export function register(phone) {
  return {
    type: REGISTER,
    uri: '/api/authenticate/signup',
    method: 'get',
    data: {phone},
    payload: {phone}
  }
}

export function registerUpdate(data) {
  return {
    type: REGISTER_UPDATE,
    uri: '/api/users/signup/update',
    method: 'post',
    data: { entry: {...data} },
    payload: {}
  }
}

export function activate(phone, code) {
  return {
    type: REGISTER_ACTIVATE,
    uri: '/api/authenticate/activate',
    method: 'get',
    data: {phone, code},
    payload: {}
  }
}

export function reset() {
  return {
    type: RESET_SESSION
  }
}