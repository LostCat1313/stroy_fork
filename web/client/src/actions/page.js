import { 
  PAGES_LOAD,
  PAGE_DELETE,
  PAGE_SET_ACTUAL
} from './const'

function loadPages(docId, docVersionId) {
  let version = ''
  if(docVersionId) {
    version = '/' + docVersionId
  }
  return {
    type: PAGES_LOAD,
    uri: '/api/documents/' + docId + version + '/pages',
    method: 'get',
    data: {  },
    payload: {  }
  } 
}


function deletePage(id) {
  return {
    type: PAGE_DELETE,
    uri: '/api/pages/' + id,
    method: 'delete',
    data: { },
    payload: {  }
  } 
}

function setPageActual(id, value) {
  return {
    type: PAGE_SET_ACTUAL,
    uri: '/api/pages/' + id + '/actual',
    method: 'post',
    data: { actual:value },
    payload: {  }
  } 
}

module.exports = {
  loadPages,
  deletePage,
  setPageActual
}
