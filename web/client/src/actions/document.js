import { 
  DOCUMENTS_LOAD,
  LAST_DOCUMENTS_LOAD,
  DOCUMENT_LOAD_BY_ID
} from './const'

export default {
  loadDocuments: ({projectId, parentId}) => {
    return {
      type: DOCUMENTS_LOAD,
      uri: '/api/projects/' + projectId + '/documents',
      method: 'get',
      data: { parentId, plain: false },
      payload: {  }
    }
  },
  loadLastDocuments: (projectId) => {
    return {
      type: LAST_DOCUMENTS_LOAD,
      uri: '/api/documents/last',
      method: 'get',
      data: { projectId },
      payload: {  }
    }
  }
}
