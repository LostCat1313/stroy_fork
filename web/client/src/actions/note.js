import { 
  NOTES_DELETE_ALL
} from './const'

function deleteAllNotes(noteIds) {
  return {
    type: NOTES_DELETE_ALL,
    uri: '/api/notes/',
    method: 'delete',
    data: { noteIds },
    payload: {  }
  } 
}

module.exports = {
  deleteAllNotes
}
