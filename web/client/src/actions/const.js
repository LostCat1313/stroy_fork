import { generateCrudActionTypes } from './generic'

const ACTIONS_KEYS = [
  'GLOBAL_SET_HEADER',
  'GLOBAL_SET_SUBHEADER',
  'RESET_SESSION',
  'VERIFY_TOKEN',
  'VERIFY_TOKEN_REG',
  'LOGOUT',
  'LOGIN',
  'REGISTER',
  'REGISTER_UPDATE',
  'REGISTER_ACTIVATE',
  'CONTACTS_LOAD',
  // organization operations
  'ORGANIZATION_USERS_LOAD',
  'ORGANIZATION_USERS_SAVE',
  // project operations
  'PROJECT_USERS_LOAD',
  'PROJECT_USERS_SAVE',
  // positions
  'POSITIONS_LOAD',
  // pages
  'PAGES_LOAD',
  'PAGE_DELETE',
  'PAGE_NUMBER_UPDATE',
  'PAGE_SET_ACTUAL',
  // documents
  'DOCUMENT_CURRENT',
  'DOCUMENT_LOAD_BY_ID',
  'LAST_DOCUMENTS_LOAD',
  // notes
  'NOTES_DELETE_ALL'
]
const ACTIONS = generateKeys('', ACTIONS_KEYS)
// const REQUEST_POSTFIXES = ['_REQUEST', '_SUCCESS', '_FAILURE']
const c = {
  // prefixes
  ...ACTIONS,
  ...initEntity('User'),
  ...initEntity('Project'),
  ...initEntity('Document'),
  ...initEntity('Organization'),
  ...initEntity('Notification'),
  ...initEntity('Position'),
  ...initEntity('Note'),
  ...initEntity('Descriptor'),
  ...initEntity('Task'),
  ...initEntity('File'),
  ...initEntity('Group'),
  ...initEntity('Right'),
  ...initEntity('RegId'),
}

function initEntity(entity) {
  return generateKeys('', generateCrudActionTypes(entity))
}

function generateAllKeys(prefixes, postfixes) {
  var res = {}
  prefixes.forEach(function(prefix){
    postfixes.forEach(function(p) {
      res[prefix+p] = prefix+p
      return true
    })
    return true
  })
  return res
}

function generateKeys(prefix, postfixes) {
  var res = {}
  postfixes.forEach(function(p) {
    res[prefix+p] = prefix+p
    return true
  })
  return res
}

function generateActionTypes(prefix, postfixes) {
  var res = []
  postfixes.forEach(function(p) {
    res.push(prefix+p)
    return true
  })
  return res
}

module.exports = c

