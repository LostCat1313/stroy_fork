import { 
  NOTIFICATIONS_LOAD
} from './const'

function loadNotifications(projectId, parentId) {
  return {
    type: NOTIFICATIONS_LOAD,
    uri: '/api/notifications',
    method: 'get',
    data: { offset: 0 },
    payload: {  }
  }
}

module.exports = {
  loadNotifications
}
