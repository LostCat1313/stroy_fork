import { 
  PROJECT_USERS_LOAD,
  PROJECT_USERS_SAVE
} from './const'

function loadProjectUsers(id) {
  return {
    type: PROJECT_USERS_LOAD,
    uri: '/api/projects/' + id + '/users',
    method: 'get',
    data: {  },
    payload: {  }
  } 
}


function saveProjectUsers(params) {
  return {
    type: PROJECT_USERS_SAVE,
    uri: '/api/projects/' + params.projectId + '/users',
    method: 'put',
    data: { ...params },
    payload: {  }
  } 
}

module.exports = {
  loadProjectUsers,
  saveProjectUsers
}
