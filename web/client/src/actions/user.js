import { 
  CONTACTS_LOAD
} from './const'

function loadContacts(userId) {
  return {
    type: CONTACTS_LOAD,
    uri: '/api/contacts',
    method: 'get',
    data: { userId },
    payload: {  }
  }
}

// TODO: make local id => entry db. setcurrent not good
module.exports = {
  loadContacts
}
