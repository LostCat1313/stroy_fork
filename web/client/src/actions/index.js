import { generateCrud } from './generic'
import document from './document'
import organization from './organization'
import project from './project'
import user from './user'
import page from './page'
import notification from './notification'
import note from './note'
import * as session from './session';
import right from './right'
import global from './global'

const actions = {
   // session
   ...session,
   ...generateCrud('User', { baseUri: '/api/users/'}),
   ...generateCrud('Project', { baseUri: '/api/projects/', /*load: { data: { all: true }}*/ }),
   ...generateCrud('Organization', { baseUri: '/api/organizations/'}),
   ...generateCrud('Position', { baseUri: '/api/positions/'}),
   ...generateCrud('Document', { baseUri: '/api/documents/'}),
   ...generateCrud('DocumentVersion', { baseUri: '/api/document/versions'}),
   ...generateCrud('Note', { baseUri: '/api/notes/'}),
   ...generateCrud('Descriptor', { baseUri: '/api/descriptors/'}),
   ...generateCrud('Task', { baseUri: '/api/tasks/'}),
   ...generateCrud('File', { baseUri: '/api/files/'}),
   ...generateCrud('Group', { baseUri: '/api/groups/'}),
   ...generateCrud('Right', { baseUri: '/api/rights/'}),
   ...generateCrud('RegId', { baseUri: '/api/regids/'}),
   ...user,
   ...organization,
   ...project,
   ...document,
   ...page,
   ...notification,
   ...note
   // ,
  //  ...right
}

module.exports = actions
