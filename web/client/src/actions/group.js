import { 
  GROUP_USERS_LOAD,
  GROUP_USERS_SAVE
} from './const'

function loadGroupUsers(orgId) {
  return {
    type: GROUP_USERS_LOAD,
    uri: '/api/groups/' + orgId + '/users',
    method: 'get',
    data: {  },
    payload: {  }
  } 
}


function saveGroupUsers(orgId, ids) {
  return {
    type: GROUP_USERS_SAVE,
    uri: '/api/groups/' + orgId + '/users',
    method: 'put',
    data: { userIds: ids },
    payload: {  }
  } 
}

module.exports = {
  loadGroupUsers,
  saveGroupUsers
}
