import React, { PureComponent, PropTypes } from 'react';
import { Animated, Easing, View } from 'react-native';

const ANIMATED_EASING_PREFIXES = ['easeInOut', 'easeOut', 'easeIn'];

// Скопированный https://github.com/oblador/react-native-collapsible/blob/master/Collapsible.js
// как основа для изменений и создания своих анимаций и пр.
// На данный момент анимация полностью отключена

class Expandable extends PureComponent {

    // Типы свойств
    static propTypes = {
        align: PropTypes.oneOf(['top', 'center', 'bottom']),
        expanded: PropTypes.bool,
        collapsedHeight: PropTypes.number,
        duration: PropTypes.number,
        easing: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.func,
        ]),
        style: View.propTypes.style,
    };

    // Начальные значения
    static defaultProps = {
        align: 'top',
        expanded: true,
        collapsedHeight: 0,
        duration: 300,
        easing: 'easeOutCubic',
    };

    // Вызывается когда изменились props/state
    componentWillReceiveProps(nextProps) {
        if (nextProps.expanded !== this.props.expanded) {
            this._toggle(nextProps.expanded);
        } else if (nextProps.expanded && nextProps.collapsedHeight !== this.props.collapsedHeight) {
            this.state.height.setValue(nextProps.collapsedHeight);
        }
    }

    constructor(props) {
        super(props);
        this.state = {
            measuring: false,
            measured: false,
            height: undefined, //new Animated.Value(props.collapsedHeight),
            contentHeight: undefined,
            animating: false,
        };
    }

    contentHandle = null;

    _handleRef = (ref) => {
        this.contentHandle = ref;
    }

  _measureContent(callback) {
    this.setState({
      measuring: true,
    }, () => {
      requestAnimationFrame(() => {
        if (!this.contentHandle) {
          this.setState({
            measuring: false,
          }, () => callback(this.props.collapsedHeight));
        } else {
          this.contentHandle.getNode().measure((x, y, width, height) => {
            this.setState({
              measuring: false,
              measured: true,
              contentHeight: height,
            }, () => callback(height));
          });
        }
      });
    });
  }

    _toggle(expanded) {
        console.log(expanded);
        if (expanded) {
            this._measureContent(contentHeight => {
                // console.log("toggle 1 -> " + contentHeight);
                this._transitionToHeight(contentHeight);
            });
        } else if (!this.contentHandle) {
            if (this.state.measured) {
                // console.log("toggle 2 -> " + this.state.contentHeight);
                this._transitionToHeight(this.state.contentHeight)
            }
        } else {
            // console.log("toggle 3 -> " + this.props.collapsedHeight);
            this._transitionToHeight(this.props.collapsedHeight);
        }
    }

    _transitionToHeight(height) {
        // const { duration } = this.props;
        // let easing = this.props.easing;
        // if (typeof easing === 'string') {
        //     let prefix;
        //     let found = false;
        //     for (let i = 0; i < ANIMATED_EASING_PREFIXES.length; i++) {
        //         prefix = ANIMATED_EASING_PREFIXES[i];
        //         if (easing.substr(0, prefix.length) === prefix) {
        //             easing = easing.substr(prefix.length, 1).toLowerCase() + easing.substr(prefix.length + 1);
        //             prefix = prefix.substr(4, 1).toLowerCase() + prefix.substr(5);
        //             easing = Easing[prefix](Easing[easing || 'ease']);
        //             found = true;
        //             break;
        //         }
        //     }
        //     if (!found) {
        //         easing = Easing[easing];
        //     }
        //     if (!easing) {
        //         throw new Error('Invalid easing type "' + this.props.easing + '"');
        //     }
        // }
        //
        // if (this._animation) {
        //     this._animation.stop();
        // }
        // this.setState({ animating: true });
        // this._animation = Animated.timing(this.state.height, {
        //     toValue: height,
        //     duration,
        //     easing,
        // }).start(event => this.setState({ animating: false }));
        this.setState({
            height: height
        })
    }

    _handleLayoutChange = (event) => {
        const contentHeight = event.nativeEvent.layout.height;
        if (this.state.animating || this.props.expanded || this.state.measuring || this.state.contentHeight === contentHeight) {
            return;
        }
        this.state.height.setValue(contentHeight);
        this.setState({ contentHeight });
    };

    render() {
        const { expanded } = this.props;
        const { height, contentHeight, measuring, measured } = this.state;
        const hasKnownHeight = !measuring && (measured || expanded);
        const style = hasKnownHeight && {
            overflow: 'hidden',
            height: height,
        };
        const contentStyle = {};

        if (measuring) {
            contentStyle.position = 'absolute',
            contentStyle.opacity = 0;
        } else if (this.props.align === 'center') {
            contentStyle.transform = [{
                translateY: height.interpolate({
                    inputRange: [0, contentHeight],
                    outputRange: [contentHeight / -2, 0],
                }),
            }];
        } else if (this.props.align === 'bottom') {
            contentStyle.transform = [{
                translateY: height.interpolate({
                inputRange: [0, contentHeight],
                outputRange: [-contentHeight, 0],
                }),
            }];
        }
        return (
            <Animated.View
                ref={this._handleRef}
                style={[style, this.props.style, contentStyle]}
                onLayout={this.state.animating ? undefined : this._handleLayoutChange}>

                {this.props.children}

            </Animated.View>
        );
    }
}

export default Expandable;
