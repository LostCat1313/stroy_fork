const React = require('react-native');
const { StyleSheet } = React;

export default {
    container: {
        elevation: 4,
        backgroundColor: '#28B6F6',
        position: 'absolute',
        bottom: 110,
        right: 20,
        width: 150,
    },
    button: {
        backgroundColor: '#28B6F6',
        padding: 10,
        paddingLeft: 20
    },
    sectionHeader: {
        padding: 15,
        backgroundColor: '#DDDDDD',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    sectionHeaderText: {
        color: 'black'
    },
    listItem: {
        padding: 12,
        paddingLeft: 26,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    listItemText: {
        color: 'black'
    },
    listItemCount: {
        paddingLeft: 5,
        paddingRight: 5,
        backgroundColor: '#FA9312',
        color: 'black'
    }
};
