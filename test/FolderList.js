import React, { PureComponent } from 'react';
import { SectionList, Text, View, TouchableOpacity } from 'react-native';
import Expandable from './Expandable';
import styles from './styles';

const childs = [
    {
        key: '1',
        title: 'Раздел 1',
        count: 2
    }, {
        key: '2',
        title: 'Раздел 2',
        count: 559
    }, {
        key: '3',
        title: 'Раздел 3',
        count: 2
    }, {
        key: '4',
        title: 'Раздел N',
        count: 2
    }, {
        key: '5',
        title: 'Раздел 71',
        count: 2
    }, {
        key: '6',
        title: 'Раздел 72',
        count: 559
    }, {
        key: '7',
        title: 'Раздел 73',
        count: 2
    }, {
        key: '8',
        title: 'Раздел 7N',
        count: 2
    }
];

const childs1 = [
    {
        key: '11',
        title: 'Раздел 21',
        count: 2
    }, {
        key: '21',
        title: 'Раздел 22',
        count: 559
    }, {
        key: '31',
        title: 'Раздел 23',
        count: 2
    }, {
        key: '41',
        title: 'Раздел 2N',
        count: 2
    }
];

const initialState = {
  list: [
    {
        key: 'АРХИТЕКТУРНЫЕ решения',
        data: childs,
        expanded: true
    }, {
        key: 'ВЕНТИЛЯЦИЯ',
        data: childs1,
        expanded: true
    },{
        key: 'Электрика',
        data: childs1,
        expanded: true
    },{
        key: 'Водоснабжение',
        data: childs1,
        expanded: true
    },{
        key: 'Землеустройство',
        data: childs1,
        expanded: true
    }, {
        key: 'Еще какое-то устройство',
        data: childs1,
        expanded: true
    }
  ],
  selectedIndex: null,
};

class FolderList extends PureComponent {
    expanded = new Map();

    _keyExtractor = (item, index) => item.key;

    _onPressItem = (item) => {
        alert(item.item.title);
    };

    _onPressHeader = (item) => {
        item.section.expanded = !item.section.expanded;
        arr = this.expanded.get(item.section.key);

        item.section.data.map((v) => {
            console.log(v);
            arr[v.key]._toggle(item.section.expanded);
        });
    };

    _addComponentToSection = (sectionKey, itemKey, component) => {
        var components = this.expanded.get(sectionKey);

        if (components === undefined)
            components = [];

        components[itemKey] = component;
        this.expanded.set(sectionKey, components);
    };

    _renderItem = (item) => {
        // console.log(item);
        return (
            <Expandable
                ref={component => this._addComponentToSection(item.section.key, item.item.key, component)}
                expanded={item.section.expanded}>

                <TouchableOpacity
                    style={styles.listItem}
                    onPress={() => this._onPressItem(item)}>

                    <Text style={styles.listItemText}>{item.item.title}</Text>
                    {(item.item.count > 0) && <Text style={styles.listItemCount}>{item.item.count}</Text>}

                </TouchableOpacity>

            </Expandable>
        )
    };

    _renderSectionHeader = (headerItem) => {
        // console.log(headerItem);
        var text = headerItem.section.key.toUpperCase();
        return (
            <TouchableOpacity
                activeOpacity={0.7}
                style={styles.sectionHeader}
                onPress={() => this._onPressHeader(headerItem)}>

                <Text style={styles.sectionHeaderText}>{text}</Text>
                <Text>DOWN</Text>
            </TouchableOpacity>
        )
    };

    _itemSeparatorComponent = () => {
        return <View style={{backgroundColor: '#535353', height: 1, marginLeft: 24}}/>
    };

    render() {
        console.log(this.props.items);
        return (
            <SectionList
                sections={initialState.list}
                keyExtractor={this._keyExtractor}
                renderItem={this._renderItem}
                renderSectionHeader={this._renderSectionHeader}
                ItemSeparatorComponent={this._itemSeparatorComponent}
                stickySectionHeadersEnabled
            />
        );
    }
}

export default FolderList;
